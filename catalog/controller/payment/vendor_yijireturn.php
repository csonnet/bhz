<?php
/***
 * 处理 return_url.php 的业务
 */

class controllerPaymentVendorYijiReturn extends controller{
    public function index(){
        $para = $this->request->get;
        if(!$para['success'] || $para['resultCode'] == 'EXECUTE_FAIL'){
            echo '订单'.$para['orderNo'].$para['resultMessage'];
        }else{
            $this->response->redirect($this->url->link('common/index', '', 'SSL'));
        }
        
   }
}