<?php
/***
 * 结束配货时在线退款的异步回调页面
 * @author sonicsjh
 */
class controllerPaymentYijiRefundV2 extends controller{
    public function index(){
        $config = $this->getConfig();
        $this->load->helper('YijipayClient');
        $client = new YijipayClient($config);
        $params = $this->request->post;

        $this->load->model('checkout/order');
        $OORPK = substr($params['orderNo'], '20');

        $status = 1;
        $ret = 'fail';
        if($client->verify($params)){
            $ret = 'success';
            if('FINISHED' == $params['refundStatus']){
                $status = 2;
            }else{
                $status = 3;
            }
        }
        //更新退款记录的同步返回信息
        $orderOnlineRefundData = array(
            'status' => $status,
            'notify_params' => json_encode($params),
        );
        $this->model_checkout_order->updateOrderOnlineRefund($OORPK, $orderOnlineRefundData);
        //更新退款记录的同步返回信息
        exit($ret);
    }

    public function getConfig(){
        return array(
            'partnerId' => '20161201020011981315', //商户ID
            'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key

            //网关
            'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
            // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
    }
}
