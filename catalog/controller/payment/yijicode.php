<?php

class ControllerPaymentYijicode extends Controller {


	private $key = 'db3d0a81a79b6832c439bbf5ec92b77c';


// 获取参数方法
public function getpara(){
			// 默认get方式
			$para = $this->request->request;
			unset($para['route']);
			return $para;
}

	//查看订单详情方法
	public function getOrderdetail(){

			// 获取参数
			$para=$this->getpara();
			$this->load->helper('yijiSignHelper');
			$data=array();

			$data['orderSta']='99';
			$data['result'] = '01';


				if(!$check=yijiSignHelper::verify($para,$this->getkey())){		
					$data['orderSta']='99';
				}
				
				if (array_key_exists('orderID',$para) && !empty($para['orderID'])) {
					$orderID = $para['orderID'];
				} 

			$this->load->model('payment/yijicode');
			$res=$this->model_payment_yijicode->getOrder($orderID);
			
			// 订单不存在
			if(!$res){
				$data['orderSta'] = '01';
			}

			
			$data['ordAmt'] =(string)(round($res['total'],2)*100);
		

			// 正常
				if(0 == $res['is_pay'] && $check){
					$data['orderSta'] = '00';
					$data['result'] = '00';
				}
			// 订单已支付
				if(1 == $res['is_pay']){
					$data['orderSta'] = '02';
				}

				
			$product_infos=array(
				'merchantPartnerId' => '20161201020011981315',
				'goodsType' => '百货栈',
				'goodsName' => '百货',
				'sellerUserId' => '20161201020011981315'
			);

				if(true||'微信支付' == $res['payment_method'] || '支付宝' == $res['payment_method']){
					$data['exData'] = json_encode($product_infos);
				}
			
			//判断是否为重复查询
			
			// 	if(!isset($_SESSION['check_time']) || (time()-$_SESSION['check_time'])>3){
			// 		$_SESSION['check_time'] = time();
			// 		$_SESSION['check_id'] = NULL;
			// 	}

			// 	if($_SESSION['check_id'] == $orderID){
			// 		$data['orderSta'] = '03';
			// 		$error='重复查询';
			// 	}

			//  $_SESSION['check_id'] = $orderID;


					
				$data['mercId'] = $para['mercId'];
				$data['termId'] = $para['termId'];
				$data['refNumber'] = $para['refNumber'];
				$data['signType'] = $para['signType'];

				$preStr = yijiSignHelper::getPreSignStr($data);
				$mySign = yijiSignHelper::sign($preStr, $this->getkey(), $para['signType']);
				$data['sign'] = $mySign;

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($data));	
						
    }


	// 订单确认方法
	public function OrderConfirm(){

			$para=$this->getpara();
			$this->load->helper('yijiSignHelper');
		
			$data=array();
			$data['result'] = '01';

			if(!$check=yijiSignHelper::verify($para,$this->getkey())){		
					$data['result'] = '01';
				}
			
			if (array_key_exists('orderID',$para) && !empty($para['orderID'])) {
					$orderID = $para['orderID'];
				} else {
					$data['result'] = '01';
				}

			$this->load->model('payment/yijicode');
			$this->load->model('payment/paymentlog');
			$this->load->model('checkout/order');

			$order_info=$this->model_payment_yijicode->getOrder($orderID);
	
			// 更改订单状态
			if('00' == $para['orderSta'] && 0 == $order_info['is_pay'] && $check){	
				$this->model_payment_yijicode->changeorderispay($orderID);
				$res = $this->model_checkout_order->getOrderById($orderID);
				if ($res) {
					$this->model_checkout_order->updatemoney($res,$orderID);
				}
                

				//$order_status = $this->model_payment_yijicode->getOrder($orderID);
				if($order_info['order_status_id'] == 1)
					$this->model_checkout_order->addorderHistory($orderID,$this->model_checkout_order->findOrderStatus('待审核'),'通过POS机支付');
				else
					$this->model_checkout_order->addorderHistory($orderID,$order_info['order_status_id'],'通过POS机支付');

				$this->model_payment_paymentlog->addlog(array(
					'order_id' => $orderID,
					'payment_method' => $order_info['payment_method'],
					'payment_para' => serialize($para),
					'interface_file' => 'yijicode'
				));
				
				$data['result'] = '00';
			}
				
			
				$data['mercId'] = $para['mercId'];
				$data['refNumber'] = $para['refNumber'];
				$data['signType'] = $para['signType'];

				$preStr = yijiSignHelper::getPreSignStr($data);
				$mySign = yijiSignHelper::sign($preStr, $this->getkey(), $para['signType']);
	
				$data['sign'] = $mySign;

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($data));		
    }


	public function getkey(){
		return $this->key;
	}
}