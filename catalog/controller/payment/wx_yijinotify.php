<?php
/***
 * 易极付微信支付回调页面
 */

class controllerPaymentWxYijiNotify extends controller{
    public function index(){
              
        $config = $this->getConfig();

        $this->load->helper('YijipayClient');

        $client = new YijipayClient($config);

        $params = $this->request->post;
        $this->load->model('checkout/order');
        $this->load->model('payment/paymentlog');
        $this->load->model('payment/yijicode');

        $order_id = substr($params['orderNo'],'20');
		$order_id = explode("-",$order_id)[0];

        $order_info=$this->model_payment_yijicode->getOrder($order_id);

        //验签成功
        if($client->verify($params)){   
            if('true' === $params['success']){
                if('EXECUTE_SUCCESS' == $params['resultCode']){
                    $this->model_checkout_order->addtradeNo($order_id,$params['tradeNo']);
                    $this->model_checkout_order->addOrderHistory($order_id,20);
                    $res = $this->model_checkout_order->getOrderById($order_id);
                    if ($res) {
                        $this->model_checkout_order->updatemoney($res,$order_id);
                    }
					
					/*判断在线支付是哪种*/
					if($params['bankCode']){
						if($params['bankCode'] == 'WEIXIN'){
							$method = '微信支付';
						}
						else{
							$method = '银行卡支付';
						}
					}
					else{
						$method = 'POS机支付';
					}
					
					$data['order_id'] = $order_id; 
					$data['payment_method'] = $method; 
					$this->model_checkout_order->changeOrderMethod($data);
					/*判断在线支付是哪种*/

					$this->model_payment_paymentlog->addlog(array(
                        'order_id' => $order_id,
                        'payment_method' => $method,
                        'payment_para' => serialize($params),
                        'interface_file' => 'wx_yijinotify'
                    ));
                    
                    $data = 'success';
                }else if('EXECUTE_PROCESSING' == $params['resultCode']){
                    $data = '正在处理中';
                }else if('WAIT_PAY' == $params['resultCode']){
                    $data = '等待付款';
                }else{
                    $data = '其他错误';
                }
            }else{
               $data = $params['resultMessage'];
            }
        }else{
            $data = '验签失败';
        }
        $this->response->setOutput($data);
   }


      public function getConfig(){
        return array(
        'partnerId' => '20161201020011981315', //商户ID
        'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key
        
        //网关
        'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
        // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
       }
    }
