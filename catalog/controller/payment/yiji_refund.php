<?php
/***
 * 易极付微信支付回调页面
 */

class controllerPaymentYijiRefund extends controller{
    public function index(){
              
        $config = $this->getConfig();

        $this->load->helper('YijipayClient');

        $client = new YijipayClient($config);

        $params = $this->request->post;

        $this->load->model('checkout/order');
        $order_id = substr($params['orderNo'],'20');

        //验签成功
        if($client->verify($params)){   
            if('true' === $params['success']){
                if('EXECUTE_SUCCESS' == $params['resultCode'] || 'refundStatus' == $params['refundStatus']){
                    $this->model_checkout_order->addtradeNohistory($order_id,$params);
                    $data = 'success';
                }
            }else{
               $data = $params['resultMessage'];
            }
        }else{
            $data = '验签失败';
        }
        $this->response->setOutput($data);
   }


      public function getConfig(){
        return array(
        'partnerId' => '20161201020011981315', //商户ID
        'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key
        
        //网关
        'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
        // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
       }
    }
