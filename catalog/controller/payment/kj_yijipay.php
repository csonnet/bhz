<?php
class ControllerPaymentKjyijiPay extends Controller {

    
    public  function index(){

        $this->load->helper('yijiSignHelper');
        $this->load->helper('dto/FastPayTrade.class');
        $this->load->helper('YijipayClient');
        $this->load->helper('mobile');


        if(isset($this->session->data['order_id'])){
            $order_id = $this->session->data['order_id'];
        }else{
            $order_id = 0;
        }
       
        $this->load->model('payment/wx_yijipay');
        $this->load->model('payment/kj_yijipay');

        $order_info = $this->model_payment_wx_yijipay->getOrder($order_id);
        $user_id = $this->model_payment_kj_yijipay->getUserId($order_info['customer_id']);
        
        //print_r($user_id);exit;

        $config = $this->getConfig();
        $user = $this->getUser($order_info,$user_id);
        
        $objReq = new FastPayTrade();
        
        //请求公共部分
        $objReq->setReturnUrl('http://m.bhz360.com/index.php?route=payment/wx_yijireturn');
        $objReq->setNotifyUrl('http://m.bhz360.com/index.php?route=payment/wx_yijinotify');
        
        $objReq->setPartnerId($config['partnerId']);

        $objReq->setOrderNo($this->getBefstr().$order_id);
        $objReq->setMerchOrderNo($this->getBefstr().$order_info['order_id']);
        $objReq->setSignType("MD5");
        //构建交易参数
        $item1 = $this->getTradeInfo($user,$order_info);
        
        $objReq->setBuyerOrgName($order_info['payment_company']);
        $objReq->setBuyerRealName($order_info['payment_fullname']);
        if(intval($user['buyer_uid_yiji']) != 0){
            $objReq->setBuyerUserId($user['buyer_uid_yiji']);
        }
        $objReq->setOutUserId($user['buyer_id']);
        $objReq->setTradeInfo([$item1]);
        
        //收银台参数
        $objReq->setPaymentType("QUICKPAY");
        $objReq->setUserTerminalType("PC");

        if(is_mobile()){
            $objReq->setOpenid($this->session->data['weixin_login_openid']);
            $objReq->setPaymentType("PAYMENT_TYPE_SUPER");
            $objReq->setUserTerminalType("MOBILE");
        }

        //构建请求
        $client = new YijiPayClient($config);
        
        $response = $client->execute($objReq);
        return $response; 
    }
    
    
    public function getTradeInfo($user,$order_info){
        //构建订单
        $this->load->helper('dto/TradeInfo.class');
        $item1 = new TradeInfo();
        $item1->setTradeName("百货栈");
        $item1->setSellerUserId($user['seller_uid_yiji']);
        $item1->setSellerOrgName($user['seller_name']);
        $item1->setMerchOrderNo($this->getBefstr().$order_info['order_id']);
        $item1->setTradeAmount($order_info['total']);
        $item1->setGoodsName("百货栈百货");
        $item1->setCurrency("CNY");
        
        $item1->setMemo("进货");

        return $item1;
    }
    
    
    
    public function getConfig(){
        return array(
        'partnerId' => '20161201020011981315', //商户ID
        'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key
        
        //网关
        'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
        // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
    }
    
    public function getUser($order_info,$user_id){
        
        // dump($order_info);die();
        $user['seller_uid_yiji'] = "20161201020011981315";          //卖家易极付userId
        $user['seller_id'] = "3314";                //卖家商户平台上的会员Id
        $user['seller_name'] = "百货栈";
        
        //买家信息 buyer TODO: 修改为你自己的买家信息
        $user['buyer_uid_yiji'] = $user_id['yiji_userid'];           //买家易极付userId
        /*if(intval($user['buyer_uid_yiji']) == 0){
            $this->load->model('payment/yiji_register');
            $yiji_id = $this->model_payment_yiji_register->getUserId($user_id['fullname']);

            $this->load->model('account/customer');
            $this->model_account_customer->saveVip($user_id['fullname'],$yiji_id);
            $user['buyer_uid_yiji'] = $yiji_id;
        }*/
        $user['buyer_id'] = $order_info['customer_id'];                 //买家商户平台上的会员Id
        $user['buyer_name'] = $order_info['payment_fullname'];                            //买家商户平台上的会员名称
        return $user;
    }


    public function getBefstr(){
         return $order_befstr='BHZ2017ORDERS57'.substr(time(),'-5');
    }
}