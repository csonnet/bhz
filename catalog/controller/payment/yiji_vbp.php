<?php
//工厂对账单打款返回（企付通）
class controllerPaymentYijiVbp extends controller{
    public function ret(){//同步返回
        $config = $this->_getConfig();
        $this->load->helper('YijipayClient');
        $client = new YijipayClient($config);
        $params = $this->request->get;

        $this->load->model('tool/yjf_log');
        $VBNPID = substr($params['orderNo'], '20');

        $ret = '打款申请处理中。。。';
        $status = 1;
        if($client->verify($params)){
            $ret = '工厂对账单付款失败，请确认对方收款信息后重试或联系it部门。。。';
            if($params['success']){
                if ('TRADE_CANCEL' == $params['tradeStatus']) {//操作人取消转账，不会有异步返回信息
                    $status = 4;
                //}else if ('EXECUTE_SUCCESS' == $params['resultCode']){
                    //$status = 2;
                }else if ('EXECUTE_FAIL' == $params['resultCode']) {
                    $status = 3;
                }
            }else{
                $status = 3;
            }
        }
        //更新同步返回信息
        $VBNPData = array(
            'status' => $status,
            'return_params' => json_encode($params),
        );
        $this->model_tool_yjf_log->updateReturnData($VBNPID, $VBNPData, 'vendor_bill_new_payment');
        //更新同步返回信息
        exit($ret);
    }

    public function not(){//异步返回
        $config = $this->_getConfig();
        $this->load->helper('YijipayClient');
        $client = new YijipayClient($config);
        $params = $this->request->post;

        $this->load->model('tool/yjf_log');
        $VBNPID = substr($params['orderNo'], '20');

        $ret = 'fail';
        $status = 1;
        if($client->verify($params)){
            $ret = 'success';
            if ('FAILED' == $params['itemTradeStatus']) {
                $status = 3;
            }else{
                if($params['success']){
                    if('EXECUTE_SUCCESS' == $params['resultCode']){
                        $status = 2;
                    }
                }else{
                    $status = 3;
                }
            }
        }
        //更新异步返回信息
        $VBNPData = array(
            'status' => $status,
            'notify_params' => json_encode($params),
        );
        $this->model_tool_yjf_log->updateReturnData($VBNPID, $VBNPData, 'vendor_bill_new_payment');
        //更新异步返回信息
        exit($ret);
    }

    protected function _getConfig(){
        return array(
            'partnerId' => '20161201020011981315', //商户ID
            'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key

            //网关
            'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
            // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
    }
}
