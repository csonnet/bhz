<?php
class ControllerAnalyticsGoogleAnalytics extends Controller {
    public function index() {
		return html_entity_decode($this->config->get('google_analytics_code'), ENT_QUOTES, 'UTF-8');
    }
    //热销商品统计
    public function hotproducts(){
        $userid=$this->request->get['userid'];
        $this->load->model('public/public');
        $orderid=$this->model_public_public->getorderProductid($userid);
        $orderids=array_column($orderid, 'order_id');
        $string=implode(',', $orderids);
        $gropcode=$this->model_public_public->getgropcode($string);
        $product_codes=array_column($gropcode, 'order_product_id');
        $productcodes=implode(',', $product_codes);

         $exclude=$this->model_public_public->gethotproducts($string,$productcodes);
         foreach ($exclude as $key => $value) {
            foreach ($exclude as $k => $v) {
                if($value['name']==$v['name']&&$v['nub']==1){
                     unset($exclude[$k]);
                }else{
                     $end[$k]=$v;
                }
            }

         }
        $result['success']=$end;
       $this->response->addHeader('Content-Type: application/json');

         if($result['success']){
           $this->response->setOutput(json_encode($result));
         }else{
          $result['error']="无数据...";
          $this->response->setOutput(json_encode($result));
        }


    }
}
