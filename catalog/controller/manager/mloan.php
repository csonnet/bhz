<?php

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerManagerMloan extends RestController {
	
	//»ñÈ¡´ýÎÒ²Ù×÷µÄ½è»õµ¥
	public function getToDo(){

		$this->checkPlugin();
		$this->load->model('manager/mloan');

		$filter['start'] = 0;

		if(isset($this->request->get['limit'])){
			$filter['limit'] = $this->request->get['limit'];
		}
		else{
			$filter['limit'] = 10;
		}

		$json = array('success' => true);
		
		//»ñÈ¡ÓÃ»§userid
		$this->load->model('public/public');
		$user = $this->model_public_public->getUser();
		$filter['user_id'] = $user[0]['user_id'];

		$results = $this->model_manager_mloan->getToDo($filter);

		foreach($results as $v){

			switch($v['status']){
				case 0:
				case 1:
					$json['verifyloans'][] = $v;
					break;
				case 4:
					$json['signloans'][] = $v;
					break;
				case 5:
					$json['returnloans'][] = $v;
					break;
				default;
					break;
			}
		
		}

		$this->response->setOutput(json_encode($json));

	}
	
	//»ñÈ¡ÎÒ·¢ÆðµÄ½è»õµ¥
	public function getMyLoan(){

		$this->checkPlugin();
		$this->load->model('manager/mloan');

		$filter['start'] = 0;

		if(isset($this->request->get['limit'])){
			$filter['limit'] = $this->request->get['limit'];
		}
		else{
			$filter['limit'] = 10;
		}

		$json = array('success' => true);

		//»ñÈ¡ÓÃ»§userid
		$this->load->model('public/public');
		$user = $this->model_public_public->getUser();
		$filter['user_id'] = $user[0]['user_id'];

		$results = $this->model_manager_mloan->getMyLoan($filter);
		$json['myloans'] = $results;

		$this->response->setOutput(json_encode($json));

	}


	public function loanorder() {
        $this->checkPlugin();
        $order_data = array();
        $json = array('success' => true);

        if (!$this->cart->hasShipping()) {
            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }
        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json["error"]['warning']		= "购物车内产品不足或购物车为空";
            $json["success"]	= false;
        }

        // Validate minimum quantity requirements.
        $products = $this->cart->getProducts();

        $this->load->model('account/customer');
        $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
        $order_data['email'] = $customer_info['email'];

        //用户是否有关联物流营销中心检查
        $logcenter_id = $this->customer->getLogcenterId();
        if(!$logcenter_id || $logcenter_id == 0) {
            $json['success'] = false;
            $json["error"]['warning'] = '用户还未关联物流营销中心，不能下单';
            $this->response->setOutput(json_encode($json));
            return;
        }

        if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
            $requestjson = file_get_contents('php://input');
            $requestjson = json_decode($requestjson, true);
            $post  = $requestjson;
            $post_address_id = $post['address_id'];
            if(isset($post['address_id'])) {
                $post_address_id=$post['address_id'];
                $this->load->model('account/address');
                $address_info = $this->model_account_address->getAddress($post_address_id);
                // array(18) { ["address_id"]=> string(4) "4359" ["fullname"]=> string(6) "测试" ["company"]=> string(9) "it.moshan" ["address"]=> string(9) "长阳路" ["postcode"]=> string(0) "" ["shipping_telephone"]=> string(11) "17317253840" ["city"]=> string(9) "长宁区" ["city_id"]=> string(2) "40" ["zone_id"]=> string(1) "3" ["zone"]=> string(9) "上海市" ["zone_code"]=> string(0) "" ["country_id"]=> string(1) "9" ["country"]=> string(9) "上海市" ["iso_code_2"]=> string(2) "SH" ["iso_code_3"]=> string(2) "SH" ["address_format"]=> string(0) "" ["is_default"]=> string(1) "1" ["custom_field"]=> array(0) { } }
                 // {{selected_address.country + selected_address.zone + selected_address.city + selected_address.address}}
                // var_dump($address_info);die();
                if(!$address_info){
                    $json["success"] = false;
                    $json['error']['warning'] = '请选择收货地址！';
                    $this->response->setOutput(json_encode($json));
                }else{
                    $loaddata['shipping_tel'] = $address_info['shipping_telephone'];
                    $loaddata['shipping_name'] = $address_info['company'];
                    $loaddata['customer_id'] = $this->customer->getId();
               		$loaddata['shipping_address'] = $address_info['country'].$address_info['zone'].$address_info['city'].$address_info['address'];

                }
            }else{
                $json["success"] = false;
                $json['error']['warning'] = '请选择收货地址！';
                $this->response->setOutput(json_encode($json));
            }

      
        }
        $loan_order_id = M('loan_order')->add($loaddata);
        $produncts = $this->cart->getProducts();
        foreach ($produncts as $key => $value) {
        	$loadproduct['loan_order_id'] = $loan_order_id;
        	$loadproduct['product_code'] = $value['option'][0]['product_code'];
        	$loadproduct['product_price'] = $value['price'];
        	$loadproduct['apply_qty'] = $value['quantity'];
    		M('loan_order_details')->add($loadproduct);

        }

		$this->response->setOutput(json_encode($json));


  }

}

?>