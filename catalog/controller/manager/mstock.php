<?php

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerManagerMstock extends RestController {
	
	//获取业务员全部出库单
	public function getStockOut(){
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');
		
		$filter['start'] = 0;

		if(isset($this->request->get['limit'])){
			$filter['limit'] = $this->request->get['limit'];
		}
		else{
			$filter['limit'] = 10;
		}

		$json = array('success' => true);

		$customer_id = $this->customer->getId();
		
		//取业务员的仓库
		$this->load->model('account/customer');

		$tel = $this->model_manager_mstock->getTel($customer_id);
		$logcenter = $this->model_account_customer->userCheckByTelephone($tel);
		if ($customer_i!=2786) {
			$filter['logcenter_id'] = $logcenter['MG_logcenterId'];
			
		}


		$results = $this->model_manager_mstock->getStockOut($filter);

		foreach($results as $v){

			switch($v['tab']){
				case 'send':
					$json['outList']['send'][] = $v;
					$json['sendCount'] ++;
					break;
				case 'sign':
					$json['outList']['sign'][] = $v;
					$json['signCount'] ++;
					break;
				case 'reject':
					$json['outList']['reject'][] = $v;
					$json['rejectCount'] ++;
					break;
				case 'over':
					$json['outList']['over'][] = $v;
					$json['overCount'] ++;
					break;
				default;
					break;
			}
		
		}

		$json['logcenter_id'] = $filter["logcenter_id"];

		$this->response->setOutput(json_encode($json));

	}
	
	//加载更多单据
	public function loadMoreOut(){

		$json = array('success' => true);

		$json['count'] = 0;
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');

		if(isset($this->request->get['status']) && $this->request->get['status'] != 'all'){
			$filter['status'] = $this->request->get['status'];
		}

		if(isset($this->request->get['tab'])){
			$filter['tab'] = $this->request->get['tab'];
		}

		if(isset($this->request->get['limit'])){
			$filter['limit'] = $this->request->get['limit'];
		}
		else{
			$filter['limit'] = 10;
		}
		
		if(isset($this->request->get['start'])){
			$filter['start'] = $this->request->get['start'];
		}

		if(isset($this->request->get['logcenter_id'])){
			$filter['logcenter_id'] = $this->request->get['logcenter_id'];
		}

		$results = $this->model_manager_mstock->loadMoreOut($filter);

		foreach($results as $v){

			$json['outList'][$filter['tab']][] = $v;
			$json['count'] ++;
		
		}

		$this->response->setOutput(json_encode($json));

	}
	
	//点击配送按钮
	public function checkSend(){
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$message = $this->model_manager_mstock->checkSend($data);

		$json = array(
			'success' => true,	
			'message' => $message
		);
		$this->response->setOutput(json_encode($json));

	}

	//点击签收按钮
	public function checkSign(){
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$message = $this->model_manager_mstock->checkSign($data);

		$json = array(
			'success' => true,	
			'message' => $message
		);
		$this->response->setOutput(json_encode($json));

	}

	//点击拒收按钮
	public function checkReject(){
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$message = $this->model_manager_mstock->checkReject($data);

		$json = array(
			'success' => true,	
			'message' => $message
		);
		$this->response->setOutput(json_encode($json));

	}
	
	//筛选送货单
	public function searchSend(){
		
		$this->checkPlugin();
		$this->load->model('manager/mstock');

		$requestjson = file_get_contents('php://input');
		$filter = json_decode($requestjson, true);
		
		$filter['start'] = 0;

		if(isset($this->request->get['limit'])){
			$filter['limit'] = $this->request->get['limit'];
		}
		else{
			$filter['limit'] = 10000;
		}

		$json = array('success' => true);

		$customer_id = $this->customer->getId();
		
		//取业务员的仓库
		$this->load->model('account/customer');

		$tel = $this->model_manager_mstock->getTel($customer_id);
		$logcenter = $this->model_account_customer->userCheckByTelephone($tel);

		$filter['logcenter_id'] = $logcenter['MG_logcenterId'];

		$json['searchList'] = $this->model_manager_mstock->searchSend($filter);

		$json['logcenter_id'] = $filter["logcenter_id"];

		$this->response->setOutput(json_encode($json));

	}

}

?>