<?php
class ControllerApiRange extends Controller {
	function getdistance($lng1, $lat1, $lng2, $lat2) {
	    // 将角度转为狐度
	    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
	    $radLat2 = deg2rad($lat2);
	    $radLng1 = deg2rad($lng1);
	    $radLng2 = deg2rad($lng2);
	    $a = $radLat1 - $radLat2;
	    $b = $radLng1 - $radLng2;
	    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
	    return $s;
	} 

	public function customer(){
        
        
		$customer_id = $this->request->get['customer_id'];
		$lnglat = M('address')->where(array('customer_id'=>$customer_id))->field('lng,lat')->find();
		if ($lnglat['lng']==0&&$lnglat['lat']==0) {
			return;
		}
		$where = 'customer_id!='.$customer_id;
		$lnglats = M('address')->field('lng,lat,customer_id')->where($where)-> select();
		foreach ($lnglats as $key => $value) {
			if ($value['lng']==0&&$value['lat']==0) {
				continue;
			}
			$dist = $this->getdistance($lnglat['lng'], $lnglat['lat'], $value['lng'], $value['lat']);
			$dists[]=array(
				'customer_id'=>$value['customer_id'],
				'dist'=>abs($dist)
			);
		}
		$date = array_column($dists, 'dist');

        array_multisort($date,SORT_ASC ,$dists);
        // var_dump($dists);
        $i=0;
        foreach ($dists as $key => $value) {
        	

        	$info = $this->getRangeOrderTotal($value['customer_id']);
        	if (!empty($info['customer_id'])) {
        		$infoarr[$i]=$info;
        		$infoarr[$i]['dist']=$value['dist'];
        		$i++;
        	}
        	
        	if ($i==7) {
        		break;
        	}
        	
        }
        $this->response->setOutput(json_encode($infoarr));
	}
	
	public function getRangeOrderTotal($customer_id){
   		$sql = "SELECT o.customer_id, o.customer_group_id, o.fullname, o.email, o.telephone, o.payment_company, o.payment_address, o.payment_city, o.payment_city_id, o.payment_postcode, o.payment_country, o.payment_country_id, o.payment_zone, o.payment_zone_id, o.shipping_fullname, o.shipping_company, o.shipping_address, o.shipping_city, o.shipping_city_id, o.shipping_country, o.shipping_country_id, o.shipping_zone, o.shipping_zone_id, o.shipping_code, o.shipping_telephone, SUM(o.total) as 'total', o.ip, o.logcenter_id, o.recommend_usr_id FROM `order` AS o  WHERE o.order_status_id<>16 AND o.order_status_id<>0 AND o.customer_id = ".$customer_id;
   		// echo $sql;
   		$query = $this->db->query($sql);
   		return $query->row;
	}

	public function customerOrder(){
        $customer_id = $this->request->get['customer_id'];
        $start= date("Y-m", strtotime("-1 year"));
        $startarr = explode('-', $start);
        for ($i=(int)$startarr[1]; $i <=12 ; $i++) { 
        	if ($i>=10) {
   				$dateArr[] = $startarr[0].$i;
   			}else{
   				$dateArr[] = $startarr[0].'0'.$i;
   			}
        }
        $end  = date("Y-m");
        $endarr = explode('-', $end);
        for ($i=1; $i <=(int)$endarr[1] ; $i++) { 
        	if ($i>=10) {
   				$dateArr[] = $endarr[0].$i;
   			}else{
   				$dateArr[] = $endarr[0].'0'.$i;
   			}
        }

        $sql = "SELECT extract(YEAR_MONTH FROM o.date_added) AS 'yearMonth', sum(o.total) AS 'total', COUNT(o.order_id) AS 'num'FROM `order` AS o WHERE o.order_status_id <> 16 AND o.order_status_id <> 0 AND o.order_status_id <> 13 AND o.order_status_id <> 11 AND  customer_id = ".$customer_id." AND date_added>'".$start."-01 00:00:00' GROUP BY extract(YEAR_MONTH FROM o.date_added)";
        // echo $sql;
        $query = $this->db->query($sql);
        $info = $query->rows;
        foreach ($info as $key => $value) {
        	$yearMonth = $value['yearMonth'];
        	$yearArr = str_split($value['yearMonth'],4);
        	$value['yearMonth'] = $yearArr[0].'-'.$yearArr[1];
           	$infoarr[$yearMonth] = $value;
        }
        foreach ($dateArr as $key => $value) {
        	if(!array_key_exists($value, $infoarr)){
        		$yearArr = str_split($value,4);
        		$value1 = $yearArr[0].'-'.$yearArr[1];
        		$infoarr[$value]['yearMonth'] = $value1;
        		$infoarr[$value]['total'] = 0;
        		$infoarr[$value]['num'] = 0;
        	}
        }
        $date = array_column($infoarr, 'yearMonth');

        array_multisort($date,SORT_ASC ,$infoarr);
        $this->response->setOutput(json_encode($infoarr));
	}

	public function customerProfit(){
        $customer_id = $this->request->get['customer_id'];
        $sql = "SELECT product.product_code, product.price FROM `product`";
        $query = $this->db->query($sql);
        $priceArr = $query->rows;
        foreach ($priceArr as $key => $value) {
        	$pcode = $value['product_code'];
        	$PcodePrice[$pcode] = $value['price'];
        }
        $sql = "SELECT * FROM `order_product` AS op  WHERE op.order_id IN(SELECT o.order_id FROM `order` AS o WHERE o.order_status_id <> 16 AND o.order_status_id <> 0 AND o.order_status_id <> 13 AND o.order_status_id <> 11 AND customer_id = ".$customer_id.")";
        $query = $this->db->query($sql);
        $info = $query->rows;
        foreach ($info as $key => $value) {
        	$pcode = substr($value['product_code'],0,10);
        	$total += ($value['pay_price']-$PcodePrice[$pcode])*$value['quantity'];
        }
        $total =  round($total,2);
        $this->response->setOutput(json_encode($total));
	}

}
