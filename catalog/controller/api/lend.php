<?php
class ControllerApiLend extends Controller {
	public function close(){

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = '无权限';
		} else {

			$this->load->model('checkout/lend');

			if (isset($this->request->get['loan_order_id'])) {
				$loan_order_id = $this->request->get['loan_order_id'];
			} else {
				$loan_order_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result = $this->model_checkout_lend->getlend($loan_order_id);

			if((int)$result['status'] == 0 || (int)$result['status'] == 1){

				if ($result['loan_order_id']) {

					$this->model_checkout_lend->closelend($loan_order_id,$user_id);

				}else {
					$json['error'] = '找不到ID';
				}

			}else{

				$json['error'] = '操作条件不符';

			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function verify(){

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = '无权限';
		} else {

			$this->load->model('checkout/lend');

			if (isset($this->request->get['loan_order_id'])) {
				$loan_order_id = $this->request->get['loan_order_id'];
			} else {
				$loan_order_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			if (isset($this->request->post['comment'])) {
				$comment = $this->request->post['comment'];
			} else {
				$comment = '';
			}

			$data = $this->model_checkout_lend->getlendinfo($loan_order_id);

			$allowverifymark = false;

			if($data['status_id'] == 0 || $data['status_id'] == 1){

				$approvel_ids_arr = explode(',',trim($data['approvel_ids'],','));

				if(!empty($data['approveled_ids'])){
					$approveled_ids_arr = explode(',',trim($data['approveled_ids'],','));
				}else{
					$approveled_ids_arr = array();
				}

				foreach($approvel_ids_arr as $key=>$val){

					if($val == $user_id){

						if($key != 0 || count($approveled_ids_arr) != 0){

							if($key != 0 && count($approveled_ids_arr) != 0){

								if($approveled_ids_arr[count($approveled_ids_arr)-1] == $approvel_ids_arr[$key-1]){

									$allowverifymark = true;

								}

							}

						}else{

							$allowverifymark = true;

						}

						break;

					}			
						
				}

			}

			if($allowverifymark){

				$approveled_ids_arr[] = $user_id;

				$approveled_ids = ','.implode(',',$approveled_ids_arr).',';

				if($approvel_ids_arr[count($approvel_ids_arr)-1] == $user_id){

					$this->model_checkout_lend->finishverify($user_id,$data['status_id'],$approveled_ids,$comment,$loan_order_id);

				}else{

					$this->model_checkout_lend->verify($user_id,$data['status_id'],$approveled_ids,$comment,$loan_order_id);

				}

				$json['success'] = 'ok';


			}else{

				$json['error'] = "非法操作";

			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function rverify(){

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = '无权限';
		} else {

			$this->load->model('checkout/lend');

			if (isset($this->request->get['loan_order_id'])) {
				$loan_order_id = $this->request->get['loan_order_id'];
			} else {
				$loan_order_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			if (isset($this->request->post['comment'])) {
				$comment = $this->request->post['comment'];
			} else {
				$comment = '';
			}

			$data = $this->model_checkout_lend->getlend($loan_order_id);

			$allowverifymark = false;

			if($data['status_id'] == 0 || $data['status_id'] == 1){

				$approvel_ids_arr = explode(',',trim($data['approvel_ids'],','));

				if(!empty($data['approveled_ids'])){
					$approveled_ids_arr = explode(',',trim($data['approveled_ids'],','));
				}else{
					$approveled_ids_arr = array();
				}

				foreach($approvel_ids_arr as $key=>$val){

					if($val == $user_id){

						if($key != 0 || count($approveled_ids_arr) != 0){

							if($key != 0 && count($approveled_ids_arr) != 0){

								if($approveled_ids_arr[count($approveled_ids_arr)-1] == $approvel_ids_arr[$key-1]){

									$allowverifymark = true;

								}

							}

						}else{

							$allowverifymark = true;

						}

						break;

					}			
						
				}

			}

			if($allowverifymark){

				$this->model_checkout_lend->rverify($user_id,$data['status_id'],$comment,$loan_order_id);

				$json['success'] = 'ok';

			}else{

				$json['error'] = "非法操作";

			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function receive(){

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = '无权限';
		} else {

			$this->load->model('checkout/lend');

			if (isset($this->request->post['product_code'])) {
				$product_code = $this->request->post['product_code'];
			} else {
				$product_code = array();
			}

			if (isset($this->request->post['receive_qty'])) {
				$receive_qty = $this->request->post['receive_qty'];
			} else {
				$receive_qty = array();
			}

			if (isset($this->request->get['loan_order_id'])) {
				$loan_order_id = $this->request->get['loan_order_id'];
			} else {
				$loan_order_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$data = $this->model_checkout_lend->getlendinfo($loan_order_id);

			if($data['louserid'] == $user_id && $data['status_id'] == 4){

				$this->model_checkout_lend->receive($product_code,$receive_qty,$user_id,$loan_order_id);

				$json['success'] = 'ok';

			}else{

				$json['error'] = '非法操作';

			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function back(){

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = '无权限';
		} else {

			$this->load->model('checkout/lend');

			if (isset($this->request->post['return_qty_good'])) {
				$return_qty_good = $this->request->post['return_qty_good'];
			} else {
				$return_qty_good = array();
			}

			if (isset($this->request->post['return_qty_bad'])) {
				$return_qty_bad = $this->request->post['return_qty_bad'];
			} else {
				$return_qty_bad = array();
			}

			if (isset($this->request->post['product_code'])) {
				$product_code = $this->request->post['product_code'];
			} else {
				$product_code = array();
			}

			if (isset($this->request->get['loan_order_id'])) {
				$loan_order_id = $this->request->get['loan_order_id'];
			} else {
				$loan_order_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			foreach($return_qty_good as $key=>$val){

				$return_qty_good[$key] = (int)$val;

			}

			foreach($return_qty_bad as $key=>$val){

				$return_qty_bad[$key] = (int)$val;
				
			}

			$data = $this->model_checkout_lend->getlendinfo($loan_order_id);

			if($data['lluserid'] == $user_id && $data['status_id'] == 5){

				$product_arr = $this->model_checkout_lend->getlendetail($loan_order_id);

				$new_product_arr = array();

				foreach($product_arr as $val){

					$new_product_arr[$val['product_code']] = $val['receive_qty'];

				}

				foreach($product_code as $key=>$val){

					if($return_qty_good[$key] + $return_qty_bad[$key] > $new_product_arr[$val]){

						$json['error'] = '商品编码为'.$val.'的正、次品数量合计数超出记录的出库数';

					}

				}	

				if(empty($json['error'])){

					$this->model_checkout_lend->back($product_code,$return_qty_good,$return_qty_bad,$user_id,$loan_order_id,$data['from_warehouse_id']);

					$json['success'] = 'ok';

				}

			}else{

				$json['error'] = '非法操作';

			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}