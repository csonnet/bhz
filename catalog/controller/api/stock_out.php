<?php 
class ControllerApiStockOut extends Controller {
	public function delete(){
		$this->load->language('api/stock_out');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {

			$this->load->model('checkout/stock_out');
			$this->load->model('checkout/order');
			$this->load->model('checkout/lend');

			if (isset($this->request->get['out_id'])) {
				$out_id = $this->request->get['out_id'];
			} else {
				$out_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result = $this->model_checkout_stock_out->getstockoutbyoutid($out_id);

			if($result['status'] == 1){

				if ($result['out_id']) {

					$this->model_checkout_stock_out->delete_stock($result['out_id'],$user_id);

					if((int)$result['refer_type_id'] == 1){

						$this->model_checkout_order->reverse_order($result['out_id']);

					}elseif((int)$result['refer_type_id'] == 6){

						$this->model_checkout_lend->closelend($result['refer_id'],$user_id);

					}

					$data = array(
						'out_id' => $result['out_id'],
						'user_id' => $user_id,
						'comment' => '无效出库单',
					);

					$this->model_checkout_stock_out->addstockhistory($data);

					$json['success'] = $this->language->get('text_success');
				} else {
					$json['error'] = $this->language->get('error_not_found');
				}

			}else
				$json['error'] = '操作条件不符';
		}


		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function recover(){

		$this->load->language('api/stock_out');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {

			$this->load->model('checkout/stock_out');

			if (isset($this->request->get['out_id'])) {
				$out_id = $this->request->get['out_id'];
			} else {
				$out_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result = $this->model_checkout_stock_out->getstockoutbyoutid($out_id);

			if($result['status'] == 0 && $result['refer_type_id'] != 1){

				$result_out_id = $this->model_checkout_stock_out->getstockout($out_id);

				if ($result_out_id) {
					$result = $this->model_checkout_stock_out->recover_stock($out_id,$user_id);
					if($result['status']){
						$json['success'] = $this->language->get('text_success');
						$data = array(
							'out_id' => $out_id,
							'user_id' => $user_id,
							'comment' => '恢复出库单',
						);

						$this->model_checkout_stock_out->addstockhistory($data);
					}else{
						$json['error'] = $result['msg'];
					}
				} else {
					$json['error'] = $this->language->get('error_not_found');
				}

			}else
				$json['error'] = '操作条件不符';
		}


		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function complete(){
		$this->load->language('api/stock_out');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {

			$this->load->model('checkout/stock_out');
			$this->load->model('checkout/order');

			if (isset($this->request->get['out_id'])) {
				$out_id = $this->request->get['out_id'];
			} else {
				$out_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result1 = $this->model_checkout_stock_out->getstockoutbyoutid($out_id);

			if($result1['status'] == 1){
				$result = $this->model_checkout_stock_out->complete_stock($out_id,$user_id);
				if($result['status']){
					$data = array(
							'out_id' => $out_id,
							'user_id' => $user_id,
							'comment' => '完成出库单',
						);

					$this->model_checkout_stock_out->addstockhistory($data);
					
					//添加应收款项
					if ($result1['refer_type_id']==1) {
						$res = $this->model_checkout_stock_out->getMoney($result1['refer_id']);
						if (!empty($res)) {
							$this->model_checkout_stock_out->updatemoney($res,$result1);
							$comments = "增加应收金额：".$result1['sale_money']; 
	 						$this->model_checkout_stock_out->addMHistory($res['money_in_id'],$user_id,$comments);
						}
						if($result['status'] == 'change'){
							$this->model_checkout_order->addOrderHistory($result1['refer_id'],18);
						}elseif($result['status'] == 'change2'){
							$this->model_checkout_order->addOrderHistory($result1['refer_id'],17);
						}
					}elseif($result1['refer_type_id']==6){

						$this->load->model('checkout/lend');

						$this->model_checkout_lend->send($result1['refer_id'],$result1['out_id'],$user_id);

					}
					$json['success'] = $this->language->get('text_success');
				}
				else
					$json['error'] = '未知错误';
			}else{
				$json['error'] = '操作条件不符';
			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function deliver(){

		$this->load->language('api/stock_out');

		$json = array();

		if (!isset($this->session->data['api_id'])) {

			$json['error'] = $this->language->get('error_permission');

		} else {

			$this->load->model('checkout/stock_out');
			$this->load->model('checkout/order');

			if (isset($this->request->get['out_id'])) {
				$out_id = $this->request->get['out_id'];
			} else {
				$out_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result = $this->model_checkout_stock_out->getstockoutbyoutid($out_id);

			if($result['status'] == 2 && $result['refer_type_id'] == 1){

				$changemark = $this->model_checkout_stock_out->deliver_stock($out_id,$user_id);

				if($changemark['status']){

					if($changemark['status'] == 'change'){

						$this->model_checkout_order->addOrderHistory($changemark['order_id'],3,'出库单全部确认派送');

					}

				}

			}else{
				$json['error'] = '操作条件不符';
			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function finalcomplete(){

		$this->load->language('api/stock_out');

		$json = array();

		if (!isset($this->session->data['api_id'])) {

			$json['error'] = $this->language->get('error_permission');

		} else {

			$this->load->model('checkout/stock_out');
			$this->load->model('checkout/order');

			if (isset($this->request->get['out_id'])) {
				$out_id = $this->request->get['out_id'];
			} else {
				$out_id = 0;
			}

			if (isset($this->request->get['user_id'])) {
				$user_id = $this->request->get['user_id'];
			} else {
				$user_id = 0;
			}

			$result = $this->model_checkout_stock_out->getstockoutbyoutid($out_id);

			if($result['status'] == 3 && $result['refer_type_id'] == 1){

				$changemark = $this->model_checkout_stock_out->final_complete_stock($out_id,$user_id);

				if($changemark['status']){

					if($changemark['status'] == 'change'){

						$this->model_checkout_order->addOrderHistory($changemark['order_id'],5,'出库单全部确认签收');

					}

				}

			}else{
				$json['error'] = '操作条件不符';
			}

		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
}