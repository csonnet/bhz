<?php

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerBalanceRecharge extends RestController {
	
	//遍历充值方式
	public function getMethod(){
		
		$this->checkPlugin();

		$json = array('success' => true);

		$this->load->model('extension/extension');
		$results = $this->model_extension_extension->getExtensions('payment');
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status') && $result['code'] != 'cod' && $result['code'] != 'magfin' && $result['code'] != 'balance') {
				$this->load->model('payment/' . $result['code']);
				$method = $this->{'model_payment_'.$result['code']}->getMethod($this->session->data['payment_address'], 10000);
				if ($method) {
					$method_data[$result['code']] = $method;
				}

			}
		}

		$sort_order = array();
		foreach ($method_data as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}			
		array_multisort($sort_order, SORT_ASC, $method_data);

		$this->session->data['payment_methods'] = $method_data;		

		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods']; 
		} else {
			$data['payment_methods'] = array();
		}
	  	
		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}

		$json["payment_info"] = $data;

		//添加是否允许免息账期相关信息
		$json['is_magfin'] = $this->customer->getIsMagfin();
		$json['log_name'] = $this->customer->getLogName(); 
		$json['log_user'] = $this->customer->getLogUser(); 
		$json['log_telephone'] = $this->customer->getLogTelephone();

		$this->response->setOutput(json_encode($json));

	}
	
	//检查充值表单
	public function checkout(){

		$this->checkPlugin();

		$this->load->model('balance/recharge');
		$json = array('success' => true);

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);
		
		//验证用户账号输入是否正确
		$account = $this->model_balance_recharge->checkAccount($data['telephone']);
		if(!$account){
			$json['error']['warning'] ="账号不存在";
		}

		//验证充值金额格式
		$money_reg = "/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/";
		if (!preg_match($money_reg, $data['money'])) {
			$json['error']['warning'] ="请正确填写金额数字";
		}

		$json['rechargeInfo'] = $data;
		
		$this->response->setOutput(json_encode($json));

	}
	
	//提交充值
	public function confirmRecharge(){
		
		$this->load->model('balance/recharge');

		$json = array('success' => true);

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$paymentCode = $data['payment_code'];
		$balancetype = $data['balance_type'];

		$filter = array(
			'runner_id' => $this->customer->getId(),
			'runner_type' => 1,
			'account' => $data['telephone'],
			'money' => $data['money'],
			'payment_code' => $data['payment_code'],
			'payment_method' => $data['payment_method'],
			'balance_type' => $balancetype,
			'status' => 2
		);

		switch($paymentCode){

			case "alipay_direct":
				$this->session->data['recharge_id'] = $this->model_balance_recharge->addRecharge($filter);
				$data['payment'] = $this->load->controller('balance/'.$paymentCode);
				$this->gateway_pay($data);
				break;
			case "kj_yijipay":
				$this->session->data['recharge_id'] = $this->model_balance_recharge->addRecharge($filter);
				$data['payment'] = $this->load->controller('balance/'.$paymentCode);	
				$data['redirect'] = $data['payment'];
				break;
			default:
				break;

		}

		$json["data"] = $data;

		if($this->debugIt){
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		}
		else{
			$this->response->setOutput(json_encode($json));
		}

	}
	
	//再次充值
	public function rechargeAgain(){
		
		$this->load->model('balance/recharge');

		$json = array('success' => true);

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);
		$paymentCode = $data['payment_code'];

		$filter = array(
			'payment_code' => $data['payment_code'],
		);

		switch($paymentCode){

			case "alipay_direct":
				$filter['payment_method'] = '支付宝';
				$this->session->data['recharge_id'] = $this->model_balance_recharge->editRecharge($data['recharge_id'],$filter);
				$data['payment'] = $this->load->controller('balance/'.$paymentCode);
				$this->gateway_pay($data);
				break;
			case "kj_yijipay":
				$filter['payment_method'] = '在线支付';
				$this->session->data['recharge_id'] = $this->model_balance_recharge->editRecharge($data['recharge_id'],$filter);
				$data['payment'] = $this->load->controller('balance/'.$paymentCode);	
				$data['redirect'] = $data['payment'];
				break;
			default:
				break;

		}

		$json["data"] = $data;

		if($this->debugIt){
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		}
		else{
			$this->response->setOutput(json_encode($json));
		}

	}
	
	//作废充值单据
	public function destoryRecharge(){

		$this->load->model('balance/recharge');

		$json = array('success' => true);

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);
		
		$this->model_balance_recharge->destoryRecharge($data['rechargeid']);

		if($this->debugIt){
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		}
		else{
			$this->response->setOutput(json_encode($json));
		}

	}
	
	//支付宝跳转相关方法
	public function gateway_pay($data) {
		echo $data['payment'];
		die();
	}

}

?>