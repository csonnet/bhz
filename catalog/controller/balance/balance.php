<?php

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerBalanceBalance extends RestController {
	
	//获取充值单据列表
	public function getRecharge(){

		$this->checkPlugin();
		$this->load->model('balance/balance');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();

		$customer_money = $this->model_balance_balance->getCustomerBalance($customer_id);
		$results = $this->model_balance_balance->getRechargeOrder($customer_id);
		
		$json['customerBalance'] = $customer_money;
		$json['rechargeOrder'] = $results;
		
		$this->response->setOutput(json_encode($json));

	}

	//获取定金单据列表
	public function getDeposit(){

		$this->checkPlugin();
		$this->load->model('balance/balance');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();

		$results = $this->model_balance_balance->getDepositOrder($customer_id);

		$json['depositOrder'] = $results;
		
		$this->response->setOutput(json_encode($json));

	}

	//获取交易单据列表
	public function getTrade(){

		$this->checkPlugin();
		$this->load->model('balance/balance');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();

		$orders = $this->model_balance_balance->getCustomerOrder($customer_id);
		
		$json['tradeOrder'] = $orders;
		
		$this->response->setOutput(json_encode($json));

	}
	
	//获取用户余额
	public function getBalance(){

		$this->load->model('balance/balance');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();

		$customer_money = $this->model_balance_balance->getCustomerBalance($customer_id);

		$json['customerBalance'] = $customer_money;

		$this->response->setOutput(json_encode($json));

	}

}

?>