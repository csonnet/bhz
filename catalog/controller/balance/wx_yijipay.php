<?php
class ControllerBalanceWxyijiPay extends Controller {

    
    public  function index(){

        $this->load->helper('yijiSignHelper');
        $this->load->helper('dto/FastPayTrade.class');
        $this->load->helper('YijipayClient');
        $this->load->helper('mobile');

        if(isset($this->session->data['recharge_id'])){
            $recharge_id = $this->session->data['recharge_id'];
        }else{
            $recharge_id = 0;
        }
       
	    $this->load->model('balance/recharge');

		$recharge_info = $this->model_balance_recharge->getRecharge($recharge_id);
        $user_id = $this->model_balance_recharge->getUserInfo($recharge_info['customer_id']);
        
        $config = $this->getConfig();
        $user = $this->getUser($recharge_info,$user_id);
        
        $objReq = new FastPayTrade();
        
        //请求公共部分
        $objReq->setReturnUrl('http://m.bhz360.com/index.php?route=balance/wx_yijireturn');
        $objReq->setNotifyUrl('http://m.bhz360.com/index.php?route=balance/wx_yijinotify');
        
        $objReq->setPartnerId($config['partnerId']);

        $objReq->setOrderNo($this->getBefstr().$recharge_id);
        $objReq->setMerchOrderNo($this->getBefstr().$recharge_info['recharge_id']);
        $objReq->setSignType("MD5");
        //构建交易参数
        $item1 = $this->getTradeInfo($user,$recharge_info);
        
        $objReq->setBuyerOrgName($recharge_info['fullname']);
        $objReq->setBuyerRealName($recharge_info['fullname']);
        if(intval($user['buyer_uid_yiji']) != 0){
            $objReq->setBuyerUserId($user['buyer_uid_yiji']);
        }
        $objReq->setOutUserId($user['buyer_id']);
        $objReq->setTradeInfo([$item1]);
        
        //收银台参数
        $objReq->setPaymentType("QUICKPAY");
        $objReq->setUserTerminalType("PC");

        if(is_mobile()){
            $objReq->setOpenid($this->session->data['weixin_login_openid']);
            $objReq->setPaymentType("PAYMENT_TYPE_SUPER");
            $objReq->setUserTerminalType("MOBILE");
        }

        //构建请求
        $client = new YijiPayClient($config);
        
        $response = $client->execute($objReq);
        return $response; 
    }
    
    
    public function getTradeInfo($user,$recharge_info){
        //构建订单
        $this->load->helper('dto/TradeInfo.class');
        $item1 = new TradeInfo();
        $item1->setTradeName("百货栈");
        $item1->setSellerUserId($user['seller_uid_yiji']);
        $item1->setSellerOrgName($user['seller_name']);
        $item1->setMerchOrderNo($this->getBefstr().$recharge_info['recharge_id']);
        $item1->setTradeAmount($recharge_info['money']);
        $item1->setGoodsName("百货栈商城充值");
        $item1->setCurrency("CNY");   
        $item1->setMemo("充值");

        return $item1;
    }
    
    
    
    public function getConfig(){
        return array(
        'partnerId' => '20161201020011981315', //商户ID
        'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key
        
        //网关
        'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
        // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
    }
    
    public function getUser($recharge_info,$user_id){
        
        // dump($order_info);die();
        $user['seller_uid_yiji'] = "20161201020011981315";          //卖家易极付userId
        $user['seller_id'] = "3314";                //卖家商户平台上的会员Id
        $user['seller_name'] = "百货栈";                          //卖家商户平台上的会员名称
        
        //买家信息 buyer TODO: 修改为你自己的买家信息
        $user['buyer_uid_yiji'] = $user_id['yiji_userid'];           //买家易极付userId
        $user['buyer_id'] = $recharge_info['customer_id'];                 //买家商户平台上的会员Id
        $user['buyer_name'] = $user_id['fullname'];                            //买家商户平台上的会员名称
        return $user;
    }


    public function getBefstr(){
         return $order_befstr='BHZ2017BALANCE57'.substr(time(),'-4');
    }
}