<?php

class ControllerBalanceAlipayDirect extends Controller {

	public function index() {
		
		$this->load->helper('alipay_dt_core');
		$this->load->helper('alipay_dt_md');
		
		$this->language->load('payment/alipay_direct');

		$data['button_confirm'] = $this->language->get('button_confirm');
		
		$alipay_config['partner']	=	$this->config->get('alipay_direct_partner');
		$alipay_config['key']		=	$this->config->get('alipay_direct_security_code');
		$alipay_config['sign_type']    = strtoupper('MD5');
		$alipay_config['input_charset']= strtolower('utf-8');
		$alipay_config['cacert']    = getcwd().'\\cacert.pem';
		$alipay_config['transport']    = HTTPS_SERVER;
		
		$this->load->model('balance/recharge');

		$recharge_id = $this->session->data['recharge_id'];
		$recharge_info = $this->model_balance_recharge->getRecharge($recharge_id);

        $payment_type = "1";
        $notify_url = HTTPS_SERVER.'catalog/controller/balance/alipay_direct_callback.php';
        $return_url = $this->url->link('balance/success');
        $seller_email = $this->config->get('alipay_direct_seller_email');
        $out_trade_no = $recharge_id."-RC".time();
		$total_fee = number_format($recharge_info['money'],2,'.','');
        $subject = '百货栈商城充值';
        $body =  '百货栈商城充值';
        $show_url = $this->url->link('common/home', '', 'SSL');
        $anti_phishing_key = "";
        $exter_invoke_ip = "";

		$parameter = array(
				"service" => "create_direct_pay_by_user",
				"partner" => trim($alipay_config['partner']),
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"seller_email"	=> $seller_email,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject, //名称
				"total_fee"	=> $total_fee,
				"body"	=> $body, //描述
				"show_url"	=> $show_url,
				"anti_phishing_key"	=> $anti_phishing_key,
				"exter_invoke_ip"	=> $exter_invoke_ip,
				"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
		);
		
		$this->load->library('alipaydtsubmit');
		
		$this->alipaydtsubmit = new Alipaydtsubmit($alipay_config);
		
		$data['html_text'] = $this->alipaydtsubmit->buildRequestForm($parameter,"get", $this->language->get('button_confirm'));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/alipay_direct.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/alipay_direct.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/alipay_direct.tpl', $data);
		}
		
	}
	
	//回调
	public function callback(){

		$this->load->helper('alipay_dt_core');
		$this->load->helper('alipay_dt_md');

		$alipay_config['partner']	=	$this->config->get('alipay_direct_partner');
		$alipay_config['key']		=	$this->config->get('alipay_direct_security_code');	
		$alipay_config['sign_type']    = strtoupper('MD5');
		$alipay_config['input_charset']= strtolower('utf-8');
		$alipay_config['cacert']    = getcwd().'\\cacert.pem';
		$alipay_config['transport']    = HTTPS_SERVER;
		$log = $this->config->get('alipay_direct_log');

		$this->load->library('alipaydtnotify');
		
		$alipayNotify = new Alipaydtnotify($alipay_config);
		$verify_result = $alipayNotify->verifyNotify();

		if($verify_result) {

			$recharge = explode('-',$_POST['out_trade_no']);
			$recharge_id = $recharge[0];

			//添加余额
			$this->load->model('balance/recharge');
			$this->model_balance_recharge->addBalance($recharge_id);

			echo 'success';

		}
		else{

			echo 'fail';

		}

	}
	
}
