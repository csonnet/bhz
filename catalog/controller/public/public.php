<?php

class ControllerPublicPublic extends Controller {
	
	//获取全部仓库
	public function getWarehouses(){	

		$this->load->model('public/public');

		$json = array('success' => true);
		$json['warehouses'] = $this->model_public_public->getWarehouses();

		$this->response->setOutput(json_encode($json));

	}

	//获取全部物流中心
	public function getLogcenters(){

		$this->load->model('public/public');

		$json = array('success' => true);
		$json['logcenters'] = $this->model_public_public->getLogcenters();

		$this->response->setOutput(json_encode($json));

	}

}

?>