<?php
// https://m.bhz360.com/index2.php?route=ssl/customer
class ControllerSslCustomer extends Controller {
    public function getInfo() {
        $customerId = intval($this->request->get['customerId']);
        $this->load->model('ssl/customer');
        $info = $this->model_ssl_customer->getByPK($customerId);
        extract($this->model_ssl_customer->getCustomerImagesByPK($customerId));
        $ret = array(
            'fullname' => $info['fullname'],
            'companyName' => $info['company_name'],
            'shopName' => $info['shop_name'],
            'discountMessage' => $info['discount_message'],
            'shopDescribe' => $info['shop_describe'],
            'shelfNum' => $info['shelf_num'],
            'shopType' => $info['shop_type'],
            'recommendedCode' => $info['recommended_code'],
            'cardPic' => $cardPic,
            'picList' => $picList,
            'LogoPic' => $LogoPic,
            'Propaganda' => $Propaganda,
        );
        die(json_encode($ret));
    }

  //我的货架------------------------------
   public function getSolution (){
        $customerId = intval($this->request->get['customerId']);
        $name = $this->request->get['name'];
        $filter = array(
            'customer_id'=>$customerId,
            'name' => $name,
        );

         $this->load->model('ssl/solution');
         $this->load->model('ssl/shelf');
         $info=$this->model_ssl_solution->getall($filter);

         foreach ($info as $k=>$v) {
            $info[$k]['shelf_id'] = $this->model_ssl_shelf->getEnterShelfInSolution($v['solution_id']);
         }
         var_dump($info);die;
        die(json_encode($info));
    }

    //我的货架
    public function getShop(){
        // $requestjson = file_get_contents('php://input');
        // $data = json_decode($requestjson, true);
         $customerId = intval($this->request->get['customerId']);
         $name = $this->request->get['name'];
       // var_dump($name);
        $filter = array(
            'customer_id'=>$customerId,
            'name' => $name,
        );
         $this->load->model('ssl/solution');
         $this->load->model('ssl/shelf');
         $info=$this->model_ssl_solution->getallshop($filter);
        // var_dump($info);die;
         foreach ($info as $k=>$v) {
            $shelf=$this->model_ssl_shelf->shelfinshop($v['shop_id']);
            $info[$k]['shelf_id'] = $shelf['shelfs'];
            $info[$k]['num'] = $shelf['num'];
         }

        die(json_encode($info));
    }




    public function getShelf() {
        $customerId = intval($this->request->get['customerId']);
        $solutionId = intval($this->request->get['solutionId']);
        $filter = array(
            'customer_id'=>$customerId,
            'solution_id'=>$solutionId,
        );
        $this->load->model('ssl/shelf');
        $list = $this->model_ssl_shelf->getList($filter);
        die(json_encode($list));
    }

    public function imgUpload() {
        $customerId = intval($this->request->post['customerId']);
        $uploadId = intval($this->request->post['uploadId']);
        $type = intval($this->request->post['type']);
        $ret = array(
            'status' => false,
            'message' => '未知错误',
        );
        if ($customerId) {
            if (1 == $type) {
                $img = $this->request->files['picList'];
            }elseif (2 == $type){
                $img = $this->request->files['cardPic'];
            }elseif (3 == $type){
                $img = $this->request->files['LogoPic'];
            }elseif (4 == $type){
                $img = $this->request->files['Propaganda'];
            }
            $shop_id =M('customer')->where(array('customer_id'=>$customerId))->getField('shop_id');

            if ($img['error']) {
                $ret['message'] = '图片上传失败';
            }else{
                move_uploaded_file($img['tmp_name'], DIR_UPLOAD.$img['name']);
                $this->load->model('ssl/customer');
                if ($uploadId){
                    $uploadInfo = array(
                        'name' => $img['name'],
                        'filename' => $img['name'],
                    );
                    $this->model_ssl_customer->updateUploadByPK($uploadId, $uploadInfo);
                }else{
                    $code = sha1(uniqid(mt_rand(), true));

                      $customerImagesInfo = array(
                        'customer_id' => $customerId,
                        'code' => $code,
                        'shop_id' => $shop_id,
                        'date_added' => date('Y-m-d H:i:s'),
                        'type' => $type,
                    );
                    $customer_images_id = $this->model_ssl_customer->addCustomerImages($customerImagesInfo);
                    $uploadInfo = array(
                        'name' => $img['name'],
                        'filename' => $img['name'],
                        'code' => $code,
                        'date_added' => date('Y-m-d H:i:s'),
                        'customer_images_id'=>$customer_images_id,

                    );
                   $uploadId = M('upload')->add($uploadInfo);
                    // $uploadId = $this->model_ssl_customer->addUpload($info);

                }
                $ret['status'] = true;
                $ret['uploadId'] = $uploadId;
            }
        }
        die(json_encode($ret));
    }

    public function updateInfo() {
        $customerId = intval($this->request->get['customerId']);
        $info = array(
            'fullname' => trim($this->request->get['fullname']),
            'company_name' => trim($this->request->get['companyName']),
            'shop_type' => trim($this->request->get['shopType']),
            'shop_name' => trim($this->request->get['shopName']),
            'discount_message' => trim($this->request->get['discountMessage']),
            'shop_describe' => trim($this->request->get['shopDescribe']),
            'recommended_code' => trim($this->request->get['recommendedCode']),
        );
        $this->load->model('ssl/customer');
        $this->model_ssl_customer->modifyByPK($customerId, $info);
        die(json_encode(array('success'=>true)));

    }
}
