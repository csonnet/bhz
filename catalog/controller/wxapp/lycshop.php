<?php
require_once(DIR_APPLICATION.'controller/wxapp/deciphering/wxBizDataCrypt.php');

class ControllerWxappLycShop extends Controller {

	private $_APPID = 'wx8cdac7431d56c5f4'; //小程序appid
    private $_APPSECRET = 'c9bbeea4e83a0d8489f165573dd6772a'; //小程序密钥
	
	//获取附近商铺信息
	public function getshop(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['lat'] = $data['lat'];
        $filter['lng'] = $data['lng'];
		$filter['inshop_id'] = $data['inshop_id'];
		$filter['sort'] = $data['obj']['sort'];
		$filter['sortway'] = $data['obj']['sortway'];
		$filter['search'] = $data['obj']['search'];

		$this->load->model('wxapp/lycshop');
		$shopList =  $this->model_wxapp_lycshop->getShopInfo($filter);
		
		if($shopList != null){
			$json['result'] = 'success';		
		}
		else{
			$json['result'] = 'fail';
		}

		$json['shopList'] = $shopList;
		$this->response->setOutput(json_encode($json));

	}

	//筛选搜索商铺
	public function searchShop(){
		
		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['shop_name'] = $data['shop_name'];

		$this->load->model('wxapp/lycshop');
		$json['searchList'] = $this->model_wxapp_lycshop->getSearchShop($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//根据id获取商铺货架信息
	public function getShelfById(){

		$filter['shelf_id'] = trim($this->request->get['shelf_id']);

		$this->load->model('wxapp/lycshop');
		$this->load->model('tool/image');

		$result = $this->model_wxapp_lycshop->getShelfById($filter);
		
		foreach($result as &$v){
			
			/*商品缩略图*/
			if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
            } else {
                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
			/*商品缩略图*/

		}

		$json['shelfProducts'] = $result;
		$this->response->setOutput(json_encode($json));

	}
	
	//通过id获取商铺信息
	public function getshopById(){

		$filter['shop_id'] = trim($this->request->get['shop_id']);
		$filter['customer_in_shop_id'] = trim($this->request->get['customer_in_shop_id']);

		$this->load->model('wxapp/lycshop');
       	$json['shopinfo'] =  $this->model_wxapp_lycshop->getShopInfoById($filter); 

		$this->response->setOutput(json_encode($json));	

	}
	
	//获取店铺购物车
	public function getCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['shop_id'] = $data['shop_id'];
		$filter['customer_in_shop_id'] = $data['customer_in_shop_id'];

		$this->load->model('wxapp/lycshop');
		$carts = $this->model_wxapp_lycshop->getCart($filter);

		$this->load->model('tool/image');
		foreach($carts as &$v){
			
			/*货缩略图*/
			if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
            } else {
                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
			/*货缩略图*/

			if($v['saleprice'] <= 0){
				$v['saleprice'] = $v['originprice'];
			}

			$v['check'] = false;

		}

		$json['cartList'] = $carts;

		$this->response->setOutput(json_encode($json));

	}
	
	//店铺添加购物车
	public function addCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['shop_id'] = $data['shop_id'];
		$filter['customer_in_shop_id'] = $data['customer_in_shop_id'];
		$filter['shop_inventory_id'] = $data['shop_inventory_id'];
		$filter['quantity'] = $data['quantity'];

		$this->load->model('wxapp/lycshop');
		$json['result'] = $this->model_wxapp_lycshop->addCart($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//更新购物车
	public function updateCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$method = $data['pdata']['method'];

		$this->load->model('wxapp/lycshop');
		if($method == 'delete'){
			$cart_id = $data['pdata']['cart_id'];
			$this->model_wxapp_lycshop->deleteCart($cart_id);
		}
		else if($method == 'update'){
			$filter['customer_in_shop_id'] = $data['pdata']['customer_in_shop_id'];
			$filter['shop_inventory_id'] = $data['pdata']['shop_inventory_id'];
			$filter['quantity'] = $data['pdata']['quantity'];
			$this->model_wxapp_lycshop->updateCart($filter);
		}

	}
	
	//店铺立即购买
	public function toBuy(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['payment_customer_id'] = $data['payment_customer_id'];
		$filter['shop_id'] = $data['shop_id'];
		 $filter['paycode'] = $data['paycode'];
		 $filter['payname'] = $data['payname'];
		$filter['sendaddress'] = $data['sendaddress'];
		$filter['buyList'] = $data['buyList'];
		$filter['allprice'] = $data['allprice'];

		$this->load->model('wxapp/lycshop');
		$json['order_id'] = $this->model_wxapp_lycshop->toBuy($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//获取客户订单
	public function getshoporder(){

		$customer_id = trim($this->request->get['customer_id']);

		$this->load->model('wxapp/lycshop');
		$json['result'] = $this->model_wxapp_lycshop->getshoporder($customer_id);

		$this->response->setOutput(json_encode($json));

	}

	//获取商铺资金动向
	public function getShopMoney(){

		$customer_id = trim($this->request->get['customer_id']);

		$this->load->model('wxapp/lycshop');
		$json['result'] = $this->model_wxapp_lycshop->getShopMoney($customer_id);

		$this->response->setOutput(json_encode($json));

	}
	
	//获取手机号快速注册商城会员
	public function fastRegister(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$appid = $this->_APPID;
		$sessionKey = $this->getOpenSession($data['code'])->session_key;
		$encryptedData = $data['encryptedData'];
		$iv = $data['iv'];

		$pc = new WXBizDataCrypt($appid, $sessionKey);
		$errCode = $pc->decryptData($encryptedData, $iv, $phoneinfo);	
		
		if ($errCode == 0) {
			$json['phoneinfo'] = json_decode($phoneinfo,true);
		}
		$json['result'] = $errCode;

		$this->response->setOutput(json_encode($json));

	}

	//获取微信OpenSession
    protected function  getOpenSession($code) {
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$this->_APPID.'&secret='.$this->_APPSECRET.'&js_code='.$code.'&grant_type=authorization_code';
        $ret = json_decode(file_get_contents($url));
		return $ret;
    }

}

?>