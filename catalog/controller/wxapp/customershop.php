<?php

class ControllerWxappCustomerShop extends Controller {
	
	//初始获取货架所有订单
	// public function getshop(){
	// 	$lat = trim($this->request->get['lat']);
 //        $lng = trim($this->request->get['lng']);

	// }

	// //获取附近商铺信息
	// public function getshop(){
	// 	$lat = trim($this->request->get['lat']);
 //        $lng = trim($this->request->get['lng']);
	// 	$this->load->model('wxapp/customershop');
 //       $customerinfo =  $this->model_wxapp_customershop->getShopInfo();
	// 	// $where = 'order_sum!=0 and lat!=0';
	// 	// $lnglats = M('customer')->field('*')->where($where)-> select();
	// 	$i = 0;
	// 	foreach ($customerinfo as $key => $value) {
	// 		if ($value['lng']==0&&$value['lat']==0) {
	// 			continue;
	// 		}
	// 		$i++;

	// 		$dist = $this->getdistance($lng, $lat, $value['lng'], $value['lat']);
	// 		$dist = round(abs($dist/1000),2);

	// 		$dists[]=array(
	// 			'customer_id'=>$value['customer_id'],
	// 			'shop_id'=>$value['shop_id'],
	// 			'lat'=>$value['lat'],
	// 			'lng'=>$value['lng'],
	// 			'telephone'=>$value['telephone'],
	// 			'date_added'=>$value['date_added'],
	// 			'level'=>$value['level'],
	// 			'status'=>$value['status'],
	// 			'zone'=>$value['zone'],
	// 			'zone_id'=>$value['zone_id'],
	// 			'country'=>$value['country'],
	// 			'country_id'=>$value['country_id'],
	// 			'city'=>$value['city'],
	// 			'city_id'=>$value['city_id'],
	// 			'address'=>$value['address'],
	// 			'shelf_num'=>$value['shelf_num'],
	// 			'shop_describe'=>$value['shop_describe'],
	// 			'discount_message'=>$value['discount_message'],
	// 			'shop_name'=>$value['shop_name'],
	// 			'productnum'=>$value['productnum'],
	// 			'visitnum'=>$value['visitnum'],
	// 			'customer_ordernum'=>$value['customer_ordernum'],
	// 			'image'=>$value['image'],
	// 			'dist'=>$dist
	// 		);
	// 		if ($i==100) {
	// 			break;
	// 		}
	// 	}
	// 	$date = array_column($dists, 'dist');

 //        array_multisort($date,SORT_ASC ,$dists);

	// 	foreach($dists as &$v){
	// 		if($v['dist'] > 40){
	// 			$v['dist'] = ">40";
	// 		}
	// 	}

	// 	$json['shopList'] = $dists;
        
	// 	// return $infoarr;
	// 	$this->response->setOutput(json_encode($json));
		
	// }

	//获取附近商铺信息
	public function getshop(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['lat'] = $data['lat'];
        $filter['lng'] = $data['lng'];
		$filter['sort'] = $data['obj']['sort'];
		$filter['sortway'] = $data['obj']['sortway'];
		$filter['search'] = $data['obj']['search'];

		$this->load->model('wxapp/customershop');
		$shopList =  $this->model_wxapp_customershop->getShopInfo($filter);

		$json['shopList'] = $shopList;
		$this->response->setOutput(json_encode($json));

	}
	//商铺距离
	public function getdistance($lng1, $lat1, $lng2, $lat2) {
	    // 将角度转为狐度
	    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
	    $radLat2 = deg2rad($lat2);
	    $radLng1 = deg2rad($lng1);
	    $radLng2 = deg2rad($lng2);
	    $a = $radLat1 - $radLat2;
	    $b = $radLng1 - $radLng2;
	    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
	    return $s;
	}


	public function getshopById(){
		$shop_id = trim($this->request->get['shop_id']);
		$this->load->model('wxapp/customershop');
       	$customerinfo =  $this->model_wxapp_customershop->getShopInfoById($shop_id);
		foreach ($customerinfo as $key => $value) {
			$dists = array(
				'customer_id'=>$value['customer_id'],
				'lat'=>$value['lat'],
				'lng'=>$value['lng'],
				'telephone'=>$value['telephone'],
				'date_added'=>$value['date_added'],
				'level'=>(int)$value['level'],
				'status'=>$value['status'],
				'zone'=>$value['zone'],
				'zone_id'=>$value['zone_id'],
				'country'=>$value['country'],
				'country_id'=>$value['country_id'],
				'city'=>$value['city'],
				'city_id'=>$value['city_id'],
				'address'=>$value['address'],
				'shelf_num'=>$value['shelf_num'],
				'images'=>$value['images'],
				'shop_describe'=>$value['shop_describe'],
				'discount_message'=>$value['discount_message'],
				'shop_name'=>$value['shop_name'],
				'productnum'=>$value['productnum'],
				'visitnum'=>$value['visitnum'],
				'customer_ordernum'=>$value['customer_ordernum'],
			);
		}
		$this->response->setOutput(json_encode($dists));
		

	}

	public function getProducts(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_id = trim($this->request->get['shop_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_id) {
       		$this->load->model('wxapp/customershop');
            $this->load->model('tool/image');

			$data = $this->model_wxapp_customershop->getProduct($shop_id);
			foreach ($data as $key => $value) {
				if ($value['image'] != null && file_exists(DIR_IMAGE . $value['image'])) {
	                $image = $this->model_tool_image->resize($value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
	            $data[$key]['image'] = $image;

			}
			$json['result'] = $data;
		}else{
			$json['error'] ='参数错误';
		}
		
		$this->response->setOutput(json_encode($json));

	}

		//修改店铺定位
    public function location(){
    	$requestjson = file_get_contents('php://input');
			$data = json_decode($requestjson, true);
			$this->load->model('wxapp/customershop');
			$json = $this->model_wxapp_customershop->modlocation($data);
			$this->response->setOutput(json_encode($json));
    }
    //关注
    public function concern(){
		$shop_id = trim($this->request->get['shop_id']);
		$customerInShopId = trim($this->request->get['customerInShopId']);
		$wxAppOpenId = trim($this->request->get['wxAppOpenId']);
		$this->load->model('wxapp/customershop');
		if (empty($customerInShopId)) {
			$customerInShopId = M('customer_in_shop')->where(array('open_id'=>$wxAppOpenId))->getField('customer_in_shop_id');
		}
		$this->load->model('wxapp/customershop');
		$info = $this->model_wxapp_customershop->getinfo($customerInShopId,$shop_id);
		if (empty($info)) {
			$data = array(
				'shop_id'=>$shop_id,
				'customer_in_shop_id'=>$customerInShopId,
				'status'=>1,
				'date_added'=>date("Y-m-d H:i:s"),
			);
			$this->model_wxapp_customershop->concern($data);
			$json['status']=1;
		}else{
			if ($info['status']==1) {
				$this->model_wxapp_customershop->upstatus($customerInShopId,$shop_id,0);
				$json['status']=0;

			}elseif ($info['status']==0){
				$this->model_wxapp_customershop->upstatus($customerInShopId,$shop_id,1);
				$json['status']=1;

			}
		}
		$json['success'] = 'true';
		$this->response->setOutput(json_encode($json));
    }

    public function getShelf(){
		$shop_id = trim($this->request->get['shop_id']);
		$this->load->model('wxapp/customershop');
		if (empty($customerInShopId)) {
			$customerInShopId = M('customer_in_shop')->where(array('open_id'=>$wxAppOpenId))->getField('customer_in_shop_id');
		}
		$this->load->model('wxapp/customershop');
		$info = $this->model_wxapp_customershop->getinfo($customerInShopId,$shop_id);
		if (empty($info)) {
			$data = array(
				'shop_id'=>$shop_id,
				'customer_in_shop_id'=>$customerInShopId,
				'status'=>1,
				'date_added'=>date("Y-m-d H:i:s"),
			);
			$this->model_wxapp_customershop->concern($data);
		}else{
			if ($info['status']==1) {
				$this->model_wxapp_customershop->upstatus($customerInShopId,$shop_id,0);
			}elseif ($info['status']==0){
				$this->model_wxapp_customershop->upstatus($customerInShopId,$shop_id,1);

			}
		}
		$json['success'] = 'true';
		$this->response->setOutput(json_encode($json));
    }



}

?>
