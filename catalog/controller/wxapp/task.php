<?php
//业务员任务相关类
class ControllerWxappTask extends Controller {
    
    //获取业务员任务列表 getTaskList
    public function getTaskList(){

        // 加载model
        $this->load->model('wxapp/task');
        //获取当前用户的customerId
        $customer_id = trim($this->request->get['customerId']);
        if( !empty($customer_id) && isset($customer_id) ){
            $taskInfo = $this->model_wxapp_task->getTaskList($customer_id);
        } else {
            $taskInfo['error'] = '参数错误';
        }   

        echo json_encode($taskInfo);
        exit;
    }

    // 业务员更新任务状态和备注信息
    public function updateTaskStatus(){

        $task_id = trim($this->request->get['task_id']);
        $customer_id = trim($this->request->get['customerId']);
        $status = trim($this->request->get['status']);
        $remark = trim($this->request->get['remark']);

        $ret = array(
            'msg' => 'error',
            'status' => true,
            );

        if( !empty($customer_id) && isset($customer_id) ){
            $data = array(
                'operator_id' => $customer_id,
                'status'      => $status,
                'remark'      => $remark,
                'finished_date' => date('Y-m-d H:i:s'),
                );
            $filter = array(
                'task_id' => $task_id,
                );
            M('salesman_task')->where($filter)->save($data);
            $ret['msg'] = '保存成功';
        } else {
           $ret['status'] = false;
        }

        echo json_encode($ret);
        exit;

    }

    // 搜索业务员的接口
    public function searchSalesman(){

        $ret = array();
        $salesman_name = trim($this->request->get['salesman_name']);
        $this->load->model('wxapp/task');
        if(empty($salesman_name)){
            $ret['msg'] = '请输入业务员名称';
        }else{
            $ret = $this->model_wxapp_task->getSalesman($salesman_name);
            if(empty($ret)){
                $ret['msg'] = '没有数据'; 
            }
        }

        echo json_encode($ret);
        exit;
    }

    // 给业务员创建任务
    public function createTask(){

        $ret = array(
            'msg' => '',
            'status' => false,
        );
        $requestjson = file_get_contents('php://input');
        $postInfo = json_decode($requestjson, true);
        //打印调试
        // $ret['data'] = $postInfo;
        $task_title    = $postInfo['task_title'];   //任务标题
        $task_type     = $postInfo['task_type'];    //任务类型
        $task_date     = $postInfo['task_date'];    //任务有效期
        $task_describe = $postInfo['task_describe'];//任务描述
        $customer_id   = $postInfo['customer_id'];  //当前发任务人的id
        $now = date('Y-m-d H:i:s');

        foreach ($postInfo['chose_list'] as $k => $v) {

            $shop_id = $v['shop_id'];   
            $salesman_id = $v['customer_id'];

                if ( !empty($postInfo['customer_id']) && isset($postInfo['customer_id']) && !empty($salesman_id) && !empty($shop_id) && !empty($task_describe) ) {

                $taskData = array(
                   'shop_id'      => $shop_id,
                   'creator_id'   => $customer_id,
                   'operator_id'  => $salesman_id,
                   'title'        => $task_title,
                   'description'  => $task_describe,
                   'task_type'    => $task_type,
                   'status'       => '0',    //完成状态默认0 未完成
                   'remark'       => '',
                   'validtime'     => $task_date,
                   'date_added'   => $now,   //任务创建时间
                );

                $result = M('salesman_task')->add($taskData);
                if ($result) {
                    $ret['status'] = true;
                    $ret['msg']    = '发送成功';
                }else{
                    $ret['msg']    = '发送失败';
                }

           }else{

               if(empty($task_describe)){
                    $ret['msg'] = '任务描述不能为空';
                    die(json_encode($ret));
               }
               if(empty($customer_id)){
                    $ret['msg'] = '发送失败';
                    die(json_encode($ret));
               }
           }
           
        }

        echo json_encode($ret);
        exit;
    }

    //业务员操作也相关客户终止合作
    public function stopCooperation(){
        $ret = array(
            'status' => false,
            'msg'    => '',
        );
        //店铺id
        $shopId = trim($this->request->post['shop_id']);
        if(empty($shopId) || !isset($shopId) ){
            $ret['msg'] = '参数错误';
            echo json_encode($ret);
            exit;
        }
        $status = array(
          'status' => 4,   //4终止合作
        );
        $result = M('shop')->where('shop_id='.$shopId)->save($status);
        if(false !== $result){
            $ret['status'] = true;
            $ret['msg'] = '操作成功';
        }else{
            $ret['msg'] = '操作失败';
        }

        echo json_encode($ret);
        exit;
    }


}