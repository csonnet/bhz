<?php

class ControllerWxappCode extends Controller {
	 private $_APPID = 'wx8cdac7431d56c5f4';
   private $_APPSECRET = 'c9bbeea4e83a0d8489f165573dd6772a';
	//扫码跳转
	public function toPage(){
		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);
		$code = trim($data['code_id']);
		$code_id = trim($data['code']);
		$cusinfo = $data['info'];
		$wxAppOpenId = $this->_getOpenIdByCode($code_id);
		$this->load->model('wxapp/code');
		$info = $this->model_wxapp_code->separate($wxAppOpenId);
		$this->model_wxapp_code->customerinshop($wxAppOpenId,$cusinfo);
		// var_dump($info);die();
		if ($code>80000) {
			$shop_id = $this->model_wxapp_code->getShop($code);
			if (empty($shop_id)) {
				$json['url'] = "/pages/visitor/index";
				$json['index'] = 1;
			}else{
				$json['url'] = "/pages/visitor/shop/shop";
				$json['shop_id'] = $shop_id;
				$json['index'] = 2;

			}
			
		}else{
			$shelf_category = $this->model_wxapp_code->toKnowledge($code);
			foreach ($shelf_category as $key => $value) {
					$category = $value['shelf_category'];
					$shop_id = $value['shop_id'];
					$shelf_id = $value['shelf_id'];
					$solution_id = $value['solution_id'];
			}
			if (empty($shelf_id)) {
				$json['url'] = "/pages/visitor/index";
				$json['index'] = 1;

			}else{
				if($info){

				// $customerid=$info['customerId'];
					$json['url'] = "/pages/customer/salesman/sale?shelf_id=".$shelf_id;
					$json['index'] = 3;

				}else{
					//var_dump($cusinfo);die;
					// $inshop=$this->model_wxapp_code->customerinshop($wxAppOpenId,$cusinfo);
					// var_dump($inshop);die;
					$json['url'] = "/pages/visitor/shop/shelf/shelf";
					$json['shelf_id'] = $shelf_id;
					$json['index'] = 4;


					// $json['url'] = "/pages/shop_customer/categoryKnowledge?category=".$category;
				}
			}
			//查不到这位用户的openid就跳到知识页面
		
		}

		$json['wxAppOpenId'] = $wxAppOpenId;
		$this->response->setOutput(json_encode($json));
	}

	//获取用户openid
	 protected function _getOpenIdByCode($code) {
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$this->_APPID.'&secret='.$this->_APPSECRET.'&js_code='.$code.'&grant_type=authorization_code';
        $ret = json_decode(file_get_contents($url));
        return $ret->openid;
    	}

	//获取店铺名-------------------------
		public function getshopname(){
			$requestjson = file_get_contents('php://input');
			$data = json_decode($requestjson, true);
			$this->load->model('wxapp/code');
			$shopname = $this->model_wxapp_code->getshopname($data['userid']);
			$this->response->setOutput(json_encode($shopname));
		}
	//获取店铺名和店铺id
		public function getshopinfo(){
			$requestjson = file_get_contents('php://input');
			$data = json_decode($requestjson, true);
			$this->load->model('wxapp/code');
			$shopname = $this->model_wxapp_code->getshopnameandshopid($data['userid']);
			//var_dump($shopname);die;
			$this->response->setOutput(json_encode($shopname));
		}

	//业务员下商铺整合
	public function shopcombine(){
				$customer_id = trim($this->request->get['user_id']);
				$this->load->model('wxapp/code');
        $json = $this->model_wxapp_code->getshopnames($customer_id);
        $this->response->setOutput(json_encode($json));

	}
}
?>
