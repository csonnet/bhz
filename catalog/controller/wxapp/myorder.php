<?php

class ControllerWxappMyorder extends Controller {
	
	//初始获取货架所有订单
	public function initOrders(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['customer_id'] = $data['customer_id'];

		$this->load->model('wxapp/myorder');
		
		$json['orderList'][0] = $this->model_wxapp_myorder->getPayOrders($filter);
		$json['orderList'][1] = $this->model_wxapp_myorder->getDeliverOrders($filter);
		$json['orderList'][2] = $this->model_wxapp_myorder->getReceiveOrders($filter);
		$json['orderList'][3] = $this->model_wxapp_myorder->getReturnOrders($filter);
		$json['orderList'][4] = $this->model_wxapp_myorder->getFinishOrders($filter);

		$this->response->setOutput(json_encode($json));

	}

}

?>