<?php

class ControllerWxappOrderInfo extends Controller {

    
    public function getshoporder(){
            
            $customer_id = trim($this->request->get['customerId']);
            
            $this->load->model('wxapp/orderinfo');
            $result = $this->model_wxapp_orderinfo->getshoporder($customer_id);
            foreach ($result as $key => $value) {
                if ($value['status']==1) {
                    $status = '初始';
                }elseif ($value['status']==2) {
                    $status = '已接单';
                }elseif ($value['status']==3) {
                    $status = '完成';
                }
                $shopInfo = $this->model_wxapp_orderinfo->getshopById($value['shop_id']);
                $info[] = array(
                    'order_id'=>$value['order_id'],
                    'total'=>$value['total'],
                    'payment_fullname'=>$value['payment_fullname'],
                    'payment_telephone'=>$value['payment_telephone'],
                    'payment_method'=>$value['payment_method'],
                    'payment_address'=>$value['payment_country'].$value['payment_zone'].$value['payment_city'].$value['payment_address'],
                    'status'=>$status,
                    'status_id'=>$value['status'],
                    //在订单列表新增加店铺信息字段 by ypj at 2018.4.24
                    'shop_name'=>$shopInfo['shopinfo']['shop_name'],
                    'shop_address'=>$shopInfo['shopinfo']['address'],
                    'shop_telephone'=>$shopInfo['shopinfo']['telephone'], 
                    // end
                );
            }

            $json = $info;
            if (empty($json)) {
                $json['error'] ='你没有订单';
                
            }

        $this->response->setOutput(json_encode($json));

    }




}