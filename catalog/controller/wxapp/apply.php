<?php

class ControllerWxappApply extends Controller {

/**
     * 供应商资质证书上传
     * @return [type] [description]
     * @author ypj 
     * @date(2018.4.28)
     * 测试文件
     */
    public function imgUpload2(){
        $customerInShopId = intval($this->request->post['customerInShopId']);
        $img = $this->request->files['cardPic'];
        $ret = array(
            'status' => false,
            'message' => '未知错误',
        );

        // print_r($ret);
        if (!empty($customerInShopId) && isset($customerInShopId)) {

            if ($img['error']) {
                $ret['message'] = '图片上传失败';
            } else {
                move_uploaded_file($img['tmp_name'], DIR_UPLOAD.$img['name']);
                $vendorAplyMdl = M('vendor_apply');
                $uploadInfo = array(
                    'certificates_image' => $img['name'],
                    'customer_in_shop_id' => $customerInShopId,
                );
                $filter = array(
                    'customer_in_shop_id' => $customerInShopId,
                );
                $count = $vendorAplyMdl->where($filter)->count();
                if($count > 0) {
                    $result = M('vendor_apply')->where($filter)->save($uploadInfo);
                } else {
                    $result = M('vendor_apply')->add($uploadInfo);
                }
            }
                $ret = array(
                    'status' => true,
                    'message' => '图片上传成功',
                );
            }else{
                $ret['message'] = '异常登陆';
            }

            die(json_encode($ret));
        }


         /**
     * 供应商获取资质证书接口
     * @return [type] [description]
     * @author ypj <[email]>
     * @date 20184.28
     */
    public function getCertificate(){

        $info = array();
        $customerInShopId = intval($this->request->get['customerInShopId']);
        // print_r($customerInShopId);
        $res = array(
            'message'=> 'failed',
            'data'   => '',
        );
        $filter = array(
            'customer_in_shop_id' => $customerInShopId,
            );
        
        if(isset($customerInShopId)){
            $pic = M('vendor_apply')->where($filter)->getField('certificates_image');
            $res['data'] = 'http://m.bhz360.com/system/storage/upload/'.$pic;
            $res['message'] = 'success';
        }
        $info[] = $res;
        
        $this->response->setOutput(json_encode($info));

    }    

    /**
     * 业务员帮商家增加客户
     * @return [type] [description]
     */
    public function registerCustomer(){

        $ret = array(
            'status'     => false,
            'customerId' => 0,
            'msg'        => '未知错误',
        );
        $filter = array();
        $shopName =trim($this->request->get['shop_name']);  //店铺名称
        $contactName = trim($this->request->get['contact_name']);//联系人姓名
        $tel = intval($this->request->get['tel']);    //电话
        $lat = trim($this->request->get['lat']);      //维度
        $lng = trim($this->request->get['lng']);      //经度
        $imgName = trim($this->request->get['shop_pic']); // 图片名称

        $pass = 123456; //默认注册密码设置为123456

        $now = date('Y-m-d H:i:s');
        $this->load->model('ssl/customer');
        if($tel == '' || $tel == 0){
            $ret['msg'] = '请输入手机号码!';
            die(json_encode($ret) );
        }
        $ret['customerId'] = intval($this->model_ssl_customer->telCheck($tel));
        if($ret['customerId'] != 0){
            $ret['msg'] = '该手机号已存在';
            die(json_encode($ret) );
        }
        if(empty($shopName)){
            $ret['msg'] = '店铺名称为空！';
            die(json_encode($ret) );
        }
        if ($ret['customerId'] == 0) {
                // 对customer 表操作的数据
                $customerInfo = array(
                    'customer_group_id' => 1,
                    'fullname'          => $contactName,//联系人姓名
                    'email'             => $tel.'@bhz360.com',
                    'telephone'         => $tel,//联系电话
                    'fax'               => $tel,
                    'password'          => md5($pass),
                    'salt'              => $this->getRandomCode(9),
                    'date_added'        => $now,
                    'shop_name'         => $shopName,   //店铺名称
                    'address'           => '',//详细地址
                );
                //如果insert customer 数据成功 
                if( M('customer')->add($customerInfo) ){
                    //通过手机号码查询出来用户的customerId值
                    $filter['telephone'] = $tel;
                    $rst = M('customer')->where($filter)->find();
                    
                    $shopInfo = array(
                        'customer_id' => $rst['customer_id'], //用户id
                        'shop_name'   => $shopName,   //商铺名称
                        'lat'         => $lat,        //维度
                        'lng'         => $lng,        //经度
                        'status'      => 0,           //业务员帮助注册默认为1
                        'date_added'  => $now,        //创建时间
                        'telephone'   => $tel,
                    );
                    
                    $result = M('shop')->add($shopInfo);
                    if($result){
                        $shopId =M('shop')->where(array('customer_id'=>$rst['customer_id']))->getField('shop_id');
                        $uploadInfo = array(
                            'shop_id' => $shopId,
                        );
                        M('customer')->where('customer_id='.$rst['customer_id'])->save($uploadInfo);

                        // 存入图片地址
                        $this->addImage($shopId, $rst['customer_id'], $imgName);        

                        $ret['status'] = true;
                        $ret['msg'] = '注册成功';
                    }
                    
                }else{
                    $ret['msg'] = '注册失败';
                }
        }
        die(json_encode($ret));
    }


    // 随机数
    public function getRandomCode($len) {
        $list = array ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $ret = '';
        while (strlen($ret) < $len) {
            $ret .= $list[array_rand($list, 1)];
        }
        return $ret;
    }

    // 注册时业务员为客户上传图片把图片名称保存相应表中
    public function addImage($shopId, $customerId, $imgName) {
        // 测试
        // $imgName = 'http://tmp/wx8cdac7431d56c5f4.o6zAJs5ir4NjBMYpyaUMS7cyVW_8.GG1sIt6r377Ta8d1ba0b4c03dfdd4462ab655c45af49.jpg';
        //截取地址名称的字符串
        $imgName = substr($imgName, 7);
        // end
        $ret = array(
            'status' => false,
            'message' => '未知错误',
        );
        if (!empty($customerId) && !empty($shopId)) {
            // print_r(11);exit;
            move_uploaded_file($imgName, DIR_UPLOAD.$imgName);
            $this->load->model('ssl/customer');
            
                $code = sha1(uniqid(mt_rand(), true));
                
                $customerImagesInfo = array(
                    'customer_id' => $customerId,
                    'code' => $code,
                    'shop_id' => $shopId,
                    'date_added' => date('Y-m-d H:i:s'),
                    'type' => 1,   //上传的是店铺实景图默认给1
                );
                $customer_images_id = $this->model_ssl_customer->addCustomerImages($customerImagesInfo);
                $uploadInfo = array(
                    'name' => $imgName,
                    'filename' => $imgName,
                    'code' => $code,
                    'date_added' => date('Y-m-d H:i:s'),
                    'customer_images_id'=>$customer_images_id,
                );
               $uploadId = M('upload')->add($uploadInfo);

                return true;
          
        }else{
                return false;
        }
    }
    
}

