<?php

class ControllerWxappAnalysis extends Controller {
	
	//获取业务分析相关数据
	public function getAnalysis(){

		$customer_id = $this->request->get['customer_id'];

		$json['customer'] = $this->getBasicInfo($customer_id);
		$json['trend'] = $this->getTrend($customer_id);
		$json['near'] = $this->getNear($customer_id);
		$json['hot'] = $this->hotproducts($customer_id);

		$this->response->setOutput(json_encode($json));

	}
	
	//获取业务分析基本信息
	public function getBasicInfo($customer_id){

		$this->load->model('wxapp/analysis');
		
		//用户名
		$customerInfo = $this->model_wxapp_analysis->getCustomerInfo($customer_id);

		//商品数
		$productCount = $this->model_wxapp_analysis->getProductCount($customer_id);
		
		//货架数
		$shelfCount = $this->model_wxapp_analysis->getShelfCount($customer_id);

		//预期利润
		$profit = $this->model_wxapp_analysis->getProfit($customer_id);

		$customerData = array(
			"name" => $customerInfo[0]['fullname'],
			"productCount" => $productCount,
			"shelfCount" => $shelfCount,
			"profit" => $profit
		);

		return $customerData;

	}
	
	//获取业绩走势
	public function getTrend($customer_id){
        $start= date("Y-m", strtotime("-1 year"));
        $startarr = explode('-', $start);
        for ($i=(int)$startarr[1]; $i <=12 ; $i++) { 
        	if ($i>=10) {
   				$dateArr[] = $startarr[0].$i;
   			}else{
   				$dateArr[] = $startarr[0].'0'.$i;
   			}
        }
        $end  = date("Y-m");
        $endarr = explode('-', $end);
        for ($i=1; $i <=(int)$endarr[1] ; $i++) { 
        	if ($i>=10) {
   				$dateArr[] = $endarr[0].$i;
   			}else{
   				$dateArr[] = $endarr[0].'0'.$i;
   			}
        }

        $sql = "SELECT extract(YEAR_MONTH FROM o.date_added) AS 'yearMonth', sum(o.total) AS 'total', COUNT(o.order_id) AS 'num'FROM `order` AS o WHERE o.order_status_id <> 16 AND o.order_status_id <> 0 AND o.order_status_id <> 13 AND o.order_status_id <> 11 AND  customer_id = ".$customer_id." AND date_added>'".$start."-01 00:00:00' GROUP BY extract(YEAR_MONTH FROM o.date_added)";
        $query = $this->db->query($sql);
        $info = $query->rows;
        foreach ($info as $key => $value) {
        	$yearMonth = $value['yearMonth'];
        	$yearArr = str_split($value['yearMonth'],4);
        	$value['yearMonth'] = $yearArr[0].'-'.$yearArr[1];
           	$infoarr[$yearMonth] = $value;
        }
        foreach ($dateArr as $key => $value) {
        	if(!array_key_exists($value, $infoarr)){
        		$yearArr = str_split($value,4);
        		$value1 = $yearArr[0].'-'.$yearArr[1];
        		$infoarr[$value]['yearMonth'] = $value1;
        		$infoarr[$value]['total'] = 0;
        		$infoarr[$value]['num'] = 0;
        	}
        }
        $date = array_column($infoarr, 'yearMonth');

        array_multisort($date,SORT_ASC ,$infoarr);

        return $infoarr;
	}
	
	//获取附近商铺信息
	public function getNear($customer_id){

		$lnglat = M('address')->where(array('customer_id'=>$customer_id))->field('lng,lat')->find();
		if ($lnglat['lng']==0&&$lnglat['lat']==0) {
			return;
		}
		//$where = 'customer_id!='.$customer_id;
		$lnglats = M('address')->field('lng,lat,customer_id')->where($where)-> select();
		foreach ($lnglats as $key => $value) {
			if ($value['lng']==0&&$value['lat']==0) {
				continue;
			}
			$dist = $this->getdistance($lnglat['lng'], $lnglat['lat'], $value['lng'], $value['lat']);
			$dists[]=array(
				'customer_id'=>$value['customer_id'],
				'dist'=>abs($dist)
			);
		}
		$date = array_column($dists, 'dist');

        array_multisort($date,SORT_ASC ,$dists);
		
		$this->load->model('wxapp/analysis');
        $i=0;
        foreach ($dists as $key => $value) {	
		
        	$info = $this->model_wxapp_analysis->getRangeOrderTotal($value['customer_id']);
        	if ($info['total'] != null) {
        		$infoarr[$i]=$info;
        		$infoarr[$i]['dist']=$value['dist'];
        		$i++;
        	}
			else if($value['customer_id'] == $customer_id){
				$infoarr[$i]=$info;
				$infoarr[$i]['total']=0;
        		$infoarr[$i]['dist']=$value['dist'];
        		$i++;
			}
        	
        	if ($i==8) {
        		break;
        	}
        	
        }
        
		return $infoarr;

	}
	
	//商铺距离
	public function getdistance($lng1, $lat1, $lng2, $lat2) {
	    // 将角度转为狐度
	    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
	    $radLat2 = deg2rad($lat2);
	    $radLng1 = deg2rad($lng1);
	    $radLng2 = deg2rad($lng2);
	    $a = $radLat1 - $radLat2;
	    $b = $radLng1 - $radLng2;
	    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
	    return $s;
	}

	//热销商品统计
    public function hotproducts($customer_id){

        
		$this->load->model('public/public');

		$userid=$customer_id;
        $orderid=$this->model_public_public->getorderProductid($userid);
        $orderids=array_column($orderid, 'order_id');
        $string=implode(',', $orderids);
        $gropcode=$this->model_public_public->getgropcode($string);
        $product_codes=array_column($gropcode, 'order_product_id');
		$productcodes=implode(',', $product_codes);

		$exclude=$this->model_public_public->gethotproducts($string,$productcodes);
		foreach ($exclude as $key => $value) {
			foreach ($exclude as $k => $v) {
				if($value['name']==$v['name']&&$v['nub']==1){
					unset($exclude[$k]);
				}else{
					$end[$k]=$v;
				}
			}
		}
		
		return $end;

    }

}

?>