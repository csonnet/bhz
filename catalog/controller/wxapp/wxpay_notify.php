<?php

//* 139 服务器
$GBCONFIG = array(
    'HOST'  => 'localhost',
    'USER'  => 'root',
    'PASS'  => 'dbonline@#$bhz360',
    'PORT'  => '3306',
    'DB'    => 'bhz',
);
// */

$mysql = mysqli_connect($GBCONFIG['HOST'], $GBCONFIG['USER'], $GBCONFIG['PASS'], $GBCONFIG['DB']);
mysqli_query($mysql, 'SET NAMES utf8');

$postXml = $GLOBALS["HTTP_RAW_POST_DATA"]; //接收微信参数 
if (empty($postXml)) { 
  return false; 
}
 
$post_data = xmlToArray($postXml); //微信支付成功，返回回调地址url的数据：XML转数组Array
$postSign = $post_data['sign'];
unset($post_data['sign']);

if($post_data['return_code']=='SUCCESS'&&$postSign){

	$order_id = explode("-",$post_data['out_trade_no'])[1]; //百货栈订单号，分割字符取 “-” 后面的数字
	$transaction_id = $post_data['transaction_id']; //微信支付订单号
	$money = $post_data['total_fee']/100;  //金额返回的结果单位是分，需要除以100化为元

	isPayOrder($order_id); //修改订单状态为已付款
	changeShopMoney($order_id,$money); //修改商铺金额
	addSaleLog($order_id,$transaction_id,$money); //添加商铺买卖日志

	return_success(); //返回成功信息方法，避免微信重复回调

}
else{
	echo '微信支付失败';
}

//修改订单状态为已付款
function isPayOrder($order_id){
	global $mysql;
	$sql = "UPDATE `shop_order` SET `is_pay`=1 WHERE `order_id`=".$order_id;
	mysqli_query($mysql, $sql);
}

//修改商铺金额
function changeShopMoney($order_id,$money){
	global $mysql;
	$search = "SELECT `shop_id` FROM `shop_order` WHERE `order_id`=".$order_id;
	$info = mysqli_fetch_assoc(mysqli_query($mysql, $search));

	$sql = "UPDATE `shop` SET `money`=`money`+".$money." WHERE `shop_id`=".$info['shop_id']; 
	mysqli_query($mysql, $sql);
}

//添加商铺买卖日志
function addSaleLog($order_id,$transaction_id,$money){
	global $mysql;
	$search = "SELECT so.shop_id, so.payment_customer_id, s.shop_name, c.fullname FROM `shop_order` so 
	LEFT JOIN `shop` s ON so.shop_id = s.shop_id 
	LEFT JOIN `customer` c ON so.payment_customer_id = c.customer_id 
	WHERE so.order_id = ".$order_id;
	$info = mysqli_fetch_assoc(mysqli_query($mysql, $search));
	
	$shop_id = $info['shop_id'];
	$shop_name = $info['shop_name'];
	$customer_id = $info['payment_customer_id'];
	$customer_name = $info['fullname'];
	$case_id = 2; //shop_case表: 2=购买
	$method = "微信支付";
	$date_added = date('Y-m-d H:i:s');
	$sql = "INSERT INTO `shop_log` SET 
	`shop_id`=".$shop_id.",
	`shop_name`='".$shop_name."',
	`order_id`=".$order_id.",
	`transaction_id`=".$transaction_id.",
	`customer_id`=".$customer_id.",
	`customer_name`='".$customer_name."',
	`case_id`=".$case_id.",
	`method`='".$method."',
	`money`=".$money.",
	`date_added`='".$date_added."'";
	mysqli_query($mysql, $sql);
}

//将xml格式转换成数组 
function xmlToArray($xml) { 	
  libxml_disable_entity_loader(true); //禁止引用外部xml实体
  $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA); 
  $val = json_decode(json_encode($xmlstring), true);  
  return $val; 
}

//给微信发送确认订单金额和签名正确，SUCCESS信息
function return_success(){
	$return['return_code'] = 'SUCCESS';
	$return['return_msg'] = 'OK';
	$xml_post = '<xml>
		<return_code>'.$return['return_code'].'</return_code>
		<return_msg>'.$return['return_msg'].'</return_msg>
		</xml>';
	echo $xml_post;exit;
}

?>