<?php

class ControllerWxappShelfanalysis extends Controller {

  //获取业务分析相关数据
  public function shelfanalysis(){

    $shelf_id = $this->request->get['shelf_id'];
    $productinfo=$this->getBasicInfo($shelf_id);
    // var_dump($productinfo);die;
    $price=0;
    $num=0;
    $arr=$productinfo->rows;
    $this->load->model('wxapp/point');
    $productallprice = $this->model_wxapp_point->getProductallprice($shelf_id);
    foreach ($productallprice as $key => $value) {
        $price+=$value['price'];
        $num+=$value['num'];
    }

    foreach ($arr as $key => $value) {
        $arr['info']['shelfname']=$value['shelf_name'];
        $arr['info']['meta_title']=$value['meta_title'];
        $arr['info']['price']=$price;
        $arr['info']['num']=$num;
        $arr['info']['dan']=$productinfo->num_rows;
    }

    $this->response->setOutput(json_encode($arr));

  }

  //获取业务分析基本信息
  public function getBasicInfo($shelf_id){
    $this->load->model('wxapp/point');
    $productinfo = $this->model_wxapp_point->getProductinfo($shelf_id);
    return $productinfo;

  }



}

?>
