<?php
require_once(DIR_APPLICATION.'controller/wxapp/deciphering/wxBizDataCrypt.php');

class ControllerwxappLycshopnew extends Controller {

	private $_APPID = 'wx8cdac7431d56c5f4'; //小程序appid
    private $_APPSECRET = 'c9bbeea4e83a0d8489f165573dd6772a'; //小程序密钥
	
	//获取附近商铺信息
	public function getshop(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['lat'] = $data['lat'];
        $filter['lng'] = $data['lng'];
		$filter['inshop_id'] = $data['inshop_id'];
		$filter['sort'] = $data['obj']['sort'];
		$filter['sortway'] = $data['obj']['sortway'];
		$filter['search'] = $data['obj']['search'];

		$this->load->model('wxapp/lycshopnew');
		$shopList =  $this->model_wxapp_lycshopnew->getShopInfo($filter);
		
		if($shopList != null){
			$json['result'] = 'success';		
		}
		else{
			$json['result'] = 'fail';
		}

		$json['shopList'] = $shopList;
		$this->response->setOutput(json_encode($json));

	}

	//筛选搜索商铺
	public function searchShop(){
		
		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['shop_name'] = $data['shop_name'];

		$this->load->model('wxapp/lycshopnew');
		$json['searchList'] = $this->model_wxapp_lycshopnew->getSearchShop($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//根据id获取商铺货架信息
	public function getShelfById(){

		$filter['shelf_id'] = trim($this->request->get['shelf_id']);

		$this->load->model('wxapp/lycshopnew');
		$this->load->model('tool/image');

		$result = $this->model_wxapp_lycshopnew->getShelfById($filter);
		
		foreach($result as &$v){
			
			/*商品缩略图*/
			if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
            } else {
                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
			/*商品缩略图*/

		}

		$json['shelfProducts'] = $result;
		$this->response->setOutput(json_encode($json));

	}
	
	//通过id获取商铺信息
	public function getshopById(){

		$filter['shop_id'] = trim($this->request->get['shop_id']);
		$filter['customer_in_shop_id'] = trim($this->request->get['customer_in_shop_id']);

		$this->load->model('wxapp/lycshopnew');
       	$json['shopinfo'] =  $this->model_wxapp_lycshopnew->getShopInfoById($filter); 

		$this->response->setOutput(json_encode($json));	

	}

		//通过id获取商铺信息，全部商品
	public function getshopProductsById(){

		$filter['shop_id'] = trim($this->request->get['shop_id']);
		$filter['customer_in_shop_id'] = trim($this->request->get['customer_in_shop_id']);

		$this->load->model('wxapp/lycshopnew');
       	$json['shopinfo'] =  $this->model_wxapp_lycshopnew->getshopProductsById($filter); 

		$this->response->setOutput(json_encode($json));	

	}
	
	//获取店铺购物车
	public function getCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['shop_id'] = $data['shop_id'];
		$filter['customer_in_shop_id'] = $data['customer_in_shop_id'];

		$this->load->model('wxapp/lycshopnew');
		$carts = $this->model_wxapp_lycshopnew->getCart($filter);

		$this->load->model('tool/image');
		foreach($carts as &$v){
			
			/*货缩略图*/
			if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
            } else {
                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
			/*货缩略图*/

			if($v['saleprice'] <= 0){
				$v['saleprice'] = $v['originprice'];
			}

			$v['check'] = false;

		}

		$json['cartList'] = $carts;

		$this->response->setOutput(json_encode($json));

	}
	
	//店铺添加购物车
	public function addCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);
		$this->load->model('wxapp/lycshopnew');

		$filter['shop_id'] = $data['shop_id'];
		$filter['customer_in_shop_id'] = $data['customer_in_shop_id'];
		$filter['shop_product_id'] = $data['shop_product_id'];
		$filter['quantity'] = $data['quantity'];



		$json['result'] = $this->model_wxapp_lycshopnew->addCart($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//更新购物车
	public function updateCart(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$method = $data['pdata']['method'];

		$this->load->model('wxapp/lycshopnew');
		if($method == 'delete'){
			$cart_id = $data['pdata']['cart_id'];
			$this->model_wxapp_lycshopnew->deleteCart($cart_id);
		}
		else if($method == 'update'){
			$filter['customer_in_shop_id'] = $data['pdata']['customer_in_shop_id'];
			$filter['shop_inventory_id'] = $data['pdata']['shop_inventory_id'];
			$filter['quantity'] = $data['pdata']['quantity'];
			$this->model_wxapp_lycshopnew->updateCart($filter);
		}

	}
	
	//店铺立即购买
	public function toBuy(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['payment_customer_id'] = $data['payment_customer_id'];
		$filter['shop_id'] = $data['shop_id'];
		 $filter['paycode'] = $data['paycode'];
		 $filter['payname'] = $data['payname'];
		$filter['sendaddress'] = $data['sendaddress'];
		$filter['buyList'] = $data['buyList'];
		$filter['allprice'] = $data['allprice'];

		$this->load->model('wxapp/lycshopnew');
		$json['order_id'] = $this->model_wxapp_lycshopnew->toBuy($filter);

		$this->response->setOutput(json_encode($json));

	}
	
	//获取客户订单
	public function getshoporder(){

		$customer_id = trim($this->request->get['customer_id']);

		$this->load->model('wxapp/lycshopnew');
		$json['result'] = $this->model_wxapp_lycshopnew->getshoporder($customer_id);

		$this->response->setOutput(json_encode($json));

	}

	//获取商铺资金动向
	public function getShopMoney(){

		$customer_id = trim($this->request->get['customer_id']);

		$this->load->model('wxapp/lycshopnew');
		$json['result'] = $this->model_wxapp_lycshopnew->getShopMoney($customer_id);

		$this->response->setOutput(json_encode($json));

	}
	
	//获取手机号快速注册商城会员
	public function fastRegister(){

		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$appid = $this->_APPID;
		$sessionKey = $this->getOpenSession($data['code'])->session_key;
		$encryptedData = $data['encryptedData'];
		$iv = $data['iv'];

		$pc = new WXBizDataCrypt($appid, $sessionKey);
		$errCode = $pc->decryptData($encryptedData, $iv, $phoneinfo);	
		
		if ($errCode == 0) {
			$json['phoneinfo'] = json_decode($phoneinfo,true);
		}
		$json['result'] = $errCode;

		$this->response->setOutput(json_encode($json));

	}

	//获取微信OpenSession
    protected function  getOpenSession($code) {
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$this->_APPID.'&secret='.$this->_APPSECRET.'&js_code='.$code.'&grant_type=authorization_code';
        $ret = json_decode(file_get_contents($url));
		return $ret;
    }


    public function getProductById(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_product_id = trim($this->request->get['shop_product_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_product_id) {

			$this->load->model('wxapp/lycshopnew');

            $this->load->model('tool/image');

			$data = $this->model_wxapp_lycshopnew->getShopProductById($shop_product_id);
		
			foreach ($data as $key => $value) {
				$images = $this->model_wxapp_lycshopnew->getImages($shop_product_id);
				foreach ($images as $key2 => $value2) {
					if ($value2['image'] != null && file_exists(DIR_IMAGE . $value2['image'])) {
		                $image = $this->model_tool_image->resize($value2['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
		            } 
		            $imagesarr[] = $image;
				}
				if ($value['image'] != null && file_exists(DIR_IMAGE . $value['image'])) {
	                $image = $this->model_tool_image->resize($value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
		        $imagesarr[] = $image;

	            $description = $value['description'];
	            $arr = explode( 'src',$description);
	            foreach ($arr as $key1 => $value1) {
	            	$res =  $this ->_cut('http','jpg',$value1);
	            	if(strpos($res,'bhz360')===false){
	            		continue;
	            	}
	            	$descriptionarr[] = 'http'.$res.'jpg';
	            }
	            $result = array(
	            	'image'=>$image,
	            	'images'=>$imagesarr,
	            	'customer_id' => $value['customer_id'],
	            	'shop_product_id' => $value['shop_product_id'],
	            	'shop_id' => $value['shop_id'],
	            	'product_id' => $value['product_id'],
	            	'product_code' => $value['product_code'],
	            	'stock_qty' => $value['stock_qty'],
	            	'save_qty' => $value['save_qty'],
	            	'sale_qty' => $value['sale_qty'],
	            	'date_added' => $value['date_added'],
	            	'price' => $value['price'],
	            	'status' => $value['status'],
	            	'name' => $value['name'],
	            	'sale_price' => $value['sale_price'],
	            	'description' => $descriptionarr,
	            );
				// var_dump($value);

			}
			if (empty($result)) {
				$json['error'] ='没有商品';
				
			}else{
				$json['result'] = $result;

			}
		}else{
			$json['error'] ='参数错误';
		}
		// var_dump($result);
		$this->response->setOutput(json_encode($json));

	}

	public function _cut($begin,$end,$str){
        $b = mb_strpos($str,$begin) + mb_strlen($begin);
        $e = mb_strpos($str,$end) - $b;

        return mb_substr($str,$b,$e);
    }

    public function getProducts(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_id = trim($this->request->get['shop_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_id) {
       		$this->load->model('wxapp/lycshopnew');

            $this->load->model('tool/image');

		

			$data = $this->model_wxapp_lycshopnew->getProduct($shop_id);
			foreach ($data as $key => $value) {
				if ($value['image'] != null && file_exists(DIR_IMAGE . $value['image'])) {
	                $image = $this->model_tool_image->resize($value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
	            $data[$key]['image'] = $image;

			}
			$json['result'] = $data;
		}else{
			$json['error'] ='参数错误';
		}
		
		$this->response->setOutput(json_encode($json));

	}


	public function concern(){
		$shop_id = trim($this->request->get['shop_id']);
		$customerInShopId = trim($this->request->get['customerInShopId']);
		$wxAppOpenId = trim($this->request->get['wxAppOpenId']);
		// $this->load->model('wxapp/customershop');
		$this->load->model('wxapp/lycshopnew');
		if (empty($customerInShopId)) {
			$customerInShopId = M('customer_in_shop')->where(array('open_id'=>$wxAppOpenId))->getField('customer_in_shop_id');
		}
		$this->load->model('wxapp/customershop');
		$info = $this->model_wxapp_lycshopnew->getinfo($customerInShopId,$shop_id);
		if (empty($info)) {
			$data = array(
				'shop_id'=>$shop_id,
				'customer_in_shop_id'=>$customerInShopId,
				'status'=>1,
				'date_added'=>date("Y-m-d H:i:s"),
			);
			$this->model_wxapp_lycshopnew->concern($data);
			$json['status']=1;
		}else{
			if ($info['status']==1) {
				$this->model_wxapp_lycshopnew->upstatus($customerInShopId,$shop_id,0);
				$json['status']=0;

			}elseif ($info['status']==0){
				$this->model_wxapp_lycshopnew->upstatus($customerInShopId,$shop_id,1);
				$json['status']=1;

			}
		}
		$json['success'] = 'true';
		$this->response->setOutput(json_encode($json));
    }

      //获取配置商品信息
  public function detailconfig(){
      $requestjson = file_get_contents('php://input');
      $data = json_decode($requestjson, true);
	  $this->load->model('wxapp/lycshopnew');
      
        /*货架商品详细*/
        //var_dump($data['shelf_id']);die;
      $shelfProduct = array();
      $shelfProducts = $this->model_wxapp_lycshopnew->getShelfProducts($data['shelf_id']);

      foreach($shelfProducts as $key => $v2){
        $this->load->model('tool/image');

         if ($v2['image'] != null && file_exists(DIR_IMAGE . $v2['image'])) {
            $image = $this->model_tool_image->resize($v2['image'], 100, 100);
        } else {
            $image = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }
      $shelfProduct[$key]['shelf_quantity'] = $v2['show_qty'];
      $shelfProduct[$key]['shelf_id'] = $v['shelf_id'];
      $shelfProduct[$key]['shelf_x'] = $v2['layer_position'];
      $shelfProduct[$key]['shelf_y'] = $v2['shelf_layer'];
      $shelfProduct[$key]['min_order_qty'] = $v2['min_order_qty'];
      $shelfProduct[$key]['shelf_product_id'] = $v2['shelf_product_id'];
      $shelfProduct[$key]['stock_qty_temp'] = $v2['stock_qty_temp'];
      $shelfProduct[$key]['shop_product_id'] = $v2['shop_product_id'];
      $shelfProduct[$key]['stock_qty'] = $v2['stock_qty'];
      $shelfProduct[$key]['name'] = $v2['name'];
      $shelfProduct[$key]['image'] = $image;
      $shelfProduct[$key]['shop_basic_code'] = $v2['shop_basic_code'];
      $shelfProduct[$key]['status'] = $v2['status'];
      $shelfProduct[$key]['product_class1'] = $v2['product_class1'];
      $shelfProduct[$key]['product_class2'] = $v2['product_class2'];
      $shelfProduct[$key]['product_class3'] = $v2['product_class3'];
      $shelfProduct[$key]['sku'] = $v2['sku'];
      $shelfProduct[$key]['product_id'] = $v2['product_id'];
      $shelfProduct[$key]['shop_id'] = $v2['shop_id'];
      $shelfProduct[$key]['sale_price'] = $v2['sale_price'];
      $shelfProduct[$key]['price'] = $v2['price'];

      }
      /*货架商品详细*/

      $shelf_name = $v['solution_name'].'_'.$v['shelf_name'];
      $json['shelf'][] = array(
            'shelf_id' => $data['shelf_id'],
            'shelfProduct' => $shelfProduct, //去空数组重编index
      );


    $this->response->setOutput(json_encode($json));



  }
   public function getProductsCate(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_id = trim($this->request->get['shop_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_id) {
       		$this->load->model('wxapp/lycshopnew');
            $this->load->model('tool/image');

			$data = $this->model_wxapp_lycshopnew->getProductCategory($shop_id);
			foreach ($data as $key => $value) {
	            if ($value['image3'] != null && file_exists(DIR_IMAGE . $value['image3'])) {
	                $image = $this->model_tool_image->resize($value['image3'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
	            // $data[$key]['image3'] = $image3;
	            $product_class2 = $value['product_class2'];
	            $product_class3 = $value['product_class3'];
	            if (!empty($product_class2)) {
	             	 $result[$product_class2]['name'] =$value['product_class2name'];
	             	 // $result[$product_class2]['image'] =$value['image2']; 
	             	 $result[$product_class2]['id'] =$value['product_class2']; 
	             	 $arr = array(
	            	'id'=>$value['product_class3'],
	            	'name'=>$value['product_class3name'],
	            	'image'=>$image,
	            	);
		            if (!in_array( $arr,$result[$product_class2]['nodes'])) {

		            	$result[$product_class2]['nodes'][] = $arr;
		            }
	            } 
	       
			}
		     foreach ($result as $key => $value) {
            	$newresult[] = $value;
            }

			if (empty($newresult)) {
				$json['error'] ='没有商品';
				
			}else{
				$json['result'] = $newresult;

			}
		}else{
			$json['error'] ='参数错误';
		}
		// var_dump($result);
		$this->response->setOutput(json_encode($json));

	}


		//获取分类商品
	public function getCategoryProducts(){

		$filter['shop_id'] = trim($this->request->get['shop_id']);
        $filter['category'] = trim($this->request->get['category']);

		$this->load->model('wxapp/lycshopnew');
		$result = $this->model_wxapp_lycshopnew->getCategoryProducts($filter);
		
		$this->load->model('tool/image');
		foreach($result as &$v){

			/*货缩略图*/
			if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
            } else {
                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
			/*货缩略图*/
			
		}

		$json['products'] = $result;

		$this->response->setOutput(json_encode($json));

	}

    


}

?>