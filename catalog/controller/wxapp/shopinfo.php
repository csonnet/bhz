<?php

class ControllerWxappShopinfo extends Controller {
	

		//获取商铺信息
	public function getshopById(){
		$this->load->model('wxapp/shopinfo');
		$customer_id = trim($this->request->get['customerId']);
		// var_dump($customer_id);
		$shop_id =M('customer')->where(array('customer_id'=>$customer_id))->getField('shop_id');

		// var_dump($shop_id);
		if ($shop_id) {
       		$shopinfo =  $this->model_wxapp_shopinfo->getshopById($shop_id);
       		$json =$shopinfo;
		}else{
			$json['error'] ='你没有店铺';
		}
       
		$this->response->setOutput(json_encode($json));
		

	}


	public function updateInfo(){
		$this->load->model('wxapp/shopinfo');


		$customer_id = trim($this->request->get['customerId']);
		$shop_id =M('customer')->where(array('customer_id'=>$customer_id))->getField('shop_id');

		// var_dump($shop_id);
		if ($shop_id) {
       		$shop_name = trim($this->request->get['shop_name']);
			$address = trim($this->request->get['address']);
			$city = trim($this->request->get['city']);
			$country = trim($this->request->get['country']);
			$zone = trim($this->request->get['zone']);
			$telephone = trim($this->request->get['telephone']);
			$discount_message = trim($this->request->get['discount_message']);
			$shop_describe = trim($this->request->get['shop_describe']);
			$lng = trim($this->request->get['lng']);
			$lat = trim($this->request->get['lat']);
			$city_id = M('city')->where("name like '%city%' ")->getField('city_id');
			$zone_id = M('zone')->where("name like '%zone%' ")->getField('zone_id');
			$country_id = M('country')->where("name like '%country%' ")->getField('country_id');
			$data = array(
				'shop_name'=>$shop_name,
				'address'=>$address,
				'city'=>$city,
				'country'=>$country,
				'zone'=>$zone,
				'telephone'=>$telephone,
				'discount_message'=>$discount_message,
				'shop_describe'=>$shop_describe,
				'lng'=>$lng,
				'lat'=>$lat,
				'city_id'=>$city_id,
				'zone_id'=>$zone_id,
				'country_id'=>$country_id,
			);
			M('shop')->where(array('shop_id'=>$shop_id ))->save($data);
			// echo M('shop')->getlastsql();
			$json['success'] ='修改成功';

		}else{
			$json['error'] ='参数错误';
		}

		$this->response->setOutput(json_encode($json));
		
	}

	/**
	 * 供应商申请提交资料页面接口
	 * @return [type] [description]
	 * @author ypj
	 * @date 2018.4.27
	 */
	public function vendorsApply(){

		$vendorInfo = array();
		$json = array(
			'msg'    => '未知错误',
			'status' => false,
		);
		$customer_in_shop_id = trim($this->request->get['customerInShopId']);
		$area_name = trim($this->request->get['area_name']);
		$apply_name = trim($this->request->get['apply_name']);
		$apply_tel 	= trim($this->request->get['apply_tel']);
		if(!empty($customer_in_shop_id) && isset($customer_in_shop_id) && !defined($area_name) && !empty($apply_name) && !empty($apply_tel) ){
			$category_name 		= trim($this->request->get['category_name']);
			$cooperation_time 	= trim($this->request->get['cooperation_time']);
			$platform_model 	= trim($this->request->get['platform_model']);
			$delivery_model 	= trim($this->request->get['delivery_model']);
			$aftersales_model 	= trim($this->request->get['aftersales_model']);
			$account_model 		= trim($this->request->get['account_model']);
			$vendor_name 		= trim($this->request->get['vendor_name']);
			$vendor_type 		= trim($this->request->get['vendor_type']);
			$apply_tel 			= trim($this->request->get['apply_tel']);
			$vendor_brief 		= trim($this->request->get['vendor_brief']);

			$vendorInfo = array(
				'customer_in_shop_id' 	=> $customer_in_shop_id,
				'area_name' 			=> $area_name,
				'category_name'			=> $category_name,
				'cooperation_time'		=> $cooperation_time,
				'platform_model'		=> $platform_model,
				'delivery_model'		=> $delivery_model,
				'aftersales_model'		=> $aftersales_model,
				'account_model'			=> $account_model,
				'vendor_name'			=> $vendor_name,
				'vendor_type'			=> $vendor_type,
				'apply_name'			=> $apply_name,
				'apply_mobile'			=> $apply_tel,
				'brief'					=> $vendor_brief,
				'date_added'			=> date('Y-m-d H:i:s'),
			);

			$vendorAplyMdl = M('vendor_apply');
			$filter = array(
				'customer_in_shop_id' => $customer_in_shop_id,
				);
			$count = $vendorAplyMdl->where($filter)->count();

			if($count > 0){
				$rst = M('vendor_apply')->where($filter)->save($vendorInfo);
			} else {
				$rst = M('vendor_apply')->add($vendorInfo);
			}

			if($rst){
				$json['msg'] ='提交成功';
				$json['status'] = true;
			} else {
				$json['msg'] = '提交失败';
			}

		} else {

			if(empty($customer_in_shop_id)){
				$json['msg'] = '异常错误';
			}
			if(empty($apply_tel)){
				$json['msg'] = '请填写联系方式';
			}
			if(empty($apply_name)){
				$json['msg'] = '申请人姓名不能为空';
			}
			if(empty($area_name)){
				$json['msg'] = '合作区域不能为空';
			}
		}

		echo json_encode($json);
		exit;
	}

	/**
     * 供应商资质证书上传
     * @return [type] [description]
     * @author ypj 
     * @date(2018.4.28)
     */
    public function imgUpload2(){

    	$customerInShopId = intval($this->request->post['customerInShopId']);

        $img = $this->request->files['cardPic'];
        $ret = array(
            'status' => false,
            'message' => '未知错误',
        );
        // echo json_encode($ret);exit;
        // print_r($customerInShopId);
        if (!empty($customerInShopId) && isset($customerInShopId)) {

            if ($img['error']) {
                $ret['message'] = '图片上传失败';
            } else {
                move_uploaded_file($img['tmp_name'], DIR_UPLOAD.$img['name']);
                $vendorAplyMdl = M('vendor_apply');
				$uploadInfo = array(
                    'certificates_image' => $img['name'],
                    'customer_in_shop_id' => $customerInShopId,
                );
                $filter = array(
					'customer_in_shop_id' => $customerInShopId,
				);
				$count = $vendorAplyMdl->where($filter)->count();
				if($count > 0) {
					$result = M('vendor_apply')->where($filter)->save($uploadInfo);
				} else {
					$result = M('vendor_apply')->add($uploadInfo);
				}
            }
                $ret = array(
                	'status' => true,
                	'message' => '图片上传成功',
                );
            }else{
             	$ret['message'] = '异常登陆';
            }

            die(json_encode($ret));
        }


    /**
     * 供应商获取资质证书接口
     * @return [type] [description]
     * @author ypj <[email]>
     * @date 20184.28
     */
    public function getCertificate(){

        $info = array();
        $customerInShopId = intval($this->request->get['customerInShopId']);
        // print_r($customerInShopId);
        $res = array(
            'message'=> 'failed',
            'data'   => '',
        );
        $filter = array(
            'customer_in_shop_id' => $customerInShopId,
            );
        
        if(isset($customerInShopId)){
            $pic = M('vendor_apply')->where($filter)->getField('certificates_image');
            $res['data'] = 'http://m.bhz360.com/system/storage/upload/'.$pic;
            $res['message'] = 'success';
        }
        $info[] = $res;
        
        $this->response->setOutput(json_encode($info));

    }
       
   



	public function getProductsCate(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_id = trim($this->request->get['shop_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_id) {
       		$this->load->model('wxapp/shopinfo');
            $this->load->model('tool/image');

			$data = $this->model_wxapp_shopinfo->getProduct($shop_id);
			foreach ($data as $key => $value) {
	            if ($value['image3'] != null && file_exists(DIR_IMAGE . $value['image3'])) {
	                $image = $this->model_tool_image->resize($value['image3'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
	            // $data[$key]['image3'] = $image3;
	            $product_class2 = $value['product_class2'];
	            $product_class3 = $value['product_class3'];
	            if (!empty($product_class2)) {
	             	 $result[$product_class2]['name'] =$value['product_class2name'];
	             	 // $result[$product_class2]['image'] =$value['image2']; 
	             	 $result[$product_class2]['id'] =$value['product_class2']; 
	             	 $arr = array(
	            	'id'=>$value['product_class3'],
	            	'name'=>$value['product_class3name'],
	            	'image'=>$image,
	            	);
		            if (!in_array( $arr,$result[$product_class2]['nodes'])) {

		            	$result[$product_class2]['nodes'][] = $arr;
		            }
	            } 
	       
			}
		     foreach ($result as $key => $value) {
            	$newresult[] = $value;
            }

			if (empty($newresult)) {
				$json['error'] ='没有商品';
				
			}else{
				$json['result'] = $newresult;

			}
		}else{
			$json['error'] ='参数错误';
		}
		// var_dump($result);
		$this->response->setOutput(json_encode($json));

	}

	public function getProductById(){

		// $requestjson = file_get_contents('php://input');
		// $data = json_decode($requestjson, true);
		// var_dump($data);
		$shop_inventory_id = trim($this->request->get['shop_inventory_id']);
		
		// $shop_id = $data['shop_id'];
		// var_dump($shop_id);
		if ($shop_inventory_id) {
       		$this->load->model('wxapp/shopinfo');
            $this->load->model('tool/image');

			$data = $this->model_wxapp_shopinfo->getShopProductById($shop_inventory_id);
		
			foreach ($data as $key => $value) {
				$images = $this->model_wxapp_shopinfo->getImages($value['product_id']);
				foreach ($images as $key2 => $value2) {
					if ($value2['image'] != null && file_exists(DIR_IMAGE . $value2['image'])) {
		                $image = $this->model_tool_image->resize($value2['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
		            } 
		            $imagesarr[] = $image;
				}
				if ($value['image'] != null && file_exists(DIR_IMAGE . $value['image'])) {
	                $image = $this->model_tool_image->resize($value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
		        $imagesarr[] = $image;

	            $description = $value['description'];
	            $arr = explode( 'src',$description);
	            foreach ($arr as $key1 => $value1) {
	            	$res =  $this ->_cut('http','jpg',$value1);
	            	if(strpos($res,'bhz360')===false){
	            		continue;
	            	}
	            	$descriptionarr[] = 'http'.$res.'jpg';
	            }
	            $result = array(
	            	'image'=>$image,
	            	'images'=>$imagesarr,
	            	'customer_id' => $value['customer_id'],
	            	'shop_inventory_id' => $value['shop_inventory_id'],
	            	'shop_id' => $value['shop_id'],
	            	'product_id' => $value['product_id'],
	            	'product_code' => $value['product_code'],
	            	'stock_qty' => $value['stock_qty'],
	            	'save_qty' => $value['save_qty'],
	            	'sale_qty' => $value['sale_qty'],
	            	'date_added' => $value['date_added'],
	            	'price' => $value['price'],
	            	'status' => $value['status'],
	            	'name' => $value['name'],
	            	'sale_price' => $value['sale_price'],
	            	'description' => $descriptionarr,
	            );
				// var_dump($value);

			}
			if (empty($result)) {
				$json['error'] ='没有商品';
				
			}else{
				$json['result'] = $result;

			}
		}else{
			$json['error'] ='参数错误';
		}
		// var_dump($result);
		$this->response->setOutput(json_encode($json));

	}
	
  	public function _cut($begin,$end,$str){
            $b = mb_strpos($str,$begin) + mb_strlen($begin);
            $e = mb_strpos($str,$end) - $b;

            return mb_substr($str,$b,$e);
    }

    public function AddActivity(){
    			// var_dump($data);
		$wxAppOpenId = trim($this->request->get['wxAppOpenId']);
		$customerInShopId = trim($this->request->get['customerInShopId']);
		$shop_id = trim($this->request->get['shop_id']);
		$lng = trim($this->request->get['lng']);
		$lat = trim($this->request->get['lat']);
		$key = trim($this->request->get['key']);
		if (empty($customerInShopId)) {
			$customerInShopId = M('customer_in_shop')->where(array('open_id'=>$wxAppOpenId))->getField('customer_in_shop_id');
		}
		$this->load->model('wxapp/shopinfo');
		$data = array(
			'customer_in_shop_id'=>$customerInShopId,
			'shop_id'=>$shop_id,
			'lng'=>$lng,
			'lat'=>$lat,
			'key'=>$key,
			'date_added'=>date('Y-m-d H:i:s'),
		);
		$this->model_wxapp_shopinfo->addActivity($data);
		$this->response->setOutput(json_encode(array('success'=>true)));


    }

 //    public function getShopCustomer(){
	// 	$customerId = trim($this->request->get['customerId']);
	// 	$shop_id = M('customer')->where(array('customer_id'=>$customerId))->getField('shop_id');
	// 	$this->load->model('wxapp/shopinfo');
	// 	$info = $this->model_wxapp_shopinfo->getShopCustomer($shop_id);
	// 	// var_dump($info);
	// 	$CusInfo['visit']=[];
	// 	$CusInfo['buy'] = [];
	// 	$CusInfo['all'] = [];
	// 	foreach ($info as $key => $value) {
	// 		$customer_in_shop_id = $value['customer_in_shop_id'];
	// 		$value['visitnum'] = 0;
	// 		$value['buynum'] = 0;
	// 		switch ($value['key']) {
	// 			case 'visit':
	// 				$CusInfo['visit'][]=$value;
	// 				$value['visitnum'] = $value['num'];
	// 				break;
	// 			case 'buy':
	// 				$CusInfo['buy'][]=$value;
	// 				$value['buynum'] = $value['num'];

	// 				break;
	// 			default:
	// 				break;
	// 		}
	// 		if (array_key_exists($customer_in_shop_id,$CusInfo['all'])) {
	// 			if ($value['key']=='visit') {
	// 				$CusInfo['all'][$customer_in_shop_id]['visitnum'] = $value['num'];
					
	// 			}elseif ($value['key']=='buy') {
	// 				$CusInfo['all'][$customer_in_shop_id]['buynum'] = $value['num'];
					
	// 			}
	// 		}else{
	// 			$CusInfo['all'][$customer_in_shop_id] = $value;

	// 		}
			
	// 	}
	// 	$this->response->setOutput(json_encode($CusInfo));

	// }
	
	public function getShopCustomer(){
		$customerId = trim($this->request->get['customerId']);
		$shop_id = M('customer')->where(array('customer_id'=>$customerId))->getField('shop_id');
		if (empty($shop_id)) {
			$json['error'] ='你没有店铺';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$json['info'] = $this->model_wxapp_shopinfo->getShopCustomer($shop_id);
			if (empty($json['info'])) {
				$json['error'] ='你没有关注用户';
				
			}
		}

		$this->response->setOutput(json_encode($json));

	}
	public function getCategoryProducts(){

		$customer_id = trim($this->request->get['customer_id']);
		$shop_id = M('customer')->where(array('customer_id'=>$customer_id))->getField('shop_id');

		if (empty($shop_id)) {
			$json['error'] ='你没有店铺';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->getCategoryProducts($shop_id);
			
			$this->load->model('tool/image');
			foreach($result as &$v){

				/*货缩略图*/
				if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
	                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
	            } else {
	                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
	            }
				/*货缩略图*/
				
			}

			$json['products'] = $result;
			if (empty($json['products'])) {
				$json['error'] ='你没有商品';
				
			}
		}



		$this->response->setOutput(json_encode($json));

	}
	// 下架
	public function unStatus(){
			$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$shop_inventory_id = $data['shop_inventory_id'];

		if (empty($shop_inventory_id)) {
			$json['error'] ='参数错误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->unStatus($shop_inventory_id);

			$json['success'] = 'success';

		}



		$this->response->setOutput(json_encode($json));

	}

	// 下架
	public function upStatus(){
		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$shop_inventory_id = $data['shop_inventory_id'];

		if (empty($shop_inventory_id)) {
			$json['error'] ='参数错误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->upStatus($shop_inventory_id);

			$json['success'] = 'success';

		}



		$this->response->setOutput(json_encode($json));

	}

	public function updateproduct(){

		$shop_inventory_id = trim($this->request->get['shop_inventory_id']);
		// var_dump($shop_inventory_id);

		if (empty($shop_inventory_id)) {
			$json['error'] ='参数错误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->updateproduct($shop_inventory_id);
			
			$this->load->model('tool/image');
			foreach($result as &$v){

				/*货缩略图*/
				if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
	                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
	            } else {
	                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
	            }
				/*货缩略图*/
				
			}

			$json['product'] = $result[0];
			if (empty($json['product'])) {
				$json['error'] ='参数错误';
				
			}
		}

		// var_dump($json);

		$this->response->setOutput(json_encode($json));

	}

	public function save(){

		$shop_inventory_id = trim($this->request->get['shop_inventory_id']);
		$price = trim($this->request->get['price']);
		$sale_price = trim($this->request->get['sale_price']);
		$sale_qty = trim($this->request->get['sale_qty']);

		if (empty($shop_inventory_id)) {
			$json['info'] ='参数错误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$data = array(
				'price'=>$price,
				'sale_price'=>$sale_price,
				'sale_qty'=>$sale_qty,
			);
			$result = $this->model_wxapp_shopinfo->save($shop_inventory_id,$data);
			

			$json['info'] ='修改成功';

		}


		$this->response->setOutput(json_encode($json));

	}
// {orders: [{order_id: "1", payment_customer_id: "5957", shop_id: "3", payment_fullname: "百货栈测试",…}]} orders : [{order_id: "1", payment_customer_id: "5957", shop_id: "3", payment_fullname: "百货栈测试",…}] 0 : {order_id: "1", payment_customer_id: "5957", shop_id: "3", payment_fullname: "百货栈测试",…} date_added : "2018-04-18 16:48:17"date_modified : "2018-04-18 16:48:17"is_pay : "0"order_id : "1"payment_address : "aaa"payment_zonepayment_zone : "杨浦区"payment_city_id : "45"payment_code : "1"payment_country : "上海市"payment_country_id : "9"payment_customer_id : "5957"payment_fullname : "百货栈测试"payment_method : "货到付款"payment_telephone : "13816354916"payment_zone : "上海市"payment_zone_id : "3"shop_id : "3"status : "1"total : "109.0000"
	

	public function getshoporder(){

		$customer_id = trim($this->request->get['customerId']);
		$shop_id = M('customer')->where(array('customer_id'=>$customer_id))->getField('shop_id');
		// echo M('customer')->getlastsql();

		if (empty($shop_id)) {
			$json['error'] ='你没有店铺';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->getshoporder($shop_id);
			foreach ($result as $key => $value) {
				if ($value['status']==1) {
					$status = '初始';
				}elseif ($value['status']==2) {
					$status = '超市已接单';
				}elseif ($value['status']==3) {
					$status = '完成';
				}
				$info[] = array(
					'order_id'=>$value['order_id'],
					'total'=>$value['total'],
					'payment_fullname'=>$value['payment_fullname'],
					'payment_telephone'=>$value['payment_telephone'],
					'payment_method'=>$value['payment_method'],
					'payment_address'=>$value['payment_country'].$value['payment_zone'].$value['payment_city'].$value['payment_address'],
					'status'=>$status,
					'status_id'=>$value['status'],
				);
			}
			
			$json = $info;
			if (empty($json)) {
				$json['error'] ='你没有订单';
				
			}
		}



		$this->response->setOutput(json_encode($json));

	}


	


	public function upOrderStatus(){

		$order_id = trim($this->request->get['order_id']);
		$status_id = trim($this->request->get['status_id']);

		if (empty($order_id)) {
			$json['error'] ='参数有误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->upOrderStatus($order_id,$status_id);
			$json['success'] = $result;
			$json['order_id'] = $order_id;
			
		}



		$this->response->setOutput(json_encode($json));

	}


	public function getorderPorducts(){

		$order_id = trim($this->request->get['order_id']);

		if (empty($order_id)) {
			$json['error'] ='参数有误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->getorderPorducts($order_id);
			
			$this->load->model('tool/image');
			foreach($result as &$v){

				/*货缩略图*/
				if ($v['image'] != null && file_exists(DIR_IMAGE . $v['image'])) {
	                $v['image'] = $this->model_tool_image->resize($v['image'], 100, 100);
	            } else {
	                $v['image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
	            }
				/*货缩略图*/
				
			}

			$json['products'] = $result;
			if (empty($json['products'])) {
				$json['error'] ='你没有商品';
				
			}
			
			
		}



		$this->response->setOutput(json_encode($json));

	}

	public function getcustomerinfo(){

		$customerId = trim($this->request->get['customerId']);

		if (empty($customerId)) {
			$json['error'] ='参数有误';
			
		}else{
			$this->load->model('wxapp/shopinfo');
			$result = $this->model_wxapp_shopinfo->getcustomerinfo($customerId);
			// var_dump($result);
			// var_dump($result['country']);
			// var_dump($result['zone']);
			// var_dump($result['city']);
			if (empty($result['country'])||empty($result['zone'])||empty($result['city'])) {
				$country = "上海市";
				$zone = "上海市";
				$city = "上海市";

			}else{
				$country = $result['country'];
				$zone = $result['zone'];
				$city = $result['city'];
			}
			$data = array(
				'fullname'=>$result['fullname'] ,
				'telephone'=>$result['telephone'] ,
				'address'=>$result['address'] ,
				'address_id'=>$result['address_id'] ,
				'country'=>$country ,
				'zone'=>$zone,
				'city'=>$city ,
				
			);

			$json['customerinfo'] = $data;
			// var_dump($data);
			if (empty($json['customerinfo'])) {
				$json['error'] ='你没有信息';
				
			}
			
			
		}



		$this->response->setOutput(json_encode($json));

	}

	
	public function upcustomerInfo(){

		$customerId = trim($this->request->get['customerId']);
		$fullname = trim($this->request->get['fullname']);
		$address = trim($this->request->get['address']);
		$address_id = trim($this->request->get['address_id']);
		$city = trim($this->request->get['city']);
		$zone = trim($this->request->get['zone']);
		$telephone = trim($this->request->get['telephone']);
		$country = trim($this->request->get['country']);
		// var_dump($this->request->get);
		M('customer')->where(array('customer_id'=>$customerId))->save(array('fullname'=>$fullname,'telephone'=>$telephone));
		if (empty($address_id)) {
			M('address')->where(array('customer_id'=>$customerId))->add(array('address'=>$address,'city'=>$city,'zone'=>$zone,'country'=>$country));
		}else{
			M('address')->where(array('address_id'=>$address_id))->save(array('address'=>$address,'city'=>$city,'zone'=>$zone,'country'=>$country,'customer_id'=>$customerId));
		}
		// echo M('customer')->getlastsql();
		// echo M('address')->getlastsql();


	}

	
	public function getpower(){

		$customer_id = trim($this->request->get['customer_id']);
		$result = M('customer')->where(array('customer_id'=>$customer_id))->find();
		$json['shop_id'] = $result['shop_id'];
		$json['is_user'] = $result['is_user'];
		$json['is_vendor'] = $result['is_vendor'];

		$this->response->setOutput(json_encode($json));
	}

	public function getshop(){

		$code_id = trim($this->request->get['code_id']);
		$result = M('shop')->where(array('qrcode_id'=>$code_id))->find();
		// echo M('shop')->getlastsql();
		if ($result['shop_id']) {
			$json['shop_id'] = $result['shop_id'];
			
		}

		$this->response->setOutput(json_encode($json));
	}


}

?>
