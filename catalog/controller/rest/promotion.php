<?php
/**
 * account.php
 *
 * Promotion management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://newapi2.opencart-api.com/demo/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestPromotion extends RestController {
	public function rewardinfo()
	{	

		$json = array('success' => true);
		if (!$this->customer->isLogged()) {
			$json["error"]["warning"] = "用户还没有登陆";
			$json["success"] = false;
		}
		$this->load->model('account/customer');
		$this->load->model('account/reward');
		if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
			if($json['success']){
				$rewordarticle = $this->model_account_reward->getRewordArcitle("积分回扣");
				if($rewordarticle){
						$description = $rewordarticle['description'];
						$description = htmlspecialchars_decode($description);		
						$json['rewordarticle'] = $description;
				}else{
					$json["error"]["warning"] = "积分介绍不存在";
					$json["success"] = false;
				}
			}

			if($json["success"]){
				$user = $this->model_account_customer->getCustomer($this->customer->getId());
				$json['customer_id']=$user['customer_id'];
				$totalpoint = $this->model_account_reward->getTotalPoints();
				if($totalpoint){
					$json['totalpoint'] = $totalpoint;
				}else{
					$json['totalpoint'] = 0;
				}
			}
			
			$this->sendResponse($json);
		}

	}
	public function hotcake(){

		$json = array('success' => true);
		if (!$this->customer->isLogged()) {
			$json["error"]["warning"] = "用户还没有登陆";
			$json["success"] = false;
		}

		$this->load->model('catalog/category');
		if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
			if($json["success"]){
			$category = $this->model_catalog_category->getCategoryByName('爆款');
				if($category){
					$json['category'] = $category;
				}
				else{
					$json['success'] = false; 
					$json["error"]["warning"] = "爆款不存在";
				}
			}
			$this->sendResponse($json);
		}
		
	}
}