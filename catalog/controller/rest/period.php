<?php 

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestPeriod extends RestController {
	
	private $error = array();

	public function getProductByOrder($order_id){
		if ( $order_id ){
			$this->load->model('catalog/product');
			$results=$this->model_catalog_product->getProductsByOrder($order_id);

			return $results;
		}
		else{
			return 0;
		}
		
	}
	
	public function periods() {

		$this->checkPlugin();
		

		if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){

			//get details
			if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
				$this->getOrder($this->request->get['id']);
			}else {
				//get list
				$this->listPeriods();
			}
		}else if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			//reorder
			if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
                if (isset($this->request->get['order_product_id'])) {
                    $order_product_id = $this->request->get['order_product_id'];
                } else {
                    $order_product_id = 0;
                }

				$this->reorder($this->request->get['id'], $order_product_id);
			}else {
				$this->response->setOutput(json_encode(array('success' => false)));
			}   

		}

	}

	public function listPeriods() {
		$json = array('success' => true);
		
		if (!$this->customer->isLogged()) {
			$json["error"]['warning'] = "用户未登录";
			$json["success"] = false;
		}

		$this->language->load('account/order');

		$this->load->model('account/order');

		if($json["success"]){
			$page = 1;

			$data['periods'] = array();

			$order_total = $this->model_account_order->getTotalPeriods();

			$results = $this->model_account_order->getPeriods(($page - 1) * 10, 1000);

			foreach ($results as $result) {
				
				$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
				$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

				//判断本订单的幸运抽奖情况
				$luckydraw = $this->model_account_order->getLuckydraw($result);
				$is_pay	 = $result['is_pay']?'(已付款)':'(未付款)';	
				$pata = array(
					'order_id'   => $result['order_id'],
					'name'       => $result['fullname'] ,
					'status'     => $result['status'].$is_pay,
					'order_status_id'     => $result['order_status_id'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'products'   => ($product_total + $voucher_total),
					'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                    'currency_code'	=> $result['currency_code'],
                    'currency_value'=> $result['currency_value'],
                    'product_detail'=> $this->getProductByOrder($result['order_id']),
                    'luckydraw'=>$luckydraw
				);

				if($result['is_pay'] == 0){
					$pata['need_pay'] = 1;
				}else{
					$pata['need_pay'] = 0;
				}

				if($result['status'] == '部分发货' || $result['status'] == "全部发货") {
					$data2['待收货'][] = $pata;
				} else {
					$data2[$result['status']][] = $pata;
				}
			}

			$data['periods']=$data2;
			if(count($data['periods']) > 0){
				$json["data"] = $data;
			}else {
				$json["data"] = $data;
				// $json["success"] = false;
				// $json["error"]['warning']= "没有找到用户订单";
			}
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}			
	}

}