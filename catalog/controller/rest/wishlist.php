<?php 
/**
 * wishlist.php
 *
 * wishlist management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://newapi2.opencart-api.com/demo/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestWishlist extends RestController {
	
	/*
	* Get wishlist
	*/
  	public function loadwishlist() {

		$json = array('success' => true);

        $this->load->language('account/wishlist');

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $wishlist = array();

        if( !$this->customer->isLogged()){
            if (isset($this->session->data['wishlist'])) {
                $wishlist = $this->session->data['wishlist'];
            }
        } else {
            
                $this->load->model('account/wishlist');
                $list = $this->model_account_wishlist->getWishlist();
                foreach($list as $item){
                    $wishlist[$item['product_id']] = $item['product_id'];
                }
            
        }

        $json["data"]['products'] = array();

        foreach ($wishlist as $key => $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                } else {
                    $image = false;
                }

                if ($product_info['quantity'] <= 0) {
                    $stock = $product_info['stock_status'];
                } elseif ($this->config->get('config_stock_display')) {
                    $stock = $product_info['quantity'];
                } else {
                    $stock = $this->language->get('text_instock');
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                $json["data"]['products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'thumb'      => $image,
                    'name'       => $product_info['name'],
                    'model'      => $product_info['model'],
                    'minimum'      => $product_info['minimum'],
                    'stock'      => $stock,
                    'price'      => $price,
                    'special'    => $special
                );
            } else { if(!$this->customer->isLogged()){
                    unset($this->session->data['wishlist'][$key]);
                } else {
                        $this->model_account_wishlist->deleteWishlist($product_id);
                    
                }
            }
        }
        $this->sendResponse($json);
	}



    public function loadwishlistmanu() {



        $json = array('success' => true);

        $this->load->language('account/wishlist');

        $this->load->model('catalog/vendor');
      
        $this->load->model('tool/image');

        $wishlist = array();

        if( !$this->customer->isLogged()){

            if (isset($this->session->data['wishlist'])) {
                $wishlist = $this->session->data['wishlist'];
            }
        } else {


                $this->load->model('account/wishlist');
                $list = $this->model_account_wishlist->getWishlistmanu();
               
                foreach($list as $item){
                    $wishlist[$item['vendor_id']] = $item['vendor_id'];
                }
            
        }


        $json["data"]['vendor'] = array();

        foreach ($wishlist as $key => $vendor_id) {
            $vendor_info = $this->model_catalog_vendor->getvendor($vendor_id);
         
           

            if ($vendor_info) {
                if ($vendor_info['vendor_image']) {
                    $image = $this->model_tool_image->resize($vendor_info['vendor_image'], $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                } else {
                    $image = false;
                }
                if(isset($vendor_info['products_image'])){  
                    $products_image_private=$vendor_info['products_image'];
                    
                    $vendor_info['products_image']['0']=$this->model_tool_image->resize('no_image.jpg', $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                    $vendor_info['products_image']['1']=$this->model_tool_image->resize('no_image.jpg', $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                    $vendor_info['products_image']['2']=$this->model_tool_image->resize('no_image.jpg', $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));    
                   foreach ($products_image_private as $key => $value) {

                         if (($value) && file_exists(DIR_IMAGE . $value)) {
                            $vendor_info['products_image'][$key] = $this->model_tool_image->resize($value, $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                        } else {
                            $vendor_info['products_image'][$key] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_rest_api_image_width'), $this->config->get('config_rest_api_image_height'));
                        }
                   }
                    $products_image = $vendor_info['products_image'];
                }

             

                $json["data"]['vendor'][] = array(
                    'vendor_id' => $vendor_info['vendor_id'],
                    'thumb'      => $image,
                    'name'       => $vendor_info['vendor_name'],
                    'description'  => $vendor_info['vendor_description'],
                    'products_image'=>$products_image,
                );
            } else { if(!$this->customer->isLogged()){
                    unset($this->session->data['wishlist'][$key]);
                } else {
                        $this->model_account_wishlist->deleteWishlistmanu($manu_id);
                    
                }
            }
        }
        $this->sendResponse($json);
    }

	/* 
	* delete wishlist
	*/
	public function deleteWishlist($productId) {

		$this->load->language('account/wishlist');

		$json = array('success' => true);
        if(!$this->customer->isLogged()){
            $key = array_search($productId, $this->session->data['wishlist']);

            if ($key !== false) {
                unset($this->session->data['wishlist'][$key]);
                $json["data"]['success'] = $this->language->get('text_remove');
            } else {
                $json["error"]["warning"] = "没有找到此产品";
                $json["success"] = false;
            }
        } else {
                $this->load->model('account/wishlist');
                $this->model_account_wishlist->deleteWishlist($productId);
                $json["data"]['success'] = $this->language->get('text_remove');
        }

        $this->sendResponse($json);
	}


    public function deleteWishlistmanu($manu_id) {

        $this->load->language('account/wishlist');

        $json = array('success' => true);
        if(!$this->customer->isLogged()){
            $key = array_search($manu_id, $this->session->data['wishlist']);

            if ($key !== false) {
                unset($this->session->data['wishlist'][$key]);
                $json["data"]['success'] = $this->language->get('text_remove');
            } else {
                $json["error"]["warning"] = "没有找到此产品";
                $json["success"] = false;
            }
        } else {   
                $this->load->model('account/wishlist');
                $this->model_account_wishlist->deleteWishlistmanu($manu_id);
                $json["data"]['success'] = $this->language->get('text_remove');
        }

        $this->sendResponse($json);
    }


	/* 
	* add to wishlist
	*/
	public function addWishlist($productId) {
		
		$json = array('success' => true);		
		
		$this->load->language('account/wishlist');

		if (!empty($productId)) {
			$product_id = $productId;
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
            if(!$this->customer->isLogged()){
                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }
                $this->session->data['wishlist']['product_id'] = $product_id;
                $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
            } else {
                    $this->load->model('account/wishlist');
                    $this->model_account_wishlist->addWishlist($product_id);
            }

            //$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}else {
            $json['success'] = false;
            $json['error']['warning']= '没有找到此产品';
        }

        $this->sendResponse($json);
	}



    public function addWishlistmanu($manu_id) {
        
        $json = array('success' => true);       

        if (!empty($manu_id)) {
            $manu_id = $manu_id;
        } else {
            $manu_id = 0;
        }

        $this->load->model('catalog/vendor');

        $manu_info = $this->model_catalog_vendor->getvendor($manu_id);

        if ($manu_info) {
            if( !$this->customer->isLogged()){
                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }
                $this->session->data['wishlist']['manu_id'] = $manu_id;
                $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
            } else {
                    $this->load->model('account/wishlist');
                    $this->model_account_wishlist->addWishlistmanu($manu_id);
             
            }

            
        }else {
            $json['success'] = false;
            $json['error']['warning']= '没有找到此商家';
        }

        $this->sendResponse($json);
    }




	/*
	* WISHLIST FUNCTIONS
	*/	
	public function wishlist() {

		$this->checkPlugin();

		if ( $_SERVER['REQUEST_METHOD'] === 'GET' ){
			//add item to wishlist
             if (isset($this->request->get['product'])) {
               
                $this->loadwishlist();
            }elseif (isset($this->request->get['vendor'])) {
              

                $this->loadwishlistmanu();}
        }
         else if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			//add item to wishlist
			 if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
               
				$this->addWishlist($this->request->get['id']);
			}elseif (isset($this->request->get['manu_id']) && ctype_digit($this->request->get['manu_id'])) {
                $this->addwishlistmanu($this->request->get['manu_id']);
                
            }else{
                 $this->sendResponse(array('success' => false));
			}
		}else if ( $_SERVER['REQUEST_METHOD'] === 'DELETE' ){
			//delete item from wishlist
			 if (isset($this->request->get['id']) && ctype_digit($this->request->get['id'])) {
				$this->deleteWishlist($this->request->get['id']);
			}elseif (isset($this->request->get['manu_id']) && ctype_digit($this->request->get['manu_id'])) {
                $this->deletewishlistmanu($this->request->get['manu_id']);
                
            }
            else {
                 $this->sendResponse(array('success' => false));
			}
		}

    }
}