<?php
class ControllerCommonIndex extends Controller {
  public function index() {
    
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/m/template/index.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/m/template/index.tpl'));
    } else {
      $this->response->setOutput($this->load->view('default/template/common/home.tpl'));
    }
  }
}