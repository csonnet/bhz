<?php
class ControllerAccountAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['password'] = $this->url->link('account/password', '', 'SSL');
		$data['address'] = $this->url->link('account/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['return'] = $this->url->link('account/return', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['recurring'] = $this->url->link('account/recurring', '', 'SSL');

		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function scanToGetCoupon() {
		$error = '';
		//从GET参数中获取coupon code
		$code = $this->request->get['coupon_code'];
		if(empty($code)) {
			$error = '优惠券二维码错误！';
		}

		$this->load->model('checkout/coupon');
		$error = $this->model_checkout_coupon->validateCoupon($code);
		// //判断coupon是否已经绑定过customer
		// if(empty($error)) {
		// 	$coupon_has_customer = $this->model_checkout_coupon->couponHasCustomer($code);
		// 	if($coupon_has_customer) {
		// 		$error = '此优惠券已经绑定过，不能再绑定。如有任何问题，请联系现场工作人员。';
		// 	}
		// }
		// //判断coupon是否存在于coupon表，且是有效的coupon
		// if(empty($error)) {
		// 	$coupon = $this->model_checkout_coupon->getCoupon2($code);
		// 	if(!$coupon) {
		// 		$error = '未找到优惠券。如有任何问题，请联系现场工作人员。';
		// 	}
		// }
		// //判断此用户是否绑过这次活动的优惠券（根据name来判断）
		// if(empty($error)) {
		// 	$had_get_scan_coupon = $this->model_checkout_coupon->hasHadScanCoupon($code);
		// 	if($had_get_scan_coupon) {
		// 		$error = '您的账号已经绑定过本次活动的优惠券';
		// 	}
		// }

		if(!empty($error)) {
			$data = array('text_error'=>$error);
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}

		//判断用户是否已经登录
		if ($this->customer->getId()) {
			//如果已经登录，将coupon放入他的账号下
			$this->model_checkout_coupon->assignCouponToCustomer($code);
			$this->redirectAfterScan('coupon');
		} else {
			//如果没有登录，跳转到account login，同时将coupon code记录在session中
			$this->session->data['scan_coupon_code'] = $code;
			$this->redirectAfterScan('login');
		}
		
	}

	public function scanToGetCouponForEveryOne() {
		// return;
		//判断用户是否可以拿券
		$coupon_name = '628家居钜惠节';
		$this->load->model('checkout/coupon');
		$has_had = $this->model_checkout_coupon->hasHadScanCouponByName($coupon_name);
		if($has_had) {
			$data = array('text_error'=>'您已经参加过本次活动！');
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}

		//判断用户是否已经登录
		if ($this->customer->getId()) {
			//如果已经登录，将coupon放入他的账号下
			$this->load->model('marketing/coupon');
	    $data['name'] = $coupon_name;
	    $data['type'] = 'F';
	    $data['discount'] = 300;
	    $data['total'] = 3000;
	    $data['logged'] = 1;
	    $data['shipping'] = 0;
	    $data['date_start'] = '2016-06-27';
	    $data['date_end'] = '2016-07-11';
	    $data['uses_total'] = 1;
	    $data['uses_customer'] = 1;
	    $data['status'] = 1;

	    $this->load->model('marketing/coupon');
	    $data['code'] = substr(md5(uniqid(rand(), true)), 0, 10);
	    $coupon_id = $this->model_marketing_coupon->addCoupon($data);
	  	
			$this->model_checkout_coupon->assignCouponToCustomer($data['code']);
			$this->redirectAfterScan('coupon');
		} else {
			$rtnUrl = $this->url->link('common/index' . '#/login');
			$data = array('text_error'=>'您还未登录百货栈！您需要登录后才能获取优惠券', 'rtnUrl'=>$rtnUrl);
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}
	}

	public function scanToGet2KCouponForEveryOne() {
		// return;
		//判断用户是否可以拿券
		$coupon_name = '628家居钜惠节';
		$this->load->model('checkout/coupon');
		$has_had = $this->model_checkout_coupon->hasHadScanCouponByName($coupon_name);
		if($has_had) {
			$data = array('text_error'=>'您已经参加过本次活动！');
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}

		//判断用户是否已经登录
		if ($this->customer->getId()) {
			//如果已经登录，将coupon放入他的账号下
			$this->load->model('marketing/coupon');
	    $data['name'] = $coupon_name;
	    $data['type'] = 'F';
	    $data['discount'] = 200;
	    $data['total'] = 2000;
	    $data['logged'] = 1;
	    $data['shipping'] = 0;
	    $data['date_start'] = '2016-08-08';
	    $data['date_end'] = '2016-08-16';
	    $data['uses_total'] = 1;
	    $data['uses_customer'] = 1;
	    $data['status'] = 1;

	    $this->load->model('marketing/coupon');
	    $data['code'] = substr(md5(uniqid(rand(), true)), 0, 10);
	    $coupon_id = $this->model_marketing_coupon->addCoupon($data);
	  	
			$this->model_checkout_coupon->assignCouponToCustomer($data['code']);
			$this->redirectAfterScan('coupon');
		} else {
			$rtnUrl = $this->url->link('common/index' . '#/login');
			$data = array('text_error'=>'您还未登录百货栈！您需要登录后才能获取优惠券', 'rtnUrl'=>$rtnUrl);
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}
	}

	public function easyScanToGetCp() {
		// return;
		//3月促销优惠券11.01 ~ 11-30
		
		$coupon_level = array(
			'50'=>'800',
			'100'=>'1500',
			'200'=>'3000',
			);

		$coupon_num = array('A','B','C','D','E','F');

		$validated = false;
		if(isset($this->request->get['lvl'])) {
			if(array_key_exists($this->request->get['lvl'], $coupon_level)) {
				$discount = (float)$this->request->get['lvl'];
				$total = (float)$coupon_level[$discount];
			}
		}

		if(!isset($discount)) {
			$data = array('text_error'=>'二维码错误');
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}

		if(isset($this->request->get['num'])) {
			if(in_array($this->request->get['num'], $coupon_num)) {
				$validated = true;
			}
		}

		$coupon_name = '3月优惠券'.$this->request->get['lvl'].$this->request->get['num'];
		//判断此用户是否绑过这次活动的优惠券（根据name来判断）
			if(!$validated) {
				$error = array('text_error'=>'二维码错误');
				$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $error));
				return;
			}

			$this->load->model('checkout/coupon');
				if(empty($error)) {
				$had_get_scan_coupon = $this->model_checkout_coupon->hasHadScanCouponByName($coupon_name);
				if($had_get_scan_coupon) {
					$error = array('text_error'=>'您的账号已经绑定过本次活动的优惠券');
					$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $error));
					return;
				}
			}

		$this->load->model('checkout/coupon');

		//判断用户是否已经登录
		if ($this->customer->getId()) {
			//如果已经登录，将coupon放入他的账号下
		$this->load->model('marketing/coupon');
	    $data['name'] = $coupon_name;
	    $data['type'] = 'F';
	    $data['discount'] = $discount;
	    $data['total'] = $total;
	    $data['logged'] = 1;
	    $data['shipping'] = 0;
	    $data['date_start'] = '2017-03-01';
	    $data['date_end'] = '2017-05-01';
	    $data['uses_total'] = 1;
	    $data['uses_customer'] = 1;
	    $data['status'] = 1;
		$data['code'] = substr(md5(uniqid(rand(), true)), 0, 10);

	    $this->load->model('marketing/coupon');
	    $coupon_id = $this->model_marketing_coupon->addCoupon($data);
	  	
			$this->model_checkout_coupon->assignCouponToCustomer($data['code']);
			$this->redirectAfterScan('coupon');
		} else {
			$rtnUrl = $this->url->link('common/index' . '#/login');
			$data = array('text_error'=>'您还未登录百货栈！您需要登录后才能获取优惠券', 'rtnUrl'=>$rtnUrl);
			$this->response->setOutput($this->load->view('default/template/error/coupon_not_found.tpl', $data));
			return;
		}
	}

	public function redirectAfterScan($rdFlag = 'coupon') {
		$this->load->helper('mobile');
		if($rdFlag == 'coupon') {
			$route = '#/usercenter/coupon';
		} else if($rdFlag == 'login') {
			$route = '#/login';
		}
		if(is_mobile()) {
			$rtnUrl = $this->url->link('common/index' . $route);
		} else {
			$rtnUrl = $this->url->link('common/pc') . $route;
		}
	 	$this->response->redirect($rtnUrl);
	}
}