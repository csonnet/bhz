<?php
class ControllerAccountAddCouponForNewRegTmp extends Controller {
  public function add() {
    return;
    $this->load->model('marketing/coupon');
    $data['name'] = '新用户注册';
    $data['type'] = 'F';
    $data['discount'] = 20;
    $data['total'] = 30;
    $data['logged'] = 1;
    $data['shipping'] = 0;
    $data['date_start'] = date('Y-m-d H:i:s');
    $data['date_end'] = date('Y-m-d H:i:s', time()+3600*24*30);
    $data['uses_total'] = 1;
    $data['uses_customer'] = 1;
    $data['status'] = 1;
    $data['number'] = 5;

    $customer = M('customer');
    $query = $customer->field('customer_id')->select();

    foreach ($query as $cid) {
      $customer_id = $cid['customer_id'];
      $this->model_marketing_coupon->addCouponToCustomer($customer_id, $data);
    }
    echo 'OK';
  }

  public function exhibition() {
    return;
    //生成300张500元券，有效期是6月28号，最小启用金额是3000元
    $this->load->model('marketing/coupon');
    $data['name'] = '628家居钜惠节测试';
    $data['type'] = 'F';
    $data['discount'] = 500;
    $data['total'] = 3000;
    $data['logged'] = 1;
    $data['shipping'] = 0;
    $data['date_start'] = '2016-06-15';
    $data['date_end'] = '2016-06-16';
    $data['uses_total'] = 1;
    $data['uses_customer'] = 1;
    $data['status'] = 1;

    // $data['number'] = 300;
    $data['number'] = 2;

    $this->load->model('marketing/coupon');
    $number = (int)$data['number'];
    while($number > 0) {
        $number--;
        $data['code'] = substr(md5(uniqid(rand(), true)), 0, 10);
        $coupon_id = $this->model_marketing_coupon->addCoupon($data);
        echo $data['code'] . '<br>';    
    }
    echo 'OK';
  }
}