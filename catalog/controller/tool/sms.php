<?php
class ControllerToolSms extends Controller {
    public function index(){//临时发送短信用
        die();
        $this->load->helper('sms');
        $appId = $this->config->get('tianyi_appid');
        $appSecret = $this->config->get('tianyi_appsecret');
        $templateId = '91553507';//开通帐期确认，详见 http://open.189.cn/ 
        $phoneList = array(
            '15251908369',
            '15952958069',
            '17712327257',
            '17768795888',
            '15888727005',
            '18068420984',
            '13636433732',
            '18013297561',
            //'17751258061',
            '13584876767',
            '18912382806',
            '13990352220',
            '18057899995',
            '18651029228',
            '13358166816',
            '15161153921',
            '18151200003',
            '15851666166',
            '13115286299',//路栋维
            '13780179189',//任奎
            '18662639487',//史东波
            '13771974902',//宋振
            //'18806766220',//王文敏
            '15005642551',//鲍剑
            '13485063782',//杨传民
            '15995065805',//张俊雅
            '15996962623',//赵庆宝
            '13661895330',//孙建华
            '13564536028',//顾俊
            '18616897011',//陈启令
        );
        $tempParam = array(
            'endDate' => '11/8',
            'rangeDate' => '11/1-11/30',
            'payDate' => '12/25',
            'rangeDate1' => '11/1-11/30',
        );
        $sms = new smsHelper();
        foreach ($phoneList as $phone) {
            print_r($sms->sendSms($appId, $appSecret, $templateId, $phone, $tempParam));
        }
        exit();
    }
}
