<?php

class ControllerToolScan extends Controller {

    public function check_order(){

        $orderid = I('get.orderid');
        $this->load->model('tool/scan');
        if (empty($orderid)) {
        	$json['error']='订单号不能为空';
        }else{
        	$data =$this->model_tool_scan->checkorder($orderid);
        	if (empty($data)) {
        		$json['error']='订单不存在';
        	}else{
        		$json['orderid'] = $data;
        	}
        }
        

    	// var_dump($json);
        echo json_encode($json);
    }


    public function get_order_info(){

        $orderid = I('get.orderid');
        // var_dump($orderid);
        // echo 111;
        $this->load->model('tool/scan');
        if (empty($orderid)) {
        	$json['error']='订单号不对';
        }else{
        	$data =$this->model_tool_scan->getStockOut($orderid);
            $data['differ'] = $data['sign_price']-$data['conform_price'];

            $totals = $this->model_tool_scan->getOrderTotals($orderid);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }
        	if (empty($data)) {
        		$json['error']='订单号不对';
        	}else{
        		$json['orderinfo'] = $data;
        	}
        }
        

    	// var_dump($json);
        echo json_encode($json);
    }
    
    public function get_order_products(){

        $order_id = I('get.order_id');
        // echo 111;
        $this->load->model('tool/scan');

        $products = $this->model_tool_scan->getOrderProducts($order_id); 
        // var_dump($products);
        $childproducts = NULL;
        foreach ($products as $product) {
            if($product['product_type'] == 2){
                $childproducts = $this->model_tool_scan->getchildorderproducts($product['order_product_id']);
                foreach($childproducts as $key=>$val){
                    $data['product'][] = array(
                        'order_product_id' => $val['order_product_id'],
                        'product_id'       => $val['product_id'],
                        'product_code'       => $val['product_code'],
                        'sku'              => $val['sku'],
                        'name'             => $val['pdname'],
                        'model'            => $val['model'],
                        'quantity'         => (int)$val['quantity'],
                        'order_ids'    => $product['order_ids'].'-'.$val['order_ids'],
                        'sign_quantity'    => (int)$val['sign_quantity'],
                        'lack_quantity'    => (int)$val['lack_quantity'],
                        'price'            => $this->currency->format($val['price']),
                        'total'            => $this->currency->format($val['total'])
                    );
                }
                continue;
            }



            $data['product'][] = array(
                'order_product_id' => $product['order_product_id'],
                'product_id'       => $product['product_id'],
                'product_code'       => $product['product_code'],
                'sku'              => $product['sku'],
                'name'             => $product['name'],
                'model'            => $product['model'],
                'quantity'         => (int)$product['quantity'],
                'order_ids'    => $product['order_ids'],
                'sign_quantity'    => (int)$product['sign_quantity'],
                'lack_quantity'    => (int)$product['lack_quantity'],
                'price'            => $this->currency->format($product['price']),
                'total'            => $this->currency->format($product['total'])
            );
        }
        $json = $data;
        

    	// var_dump($json);
        echo json_encode($json);
    }
    

    public function get_sku_product(){

        $sku = trim(I('get.sku'));
        $order_id = trim(I('get.order_id'));
        // $name = I('get.name');
        // echo 111;
        $this->load->model('tool/scan');

        // $json['sproduct'] = $data;

        if (empty($sku)) {
        	$json['error']='参数不对';
        }else{
            //在非组合商品中查找
            $data =[];
        	$productsku = $this->model_tool_scan->getOrderProductsBysku($sku,$order_id); 
            // var_dump($productsku);
            foreach ($productsku as $product) {
                $data['sproduct'][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id'       => $product['product_id'],
                    'product_code'       => $product['product_code'],
                    'sku'              => $product['sku'],
                    'name'             => $product['name'],
                    'model'            => $product['model'],
                    'quantity'         => (int)$product['quantity'],
                    'order_ids'    => $product['order_ids'],
                    'sign_quantity'    => (int)$product['sign_quantity'],
                    'lack_quantity'    => (int)$product['lack_quantity'],
                    'price'            => $this->currency->format($product['price']),
                    'total'            => $this->currency->format($product['total'])
                );
            }
            // var_dump($data);
            // die();
            $products = $this->model_tool_scan->getOrderProducts($order_id); 
            //在组合商品中查找
            foreach ($products as $product) {
                if($product['product_type'] == 2){
                    $childproducts = $this->model_tool_scan->getchildorderproductsBysku($product['order_product_id'],$sku);
                    foreach($childproducts as $key=>$val){
                        $data['sproduct'][] = array(
                            'order_product_id' => $val['order_product_id'],
                            'product_id'       => $val['product_id'],
                            'product_code'       => $val['product_code'],
                            'sku'              => $val['sku'],
                            'name'             => $val['pdname'],
                            'model'            => $val['model'],
                            'quantity'         => $val['quantity'],
                            'order_ids'    => $product['order_ids'].'-'.$val['order_ids'],
                            'sign_quantity'    => (int)$val['sign_quantity'],
                            'lack_quantity'    => (int)$val['lack_quantity'],
                            'price'            => $this->currency->format($val['price']),
                            'total'            => $this->currency->format($val['total'])
                        );
                    }
                }

            }
            // var_dump($data); die();

        	if (empty($data)) {
        		$json['error']='条码不对';
        	}else{
        		$json = $data;
        	}
        }
        

    	// var_dump($json);
        echo json_encode($json);
    }

    public function conform(){

        $product_code = I('get.product_code');
        $sign_quantity = I('get.sign_quantity');
        $order_id = I('get.order_id');
        $order_ids = I('get.order_ids');
        $this->load->model('tool/scan');
        $sign_quantity = $sign_quantity;
        // if (isset($sign_quantity)||isset($product_code)) {
        //     $json['error']='参数不对';
        //     $json['order_id'] = $order_id;
        // }else{
            $data =$this->model_tool_scan->conform($product_code,$sign_quantity,$order_id,$order_ids);
            if (empty($data)) {
                $json['error']='参数不对';
            }else{
                $json['success'] = $data;
                $json['order_id'] = $order_id;
            }
           
        // }
        

        // var_dump($json);
        echo json_encode($json);
    }

    public function sign(){

        $order_id = trim(I('get.order_id'));
        $this->load->model('tool/scan');

        if (empty($order_id)) {
            $json['error']='参数不对';
            $json['order_id'] = $order_id;
        }else{
            $data =$this->model_tool_scan->sign($order_id);
            if (empty($data)) {
                $json['error']='参数不对';
            }else{
                $json['success'] = $data;
                $json['order_id'] = $order_id;
            }
           
        }
        

        // var_dump($json);
        echo json_encode($json);
    }

    
    public function conform_pay(){

        $order_id = trim(I('get.order_id'));
        $sign_price = trim(I('get.sign_price'));
        $this->load->model('tool/scan');
        if (empty($order_id)) {
            $json['error']='参数不对';
            $json['order_id'] = $order_id;
        }else{
            $data =$this->model_tool_scan->conform_pay($order_id,$sign_price);
            if (empty($data)) {
                $json['error']='参数不对';
            }else{
                $json['success'] = $data;
                $json['order_id'] = $order_id;
            }
           
        }
        

        // var_dump($json);
        echo json_encode($json);
    }

    public function search_price(){

        $order_id = trim(I('get.order_id'));
        $skuname = trim(I('get.skuname'));
        $this->load->model('tool/scan');
        // var_dump($skuname);
        $customer_id = M('order')->where(array('order_id'=>$order_id))->field('customer_id')->find();
        if (empty($skuname)) {
            $json['error']='参数不对';
        }else{
            //在非组合商品中查找
            $data =[];

            $products = $this->model_tool_scan->getAllOrderProducts($customer_id['customer_id']); 

            //在组合商品中查找
            foreach ($products as $product) {
                if($product['product_type'] == 2){
                    $childproducts = $this->model_tool_scan->getAllchildorderproductsBysku($product['order_product_id'],$skuname);
                    foreach($childproducts as $key=>$val){
                        $product_code = $val['product_code'];
                        $data['sproduct'][$product_code] = array(
                            'order_product_id' => $val['order_product_id'],
                            'product_id'       => $val['product_id'],
                            'product_code'       => $val['product_code'],
                            'sku'              => $val['sku'],
                            'name'             => $val['pdname'],
                            'price'            => $this->currency->format($val['price']),
                        );
                    }
                }

            }
            $productsku = $this->model_tool_scan->getAllOrderProductsBysku($skuname,$customer_id['customer_id']); 
            foreach ($productsku as $product) {
                $product_code = $product['product_code'];
                $data['sproduct'][$product_code] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id'       => $product['product_id'],
                    'product_code'       => $product['product_code'],
                    'sku'              => $product['sku'],
                    'name'             => $product['name'],
                    'price'            => $this->currency->format($product['price']),
                );
            }
            // var_dump($data);
            // die();

            // var_dump($data); die();

                $json = $data;
        }

        // var_dump($json);
        echo json_encode($json);
    }
}

?>