<?php

class ControllerToolWxapp extends Controller {
	
	//检查用户是否第一次开启小程序
	public function checkAccount(){
		
		$requestjson = file_get_contents('php://input');
		$data = json_decode($requestjson, true);

		$filter['openid'] = $data['openid'];

		$this->load->model('tool/wxapp');
		$json['customer'] = $this->model_tool_wxapp->checkMp($filter);
		
		$this->response->setOutput(json_encode($json));

	}

}

?>