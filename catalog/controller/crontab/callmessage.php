<?php

header("Content-Type: text/html;charset=utf-8");

require_once(DIR_APPLICATION.'controller/rest/sms.php');

class ControllerCrontabCallMessage extends RestController {
	
	//一周提醒一次未读站内消息用户
	public function checkNoRead(){
		
		$this->load->model('crontab/callmessage');

		$appId = '356090190000250362';
		$appSecret = '386d5615c7a6d9215e23f13384947599';
		$templateId = '91553658';

		$customer = $this->model_crontab_callmessage->getNoRead();
		
		foreach($customer as $v){
			$phone = $v['telephone'];
			$count= $v['count'];
			$url = 'http://m.bhz360.com/m/'.$phone;
			$template_param = json_encode(array(
				'count'=>$count,
				'url'=>$url,
			));

			$sms = new ControllerRestSms;
			$resms = $sms->sendSms($appId, $appSecret, $templateId, $phone, $template_param);
			sleep(2);
		}

	}

}

?>