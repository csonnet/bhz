<?php

require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerMessageMessage extends RestController {

	public function index(){
		
		$url = "http://m.bhz360.com/index.php?route=common/index#/usercenter/message";
		Header("HTTP/1.1 303 See Other");
		Header("Location: $url"); 

	}
	
	//获取我的消息
	public function getMyMessage(){

		$this->checkPlugin();
		$this->load->model('message/message');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();
		$results = $this->model_message_message->getMyMessage($customer_id);
		$json['messages'] = $this->formatResult($results);

		$this->response->setOutput(json_encode($json));

	}

	//获取消息详细
	public function getMessageInfo(){

		$this->checkPlugin();
		$this->load->model('message/message');

		$json = array('success' => true);
		
		$id = $this->request->get['id'];
		$results = $this->model_message_message->getMessageInfo($id);
		$results = $this->formatResult($results);
		$json['messageInfo'] = $results[0];

		$this->response->setOutput(json_encode($json));

	}
	
	//格式化消息
	public function formatResult($data){

		foreach($data as &$v){

			if($v['template_data'] != null){
				$v['content'] = $v['template_data'];
			}

		}

		return $data;
	
	}
	
	//获取我的未读消息数量
	public function noReadCount(){
		
		$this->checkPlugin();
		$this->load->model('message/message');

		$json = array('success' => true);

		$customer_id = $this->customer->getId();
		$json['noReadCount'] = $this->model_message_message->noReadCount($customer_id);

		$this->response->setOutput(json_encode($json));

	}
	
	//已读
	public function hasRead(){

		$this->checkPlugin();
		$this->load->model('message/message');

		$requestjson = file_get_contents('php://input');
		$filter = json_decode($requestjson, true);

		$this->model_message_message->hasRead($filter);

	}
	
	//提醒停用的商品有库存
	public function hasGoods(){
		$this->load->model('message/message');
		$this->model_message_message->hasGoods();
	}

}

?>