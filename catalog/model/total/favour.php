<?php
class ModelTotalFavour extends Model {
	public function getTotal(&$total_data, &$total, &$taxes, $mark) {
		//if (time()<strtotime('2017-8-23 00:00:00') || time()>strtotime('2017-8-23 23:59:59')) {
        // if (time()<strtotime('2018-1-13 00:00:00') || time()>strtotime('2018-2-5 23:59:59')) {
			$this->load->language('total/favour');

			$sub_total = $this->cart->getSubTotal();
			$Ctotal = $this->cart->getCouponC();
			// var_dump($Ctotal);

			if(!$mark){
				// if($sub_total >= 800)
				// 	$favour = -25;

				// if($sub_total >= 1500)
				// 	$favour = -50;

				// if($sub_total >= 3000)
				// 	$favour = -108;
				
				// if($sub_total >= 5000)
				// 	$favour = -168;
				// if($Ctotal >= 1000)
				// 	$favour = -25;
				
				// if($Ctotal >= 2500)
				// 	$favour = -50;

				if($Ctotal >= 1000)
					$favour = -30;
				
				if($Ctotal >= 2000)
					$favour = -60;
				if($Ctotal >= 3000)
					$favour = -118;
            }

            if ($favour < 0){
                $total_data[] = array(
                    'code'       => 'favour',
                    'title'      => $this->language->get('text_favour'),
                    'value'      => $favour,
                    'sort_order' => $this->config->get('favour_sort_order')
                );
            }
            $total += $favour;
		}
	}
// }