<?php
class ModelWxappAnalysis extends Model {
	
	//获取用户
	public function getCustomerInfo($customer_id){
		$where['customer_id'] = $customer_id;
		$result = M('customer')->where($where)->select();
		return $result;
	}

	//获取用户商品数
	public function getProductCount($customer_id){
		$where['o.customer_id'] = $customer_id;
		$where['o.order_status_id'] = array(array('neq',0),array('neq',13),array('neq',11));
		$result = M('order_product')
			->alias('op')
			->join('`order` o on op.order_id = o.order_id','left')
			->where($where)
			->field('op.product_code')->select();

		$productGroup = array();
		foreach($result as $v){
			$temp = explode(",",$v['product_code']);
			$productGroup = array_merge($productGroup,$temp);
		}
		$productCount = count(array_unique($productGroup));

		return $productCount;

	}
	
	//获取用户货架数
	public function getShelfCount($customer_id){
		$where['o.customer_id'] = $customer_id;
		$where['o.order_status_id'] = array(array('neq',0),array('neq',13),array('neq',11));
		$result = M('shelf_order')
			->alias('so')
			->join('`order` o on so.shelf_order_id = o.shelf_order_id','left')
			->where($where)
			->field('so.shelf_ids')->select();
		
		$shelfGroup = array();
		foreach($result as $v){
			$temp = explode(",",$v['shelf_ids']);
			$shelfGroup = array_merge($shelfGroup,$temp);
		}
		$shelfCount = count(array_unique($shelfGroup));

		return $shelfCount;
	}
	
	//获取预期利润
	public function getProfit($customer_id){
		$sql = "SELECT product.product_code, product.price FROM `product`";
        $query = $this->db->query($sql);
        $priceArr = $query->rows;
        foreach ($priceArr as $key => $value) {
        	$pcode = $value['product_code'];
        	$PcodePrice[$pcode] = $value['price'];
        }
        $sql = "SELECT * FROM `order_product` AS op  WHERE op.order_id IN(SELECT o.order_id FROM `order` AS o WHERE o.order_status_id <> 16 AND o.order_status_id <> 0 AND o.order_status_id <> 13 AND o.order_status_id <> 11 AND customer_id = ".$customer_id.")";
        $query = $this->db->query($sql);
        $info = $query->rows;
        foreach ($info as $key => $value) {
        	$pcode = substr($value['product_code'],0,10);
        	$total += ($PcodePrice[$pcode]-$value['pay_price'])*$value['quantity'];
        }
        $total =  round($total,2);
        
		return $total;
	}
	
	//获取附近商铺信息
	public function getRangeOrderTotal($customer_id){
   		$sql = "SELECT c.fullname,SUM( o.total ) AS 'total' FROM `customer` c LEFT JOIN `order` o ON c.customer_id = o.customer_id WHERE o.order_status_id <>16 AND o.order_status_id <>0 AND c.customer_id = ".$customer_id;

   		$query = $this->db->query($sql);
   		return $query->row;
	}

}