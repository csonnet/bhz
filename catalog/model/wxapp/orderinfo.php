<?php
class ModelWxappOrderinfo extends Model {

    // 获取店铺信息 
    public function getshopById($shop_id){
        $info['shopinfo'] = M('shop')->where(array('shop_id'=>$shop_id))->find();
        $sql = "SELECT  ci.shop_id, ci.type, u.`name`, u.filename,u.upload_id FROM customer_images AS ci LEFT JOIN upload AS u ON u.`customer_images_id` = ci.`customer_images_id` where ci.shop_id=".$shop_id; 
        // echo $sql;
        $query = $this->db->query($sql);
        $info['shopinfo']['LogoPic'] = [];
        $info['shopinfo']['Propaganda'] = [];
        $info['shopinfo']['cardPic'] = [];
        $info['shopinfo']['picList'] = [];
        foreach ($query->rows as $key => $value) {
            if ($value['type']==3) {
                $info['shopinfo']['LogoPic'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
                
            }elseif($value['type']==4){
                $info['shopinfo']['Propaganda'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
            }elseif ($value['type']==2) {
                $info['shopinfo']['cardPic'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
            }elseif ($value['type']==1) {
                $info['shopinfo']['picList'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
            }
            // var_dump($temp5[$shop_id]);die();
        }
        return $info;
        
    }


    public function getshoporder($payment_customer_id){
        $where['si.payment_customer_id'] = $payment_customer_id;
        $result = M('shop_order')
            ->alias('si')
            ->where($where)
            ->select();
        return $result;

    }

}