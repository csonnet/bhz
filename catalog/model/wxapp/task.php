<?php

class ModelWxappTask extends Model {

    // 获取任务数据model by ypj at 20185.14  
    public function getTaskList($customerId){

        if(empty($customerId)) {
            return false;
        }
        $filter = array();
        $filter['t.operator_id'] = $customerId;
        $taskInfo = M('salesman_task')->alias('t')
        ->field('t.task_id,t.operator_id,t.creator_id, t.shop_id,c.fullname AS operator_name,cu.fullname AS creator_name,s.shop_name,t.title,t.task_type,t.validtime,t.finished_date,t.`status`, t.description,s.address')
        ->join('customer AS c ON t.operator_id=c.customer_id')
        ->join('shop AS s ON s.shop_id=t.shop_id')
        ->join('customer AS cu ON t.creator_id=cu.customer_id')
        ->where($filter)
        ->select();

        return $taskInfo;
    } 

    // 查询到的业务员信息
    public function getSalesman($salesman_name){

        $filter = array();
        if(empty($salesman_name)){
            return false;
        }
        $filter['fullname'] = array('like', '%'.$salesman_name.'%');
        $salesmanInfo = M('customer')->where($filter)->field('customer_id, fullname, email, telephone')->find();

        return $salesmanInfo;
    }



}