<?php
class ModelWxappMyorder extends Model {

	//获取待收款货架订单
	public function getPayOrders($filter){
		$sql = "
            SELECT
                o.*,
                os.name as order_status
            FROM
                `" . DB_PREFIX . "order` o
                LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
            WHERE o.shelf_order_id != 0 AND o.is_pay = 0 AND o.order_status_id != 16";

		if (!empty($filter['customer_id'])) {
			$sql .= " AND o.customer_id = '".$filter['customer_id']."' ";
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	//获取待发货货架订单
	public function getDeliverOrders($filter){
		$sql = "
            SELECT
                o.*,
                os.name as order_status
            FROM
                `" . DB_PREFIX . "order` o
                LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
            WHERE o.shelf_order_id != 0 AND o.order_status_id = 2";

		if (!empty($filter['customer_id'])) {
			$sql .= " AND o.customer_id = '".$filter['customer_id']."' ";
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	//获取待收货货架订单
	public function getReceiveOrders($filter){
		$sql = "
            SELECT
                o.*,
                os.name as order_status
            FROM
                `" . DB_PREFIX . "order` o
                LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
            WHERE o.shelf_order_id != 0 AND o.order_status_id = 3";

		if (!empty($filter['customer_id'])) {
			$sql .= " AND o.customer_id = '".$filter['customer_id']."' ";
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	//获取退换货货架订单
	public function getReturnOrders($filter){
		$sql = "
            SELECT
                o.*,
                os.name as order_status
            FROM
                `" . DB_PREFIX . "order` o
                LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
            WHERE o.shelf_order_id != 0 AND o.order_status_id = 7";

		if (!empty($filter['customer_id'])) {
			$sql .= " AND o.customer_id = '".$filter['customer_id']."' ";
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	//获取已完成货架订单
	public function getFinishOrders($filter){
		$sql = "
            SELECT
                o.*,
                os.name as order_status
            FROM
                `" . DB_PREFIX . "order` o
                LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
            WHERE o.shelf_order_id != 0 AND o.order_status_id = 5";

		if (!empty($filter['customer_id'])) {
			$sql .= " AND o.customer_id = '".$filter['customer_id']."' ";
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

}
