<?php
class ModelWxappShopinfo extends Model {
	public function getshopById($shop_id){
		$info['shopinfo'] = M('shop')->where(array('shop_id'=>$shop_id))->find();
		$sql = "SELECT  ci.shop_id, ci.type, u.`name`, u.filename,u.upload_id FROM customer_images AS ci LEFT JOIN upload AS u ON u.`customer_images_id` = ci.`customer_images_id` where ci.shop_id=".$shop_id; 
		// echo $sql;
		$query = $this->db->query($sql);
		$info['shopinfo']['qrcode'] = 'http://m.bhz360.com//image/wxapp/qrcode/'.$info['shopinfo']['qrcode_id'].'.jpg';
		$info['shopinfo']['LogoPic'] = [];
		$info['shopinfo']['Propaganda'] = [];
		$info['shopinfo']['cardPic'] = [];
		$info['shopinfo']['picList'] = [];
		foreach ($query->rows as $key => $value) {
			if ($value['type']==3) {
				$info['shopinfo']['LogoPic'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
				
			}elseif($value['type']==4){
				$info['shopinfo']['Propaganda'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
			}elseif ($value['type']==2) {
				$info['shopinfo']['cardPic'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
			}elseif ($value['type']==1) {
				$info['shopinfo']['picList'][$value['upload_id']] = 'http://m.bhz360.com/system/storage/upload/'.$value['filename'];
			}

			

			
			// var_dump($temp5[$shop_id]);die();
		}
		return $info;
		
	}


	public function getProduct($shop_id){
		$sql = "SELECT (SELECT cd.`name` FROM category_description AS cd WHERE cd.category_id = p.product_class3 ) AS product_class3name, (SELECT c.`image` FROM category AS c WHERE c.category_id = p.product_class3 ) AS image3, (SELECT c.`image` FROM category AS c WHERE c.category_id = p.product_class2 ) AS image2, p.product_class3, p.product_class2, (SELECT cd.`name` FROM category_description AS cd WHERE cd.category_id = p.product_class2 ) AS product_class2name FROM shop_inventory AS shi INNER JOIN product AS p ON shi.product_id = p.product_id WHERE shi.shop_id = ".$shop_id;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getShopProductById($shop_inventory_id){
		$sql = "SELECT sh.shop_inventory_id, sh.customer_id, sh.shop_id, sh.product_id, sh.product_code, sh.stock_qty, sh.save_qty, sh.sale_qty, sh.date_added, sh.date_modify, sh.price, sh.sale_price, sh.`status`, p.image, pd.`name`, pd.description FROM shop_inventory AS sh LEFT JOIN product AS p ON sh.product_id = p.product_id LEFT JOIN product_description AS pd ON p.product_id = pd.product_id WHERE sh.shop_inventory_id =".$shop_inventory_id;
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getImages($product_id){
		$sql = "SELECT product_image.image, product_image.product_id FROM `product_image` WHERE product_image.product_id =".$product_id;
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getInShopId($wxAppOpenId){
		$sql = "SELECT product_image.image, product_image.product_id FROM `product_image` WHERE product_image.product_id =".$product_id;
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function addActivity($data){
		return 	M('shop_activity')->add($data);
	}

	// public function getShopCustomer($shop_id){
	// 	$sql = "SELECT sa.shop_activity_id, sa.customer_in_shop_id, sa.shop_id, count(*) as num, sa.`key`, sa.lng, sa.lat, sa.date_added, ci.customer_in_shop_id, ci.open_id, ci.nick_name, ci.gender, ci.country, ci.city, ci.province, ci.`language`, ci.pic, ci.data_in_json FROM shop_activity AS sa LEFT JOIN customer_in_shop AS ci ON sa.customer_in_shop_id = ci.customer_in_shop_id where sa.shop_id =".$shop_id." group by sa.`key` order by sa.shop_activity_id desc ";
	// 	// echo $sql; 
	// 	$query = $this->db->query($sql);
	// 	return $query->rows;
	// }

	public function getShopCustomer($shop_id){
		$sql = "SELECT sc.shop_concern_id, sc.customer_in_shop_id, sc.shop_id, ci.customer_in_shop_id, ci.open_id, ci.nick_name, ci.gender, ci.country, ci.city,sc.date_added, ci.province, ci.`language`, ci.pic, ci.data_in_json FROM shop_concern AS sc LEFT JOIN customer_in_shop AS ci ON sc.customer_in_shop_id = ci.customer_in_shop_id where sc.shop_id =".$shop_id; 
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCategoryProducts($shop_id){
		$where['si.shop_id'] = $shop_id;
		$result = M('shop_inventory')
			->alias('si')
			->join('product p on si.product_id = p.product_id','left')
			->join('product_description pd on si.product_id = pd.product_id','left')
			->where($where)
			->field('pd.name, p.image, si.*')->select();
			// echo M('shop_inventory')->getlastsql();
		return $result;
	}
	

	public function unStatus($shop_inventory_id){
		$where['si.shop_inventory_id'] = $shop_inventory_id;
		$result = M('shop_inventory')
			->alias('si')
			->where($where)
			->save(array('status'=>0));
			// echo M('shop_inventory')->getlastsql();
		return $result;
	}

	public function upStatus($shop_inventory_id){
		$where['si.shop_inventory_id'] = $shop_inventory_id;
		$result = M('shop_inventory')
			->alias('si')
			->where($where)
			->save(array('status'=>1));
			// echo M('shop_inventory')->getlastsql();
		return $result;
	}

	

	public function updateproduct($shop_inventory_id){
		$where['si.shop_inventory_id'] = $shop_inventory_id;
		$result = M('shop_inventory')
			->alias('si')
			->join('product p on si.product_id = p.product_id','left')
			->join('product_description pd on si.product_id = pd.product_id','left')
			->where($where)
			->field('pd.name, p.image, si.*')->select();
			// echo M('shop_inventory')->getlastsql();
		return $result;
	}

	

	public function save($shop_inventory_id,$data){
		$where['si.shop_inventory_id'] = $shop_inventory_id;
		$result = M('shop_inventory')
			->alias('si')
			->where($where)
			->save($data);
			// echo M('shop_inventory')->getlastsql();
		return $result;
	}

	

	public function getshoporder($shop_id){
		$where['si.shop_id'] = $shop_id;
		$result = M('shop_order')
			->alias('si')
			->where($where)
			->select();
			// echo M("shop_order")->getlastsql();
		return $result;
	}

	
	public function upOrderStatus($order_id,$status_id){
		$status_id = $status_id+1;
		$where['si.order_id'] = $order_id;
		$result = M('shop_order')
			->alias('si')
			->where($where)
			->save(array('status'=>$status_id));
		return $result;
	}
	

	public function getorderPorducts($order_id){
		$where['shi.order_id'] = $order_id;
		$result = M('shop_order_product')
			->alias('shi')
			->join('shop_inventory si on si.shop_inventory_id = shi.shop_inventory_id','left')
			->join('product p on si.product_id = p.product_id','left')
			->join('product_description pd on si.product_id = pd.product_id','left')
			->where($where)
			->field('pd.name, p.image, shi.*')->select();
			// echo M('shop_order_product')->getlastsql();
		return $result;
	}
	
	public function getcustomerinfo($customerId){
		$sql = "SELECT c.fullname, c.telephone, c.shop_id, c.customer_id, c.address_id, a.address, a.city, a.zone, a.country
FROM customer AS c
LEFT JOIN address AS a ON c.customer_id = a.customer_id
WHERE c.customer_id =".$customerId;
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->row;
	}
}
