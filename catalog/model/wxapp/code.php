<?php
class ModelWxappCode extends Model {
	private $_defaultDB = 'customer';
	//跳转商品知识
	public function toKnowledge($code){

		$where['sm.shelf_map_id'] = $code;
		$category = M('shelf_map')
			->alias('sm')
			->join('shelf s on sm.shelf_id = s.shelf_id','left')
			->where($where)->getField('s.shelf_category,s.shelf_id,s.shop_id,s.solution_id',3);

		return $category;
	}
        
    public function getShop($code){

        $where['s.qrcode_id'] = $code;
        $shop_id = M('shop')
            ->alias('s')
            ->where($where)->getField('s.shop_id');
        return $shop_id;
    }
	public function separate($wxAppOpenId){
        $sql = "SELECT * FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `wx_mp_openid`='".$wxAppOpenId."'";//OR wx_op_opendid='".$wxAppOpenId."'
        $q = $this->db->query($sql);
        if ($q->num_rows){
            $tmp = $q->row;
            $ret['customerId'] = $tmp['customer_id'];
            //$ret['hasChecked'] = !(bool)$tmp['need_review'];
            if ($userInfo = $this->_getUserIdByTel($tmp['telephone'])) {
                $ret = array_merge($ret, $userInfo);
            }
             return $ret;
        }else{
        	   return false;
        }

    }

    public function getshopnames($customer_id){
        $sql = "SELECT * FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `customer_id`='".$customer_id."'";//OR wx_op_opendid='".$wxAppOpenId."'
        $q = $this->db->query($sql);
        if ($q->num_rows){
            $tmp = $q->row;
            $ret['customerId'] = $tmp['customer_id'];
            //$ret['hasChecked'] = !(bool)$tmp['need_review'];
            if ($userInfo = $this->_getUserIdByTel($tmp['telephone'])) {
                $ret = array_merge($ret, $userInfo);
            }
               $user_id=$ret['userId'];
               $names= $this->customershopnames($user_id);
               return $names;
        }else{
               return false;
        }
    }

    public function customershopnames($userId){
    $sql = "SELECT customer_id,company_name,fullname  FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `user_id`='".$userId."'";//OR ;
        $shopname = $this->db->query($sql)->rows;

        foreach ($shopname as $key => $value) {
            if(!empty($shopname[$key]['company_name'])){
                $value['sum']=$shopname[$key]['company_name'].'+'.$shopname[$key]['fullname'];
                $names[]=$value;
            }
        }
        return $names;
    }


	 public function _getUserIdByTel($tel) {
        $ret = array(
            'userId' => 0,//user表pk，记录操作人信息
            'saleId' => 0,//业务员pk，业务员角色获取订单、客户、数据报表
            'logcenterId' => 0,//物流中心pk，仓管角色获取订单、出库单
        );
        $sql = "SELECT * FROM `".DB_PREFIX."user` WHERE `contact_tel`='".$tel."'";
        $q = $this->db->query($sql);
        if ($q->num_rows){
            $tmp = $q->row;
            $ret['userId'] = $tmp['user_id'];
            $ret['logcenterId'] = $tmp['logcenter_permission'];
            if (63 == $tmp['user_group_id']) {
                $ret['saleId'] = $tmp['user_id'];
            }
        }
        return $ret;
    }

    public function customerinshop($wxAppOpenId,$info){
        $where['open_id'] = $wxAppOpenId;
        $shop = M('customer_in_shop')->where($where)->find();
                if(empty($shop)){
                    $data['open_id'] = $wxAppOpenId;
                    $data['nick_name'] = $info['nickName'];
                    $data['gender'] = $info['gender'];
                    $data['country'] = $info['country'];
                    $data['city'] = $info['city'];
                    $data['province'] = $info['province'];
                    $data['language'] = $info['language'];
                    $data['pic'] = $info['avatarUrl'];
                    $data['data_in_json'] = json_encode($info);
                    M('customer_in_shop')->add($data);
                }
    }
    //根据用户id获取店铺名称
    public function getshopname($user_id){
        $where['customer_id'] = $user_id;
        $shopname = M('customer')->where($where)->getField('company_name');
        return $shopname;
    }

    //根据用户id获取店铺名称
    public function getshopnameandshopid($user_id){
        $shopinfo = $this->db->query("SELECT shop_name,shop_id  FROM `".DB_PREFIX."shop` WHERE `customer_id`='".$user_id."'");
        //var_dump($shopinfo);
        return $shopinfo->row;
    }


}
