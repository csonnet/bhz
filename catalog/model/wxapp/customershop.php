<?php
class ModelWxappCustomerShop extends Model {

	// //获取附近商铺
	// public function getShopInfo(){
	// 	// echo 111;
	// 	$sql = "SELECT COUNT(*) as productnum,shop_id FROM `shop_inventory` GROUP BY shop_id ";
	// 	$query = $this->db->query($sql);
	// 	foreach ($query->rows as $key => $value) {
	// 		$shop_id = $value['shop_id'];
	// 		$temp1[$shop_id] = array(
	// 			'shop_id'=>$value['shop_id'],
	// 			'productnum'=>$value['productnum'],

	// 		);
	// 	}

	// 	$sql = "SELECT COUNT(*) as visitnum,shop_id FROM `shop_activity` GROUP BY shop_id";
	// 	$query = $this->db->query($sql);
	// 	foreach ($query->rows as $key => $value) {
	// 		$shop_id = $value['shop_id'];
	// 		$temp2[$shop_id] = array(
	// 			'shop_id'=>$value['shop_id'],
	// 			'visitnum'=>$value['visitnum'],

	// 		);
	// 	}


	// 	$sql = "SELECT DISTINCT ci.shop_id, ci.type, u.`name`, u.filename FROM customer_images AS ci LEFT JOIN upload AS u ON u.`customer_images_id` = ci.`customer_images_id` where ci.type=1 OR ci.type=3 ORDER BY ci.type DESC ";
	// 	$query = $this->db->query($sql);
	// 	foreach ($query->rows as $key => $value) {
	// 		$shop_id = $value['shop_id'];
	// 		// /tmp_c832a625bc1e0779534b8af7e37bcc5f95957a96970e530d.jpg
	// 		$temp5[$shop_id] = array(
	// 			'shop_id'=>$value['shop_id'],
	// 			'image'=>'http://m.bhz360.com/system/storage/upload/'.$value['filename'],

	// 		);
	// 		// var_dump($temp5[$shop_id]);die();
	// 	}
	// 	$sql = "SELECT COUNT(*) as customer_ordernum,shop_id FROM `shop_order` GROUP BY shop_id";
	// 	$query = $this->db->query($sql);
	// 	foreach ($query->rows as $key => $value) {
	// 		$shop_id = $value['shop_id'];
	// 		$temp3[$shop_id] = array(
	// 			'shop_id'=>$value['shop_id'],
	// 			'customer_ordernum'=>$value['customer_ordernum'],

	// 		);
	// 	}


	// 	$sql = "SELECT c.shop_id,c.customer_id, c.lat, c.lng, c.city_id, (SELECT z.`name` FROM zone AS z WHERE z.zone_id = c.zone_id ) AS zone, c.zone_id, (SELECT co.`name` FROM country AS co WHERE co.country_id = c.country_id ) AS country, c.country_id, (SELECT ci.`name` FROM city AS ci WHERE ci.city_id = c.city_id ) AS city, c.address, c.shelf_num, c.shop_describe, c.discount_message, c.shop_name, c.telephone,c.date_added,c.level,c.status FROM shop AS c WHERE c.lat != 0";
	// 	$query = $this->db->query($sql);
	// 	foreach ($query->rows as $key => $value) {
	// 		$shop_id = $value['shop_id'];
	// 		// var_dump($value);
	// 		$customerinfo[$shop_id] = array(
	// 			'customer_id'=>$value['customer_id'],
	// 			'shop_id'=>$value['shop_id'],
	// 			'lat'=>$value['lat'],
	// 			'lng'=>$value['lng'],
	// 			'telephone'=>$value['telephone'],
	// 			'date_added'=>$value['date_added'],
	// 			'level'=>$value['level'],
	// 			'status'=>$value['status'],
	// 			'zone'=>$value['zone'],
	// 			'zone_id'=>$value['zone_id'],
	// 			'country'=>$value['country'],
	// 			'country_id'=>$value['country_id'],
	// 			'city'=>$value['city'],
	// 			'city_id'=>$value['city_id'],
	// 			'address'=>$value['address'],
	// 			'shelf_num'=>1,
	// 			'shop_describe'=>$value['shop_describe'],
	// 			'discount_message'=>$value['discount_message'],
	// 			'shop_name'=>$value['shop_name'],
	// 			'productnum'=>$temp1[$shop_id]['productnum'],
	// 			'visitnum'=>$temp2[$shop_id]['visitnum'],
	// 			'customer_ordernum'=>$temp3[$shop_id]['customer_ordernum'],
	// 			'image'=>$temp5[$shop_id]['image'],

	// 		);
	// 		// var_dump($customerinfo);die();

	// 	}
	// 	// var_dump($customerinfo);die();
	// 	// die();
	// 	return $customerinfo;

	// }


		//获取附近商铺
	public function getShopInfo($filter){

		$lat = $filter['lat'];
		$lng = $filter['lng'];
		$sort = $filter['sort'];
		$sortway = $filter['sortway'];
		$search = $filter['search'];

		if($sort && $sortway){
			$orderby = " ORDER BY ".$sort." ".$sortway;
		}
		else{
			$orderby = "";
		}
		
		$distance = "acos(cos(".$lat."*pi()/180 )*cos(s.lat*pi()/180)*cos(".$lng."*pi()/180 -s.lng*pi()/180)+sin(".$lat."*pi()/180 )*sin(s.lat*pi()/180))*6370996.81/1000"; //计算距离

		$where = "s.lat != 0 AND s.status = 1 ";
		$where .= "AND s.shop_name LIKE '%".$search."%'";
		$sql = "SELECT 
		convert(".$distance.",decimal(11,2))  as distance, 
		s.*, 
		count(sa.shop_id) as visitnum
		FROM shop s 
		LEFT JOIN shop_concern sa on s.shop_id = sa.shop_id 
		WHERE ".$where." 
		GROUP BY s.shop_id";
		$sql .= $orderby." LIMIT 0,20";
		// echo $sql;
		$this->load->model('tool/image');


		$query = $this->db->query($sql);
		foreach ($query->rows as &$v) {

			if($v['distance'] > 40){
				$v['distance'] = ">40";
			}

			//获取货架
			$shelfInfo = M('shelf')->where(array('shop_id'=>$v['shop_id'],'status'=>1))->Field('shelf_image,shelf_id')->find();
			$shelfimage = "http://m.bhz360.com/system/storage/upload/".$shelfInfo['shelf_image'];

	        $sql  = "SELECT si.shop_inventory_id, si.product_id, si.product_code, p.image FROM shop_inventory AS si LEFT JOIN product AS p ON si.product_id = p.product_id WHERE si.shop_id = ".$v['shop_id']." LIMIT 0,3";
	        $query = $this->db->query($sql);
	        foreach ($query->rows as $shopproduct) {
	        	if ($shopproduct['image'] != null && file_exists(DIR_IMAGE . $shopproduct['image'])) {
	                $image = $this->model_tool_image->resize($shopproduct['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            } else {
	                $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
	            }
				$products[] = array(

		        	'shop_inventory_id'=>$shopproduct['shop_inventory_id'],
		        	'product_id'=>$shopproduct['product_id'],
		        	'product_code'=>$shopproduct['product_code'],
		        	'image'=>$image,
		        );
			}
	     	// var_dump($products);die();
	        
			$shopinfo[] = array(
				'image'=>$this->getShopLogo($v['shop_id']),
				'distance'=>$v['distance'],
				'shop_id'=>$v['shop_id'],
				'shop_name'=>$v['shop_name'],
				'customer_id'=>$v['customer_id'],
				'shop_describe'=>$v['shop_describe'],
				'discount_message'=>$v['discount_message'],
				'level'=>$v['level'],
				'address'=>$v['address'],
				'country_id'=>$v['country_id'],
				'country'=>$v['country'],
				'zone_id'=>$v['zone_id'],
				'zone'=>$v['zone'],
				'city_id'=>$v['city_id'],
				'city'=>$v['city'],
				'status'=>$v['status'],
				'date_added'=>$v['date_added'],
				'telephone'=>$v['telephone'],
				
				'shelf_num'=>$v['shelf_num'],
				'now_shelf_id'=>$shelfInfo['shelf_id'],
				'shelfimage'=>$shelfimage,
				'products'=>$products,
				'visitnum'=>$v['visitnum'],
			);

		}

		return $shopinfo;
		 
	}

		//获取商铺logo
	public function getShopLogo($shop_id){
		$sql = "SELECT u.filename FROM customer_images ci LEFT JOIN upload u ON ci.`customer_images_id` = u.`customer_images_id` where (ci.type=1 OR ci.type=3) AND ci.shop_id=".$shop_id." ORDER BY ci.type DESC LIMIT 0,1";
		$result = $this->db->query($sql)->row;
		$image = 'http://m.bhz360.com/system/storage/upload/'.$result['filename'];
		return $image;
	}
	
	//根据id获取商铺信息
	public function getShopInfoById($shop_id){
		// echo 111;
		$sql = "SELECT COUNT(*) as productnum,shop_id FROM `shop_inventory` where shop_id =".$shop_id;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			$shop_id = $value['shop_id'];
			$temp1[$shop_id] = array(
				// 'shop_id'=>$value['shop_id'],
				'productnum'=>(int)$value['productnum'],

			);
		}

		$sql = "SELECT COUNT(*) as visitnum,shop_id FROM `shop_activity` where shop_id =".$shop_id;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			// $shop_id = $value['shop_id'];
			$temp2[$shop_id] = array(
				// 'shop_id'=>$value['shop_id'],
				'visitnum'=>(int)$value['visitnum'],

			);
		}


		$sql = "SELECT  ci.shop_id, ci.type, u.`name`, u.filename FROM customer_images AS ci LEFT JOIN upload AS u ON u.`customer_images_id` = ci.`customer_images_id` where ci.shop_id=".$shop_id;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			$type = $value['type'];

			$temp5[$shop_id]['images'][$type][] = array(
				'shop_id'=>$value['shop_id'],
				'image'=>'http://m.bhz360.com/system/storage/upload/'.$value['filename'],

			);
			// var_dump($temp5[$shop_id]);die();
		}
		$sql = "SELECT COUNT(*) as customer_ordernum,shop_id FROM `shop_order` where shop_id =".$shop_id;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			// $shop_id = $value['shop_id'];
			$temp3[$shop_id] = array(
				'shop_id'=>$value['shop_id'],
				'customer_ordernum'=>(int)$value['customer_ordernum'],

			);
		}


		$sql = "SELECT c.shop_id, c.lat, c.lng, c.city_id, (SELECT z.`name` FROM zone AS z WHERE z.zone_id = c.zone_id ) AS zone, c.zone_id, (SELECT co.`name` FROM country AS co WHERE co.country_id = c.country_id ) AS country, c.country_id, (SELECT ci.`name` FROM city AS ci WHERE ci.city_id = c.city_id ) AS city, c.address, c.shelf_num, c.shop_describe, c.discount_message, c.shop_name,c.telephone,c.date_added,c.level,c.status FROM shop AS c WHERE shop_id =".$shop_id;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			// $shop_id = $value['shop_id'];

			// if ($temp1[$shop_id]['productnum']==0) {
			// 	continue;
			// }
			// var_dump($value);
			$customerinfo[$shop_id] = array(
				'shop_id'=>$value['shop_id'],
				'lat'=>$value['lat'],
				'lng'=>$value['lng'],
				'telephone'=>$value['telephone'],
				'date_added'=>$value['date_added'],
				'level'=>$value['level'],
				'status'=>$value['status'],
				'zone'=>$value['zone'],
				'zone_id'=>$value['zone_id'],
				'country'=>$value['country'],
				'country_id'=>$value['country_id'],
				'city'=>$value['city'],
				'city_id'=>$value['city_id'],
				'address'=>$value['address'],
				'shelf_num'=>1,
				'shop_describe'=>$value['shop_describe'],
				'discount_message'=>$value['discount_message'],
				'shop_name'=>$value['shop_name'],
				'productnum'=>$temp1[$shop_id]['productnum'],
				'visitnum'=>$temp2[$shop_id]['visitnum'],
				'customer_ordernum'=>$temp3[$shop_id]['customer_ordernum'],
				'images'=>$temp5[$shop_id]['images'],

			);
			// var_dump($customerinfo);die();

		}
		// var_dump($customerinfo);die();
		// die();
		return $customerinfo;

	}

	public function getProduct($shop_id){
		$sql = "SELECT shi.*, (SELECT pd.`name` FROM `product_description` AS pd WHERE shi.product_id = pd.product_id ) AS 'name', p.model, p.image FROM shop_inventory AS shi LEFT JOIN product AS p ON shi.product_id = p.product_id where shop_id = ".$shop_id;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	//修改经纬度
	public function modlocation($data){
			$latitude=$data['latitude'];
			$longitude=$data['longitude'];
			$categoryfullname=$data['categoryfullname'];
			$end=$this->db->query("UPDATE " . DB_PREFIX . "customer SET  lat= '" .$latitude. "',lng='".$longitude."' WHERE fullname = '".$categoryfullname."'");
			$shop=$this->db->query("UPDATE " . DB_PREFIX . "shop SET  lat= '" .$latitude. "',lng='".$longitude."' WHERE telephone = '".$categoryfullname."'");
			return $end;
	}

	

	public function getinfo($customerInShopId,$shop_id){
		
		return M('shop_concern')->where(array('customer_in_shop_id'=>$customerInShopId,'shop_id'=>$shop_id))->find();
	}
	
	public function concern($data){
		
		return M('shop_concern')->add($data);
	}

	

	public function upstatus($customerInShopId,$shop_id,$status){
		
		return M('shop_concern')->where(array('customer_in_shop_id'=>$customerInShopId,'shop_id'=>$shop_id))->save(array('status'=>$status));
	}
}
