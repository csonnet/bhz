<?php
class ModelWxappPoint extends Model {

  //查业务员所绑定的店铺信息
  public function pointinfo($user_id){

    $where['sm.user_id'] = $code;
    $category = M('customer')
      ->alias('sm')
      ->where($where)->getField('s.shelf_category',1);

    $url = "/pages/shop_customer/categoryKnowledge?category=".$category;

    return $url;

  }

  public function latlong($latlong){
      $sql=$this->db->query("SELECT shop_id,lng,lat  FROM shop WHERE lng<".$latlong['maxlng']." AND lng>".$latlong['minlng']." AND lat<".$latlong['maxlat']." AND lat>".$latlong['minlat']);
      return $sql->rows;
  }

  public function getShopProductById($sku){
    $sql = "SELECT p.sale_price, p.image, p.product_code,p.sku,p.product_id,pd.`name`,pd.description   FROM product AS p LEFT JOIN product_description AS pd ON pd.product_id = p.product_id WHERE p.sku = ".$sku;
    $query = $this->db->query($sql);
    return $query->rows;
  }
  public function getImages($product_id){
    $sql = "SELECT product_image.image, product_image.product_id FROM `product_image` WHERE product_image.product_id =".$product_id;
    // echo $sql;
    $query = $this->db->query($sql);
    return $query->rows;
  }
  //获取该方案下的所有货架信息
  public function getshelfinfo($data){
    $sql="SELECT so.solution_name, so.shelf_length,sh.shelf_name, pd.`name`, sh.grade_start_search, sh.grade_end_search, sp.product_code, sp.shelf_layer, sp.layer_position, sp.show_qty, sp.min_order_qty,p.sku,p.sale_price,sp.product_id,cd.meta_title
      FROM solution AS so
      LEFT JOIN shelf AS sh ON so.solution_id = sh.solution_id
      LEFT JOIN shelf_product AS sp ON sh.shelf_id = sp.shelf_id
      LEFT JOIN product_description AS pd ON sp.product_id = pd.product_id
      LEFT JOIN category_description AS cd ON cd.category_id=sh.shelf_category
      LEFT JOIN product AS p ON p.product_id = pd.product_id";
      if($data==1){
          $sql.=" WHERE so.is_template =1";
      }else{
          $sql.=" WHERE so.solution_id =".$data;
      }

    $query = $this->db->query($sql);
    $query=$query->rows;
    $special="SELECT * FROM product_special AS ps  where ps.date_start< now() AND ps.date_end > now() AND ps.customer_group_id=1";
    $que = $this->db->query($special);
    $que=$que->rows;
    //var_dump($que);
     foreach ($query as $key => $value) {
        foreach ($que as $k => $v){
          if($value['product_id']==$v['product_id']){
            $query[$key]['sale_price']=$que[$k]['price'];
          }
        }
     }
     return $query;
  }

  public function getshelf($data){
    $sql="SELECT shop.shop_name,(SELECT count(*) FROM " . DB_PREFIX . "shelf  WHERE shop_id='" . (int)$data. "') AS shelf_length, sh.shelf_name, pd.`name`, sh.grade_start_search, sh.grade_end_search, sp.product_code, sp.shelf_layer, sp.layer_position, sp.show_qty, sp.min_order_qty,p.sku,p.sale_price,sp.product_id,cd.meta_title
      FROM shop
      LEFT JOIN shelf AS sh ON sh.shop_id = shop.shop_id
      LEFT JOIN shelf_product AS sp ON sh.shelf_id = sp.shelf_id
      LEFT JOIN product_description AS pd ON sp.product_id = pd.product_id
      LEFT JOIN category_description AS cd ON cd.category_id=sh.shelf_category
      LEFT JOIN product AS p ON p.product_id = pd.product_id
      WHERE sh.shop_id =".$data;
    $query = $this->db->query($sql);
    $query=$query->rows;
    $special="SELECT * FROM product_special AS ps  where ps.date_start< now() AND ps.date_end > now() AND ps.customer_group_id=1";
    $que = $this->db->query($special);
    //var_dump($query);
    $que=$que->rows;
    //var_dump($que);
     foreach ($query as $key => $value) {
        foreach ($que as $k => $v){
          if($value['product_id']==$v['product_id']){
            $query[$key]['sale_price']=$que[$k]['price'];
             //$query[$key]['shelf_length']=$num;
          }
        }
     }
     return $query;
  }
  //获取勾选货架信息
   public function getcheckshelf($data){
      $shelf_ids=explode(',', $data);
      foreach ($shelf_ids as $key => $value) {
          $sql="SELECT shop.shop_name,(SELECT count(*) FROM " . DB_PREFIX . "shelf  WHERE shop_id='" . (int)$data. "') AS shelf_length, sh.shelf_name, pd.`name`, sh.grade_start_search, sh.grade_end_search, sp.product_code, sp.shelf_layer, sp.layer_position, sp.show_qty, sp.min_order_qty,p.sku,p.sale_price,sp.product_id,cd.meta_title
            FROM shop
            LEFT JOIN shelf AS sh ON sh.shop_id = shop.shop_id
            LEFT JOIN shelf_product AS sp ON sh.shelf_id = sp.shelf_id
            LEFT JOIN product_description AS pd ON sp.product_id = pd.product_id
            LEFT JOIN category_description AS cd ON cd.category_id=sh.shelf_category
            LEFT JOIN product AS p ON p.product_id = pd.product_id
            WHERE sh.shelf_id =".$value;
          $query = $this->db->query($sql);
           $arr[]=$query->rows;

      }

      foreach ($arr as $key => $value) {
         foreach($value as $k =>$v){
              $newarr[]=$v;
         }
      }
     // var_dump($arr);die;
     return $newarr;
  }
  //修改方案所属用户修改货架所属用户
  public function modification($data){
    $condition['fullname'] = $data['phone'];
    $list = M('customer')->where($condition)->getField('customer_id');
    if(!empty($list)){
      $id="solution_id=".$data['solution_id'];
      $data['customer_id']=$list;
      $shelf['customer_id']=$list;
      $shelf['status']=1;
      $solution= M('solution')->where($id)->save($data);
      $shelf= M('shelf')->where($id)->save($shelf);
      if($solution&&$shelf){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  //购物清单
  public function cart($shop_id){
      $sql=$this->db->query("SELECT sp.* from shelf as s LEFT JOIN shelf_product as sp ON sp.shelf_id=s.shelf_id where s.status=1 and  s.shop_id=".$shop_id." and sp.stock_qty>0")->rows;
      return $sql;
  }
  //搜索清单
  public function searchrepair($shop_id,$searchInput){
       $sql=$this->db->query("SELECT sp.shelf_product_id,sp.shelf_id,sp.product_id,sp.product_code,sp.stock_qty,sp.shop_id  from shelf as s LEFT JOIN shelf_product as sp ON sp.shelf_id=s.shelf_id LEFT JOIN product as p ON sp.product_id=p.product_id LEFT JOIN product_description as pd ON pd.product_id=p.product_id where s.status=1 AND s.shop_id=".$shop_id." AND sp.stock_qty=0 AND (pd.name like '%".$searchInput."%' OR p.sku like '%".$searchInput."%') limit 0,10")->rows;
         return $sql;
  }
  //获取模板方案信息
  public function getresult(){
      $solution = $this->db->query("SELECT solution_id,solution_name,solution_category,grade_start_search,grade_end_search  FROM  solution where is_template=1")->rows;
      foreach ($solution as $key => $value) {
          $solution[$key]['category_id'] = trim($value['solution_category'],',');
          $solution[$key]['image'] = $this->db->query("SELECT image FROM  category where category_id=".trim($value['solution_category'],','))->row['image'];
      }
      return $solution;
  }

 //获取商品信息
  public function getProductinfo($shelf_id){
    $sql = "SELECT o.name,sum(o.quantity) as num,sum(o.pay_total) as price,s.shelf_name,c.meta_title  FROM `order_product`o  LEFT JOIN `shelf` s ON s.shelf_id = o.shelf_id LEFT JOIN `category_description` c ON c.category_id = s.shelf_category WHERE o.shelf_id=".$shelf_id." group by o.product_code order by num desc  limit 20";
    $query = $this->db->query($sql);
    return $query;
  }
 public function getProductallprice($shelf_id){
    $sql = "SELECT sum(o.quantity) as num,sum(o.pay_total) as price  FROM `order_product`o  LEFT JOIN `shelf` s ON s.shelf_id = o.shelf_id LEFT JOIN `category_description` c ON c.category_id = s.shelf_category WHERE o.shelf_id=".$shelf_id." group by o.product_code order by num desc";
    $query = $this->db->query($sql)->rows;
    return $query;
  }

}

