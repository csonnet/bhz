<?php
class ModelMessageMessage extends Model {
	
	//获取我的消息
	public function getMyMessage($customer_id){

		$where['n.customer_id'] = $customer_id;
		$where['n.is_del'] = 0;
		
		$m = M('notice')
			->alias('n')
			->join('notice_description nd on n.notice_id = nd.notice_id','left')
			->join('notice_type nt on nd.type = nt.id','left')
			->where($where)
			->field('n.id as send_id,n.*,nd.*,nt.*')
			->order('n.date_added desc')
			->select();

		return $m;

	}
	
	//获取消息详细
	public function getMessageInfo($id){

		$where['n.id'] = $id;
		$m = M('notice')
			->alias('n')
			->join('notice_description nd on n.notice_id = nd.notice_id','left')
			->where($where)
			->select();

		return $m;
		
	}
	
	//获取我的未读消息数量
	public function noReadCount($customer_id){

		$where['customer_id'] = $customer_id;
		$where['is_del'] = 0;
		$where['read_time'] = array('exp','is null');
		$count = M('notice')->where($where)->count();

		return $count;

	}
	
	//已读
	public function hasRead($filter){

		$where['id'] = $filter['id'];
		$save['read_time'] = date('Y-m-d H:i:s');
		M('notice')->where($where)->save($save);

	}
	
	//提醒停用的商品有库存
	public function hasGoods(){
		
		$where['p.status'] = 0;
		$where['i.available_quantity'] = array('neq',0);
		$results = M('product')
			->alias('p')
			->join('product_option_value pov on p.product_id = pov.product_id','left')
			->join('product_description pd on p.product_id = pd.product_id','left')
			->join('inventory i on pov.product_code = i.product_code','left')
			->join('warehouse w on i.warehouse_id = w.warehouse_id','left')
			->where($where)
			->field('pd.name as pname,pov.product_code,w.name as wname,i.available_quantity as qty')
			->select();

		echo M('product')->getLastSql();

	}

}
?>