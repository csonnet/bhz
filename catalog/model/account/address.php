<?php
class ModelAccountAddress extends Model {
	public function addAddress($data) {
		$this->event->trigger('pre.customer.add.address', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET need_review = '1', customer_id = '" . (int)$this->customer->getId() . "', fullname = '" . $this->db->escape($data['fullname']) . "', company = '" . $this->db->escape($data['company']) . "', address = '" . $this->db->escape($data['address']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city_id = '" . $this->db->escape($data['city_id']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "'");

		$address_id = $this->db->getLastId();
		
		//edit mcc
		$total_address = $this->getTotalAddresses();
		
		if($total_address == 1) {
			
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
			
		}else{

			if (!empty($data['default'])) {
			    $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
			}
		
		}
		//end mcc

		$this->event->trigger('post.customer.add.address', $address_id);

		return $address_id;
	}

	public function closeZone($address){
		$this->load->model('account/customer');
		$json['success'] = "false";
		$where['zone_id'] = $address['zone_id'];
		$zonefind = M('zone');
		$zonename = $zonefind
		->where($where)
		->find();

        if(isset($zonename['zone_id'])){
	        $lng = $zonename['lng'];
	        $lat = $zonename['lat'];
	        $min = pow(180,3);
			$logcenters = M('logcenters');
			$data = $logcenters
			->select();
			
			foreach ($data as $result) {
				$now = pow($result['lat']-$lat,2) + pow($result['lng']-$lng,2);
				if($now < $min){
					$min = $now;
					$closelogcenter = $result['logcenter_id']; 
				}

			}
			$savedata['logcenter_id']=$closelogcenter;
			$ad = M('customer');
			$flag = $this->model_account_customer->getCustomer($this->customer->getId());
			if(empty($flag['logcenter_id']) || $flag['logcenter_id']== 0 ){
				$save = $ad
				->where("customer_id = '" . (int)$this->customer->getId()."'")
				->data($savedata)
				->save();
			}	
			$json['success'] = "success";
			$json['result'] = $closelogcenter;

        }
        
		return $json;
	}

	public function editAddress($address_id, $data) {
		$this->event->trigger('pre.customer.edit.address', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "address SET need_review = '1', fullname = '" . $this->db->escape($data['fullname']) . "', company = '" . $this->db->escape($data['company']) . "', address = '" . $this->db->escape($data['address']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city_id = '" . $this->db->escape($data['city_id']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}

		$this->event->trigger('post.customer.edit.address', $address_id);
	}

	public function deleteAddress($address_id) {
		$this->event->trigger('pre.customer.delete.address', $address_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.delete.address', $address_id);
	}

	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}
			//jeep  city查询
			$city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$address_query->row['city_id'] . "'");
			if ($city_query->num_rows) {
				$city = $city_query->row['name'];
			} else {
				$city = '';
			}

			//Liqn 添加is_default
			$is_default = '0';
			$customer_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");
			if($customer_query->num_rows) {
				$is_default = '1';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'fullname'      => $address_query->row['fullname'],
				'company'        => $address_query->row['company'],
				'address'      => $address_query->row['address'],
				'postcode'       => $address_query->row['postcode'],
				'shipping_telephone'       => $address_query->row['shipping_telephone'],
				'city'           => $city,
				'city_id'        => $address_query->row['city_id'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'is_default' => $is_default,
				'custom_field'   => json_decode($address_query->row['custom_field'], true)
			);

			return $address_data;
		} else {
			return false;
		}
	}

	public function getDefaultAddressId() {
		//Liqn 添加is_default
		$default_address_id = 0;
		$customer_query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		if($customer_query->num_rows) {
			$default_address_id = $customer_query->row['address_id'];
		}
		return $default_address_id;
	}
	
	public function getIsBuy() {
		//Liqn 添加is_default
		$default_address_id = 0;
		$customer_query = $this->db->query("SELECT is_buy FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		if($customer_query->num_rows) {
			$is_buy = $customer_query->row['is_buy'];
		}
		return $is_buy;
	}
	public function getAddresses($if_need_review = 1) {
		$address_data = array();
		if($if_need_review){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "' AND need_review = '0' ");
		}
		
		//Liqn 添加is_default
		$default_address_id = 0;
		$customer_query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		if($customer_query->num_rows) {
			$default_address_id = $customer_query->row['address_id'];
		}

		foreach ($query->rows as $result) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}
        
        //jeep  city查询
			$city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city_id = '" . (int)$result['city_id'] . "'");

			if ($city_query->num_rows) {
				$city = $city_query->row['name'];

				
			} else {
				$city = '';

				
			}
            
			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'fullname'      => $result['fullname'],
				'company'        => $result['company'],
				'address'      => $result['address'],
				'postcode'       => $result['postcode'],
				'shipping_telephone'       => $result['shipping_telephone'],
				'city'           => $city,
				'city_id'        => $result['city_id'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $result['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'is_default'  => $default_address_id==$result['address_id']?'1':'0',
				'custom_field'   => json_decode($result['custom_field'], true)

			);
		}

		return $address_data;
	}

	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}