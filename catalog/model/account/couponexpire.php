<?php
class ModelAccountCouponExpire extends Model {
	
	//获取即将N天到期用户
	public function getCouponUser($day){
	
		$sql = "SELECT DISTINCT c.fullname as name,c.telephone,cp.discount,cp.date_end FROM " . DB_PREFIX . "customer c ";
		$sql.="LEFT JOIN " . DB_PREFIX . "customer_coupon cc ON c.customer_id = cc.customer_id ";
		$sql.="LEFT JOIN " . DB_PREFIX . "coupon cp ON cc.coupon_id = cp.coupon_id ";
		$sql.="WHERE TO_DAYS(cp.date_end) - TO_DAYS(now()) = ".$day." AND cp.status = 1 GROUP BY c.telephone ";
		
		$query = $this->db->query($sql);
		return $query->rows;

	}

}