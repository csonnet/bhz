<?php
class ModelAccountUser extends Model {

	public function getUser($filter){
		
		$user = M('user')->where($filter)->getField('user_id');

		return $user;

	}

    public function getDefaultLogcenter($userId) {
        $salesArea = M('user')->where(array('user_id'=>$userId))->getField('sales_area_id');
        return intval(M('sales_area')->where(array('sales_area_id'=>$salesArea))->getField('default_logcenters'));
    }

}
?>