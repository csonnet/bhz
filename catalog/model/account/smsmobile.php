<?php
class ModelAccountSmsMobile extends Model {

	public function addSmsMobile($telephone, $verify_code) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "sms_mobile SET sms_mobile = '" . $telephone . "', verify_code = '" . $verify_code . "'");
		
	}
	
	public function editSmsMobile($sms_mobile_id, $telephone, $verify_code) {
		$this->db->query("UPDATE " . DB_PREFIX . "sms_mobile SET sms_mobile = '" . $telephone . "', verify_code = '" . $verify_code . "' WHERE sms_mobile_id = '" . $sms_mobile_id . "'");

	}
	
	public function deleteSmsMobile($sms_mobile) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "sms_mobile WHERE sms_mobile = '" . $sms_mobile . "'");
	}
	
	public function getIdBySmsMobile($sms_mobile) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sms_mobile WHERE sms_mobile = '" . $sms_mobile . "'");
		

		return $query->row;
	}
	
	public function verifySmsCode($sms_mobile, $sms_code) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sms_mobile WHERE sms_mobile = '" . $sms_mobile . "' AND verify_code = '" . $sms_code . "'");

		return $query->row['total'];
	}

	/**
	 * [checkSendCodeNum description]
	 * @return true 停止发送
	 * @user ypj
	 * @date 2018.4.20
	 */
	public function checkSendCodeNum($telephone){

		$codeNum = array();

		$beginTime = date('Y-m-d',time());
		$currentTime = date('Y-m-d H:i:s',time());
		$sql = " SELECT COUNT(sms_log_id) AS num from sms_log where date_added BETWEEN '".$beginTime."' AND '".$currentTime."' AND telphone = ".$telephone." AND return_status = 1";

		$sql2 = " SELECT COUNT(sms_log_id) AS total from sms_log where date_added BETWEEN '".$beginTime."' AND '".$currentTime."' AND return_status = 1";
		//同一手机号验证码同一天数量
		$num = $this->db->query($sql);
		// 当天验证码发送总数量
		$total = $this->db->query($sql2);
		$codeNum['num'] = $num->row['num'];
		$codeNum['total'] = $total->row['total'];
		if($codeNum['num'] > 1 || $codeNum['total'] > 99 ){
			return true; 
		} else {
			return false; 
		}
	}
}