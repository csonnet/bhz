<?php
class ModelCrontabCallMessage extends Model {
	
	//获得未读消息用户
	public function getNoRead(){
		
		//获取内部人员
		$temp['u.contact_tel'] = array('exp','is not null');
		$customer = M('customer')
			->alias('c')
			->join('user u on c.telephone =u.contact_tel','left')
			->where($temp)
			->field('c.customer_id')->select();

		foreach($customer as $c){
			$cid[] = $c['customer_id'];
		}
		
		//获取发送对象
		$where['n.read_time'] = array('exp','is null');
		$where['n.customer_id'] = array('in',$cid);
		$customer = M('notice')
			->alias('n')
			->join('customer c on n.customer_id = c.customer_id','left')
			->where($where)
			->group('n.customer_id')
			->field('c.telephone,count(*) as count')->select();

		return $customer;

	}

}
?>