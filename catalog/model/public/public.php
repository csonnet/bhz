<?php
class ModelPublicPublic extends Model {
	
	//获取用户
	public function getUser(){
		$customer_id = $this->customer->getId();
		$where['contact_tel'] = M('customer')->where('customer_id='.$customer_id)->getField('telephone');
		$user = M('user')->where($where)->select();		
		return $user;
	}
	
	//获取全部仓库
	public function getWarehouses(){	
		$warehouses = M('warehouse')->select();
		return $warehouses;
	}

	//获取全部物流中心
	public function getLogcenters(){	
		$logcenters = M('logcenters')->select();
		return $logcenters;
	}

	//获取对应用户所有订单id
		 public function getorderProductid($userid) {
	    $order_id=$this->db->query("SELECT order_id  FROM `" . DB_PREFIX . "order` WHERE customer_id = '" . (int)$userid . "'AND to_days(now())-to_days(date_added) < 90 AND order_status_id!=0 AND order_status_id!=16 AND order_status_id!=11 AND order_status_id!=13");
	       return $order_id->rows;
	  }
	  //获取详细订单id
	  public function getgropcode($orderids){
	  		$gropcode=$this->db->query("SELECT  order_product_id  FROM `".DB_PREFIX."order_product`  WHERE  order_id IN (".$orderids.") ") ;
	  		return $gropcode->rows;
	  }
	  //获取商品热销排名
		  public function gethotproducts($orderids,$productcodes){
		  	$sql=$this->db->query("SELECT * from (SELECT od.name AS type,op.name,sum(op.quantity) AS nub FROM `".DB_PREFIX."order_product` op LEFT JOIN `".DB_PREFIX."product_option_value` pov ON pov.product_id = op.product_id  LEFT JOIN `".DB_PREFIX."option` o ON o.option_id = pov.option_id LEFT JOIN `".DB_PREFIX."option_description` od ON od.option_id = pov.option_id LEFT JOIN `".DB_PREFIX."option_value_description` ovd ON ovd.option_value_id = pov.option_value_id  WHERE  order_id IN (".$orderids.")  group by op.product_code  UNION ALL SELECT optd.name AS type,ord.name,sum(gr.quantity) AS nub FROM `".DB_PREFIX."order_product_group` gr LEFT JOIN `".DB_PREFIX."order_product` ord  ON ord.order_product_id = gr.order_product_id LEFT JOIN `".DB_PREFIX."product_option_value` pro ON pro.product_id = gr.product_id  LEFT JOIN `".DB_PREFIX."option` opt ON opt.option_id = pro.option_id LEFT JOIN `".DB_PREFIX."option_description` optd ON optd.option_id = pro.option_id LEFT JOIN `".DB_PREFIX."option_value_description` opvd ON opvd.option_value_id = pro.option_value_id  WHERE  gr.order_product_id IN (".$productcodes.")  group by gr.order_product_id ) AS temporary ORDER BY nub DESC limit 10 ");
		      return $sql->rows;
	  }

}
?>