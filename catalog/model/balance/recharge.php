<?php
class ModelBalanceRecharge extends Model {
	
	//验证用户账号是否存在
	public function checkAccount($telephone){
		
		$where['telephone'] = $telephone;
		$account = M('customer')->where($where)->getField('customer_id');

		return $account;

	}
	
	//生成充值单据
	public function addRecharge($data){
		
		$data['date_added'] = date('Y-m-d H:i:s');
		$rechargeid = M('balance_recharge')->data($data)->add();
		
		return $rechargeid;

	}
	
	//修改充值单据
	public function editRecharge($id,$data){
		
		$where['recharge_id'] = $id;
		M('balance_recharge')->where($where)->save($data);

		return $id;

	}
	
	//查询单个充值单据
	public function getRecharge($recharge_id){
		
		$where['recharge_id'] = $recharge_id;
		$recharge = M('balance_recharge')->where($where)->select();
		return $recharge[0];

	}
	
	//获取充值用户信息
	public function getUserInfo($customer_id){
		$model = M('customer');
		$where = array('customer_id' => $customer_id);
		$field = array('customer_id','yiji_userid','fullname');
		$query = $model->field($field)->where($where)->find();
		return $query;
	}
	
	//确认充值改变状态和加钱
	public function addBalance($recharge_id){

		$where['recharge_id'] = $recharge_id;
		$recharge = M('balance_recharge')->where($where)->select();
		$money = $recharge[0]['money'];
		$account = $recharge[0]['account'];
		$save['status'] = 3;
		
		$where_customer['telephone'] = $account;

		if($recharge[0]['balance_type'] == 1){
			M('customer')->where($where_customer)->setInc('balance',$money);
		}

		M('balance_recharge')->where($where)->save($save);

	}
	
	//修改充值单据
	public function editBalance($data){
		$where['recharge_id'] = $data['recharge_id'];
		$save['payment_method'] = $data['payment_method'];
		M('balance_recharge')->where($where)->save($save);
	}

	//作废充值单据
	public function destoryRecharge($recharge_id){
		
		$where['recharge_id'] = $recharge_id;
		$data['status'] = 4;
		M('balance_recharge')->where($where)->save($data);

	}

}
?>