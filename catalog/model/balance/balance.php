<?php
class ModelBalanceBalance extends Model {
	
	//查找用户余额
	public function getCustomerBalance($customer_id){

		$balance = M('customer')->where('customer_id='.$customer_id)->getField('balance');
		return $balance;

	}
	
	//查找用户充值单据
	public function getRechargeOrder($customer_id){

		//我的充值记录
		$m_where['runner_id'] = $customer_id;
		$m_where['balance_type'] = 1;

		$m_recharge = M('balance_recharge')
			->alias('br')
			->join('customer c on br.runner_id = c.customer_id','left')
			->join('balance_status bs on br.status = bs.id','left')
			->join('runner_type rt on br.runner_type = rt.id')
			->where($m_where)->order('date_added desc')->field('c.fullname as runner,bs.name as status_name,rt.name as rtype,br.*')->select();
		
		//别的会员给我的充值记录
		$account = M('customer')->where('customer_id='.$customer_id)->getField('telephone');
		$oc_where['runner_id'] =array('neq',$customer_id);
		$oc_where['account'] = $account;
		$oc_where['runner_type'] = 1;
		$oc_where['balance_type'] = 1;

		$oc_recharge = M('balance_recharge')
			->alias('br')
			->join('customer c on br.runner_id = c.customer_id','left')
			->join('balance_status bs on br.status = bs.id','left')
			->join('runner_type rt on br.runner_type = rt.id')
			->where($oc_where)->order('date_added desc')->field('c.fullname as runner,bs.name as status_name,rt.name as rtype,br.*')->select();

		//业务员给我的充值记录
		$ou_where['runner_id'] =array('neq',$customer_id);
		$ou_where['account'] = $account;
		$ou_where['runner_type'] = 2;
		$ou_where['balance_type'] = 1;

		$ou_recharge = M('balance_recharge')
			->alias('br')
			->join('user u on br.runner_id = u.user_id','left')
			->join('balance_status bs on br.status = bs.id','left')
			->join('runner_type rt on br.runner_type = rt.id')
			->where($ou_where)->order('date_added desc')->field('u.username as runner,bs.name as status_name,rt.name as rtype,br.*')->select();

		$recharge = array_merge($m_recharge,$oc_recharge,$ou_recharge);

		/*按照时间排序*/
		foreach($recharge as $key=>$v){
			$recharge[$key]['date_added'] = date('Y-m-d H:i:s',strtotime($v['date_added']));
		} 
		$datetime = array();
		foreach ($recharge as $v) {
			$datetime[] = $v['date_added'];
		}
		array_multisort($datetime,SORT_DESC,$recharge);
		/*按照时间排序*/

		return $recharge;

	}

	//查找用户定金单据
	public function getDepositOrder($customer_id){

		//我的定金记录
		$m_where['runner_id'] = $customer_id;
		$m_where['balance_type'] = 2;

		$m_deposit = M('balance_recharge')
			->alias('br')
			->join('customer c on br.runner_id = c.customer_id','left')
			->join('balance_status bs on br.status = bs.id','left')
			->join('balance_type bt on br.balance_type = bt.id')
			->where($m_where)->order('date_added desc')->field('c.fullname as runner,bs.name as status_name,bt.name as btype,br.*')->select();
		
		//别的会员给我的定金记录
		$account = M('customer')->where('customer_id='.$customer_id)->getField('telephone');
		$oc_where['runner_id'] =array('neq',$customer_id);
		$oc_where['account'] = $account;
		$oc_where['runner_type'] = 1;
		$oc_where['balance_type'] = 2;

		$oc_deposit = M('balance_recharge')
			->alias('br')
			->join('customer c on br.runner_id = c.customer_id','left')
			->join('balance_status bs on br.status = bs.id','left')
			->join('balance_type bt on br.balance_type = bt.id')
			->where($oc_where)->order('date_added desc')->field('c.fullname as runner,bs.name as status_name,bt.name as btype,br.*')->select();

		$deposit = array_merge($m_deposit,$oc_deposit);

		/*按照时间排序*/
		foreach($deposit as $key=>$v){
			$deposit[$key]['date_added'] = date('Y-m-d H:i:s',strtotime($v['date_added']));
		} 
		$datetime = array();
		foreach ($deposit as $v) {
			$datetime[] = $v['date_added'];
		}
		array_multisort($datetime,SORT_DESC,$deposit);
		/*按照时间排序*/

		return $deposit;

	}

	//查找用户余额支付订单单据
	public function getCustomerOrder($customer_id){
		
		$where['`order`.customer_id'] = $customer_id;
		$where['`order`.payment_code'] = 'balance';
		$order = M('order')
			->join('order_status os on `order`.order_status_id = os.order_status_id','left')
			->where($where)->order('`order`.date_added desc')->field('os.name as status_name,`order`.*')->select();

		return $order;

	}
	
	//订单扣除余额
	public function payBalance($customer_id,$money){
		
		$where['customer_id'] = $customer_id;
		M('customer')->where($where)->setDec('balance',$money);

	}

	public function updatemoney($res,$order_id){
        $model=M('money_in');
		$data = array(
			'received'=>$res,
			'status'=>2,
			'pay_time'=>date('Y-m-d H:i:s')
			);
 			$money_in_id =  $model->data($data)->where(array('refer_id'=>$order_id,'refer_type_id'=>1))->save();
	}

	public function getOrderById($orderID){
        $model=M('order');
        $where=array('order_id'=>$orderID,'is_pay'=>1);
        $res=$model->where($where)->getField('`order`.total');
        return $res;  
    }


}
?>