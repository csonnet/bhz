<?php
class ModelCheckoutAutoPurchase extends Model {

	//购买频次
    public function getOrderedNums() {

		$where['o.customer_id'] = $this->customer->getId();

		$products = M('order_product')
			->alias('op')
			->join('`order` o on op.order_id = o.order_id','left')
			->field('count(*) as count,op.product_id')
			->where($where)
			->group('op.product_id')
			->order('count desc')
			->select();

		return $products;

    }

	//购买时间
	public function getOrderedStamp(){

		$where['o.customer_id'] = $this->customer->getId();

		$products = M('order_product')
			->alias('op')
			->join('`order` o on op.order_id = o.order_id','left')
			->field('op.product_id as product_id,o.date_added as date_added')
			->where($where)
			->group('op.product_id')
			->order('o.date_added desc')
			->select();

		return $products;

	}

	//商城销量
	public function getSalesInMall(){

		$where['order_status_id'] = 5;
		$products = M('order_product')
			->field('sum(quantity) as quantity,product_id')
			->where($where)
			->group('product_id')
			->order('quantity desc')
			->select();

		return $products;

	}

	//新品推荐
	public function getNewArrival(){

		$where['is_single'] = 1;
		$where['status'] = 1;
		$newday = date("Y-m-d h:i:s",strtotime('- 60 day'));
		$time['date_added'] = array('egt',$newday);
		$time['newpro'] = 1;
		$time['_logic'] = "or";
		$where['_complex']=$time;
		$products = M('product')
			->where($where)
			->order('date_added desc')
			->field('product_id')
			->select();

		return $products;

	}

	//获取货架
	public function getShelf($filter){
        if ($filter['solution_id']) {
            $where[] = "SL.`solution_id`='".$filter['solution_id']."'";
            $sql = "
        SELECT
            CD.`category_id`,
            CD.name AS `cname`,
            CG.`image` AS `cimage`,
            SL.*,
            SF.*,
            sm.shelf_map_id
        FROM
            `solution` AS SL
            LEFT JOIN `shelf` AS SF ON SL.`solution_id`=SF.`solution_id`
            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`
        ";

        }else if($filter['shop_id']){
        		$where[] = "shop.`shop_id`='".$filter['shop_id']."'";
            $sql = "
			        SELECT
			            CD.`category_id`,
			            CD.name AS `cname`,
			            CG.`image` AS `cimage`,
			            shop.*,
			            SF.*,
			            sm.shelf_map_id
			        FROM
            `shop` AS shop
            LEFT JOIN `shelf` AS SF ON shop.`shop_id`=SF.`shop_id`
            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";

        }else if($filter['template']){
		        		 $where[] = "SL.`is_template`='1'";//仅显示模板
		            if ($filter['shelfType'] >= 0) {//货架规格
		                $where[] = "SL.`shelf_types` LIKE '%,".$filter['shelfType'].",%'";
		            }
		            if (count($filter['categoryList']) >= 0) {//货架分类
		                $temp = array();
		                foreach ($filter['categoryList'] as $k=>$info) {
		                    $temp[] = "(SL.`solution_category` LIKE ',".$info['categoryId'].",' AND SL.`shelf_length`='".$info['quantity']."')";
		                }
		                if (count($temp)) {
		                    $where[] = implode(' OR ', $temp);
		                }
		            }
		            $gradeSearch = 0;
		            if ($filter['shopType'] >= 0 && $filter['payGrade'] >= 0) {//货架档次
		                $gradeSearch = ($filter['shopType']+1)*($filter['payGrade']+1);
		            }else if ($filter['shopType'] >= 0) {
		                $gradeSearch = pow($filter['shopType']+1, 2);
		            }else if ($filter['payGrade'] >= 0) {
		                $gradeSearch = pow($filter['payGrade']+1, 2);
		            }
		            if ($gradeSearch) {
		                $where[] = "SL.`grade_start_search`<=".$gradeSearch." AND SL.`grade_end_search`>=".$gradeSearch;
		            }

		             $sql = "
		        SELECT
		            CD.`category_id`,
		            CD.name AS `cname`,
		            CG.`image` AS `cimage`,
		            SL.*,
		            SF.*,
		            sm.shelf_map_id
		        FROM
		            `solution` AS SL
		            LEFT JOIN `shelf` AS SF ON SL.`solution_id`=SF.`solution_id`
		            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
		            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
		            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";

        }else{

		        	$where[] = "SL.`is_template`='1'";//仅显示模板
		            if ($filter['shelfType'] >= 0) {//货架规格
		                $where[] = "SL.`shelf_types` LIKE '%,".$filter['shelfType'].",%'";
		            }
		            if (count($filter['categoryList']) >= 0) {//货架分类
		                $temp = array();
		                foreach ($filter['categoryList'] as $k=>$info) {
		                    $temp[] = "(SL.`solution_category` LIKE ',".$info['categoryId'].",' AND SL.`shelf_length`='".$info['quantity']."')";
		                }
		                if (count($temp)) {
		                    $where[] = implode(' OR ', $temp);
		                }
		            }
		            $gradeSearch = 0;
		            if ($filter['shopType'] >= 0 && $filter['payGrade'] >= 0) {//货架档次
		                $gradeSearch = ($filter['shopType']+1)*($filter['payGrade']+1);
		            }else if ($filter['shopType'] >= 0) {
		                $gradeSearch = pow($filter['shopType']+1, 2);
		            }else if ($filter['payGrade'] >= 0) {
		                $gradeSearch = pow($filter['payGrade']+1, 2);
		            }
		            if ($gradeSearch) {
		                $where[] = "SL.`grade_start_search`<=".$gradeSearch." AND SL.`grade_end_search`>=".$gradeSearch;
		            }

		             $sql = "
		        SELECT
		            CD.`category_id`,
		            CD.name AS `cname`,
		            CG.`image` AS `cimage`,
		            SL.*,
		            SF.*,
		            sm.shelf_map_id
		        FROM
		            `solution` AS SL
		            LEFT JOIN `shelf` AS SF ON SL.`solution_id`=SF.`solution_id`
		            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
		            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
		            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";
        		// $where[] = "`customer_id`='".$filter['customer_id']."'";
          //   $sql = "
			       //  SELECT
			       //      CD.`category_id`,
			       //      CD.name AS `cname`,
			       //      CG.`image` AS `cimage`,
			       //      SF.*,
			       //      sm.shelf_map_id
			       //  FROM
          //   `shelf` AS SF
          //   LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
          //   LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
          //   LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";
        }


        if (count($where)) {
            $sql .= " WHERE (".implode(') AND (', $where).")";
        }

      		$start = (int)($filter['page']-1)*(int)$filter['limit'];
      		$sql.=" ORDER BY SF.date_added DESC";
      		$sql .= " LIMIT " . (int)$start . "," . (int)$filter['limit'];
      		//var_dump($sql);die;
              $query = $this->db->query($sql);

        return $query->rows;
        /*
        $where['sl.shop_type'] = $filter['shopType'];

		$customer = $this->customer->getId();
		$where['sl.customer_id'] = array(array('eq',$customer),array('eq',0), 'or');
		$category_group = array();
		foreach($filter["categoryList"] as $v){
			if(isset($v['categoryId'])){
				$category_group[] = $v['categoryId'];
			}
		}

		if(!empty($category_group)){
			$category_group = implode(",", $category_group);
			$where['sf.category_id'] = array('in',$category_group);
		}

		$shelf = M('solution')
			->alias('sl')
			->join('shelf sf on sl.solution_id = sf.solution_id','left')
			->join('category cg on sf.category_id = cg.category_id','left')
			->join('category_description cd on sf.category_id = cd.category_id','left')
			->where($where)
			->field('cd.category_id as category_id,cd.name as cname,cg.image as cimage,sl.*,sf.*')
			->select();
		return $shelf;
        */
	}

	//获取货架商品详细
	public function getShelfProducts($shelf_id){

		$where['sp.shelf_id'] = $shelf_id;
		$shelfProducts = M('shelf_product')
			->alias('sp')
			->join('product p on sp.product_id = p.product_id','left')
			->where($where)
			->field('p.product_id as product_id,sp.shelf_layer,sp.layer_position,sp.min_order_qty,sp.show_qty,sp.shelf_product_id')
			->select();

		return $shelfProducts;

	}
   //优化获取货架商品详细
    public function W_getShelfProducts($shelf_id){

    $where['sp.shelf_id'] = $shelf_id;
    $shelfProducts = M('shelf_product')
      ->alias('sp')
      ->join('product p on sp.product_id = p.product_id','left')
      ->where($where)
      ->field('sp.show_qty,sp.shop_price,sp.shop_sale_price')
      ->select();

    return $shelfProducts;

  }
	//最新
	public function W_getShelf($filter){
        if ($filter['solution_id']) {
            $where[] = "SL.`solution_id`='".$filter['solution_id']."'";
            $sql = "
        SELECT
            CD.`category_id`,
            CD.name AS `cname`,
            CG.`image` AS `cimage`,
            SL.*,
            SF.*,
            sm.shelf_map_id
        FROM
            `solution` AS SL
            LEFT JOIN `shelf` AS SF ON SL.`solution_id`=SF.`solution_id`
            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`
        ";

        }else if($filter['shop_id']){
        		$where[] = "shop.`shop_id`='".$filter['shop_id']."'";
            $sql = "
			        SELECT
			            CD.`category_id`,
			            CD.name AS `cname`,
			            CG.`image` AS `cimage`,
			            shop.*,
			            SF.*,
			            sm.shelf_map_id
			        FROM
            `shop` AS shop
            LEFT JOIN `shelf` AS SF ON shop.`shop_id`=SF.`shop_id`
            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";

        }else if($filter['template']){
		        		 $where[] = "SL.`is_template`='1'";//仅显示模板
		            if ($filter['shelfType'] >= 0) {//货架规格
		                $where[] = "SL.`shelf_types` LIKE '%,".$filter['shelfType'].",%'";
		            }
		            if (count($filter['categoryList']) >= 0) {//货架分类
		                $temp = array();
		                foreach ($filter['categoryList'] as $k=>$info) {
		                    $temp[] = "(SL.`solution_category` LIKE ',".$info['categoryId'].",' AND SL.`shelf_length`='".$info['quantity']."')";
		                }
		                if (count($temp)) {
		                    $where[] = implode(' OR ', $temp);
		                }
		            }
		            $gradeSearch = 0;
		            if ($filter['shopType'] >= 0 && $filter['payGrade'] >= 0) {//货架档次
		                $gradeSearch = ($filter['shopType']+1)*($filter['payGrade']+1);
		            }else if ($filter['shopType'] >= 0) {
		                $gradeSearch = pow($filter['shopType']+1, 2);
		            }else if ($filter['payGrade'] >= 0) {
		                $gradeSearch = pow($filter['payGrade']+1, 2);
		            }
		            if ($gradeSearch) {
		                $where[] = "SL.`grade_start_search`<=".$gradeSearch." AND SL.`grade_end_search`>=".$gradeSearch;
		            }

		             $sql = "
		        SELECT
		            CD.`category_id`,
		            CD.name AS `cname`,
		            CG.`image` AS `cimage`,
		            SL.*,
		            SF.*,
		            sm.shelf_map_id
		        FROM
		            `solution` AS SL
		            LEFT JOIN `shelf` AS SF ON SL.`solution_id`=SF.`solution_id`
		            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
		            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
		            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";

        }else if($filter['shelfids']){

          $shelf_ids=explode(',', $filter['shelfids']);
          $sql=$this->db->query("select shop_id from shelf where shelf_id=".$shelf_ids[0])->row;

          if($sql['shop_id']==$filter['myshop']){
            return;
          }else{
           foreach ($shelf_ids as $key => $value) {
               $data[$value]=$this->db->query("select * from shelf where shelf_id=".$value)->row;
                $data[$value]['product']=$this->db->query("select * from shelf_product where shelf_id=".$value)->rows;
          }

          foreach ($data as $key => $value) {
              $this->db->query("INSERT INTO `" . DB_PREFIX . "shelf` SET solution_id = 0 , customer_id = '" . (int)$filter['customer_id'] . "', shelf_name = '" . $this->db->escape($value['shelf_name']) . "', shelf_type = '" . $this->db->escape($value['shelf_type']) . "', shelf_class1 = '" . $this->db->escape($value['shelf_class1']) . "', shelf_class2 = '" . $this->db->escape($value['shelf_class2']) . "', shelf_class3 ='".$this->db->escape($value['shelf_class3'])."', shop_type = '" . $this->db->escape($value['shop_type']) . "', pay_grades = '" . (int)$value['pay_grades'] . "', date_added = NOW(), `grade_start_search`='".$this->db->escape($value['grade_start_search'])."', `grade_end_search`='".$this->db->escape($value['grade_end_search'])."', `shelf_category`='".$this->db->escape($value['shelf_category'])."',`shop_id`='".$this->db->escape($filter['myshop']). "' ,`shelf_image`='".$this->db->escape($value['shelf_image'])."'");
                $shelf_id = $this->db->getLastId();
                if($shelf_id){
                       foreach ($value['product'] as $k => $v) {
                      $this->db->query("INSERT INTO `" . DB_PREFIX . "shelf_product` SET shelf_id = ".$shelf_id." , product_id = '" . (int)$v['product_id'] . "', product_code = '" . $this->db->escape($v['product_code']) . "', shelf_layer = '" . $this->db->escape($v['shelf_layer']) . "', layer_position = '" . $this->db->escape($v['layer_position']) . "', attr_list = '" . $this->db->escape($v['attr_list']) . "', group_name ='".$this->db->escape($v['group_name'])."', show_qty = '" . $this->db->escape($v['show_qty']) . "', min_order_qty = '" . (int)$v['min_order_qty'] . "', date_added = NOW(), `stock_qty`='".$this->db->escape($v['stock_qty'])."', `stock_qty_temp`='".$this->db->escape($v['stock_qty_temp'])."', `shop_inventory_id`=0 ");
                  }

                }

              }
           }
        }else{

        	$where[] = "`customer_id`='".$filter['customer_id']."'";
          $where[] = "`is_template`!=1";
            $sql = "
			        SELECT
			            CD.`category_id`,
			            CD.name AS `cname`,
			            CG.`image` AS `cimage`,
			            SF.*,
			            sm.shelf_map_id
			        FROM
            `shelf` AS SF
            LEFT JOIN `category` AS CG ON SF.`shelf_category`= CG.`category_id`
            LEFT JOIN `category_description` AS CD ON SF.`shelf_category`=CD.`category_id`
            LEFT JOIN `shelf_map` AS sm ON sm.`shelf_id`=SF.`shelf_id`";
        }




        if (count($where)) {
            $sql .= " WHERE (".implode(') AND (', $where).")";
        }

		$start = (int)($filter['page']-1)*(int)$filter['limit'];
		$sql.=" ORDER BY SF.date_added DESC";
		$sql .= " LIMIT " . (int)$start . "," . (int)$filter['limit'];
		  //var_dump($sql);die;
        $query = $this->db->query($sql);

        return $query->rows;

	}


	//获取推荐商品
	public function getSearchProducts($filter){

		//if($filter['searchinput'] == "" || $filter['shelf_id'] != ""){
		if($filter['searchinput'] == ""){
			$temp['shelf_id'] = $filter['shelf_id'];
			$shelf = M('shelf')->where($temp)->select();

			$product_class3  =  trim($shelf[0]['shelf_class3'],',');
			$map['p.product_class3'] = array('in',$product_class3);

			$map['_logic'] = "or";
			$where['_complex'] = $map;
		}
		else{
			$map['pd.name'] = array('like','%'.$filter['searchinput'].'%');
			$map['p.sku'] = array('like','%'.$filter['searchinput'].'%');
			$map['_logic'] = "or";
			$where['_complex'] = $map;
		}

		// $where['p.status']  =  1;
		$start = $filter['start'];
		$limit = $filter['limit'];
		$products = M('product')
			->alias('p')
			->join('product_description pd on p.product_id = pd.product_id','left')
			->where($where)
			->limit($start,$limit)
			->field('p.product_id as product_id,p.minimum as minimum')
			->select();
			// echo  M('product')->getlastsql();

		return $products;

	}

	//货架加入购物车
	public function addCart($data){



		foreach($data['products'] as $v){

			$shelf_id = $v['shelf_id'];
			$customer = $data['customer_id'];
			$product_id = $v['id'];
			$buy = $v['buy_quantity'];
			$date_added = date('Y-m-d H:i:s');

			$where['customer_id'] = $customer;
			$where['product_id'] = $product_id;
			$incart = M('cart')->where($where)->select();

			if($incart){
				$save['shelf_id'] = $shelf_id;
				$save['quantity'] = array('exp','quantity+'.$buy);
				$save['date_added'] = $date_added;
				M('cart')->where($where)->save($save);
			}
			else{
				$add['customer_id'] = $customer;
				$add['product_id'] = $product_id;
				$po = $v['options'][0]['product_option_id'];
				$pov = $v['options'][0]['option_value'][0]['product_option_value_id'];
				$add['option'] = '{"'.$po.'":"'.$pov.'"}';
				$add['shelf_id'] = $shelf_id;
				$add['quantity'] = $buy;
				$add['date_added'] = $date_added;

				M('cart')->data($add)->add();
			}

		}

	}

	//获取用户支付权限
	public function getPayCheck($customer_id){
		$where['customer_id'] = $customer_id;
		$result = M('customer')
			->alias('c')
			->join('logcenters l on c.logcenter_id = l.logcenter_id','left')
			->join('user u on c.user_id = u.user_id','left')
			->where($where)
			->Field('c.is_magfin,l.logcenter_name as lgname,u.fullname as username,u.contact_tel as usertel')
			->select();

		return $result[0];
	}

	//获取用户默认地址
	public function getDefaultAddress($customer_id){

		$where['c.customer_id'] = $customer_id;
		$sendAddress = M('customer')
			->alias('c')
			->join('address a on c.address_id = a.address_id','left')
			->join('country co on a.country_id = co.country_id','left')
			->join('city ci on a.city_id = ci.city_id','left')
			->join('zone zo on a.zone_id = zo.zone_id','left')
			->where($where)
			->field('a.address_id as address_id,a.company as company,a.fullname as name,a.shipping_telephone as tel,co.name as country,ci.name as city,zo.name as zone,a.address as address')
			->select();
		return $sendAddress[0];
	}

	//获取用户所有地址
	public function getAllAddress($customer_id){
		$where['a.customer_id'] = $customer_id;
		$where['a.need_review'] = 0;
		$addresslist = M('address')
			->alias('a')
			->join('country co on a.country_id = co.country_id','left')
			->join('city ci on a.city_id = ci.city_id','left')
			->join('zone zo on a.zone_id = zo.zone_id','left')
			->where($where)
			->field('a.address_id as address_id,a.company as company,a.fullname as name,a.shipping_telephone as tel,co.name as country,ci.name as city,zo.name as zone,a.address as address')
			->select();
		return $addresslist;
	}

	//覆盖模板
	public function editTemplate($filter){

		$solution_id = $filter['solution_id'];

		if($this->editSolution($filter)){
			$where['solution_id'] = $solution_id;
			$so = M('solution')->where($where)->select();

			$filter['customer_id'] = $so[0]['customer_id'];
			$filter['shelf_types'] = trim(",",$so[0]['shelf_types']);
			$filter['shop_types'] = trim(",",$so[0]['shop_types']);
			$filter['pay_grades'] = trim(",",$so[0]['pay_grades']);

			$result = $this->delShelf($solution_id);
			if($result){
				$this->addShelf($solution_id,$filter);
				return 'success';
			}
		}

	}
	//保存货架
	public function saveshelf($filter){
				$this->addShelf(0,$filter);
				return 'success';
	}

	//添加模板
	public function addTemplate($filter){
		$solution_id = $this->addSolution($filter);
		$this->addShelf($solution_id,$filter);
		return $solution_id;
	}

	//修改模板方案
	public function editSolution($filter){

		foreach($filter['shelflist'] as $v){
			$temp = $v['shelfProduct'];
			foreach($temp as $j){
				$product_class1[] = $j['product_class1'];
				$product_class2[] = $j['product_class2'];
				$product_class3[] = $j['product_class3'];
			}
		}

		$shelf_class1 = array_unique($product_class1);
		$shelf_class2 = array_unique($product_class2);
		$shelf_class3 = array_unique($product_class3);
		$shelf_class1 = ",".implode(',', $shelf_class1).",";
		$shelf_class2 = ",".implode(',', $shelf_class2).",";
		$shelf_class3 = ",".implode(',', $shelf_class3).",";

		$where['solution_id'] = $filter['solution_id'];
		$solution['shelf_class1'] = $shelf_class1;
		$solution['shelf_class2'] = $shelf_class2;
		$solution['shelf_class3'] = $shelf_class3;
		$solution['shelf_length'] = count($filter['shelflist']);
		$solution['date_added'] = date('Y-m-d H:i:s');

		M('solution')->where($where)->save($solution);

		return true;

	}

	//添加模板方案
	public function addSolution($filter){

		$product_class1 = array();
		$product_class2 = array();
		$product_class3 = array();
		foreach($filter['shelflist'] as $v){
			$temp = $v['shelfProduct'];
			foreach($temp as $j){
				$product_class1[] = $j['product_class1'];
				$product_class2[] = $j['product_class2'];
				$product_class3[] = $j['product_class3'];
			}
		}

		$shelf_class1 = array_unique($product_class1);
		$shelf_class2 = array_unique($product_class2);
		$shelf_class3 = array_unique($product_class3);
		$shelf_class1 = ",".implode(',', $shelf_class1).",";
		$shelf_class2 = ",".implode(',', $shelf_class2).",";
		$shelf_class3 = ",".implode(',', $shelf_class3).",";

		$solution['solution_name'] = $filter['solution_name'];
		$solution['customer_id'] = $filter['customer_id'];

		$solution['shelf_class1'] = $shelf_class1;
		$solution['shelf_class2'] = $shelf_class2;
		$solution['shelf_class3'] = $shelf_class3;
		$solution['shelf_types'] = ",".$filter['shelf_types'].",";
		$solution['shop_types'] = ",".$filter['shop_types'].",";
		$solution['pay_grades'] = ",".$filter['pay_grades'].",";
		$solution['shelf_length'] = count($filter['shelflist']);

		$solution['date_added'] = date('Y-m-d H:i:s');

		$solution_id = M('solution')->data($solution)->add();

		return $solution_id;
	}

	//删除模板货架
	public function delShelf($solution_id){
		$where['solution_id'] = $solution_id;
		$shelfs = M('shelf')->where($where)->getField('shelf_id',true);

		//删除shelf_product
		$inshelfs = implode(',', $shelfs);
		$where2['shelf_id'] = array('in',$inshelfs);
		M('shelf_product')->where($where2)->delete();

		//删除shelf
		M('shelf')->where($where)->delete();

		return true;
	}

	//添加模板货架
	public function addShelf($solution_id,$filter){

		$time = date("Y-m-d H:i:s");
		foreach($filter['shelflist'] as $v){
			$shelf['solution_id'] = $solution_id;
			$shelf['customer_id'] = $filter['customer_id'];
			$shelf['shelf_name'] = $v['name'];
			$shelf['shelf_type'] = $filter['shelf_types'];
			$shelf['shop_type'] = $filter['shop_types'];
			$shelf['pay_grades'] = $filter['pay_grades'];
			$shelf['shelf_category'] = $v['category_id'];
			$shelf['date_added'] = $time;
			if($filter['shop_id']){
					$shelf['shop_id'] = $filter['shop_id'];
			}

			$temp = $v['shelfProduct'];
			$product_class1 = array();
			$product_class2 = array();
			$product_class3 = array();
			foreach($temp as $j){
				$product_class1[] = $j['product_class1'];
				$product_class2[] = $j['product_class2'];
				$product_class3[] = $j['product_class3'];
			}

			$product_class1 = array_unique($product_class1);
			$product_class2 = array_unique($product_class2);
			$product_class3 = array_unique($product_class3);
			$shelf['shelf_class1'] = ",".implode(',', $product_class1).",";
			$shelf['shelf_class2'] = ",".implode(',', $product_class2).",";
			$shelf['shelf_class3'] = ",".implode(',', $product_class3).",";

			$shelf_id = M('shelf')->data($shelf)->add();
			$this->addShelfProducts($shelf_id,$temp);

			$i++;
		}

	}
//添加自定义货架
  public function	savecustomshelf($filter){
  		$time = date("Y-m-d H:i:s");
  		$shelf['solution_id'] = '';
			$shelf['customer_id'] = $filter['customer_id'];
			$shelf['shelf_name'] = $filter['shelf_name'];
			$shelf['shelf_type'] = $filter['shelf_types'];
			$shelf['shop_type'] = $filter['shop_types'];
			$shelf['pay_grades'] = $filter['pay_grades'];
			$shelf['shelf_category'] = $filter['category_id'];
			$shelf['date_added'] = $time;
			$shelf['shelf_class1'] = ",";
			$shelf['shelf_class2'] = ",";
			$shelf['shelf_class3'] = ",";
			$shelf['shelf_image'] = $filter['shelf_image'];
			$shelf['shop_id'] = $filter['shop_id'];
			//var_dump($shelf);die;
			$shelf_id = M('shelf')->data($shelf)->add();
			if($shelf_id){
				return "success";
			}

  }

	//添加模板商品
	public function addShelfProducts($shelf_id,$products){
    $time = date("Y-m-d H:i:s");
		foreach($products as $v){
			$sp['shelf_id'] = $shelf_id;
			$sp['product_id'] = $v['id'];
			$sp['product_code'] = $v['pov_code'];
			$sp['attr_list'] = "[]";
			$sp['shelf_layer'] = $v['shelf_y'];
			$sp['layer_position'] = $v['shelf_x'];
			$sp['show_qty'] = $v['shelf_quantity'];
			$sp['min_order_qty'] = $v['shelf_quantity'];
      $sp['date_added'] = $time;
			M('shelf_product')->data($sp)->add();
		}

	}

	//获取分类
	public function getSolutionCategory(){

		$where['is_template'] = 1;
		$result = M('solution')
			->where($where)
			->Distinct(true)
			->getField('solution_category',true);

		$temp;
		foreach($result as $v){
			$temp .= trim(trim($v),",").",";
		}
		$temp = trim(trim($temp),",");

		$where2['c.category_id'] = array('in',$temp);
		$category = M('category')
			->alias('c')
			->join('category_description cd on c.category_id = cd.category_id','left')
			->where($where2)
			->order('c.category_id asc')
			->field('c.category_id as cid,c.image as cimage,cd.name as cname')
			->select();

		return $category;

	}

	//获取品类各种条件下的货节
	public function getSolutionQty(){
		$where['is_template'] = 1;
		$result = M('solution')
			->where($where)
			->field('shelf_types,shelf_length as length,grade_start_search as start,grade_end_search as end,solution_category')
			->select();
		return $result;
	}

	//获取分类货节
	public function getCategoryQty($id){
		$where['solution_category'] = ",".$id.",";
		$where['is_template'] = 1;
		$result = M('solution')
			->where($where)
			->Distinct(true)
			->order('shelf_length asc')
			->getField('shelf_length',true);
		return $result;
	}

	//获取知识分类
	public function getKnowCategory(){
		$where['status'] = 1;
		$result = M('know_category')->where($where)->order('sort asc')->select();
		return $result;
	}

	//绑定二维码
	public function concatCode($filter){

		$data['shelf_map_id'] = $filter['shelf_map_id'];
		$data['shelf_id'] = $filter['shelf_id'];

		M('shelf_map')->data($data)->add();

	}

	//获取筛选货架分类
	public function getClassBar($filter){
		$classbar = implode(',', $filter['classbar']);
		$where['category_id'] = array('in',$classbar);
		$result = M('category_description')->where($where)->order('category_id desc')->Field('category_id,name')->select();

		return $result;
	}

	//通过id删除方案
	public function delSolutionById($id){

		$where['solution_id'] = $id;
		$shelfs = M('shelf')->where($where)->getField('shelf_id',true);

		//删除roduct
		$inshelfs = implode(',', $shelfs);
		$where2['shelf_id'] = array('in',$inshelfs);
		M('shelf_product')->where($where2)->delete();

		//删除shelf
		M('shelf')->where($where)->delete();

		//删除solution
		M('solution')->where($where)->delete();

	}

	//货架层页面内保存商品-------------------------
	public function saveShelfProduct($filter){

		$solution_id = $filter['solution_id'];
		$where['solution_id'] = $solution_id;
		$so = M('solution')->where($where)->select();

		$filter['customer_id'] = $so[0]['customer_id'];
		$filter['shelf_types'] = trim(",",$so[0]['shelf_types']);
		$filter['shop_types'] = trim(",",$so[0]['shop_types']);
		$filter['pay_grades'] = trim(",",$so[0]['pay_grades']);

		if($filter['shelf_id'] == ''){
			$this->addShelf($solution_id,$filter);
			M('solution')->where($where)->setInc('shelf_length',1);
			return 'success';
		}
		else{
			$this->delShelf($solution_id);
			$this->addShelf($solution_id,$filter);
			return 'success';
		}

	}

	public function saveShelfProductinfo($filter){

		$shelf_id = $filter['shelf_id'];
		$where['shelf_id'] = $shelf_id;
		$so = M('shelf')->where($where)->select();
		//var_dump($so);die;
		$solution_id = $so[0]['solution_id'];
		$filter['customer_id'] = $so[0]['customer_id'];
		$filter['shelf_types'] = trim(",",$so[0]['shelf_type']);
		$filter['shop_types'] = trim(",",$so[0]['shop_type']);
		$filter['pay_grades'] = trim(",",$so[0]['pay_grades']);
		$filter['shelf_name'] = $so[0]['shelf_name'];
		$filter['shelf_category'] = $so[0]['shelf_category'];
		$filter['shelf_image'] = $so[0]['shelf_image'];
		$filter['shop_id'] = $so[0]['shop_id'];
		//var_dump($shelf_id);
		if($so){
          $this->addShelfinfo($solution_id,$filter);
					$this->delShelfinfo($shelf_id);

          return 'success';
		}

	}
  //为了修改缓存中的货架id得获得货架ID修改
    public function W_saveShelfProductinfo($filter){

    $shelf_id = $filter['shelf_id'];
    $where['shelf_id'] = $shelf_id;
    $so = M('shelf')->where($where)->select();
    //var_dump($so);die;
    $solution_id = $so[0]['solution_id'];
    $filter['customer_id'] = $so[0]['customer_id'];
    $filter['shelf_types'] = trim(",",$so[0]['shelf_type']);
    $filter['shop_types'] = trim(",",$so[0]['shop_type']);
    $filter['pay_grades'] = trim(",",$so[0]['pay_grades']);
    $filter['shelf_name'] = $so[0]['shelf_name'];
    $filter['shelf_category'] = $so[0]['shelf_category'];
    $filter['shelf_image'] = $so[0]['shelf_image'];
    $filter['shop_id'] = $so[0]['shop_id'];
    //var_dump($shelf_id);
    if($so){
          $shelfid = $this->W_addShelfinfo($solution_id,$filter);
          $this->delShelfinfo($shelf_id);

          return $shelfid;
    }

    }

		//删除模板货架
	public function delShelfinfo($shelf_id){
		$where2['shelf_id'] = $shelf_id;
		M('shelf_product')->where($where2)->delete();

		//删除shelf
		M('shelf')->where($where2)->delete();

		return true;
	}

	//添加模板货架
	public function addShelfinfo($solution_id,$filter){
		//var_dump($filter);die;
		$time = date("Y-m-d H:i:s");
		foreach($filter['shelflist'] as $v){
			$shelf['solution_id'] = $solution_id;
			$shelf['customer_id'] = $filter['customer_id'];
			$shelf['shelf_name'] = $filter['shelf_name'];
			$shelf['shelf_type'] = $filter['shelf_types'];
			$shelf['shop_type'] = $filter['shop_types'];
			$shelf['pay_grades'] = $filter['pay_grades'];
			$shelf['shelf_category'] = $filter['shelf_category'];
			$shelf['date_added'] = $time;
			$shelf['shelf_image'] = $filter['shelf_image'];
			if($filter['shop_id']){
					$shelf['shop_id'] = $filter['shop_id'];
			}
			$temp = $v['shelfProduct'];
			$product_class1 = array();
			$product_class2 = array();
			$product_class3 = array();
			foreach($temp as $j){
				$product_class1[] = $j['product_class1'];
				$product_class2[] = $j['product_class2'];
				$product_class3[] = $j['product_class3'];
			}

			$product_class1 = array_unique($product_class1);
			$product_class2 = array_unique($product_class2);
			$product_class3 = array_unique($product_class3);
			$shelf['shelf_class1'] = ",".implode(',', $product_class1).",";
			$shelf['shelf_class2'] = ",".implode(',', $product_class2).",";
			$shelf['shelf_class3'] = ",".implode(',', $product_class3).",";

			$shelf_id = M('shelf')->data($shelf)->add();

			$this->addShelfProducts($shelf_id,$temp);
			$i++;
		}

  	}
    //2018.5.22新增添加货架返回id
    public function W_addShelfinfo($solution_id,$filter){
    //var_dump($filter);die;
    $time = date("Y-m-d H:i:s");
    foreach($filter['shelflist'] as $v){
      $shelf['solution_id'] = $solution_id;
      $shelf['customer_id'] = $filter['customer_id'];
      $shelf['shelf_name'] = $filter['shelf_name'];
      $shelf['shelf_type'] = $filter['shelf_types'];
      $shelf['shop_type'] = $filter['shop_types'];
      $shelf['pay_grades'] = $filter['pay_grades'];
      $shelf['shelf_category'] = $filter['shelf_category'];
      $shelf['date_added'] = $time;
      $shelf['shelf_image'] = $filter['shelf_image'];
      if($filter['shop_id']){
          $shelf['shop_id'] = $filter['shop_id'];
      }
      $temp = $v['shelfProduct'];
      $product_class1 = array();
      $product_class2 = array();
      $product_class3 = array();
      foreach($temp as $j){
        $product_class1[] = $j['product_class1'];
        $product_class2[] = $j['product_class2'];
        $product_class3[] = $j['product_class3'];
      }

      $product_class1 = array_unique($product_class1);
      $product_class2 = array_unique($product_class2);
      $product_class3 = array_unique($product_class3);
      $shelf['shelf_class1'] = ",".implode(',', $product_class1).",";
      $shelf['shelf_class2'] = ",".implode(',', $product_class2).",";
      $shelf['shelf_class3'] = ",".implode(',', $product_class3).",";

      $shelf_id = M('shelf')->data($shelf)->add();

      $this->addShelfProducts($shelf_id,$temp);
      return $shelf_id;
    }

  }
  //获取商品id
  public function getproduct_id($data){
      $productinfo = $this->db->query("SELECT sp.show_qty,sp.product_id from shelf as s LEFT JOIN shelf_product as sp ON sp.shelf_id = s.shelf_id where solution_id=".$data)->rows;
      //货架长度
      $length = $this->db->query("SELECT count(*) as length from shelf where solution_id=".$data)->row;
      foreach($productinfo as $key=>$value){
          $productinfo[$key]['length']=$length['length'];
      }
      return $productinfo;
  }

  //获取货架大厅展示货架信息、
  public function solutionshelf($data){
       $sql = $this->db->query("SELECT shelf_id,shelf_name,shelf_image,is_template,shelf_category from shelf where solution_id=".$data['solutionId'])->rows;
       return $sql;
  }
  public function allshelf($solution_id){
     $where['solution_id'] = $solution_id;
     $so = M('shelf')->where($where)->select();
     return $so;
  }
  //添加模板货架
  public function WXT_addShelf($filter){
      $time = date("Y-m-d H:i:s");
      foreach ($filter as $key => $value) {

          $shelf['solution_id'] = 0;
          $shelf['customer_id'] = $filter[$key]['customer_id'];
          $shelf['shelf_name'] = $filter[$key]['shelf_name'];
          $shelf['shelf_type'] = $filter[$key]['shelf_type'];
          $shelf['shop_type'] = $filter[$key]['shop_type'];
          $shelf['pay_grades'] = $filter[$key]['pay_grades'];
          $shelf['shelf_category'] = $filter[$key]['shelf_category'];
          $shelf['date_added'] = $time;
          $shelf['shop_id'] = $filter[$key]['shop_id'];
          $shelf['shelf_class1'] = $filter[$key]['shelf_class1'];
          $shelf['shelf_class2'] =$filter[$key]['shelf_class2'];
          $shelf['shelf_class3'] = $filter[$key]['shelf_class3'];
          $shelf['shelf_image'] = $filter[$key]['shelf_image'];
          $product = $this->db->query("SELECT * from shelf_product where shelf_id=".$filter[$key]['shelf_id'])->rows;
          $shelf_id = M('shelf')->data($shelf)->add();
          $this->WXT_addShelfProducts($shelf_id,$product);

    }



  }

  //添加模板商品
  public function WXT_addShelfProducts($shelf_id,$products){
    $time = date("Y-m-d H:i:s");
    foreach($products as $v){
      $sp['shelf_id'] = $shelf_id;
      $sp['product_id'] = $v['product_id'];
      $sp['product_code'] = $v['product_code'];
      $sp['attr_list'] = $v['attr_list'];
      $sp['shelf_layer'] = $v['shelf_layer'];
      $sp['layer_position'] = $v['layer_position'];
      $sp['show_qty'] = $v['show_qty'];
      $sp['min_order_qty'] = $v['min_order_qty'];
      $sp['date_added'] = $time;
      M('shelf_product')->data($sp)->add();
    }

  }

}
