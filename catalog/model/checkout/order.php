<?php
class ModelCheckoutOrder extends Model {
	public function addOrder($data,$spreadratio) {
		$this->event->trigger('pre.order.add', $data);

		$sql = "INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', fullname = '" . $this->db->escape($data['fullname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', payment_fullname = '" . $this->db->escape($data['payment_fullname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address = '" . $this->db->escape($data['payment_address']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_city_id = '" . $this->db->escape($data['payment_city_id']) . "',payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? json_encode($data['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_fullname = '" . $this->db->escape($data['shipping_fullname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', shipping_address = '" . $this->db->escape($data['shipping_address']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_city_id = '" . $this->db->escape($data['shipping_city_id']) . "',shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? json_encode($data['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', ori_total = '". (float)$data['total'] ."', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', marketing_id = '" . (int)$data['marketing_id'] . "', tracking = '" . $this->db->escape($data['tracking']) . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW(), recommend_usr_id='".$this->getRecommendUserId()."'";
		if($data['payment_code'] == 'cod'){
			$sql .= ',is_pay = 0';
			
		}

		$this->db->query($sql);
		$order_id = $this->db->getLastId();

		if(isset($data['is_sample_order'])) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET is_sample_order = '" . (int)$data['is_sample_order'] . "' WHERE order_id = '" . (int)$order_id . "'");
		}

		if(isset($data['logcenter_id'])) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET logcenter_id = '" . (int)$data['logcenter_id'] . "' WHERE order_id = '" . (int)$order_id . "'");
		}

		// Products
		if (isset($data['products'])) {
			foreach($data['products'] as $key=>$product){
				if(empty($product['option'])){
					$sql = "SELECT pov.product_option_id,pov.product_option_value_id,o.type,od.name,ovd.name AS value FROM `".DB_PREFIX."product_option_value` pov LEFT JOIN `".DB_PREFIX."option` o ON o.option_id = pov.option_id LEFT JOIN `".DB_PREFIX."option_description` od ON od.option_id = pov.option_id LEFT JOIN `".DB_PREFIX."option_value_description` ovd ON ovd.option_value_id = pov.option_value_id WHERE product_id = '".(int)$product['product_id']."'";

					$option_query = $this->db->query($sql);

					$option_result = $option_query->row;

					if(empty($option_result)){
						unset($data['products'][$key]);
					}

					$data['products'][$key]['option'][] = array(
						'product_option_id' => $option_result['product_option_id'],
						'product_option_value_id' => $option_result['product_option_value_id'],
						'name' => $option_result['name'],
						'type' => $option_result['type'],
						'value' => $option_result['value']
					);
				}
			}
			$count=0;
			// var_dump($data['products']);die();
			foreach ($data['products'] as $product) {
				$count++;
				$result_product_code = $this->db->query("SELECT IF(pov.product_code,pov.product_code,p.product_code) as product_code FROM `".DB_PREFIX."product_option_value` pov,`product` p WHERE pov.product_option_value_id = '".(int)$product['option'][0]['product_option_value_id']."' AND p.product_id = '".(int)$product['product_id']."'");

				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_code = '".$result_product_code->row['product_code']."', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', pay_price = '". (float)$product['pay_price'] ."', total = '" . (float)$product['total'] . "', pay_total = '". (float)$product['pay_total'] ."' , tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "',product_type = '" . (int)$product['product_type'] . "',vendor_id = '" . (int)$product['vendor_id'] . "',  lack_quantity = '". (int)$product['quantity'] ."', order_ids = '". (int)$count ."'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}

				$result_product_type = $this->db->query("SELECT p.product_type,p.product_id FROM `".DB_PREFIX."product_option_value` pov LEFT JOIN `product` p ON p.product_id = pov.product_id WHERE pov.product_code = '".$result_product_code->row['product_code']."'");


                /*
                 * 组合商品摊销价（sp=special_price，sp不存在是取product.price计算摊售比例）
                 * @author sonicsjh
                 */
				if($result_product_type->row['product_type'] == 2){//商品组摊销价
					$sql = "UPDATE `".DB_PREFIX."order_product` SET lack_quantity = 0 WHERE order_product_id = '".$order_product_id."'";
					$this->db->query($sql);

					//$result_product_special = $this->db->query("SELECT price FROM `".DB_PREFIX."product_special` WHERE product_id = '".$result_product_type->row['product_id']."' AND ((date_start <= CURRENT_DATE() AND date_end >= CURRENT_DATE()) OR (date_start = '0000-00-00' AND date_end = '0000-00-00')) GROUP BY priority HAVING priority = MIN(priority)");
                    //获取组合商品的special_price（逻辑不严谨，sql调整）
                    $sql = "
                    SELECT
                        price
                    FROM
                        `".DB_PREFIX."product_special`
                    WHERE
                        product_id = '".$result_product_type->row['product_id']."'
                        AND customer_group_id = '".(int)$this->config->get('config_customer_group_id')."'
                        AND (
                            (date_start <= CURRENT_DATE() AND date_end >= CURRENT_DATE())
                            OR (date_start <= CURRENT_DATE() AND date_end = '0000-00-00')
                            OR (date_start = '0000-00-00' AND date_end >= CURRENT_DATE())
                            OR (date_start = '0000-00-00' AND date_end = '0000-00-00')
                        )
                    ORDER BY price ASC
                    ";
                    $result_product_special = $this->db->query($sql);

					if(empty($result_product_special->row['price'])){
						$result_product_special = $this->db->query("SELECT sale_price AS 'price' FROM `".DB_PREFIX."product` WHERE product_id = '".$result_product_type->row['product_id']."'");//当sp不存在是，获取price作为参考价
					}

					$result_product_group = $this->db->query("SELECT * FROM `".DB_PREFIX."product_group` WHERE parent_product_code = '".$result_product_code->row['product_code']."'");
					$result_product_group = $result_product_group->rows;//获取商品组合的单品列表
					$sum_special_price = 0;

					foreach($result_product_group as $key=>$val){//循环获取组合商品的单品sp
						//$result_child_product_special = $this->db->query("SELECT price FROM `".DB_PREFIX."product_special` WHERE product_id = '".$val['child_product_id']."' AND ((date_start <= CURRENT_DATE() AND date_end >= CURRENT_DATE()) OR (date_start = '0000-00-00' AND date_end = '0000-00-00')) GROUP BY priority HAVING priority = MIN(priority)");
                        //获取组合商品的special_price（逻辑不严谨，sql调整）
                        $sql = "
                        SELECT
                            price
                        FROM
                            `".DB_PREFIX."product_special`
                        WHERE
                            product_id = '".$val['child_product_id']."'
                            AND customer_group_id = '".(int)$this->config->get('config_customer_group_id')."'
                            AND (
                                (date_start <= CURRENT_DATE() AND date_end >= CURRENT_DATE())
                                OR (date_start <= CURRENT_DATE() AND date_end = '0000-00-00')
                                OR (date_start = '0000-00-00' AND date_end >= CURRENT_DATE())
                                OR (date_start = '0000-00-00' AND date_end = '0000-00-00')
                            )
                        ORDER BY price ASC
                        ";
						$result_child_product_special = $this->db->query($sql);
						if(empty($result_child_product_special->row['price'])){
							$result_child_product_special = $this->db->query("SELECT sale_price AS 'price' FROM `".DB_PREFIX."product` WHERE product_id = '".$val['child_product_id']."'");//当sp不存在是，获取price作为参考价
						}

						$result_child_product_special_v = $this->db->query("SELECT vendor_id FROM `".DB_PREFIX."product` WHERE product_id = '".$val['child_product_id']."'");//

						$result_product_group[$key]['special_price'] = $result_child_product_special->row['price'];
						$result_product_group[$key]['vendor_id'] = $result_child_product_special_v->row['vendor_id'];

						$sum_special_price += ($result_child_product_special->row['price'] * $val['child_quantity']);

					}

					foreach($result_product_group as $key=>$val){

						$result_product_group[$key]['percant'] = $val['special_price'] * $val['child_quantity'] / $sum_special_price;//计算单品在组合中占的比例

					}
					$countg=0;
					foreach($result_product_group as $val){
						$countg++;
						// var_dump($val['child_price']);
						if ($val['child_price']!=0) {
							// echo 1;
							$PGCP = $val['child_price'];
							$PGCPP = $val['child_price'];
						}else{
							// echo 2;
							$PGCP = $product['price'] * $val['percant']/$val['child_quantity'];//原价 product_group_child_price
	                        $PGCPP = $product['pay_price'] * $val['percant']/$val['child_quantity'];
	                        //摊销价 product_group_child_pay_price
						}
						// var_dump($PGCP);
						// var_dump($PGCPP);


                        $sql = "
                        INSERT INTO 
                            `".DB_PREFIX."order_product_group`
                        SET
                            order_product_id = '".$order_product_id."',
                            product_id = '".$val['child_product_id']."',
                            product_code = '".$val['child_product_code']."',
                            quantity = '".($val['child_quantity']*$product['quantity'])."',
                            price = '".$PGCP."',
                            order_ids = '".$countg."',
                            vendor_id = '".$val['vendor_id']."',
                            pay_price = '".$PGCPP."',
                            total = '".($PGCP*$val['child_quantity']*$product['quantity'])."',
                            pay_total = '".($PGCPP*$val['child_quantity']*$product['quantity'])."',
                            lack_quantity = '".($val['child_quantity']*$product['quantity'])."'";
                        $this->db->query($sql);
/*
						$temp_product_total = $result_product_special->row['price'] * $val['percant'];
						$temp_product_price = $result_product_special->row['price'] * $val['percant'] / $val['child_quantity'];
						$this->db->query("INSERT INTO `".DB_PREFIX."order_product_group` SET order_product_id = '".$order_product_id."',product_id = '".$val['child_product_id']."',product_code = '".$val['child_product_code']."',quantity = '".($val['child_quantity']*$product['quantity'])."',price = '".$temp_product_price."',pay_price = '".($temp_product_price*$spreadratio)."',total = '".($temp_product_total*$product['quantity'])."',pay_total = '".($temp_product_total*$spreadratio*$product['quantity'])."', lack_quantity = '".($val['child_quantity']*$product['quantity'])."'");
*/
					}
				}
			}
		}

		// Gift Voucher
		$this->load->model('total/voucher');

		// Vouchers
		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_total_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		// Totals
		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		$this->event->trigger('post.order.add', $order_id);

		return $order_id;
	}

	public function editOrder($order_id, $data) {
		$this->event->trigger('pre.order.edit', $data);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', fullname = '" . $this->db->escape($data['fullname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(json_encode($data['custom_field'])) . "', payment_fullname = '" . $this->db->escape($data['payment_fullname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address = '" . $this->db->escape($data['payment_address']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(json_encode($data['payment_custom_field'])) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_fullname = '" . $this->db->escape($data['shipping_fullname']) . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address = '" . $this->db->escape($data['shipping_address']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(json_encode($data['shipping_custom_field'])) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "',shipping_city_id = '" . (int)$data['shipping_city_id'] . "', payment_city_id = '" . (int)$data['payment_city_id'] . "',commission = '" . (float)$data['commission'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");

		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		// Gift Voucher
		$this->load->model('total/voucher');

		$this->model_total_voucher->disableVoucher($order_id);

		// Vouchers
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_total_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		// Totals
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		$this->event->trigger('post.order.edit', $order_id);
	}

	public function deleteOrder($order_id) {
		$this->event->trigger('pre.order.delete', $order_id);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE `or`, ort FROM `" . DB_PREFIX . "order_recurring` `or`, `" . DB_PREFIX . "order_recurring_transaction` `ort` WHERE order_id = '" . (int)$order_id . "' AND ort.order_recurring_id = `or`.order_recurring_id");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_transaction` WHERE order_id = '" . (int)$order_id . "'");

		// Gift Voucher
		$this->load->model('total/voucher');

		$this->model_total_voucher->disableVoucher($order_id);

		$this->event->trigger('post.order.delete', $order_id);
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT o.*, c.logcenter_id AS warehouse_id, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o LEFT JOIN `".DB_PREFIX."customer` c on o.customer_id = c.customer_id WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'warehouse_id'            => $order_query->row['warehouse_id'],
				'fullname'                => $order_query->row['fullname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => json_decode($order_query->row['custom_field'], true),
				'payment_fullname'        => $order_query->row['payment_fullname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address'         => $order_query->row['payment_address'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => json_decode($order_query->row['payment_custom_field'], true),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_fullname'       => $order_query->row['shipping_fullname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address'        => $order_query->row['shipping_address'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => json_decode($order_query->row['shipping_custom_field'], true),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
				'paid_time'               => $order_query->row['paid_time'],
				'auditted_time'           => $order_query->row['auditted_time'],
				'delivered_time'          => $order_query->row['delivered_time'],
				'completed_time'          => $order_query->row['completed_time'],
				'out_time'                => $order_query->row['out_time'],
				'is_sample_order'         => $order_query->row['is_sample_order'],
				'hang_status'             => $order_query->row['hang_status'],
				'total_percant'           => $order_query->row['total_percant']
			);
		} else {
			return false;
		}
	}

	public function addOrderHistory($order_id, $order_status_id, $comment = '', $notify = false, $override = false, $additionarr = array()) {
		$event_data = array(
			'order_id'		  => $order_id,
			'order_status_id' => $order_status_id,
			'comment'		  => $comment,
			'notify'		  => $notify
		);

		$this->event->trigger('pre.order.history.add', $event_data);

		$order_info = $this->getOrder($order_id);

		if ($order_info) {
			// Fraud Detection
			$this->load->model('account/customer');

			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			// Only do the fraud check if the customer is not on the safe list and the order status is changing into the complete or process order status
			// if (!$safe && !$override && in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
			// 	// Anti-Fraud
			// 	$this->load->model('extension/extension');

			// 	$extensions = $this->model_extension_extension->getExtensions('fraud');

			// 	foreach ($extensions as $extension) {
			// 		if ($this->config->get($extension['code'] . '_status')) {
			// 			$this->load->model('fraud/' . $extension['code']);

			// 			$fraud_status_id = $this->{'model_fraud_' . $extension['code']}->check($order_info);

			// 			if ($fraud_status_id) {
			// 				$order_status_id = $fraud_status_id;
			// 			}
			// 		}
			// 	}
			// }
            /*
             * config_processing_status = array(1, 2, 3, 17, 18, 20);
             * config_complete_status = array(5, 19);
             */
			// If current order status is not processing or complete but new status is processing or complete then commence completing the order
			if (!in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Redeem coupon, vouchers and reward points
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					  // Confirm coupon, vouchers and reward points
					  $fraud_status_id = $this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);

					  // If the balance on the coupon, vouchers and reward points is not enough to cover the transaction or has already been used then the fraud order status is returned.
					  // if ($fraud_status_id) {
						 //  $order_status_id = $fraud_status_id;
					  // }
				  }
				}

				//添加用户积分
				$to_add_reward = (int)((float)$order_info['total']/1000);
				if ($to_add_reward > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = '" . $this->db->escape(sprintf('订单 ID: #%s', (int)$order_info['order_id'])) . "', points = '" . (int)$to_add_reward . "', date_added = NOW()");
				}

				// Add commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id'] && $this->config->get('config_affiliate_auto')) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->addTransaction($order_info['affiliate_id'], $order_info['commission'], $order_id);
				}

				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {

					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");//修改 p 表库存（前台库存）

					//$query_info = $this->db->query("SELECT quantity,minimum FROM `".DB_PREFIX."product` WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");

					//$quantity_info = $query_info->row;

					//if($quantity_info['quantity'] < $quantity_info['minimum']){

					//  $this->db->query("UPDATE `".DB_PREFIX."product` SET status = 0 WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					//}

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");//修改 pov 表库存（前台库存）

						//$query_quantity = $this->db->query("SELECT quantity FROM `" . DB_PREFIX . "product_option_value` WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");

						//if($query_quantity->row['quantity'] < $quantity_info['minimum']){

							// $this->db->query("UPDATE `".DB_PREFIX."product` SET status = 0 WHERE product_id = '" . (int)$order_product['product_id'] . "'");

						//}
					}
				}
			}

			// Update the DB with the new statuses
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			//记录状态时间
			switch((int)$order_status_id){//修改后订单状态
				case 20://待审核时间
					if($order_info['payment_code'] != 'cod')
						$this->db->query("UPDATE `" . DB_PREFIX . "order` SET paid_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
				case 2://待发货时间
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET auditted_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
				case 17://部分发货时间
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET out_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
				case 18://全部发货时间
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET out_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
				case 3://待收货时间
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET delivered_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
				case 5://完成时间
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET completed_time = NOW() WHERE order_id = '" . (int)$order_id . "'");
					break;
			}

			//设置时区
			date_default_timezone_set ( 'Asia/Shanghai' );
			
			//推送短信
			if((int)$order_status_id == 20 || (int)$order_status_id == 1 || (int)$order_status_id == 2 || (int)$order_status_id == 17 || (int)$order_status_id == 18 || (int)$order_status_id == 3){
				require_once(DIR_APPLICATION.'/controller/rest/sms.php');
				$sms = new ControllerRestSms();
				$appId = $this->config->get ( 'tianyi_appid' );
		        $appSecret = $this->config->get ( 'tianyi_appsecret' );
		        $confirm = true;

		        switch((int)$order_status_id){
		        	case 1://在线支付，确认订单
		        		if(intval($order_info['order_status_id'])){
	        				$confirm = false;
	        			}

		        		$templateName = 'tianyi_templateid_co';
		        		$templateId = $this->config->get ( $templateName );//91552780

		        		$template_param_bman = json_encode ( array (
				            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
				            'time' => date('m月d日',strtotime($order_info['date_added'])),
				            'ordernum' => $order_info['order_id'],
				            'payment' => '在线支付',
				            'total' => floatval($order_info['total']),
				            'addition' => '。'
				        ) );

				        $template_param_cust = json_encode ( array (
				            'name' => '您',
				            'time' => date('m月d日',strtotime($order_info['date_added'])),
				            'ordernum' => $order_info['order_id'],
				            'payment' => '在线支付',
				            'total' => floatval($order_info['total']),
				            'addition' => '，请尽快支付完成。'
				        ) );
		        		break;

		        	case 20://待审核订单
		        		if($order_info['payment_code'] == 'cod'){//货到付款
		        			if(intval($order_info['order_status_id'])){
		        				$confirm = false;
		        			}

		        			$templateName = 'tianyi_templateid_co';
		        			$templateId = $this->config->get ( $templateName );//91552780

		        			$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'payment' => '货到付款',
					            'total' => floatval($order_info['total']),
					            'addition' => '。'
				        	) );

							$template_param_cust = json_encode ( array (
					            'name' => '您',
					            'time' =>date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'payment' => '货到付款',
					            'total' => floatval($order_info['total']),
					            'addition' => '。'
				        	) );
		        		}
						elseif($order_info['payment_code'] == 'magfin'){//磁金融帐期支付
							if(intval($order_info['order_status_id'])){
		        				$confirm = false;
		        			}

							$templateName = 'tianyi_templateid_co';
							$templateId = $this->config->get ( $templateName );//91552780

		        			$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'payment' => '金融账期支付',
					            'total' => floatval($order_info['total']),
					            'addition' => '。'
				        	) );

							$template_param_cust = json_encode ( array (
					            'name' => '您',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'payment' => '金融账期支付',
					            'total' => floatval($order_info['total']),
					            'addition' => '请按时转入：磁石供应链商业保险（深圳）有限公司账户。'
				        	) );
						}
						else{//在线支付订单付款成功通知
							if(intval($order_info['order_status_id']) != 1){
		        				$confirm = false;
		        			}

							$templateName = 'tianyi_templateid_sp';
							$templateId = $this->config->get ( $templateName );

		        			$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'total' => floatval($order_info['total']),
					            'addition' => '。'
				        	) );

				        	$template_param_cust = json_encode ( array (
					            'name' => '您',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'total' => floatval($order_info['total']),
					            'addition' => '，请等待收货。'
				        	) );

						}
		        		break;

		        	case 2://待发货短信通知
		        		if(intval($order_info['order_status_id']) != 20){
	        				$confirm = false;
	        			}

		        		$templateName = 'tianyi_templateid_sv';
		        		$templateId = $this->config->get ( $templateName );
		        		$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'vtime' => date('Y年m月d日')
				        	) );

			        	$template_param_cust = json_encode ( array (
				            'name' => '您',
				            'time' => date('m月d日',strtotime($order_info['date_added'])),
				            'ordernum' => $order_info['order_id'],
				            'vtime' => date('Y年m月d日')
			        	) );

		        		break;

		        	case 17://部分发货短信通知
		        		if(intval($order_info['order_status_id']) != 2){
	        				$confirm = false;
	        			}

		        		if(empty($order_info['out_time'])){
			        		$templateName = 'tianyi_templateid_tc';
			        		$templateId = $this->config->get ( $templateName );
			        		
			        		$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'dtime' => date('Y年m月d日'),
					            'dtype' => '开始货物',
					            'rinfo' => '。'
				        	) );

				        	$template_param_cust = json_encode ( array (
					            'name' => '您',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'dtime' => date('Y年m月d日'),
					            'dtype' => '开始货物',
					            'rinfo' => '。'
				        	) );
				        }else
				        	$confirm = false;
		        		break;

		        	case 18://全部发货短信通知
		        		if(intval($order_info['order_status_id']) != 17 && !$additionarr['isRMoney']){
	        				$confirm = false;
	        			}

		        		if(empty($order_info['out_time']) || $additionarr['isRMoney']){
			        		$templateName = 'tianyi_templateid_tc';
			        		$templateId = $this->config->get ( $templateName );
			        		
			        		$template_param_bman = json_encode ( array (
					            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'dtime' => date('Y年m月d日'),
					            'dtype' => '开始货物',
					            'rinfo' => ($additionarr['isRMoney']?'您有一部分商品因发货数量不足，在百货栈的余额将收到一笔退款：'.$additionarr['money'].'元。':'。')
				        	) );

				        	$template_param_cust = json_encode ( array (
					            'name' => '您',
					            'time' => date('m月d日',strtotime($order_info['date_added'])),
					            'ordernum' => $order_info['order_id'],
					            'dtime' => date('Y年m月d日'),
					            'dtype' => '开始货物',
					            'rinfo' => ($additionarr['isRMoney']?'您有一部分商品因发货数量不足，在百货栈的余额将收到一笔退款：'.$additionarr['money'].'元。':'。')
				        	) );
				        }else
				        	$confirm = false;
		        		break;

		        	case 3://待收货通知短信
		        		if(intval($order_info['order_status_id']) != 18){
	        				$confirm = false;
	        			}

		        		$templateName = 'tianyi_templateid_dc';
		        		$templateId = $this->config->get ( $templateName );
		        		
		        		$template_param_bman = json_encode ( array (
				            'name' => $order_info['payment_company'].'('.$order_info['telephone'].')',
				            'time' => date('m月d日',strtotime($order_info['date_added'])),
				            'ordernum' => $order_info['order_id'],
				            'dtime' => date('Y年m月d日'),
				            'addition' => '。'
			        	) );

		        		$template_param_cust = json_encode ( array (
				            'name' => '您',
				            'time' => date('m月d日',strtotime($order_info['date_added'])),
				            'ordernum' => $order_info['order_id'],
				            'dtime' => date('Y年m月d日'),
				            'addition' => '，请等候货物到达。'
			        	) );
		        		break;
		      	}

	        	$user_phone_data = $this->db->query("SELECT u.contact_tel FROM `" . DB_PREFIX . "customer` AS c LEFT JOIN `" . DB_PREFIX . "user` AS u on c.user_id = u.user_id where c.customer_id = '".$order_info['customer_id']."'");

	        	foreach($user_phone_data->rows as $phone_data){
	        		$user_phone = $phone_data['contact_tel'];
	        	}

	        	if($confirm){
		        	if(!empty($order_info['telephone'])){
			        	$sms_result = $sms->sendSms($appId,$appSecret,$templateId,$order_info['telephone'],$template_param_cust);//给客户发通知短信

			        	if($sms_result = 'successful'){
			        		$this->db->query("INSERT INTO `" . DB_PREFIX . "message_log` SET `order_status_id` = '".$order_status_id."', `templateid` = '".$templateName."', `stype` = 'cust', `sphone` = '".$order_info['telephone']."', `param` = '".json_encode($template_param_cust)."', `create_time` = NOW()");
			        	}
			        }

			    	if(false && !empty($user_phone)){//业务员通过网站前台直接查看订单和出库单状态，不再发送通知短信
			        	$sms_result = $sms->sendSms($appId,$appSecret,$templateId,$user_phone,$template_param_bman);//给客户所属业务员发通知短信

			        	if($sms_result = 'successful'){
			        		$this->db->query("INSERT INTO `" . DB_PREFIX . "message_log` SET `order_status_id` = ".$order_status_id.", `templateid` = '".$templateName."', `stype` = 'bman', `sphone` = '".$user_phone."', `param` = '".json_encode($template_param_bman)."', `create_time` = NOW()");
			        	}
			        }

			    	if((int)$order_status_id == 20 && $order_info['payment_code'] != 'cod' && $order_info['payment_code'] != 'magfin'){//在线支付订单，支付成功后，给财务（顾俊）发通知短信
			        	$sms_result = $sms->sendSms($appId,$appSecret,$templateId,13564536028,$template_param_bman);

			        	if($sms_result = 'successful'){
			        		$this->db->query("INSERT INTO `" . DB_PREFIX . "message_log` SET `order_status_id` = ".$order_status_id.", `templateid` = '".$templateName."', `stype` = 'fina', `sphone` = '13564536028', `param` = '".json_encode($template_param_bman)."', `create_time` = NOW()");
			        	}
			        }
			    }
		    }
			//1待付款,2待发货,3待收货,5完成,7退换货,11,已退款,13拒付,16,无效,17部分发货,18全部发货,19已结束发货
			$order_ship_array = array(1,2,3,5,11,17,18,19);
			if(($order_info['order_status_id'] == 0)&&(in_array($order_status_id, $order_ship_array))){
				$this->event->trigger('order.product.addstock', $order_id);
			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
			
			//添加用户首次拿样订单记录
			if(isset($customer_info['sample_bill_amount'])&&$customer_info['sample_bill_amount']==0) {
				if(isset($order_info['is_sample_order'])&&$order_info['is_sample_order']==1){
					$this->addCustomerSampleAmount($order_info['customer_id'], $order_info['total']);
				}
			}

			// If old order status is the processing or complete status but new status is not then commence restock, and remove coupon, voucher and reward history
			if (in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && !in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Restock
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {
					$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

					$query_info = $this->db->query("SELECT quantity,minimum FROM `".DB_PREFIX."product` WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

					$quantity_info = $query_info->row;

					// if($quantity_info['quantity'] >= $quantity_info['minimum']){

					// 	$this->db->query("UPDATE `".DB_PREFIX."product` SET status = 1 WHERE product_id = '" . (int)$product['product_id'] . "'");

					// }

					$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");

						$query_quantity = $this->db->query("SELECT quantity FROM `" . DB_PREFIX . "product_option_value` WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");

						if($query_quantity->row['quantity'] >= $quantity_info['minimum']){

							// $this->db->query("UPDATE `".DB_PREFIX."product` SET status = 1 WHERE product_id = '" . (int)$product['product_id'] . "'");
							
						}
					}
				}

				// Remove coupon, vouchers and reward points history
				$this->load->model('account/order');

				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'unconfirm')) {
						$this->{'model_total_' . $order_total['code']}->unconfirm($order_id);
					}
				}

				//移除用户积分
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");

				// Remove commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id']) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->deleteTransaction($order_id);
				}

				//删除用户首次拿样的金额
				if(isset($customer_info['sample_bill_amount'])&&$customer_info['sample_bill_amount']==$order_info['total']&&$customer_info['sample_bill_used']==0) {
					if(isset($order_info['is_sample_order'])&&$order_info['is_sample_order']==1){
						$this->addCustomerSampleAmount($order_info['customer_id'], 0);
					}
				}


			}

			$this->cache->delete('product');

			// If order status is 0 then becomes greater than 0 send main html email
			if (!$order_info['order_status_id'] && $order_status_id) {
				// Check for any downloadable products
				$download_status = false;

				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					// Check if there are any linked downloads
					$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if ($product_download_query->row['total']) {
						$download_status = true;
					}
				}

				// Load the language for any mails that might be required to be sent out
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_directory']);
				$language->load('mail/order');

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$order_status = $order_status_query->row['name'];
				} else {
					$order_status = '';
				}

				$subject = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				// HTML Mail
				$data = array();

				$data['title'] = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

				$data['text_greeting'] = sprintf($language->get('text_new_greeting'), $order_info['store_name']);
				$data['text_link'] = $language->get('text_new_link');
				$data['text_download'] = $language->get('text_new_download');
				$data['text_order_detail'] = $language->get('text_new_order_detail');
				$data['text_instruction'] = $language->get('text_new_instruction');
				$data['text_order_id'] = $language->get('text_new_order_id');
				$data['text_date_added'] = $language->get('text_new_date_added');
				$data['text_payment_method'] = $language->get('text_new_payment_method');
				$data['text_shipping_method'] = $language->get('text_new_shipping_method');
				$data['text_email'] = $language->get('text_new_email');
				$data['text_telephone'] = $language->get('text_new_telephone');
				$data['text_ip'] = $language->get('text_new_ip');
				$data['text_order_status'] = $language->get('text_new_order_status');
				$data['text_payment_address'] = $language->get('text_new_payment_address');
				$data['text_shipping_address'] = $language->get('text_new_shipping_address');
				$data['text_product'] = $language->get('text_new_product');
				$data['text_model'] = $language->get('text_new_model');
				$data['text_quantity'] = $language->get('text_new_quantity');
				$data['text_price'] = $language->get('text_new_price');
				$data['text_total'] = $language->get('text_new_total');
				$data['text_footer'] = $language->get('text_new_footer');

				$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
				$data['store_name'] = $order_info['store_name'];
				$data['store_url'] = $order_info['store_url'];
				$data['customer_id'] = $order_info['customer_id'];
				$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

				if ($download_status) {
					$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
				} else {
					$data['download'] = '';
				}

				$data['order_id'] = $order_id;
				$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
				$data['payment_method'] = $order_info['payment_method'];
				$data['shipping_method'] = $order_info['shipping_method'];
				$data['email'] = $order_info['email'];
				$data['telephone'] = $order_info['telephone'];
				$data['ip'] = $order_info['ip'];
				$data['order_status'] = $order_status;

				if ($comment && $notify) {
					$data['comment'] = nl2br($comment);
				} else {
					$data['comment'] = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{fullname}' . "\n" . '{company}' . "\n" . '{address}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{fullname}',
					'{company}',
					'{address}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'fullname' => $order_info['payment_fullname'],
					'company'   => $order_info['payment_company'],
					'address' => $order_info['payment_address'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{fullname}' . "\n" . '{company}' . "\n" . '{address}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{fullname}',
					'{company}',
					'{address}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'fullname' => $order_info['shipping_fullname'],
					'company'   => $order_info['shipping_company'],
					'address' => $order_info['shipping_address'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				// Products
				$data['products'] = array();

				foreach ($order_product_query->rows as $product) {
					$option_data = array();

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}

					$data['products'][] = array(
						'name'     => $product['name'],
						'model'    => $product['model'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				// Vouchers
				$data['vouchers'] = array();

				$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_voucher_query->rows as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				// Order Totals
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $total) {
					$data['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}

				// Text Mail
				$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
				$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
				$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
				$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

				if ($comment && $notify) {
					$text .= $language->get('text_new_instruction') . "\n\n";
					$text .= $comment . "\n\n";
				}

				// Products
				$text .= $language->get('text_new_products') . "\n";

				foreach ($order_product_query->rows as $product) {
					$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
					}
				}

				foreach ($order_voucher_query->rows as $voucher) {
					$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}

				$text .= "\n";

				$text .= $language->get('text_new_order_total') . "\n";

				foreach ($order_total_query->rows as $total) {
					$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
				}

				$text .= "\n";

				if ($order_info['customer_id']) {
					$text .= $language->get('text_new_link') . "\n";
					$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}

				if ($download_status) {
					$text .= $language->get('text_new_download') . "\n";
					$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
				}

				// Comment
				if ($order_info['comment']) {
					$text .= $language->get('text_new_comment') . "\n\n";
					$text .= $order_info['comment'] . "\n\n";
				}

				$text .= $language->get('text_new_footer') . "\n\n";

				//因为百货栈很多客户没有绑定邮箱，所以禁用下单发邮件功能
				if(0) {
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($order_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($html);
					$mail->setText($text);
					$mail->send();
				}
				
				// Admin Alert Mail
				if ($this->config->get('config_order_mail')) {
					$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);

					// HTML Mail
					$data['text_greeting'] = $language->get('text_new_received');

					if ($comment) {
						if ($order_info['comment']) {
							$data['comment'] = nl2br($comment) . '<br/><br/>' . $order_info['comment'];
						} else {
							$data['comment'] = nl2br($comment);
						}
					} else {
						if ($order_info['comment']) {
							$data['comment'] = $order_info['comment'];
						} else {
							$data['comment'] = '';
						}
					}

					$data['text_download'] = '';

					$data['text_footer'] = '';

					$data['text_link'] = '';
					$data['link'] = '';
					$data['download'] = '';

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$html = $this->load->view('default/template/mail/order.tpl', $data);
					}

					// Text
					$text  = $language->get('text_new_received') . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($html);
					$mail->setText($text);
					$mail->send();

					// Send to additional alert emails
					$emails = explode(',', $this->config->get('config_mail_alert'));

					foreach ($emails as $email) {
						if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
							$mail->setTo($email);
							$mail->send();
						}
					}
				}
			}

			// If order status is not 0 then send update text email
			if ($order_info['order_status_id'] && $order_status_id && $notify) {
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_directory']);
				$language->load('mail/order');

				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . "\n\n";
					$message .= $order_status_query->row['name'] . "\n\n";
				}

				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}

				if ($comment) {
					$message .= $language->get('text_update_comment') . "\n\n";
					$message .= strip_tags($comment) . "\n\n";
				}

				$message .= $language->get('text_update_footer');
				
				if (0) {
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($order_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setText($message);
					$mail->send();
				}
				
			}
		}

		$this->event->trigger('post.order.history.add', $order_id);
	}

	public function addCustomerSampleAmount($customer_id, $total) {
		$query = M('customer')
		->data(array('sample_bill_amount'=>(float)$total))
		->where(array('customer_id'=>(int)$customer_id))
		->save();
	}
	public function checkOrderStatusIdByOrderId($order_id){
		$order_status_name = '全部发货'; 
		$order = M("order_status");
		$result = $order
		->alias('os')
		->join(' `order` o ON o.order_status_id = os.order_status_id','right')
		->where("o.order_id = ".$order_id." and os.name = '".$order_status_name."'")
		->find();
		if($result){
			$return = 1;
		}else{
			$return = 0;
		}
		return $return;
	}
	public function findOrderStatus($status){
		$sql = "SELECT order_status_id FROM `".DB_PREFIX."order_status` WHERE name = '".$status."'";
		$query = $this->db->query($sql);
		return $query->row['order_status_id'];
		// $order_status = M('order_status');
		// $where['name'] = $status;
		// $result = $order_status
		// ->where($where)
		// ->field('order_status_id')
		// ->find();
		// if($result){
		// 	return $result['order_status_id'];
		// }else{
		// 	return 0;
		// }
	}
	public function getOrdersByOederStatusId($order_status_id){
		$order_history = M('order_history');
		$ordersdata = $order_history
		->alias('oh')
		->join(' `order` o ON o.order_id = oh.order_id and o.order_status_id = oh.order_status_id','RIGHT')
		->where('o.order_status_id = '.$order_status_id.' and datediff( NOW() , oh.date_added) >=2')
		->field('oh.order_id')
		->select();
		return $ordersdata;
	}

	public function confirmPay($order_id){
		$order_model = M('order');
		$map['order_id'] = $order_id;
		$data['is_pay'] = 1;
		$order_model 
		->where($map)
		->data($data)
		->save();
		
	}

	public function setIsPay($order_id){
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET is_pay = 1 where order_id=".$order_id);
		
	}
	//应收
	public function updateMoneystatus($order_id){
        $model=M('money_in');
		$data = array(
			'status'=>4,
			'date_modified'=>date('Y-m-d H:i:s')
			);
 			$money_in_id =  $model->data($data)->where(array('refer_id'=>$order_id,'refer_type_id'=>1))->save();
	}
    

    public function getMoneyById($orderID){
        $model=M('money_in');
        $where=array('refer_type_id'=>1,'refer_id'=>$orderID);
        $res=$model->where($where)->find();
        return $res;  
    }

	public function updatemoney($res,$order_id){
        $this->db->query(" UPDATE `money_in` SET `received`='".$res['total']."',`status`='2',`pay_time`='" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE `refer_id` = ".$order_id." AND `refer_type_id` = 1");
	}
	public function getOrderById($orderID){
		$sql = "SELECT `order`.total FROM" . DB_PREFIX . " `order` WHERE `order_id` =".$orderID." LIMIT 1"; 
		$query=$this->db->query($sql);
		return $query->row; 
    }
    

    public function addMoneyOrder($res){
        $model=M('money_in');
		$data = array(
			'payer_id'=>$res['customer_id'], 
			'refer_type_id'=>$res['payment_company'], 
			'refer_type_id'=>1, 
			'refer_id'=>$res['order_id'], 
			'refer_total'=>$res['total'], 
			'payer_name'=>$res['payment_company'].'('.$res['shipping_address'].')', 
			'status'=>1, 
			'trader_type'=>2,
			'date_added'=>date('Y-m-d H:i:s')
	  		);
 			$money_in_id =  $model->data($data)->add();
	}

    public function getMoneyOrder($orderID){
    	 $model=M('order');
            $where=array('order_id'=>$orderID);
            $res=$model->field('`order`.order_id, `order`.ori_total as total, `order`.customer_id, `order`.payment_company,`order`.shipping_address,`order`.is_pay,`order`.total as totalp')
					->where($where)->find();
         return $res;
    }


    public function getStock($order_id){
    	 $model=M('stock_out');
    	 $where =  "refer_id=".$order_id." and refer_type_id=1 and status!=0";
            $res=$model->field('stock_out.out_id')
					->where($where)->find();

         return $res;
    }

	public function addProductInitiai($order_id){
  	$order_product_model = M('order_product');
  	$map['op.order_id'] = $order_id;
  	$order_product_data = $order_product_model
  	->alias('op')
  	->join('left join `order_option` oo ON oo.order_product_id=op.order_product_id')
  	->join('left join `order` o ON o.order_id=op.order_id')
  	->field('op.product_id,oo.product_option_value_id as option_id,o.logcenter_id,op.quantity,op.order_product_id')
  	->where($map)
  	->select();

  	$product_initiai_model = M('product_initiai');
  	foreach ($order_product_data as $key => $value) {
  		$initiai_map['option_id'] = $value['option_id']?$value['option_id']:-1;
  		$initiai_map['product_id'] = $value['product_id'];
  		$initiai_map['logcenter_id'] = $value['logcenter_id'];
  		$initiai_data = $product_initiai_model->where($initiai_map)->find();
  		if($initiai_data){
  			$edit_initiai_data['real_initiai'] = $initiai_data['real_initiai'] - $value['quantity'];
  			$product_initiai_model->data($edit_initiai_data)->where($initiai_map)->save();
  		}else{
  			$edit_initiai_data['real_initiai'] = 0 - $value['quantity'];
  			$edit_initiai_data['option_id'] = $value['option_id']?$value['option_id']:-1;
  			$edit_initiai_data['product_id'] = $value['product_id'];
  			$edit_initiai_data['logcenter_id'] = $value['logcenter_id'];
  			$edit_initiai_data['date_added'] = date('Y-m-d h:i:s',time());
  			$edit_initiai_data['initial_number'] = 0 ;
  			$product_initiai_model->data($edit_initiai_data)->add();
  		}
  	}
  }

  public function getRecommendUserId(){
  	$customer_id = $this->customer->getId();
  	$recommend_usr_id = M('customer')->where('customer_id='.$customer_id)->getField('user_id');
  	if($recommend_usr_id){
  		return $recommend_usr_id;
  	}else{
  		return  0;
  	}
  }
  
  public function addtradeNo($order_id,$tradeNo='0'){
	  $model = M('order');
	  $data['yijipay_tradeNo'] = $tradeNo;
	  $data['is_pay'] = '1';
	  $model->where('order_id='.$order_id)->data($data)->save();
  }

  public function addtradeNohistory($order_id,$params){
	  $model = M('order_history');
	  $data['order_id'] = $order_id;
	  $data['order_status_id'] = '11';
	  $data['comment'] = '成功退款'.$params['refundAmount'];
	  $data['date_added'] = $params['refundFinishTime'];
	  $model->data($data)->add();
  }

    /*
     * @author sonicsjh
     */
    public function getHistoryProductsByCustomer(){
        $orderIds = $this->_getSuccOrderIdsByCustomer();
        if (false === $orderIds || '' == $orderIds)  return false;
        $productsInfo = $this->_getHistoryProductsByOrderIds($orderIds);
        if (count($productsInfo) < 1)  return false;
        return $productsInfo;
    }

    /*
     * @author sonicsjh
     */
    protected function _getSuccOrderIdsByCustomer(){
        $customerId = $this->customer->getId();//获取当前用户id
        //$customerId = 3260;//测试用id
        //$orderStatusIds = array(5,7,17,18,19);
        $orderExpireDays = date('Y-m-d 00:00:00', strtotime('-30 day'));
        
        //$sql = "SELECT GROUP_CONCAT(`order_id`) AS order_ids FROM `".DB_PREFIX."order` WHERE `customer_id`='".$customerId."' AND `order_status_id` IN (".join(',',$orderStatusIds).") AND `date_added`>'".$orderExpireDays."'";
        $sql = "SELECT GROUP_CONCAT(`order_id`) AS order_ids FROM `".DB_PREFIX."order` WHERE `customer_id`='".$customerId."' AND `date_added`>'".$orderExpireDays."'";
        $orderResult = $this->db->query($sql);
        if ($orderResult->num_rows)
            return $orderResult->row['order_ids'];
        return false;
    }

    /*
     * @author sonicsjh
     */
    protected function _getHistoryProductsByOrderIds($orderIds){
        $ret = array();
        /* OO表历史数据不值得信赖，从POV表取数据
        $sql = "SELECT 
                OP.`order_id`,OP.`product_id`,
                OO.`product_option_id`,OO.`product_option_value_id`,
                P.`minimum`,P.`status`,P.`quantity` AS P_QTY,
                POV.`quantity` AS POV_QTY
            FROM 
                `order_product` AS OP 
                LEFT JOIN `order_option` AS OO ON (OP.`order_product_id`=OO.`order_product_id`) 
                LEFT JOIN `product` AS P ON (OP.`product_id`=P.`product_id`) 
                LEFT JOIN `product_option_value` AS POV ON (OO.`product_option_value_id`=POV.`product_option_value_id`) 
            WHERE 
                OP.`order_id` IN (".$orderIds.")
            ORDER BY OP.`order_id` ASC,OP.`order_product_id` DESC";
        */
        $sql = "SELECT 
                OP.`order_id`,OP.`product_id`,
                P.`minimum`,P.`status`,P.`quantity` AS P_QTY,
                POV.`product_option_id`,POV.`product_option_value_id`,POV.`quantity` AS POV_QTY
            FROM 
                `order_product` AS OP 
                LEFT JOIN `product` AS P ON (OP.`product_id`=P.`product_id`) 
                LEFT JOIN `product_option_value` AS POV ON (OP.`product_id`=POV.`product_id`) 
            WHERE 
                OP.`order_id` IN (".$orderIds.")
                AND P.`status`='1'
            ORDER BY OP.`order_id` ASC,OP.`order_product_id` DESC";
        $orderProductResult = $this->db->query($sql);
        $ret = array();
        if ($orderProductResult->num_rows){
            foreach ($orderProductResult->rows as $row){

                //P表和POV表都有quantity字段，待处理。
                if (intval($row['product_option_id']) < 1 || intval($row['product_option_value_id']) < 1){//商品缺少选项信息
                    continue;
                }
                if($this->checkArea($row['product_id']) == 0){
							continue;
				}
                //if (intval($row['P_QTY']) < 1 || intval($row['POV_QTY']) < 1){//不再判断 p 表 qty
                if (intval($row['POV_QTY']) < 1){
                    continue;
                }
                if (intval($row['minimum']) > intval($row['P_QTY']) || intval($row['minimum']) > intval($row['POV_QTY'])){
                    //continue;库存不足的商品也可以加入购物车
                }
                $ret['productMinNum'][$row['product_id']] = $row['minimum'];
                $sku[$row['product_id'].'_'.$row['product_option_id'].'_'.$row['product_option_value_id']] = $row['order_id'];
            }
            arsort($sku, 1);
            $ret['sku'] = $sku;
        }
        return $ret;
    }

    /*
     * @author sonicsjh
     */
    public function getOrderProductsByOrderId($orderId=0){
        if (0 == $orderId) {
            return false;
        }
        $productsInfo = $this->_getOrderProductsByOrderId($orderId);
        if (count($productsInfo) < 1)  return false;
        return $productsInfo;
    }

    public function _getOrderProductsByOrderId($orderId){
        $sql = "SELECT 
                OP.`order_id`,OP.`product_id`,OP.`quantity`,
                P.`status`,P.`quantity` AS P_QTY,
                POV.`product_option_id`,POV.`product_option_value_id`,POV.`quantity` AS POV_QTY
            FROM 
                `order_product` AS OP 
                LEFT JOIN `product` AS P ON (OP.`product_id`=P.`product_id`) 
                LEFT JOIN `product_option_value` AS POV ON (OP.`product_id`=POV.`product_id`) 
            WHERE 
                OP.`order_id`='".$orderId."'
                AND P.`status`='1'
            ORDER BY OP.`order_product_id` ASC";
        $orderProductResult = $this->db->query($sql);
        $ret = array();
        if ($orderProductResult->num_rows){
            foreach ($orderProductResult->rows as $row){
                $ret[$row['product_id'].'_'.$row['product_option_id'].'_'.$row['product_option_value_id']] = $row['quantity'];
            }
        }
        return $ret;
        /*
         * 不再判断库存数量
        if ($orderProductResult->num_rows){
            foreach ($orderProductResult->rows as $row){
                //P表和POV表都有quantity字段，待处理。
                if (intval($row['product_option_id']) < 1 || intval($row['product_option_value_id']) < 1){//商品缺少选项信息
                    continue;
                }
                if (intval($row['P_QTY']) < 1 || intval($row['POV_QTY']) < 1){
                    continue;
                }
                if (intval($row['status']) != 1){
                    continue;
                }
                if (intval($row['quantity']) > intval($row['P_QTY']) || intval($row['quantity']) > intval($row['POV_QTY'])){
                    //continue;库存不足的商品也可以加入购物车
                }
                $ret[$row['product_id'].'_'.$row['product_option_id'].'_'.$row['product_option_value_id']] = $row['quantity'];
            }
        }
        return $ret;
        */
    }

  //   public function getpofromorder($order_id,$user_id){
  //   	$order_info = $this->getOrder($order_id);

  //   	if($order_info){
  //   		$part_stock = $order_info['warehouse_id'];
  //  			$main_stock = 10;
  //  			$wuxi_stock = 16;

  //  			$sql = "SELECT user_id FROM `".DB_PREFIX."logcenters` WHERE logcenter_id = 10";
  //  			$user_id_query = $this->db->query($sql);
  //  			$user_id = $user_id_query->row['user_id'];

  //  			$sql = "SELECT op.product_code,op.quantity,pov.product_option_value_id,ovd.name AS ovdname,IF(p2.packing_no,p2.packing_no,p.packing_no) AS packing_no,IF(p2.sku,p2.sku,p.sku) AS sku,v.product_cost AS price,IF(p2.product_id,p2.product_id,p.product_id) AS product_id,pd.name,vs.user_id AS vendor_id FROM `".DB_PREFIX."order_product` op LEFT JOIN `".DB_PREFIX."product_option_value` pov ON op.product_code = pov.product_code LEFT JOIN `".DB_PREFIX."option_value_description` ovd ON pov.option_value_id = ovd.option_value_id LEFT JOIN `".DB_PREFIX."product` p ON op.product_code = p.product_code LEFT JOIN `".DB_PREFIX."product` p2 ON pov.product_id = p2.product_id LEFT JOIN `".DB_PREFIX."product_description` pd ON IF(p2.product_id,p2.product_id,p.product_id) = pd.product_id LEFT JOIN `".DB_PREFIX."vendor` v ON IF(p2.product_id,p2.product_id,p.product_id) = v.vproduct_id LEFT JOIN `".DB_PREFIX."vendors` vs ON v.vendor = vs.vendor_id WHERE op.order_id = '".$order_id."' AND op.product_code != ''";

  //  			$result_query = $this->db->query($sql);

  //  			$sql = "SELECT product_code,product_quantity FROM `".DB_PREFIX."stock_out_detail` WHERE out_id in (SELECT out_id FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$order_id."')";

  //  			$result_query_arr = $result_query->rows;

		// 	$result_stock_out_detail = $this->db->query($sql);

		// 	$sql = "SELECT product_code,qty FROM `".DB_PREFIX."po_product` WHERE po_id in (SELECT id FROM `".DB_PREFIX."po` WHERE included_order_ids like '%".$order_id."%')";

		// 	$result_po_product = $this->db->query($sql);

  //  			foreach($result_query_arr as $key=>$val){
  //  				$remaindnum = intval($val['quantity']);
  //  				foreach($result_stock_out_detail->rows as $val2){
  //  					if($val2['product_code'] == $val['product_code']){
  //  						$remaindnum -= $val2['product_quantity'];
  //  					}
  //  				}

  //  				foreach($result_po_product->rows as $val3){
  //  					if($val3['product_code'] == $val['product_code']){
  //  						$remaindnum -= $val3['qty'];
  //  					}
  //  				}

  //  				//echo $remaindnum;exit;

  //  				$sql = "SELECT COUNT(id) AS count FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '16' AND product_code = '".$val['product_code']."'";

		// 		$query = $this->db->query($sql);

		// 		$count = $query->row['count'];

		// 		if($count > 0){
		// 			$wuximark = ture;
		// 		}

		// 		if($order_info['warehouse_id'] != 10 && $order_info['warehouse_id'] != 16){
		// 			$sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$order_info['warehouse_id']."' AND product_code = '".$val['product_code']."'";

		// 			$query_quantity = $this->db->query($sql);
		// 		}

		// 		$available_quantity = intval($query_quantity->row['available_quantity']);

		// 		if($remaindnum > $available_quantity){
		// 			$remaindnum -= $available_quantity;

		// 			$sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$main_stock."' AND product_code = '".$val['product_code']."'";

		// 			$query_quantity = $this->db->query($sql);

		// 			$available_quantity = intval($query_quantity->row['available_quantity']);

		// 			if($remaindnum > $available_quantity){
		// 				$remaindnum -= $available_quantity;

		// 				if($wuximark){
		// 					$sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$wuxi_stock."' AND product_code = '".$val['product_code']."'";

		// 					$query_quantity = $this->db->query($sql);

		// 					$available_quantity = intval($query_quantity->row['available_quantity']);

		// 					if($remaindnum > $available_quantity){
		// 						$result_query_arr[$key]['quantity'] = $remaindnum - $available_quantity;
		// 					}else{
		// 						$result_query_arr[$key]['quantity'] = 0;
		// 					}

		// 				}else{
		// 					$result_query_arr[$key]['quantity'] = $remaindnum;
		// 				}
		// 			}else{
		// 				$result_query_arr[$key]['quantity'] = 0;
		// 			}
		// 		}else{
		// 			$result_query_arr[$key]['quantity'] = 0;
		// 		}
  //  			}

  //  			$vendor_arr = array();

  //  			foreach($result_query_arr as $key=>$val){

  //  				$flag = true;

  //  				foreach($vendor_arr as $val2){
  //  					if($val2 == $val['vendor_id']){
  //  						$flag = false;
  //  					}
  //  				}

  //  				if($flag){
  //  					$vendor_arr[] = $val['vendor_id'];
  //  				}
  //  			}

  //  			$po_confirm_mark = false;

  //  			foreach($vendor_arr as $val){

  //  				$total = 0;

  //  				foreach($result_query_arr as $val2){
  //  					if($val == $val2['vendor_id']){
  //  						$total += floatval($val2['quantity']*$val2['price']);
  //  					}
  //  				}

  //  				$total = round($total,2);

  //  				if($total != 0){
	 //   				$sql = "INSERT INTO `".DB_PREFIX."po` SET user_id = '".$user_id."',date_added = NOW(),status = 0,vendor_id = '".$val."',total = '".$total."',included_order_ids = '".$order_id."',warehouse_id = 10";

	 //   				$this->db->query($sql);

	 //   				$id = $this->db->getLastId();

	 //   				$po_confirm_mark = true;
	 //   			}

  //  				foreach($result_query_arr as $val2){
  //  					if($val == $val2['vendor_id']){
  //  						if($val2['quantity'] != 0){
	 //   						$sql = "INSERT INTO `".DB_PREFIX."po_product` SET po_id = '".$id."',product_id = '".$val2['product_id']."',qty = '".$val2['quantity']."',delivered_qty = 0,unit_price = '".$val2['price']."',price = '".count(floatval($val2['quantity']*$val2['price']),2)."',status = 0,name = '".$val2['name']."',sku = '".$val2['sku']."',sort_order = 0,option_id = '".$val2['product_option_value_id']."',option_name = '".$val2['ovdname']."',packing_no = '".$val2['packing_no']."',product_code = '".$val2['product_code']."'";

	 //   						$this->db->query($sql);
	 //   					}
  //  					}
  //  				}
  //  			}

  //  			if($po_confirm_mark){
	 //   			return array(
	 //   				'success' => '采购单下单成功'
	 //   			);
	 //   		}else{
	 //   			return array(
	 //   				'success' => '采购单已下满足够数量'
	 //   			);
	 //   		}

  //   	}else{
		// 	return array(
		// 		'error' => '找不到单据'
		// 	);
		// }
  //   }

    //订单配货主逻辑
    public function getstockoutfromorder($order_id){
        $order_info = $this->getOrder($order_id);
        $main_stock_id = 10;
        $wuxi_stock_id = 4;
        if($order_info){
            $array_part_stock = array('warehouse_id' => $order_info['warehouse_id']);
            $array_main_stock = array('warehouse_id' => $main_stock_id);
            $array_wuxi_stock = array('warehouse_id' => $wuxi_stock_id);
            $order_product_lack = array();
            $order_product_group_lack = array();
            $all_data = array(
                'status' => 'true'
            );
            //获取订单商品信息
            //$sql = "SELECT order_product_id,product_code,quantity,pay_price,pay_total FROM `".DB_PREFIX."order_product` WHERE order_id = '".$order_info['order_id']."'";
            //获取单品销售价、摊销价、销售总价、摊销总价（销售总价和摊销总价看上去没用上）
            $sql = "SELECT order_product_id,product_code,quantity,price,pay_price,total,pay_total FROM `".DB_PREFIX."order_product` WHERE order_id = '".$order_info['order_id']."'";
            $result_order_product = $this->db->query($sql);

            //获取已有出库单的商品信息
            $sql = "SELECT product_code,product_quantity,counter_id FROM `".DB_PREFIX."stock_out_detail` WHERE out_id in (SELECT out_id FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$order_id."' AND status != 0)";
            $result_stock_out_detail = $this->db->query($sql);

            foreach($result_order_product->rows as $val){
                //$sql = "SELECT order_product_group_id,product_code,quantity,pay_price,pay_total FROM `".DB_PREFIX."order_product_group` WHERE order_product_id = '".$val['order_product_id']."'";
                //获取商品组内单品销售价、摊销价、销售总价、摊销总价（销售总价和摊销总价看上去没用上）
                $sql = "SELECT order_product_group_id,product_code,quantity,price,pay_price,total,pay_total FROM `".DB_PREFIX."order_product_group` WHERE order_product_id = '".$val['order_product_id']."'";
                $order_group_query = $this->db->query($sql);
                if(!empty($order_group_query->rows)){//判断是否商品组
                    foreach($order_group_query->rows as $val){
                        $temp_all_arr = NULL;
                        $temp_all_arr = $this->stock_out_process($val,$result_stock_out_detail,$array_part_stock,$array_main_stock,$array_wuxi_stock,$order_info,$main_stock_id,$wuxi_stock_id,$all_data,1,$order_product_lack,$order_product_group_lack);
                        $array_part_stock = $temp_all_arr['array_part_stock'];//订单默认仓库配货信息
                        $array_main_stock = $temp_all_arr['array_main_stock'];//永康总仓配货信息
                        $array_wuxi_stock = $temp_all_arr['array_wuxi_stock'];//无锡仓配货信息
                        $order_product_lack = $temp_all_arr['order_product_lack'];//单品缺货数量
                        $order_product_group_lack = $temp_all_arr['order_product_group_lack'];//商品组内单品缺货数量
                        $all_data = $temp_all_arr['all_data'];
                    }
                }else{
                    $temp_all_arr = NULL;
                    $temp_all_arr = $this->stock_out_process($val,$result_stock_out_detail,$array_part_stock,$array_main_stock,$array_wuxi_stock,$order_info,$main_stock_id,$wuxi_stock_id,$all_data,0,$order_product_lack,$order_product_group_lack);
                    $array_part_stock = $temp_all_arr['array_part_stock'];//订单默认仓库配货信息
                    $array_main_stock = $temp_all_arr['array_main_stock'];//永康总仓配货信息
                    $array_wuxi_stock = $temp_all_arr['array_wuxi_stock'];//无锡仓配货信息
                    $order_product_lack = $temp_all_arr['order_product_lack'];//单品缺货数量
                    $order_product_group_lack = $temp_all_arr['order_product_group_lack'];//商品组内单品缺货数量
                    $all_data = $temp_all_arr['all_data'];
                }
            }
            $all_data['array_part_stock'] = $array_part_stock;
            $all_data['array_main_stock'] = $array_main_stock;
            $all_data['array_wuxi_stock'] = $array_wuxi_stock;
            $all_data['order_product_lack'] = $order_product_lack;
            $all_data['order_product_group_lack'] = $order_product_group_lack;
            return $all_data;
        }else{
            return array(
                'status' => 'noid'
            );
        }
    }

    /*
     * 配货函数
     * $val                         包含 opId，pCode，qty，price，payPrice，total，payTotal 的 OP 信息
     * $result_stock_out_detail     包含 pCode，qty，opgId 的 SOD 信息
     * $array_part_stock            预计从 默认仓 发货的配货清单 * 配货清单数组从仅记录摊销价、摊销总价变成记录销售价和总价、摊销价和总价
     * $array_main_stock            预计从 永康仓 发货的配货清单 * 配货清单数组从仅记录摊销价、摊销总价变成记录销售价和总价、摊销价和总价
     * $array_wuxi_stock            预计从 无锡仓 发货的配货清单 * 配货清单数组从仅记录摊销价、摊销总价变成记录销售价和总价、摊销价和总价
     * $order_info                  订单主表 O 信息
     * $main_stock_id               永康仓 PK
     * $wuxi_stock_id               无锡仓 PK
     * $all_data                    array(status=>false) 或 未定义返回
     * $type                        商品组标记位，1是0否
     * $order_product_lack          单品缺货信息清单
     * $order_product_group_lack    商品组内单品缺货清单
     */
    public function stock_out_process($val,$result_stock_out_detail,$array_part_stock,$array_main_stock,$array_wuxi_stock,$order_info,$main_stock_id,$wuxi_stock_id,$all_data,$type,$order_product_lack,$order_product_group_lack){
        $wuximark = false;//？判断默认从是否无锡仓？感觉变量毫无意义，让逻辑变得很混乱。。。
        $query_quantity = NULL;//根据 wId，pCode 获取 可用数量
        $status_query = NULL;//根据 wId 获取仓库状态
        $query = NULL;
        $remaindnum = intval($val['quantity']);//商品采购数量
        //根据所有已有出库单，扣减采购数量
        foreach($result_stock_out_detail->rows as $val2){
            if($type){
                if($val['product_code'] == $val2['product_code'] && $val['order_product_group_id'] == $val2['counter_id']){
                    $remaindnum -= $val2['product_quantity'];
                    $remaindnum = $remaindnum < 0?0:$remaindnum;
                }
            }else{
                if($val['product_code'] == $val2['product_code']){
                    $remaindnum -= $val2['product_quantity'];
                    $remaindnum = $remaindnum < 0?0:$remaindnum;
                }
            }
        }
        //根据所有已有出库单，扣减采购数量。至此，$remaindnum = 未发货数量，最小为 0

        //判断无锡仓是否有商品库存
        $sql = "SELECT status FROM `".DB_PREFIX."warehouse` WHERE warehouse_id = '".$wuxi_stock_id."'";
        $status_query = $this->db->query($sql);
        if(intval($status_query->row['status'])){
            $sql = "SELECT COUNT(id) AS count FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$wuxi_stock_id."' AND product_code = '".$val['product_code']."'";
            $query = $this->db->query($sql);
            $count = $query->row['count'];
            if($count > 0){
                $wuximark = true;
            }
        }
        //？$wuximark 看上去是标记位，判断是否要走无锡仓配货逻辑？

        //默认仓配货逻辑
        $sql = "SELECT status FROM `".DB_PREFIX."warehouse` WHERE warehouse_id = '".$order_info['warehouse_id']."'";
        $status_query = $this->db->query($sql);
        if(intval($status_query->row['status'])){
            if($order_info['warehouse_id'] == $wuxi_stock_id){
                if($wuximark){
                    $sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$order_info['warehouse_id']."' AND product_code = '".$val['product_code']."'";
                    $query_quantity = $this->db->query($sql);
                }
            }else{
                $sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$order_info['warehouse_id']."' AND product_code = '".$val['product_code']."'";
                $query_quantity = $this->db->query($sql);
            }
        }
        $available_quantity = intval($query_quantity->row['available_quantity']);//仓库内商品可用数量
        //除了仓库的变量名，逻辑没区别（start）
        if($order_info['warehouse_id'] != $main_stock_id && $order_info['warehouse_id'] != $wuxi_stock_id){//默认仓 != 永康或无锡
            foreach($array_part_stock['products'] as $val3){
                if($val['product_code'] == $val3['product_code']){
                    $available_quantity -= $val3['product_quantitiy'];
                    $available_quantity = $available_quantity<0?0:$available_quantity;
                }
            }
        }elseif($order_info['warehouse_id'] == $main_stock_id){//默认仓 = 永康
            foreach($array_main_stock['products'] as $val3){
                if($val['product_code'] == $val3['product_code']){
                    $available_quantity -= $val3['product_quantitiy'];
                    $available_quantity = $available_quantity<0?0:$available_quantity;
                }
            }
        }else{//默认仓 = 无锡
            foreach($array_wuxi_stock['products'] as $val3){
                if($val['product_code'] == $val3['product_code']){
                    $available_quantity -= $val3['product_quantitiy'];
                    $available_quantity = $available_quantity<0?0:$available_quantity;
                }
            }
        }
        //除了仓库的变量名，逻辑没区别（end）
        //由于单品 pCode 和 商品组pCode 可能重复，所以 $available_quantity 需要减去已分配出库的数量 = 仓库实际可发数量
        if($remaindnum > $available_quantity){//未发数量 > 可发数量
            if($available_quantity > 0){//可发数量 > 0 ,实际配货数量 = 可发数量，临时保存配货信息到数组
                //除了仓库的变量名，逻辑没区别（start）
                if($order_info['warehouse_id'] != $main_stock_id && $order_info['warehouse_id'] != $wuxi_stock_id){
                    if($type){
                        $array_part_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_part_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                        );
                    }
                }elseif($order_info['warehouse_id'] == $main_stock_id){
                    if($type){
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price']
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                        );
                    }
                }else{
                    if($type){
                        $array_wuxi_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_wuxi_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $available_quantity,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $available_quantity*$val['price'],
                            'pay_products_money' => $available_quantity*$val['pay_price'],
                        );
                    }
                }
                //除了仓库的变量名，逻辑没区别（end）
            }
            $remaindnum -= $available_quantity;//更新未发数量
            //默认仓!=永康时，尝试从永康配货
            if($order_info['warehouse_id'] != $main_stock_id){
                $sql = "SELECT status FROM `".DB_PREFIX."warehouse` WHERE warehouse_id = '".$main_stock_id."'";
                $status_query = $this->db->query($sql);
                if(intval($status_query->row['status'])){
                    $sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$main_stock_id."' AND product_code = '".$val['product_code']."'";
                    $query_quantity = $this->db->query($sql);
                    $available_quantity = intval($query_quantity->row['available_quantity']);
                }else{
                    $available_quantity = 0;
                }
                foreach($array_main_stock['products'] as $val3){
                    if($val['product_code'] == $val3['product_code']){
                        $available_quantity -= $val3['product_quantitiy'];
                        $available_quantity = $available_quantity<0?0:$available_quantity;
                    }
                }
            }else{
                $available_quantity = 0;
            }
            //获取永康仓商品的数量，需要扣掉已分配出库的数量
            if($remaindnum > $available_quantity){//未发数量 > 可发数量
                if($wuximark && $order_info['warehouse_id'] != $wuxi_stock_id){//从永康和无锡配货
                    //永康仓配货
                    if($available_quantity > 0){
                        if($type){
                            $array_main_stock['products'][] = array(
                                'product_code' => $val['product_code'],
                                'product_quantitiy' => $available_quantity,
                                /*
                                'product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['pay_price'],
                                */
                                'product_price' => $val['price'],
                                'pay_product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['price'],
                                'pay_products_money' => $available_quantity*$val['pay_price'],
                                'counter_id' => $val['order_product_group_id'],
                            );
                        }else{
                            $array_main_stock['products'][] = array(
                                'product_code' => $val['product_code'],
                                'product_quantitiy' => $available_quantity,
                                /*
                                'product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['pay_price'],
                                */
                                'product_price' => $val['price'],
                                'pay_product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['price'],
                                'pay_products_money' => $available_quantity*$val['pay_price'],
                            );
                        }
                    }
                    $remaindnum -= $available_quantity;//计算未配货数量
                    //获取无锡仓库存（减去已配货数量）
                    $sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$wuxi_stock_id."' AND product_code = '".$val['product_code']."'";
                    $query_quantity = $this->db->query($sql);
                    $available_quantity = intval($query_quantity->row['available_quantity']);
                    foreach($array_wuxi_stock['products'] as $val3){
                        if($val['product_code'] == $val3['product_code']){
                            $available_quantity -= $val3['product_quantitiy'];
                            $available_quantity = $available_quantity<0?0:$available_quantity;
                        }
                    }
                    //无锡仓配货
                    if($remaindnum > $available_quantity){//配货数量=可用数量
                        if($available_quantity > 0){
                            if($type){
                                $array_wuxi_stock['products'][] = array(
                                    'product_code' => $val['product_code'],
                                    'product_quantitiy' => $available_quantity,
                                    /*
                                    'product_price' => $val['pay_price'],
                                    'products_money' => $available_quantity*$val['pay_price'],
                                    */
                                    'product_price' => $val['price'],
                                    'pay_product_price' => $val['pay_price'],
                                    'products_money' => $available_quantity*$val['price'],
                                    'pay_products_money' => $available_quantity*$val['pay_price'],
                                    'counter_id' => $val['order_product_group_id'],
                                );
                            }else{
                                $array_wuxi_stock['products'][] = array(
                                    'product_code' => $val['product_code'],
                                    'product_quantitiy' => $available_quantity,
                                    /*
                                    'product_price' => $val['pay_price'],
                                    'products_money' => $available_quantity*$val['pay_price'],
                                    */
                                    'product_price' => $val['price'],
                                    'pay_product_price' => $val['pay_price'],
                                    'products_money' => $available_quantity*$val['price'],
                                    'pay_products_money' => $available_quantity*$val['pay_price'],
                                );
                            }
                        }
                        $remaindnum -= $available_quantity;//计算未配货数量
                        //记录最终缺货数量
                        if($type){
                            $order_product_group_lack[$val['order_product_group_id']] = $remaindnum;
                        }else{
                            $order_product_lack[$val['order_product_id']] = $remaindnum;
                        }
                        $all_data['status'] = 'false';//标记位，记录是否全部配货
                    }else{//配货数量=待配货数量
                        if($remaindnum > 0){
                            if($type){
                                $array_wuxi_stock['products'][] = array(
                                    'product_code' => $val['product_code'],
                                    'product_quantitiy' => $remaindnum,
                                    /*
                                    'product_price' => $val['pay_price'],
                                    'products_money' => $remaindnum*$val['pay_price'],
                                    */
                                    'product_price' => $val['price'],
                                    'pay_product_price' => $val['pay_price'],
                                    'products_money' => $remaindnum*$val['price'],
                                    'pay_products_money' => $remaindnum*$val['pay_price'],
                                    'counter_id' => $val['order_product_group_id'],
                                );
                            }else{
                                $array_wuxi_stock['products'][] = array(
                                    'product_code' => $val['product_code'],
                                    'product_quantitiy' => $remaindnum,
                                    /*
                                    'product_price' => $val['pay_price'],
                                    'products_money' => $remaindnum*$val['pay_price'],
                                    */
                                    'product_price' => $val['price'],
                                    'pay_product_price' => $val['pay_price'],
                                    'products_money' => $remaindnum*$val['price'],
                                    'pay_products_money' => $remaindnum*$val['pay_price'],
                                );
                            }
                        }
                        //单品无缺货
                        if($type){
                            $order_product_group_lack[$val['order_product_group_id']] = 0;
                        }else{
                            $order_product_lack[$val['order_product_id']] = 0;
                        }
                    }
                }else{//仅从永康配货
                    if($available_quantity > 0){
                        if($type){
                            $array_main_stock['products'][] = array(
                                'product_code' => $val['product_code'],
                                'product_quantitiy' => $available_quantity,
                                /*
                                'product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['pay_price'],
                                */
                                'product_price' => $val['price'],
                                'pay_product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['price'],
                                'pay_products_money' => $available_quantity*$val['pay_price'],
                                'counter_id' => $val['order_product_group_id'],
                            );
                        }else{
                            $array_main_stock['products'][] = array(
                                'product_code' => $val['product_code'],
                                'product_quantitiy' => $available_quantity,
                                /*
                                'product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['pay_price'],
                                */
                                'product_price' => $val['price'],
                                'pay_product_price' => $val['pay_price'],
                                'products_money' => $available_quantity*$val['price'],
                                'pay_products_money' => $available_quantity*$val['pay_price'],
                            );
                        }
                    }
                    $remaindnum -= $available_quantity;
                    //记录未配货数量
                    if($type){
                        $order_product_group_lack[$val['order_product_group_id']] = $remaindnum;
                    }else{
                        $order_product_lack[$val['order_product_id']] = $remaindnum;
                    }
                    $all_data['status'] = 'false';//标记位，记录是否全部配货
                }
            }else{//永康可用数量 > 未配货数量
                if($remaindnum > 0){//已未配货数量配货
                    if($type){
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                        );
                    }
                }
                //单品无缺货
                if($type){
                    $order_product_group_lack[$val['order_product_group_id']] = 0;
                }else{
                    $order_product_lack[$val['order_product_id']] = 0;
                }
            }
        }else{//未发货数量 < 可用数量
            if($remaindnum > 0){//未发货数量 > 0 ，实际配货数量 = 未发货数量，临时保存配货信息到数组
                //除了仓库的变量名，逻辑没区别（start）
                if($order_info['warehouse_id'] != $main_stock_id && $order_info['warehouse_id'] != $wuxi_stock_id){
                    if($type){
                        $array_part_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_part_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                        );
                    }
                }elseif($order_info['warehouse_id'] == $main_stock_id){
                    if($type){
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_main_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                        );
                    }
                }else{
                    if($type){
                        $array_wuxi_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                            'counter_id' => $val['order_product_group_id'],
                        );
                    }else{
                        $array_wuxi_stock['products'][] = array(
                            'product_code' => $val['product_code'],
                            'product_quantitiy' => $remaindnum,
                            /*
                            'product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['pay_price'],
                            */
                            'product_price' => $val['price'],
                            'pay_product_price' => $val['pay_price'],
                            'products_money' => $remaindnum*$val['price'],
                            'pay_products_money' => $remaindnum*$val['pay_price'],
                        );
                    }
                }
                //除了仓库的变量名，逻辑没区别（end）
                //单品配货结束，缺货数量 = 0
                if($type){
                    $order_product_group_lack[$val['order_product_group_id']] = 0;
                }else{
                    $order_product_lack[$val['order_product_id']] = 0;
                }
            }
        }
        return array(
            'array_part_stock' => $array_part_stock,
            'array_main_stock' => $array_main_stock,
            'array_wuxi_stock' => $array_wuxi_stock,
            'order_product_lack' => $order_product_lack,
            'order_product_group_lack' => $order_product_group_lack,
            'all_data' => $all_data
        );
    }

    public function getautoverorder(){
		$sql = "SELECT order_id,total FROM `".DB_PREFIX."order` WHERE order_status_id = 20 AND hang_status != 1";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getorderhangstatus($order_id){
		if(intval($order_id)){
			$sql = "SELECT hang_status FROM `".DB_PREFIX."order` WHERE order_id = '".$order_id."'";
			$query = $this->db->query($sql);
			return $query->row['hang_status'];
		}
	}

	public function updateorderhang($order_id){
		if(intval($order_id)){
			$sql = "UPDATE `".DB_PREFIX."order` SET hang_status = 1 WHERE order_id = '".$order_id."'";
			$query = $this->db->query($sql);
			return $query;
		}
	}

	public function cancelorderhang($order_id){
		if(intval($order_id)){
			$sql = "UPDATE `".DB_PREFIX."order` SET hang_status = 0 WHERE order_id = '".$order_id."'";
			$query = $this->db->query($sql);
			return $query;
		}
	}

	public function updateorderverify($order_id,$verify_status){
		if(intval($order_id)){
			$sql = "UPDATE `".DB_PREFIX."order` SET verify_status = '".intval($verify_status)."' WHERE order_id = '".$order_id."'";
			$query = $this->db->query($sql);
			return $query;
		}
	}


	public function reverse_order($out_id){
		$sql = "SELECT refer_id FROM `".DB_PREFIX."stock_out` WHERE out_id = '".$out_id."'";

		$refer_query = $this->db->query($sql);

		$refer_id = $refer_query->row['refer_id'];

		$sql = "SELECT order_status_id FROM `".DB_PREFIX."order` WHERE order_id = '".$refer_id."'";

		$status_query = $this->db->query($sql);

		if($status_query->row['order_status_id'] == 2 || $status_query->row['order_status_id'] == 17){
			$this->addOrderHistory($refer_id,20,'出库单无效');
			$this->updateorderverify($refer_id,0);
		}elseif($status_query->row['order_status_id'] == 20){
			$this->updateorderverify($refer_id,0);
		}
	}

    /*
     * 更新商品出库&配送比例
     * @author sonicsjh
     */
    public function resetRates($orderId, $ratePrefix='') {
        return true;
        if ('stock_out' != $ratePrefix && 'delivery' != $ratePrefix) {
            $ratePrefix = 'stock_out';
        }
        $data = array(
            $ratePrefix.'_rate_by_type'    => '0',
            $ratePrefix.'_rate_by_qty'     => '0',
            $ratePrefix.'_rate_by_price'   => '0',
        );
        $stockOutGatherInfo = $this->_getGatherInfoForStockOut($orderId, ($ratePrefix==='delivery'));
        if (false !== $gatherInfo) {
            $orderGatherInfo = $this->_getGatherInfoForOrder($orderId);
            $data[$ratePrefix.'_rate_by_type'] = min(100, round($stockOutGatherInfo['totalProductCode']*100/$orderGatherInfo['totalProductCode']));
            $data[$ratePrefix.'_rate_by_qty'] = min(100, round($stockOutGatherInfo['totalProductQty']*100/$orderGatherInfo['totalProductQty']));
            $data[$ratePrefix.'_rate_by_price'] = min(100, round($stockOutGatherInfo['totalProductPrice']*100/$orderGatherInfo['totalProductPrice']));
        }
        foreach ($data as $k=>$v) {
            $tmp[] = "`".$k."`='".$v."'";
        }
        $sql = "UPDATE `".DB_PREFIX."order` SET ".implode(',', $tmp)." WHERE `order_id`='".$orderId."'";
        $this->db->query($sql);
    }

    /*
     * 根据订单编号获取出库单品类数，商品总数，价格总数
     * @author sonicsjh
     */
    protected function _getGatherInfoForStockOut($orderId, $isFinish=true) {
        if ($isFinish) {
            $sql = "SELECT `product_code`,`product_quantity`,`products_money` FROM `".DB_PREFIX."stock_out_detail` WHERE `out_id` IN (SELECT `out_id` FROM `".DB_PREFIX."stock_out` WHERE `refer_id`='".$orderId."' AND `refer_type_id`='1' AND `status`=2)";
        }else{
            $sql = "SELECT `product_code`,`product_quantity`,`products_money` FROM `".DB_PREFIX."stock_out_detail` WHERE `out_id` IN (SELECT `out_id` FROM `".DB_PREFIX."stock_out` WHERE `refer_id`='".$orderId."' AND `refer_type_id`='1' AND `status`>0)";
        }
        $ret = array(
            'totalProductCode' => 0,
            'totalProductQty' => 0,
            'totalProductPrice' => 0,
        );
        $tmp = array();
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            if (false === array_key_exists($row['product_code'], $tmp)) {
                $tmp[$row['product_code']] = 1;
                $ret['totalProductCode'] += 1;
            }
            $ret['totalProductQty'] += $row['product_quantity'];
            $ret['totalProductPrice'] += $row['products_money'];
        }
        if (0 == array_sum($ret)) return false;
        return $ret;
    }

    /*
     * 根据订单编号获取订单品类数，商品总数，价格总数
     * @author sonicsjh
     */
    protected function _getGatherInfoForOrder($orderId) {
        $sql = "SELECT `product_code`,`quantity`,`pay_total` FROM `".DB_PREFIX."order_product` WHERE `order_id`='".$orderId."'";
        $ret = array(
            'totalProductCode' => 0,
            'totalProductQty' => 0,
            'totalProductPrice' => 0,
        );
        $tmp = array();
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            if (false === array_key_exists($row['product_code'], $tmp)) {
                $tmp[$row['product_code']] = 1;
                $ret['totalProductCode'] += 1;
            }
            $ret['totalProductQty'] += $row['quantity'];
            $ret['totalProductPrice'] += $row['pay_total'];
        }
        if (0 == array_sum($ret)) return false;
        return $ret;
    }
	
	//改变订单方式
	public function changeOrderMethod($data){
		
		$where['order_id'] = $data['order_id'];
		$save['payment_method'] = $data['payment_method'];
		M('order')->where($where)->save($save);

	}

    //修改出货百分比
    public function updateorderpercant($orderId,$total_percant){
        $sql = "UPDATE `order` SET total_percant = '".$total_percant."' WHERE order_id = '".$orderId."'";
        $this->db->query($sql);
    }

    //获取订单状态
    public function getOrderInfoByReturnFund($order_id){
    	$sql = "SELECT order_status_id,payment_code,total,telephone,customer_id,payment_company,isreturn FROM `".DB_PREFIX."order` WHERE order_id = '".$order_id."'";

    	$query = $this->db->query($sql);

    	return $query->row;
    }

    //加入应付
    public function addMoneyOut($insert_data){

    	$sql = "SELECT username FROM `".DB_PREFIX."user` WHERE user_id = '".$insert_data['operator_id']."'";

    	$user_query = $this->db->query($sql);

    	$username = $user_query->row['username'];

    	$sql = "INSERT INTO `".DB_PREFIX."money_out` SET payables = '".$insert_data['payables']."',paid = '".$insert_data['paid']."',refer_id = '".$insert_data['refer_id']."',refer_type_id = '".$insert_data['refer_type_id']."',payee = '".$insert_data['payee']."',payee_id = '".$insert_data['payee_id']."',date_added = NOW(),status = '".$insert_data['status']."',comment = '".$insert_data['comment']."',pay_time = NOW(),trader_type = '".$insert_data['trader_type']."',operator = '".$username."',operator_id = '".$insert_data['operator_id']."',refer_total = '".$insert_data['payables']."'";

    	$this->db->query($sql);

    }


    //获取未发货货物的退款金额
    public function getReturnMoney($order_id){
        //$sql = "SELECT SUM(op.lack_quantity * op.pay_price) AS sum_lack_product,SUM(opg.sum_lack_money) AS sum_lack_product_group FROM `order_product` op LEFT JOIN (SELECT (lack_quantity * pay_price) AS sum_lack_money,order_product_id FROM order_product_group) opg ON opg.order_product_id = op.order_product_id WHERE order_id = '".$order_id."'";
        //取销售价作为退款依据
        $sql = "
        SELECT
            SUM(op.lack_quantity * op.price) AS sum_lack_product,SUM(opg.sum_lack_money) AS sum_lack_product_group
        FROM
            `order_product` op
            LEFT JOIN (SELECT (lack_quantity * price) AS sum_lack_money,order_product_id FROM order_product_group) opg ON opg.order_product_id = op.order_product_id
        WHERE
            order_id = '".$order_id."'";
        $lack_query = $this->db->query($sql);
        return round(($lack_query->row['sum_lack_product']+$lack_query->row['sum_lack_product_group']),2);
    }

    //改变订单退款状态
    public function changeordereturn($order_id){

    	$sql = "UPDATE `".DB_PREFIX."order` SET isreturn = 1 WHERE order_id = '".$order_id."'";

    	$this->db->query($sql);

    }

    /*
     * 获取订单缺货信息
     * @author sonicsjh
     */
    public function getLackInfo($orderId) {
        $ret = array();
        $sql = "
        SELECT
            OP.`name` AS PN,OP.`lack_quantity` AS LQ,
            OPG.`product_code` AS GPC,OPG.`lack_quantity` AS GLQ,
            PD.`name` AS GPN
        FROM
            `order_product` AS OP
            LEFT JOIN `order_product_group` AS OPG ON (OPG.`order_product_id`=OP.`order_product_id`)
            LEFT JOIN (SELECT `product_id`,`name` FROM `product_description` WHERE `language_id`='".(int)$this->config->get('config_language_id')."') AS PD ON (PD.`product_id`=OPG.`product_id`)
        WHERE
            OP.`order_id`='".$orderId."' AND (OP.`lack_quantity`>0 OR OPG.`lack_quantity`>0)
        ORDER BY 
            OP.`order_product_id` ASC";
        $result = $this->db->query($sql);
        foreach ($result->rows as $row) {
            $ifGroup = (int)$row['GPC'];
            if ($ifGroup) {
                $ret[] = $row['PN'].': '.$row['GPN'].'缺 '.$row['GLQ'].' 个';
            }else{
                $ret[] = $row['PN'].'缺 '.$row['LQ'].' 个';
            }
        }
        return $ret;
    }

    /*
     * 获取订单支付信息
     * @author sonicsjh
     */
    public function getPaymentInfo($orderId) {
        $sql = "SELECT `yijipay_tradeNo` FROM `order` WHERE `order_id`='".$orderId."'";
        $result = $this->db->query($sql);
        return $result->row;
    }

    /*
     * 添加订单在线退款记录
     * @author sonicsjh
     */
    public function addOrderOnlineRefund($data) {
        $sql = "INSERT INTO `order_online_refund` SET ";
        foreach ($data as $col=>$val) {
            $sql .= "`".$col."`='".$val."',";
        }
        $this->db->query(substr($sql, 0, -1));
        return $this->db->getLastId();
    }

    /*
     * 更新订单在线退款记录
     * @author sonicsjh
     */
    public function updateOrderOnlineRefund($PK, $data) {
        $sql = "UPDATE `order_online_refund` SET ";
        foreach ($data as $col=>$val) {
            $sql .= "`".$col."`='".$val."',";
        }
        $sql = substr($sql, 0, -1)." WHERE `order_online_refund_id`='".$PK."'";
        $this->db->query($sql);
    }

    /*
     * 更新订单出库满足率（仅生成出库单时触发更新）
     * @author sonicsjh
     */
    /*
    public function resetSORate($orderId) {
        $sql = "SELECT SUM(`sale_money`) AS soTotal FROM `stock_out` WHERE `refer_id`='".$orderId."' AND `refer_type_id`='1' AND `status`>'0'";
        $result = $this->db->query($sql);
        $result->row['soTotal'];
        $sql = "UPDATE `order` SET `first_stock_out_rate`=LEAST(100, ROUND(".$result->row['soTotal']."*100/`total`)) WHERE `order_id`='".$orderId."' AND `first_stock_out_rate`='0'";
        $this->db->query($sql);
        $sql = "UPDATE `order` SET `final_stock_out_rate`=LEAST(100, ROUND(".$result->row['soTotal']."*100/`total`)) WHERE `order_id`='".$orderId."'";
        $this->db->query($sql);
    }
    */
    public function resetSORate_v2($orderId, $soRate) {
        $sql = "UPDATE `order` SET `first_stock_out_rate`='".$soRate."' WHERE `order_id`='".$orderId."' AND `first_stock_out_rate`='0'";
        $this->db->query($sql);
        $sql = "UPDATE `order` SET `final_stock_out_rate`='".$soRate."' WHERE `order_id`='".$orderId."'";
        $this->db->query($sql);
    }

    /*
     * 根据订单编号，获取订货人姓名、电话，业务员电话，用于退款发送短信通知
     * @author sonicsjh
     */
    public function getCAndUInfo($orderId) {
        $sql = "
        SELECT
            O.`shipping_fullname` AS CName,O.`shipping_telephone` AS CTel,U.`contact_tel` AS UTel
        FROM
            `order` AS O
            LEFT JOIN `user` AS U ON (U.`user_id`=O.`recommend_usr_id`)
        WHERE
            `order_id`='".$orderId."'
        ";
        $res = $this->db->query($sql);
        return $res->row;
    }

    /*
     * 获取订单级优惠信息
     */
    public function getOTList($orderId) {
        $sql = "
        SELECT
            `order_total_id`,`code`,`value`
        FROM
            `order_total` AS OT
        WHERE
            OT.`order_id`='".$orderId."'
        ORDER BY sort_order ASC
        ";
        $res = $this->db->query($sql);
        return $res->rows;
    }

    /*
     * 保存待退款金额到订单主表
     * 触发条件：结束配货时，需要退款的金额超过 弓飞 的退款限额（订单总额10%）
     *          客户不是通过易极付接口进行的线上支付
     *          易极付网站异常导致的退款申请失败
     * 已发起申请，易极付退款失败的金额不记录在这里，需要 it 人工介入操作
     */
    public function saveReturnMoneyIntoOrder($orderId, $returnMoney) {
        $sql = "UPDATE `order` SET `return_amount`='".$returnMoney."' WHERE `order_id`='".$orderId."'";
        $this->db->query($sql);
    }

    /*
     * 获取订单下的所有商品清单，用于配货，单品的opgPK为零
     * 考虑到货架完整性，当商品组和单品同时存在时，优先配商品组内单品
     * 返回格式：
     * array(
     *     product_Code => array(
     *         opgPK => array(
     *             '售价' => price,
     *             '摊销价' => pay_price, 
     *             '数量' => qty,
     *             '排序' => seq,//op表排序*1000+opg表排序
     *         ),
     *     ),
     * )
     */
    public function getOPList($orderId) {
        $seqRate = 10000;
        $ret = array();
        $sql = "SELECT OPG.`product_code`,OPG.`price`,OPG.`pay_price`,OPG.`quantity`,OPG.`order_product_group_id`,OPG.`order_ids`,OP.`order_ids` AS `parent_seq` FROM `order_product_group` AS OPG LEFT JOIN `order_product` AS OP ON (OP.`order_product_id`=OPG.`order_product_id`) WHERE OP.`order_id`='".$orderId."'";
        $res = $this->db->query($sql);
        foreach ($res->rows as $row) {
            $ret[$row['product_code']][$row['order_product_group_id']] = array(
                'price' => $row['price'],
                'pay_price' => $row['pay_price'],
                'qty' => $row['quantity'],
                'seq' => $row['parent_seq']*$seqRate+$row['order_ids'],
            );
        }
        $sql = "SELECT `product_code`,`price`,`pay_price`,`quantity`,`order_ids` FROM `order_product` AS OP WHERE OP.`order_id`='".$orderId."' AND NOT EXISTS (SELECT `order_product_group_id` FROM `order_product_group` AS OPG WHERE OPG.`order_product_id`=OP.`order_product_id`)";
        $res = $this->db->query($sql);
        foreach ($res->rows as $row) {
            $ret[$row['product_code']][0] = array(
                'price' => $row['price'],
                'pay_price' => $row['pay_price'],
                'qty' => $row['quantity'],
                'seq' => $row['order_ids']*$seqRate,
            );
        }
        return $ret;
    }

    /*
     * 根据订单编号和出库单汇总商品信息，更新未发货数量
     * moshan 在审核的时候默认出货数量即是签收数量
     */
    public function updateLackQuantity($orderId, $sopList) {
        foreach ($sopList as $pCode=>$v) {
            foreach ($v as $opgPK=>$info) {
                if ($opgPK) {
                    $sql = "UPDATE `order_product_group` SET `lack_quantity`=GREATEST(0, (`quantity`-".$info['qty'].")) WHERE `order_product_group_id`='".$opgPK."'";
                	$this->db->query($sql);

                    $sql = "UPDATE `order_product_group` SET `sign_quantity`=GREATEST(0, (".$info['qty'].")) WHERE `order_product_group_id`='".$opgPK."'";
                	$this->db->query($sql);

                }else{
                     $sql = "UPDATE `order_product` SET `lack_quantity`=GREATEST(0, (`quantity`-".$info['qty'].")) WHERE `order_id`='".$orderId."' AND `product_code`='".$pCode."'";
                      $this->db->query($sql);
                    $sql = "UPDATE `order_product` SET `sign_quantity`=GREATEST(0, (".$info['qty'].")) WHERE `order_id`='".$orderId."' AND `product_code`='".$pCode."'";
                     $this->db->query($sql);
                }
               
            }
        }
    }

    /*
     * 根据大类和小类重新排序后返回，便于仓库配货和司机验货
     */
    public function reSortQtyToSO($qtyToSO) {
        $ret = array();
        $productToClass = array();
        foreach ($qtyToSO as $productCode=>$info) {
            $sql = "SELECT `product_class1`,`product_class3` FROM `product` WHERE `product_code`='".substr($productCode, 0, 10)."'";
            $res = $this->db->query($sql);
            $productToClass[$res->row['product_class1']][$res->row['product_class3']][$productCode] = 1;
        }
        foreach ($productToClass as $class1=>$info) {
            ksort($info);
        }
        ksort($productToClass);
        foreach ($productToClass as $class1=>$value) {
            foreach ($value as $class3=>$value1) {
                foreach ($value1 as $productCode=>$value2) {
                   $ret[$productCode] = $qtyToSO[$productCode];
                }
            }
        }
        return $ret;
    }

    public function checkArea($id){

		$where['customer_id'] = $this->customer->getId();

		if($where['customer_id'] == 5152 || $where['customer_id'] == 3867){ //vip小胖 vip蒋总
			$show = 1;
			return $show;
		}

		$logcenter = M('customer')
			->alias('c')
			->join('logcenters l on c.logcenter_id = l.logcenter_id','left')
			->where($where)->select();
		$country_id = $logcenter[0]['country_id'];
		$zone_id = $logcenter[0]['zone_id'];

		$pta =M('product_to_area')->where('product_id='.$id)->select();
		$oc = explode(',',$pta[0]['only_countries']);
		$oz = explode(',',$pta[0]['only_zones']);
		$nc = explode(',',$pta[0]['no_countries']);
		$nz = explode(',',$pta[0]['no_zones']);

		$show = 1;
		if($pta[0]['only_countries'] != null || $pta[0]['only_zones'] != null || $pta[0]['only_zones'] != null || $pta[0]['no_zones'] != null){
			
			$show = 0;
			if(in_array($zone_id,$oz)){
				$show = 1;
			}
			else if(in_array($country_id,$oc) && !in_array($zone_id,$nz)){
				$show = 1;
			}
			else if(in_array($zone_id,$nz)){
				$show = 0;
			}
			else if(in_array($country_id,$nc) && !in_array($zone_id,$oz)){
				$show = 0;
			}

		}

		return $show;

	}

}
