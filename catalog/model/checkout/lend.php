<?php
class ModelCheckoutLend extends Model {

	public function getlend($loan_order_id){

		$sql = "SELECT * FROM `".DB_PREFIX."loan_order` WHERE loan_order_id = '".$loan_order_id."'";

		$query = $this->db->query($sql);

		return $query->row;

	}

	public function closelend($loan_order_id,$user_id){

		$sql = "SELECT status FROM `".DB_PREFIX."loan_order` WHERE loan_order_id = '".$loan_order_id."'";

		$status_query = $this->db->query($sql);

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET status = 7 WHERE loan_order_id = '".$loan_order_id."'";

		$result = $this->db->query($sql);

		if($result){

			$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = '".$status_query->row['status']."',status_after = 7,comment = '关闭借货单',date_added = NOW()";

			$this->db->query($sql);

		}

	}

	public function getlendinfo($loan_order_id){

		$sql = "SELECT lo.loan_order_id,lo.approvel_ids,lo.approveled_ids,lo.approvel_comment,lo.shipping_name,lo.shipping_tel,lo.shipping_address,lo.comment,lo.date_added,lo.date_modified,lo.from_warehouse_id,lo.status AS status_id,ls.status_name AS status,w.name AS fwarehouse,l.logcenter_name AS logcenter,u.username,lo.user_id AS louserid,ll.user_id AS lluserid FROM `".DB_PREFIX."loan_order` lo LEFT JOIN `".DB_PREFIX."loan_status` ls ON ls.id = lo.status LEFT JOIN `".DB_PREFIX."warehouse` w ON w.warehouse_id = lo.from_warehouse_id LEFT JOIN `".DB_PREFIX."logcenters` l ON l.logcenter_id = lo.logcenter_id LEFT JOIN `".DB_PREFIX."user` u ON u.user_id = lo.user_id LEFT JOIN `".DB_PREFIX."logcenters` ll ON ll.logcenter_id = lo.from_warehouse_id WHERE loan_order_id = '".$loan_order_id."'";

		$query = $this->db->query($sql);

		$result = $query->row;

		foreach($result as $key=>$val){

			if($key == 'approvel_ids'){

				$approvel_ids_arr = explode(',',trim($val,','));

				$approvel = array();

				foreach($approvel_ids_arr as $val2){

					$sql = "SELECT username FROM `".DB_PREFIX."user` WHERE user_id = '".$val2."'";

					$username_query = $this->db->query($sql);

					$approvel[] = $username_query->row['username'];

				}

				$approvel = implode(',',$approvel);

				$result['approvel'] = $approvel;

			}

			if($key == 'approveled_ids'){

				$approveled_ids_arr = explode(',',trim($val,','));

				$approveled = array();

				foreach($approveled_ids_arr as $val2){

					$sql = "SELECT username FROM `".DB_PREFIX."user` WHERE user_id = '".$val2."'";

					$username_query = $this->db->query($sql);

					$approveled[] = $username_query->row['username'];

				}

				$approveled = implode(',',$approveled);

				$result['approveled'] = $approveled;

			}

		}

		return $result;

	}

	public function verify($user_id,$status_id,$approveled_ids,$comment,$loan_order_id){

		if((int)$status_id){

			$sql = "UPDATE `".DB_PREFIX."loan_order` SET approveled_ids = '".$approveled_ids."' WHERE loan_order_id = '".$loan_order_id."'";

			$this->db->query($sql);

		}else{

			$sql = "UPDATE `".DB_PREFIX."loan_order` SET approveled_ids = '".$approveled_ids."',status = 1 WHERE loan_order_id = '".$loan_order_id."'";

			$this->db->query($sql);

			$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = 0,status_after = 1,comment = '".$comment."',date_added = NOW()";

			$this->db->query($sql);

		}

	}

	public function rverify($user_id,$status_id,$comment,$loan_order_id){

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET approvel_comment = '".$comment."',status = 2 WHERE loan_order_id = '".$loan_order_id."'";

		$this->db->query($sql);

		$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = '".$status_id."',status_after = 2,comment = '".$comment."',date_added = NOW()";

		$this->db->query($sql);

	}

	public function finishverify($user_id,$status_id,$approveled_ids,$lend_comment,$loan_order_id){

		$sql = "SELECT lo.from_warehouse_id,u.contact_tel FROM `".DB_PREFIX."loan_order` lo LEFT JOIN `".DB_PREFIX."user` u ON lo.user_id = u.user_id WHERE loan_order_id = '".$loan_order_id."'";

		$fquery = $this->db->query($sql);

		$sql = "SELECT * FROM `".DB_PREFIX."loan_order_details` WHERE loan_order_id = '".$loan_order_id."'";

		$detail_query = $this->db->query($sql);

		$stock_out_from_lend = array();

		$comment = array();

		foreach($detail_query->rows as $key=>$val){

			$sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$fquery->row['from_warehouse_id']."' AND product_code = '".$val['product_code']."'";

			$availquery = $this->db->query($sql);

			$stock_out_from_lend[$key] = array(
				'apply_qty' => $val['apply_qty'],
				'product_code' => $val['product_code'],
				'product_price' => $val['product_price'],
				'loan_order_details_id' => $val['loan_order_details_id']
			);

			if($val['apply_qty'] > $availquery->row['available_quantity']){

				$stock_out_from_lend[$key]['out_num'] = $availquery->row['available_quantity'];

				$comment[] = '商品编码为'.$val['product_code'].'缺货'.($val['apply_qty'] - $availquery->row['available_quantity']).'个';

			}else{

				$stock_out_from_lend[$key]['out_num'] = $val['apply_qty'];

			}

		}

		if(count($comment) > 0){

			$comment[] = '发起人电话为'.$fquery->row['contact_tel'];

			$comment = implode('<br>',$comment);

		}else{

			$comment = '';

		}

		$sum_price = 0;

		foreach($stock_out_from_lend as $val){

			$sum_price += $val['product_price'] * $val['out_num'];

		}

		$sql = "INSERT INTO `".DB_PREFIX."stock_out` SET refer_id = '".$loan_order_id."',refer_type_id = 6,warehouse_id = '".$fquery->row['from_warehouse_id']."',user_id = '".$user_id."',sale_money = '".$sum_price."',status = 1,comment = '".$comment."',date_added = NOW()";

		$this->db->query($sql);

		$out_id = $this->db->getLastId();

		foreach($stock_out_from_lend as $val){

			$sql = "INSERT INTO `".DB_PREFIX."stock_out_detail` SET out_id = '".$out_id."',product_code = '".$val['product_code']."',product_quantity = '".$val['out_num']."',product_price = '".$val['product_price']."',products_money = '".($val['product_price']*$val['out_num'])."',date_added = NOW()";

			$this->db->query($sql);

			$sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity - '".$val['out_num']."' WHERE warehouse_id = '".$fquery->row['from_warehouse_id']."' AND product_code = '".$val['product_code']."'";

			$this->db->query($sql);

			$sql = "UPDATE `".DB_PREFIX."loan_order_details` SET stock_out_qty = '".$val['out_num']."' WHERE loan_order_details_id = '".$val['loan_order_details_id']."'";

			$this->db->query($sql);

			$sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$fquery->row['from_warehouse_id']."' AND product_code = '".$val['product_code']."'";

			$idquery = $this->db->query($sql);

			$sql = "INSERT INTO `".DB_PREFIX."inventory_history` SET inventory_id = '".$idquery->row['id']."',product_code = '".$val['product_code']."',warehouse_id = '".$fquery->row['from_warehouse_id']."',available_qty = '".(0 - $val['out_num'])."',comment = '出库单 : ".$out_id." 出库单生成。',user_id = '".$user_id."',date_added = NOW()";

			$this->db->query($sql);

		}

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET approveled_ids = '".$approveled_ids."',status = 3 WHERE loan_order_id = '".$loan_order_id."'";

		$this->db->query($sql);

		$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = '".$status_id."',status_after = 3,comment = '".$lend_comment."',date_added = NOW()";

		$this->db->query($sql);

	}

	public function receive($product_code,$receive_qty,$user_id,$loan_order_id){

		foreach($product_code as $key=>$val){
			$sql = "UPDATE `".DB_PREFIX."loan_order_details` SET receive_qty = '".$receive_qty[$key]."' WHERE loan_order_id = '".$loan_order_id."' AND product_code = '".$val."'";

			$this->db->query($sql);
		}

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET status = 5,date_modified = NOW() WHERE loan_order_id = '".$loan_order_id."'";

		$this->db->query($sql);

		$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = 4,status_after = 5,comment = '签收借货单',date_added = NOW()";

		$this->db->query($sql);

	}

	public function send($refer_id,$out_id,$user_id){

		$sql = "SELECT product_code,product_quantity FROM `".DB_PREFIX."stock_out_detail` WHERE out_id = '".$out_id."'";

		$query = $this->db->query($sql);

		foreach($query->rows as $val){

			$sql = "UPDATE `".DB_PREFIX."loan_order_details` SET stock_out_qty = '".$val['product_quantity']."' WHERE loan_order_id = '".$refer_id."' AND product_code = '".$val['product_code']."'";

			$this->db->query($sql);

		}

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET status = 4 WHERE loan_order_id = '".$refer_id."'";

		$this->db->query($sql);

		$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$refer_id."',user_id = '".$user_id."',status_before = 3,status_after = 4,comment = '发货借货单',date_added = NOW()";

		$this->db->query($sql);

	}

	public function back($product_code,$return_qty_good,$return_qty_bad,$user_id,$loan_order_id,$from_warehouse_id){

        $products = $warehouse = array();//@author sonicsjh

		foreach($product_code as $key=>$val){

			$sql = "UPDATE `".DB_PREFIX."loan_order_details` SET return_qty_good = '".(int)$return_qty_good[$key]."',return_qty_bad = '".(int)$return_qty_bad[$key]."',unreturn_qty = stock_out_qty - '".((int)$return_qty_good[$key] + (int)$return_qty_bad[$key])."' WHERE loan_order_id = '".$loan_order_id."' AND product_code = '".$val."'";

			$this->db->query($sql);

            // @author sonicsjh
            if ($return_qty_good[$key]) {
                $products['good'][$val] = $return_qty_good[$key];
            }
            if ($return_qty_bad[$key]) {
                $products['bad'][$val] = $return_qty_bad[$key];
            }
            // @author sonicsjh
		}
        /*
         * 入库操作
         * @author sonicsjh
         */
        $sql = "SELECT `username`,`logcenter_permission` FROM `user` WHERE `user_id`='".$user_id."'";
        $res = $this->db->query($sql);
        $operateName = $res->row['username'];
        $warehouseId = $res->row['logcenter_permission'];
        if ($warehouseId) {
            $now = date('Y-m-d H:i:s');
            //$sql = "SELECT `warehouse_id`,`type` FROM `warehouse` WHERE `warehouse_id`='".$warehouseId."' OR `parent_warehouse`='".$warehouseId."' AND `status`='1'";
            $sql = "SELECT `warehouse_id`,`type` FROM `warehouse` WHERE `warehouse_id`='".$warehouseId."' OR `parent_warehouse`='".$warehouseId."'";//暂时不判断仓库可用性
            $res = $this->db->query($sql);
            foreach ($res->rows as $row) {
                if ('3' == $row['type']) {
                    $warehouse['good'] = $row['warehouse_id'];
                }else{
                    $warehouse['bad'] = $row['warehouse_id'];
                }
            }
            foreach ($warehouse as $k=>$warehouseId) {//入库
                if (count($products[$k])) {
                    $this->db->query("INSERT INTO `stock_in` SET `refer_id`='".$loan_order_id."',`refer_type_id`='6',`warehouse_id`='".$warehouseId."',`user_id`='".$user_id."',`status`='2',`comment`='借货单 ".$loan_order_id." 还货。',`date_added`='".$now."',`in_date`='".$now."'");//添加入库单主表记录
                    $SIId = $this->db->getLastId();//获取入库单编号
                    $total = 0;//入库单总价
                    foreach ($products[$k] as $pCode=>$qty) {
                        $res = $this->db->query("SELECT `product_cost` FROM `vendor` WHERE `vproduct_id`=(SELECT `product_id` FROM `product_option_value` WHERE product_code='".$pCode."')");//获取成本价
                        $pCost = $res->row['product_cost'];
                        $tmpTotal = $pCost*$qty;//单品总价
                        $this->db->query("INSERT INTO `stock_in_detail` SET `in_id`='".$SIId."',`product_code`='".$pCode."',`product_quantity`='".$qty."',`product_price`='".$pCost."',`products_money`='".$tmpTotal."',`date_added`='".$now."'");//添加入库单商品明细
                        $total += $tmpTotal;//入库单总价
                        //加后台库存
                        $this->db->query("SELECT `id` FROM `inventory` WHERE `warehouse_id`='".$warehouseId."' AND `product_code`='".$pCode."'");
                        $inventoryPK = (int)$res->row['id'];
                        if ($inventoryPK) {
                            $this->db->query("UPDATE `inventory` SET `account_quantity`=`account_quantity`+".$qty.",`available_quantity`=`available_quantity`+".$qty.",`date_modified`='".$now."' WHERE `inventory_id`='".$$inventoryPK."'");
                        }else{
                            $this->db->query("INSERT INTO `inventory` SET `warehouse_id`='".$warehouseId."',`product_code`='".$pCode."',`account_quantity`='".$qty."',`available_quantity`=".$qty.",`date_added`='".$now."',`date_modified`='".$now."'");
                            $inventoryPK = $this->db->getLastId();
                        }
                        //加后台库存
                        $this->db->query("INSERT INTO `inventory_history` SET `inventory_id`='".$inventoryPK."',`product_code`='".$pCode."',`warehouse_id`='".$warehouseId."',`account_qty`='".$qty."',`available_qty`='".$qty."',`comment`='借货单：".$loan_order_id." 还货。',`user_id`='".$user_id."',`date_added`='".$now."'");//添加后台库存日志
                    }
                    $this->db->query("UPDATE `stock_in` SET `buy_money`='".$total."' WHERE `in_id`='".$SIId."'");//更新入库单总价
                    $this->db->query("INSERT INTO `stock_in_history` SET `in_id`='".$SIId."',`user_id`='".$user_id."',`operator_name`='".$operateName."',`comment`='借货单还货，直接完成。',`date_added`='".$now."'");//添加入库单日志
                }
            }
        }
        // @author sonicsjh

		$sql = "UPDATE `".DB_PREFIX."loan_order` SET status = 6,date_modified = NOW() WHERE loan_order_id = '".$loan_order_id."'";

		$this->db->query($sql);

		$sql = "INSERT INTO `".DB_PREFIX."loan_order_history` SET loan_order_id = '".$loan_order_id."',user_id = '".$user_id."',status_before = 5,status_after = 6,comment = '归还借货单',date_added = NOW()";

		$this->db->query($sql);

	}

}
