<?php
class ModelCheckoutStockOut extends Model {

	public function add($data){
		$sql = "INSERT INTO `".DB_PREFIX."stock_out`";

		if(!empty($data['refer_id'])){
			$set[] = "refer_id = '".$data['refer_id']."'";
		}

		if(!empty($data['refer_type_id'])){
			$set[] = "refer_type_id = '".$data['refer_type_id']."'";
		}

		if(!empty($data['warehouse_id'])){
			$set[] = "warehouse_id = '".$data['warehouse_id']."'";
		}

		if(!empty($data['user_id'])){
			$set[] = "user_id = '".$data['user_id']."'";
		}

		if(!empty($data['sale_money'])){
			$set[] = "sale_money = '".$data['sale_money']."'";
		}

        if(!empty($data['pay_sale_money'])){//出库单摊销总价
			$set[] = "pay_sale_money = '".$data['pay_sale_money']."'";
		}

		if(!empty($data['status'])){
			$set[] = "status = '".$data['status']."'";
		}

		if(!empty($data['out_date'])){
			$set[] = "out_date = '".$data['out_date']."'";
		}

		if(!empty($data['date_added'])){
			$set[] = "date_added = '".$data['date_added']."'";
		}else{
			$set[] = "date_added = NOW()";
		}

		if(!empty($data['comment'])){
			$set[] = "comment = '".$data['comment']."'";
		}

		$sql .= ' SET '.implode(',',$set);

		$this->db->query($sql);

		$out_id = $this->db->getLastId();

		return $out_id;
	}

	public function addetail($data){
		$sql = "INSERT INTO `".DB_PREFIX."stock_out_detail`";

		if(!empty($data['out_id'])){
			$set[] = "out_id = '".$data['out_id']."'";
		}

		if(!empty($data['product_code'])){
			$set[] = "product_code = '".$data['product_code']."'";
		}

		if(!empty($data['product_quantity'])){
			$set[] = "product_quantity = '".$data['product_quantity']."'";
		}

		if(!empty($data['product_price'])){
			$set[] = "product_price = '".$data['product_price']."'";
		}

        if(!empty($data['pay_product_price'])){//摊销价
			$set[] = "pay_product_price = '".$data['pay_product_price']."'";
		}

		if(!empty($data['products_money'])){
			$set[] = "products_money = '".$data['products_money']."'";
		}

		if(!empty($data['pay_products_money'])){//摊销总价
			$set[] = "pay_products_money = '".$data['pay_products_money']."'";
		}

		if(!empty($data['counter_id'])){
			$set[] = "counter_id = '".$data['counter_id']."'";
		}

		if(!empty($data['date_added'])){
			$set[] = "date_added = '".$data['date_added']."'";
		}else{
			$set[] = "date_added = NOW()";
		}

        if(!empty($data['order_ids'])){
			$set[] = "order_ids = '".$data['order_ids']."'";
		}

		$sql .= ' SET '.implode(',',$set);

		$this->db->query($sql);

		$id = $this->db->getLastId();

		return $id;
	}

	public function getstockout($out_id){
		$sql = "SELECT so.*,rt.name AS rtname,wh.name AS whname,u.username,ts.name AS tsname FROM `".DB_PREFIX."stock_out` so LEFT JOIN `".DB_PREFIX."refer_type` rt ON so.refer_type_id = rt.refer_type_id LEFT JOIN `".DB_PREFIX."warehouse` wh ON so.warehouse_id = wh.warehouse_id LEFT JOIN `".DB_PREFIX."user` u ON so.user_id = u.user_id LEFT JOIN `".DB_PREFIX."inout_state` ts ON so.status = ts.id WHERE so.out_id = '".$out_id."'";

		$query = $this->db->query($sql);

		return $query->row['out_id'];
	}

	public function delete_stock($out_id,$user_id){
		if(!empty($out_id)){

			$sql = "SELECT warehouse_id,refer_type_id FROM `".DB_PREFIX."stock_out` WHERE out_id = '".$out_id."'";

			$warehouse_query = $this->db->query($sql);

			$warehouse_id = $warehouse_query->row['warehouse_id'];

			$sql = "SELECT sod.product_code,sod.product_quantity,sod.counter_id,so.refer_id FROM `".DB_PREFIX."stock_out_detail` sod LEFT JOIN `".DB_PREFIX."stock_out` so ON sod.out_id = so.out_id WHERE sod.out_id = '".$out_id."'";

			$products_query = $this->db->query($sql);

			foreach($products_query->rows as $val){
				if(intval($val['counter_id'])){

					$sql = "SELECT SUM(product_quantity) AS maxquan FROM `".DB_PREFIX."stock_out_detail` WHERE counter_id = '".intval($val['counter_id'])."' AND so.status != 0 AND out_id != '".$out_id."'";

				}else{

					$sql = "SELECT SUM(sod.product_quantity) AS maxquan FROM `".DB_PREFIX."stock_out_detail` sod LEFT JOIN `".DB_PREFIX."stock_out` so ON so.out_id = sod.out_id WHERE so.refer_id = '".$val['refer_id']."' AND so.status != 0 AND sod.out_id != '".$out_id."' AND sod.product_code = '".$val['product_code']."'";

				}

				$sumquery = $this->db->query($sql);

				$maxquan = intval($sumquery->row['maxquan']);

				if((int)$warehouse_query->row['refer_type_id'] == 1){

					if(intval($val['counter_id'])){

						$sql = "UPDATE `".DB_PREFIX."order_product_group` SET lack_quantity = IF((quantity - ".$maxquan.") < 0,0,(quantity - ".$maxquan.")) WHERE order_product_group_id = '".intval($val['counter_id'])."'";

					}else{

						$sql = "UPDATE `".DB_PREFIX."order_product` SET lack_quantity = IF((quantity - ".$maxquan.") < 0,0,(quantity - ".$maxquan.")) WHERE product_code = '".$val['product_code']."' AND order_id = '".$val['refer_id']."'";

					}

				}

				$this->db->query($sql);

				$sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity + ".intval($val['product_quantity'])." WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";

				$this->db->query($sql);

				//添加库存变动记录

				$sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";

				$id_query = $this->db->query($sql);

				if (0 != (int)$val['product_quantity']){
				    $inventoryHistoryData = array(
				        'inventory_id'  => $id_query->row['id'],//inventory表PK
				        'product_code'  => $val['product_code'],//商品编码（冗余字段）
				        'warehouse_id'  => $warehouse_id,//仓库编号（冗余字段）
				        'account_qty'   => 0,//财务数量变更（正+ 负-）
				        'available_qty' => (int)$val['product_quantity'],//可用数量变更（正+ 负-）
				        'comment'       => '出库单：'.$out_id.' 出库单作废。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
				        'user_id'       => $user_id,//操作人PK
				        'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
				    );
				    M('inventory_history')->data($inventoryHistoryData)->add();
				}
			}

			$sql = "UPDATE `".DB_PREFIX."stock_out` SET status = 0,user_id = '".$user_id."' WHERE out_id = '".$out_id."'";
			$result = $this->db->query($sql);
			return $result;
		}else
			return false;
	}

	public function recover_stock($out_id,$user_id){
		if(!empty($out_id)){

			$temp_stock = array();

			$sql = "SELECT warehouse_id FROM `".DB_PREFIX."stock_out` WHERE out_id = '".$out_id."'";

			$warehouse_query = $this->db->query($sql);

			$warehouse_id = $warehouse_query->row['warehouse_id'];

			$sql = "SELECT product_code,product_quantity FROM `".DB_PREFIX."stock_out_detail` WHERE out_id = '".$out_id."'";

			$products_query = $this->db->query($sql);

			foreach($products_query->rows as $val){

				if(isset($temp_stock[$val['product_code']])){

					if($temp_stock[$val['product_code']] >= $val['product_quantity']){

						$temp_stock[$val['product_code']] = $temp_stock[$val['product_code']] - $val['product_quantity'];

					}else{

						return array(
							'status' => false,
							'msg' => '库存不足'
						);

					}

				}else{

					$sql = "SELECT available_quantity FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";

					$available_query = $this->db->query($sql);

					$available_quantity = $available_query->row['available_quantity'];

					if($available_quantity >= $val['product_quantity']){

						$temp_stock[$val['product_code']] = $available_quantity - $val['product_quantity'];

					}else{

						return array(
							'status' => false,
							'msg' => '库存不足'
						);

					}

				}

			}

			foreach($products_query->rows as $val){

				$sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity - ".intval($val['product_quantity'])." WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";

				$this->db->query($sql);

				//添加库存变动记录

				$sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";

				$id_query = $this->db->query($sql);

				if (0 != (int)$val['product_quantity']){
				    $inventoryHistoryData = array(
				        'inventory_id'  => $id_query->row['id'],//inventory表PK
				        'product_code'  => $val['product_code'],//商品编码（冗余字段）
				        'warehouse_id'  => $warehouse_id,//仓库编号（冗余字段）
				        'account_qty'   => 0,//财务数量变更（正+ 负-）
				        'available_qty' => (int)(0 - $val['product_quantity']),//可用数量变更（正+ 负-）
				        'comment'       => '出库单：'.$out_id.' 出库单恢复。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
				        'user_id'       => $user_id,//操作人PK
				        'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
				    );
				    M('inventory_history')->data($inventoryHistoryData)->add();
				}

			}

			$sql = "UPDATE `".DB_PREFIX."stock_out` SET status = 1,user_id = '".$user_id."' WHERE out_id = '".$out_id."'";
			$result = $this->db->query($sql);
			return array(
				'status' => $result
			);


		}else{
			return array(
				'status' => false
			);
		}
	}

	public function getstockoutbyoutid($out_id){
		$sql = "SELECT so.*,rt.name AS rtname,wh.name AS whname,u.username,ts.name AS tsname FROM `".DB_PREFIX."stock_out` so LEFT JOIN `".DB_PREFIX."refer_type` rt ON so.refer_type_id = rt.refer_type_id LEFT JOIN `".DB_PREFIX."warehouse` wh ON so.warehouse_id = wh.warehouse_id LEFT JOIN `".DB_PREFIX."user` u ON so.user_id = u.user_id LEFT JOIN `".DB_PREFIX."inout_state` ts ON so.status = ts.id";

		if($out_id){
			$sql .= ' WHERE so.out_id = '.$out_id;

			$query = $this->db->query($sql);

			return $query->row;
		}else
			return null;
	}

    //用于老版配货
    public function stockoutfromorder($order_id,$data,$user_id,$anyway){
        if($data['status'] == 'true' || $anyway){
            $part_stock_total = 0;
            $main_stock_total = 0;
            $wuxi_stock_total = 0;
            $lack_product_num = 0;
            $sql = "SELECT op.order_product_id,p.product_type FROM `".DB_PREFIX."order_product` op LEFT JOIN `".DB_PREFIX."product` p ON p.product_id = op.product_id WHERE order_id = '".$order_id."'";
            $order_product_query = $this->db->query($sql);
            foreach($order_product_query->rows as $val){
                if($val['product_type'] == 2){
                    $sql = "SELECT order_product_group_id FROM `".DB_PREFIX."order_product_group` WHERE order_product_id = '".$val['order_product_id']."'";
                    $order_product_group_query = $this->db->query($sql);
                    foreach($order_product_group_query->rows as $val){
                        if($data['order_product_group_lack'][$val['order_product_group_id']] > 0){
                            $lack_product_num++;
                        }
                        $sql = "UPDATE `".DB_PREFIX."order_product_group` SET lack_quantity = '".$data['order_product_group_lack'][$val['order_product_group_id']]."' WHERE order_product_group_id = '".$val['order_product_group_id']."'";//保存商品组内单品缺货数量
                        $this->db->query($sql);
                    }
                }else{
                    if($data['order_product_lack'][$val['order_product_id']] > 0){
                        $lack_product_num++;
                    }
                    $sql = "UPDATE `".DB_PREFIX."order_product` SET lack_quantity = '".$data['order_product_lack'][$val['order_product_id']]."' WHERE order_product_id = '".$val['order_product_id']."'";//保存单品缺货数量
                    $this->db->query($sql);
                }
            }
            //出库单序号（批次号）
            $sql = "SELECT COUNT(out_id) AS count FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$order_id."' AND `refer_type_id`='1'";
            $count_query = $this->db->query($sql);
            $count = $count_query->row['count'];
            $new_index_start = $new_index_process = $count + 1; 
            $new_index_end = $count;
            if(!empty($data['array_part_stock']['products'])){
                $new_index_end++;
            }
            if(!empty($data['array_main_stock']['products'])){
                $new_index_end++;
            }
            if(!empty($data['array_wuxi_stock']['products'])){
                $new_index_end++;
            }
            //保存默认仓配货数据到出库单
            if(!empty($data['array_part_stock']['products'])){
                foreach($data['array_part_stock']['products'] as $val){//统计出库单商品价格小计（销售价，摊销价）
                    $part_stock_total += $val['products_money'];
                    $part_stock_pay_total += $val['pay_products_money'];
                }
                $comment = "本批次出库单".$new_index_process."(".$new_index_start."-".$new_index_end.")"." 未出库单品".$lack_product_num."种";
                $out_id = $this->add(array(
                    'refer_id' => $order_id,
                    'refer_type_id' => 1,
                    'warehouse_id' => $data['array_part_stock']['warehouse_id'],
                    'user_id' => $user_id,
                    'sale_money' => $part_stock_total,
                    'pay_sale_money' => $part_stock_pay_total,
                    'status' => 1,
                    'comment' => $comment
                ));
                $historydata = array(
                    'out_id' => $out_id,
                    'user_id' => $user_id,
                    'comment' => '添加出库单'
                );
                $this->addstockhistory($historydata);
                foreach($data['array_part_stock']['products'] as $val){
                    //扣减系统可用数量（系统后台库存）
                    $sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity - ".intval($val['product_quantitiy'])." WHERE product_code = '".$val['product_code']."' AND warehouse_id = '".$data['array_part_stock']['warehouse_id']."'";
                    $this->db->query($sql);
                    //添加库存变动记录
                    $sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$data['array_part_stock']['warehouse_id']."' AND product_code = '".$val['product_code']."'";
                    $id_query = $this->db->query($sql);
                    if (0 != (int)$val['product_quantitiy']){
                        $inventoryHistoryData = array(
                            'inventory_id'  => $id_query->row['id'],//inventory表PK
                            'product_code'  => $val['product_code'],//商品编码（冗余字段）
                            'warehouse_id'  => $data['array_part_stock']['warehouse_id'],//仓库编号（冗余字段）
                            'account_qty'   => 0,//财务数量变更（正+ 负-）
                            'available_qty' => (int)(0 - $val['product_quantitiy']),//可用数量变更（正+ 负-）
                            'comment'       => '出库单：'.$out_id.' 出库单生成。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
                            'user_id'       => $user_id,//操作人PK
                            'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
                        );
                        M('inventory_history')->data($inventoryHistoryData)->add();
                    }
                    $this->addetail(array(
                        'out_id' => $out_id,
                        'product_code' => $val['product_code'],
                        'product_quantity' => $val['product_quantitiy'],
                        'product_price' => $val['product_price'],
                        'pay_product_price' => $val['pay_product_price'],
                        'products_money' => $val['products_money'],
                        'pay_products_money' => $val['pay_products_money'],
                        'counter_id' => intval($val['counter_id']),
                    ));
                }
                $new_index_process++;
            }
            if(!empty($data['array_main_stock']['products'])){
                foreach($data['array_main_stock']['products'] as $val){
                    $main_stock_total += $val['products_money'];
                    $main_stock_pay_total += $val['pay_products_money'];
                }
                $comment = "本批次出库单".$new_index_process."(".$new_index_start."-".$new_index_end.")"." 未出库单品".$lack_product_num."种";
                $out_id = $this->add(array(
                    'refer_id' => $order_id,
                    'refer_type_id' => 1,
                    'warehouse_id' => $data['array_main_stock']['warehouse_id'],
                    'user_id' => $user_id,
                    'sale_money' => $main_stock_total,
                    'pay_sale_money' => $main_stock_pay_total,
                    'status' => 1,
                    'comment' => $comment,
                ));
                $historydata = array(
                    'out_id' => $out_id,
                    'user_id' => $user_id,
                    'comment' => '添加出库单'
                );
                $this->addstockhistory($historydata);
                foreach($data['array_main_stock']['products'] as $val){
                    //扣减系统可用数量（系统后台库存）
                    $sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity - ".intval($val['product_quantitiy'])." WHERE product_code = '".$val['product_code']."' AND warehouse_id = '".$data['array_main_stock']['warehouse_id']."'";
                    $this->db->query($sql);
                    //添加库存变动记录
                    $sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$data['array_main_stock']['warehouse_id']."' AND product_code = '".$val['product_code']."'";
                    $id_query = $this->db->query($sql);
                    if (0 != (int)$val['product_quantitiy']){
                        $inventoryHistoryData = array(
                            'inventory_id'  => $id_query->row['id'],//inventory表PK
                            'product_code'  => $val['product_code'],//商品编码（冗余字段）
                            'warehouse_id'  => $data['array_main_stock']['warehouse_id'],//仓库编号（冗余字段）
                            'account_qty'   => 0,//财务数量变更（正+ 负-）
                            'available_qty' => (int)(0 - $val['product_quantitiy']),//可用数量变更（正+ 负-）
                            'comment'       => '出库单：'.$out_id.' 出库单生成。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
                            'user_id'       => $user_id,//操作人PK
                            'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
                        );
                        M('inventory_history')->data($inventoryHistoryData)->add();
                    }
                    $this->addetail(array(
                        'out_id' => $out_id,
                        'product_code' => $val['product_code'],
                        'product_quantity' => $val['product_quantitiy'],
                        'product_price' => $val['product_price'],
                        'pay_product_price' => $val['pay_product_price'],
                        'products_money' => $val['products_money'],
                        'pay_products_money' => $val['pay_products_money'],
                        'counter_id' => intval($val['counter_id']),
                    ));
                }
                $new_index_process++;
            }
            if(!empty($data['array_wuxi_stock']['products'])){
                foreach($data['array_wuxi_stock']['products'] as $val){
                    $wuxi_stock_total += $val['products_money'];
                    $wuxi_stock_pay_total += $val['pay_products_money'];
                }
                $comment = "本批次出库单".$new_index_process."(".$new_index_start."-".$new_index_end.")"." 未出库单品".$lack_product_num."种";
                $out_id = $this->add(array(
                    'refer_id' => $order_id,
                    'refer_type_id' => 1,
                    'warehouse_id' => $data['array_wuxi_stock']['warehouse_id'],
                    'user_id' => $user_id,
                    'sale_money' => $wuxi_stock_total,
                    'pay_sale_money' => $wuxi_stock_pay_total,
                    'status' => 1,
                    'comment' => $comment
                ));
                $historydata = array(
                    'out_id' => $out_id,
                    'user_id' => $user_id,
                    'comment' => '添加出库单'
                );
                $this->addstockhistory($historydata);
                foreach($data['array_wuxi_stock']['products'] as $val){
                    //扣减系统可用数量（系统后台库存）
                    $sql = "UPDATE `".DB_PREFIX."inventory` SET available_quantity = available_quantity - ".intval($val['product_quantitiy'])." WHERE product_code = '".$val['product_code']."' AND warehouse_id = '".$data['array_wuxi_stock']['warehouse_id']."'";
                    $this->db->query($sql);
                    //添加库存变动记录
                    $sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$data['array_wuxi_stock']['warehouse_id']."' AND product_code = '".$val['product_code']."'";
                    $id_query = $this->db->query($sql);
                    if (0 != (int)$val['product_quantitiy']){
                        $inventoryHistoryData = array(
                            'inventory_id'  => $id_query->row['id'],//inventory表PK
                            'product_code'  => $val['product_code'],//商品编码（冗余字段）
                            'warehouse_id'  => $data['array_wuxi_stock']['warehouse_id'],//仓库编号（冗余字段）
                            'account_qty'   => 0,//财务数量变更（正+ 负-）
                            'available_qty' => (int)(0 - $val['product_quantitiy']),//可用数量变更（正+ 负-）
                            'comment'       => '出库单：'.$out_id.' 出库单生成。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
                            'user_id'       => $user_id,//操作人PK
                            'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
                        );
                        M('inventory_history')->data($inventoryHistoryData)->add();
                    }
                    $this->addetail(array(
                        'out_id' => $out_id,
                        'product_code' => $val['product_code'],
                        'product_quantity' => $val['product_quantitiy'],
                        'product_price' => $val['product_price'],
                        'pay_product_price' => $val['pay_product_price'],
                        'products_money' => $val['products_money'],
                        'pay_products_money' => $val['pay_products_money'],
                        'counter_id' => intval($val['counter_id'])
                    ));
                }
            }
            return true;
        }
    }

    public function complete_stock($out_id,$user_id){
        if (!empty($out_id)){
            $sql = "SELECT warehouse_id,refer_id,refer_type_id FROM `".DB_PREFIX."stock_out` WHERE out_id = '".$out_id."'";
            $warehouse_query = $this->db->query($sql);
            $warehouse_id = $warehouse_query->row['warehouse_id'];//出库单仓库编号
            $refer_id = $warehouse_query->row['refer_id'];//出库单关联单据号
            $sql = "SELECT product_code,product_quantity FROM `".DB_PREFIX."stock_out_detail` WHERE out_id = '".$out_id."'";
            $products_query = $this->db->query($sql);//出库单商品明细
            foreach($products_query->rows as $val){//循环变更库存，并保留日志
                $sql = "UPDATE `".DB_PREFIX."inventory` SET account_quantity = account_quantity - ".intval($val['product_quantity'])." WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";
                //$this->db->query($sql);
                //添加库存变动记录
                $sql = "SELECT id FROM `".DB_PREFIX."inventory` WHERE warehouse_id = '".$warehouse_id."' AND product_code = '".$val['product_code']."'";
                $id_query = $this->db->query($sql);
                if (0 != (int)$val['product_quantity']){
                    $inventoryHistoryData = array(
                        'inventory_id'  => $id_query->row['id'],//inventory表PK
                        'product_code'  => $val['product_code'],//商品编码（冗余字段）
                        'warehouse_id'  => $warehouse_id,//仓库编号（冗余字段）
                        'account_qty'   => (int)(0 - $val['product_quantity']),//财务数量变更（正+ 负-）
                        'available_qty' => 0,//可用数量变更（正+ 负-）
                        'comment'       => '出库单：'.$out_id.' 出库单完成。',//备注（入库单完成、损益单完成、出库单生成、出库单修改、出库单作废）
                        'user_id'       => $user_id,//操作人PK
                        'date_added'    => date('Y-m-d H:i:s'),//条目创建时间
                    );
                    //M('inventory_history')->data($inventoryHistoryData)->add();
                }
            }
            $sql = "UPDATE `".DB_PREFIX."stock_out` SET status = 2,user_id = '".$user_id."',out_date = NOW() WHERE out_id = '".$out_id."'";
            $this->db->query($sql);//更新出库单状态
            $ret = true;
            if ($warehouse_query->row['refer_type_id'] == 1) {//订单生成的出库单，对比出库单和订单商品数量后修改订单状态
                $sql = "SELECT order_status_id FROM `".DB_PREFIX."order` WHERE order_id = '".$refer_id."'";
                $order_query = $this->db->query($sql);
                $order_status_id = $order_query->row['order_status_id'];//获取订单状态
                if($order_status_id == 2 || $order_status_id == 17 || $order_status_id == 20){//订单状态为待审核或部分发货时，对比出库单和订单商品数量后修改订单状态
                    //获取订单商品汇总数量
                    $qtyInOrder = array();
                    $sql = "SELECT op.product_code,op.quantity,op.order_product_id,p.product_type FROM `".DB_PREFIX."order_product` op LEFT JOIN `".DB_PREFIX."product` p ON op.product_id = p.product_id WHERE order_id = '".$refer_id."'";
                    $res = $this->db->query($sql);
                    foreach ($res->rows as $row) {
                        if (1 == $row['product_type']){
                            $qtyInOrder[$row['product_code']] += $row['quantity'];
                        }else if (2 == $row['product_type']){//商品组
                            $sql = "SELECT product_code,quantity FROM `".DB_PREFIX."order_product_group` WHERE order_product_id = '".$row['order_product_id']."'";
                            $resTmp = $this->db->query($sql);
                            foreach ($resTemp->rows as $rowTmp) {
                                $qtyInOrder[$rowTmp['product_code']] += $rowTmp['quantity'];
                            }
                        }
                    }
                    //获取出库单商品汇总数量
                    $qtyInSO = array();
                    $sql = "SELECT `product_code`,`product_quantity` FROM `".DB_PREFIX."stock_out_detail` WHERE `out_id` IN (SELECT `out_id` FROM `".DB_PREFIX."stock_out` WHERE `refer_type_id`='1' AND `refer_id`='".$refer_id."' AND `status`>'1')";
                    $res = $this->db->query($sql);
                    foreach ($res->rows as $row) {
                        $qtyInSO[$row['product_code']] += $row['product_quantity'];
                    }

                    //对比数量
                    $ret = 'change';//全部发货
                    if (count($qtyInOrder) > count($qtyInSO)) {//对比品类
                        $ret = 'change2';//部分发货
                    }else{
                        foreach ($qtyInOrder as $pCode=>$qty) {//逐个品类对比数量
                            if ($qty > (int)$qtyInSO[$pCode]) {
                                $ret = 'change2';//部分发货
                                break;
                            }
                        }
                    }
                    if ('change2' == $ret && 17 == $order_status_id) {//本来就是部分发货的，不再修改部分发货状态
                        $ret = true;
                    }
                }
            }
            return array(
                'status' => $ret,
            );
        }else{
            return array(
                'status' => false,
            );
        }
    }

	public function deliver_stock($out_id,$user_id){

		if(!empty($out_id)){

			$sql = "UPDATE `".DB_PREFIX."stock_out` SET status = 3,user_id = '".$user_id."',deliver_date = NOW() WHERE out_id = '".$out_id."'";
			$result = $this->db->query($sql);

			$sql = "SELECT o.order_status_id,o.order_id FROM `".DB_PREFIX."stock_out` so LEFT JOIN `".DB_PREFIX."order` o ON o.order_id = so.refer_id WHERE out_id = '".$out_id."'";
			$status_query = $this->db->query($sql);

			if($status_query->row['order_status_id'] == 18){

				$deliver_mark = true;

				$sql = "SELECT status FROM `".DB_PREFIX."stock_out` WHERE status != 0 AND refer_id = '".$status_query->row['order_id']."'";

				$stock_status_query = $this->db->query($sql);

				foreach($stock_status_query->rows as $val){

					if($val['status'] != 3 && $val['status'] != 4){

						$deliver_mark = false;

					}

				}

				if($deliver_mark){

					return array(

						'status' => 'change',
						'order_id' => $status_query->row['order_id']

					);

				}else{

					return array(

						'status' => 'nochange'

					);

				}

			}

		}else{
			return array(
				'status' => false
			);
		}

	}

	public function final_complete_stock($out_id,$user_id){

		if(!empty($out_id)){

			$sql = "UPDATE `".DB_PREFIX."stock_out` SET status = 4,user_id = '".$user_id."',receive_date = NOW() WHERE out_id = '".$out_id."'";
			$result = $this->db->query($sql);

			$sql = "SELECT o.order_status_id,o.order_id FROM `".DB_PREFIX."stock_out` so LEFT JOIN `".DB_PREFIX."order` o ON o.order_id = so.refer_id WHERE out_id = '".$out_id."'";
			$status_query = $this->db->query($sql);

			if($status_query->row['order_status_id'] == 3){

				$complete_mark = true;

				$sql = "SELECT status FROM `".DB_PREFIX."stock_out` WHERE status != 0 AND refer_id = '".$status_query->row['order_id']."'";

				$stock_status_query = $this->db->query($sql);

				foreach($stock_status_query->rows as $val){

					if($val['status'] != 4){

						$complete_mark = false;

					}

				}

				if($complete_mark){

					return array(

						'status' => 'change',
						'order_id' => $status_query->row['order_id']

					);

				}else{

					return array(

						'status' => 'nochange'

					);

				}

			}

		}else{
			return array(
				'status' => false
			);
		}

	}

	public function addstockhistory($data){
		$sql = "INSERT INTO `".DB_PREFIX."stock_out_history`";

		if(!empty($data['out_id'])){
			$set[] = "out_id = '".$data['out_id']."'";
		}

		if(!empty($data['user_id'])){
			$set[] = "user_id = '".$data['user_id']."'";

			$usersql = "SELECT username FROM `".DB_PREFIX."user` WHERE user_id = '".$data['user_id']."'";

			$query = $this->db->query($usersql);

			$username = $query->row['username'];

			$set[] = "operator_name = '".$username."'";
		}

		if(!empty($data['notify'])){
			$set[] = "notify = '".$data['notify']."'";
		}else{
			$set[] = "notify = 0";
		}

		if(!empty($data['comment'])){
			$set[] = "comment = '".$data['comment']."'";
		}

		if(!empty($data['date_added'])){
			$set[] = "date_added = '".$data['date_added']."'";
		}else{
			$set[] = "date_added = NOW()";
		}

		$sql .= ' SET '.implode(',',$set);

		$this->db->query($sql);

		$id = $this->db->getLastId();

		return $id;
	}

	public function addautolog($data){
		$sql = "INSERT INTO `".DB_PREFIX."auto_log`";

		if(!empty($data['start_time'])){
			$set[] = "start_time = '".$data['start_time']."'";
		}else{
			$set[] = "start_time = NOW()";
		}

		if(!empty($data['end_time'])){
			$set[] = "end_time = '".$data['end_time']."'";
		}

		if(!empty($data['process_id'])){
			$set[] = "process_id = '".$data['process_id']."'";
		}

		if(!empty($set)){
			$sql .= " SET ".implode(',',$set);

			$this->db->query($sql);

			$id = $this->db->getLastId();

			return $id;
		}else
			return 0;
	}

	public function updateautolog($data,$id){
		$sql = "UPDATE `".DB_PREFIX."auto_log`";

		if(!empty($data['start_time'])){
			$set[] = "start_time = '".$data['start_time']."'";
		}

		if(!empty($data['end_time'])){
			$set[] = "end_time = '".$data['end_time']."'";
		}else{
			$set[] = "end_time = NOW()";
		}

		if(!empty($data['process_id'])){
			$set[] = "process_id = '".$data['process_id']."'";
		}

		if(!empty($set)){
			$sql .= " SET ".implode(',',$set);

			if(intval($id)){
				$sql .= " WHERE id = '".$id."'";

				$result = $this->db->query($sql);

				return $result;
			}else
				return false;
		}else
			return false;
	}

	public function getMoney($refer_id){
		$money_in = M('money_in')
			->field('money_in_id,refer_id,receivable')
			 ->where(array('refer_id'=>$refer_id))
			->find();
			return $money_in;
	}
	public function getMoneyMessage($out_id){
		$where = 'out_id='.$out_id.' AND refer_type_id =1';
		
			$moneyMessage  = M('stock_out')
			->join('left join `order` on `order`.order_id=stock_out.refer_id')
			->field('`order`.order_id, `order`.total,`order`.ori_total,`order`.is_pay, `order`.customer_id, `order`.payment_company,stock_out.out_id,stock_out.sale_money ,`order`.shipping_address')
			 ->where($where)
			->find();
			return $moneyMessage;
	}

	public function addMoney($res1,$result1){
		$receivable=M('stock_out')
			 ->where(array('status'=>2,'refer_id'=>$result1['refer_id'],'refer_type_id'=>1))
			->getField('sum(sale_money)');
		$model=M('money_in');

		$data = array(
			'payer_id'=>$res['customer_id'], 
			'payer_name'=>$res['payment_company'].'('.$res['shipping_address'].')', 
			'refer_type_id'=>1, 
			'refer_id'=>$res['order_id'], 
			'refer_total'=>$result1['ori_total'], 
			'received'=>$result1['total'], 
			'receivable'=>$receivable,  
			'status'=>2 ,
			'trader_type'=>2,
			'date_added'=>date('Y-m-d H:i:s')
	  		);
 		 $model->data($data)->add();
	}

	

	public function addMoneyn($res1,$result1){
		$receivable=M('stock_out')
			 ->where(array('status'=>2,'refer_id'=>$result1['refer_id'],'refer_type_id'=>1))
			->getField('sum(sale_money)');
		$model=M('money_in');

		$data = array(
				'payer_id'=>$res['customer_id'], 
				'payer_name'=>$res['payment_company'].'('.$res['shipping_address'].')', 
				'refer_type_id'=>1, 
				'refer_id'=>$res['order_id'], 
				'refer_total'=>$result1['ori_total'], 
				'receivable'=>$receivable,  
				'status'=>1,
				'trader_type'=>2,
				'date_added'=>date('Y-m-d H:i:s')
		  		);
	 		 $model->data($data)->add();
	}
	public function updatemoney($res,$result1){
		$receivable = (float)$result1['sale_money']+(float)$res['receivable'];
        $model=M('money_in');
		$data = array(
			'receivable'=>$receivable
			);
 		$model->data($data)->where(array('money_in_id'=>$res['money_in_id']))->save();
	}

	public function addMHistory($money_in_id, $user_id,$comments){
		$username = M('user')->where(array('user_id'=>$user_id))->getField('username');
		$data = array(
					'money_in_id'=>$money_in_id, 
		  			'user_id'=>$user_id,
		  			'operator_name'=>$username,
		  		 	'comment'=>$comments, 
					'date_added'=>date('Y-m-d H:i:s'));
		M('money_in_history')->data($data)->add();
	}

    /*
     * 根据出库单编号，获取订单编号
     */
    public function getOrderIdByStockOutId($outId) {
        $sql = "SELECT `refer_id` FROM `".DB_PREFIX."stock_out` WHERE `out_id`='".$outId."'";
        $result = $this->db->query($sql);
        return intval($result->row['refer_id']);
    }
/* this is a test */
    /*
     * 根据订单编号获取已出库货物总价
     */
    public function getSumStockOutPrice($orderId) {
        //返回出库单摊销价，计算可出库比例
        $sql = "SELECT SUM(`pay_sale_money`) as total FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$orderId."' AND `refer_type_id`=1 AND status>0";
        $res = $this->db->query($sql);
        return $res->row['total'];
        /*
        $sql = "SELECT pay_products_money FROM `".DB_PREFIX."stock_out_detail` WHERE out_id IN (SELECT out_id FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$orderId."' AND status != 0)";
        $sum_money = 0;
        $money_query = $this->db->query($sql);
        foreach($money_query->rows as $val){
            $sum_money += $val['products_money'];
        }
        return $sum_money;
        */
    }

    public function getAllActiveStockStatusByReferid($refer_id){

    	$sql = "SELECT status FROM `".DB_PREFIX."stock_out` WHERE refer_id = '".$refer_id."' AND status != 0";

    	$info_query = $this->db->query($sql);

    	return $info_query->rows;

    }

    /*
     * 流程基本和刷数据脚本（/bhzShell/resetOrderTotalInStockOut.php） 中的 insertOTintoSO 方法一致
     * 参数 amount 一定是负数
     */
    public function insertOTIntoSO($orderId, $orderTotalId, $amount) {
        if (floatval($amount) >= 0) {
            return false;
        }
        $soList = array();
        $otList = array();
        $sql = "SELECT * FROM `stock_out` WHERE `refer_id`='".$orderId."' AND `refer_type_id`='1' AND `status`>0 ORDER BY `sale_money` DESC";
        $res = $this->db->query($sql);
        foreach ($res->rows as $row) {
            //保存订单下所有有效出库单到数组
            $soList[$row['out_id']] = array(
                'total' => $row['sale_money'],
                'otList' => $row['order_total_ids'],
            );
            //拼接所有出库单中已经计入总数的 OTPK
            $otList = array_merge($otList, explode(',', $row['order_total_ids']));
        }
        if (false === in_array($orderTotalId, $otList)) {//判断当前 OTPK 是否已经计入 出库单
            foreach ($soList as $outId=>$soInfo) {
                if ($soInfo['total'] > abs($amount)) {//判断当前 SO 总金额是否超过 OT金额
                    $newSoTotal = $soInfo['total']+$amount;
                    if ($soInfo['otList']) {
                        $newSoOTList = $soInfo['otList'].','.$orderTotalId;
                    }else{
                        $newSoOTList = $orderTotalId;
                    }
                    //保存 OTPK 到当前 SO
                    $sql = "UPDATE `stock_out` SET `sale_money`='".$newSoTotal."',`order_total_ids`='".$newSoOTList."' WHERE `out_id`='".$outId."'";
                    $this->db->query($sql);
                    break;
                }
            }
        }
    }

    /*
     * 根据订单编号，获取所有有效出库单的单品汇总数量
     * 暂时把拒收的出库单算进去，全部拒收的出库单，订单不会再走审核流程
     * 出库单修改和时未更新单品缺货数量，需要取现存出库信息参与配货
     */
    public function getValidSOPList($orderId) {
        $sopList = array();
        $soTotalPrice = 0;
        $sql = "SELECT `product_code`,`product_price`,`pay_product_price`,`product_quantity`,`counter_id` FROM `stock_out_detail` WHERE `out_id` IN (SELECT `out_id` FROM `stock_out` WHERE
        `refer_type_id`='1' AND `refer_id`='".$orderId."' AND `status`<>0)";
        $res = $this->db->query($sql);
        foreach ($res->rows as $row) {
            $sopList[$row['product_code']][$row['counter_id']]['price'] = $row['product_price'];
            $sopList[$row['product_code']][$row['counter_id']]['pay_price'] = $row['pay_product_price'];
            $sopList[$row['product_code']][$row['counter_id']]['qty'] += $row['product_quantity'];
            $soTotalPrice += $row['product_price']*$row['product_quantity'];
        }
        return compact('soTotalPrice', 'sopList');
    }

    public function getSOCountByOrder($orderId) {
        $sql = "SELECT COUNT(`out_id`) AS count FROM `stock_out` WHERE `refer_type_id`='1' AND `refer_id`='".$orderId."'";
        $res = $this->db->query($sql);
        return intval($res->row['count']);
    }

    public function updateByPK($outId, $soData) {
        $temp = array();
        foreach ($soData as $col=>$val) {
            $temp[] = "`".$col."`='".$this->db->escape($val)."'";
        }
        $sql = "UPDATE `stock_out` SET ".implode(',', $temp)." WHERE `out_id`='".$outId."'";
        $this->db->query($sql);
    }
}
