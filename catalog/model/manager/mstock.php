<?php
class ModelManagerMstock extends Model {
	
	//获取业务员全部出库单
	public function getStockOut($filter){
		
		if (isset($filter['logcenter_id']) && $filter['logcenter_id'] != 0){
			$where['o.logcenter_id'] = $filter['logcenter_id'];
		}
		else{
			return;
		}

		if(isset($filter['start'])){
			$start = $filter['start'];
		}
		else{
			$start = 0;
		}

		if(isset($filter['limit'])){
			$limit = $filter['limit'];
		}
		else{
			$limit = 10;
		}

		$where['refer_type_id'] = 1;
		
		//待配送
		$where['so.status'] = array('eq',2) ;
		$send_so = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('"send" as tab,o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();
		
		//待签收
		$where['so.status'] = array('eq',3) ;
		$sign_so = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('"sign" as tab,o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();
		
		//已拒收
		$where['so.status'] = array('eq',-1) ;
		$over_so = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('"reject" as tab,o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();
		
		//已签收
		$where['so.status'] = array('eq',4) ;
		$reject_so = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('"over" as tab,o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();

		$so = array_merge($send_so,$sign_so,$reject_so,$over_so);

		return $so;

	}
	
	//获取更多单据
	public function loadMoreOut($filter){
		
		if (isset($filter['tab'])){
			$tab = $filter['tab'];
		}

		if (isset($filter['logcenter_id'])){
			$where['o.logcenter_id'] = $filter['logcenter_id'];
		}

		if(isset($filter['status'])){
			$where['so.status'] = $filter['status'];
		}

		if(isset($filter['start'])){
			$start = $filter['start'];
		}

		if(isset($filter['limit'])){
			$limit = $filter['limit'];
		}
		else{
			$limit = 10;
		}

		$where['refer_type_id'] = 1;

		$more = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('"'.$tab.'" as tab,o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();

		return $more;

	}
	
	//获取业务员的电话
	public function getTel($customer_id){

		$tel = M('customer')->where('customer_id='.$customer_id)->getField('telephone');
		return $tel;

	}	
	
	//检查配送
	public function checkSend($data){
			
		$where['out_id'] = $data['out_id'];
		$save['status'] = 3;
		$save['deliver_date'] = date('Y-m-d H:i:s');
		M('stock_out')->where($where)->save($save);

		$so_comment = "手机端更改出库单状态为已配送";
		$this->addStockHistory($data['out_id'],$so_comment);

		$this->checkAllSend($data['refer_id']);

		$message = '单据已配送';

		return $message;

	}
	
	//检查订单的出库单是否全部配送
	public function checkAllSend($refer_id){
		
		$where['so.refer_id'] = $refer_id;
		$where['so.status'] = array('neq',0);
		$isSend = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->where($where)
			->group('so.refer_id')
			->field('min(status) as out_status,o.order_status_id as order_status')->select();

		if($isSend[0]['out_status'] > 1 && $isSend[0]['order_status'] == 18){
			
			$user = $this->getUser();

			$order_id = $refer_id;
			$order_status = 3;
			$comment = "用户：".$user[0]['username']."通过手机端配送更改订单状态为待收货";

			$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($order_id,$order_status,$comment);

		}

	}

	//检查签收
	public function checkSign($data){
			
		$where['out_id'] = $data['out_id'];
		$save['status'] = 4;
		$save['receive_date'] = date('Y-m-d H:i:s');
		M('stock_out')->where($where)->save($save);

		$so_comment = "手机端更改出库单状态为已签收";
		$this->addStockHistory($data['out_id'],$so_comment);

		$this->checkAllSign($data['refer_id']);

		$message = '单据已签收';

		return $message;

	}

	//检查订单的出库单是否全部签收
	public function checkAllSign($refer_id){
		
		$where['so.refer_id'] = $refer_id;
		$where['so.status'] = array('neq',0);
		$isSign = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->where($where)
			->group('so.refer_id')
			->field('min(so.status) as out_status,o.order_status_id as order_status')->select();

		if($isSign[0]['out_status'] > 3 && ($isSign[0]['order_status'] == 18 || $isSign[0]['order_status'] == 3)){
			
			$user = $this->getUser();

			$order_id = $refer_id;
			$order_status = 5;
			$comment = "用户：".$user[0]['username']."通过手机端签收更改订单状态为完成";

			$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($order_id,$order_status,$comment);

		}

	}

	//检查拒收
	public function checkReject($data){
			
		$where['out_id'] = $data['out_id'];
		$save['status'] = -1;
		$save['receive_date'] = date('Y-m-d H:i:s');
		M('stock_out')->where($where)->save($save);

		$so_comment = "手机端更改出库单状态为拒收";
		$this->addStockHistory($data['out_id'],$so_comment);

		$this->checkAllReject($data['refer_id']);

		$message = '单据已拒收';

		return $message;

	}

	//检查订单的出库单是否全部拒收
	public function checkAllReject($refer_id){
		
		$where['so.refer_id'] = $refer_id;
		$where['so.status'] = array('not in','-1,0');
		$isReject = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->where($where)->select();

		if(!$isReject){
			
			$user = $this->getUser();

			$order_id = $refer_id;
			$order_status = 13;
			$comment = "用户：".$user[0]['username']."通过手机端签收更改订单状态为拒收";

			$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($order_id,$order_status,$comment);

		}

	}
	
	//添加出库单日志
	public function addStockHistory($out_id,$comment){

		$user = $this->getUser();
		
		$data['out_id'] = $out_id;
		$data['user_id'] = $user[0]['user_id'];
		$data['operator_name'] = $user[0]['username'];
		$data['comment'] = $comment;
		$data['notify'] = 0;
		$data['date_added'] = date('Y-m-d H:i:s');

		M('stock_out_history')->data($data)->add();

	}

	//获取用户
	public function getUser(){

		$customer_id = $this->customer->getId();
		$where['contact_tel'] = M('customer')->where('customer_id='.$customer_id)->getField('telephone');
		$user = M('user')->where($where)->select();
		
		return $user;

	}

	//筛选送货单
	public function searchSend($filter){

		if (isset($filter['logcenter_id']) && $filter['logcenter_id'] != 0){
			$where['o.logcenter_id'] = $filter['logcenter_id'];
		}
		else{
			return;
		}

		if(isset($filter['start'])){
			$start = $filter['start'];
		}
		else{
			$start = 0;
		}

		if(isset($filter['limit'])){
			$limit = $filter['limit'];
		}
		else{
			$limit = 10;
		}

		$where['refer_type_id'] = 1;

		if ($filter['out_id'] != ""){
			$where['so.out_id'] = $filter['out_id'];
		}

		if ($filter['refer_id'] != ""){
			$where['so.refer_id'] = $filter['refer_id'];
		}

		if ($filter['out_id'] == "" && $filter['refer_id'] == ""){
			return;
		}
		
		$where['so.status'] = array('neq',0);
		$search = M('stock_out')
			->alias('so')
			->join('`order` o on so.refer_id = o.order_id','left')
			->join('order_status os on o.order_status_id = os.order_status_id','left')
			->join('inout_state ios on so.status = ios.id','left')
			->join('warehouse wh on so.warehouse_id = wh.warehouse_id','left')
			->where($where)
			->field('o.*,so.*,wh.name as warehouse,os.name as o_state,ios.name as so_state')
			->order('so.date_added desc,so.out_id desc')
			->limit($start,$limit)->select();
	
		return $search;

	}


}
?>