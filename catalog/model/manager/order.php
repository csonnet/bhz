<?php
class ModelManagerOrder extends Model {
    public function getOrdersListBySaleId($saleId, $page, $limit, $status) {
        switch ($status){
            case 'receiptWithOutPay'://已签收未付款【仅显示2017.10.01以后的订单】
                $oFilter = " AND `payment_code`='cod' AND `is_pay`='0' AND `date_added`>'20171001000000' AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=4)";
                break;
            case 'logcenterShipping'://配送站已发货
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=3)";
                break;
            case 'warehouseShipping'://仓库已发货
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=2)";
                break;
            case 'warehousePrepare'://仓库备货中
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=1)";
                break;
            case 'unstart'://订单待审核
                $oFilter = "AND (`order_status_id`=20 OR `order_status_id`=17) AND `verify_status`<2";
                break;
            default ://全部订单
                $oFilter = "";
        }
        $sql = "SELECT * FROM `".DB_PREFIX."order` AS O WHERE `recommend_usr_id`='".$saleId."' AND `order_status_id`<>16 ".$oFilter." ORDER BY order_id DESC LIMIT ".($page-1)*$limit.",".$limit;
        $res = $this->db->query($sql);
        return $res->rows;
    }

    public function getOrderListByArea($areaId, $page, $limit, $status) {
        switch ($status){
            case 'receiptWithOutPay'://已签收未付款【仅显示2017.10.01以后的订单】
                $oFilter = " AND `payment_code`='cod' AND `is_pay`='0' AND `date_added`>'20171001000000' AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=4)";
                break;
            case 'logcenterShipping'://配送站已发货
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=3)";
                break;
            case 'warehouseShipping'://仓库已发货
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=2)";
                break;
            case 'warehousePrepare'://仓库备货中
                $oFilter = " AND EXISTS (SELECT `out_id` FROM `stock_out` WHERE `refer_type_id`=1 AND `refer_id`=O.`order_id` AND `status`=1)";
                break;
            case 'unstart'://订单待审核
                $oFilter = "AND (`order_status_id`=20 OR `order_status_id`=17) AND `verify_status`<2";
                break;
            default ://全部订单
                $oFilter = "";
        }
        $sql = "SELECT * FROM `".DB_PREFIX."order` AS O WHERE `order_status_id`<>16 ";
        if ($areaId) {
            $sql .= " AND `recommend_usr_id` IN (SELECT `user_id` FROM `user` WHERE `sales_area_id`='".$areaId."')";
        }
        $sql .= $oFilter." ORDER BY order_id DESC LIMIT ".($page-1)*$limit.",".$limit;
        $res = $this->db->query($sql);
        return $res->rows;
    }

    public function getOrderInfoBySaleId($saleId, $orderId) {
        $sql = "SELECT * FROM `order` WHERE `recommend_usr_id`='".$saleId."' AND `order_id`='".$orderId."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getOrderProducts($orderId) {
        $sql = "SELECT OP.`order_product_id`,OP.`name`,OP.`quantity`,OP.`price`,OP.`product_id`,OP.`lack_quantity`,P.`image`,P.`product_type` FROM `order_product` AS OP LEFT JOIN `product` AS P ON (OP.`product_id`=P.`product_id`) WHERE `order_id`='".$orderId."'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getOrderProductsGroup($orderProductId) {
        $sql = "SELECT OPG.`product_id`,OPG.`quantity`,OPG.`price`,OPG.`total`,OPG.`lack_quantity`,P.`image`,P.`product_type`,PD.`name` FROM `order_product_group` AS OPG LEFT JOIN `product` AS P ON (OPG.`product_id`=P.`product_id`) LEFT JOIN `product_description` AS PD ON (OPG.`product_id`=PD.`product_id` AND PD.`language_id`='".intval($this->config->get('config_language_id'))."') WHERE `order_product_id`='".$orderProductId."'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getOrderTotals($orderId) {
        $sql = "SELECT * FROM `order_total` WHERE `order_id`='".$orderId."' ORDER BY `sort_order` ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getOrderStatusList() {
        $ret = array();
        $sql = "SELECT `order_status_id`,`name` FROM `order_status` WHERE `language_id`='".intval($this->config->get('config_language_id'))."'";
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $ret[$row['order_status_id']] = $row['name'];
        }
        return $ret;
    }

    public function getLogcenterList() {
        $ret = array();
        $sql = "SELECT `logcenter_id`,`logcenter_name` FROM `logcenters`";
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $ret[$row['logcenter_id']] = mb_substr($row['logcenter_name'], 0, 2);
        }
        return $ret;
    }

    public function getWarehouseList() {
        $ret = array();
        $sql = "SELECT `warehouse_id`,`name` FROM `warehouse`";
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $ret[$row['warehouse_id']] = mb_substr($row['name'], 0, 2);
        }
        return $ret;
    }

    /* 
     * 根据 订单编号 获取出库单详情
     */
    public function getStockOutByOrderId($orderId) {
        $sql = "SELECT * FROM `stock_out` WHERE `refer_type_id`='1' AND `refer_id`='".$orderId."' AND `status`>0 ORDER BY `out_id` ASC";
        $res = $this->db->query($sql);
        return $res->rows;
    }

    public function getStockOutProductsBySoId($soId) {
        $sql = "
        SELECT
            SOD.`product_quantity` AS qty,SOD.`product_price` AS price,SOD.`products_money` as total,P.`image`,PD.`name`,PD2.`name` AS pg_name
        FROM
            `stock_out_detail` AS SOD
            LEFT JOIN `product_option_value` AS POV ON (SOD.`product_code`=POV.`product_code`)
            LEFT JOIN `product` AS P ON (POV.product_id=P.product_id)
            LEFT JOIN `product_description` AS PD ON (POV.`product_id`=PD.`product_id` AND PD.`language_id`='".intval($this->config->get('config_language_id'))."')
            LEFT JOIN `product_group` AS PG ON (SOD.`counter_id`=PG.`product_group_id`)
            LEFT JOIN `product_description` AS PD2 ON (PG.`parent_product_id`=PD2.`product_id` AND PD2.`language_id`='".intval($this->config->get('config_language_id'))."')
        WHERE
            SOD.`out_id`='".$soId."'
        ";
        $res = $this->db->query($sql);
        return $res->rows;
        //echo $sql;
    }

}
