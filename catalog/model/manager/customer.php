<?php
class ModelManagerCustomer extends Model {
    public function getCustomersBySaleId($saleId, $page, $limit) {
        $start = max(0, ($page-1)*$limit);
        $sql = "SELECT C.`customer_id`,C.`fullname`,C.`telephone`,C.`shop_type`,C.`company_name`,C.`order_sum`,C.`last_order_date`,A.`fullname` AS AN,A.`company` AS AC,A.`address` AS AA,A.`shipping_telephone` AS AT FROM `customer` AS C LEFT JOIN `address` AS A ON (A.`address_id`=C.`address_id`) WHERE C.`user_id`='".$saleId."' LIMIT ".$start.",".$limit;
        $query = $this->db->query($sql);
        return $query->rows;
    }
}
