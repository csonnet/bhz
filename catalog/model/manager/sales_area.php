<?php
class ModelManagerSalesArea extends Model {
    public function getAllSalesArea() {
        $sql = "SELECT * FROM `".DB_PREFIX."sales_area` WHERE `status`='1' ORDER BY `sales_area_id` ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

}
