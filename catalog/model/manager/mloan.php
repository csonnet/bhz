<?php
class ModelManagerMloan extends Model {
	
	//获取待我操作的借货单
	public function getToDo($filter){

		$where['lo.user_id'] = $filter['user_id'];

		if(isset($filter['start'])){
			$start = $filter['start'];
		}
		else{
			$start = 0;
		}

		if(isset($filter['limit'])){
			$limit = $filter['limit'];
		}
		else{
			$limit = 10;
		}

		$verifyOrders = $this->getNeedVerify($filter['user_id']);

		//待审核
		$vwhere['lo.loan_order_id'] = array('in',$verifyOrders);
		$verify = M('loan_order')
			->alias('lo')
			->join('user u on lo.user_id = u.user_id','left')
			->join('logcenters l on lo.logcenter_id = l.logcenter_id','left')
			->join('warehouse w on lo.from_warehouse_id = w.warehouse_id','left')
			->join('loan_status ls on lo.status = ls.id','left')
			->where($vwhere)
			->field('lo.*,u.fullname as user_name,l.logcenter_name as logcenter,w.name as warehouse,ls.status_name as status_name')
			->limit($start,$limit)->order('lo.date_added desc')->select();
		
		//待签收
		$where['lo.status'] = 4;
		$sign = M('loan_order')
			->alias('lo')
			->join('user u on lo.user_id = u.user_id','left')
			->join('logcenters l on lo.logcenter_id = l.logcenter_id','left')
			->join('warehouse w on lo.from_warehouse_id = w.warehouse_id','left')
			->join('loan_status ls on lo.status = ls.id','left')
			->where($where)
			->field('lo.*,u.fullname as user_name,l.logcenter_name as logcenter,w.name as warehouse,ls.status_name as status_name')
			->limit($start,$limit)->order('lo.date_added desc')->select();

		//待收货
		$where['lo.status'] = 5;
		$return = M('loan_order')
			->alias('lo')
			->join('user u on lo.user_id = u.user_id','left')
			->join('logcenters l on lo.logcenter_id = l.logcenter_id','left')
			->join('warehouse w on lo.from_warehouse_id = w.warehouse_id','left')
			->join('loan_status ls on lo.status = ls.id','left')
			->where($where)
			->field('lo.*,u.fullname as user_name,l.logcenter_name as logcenter,w.name as warehouse,ls.status_name as status_name')
			->limit($start,$limit)->order('lo.date_added desc')->select();

		$todo = array_merge($verify,$sign,$return);

		return $todo;

	}
	
	//获取我发起的借货单
	public function getMyLoan($filter){

		$where['lo.user_id'] = $filter['user_id'];

		if(isset($filter['start'])){
			$start = $filter['start'];
		}
		else{
			$start = 0;
		}

		if(isset($filter['limit'])){
			$limit = $filter['limit'];
		}
		else{
			$limit = 10;
		}
		
		$myloans = M('loan_order')
			->alias('lo')
			->join('user u on lo.user_id = u.user_id','left')
			->join('logcenters l on lo.logcenter_id = l.logcenter_id','left')
			->join('warehouse w on lo.from_warehouse_id = w.warehouse_id','left')
			->join('loan_status ls on lo.status = ls.id','left')
			->where($where)
			->field('lo.*,u.fullname as user_name,l.logcenter_name as logcenter,w.name as warehouse,ls.status_name as status_name')
			->limit($start,$limit)->order('lo.date_added desc')->select();

		return $myloans;

	}

	public function getNeedVerify($user_id){
		
		$where['v.refer_type_id'] = 6;
		$where['lo.status'] = array('in','0,1');
		$nv = M('verify')
			->alias('v')
			->join('loan_order lo on v.refer_id = lo.loan_order_id','left')
			->where($where)
			->select();

		$verifyOrders = "";

		foreach($nv as $v){

			if($v['level'] >= $v['vlevel']){
				$verifyOrders .= $v['refer_id'].",";
			}

		}

		return $verifyOrders;

	}

}
?>