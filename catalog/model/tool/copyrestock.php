<?php
class ModelToolCopyrestock extends Model {

	public function getCustomer($cus_name) {
		$sql = "SELECT c.customer_id FROM customer AS c WHERE c.telephone = '".$cus_name."' OR   c.company_name LIKE  '%".$cus_name."%' limit 0,1";
		// echo $sql;
		$query = $this->db->query($sql);
		$customer_id =  $query->row['customer_id'];
		return $customer_id;
	}
	public function get_solution_id($shelf_id) {
		$sql = "SELECT  solution_id FROM shelf AS c WHERE shelf_id = ".$shelf_id;
		// echo $sql;
		$query = $this->db->query($sql);
		$solution_id =  $query->row['solution_id'];
		return $solution_id;
	}

	public function copystock($shelf_product_id,$stock_num) {
		M('shelf_product')->where(array('shelf_product_id'=>$shelf_product_id))->save(array('stock_qty_temp'=>$stock_num));

		// echo $sql;
		return M('shelf_product')->where(array('shelf_product_id'=>$shelf_product_id))->find();
	}
	// public function getshelf($shelf_customer_id) {
	// 	$sql = "SELECT s.*, (SELECT company_name FROM customer  WHERE customer.customer_id = s.customer_id) AS 'company_name', (SELECT solution_name FROM solution  WHERE solution.solution_id = s.solution_id) AS 'solution_name'FROM shelf AS s WHERE s.customer_id =".$shelf_customer_id." order BY shelf_id";
	// 	$query = $this->db->query($sql);
	// 	return $query->rows;
	// }
	//------------------------------------------------------------------------
	public function getshelf($solution_id) {
		$sql = "select shelf.* FROM shelf where status =1 AND solution_id =".$solution_id." order BY shelf_id";
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getshelfinfo($shop_id) {
		$sql = "select shelf.* FROM shelf where status =1 AND shop_id =".$shop_id." order BY shelf_id";
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getcustshelf($shelfIds) {
		$sql = "select shelf.* FROM shelf where status =1 AND shelf_id =".$shelfIds." order BY shelf_id";
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
//-------------------
	public function get_shelfname($solution_id) {
		$sql = "select shelf.* FROM shelf where status =1 AND solution_id =".$solution_id." order BY shelf_id";
		// echo $sql;
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			$data[] =$value['shelf_id'].'_'.$value['shelf_name'];
		}
		return $data;
	}

	public function getshelfname($shop_id) {
		$sql = "select shelf.* FROM shelf where status =1 AND shop_id =".$shop_id." order BY shelf_id";

		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			$data[] =$value['shelf_id'].'_'.$value['shelf_name'];
		}
		return $data;
	}

	public function get_shelfinfo($customer_id) {
		$sql = "select shelf.* FROM shelf where status =1 AND customer_id =".$customer_id." order BY shelf_id";

		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			$data[] =$value['shelf_id'].'_'.$value['shelf_name'];
		}
		return $data;
	}


	public function shelfpro($shelf_id) {
		$sql = "SELECT sp.* FROM shelf_product AS sp WHERE sp.shelf_id=".$shelf_id.' order BY shelf_layer,layer_position';
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;

	}

	//获取货架商品详细
	public function getShelfProducts($shelf_id){

		$where['sp.shelf_id'] = $shelf_id;
		$shelfProducts = M('shelf_product')
			->alias('sp')
			->join('product p on sp.product_id = p.product_id','left')
			->where($where)
			->field('p.product_id as product_id,sp.shelf_layer,sp.layer_position,sp.min_order_qty,sp.show_qty,sp.shelf_product_id,sp.stock_qty_temp,sp.stock_qty')
			->select();
			// echo M('shelf_product')->getlastSql();

		return $shelfProducts;

	}


	public function SerShelfProducts($solution_id,$searchInput){
		$where = " shelf_id in (select shelf_id where solution_id=".$solution_id.")  AND ( p.sku LIKE '%".$searchInput."%' or pd.name LIKE '%".$searchInput."%' ";
		$where['sp.shelf_id'] = $shelf_id;
		$shelfProducts = M('shelf_product')
			->alias('sp')
			->join('product p on sp.product_id = p.product_id','left')
			->join('product_description pd on sp.product_id = p.product_id','left')
			->where($where)
			->field('p.product_id as product_id,sp.shelf_layer,sp.layer_position,sp.min_order_qty,sp.show_qty,sp.shelf_product_id')
			->select();
			echo M('shelf_product')->getlastSql();

		return $shelfProducts;

	}

	public function changeop($shelf_product_id,$shelf_id,$shelf_x,$shelf_y){

		$where['shelf_product_id'] = $shelf_product_id;

		$arr= M('shelf_product') ->where($where) ->save(array(
		 	'shelf_id'=>$shelf_id,
		 	'layer_position'=>$shelf_x,
		 	'shelf_layer'=>$shelf_y,
		 ));

			// echo M('shelf_product')->getlastSql();
		if($arr){
						return true;
		}


	}
	public function getListProduct($id){

		//检查区域
		if($this->checkArea($id) == 0){
			return;
		}

		$where['p.product_id'] = $id;

		$now = date("Y-m-d");
		$where['ps.date_start'] = array('elt',$now);
		$where['ps.date_end'] = array(array('egt',$now),array('eq','0000-00-00'), 'or') ;
		$where['ps.customer_group_id'] = (int)$this->config->get('config_customer_group_id');

		$lp = M('product')
			->alias('p')
			->join('product_description pd on p.product_id = pd.product_id','left')
			->join('product_special ps on p.product_id = ps.product_id','left')
			->where($where)
			->order('ps.priority asc, ps.price asc')
			->field('p.*,pd.name as name,ps.price as special')
			->limit(1)
			->select();

		if(!$lp){
			$bwhere['p.product_id'] = $id;
			$lp = M('product')
				->alias('p')
				->join('product_description pd on p.product_id = pd.product_id','left')
				->where($bwhere)
				->field('p.*,pd.name as name,p.sale_price as special')
				->select();
		}

		return $lp;

	}




	public function sign($order_id) {
		//优惠金额
		$sql = "SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order";
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			// var_dump($value);
			if ($value['value']<0) {
				$coupon_price +=$value['value'];
			}
		}
		// var_dump($coupon_price);
		// 签收金额
        $sql = "SELECT op.*, (SELECT product_type FROM `".DB_PREFIX."product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = '" . (INT)$order_id . "'ORDER BY order_ids";
        // echo $sql;
       	$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			if($value['product_type'] == 2){
				$sql = "SELECT SUM(opg.sign_quantity*opg.price ) AS 'pgtotal'FROM `".DB_PREFIX."order_product_group` opg WHERE order_product_id = '".$value['order_product_id']."'";
				$query = $this->db->query($sql);

		        $total += $query->row['pgtotal'];

                continue;
        	}
        	$total+= $value['sign_quantity']*$value['price'];
		}
		if ($coupon_price<0) {
			$total+=$coupon_price;
		}

		M('order')->where(array('order_id' => $order_id))->save(array('sign_price' => $total,'conform_price' => $total));
        return true;
	}


	public function conform_pay($order_id,$sign_price) {

		$sql = "UPDATE `".DB_PREFIX."order` SET `conform_price`=".$sign_price." ,`conform_pay_status`=1 WHERE `order_id`='".$order_id."'";
		// echo $sql;
        $this->db->query($sql);

        return true;
	}

	public function getAllOrderProductsBysku($skuname,$customer_id) {
		$sql = "SELECT op.order_product_id, op.product_id,op.product_code,  op.name, MAX(op.price) as 'price', (SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) AS sku, (SELECT product_type FROM `product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `order_product` op WHERE op.order_id in (SELECT order_id FROM `order` WHERE `order`.customer_id =".$customer_id."  AND order_status_id <>16 )  AND ((SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) LIKE '%".$skuname."%' OR op.`name` LIKE '%".$skuname."%') group  BY op.product_code";
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getAllOrderProducts($customer_id) {
		$sql = "SELECT op.order_product_id, (SELECT product_type FROM `product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM " .DB_PREFIX. "`order_product` op WHERE op.order_id in (SELECT order_id FROM `order` WHERE `order`.customer_id =".$customer_id." AND order_status_id <>16 )  ORDER BY order_ids";
		$query = $this->db->query($sql);
		// echo $sql;
		return $query->rows;
	}
	public function getAllchildorderproductsBysku($order_product_id,$skuname){
		$sql = "SELECT opg.*, pd. NAME AS pdname, p.sku AS sku, p.model AS model FROM `".DB_PREFIX."order_product_group` opg LEFT JOIN `".DB_PREFIX."product_description` pd ON opg.product_id = pd.product_id LEFT JOIN `".DB_PREFIX."product` p ON opg.product_id = p.product_id WHERE order_product_id = '".$order_product_id."'  AND (p.sku LIKE '%".$skuname."%' OR pd.`name` LIKE '%".$skuname."%') ORDER BY opg.order_ids";
		// echo $sql;
		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function addproduct($list,$shelf_index){
		foreach ($list as $key => $value) {
			M('shelf_product')->add(array(
				'shelf_id'=>$shelf_index,
				'product_id'=>$value['id'],
				'product_code'=>$value['product_code'],
				'date_added'=>date("Y-m-d H:i:s"),
			));
			// echo M('shelf_product')->getlastSql();die();
			$list[$key]['shelf_id'] = $shelf_index;
			$list[$key]['buyqty'] = $value['addnum'];
			$list[$key]['stock_qty_temp'] = 0;
		}

		return $list;
	}


}
