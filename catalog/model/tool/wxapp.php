<?php
class ModelToolWxapp extends Model {
	
	//检查用户是否第一次开启小程序
	public function checkMp($filter){

		$where['wx_mp_openid'] = $filter['openid'];
		$customer_id = M('customer')->where($where)->getField('customer_id');

		if($customer_id){
			return $customer_id;
		}
		else{
			return 0;
		}

	}

	//测试算中奖总金额
	public function getOrderLucky($luckydraw_id,$order_id){
		
		$where['lh.luckydraw_id'] = $luckydraw_id;
		$where['lh.order_id'] = $order_id;
		$where['lh.customer_id'] = $this->customer->getId();
		$where['lh.hit'] = 1;
		$temp = M('luckydraw_history')
			->alias('lh')
			->join('luckydraw_product lp on lh.luckydraw_product_id = lp.luckydraw_product_id','left')
			->where($where)
			->Field('lp.product_name')->select();

		$total = 0;
		foreach($temp as $v){
			$str = $v['product_name'];
			$str = substr($str,0,strlen($str)-1);
			$total += (float)$str;
		}

		print_r("用户：".$where['lh.customer_id']." 的订单：".$order_id."，中奖金额为：".$total."元");

	}

}