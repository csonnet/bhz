<?php
class ModelToolSmsLog extends Model {
    public function saveData($data) {
        if (count($data) < 1)
            return false;
        $sql = "INSERT INTO `sms_log` SET ";
        foreach ($data as $col=>$val) {
            $sql .= "`".$col."`='".$this->db->escape($val)."',";
        }
        $this->db->query(substr($sql, 0, -1));
    }

}
