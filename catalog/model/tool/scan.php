<?php
class ModelToolScan extends Model {
	public function checkorder($orderid) {
		$sql = "SELECT o.order_id FROM `order` AS o  WHERE o.order_status_id<>0 AND o.order_status_id<>16 AND o.order_id =".$orderid;
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->row['order_id'];
		
	}
	public function getStockOut($orderid) {
		$sql = "SELECT o.order_id,o.total,o.ori_total, o.payment_company AS market, o.shipping_address AS address, o.shipping_fullname AS 'name', o.telephone AS tel, o.payment_method, o.is_pay, os.`name` AS 'order_status', o.sign_price, o.`conform_price`, o.`conform_pay_status` FROM `order` o LEFT JOIN order_status os ON os.order_status_id = o.order_status_id WHERE o.order_id =  ".$orderid." AND o.order_status_id<>0 AND o.order_status_id<>16";
		$query = $this->db->query($sql);
		return $query->row;
		
	}

	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

	


	public function getOrderProducts($order_id) {
		$sql = "SELECT op.*, (SELECT sku FROM `".DB_PREFIX."product` p WHERE p.product_id = op.product_id ) AS sku, (SELECT product_type FROM `".DB_PREFIX."product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = '" . (INT)$order_id . "'ORDER BY order_ids";
		// echo $sql;die();
		$query = $this->db->query($sql);
		
		return $query->rows;
	}


	public function getchildorderproducts($order_product_id){
		$sql = "SELECT opg.*, pd. NAME AS pdname, p.sku AS sku, p.model AS model FROM `".DB_PREFIX."order_product_group` opg LEFT JOIN `".DB_PREFIX."product_description` pd ON opg.product_id = pd.product_id LEFT JOIN `".DB_PREFIX."product` p ON opg.product_id = p.product_id WHERE order_product_id = '".$order_product_id."'";
		// echo $sql;die();
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getOrderProductsBysku($sku,$order_id,$name) {
		$sql = "SELECT op.*, (SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) AS sku, (SELECT product_type FROM `product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `order_product` op WHERE op.order_id = '".$order_id."'AND ((SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) LIKE '%".$sku."%' OR op.`name` LIKE '%".$sku."%') ORDER BY order_ids";
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
		
	}
	
	public function getchildorderproductsBysku($order_product_id,$sku,$name){
		$sql = "SELECT opg.*, pd. NAME AS pdname, p.sku AS sku, p.model AS model FROM `".DB_PREFIX."order_product_group` opg LEFT JOIN `".DB_PREFIX."product_description` pd ON opg.product_id = pd.product_id LEFT JOIN `".DB_PREFIX."product` p ON opg.product_id = p.product_id WHERE order_product_id = '".$order_product_id."'  AND (p.sku LIKE '%".$sku."%' OR pd.`name` LIKE '%".$sku."%') ORDER BY opg.order_ids";
		// echo $sql;
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	
	public function conform($product_code,$sign_quantity,$order_id,$order_ids){
		$flag = strpos($order_ids,'-');
		if ($flag) {
			$order_id_arr = explode('-',$order_ids);
			$order_product_id = M('order_product')->where(array('order_id' => $order_id,'order_ids'=>$order_id_arr[0]))->getField('order_product_id');
			// echo M('order_product')->getlastsql();
			// echo $order_product_id;
			M('order_product_group')->where(array('order_product_id' => $order_product_id,'order_ids'=>$order_id_arr[1],'product_code'=>$product_code))->save(array('sign_quantity' => $sign_quantity));
			// echo M('order_product_group')->getlastsql();

		}else{
			M('order_product')->where(array('order_id' => $order_id,'order_ids'=>$order_ids,'product_code'=>$product_code))->save(array('sign_quantity' => $sign_quantity));
			// echo M('order_product')->getlastsql();
		}
		
		return true;
	}

	
	public function sign($order_id) {
		//优惠金额
		$sql = "SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order";
		$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			// var_dump($value);
			if ($value['value']<0) {
				$coupon_price +=$value['value'];
			}
		}
		// var_dump($coupon_price);
		// 签收金额
        $sql = "SELECT op.*, (SELECT product_type FROM `".DB_PREFIX."product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = '" . (INT)$order_id . "'ORDER BY order_ids";
        // echo $sql;
       	$query = $this->db->query($sql);
		foreach ($query->rows as $key => $value) {
			if($value['product_type'] == 2){
				$sql = "SELECT SUM(opg.sign_quantity*opg.price ) AS 'pgtotal'FROM `".DB_PREFIX."order_product_group` opg WHERE order_product_id = '".$value['order_product_id']."'";
				$query = $this->db->query($sql);

		        $total += $query->row['pgtotal'];
				
                continue;
        	}
        	$total+= $value['sign_quantity']*$value['price'];
		}
		if ($coupon_price<0) {
			$total+=$coupon_price;
		}

		M('order')->where(array('order_id' => $order_id))->save(array('sign_price' => $total,'conform_price' => $total));
        return true;
	}

	
	public function conform_pay($order_id,$sign_price) {

		$sql = "UPDATE `".DB_PREFIX."order` SET `conform_price`=".$sign_price." ,`conform_pay_status`=1 WHERE `order_id`='".$order_id."'";
		// echo $sql;
        $this->db->query($sql);

        return true;
	}
	
	public function getAllOrderProductsBysku($skuname,$customer_id) {
		$sql = "SELECT op.order_product_id, op.product_id,op.product_code,  op.name, MAX(op.price) as 'price', (SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) AS sku, (SELECT product_type FROM `product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM `order_product` op WHERE op.order_id in (SELECT order_id FROM `order` WHERE `order`.customer_id =".$customer_id."  AND order_status_id <>16 )  AND ((SELECT sku FROM `product` p WHERE p.product_id = op.product_id ) LIKE '%".$skuname."%' OR op.`name` LIKE '%".$skuname."%') group  BY op.product_code";
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getAllOrderProducts($customer_id) {
		$sql = "SELECT op.order_product_id, (SELECT product_type FROM `product` p2 WHERE p2.product_id = op.product_id ) AS product_type FROM " .DB_PREFIX. "`order_product` op WHERE op.order_id in (SELECT order_id FROM `order` WHERE `order`.customer_id =".$customer_id." AND order_status_id <>16 )  ORDER BY order_ids";
		$query = $this->db->query($sql);
		// echo $sql;
		return $query->rows;
	}
	public function getAllchildorderproductsBysku($order_product_id,$skuname){
		$sql = "SELECT opg.*, pd. NAME AS pdname, p.sku AS sku, p.model AS model FROM `".DB_PREFIX."order_product_group` opg LEFT JOIN `".DB_PREFIX."product_description` pd ON opg.product_id = pd.product_id LEFT JOIN `".DB_PREFIX."product` p ON opg.product_id = p.product_id WHERE order_product_id = '".$order_product_id."'  AND (p.sku LIKE '%".$skuname."%' OR pd.`name` LIKE '%".$skuname."%') ORDER BY opg.order_ids";
		// echo $sql;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
