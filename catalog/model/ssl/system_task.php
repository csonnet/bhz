<?php
class ModelSslSystemTask extends Model {
    private $_defaultDB = 'system_task';

    public function addNew($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }
}
