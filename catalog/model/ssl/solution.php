<?php
class ModelSslSolution extends Model {
    private $_defaultDB = 'solution';
 //------------------------------
        public function getList($filter) {
        $sql = "SELECT SOL.`solution_id`,SOL.`solution_name`,SOL.`shelf_length`,SOL.`status` FROM `".DB_PREFIX.$this->_defaultDB."` AS SOL ";
        $where = array();
        if ($filter['customer_id']) {
            //$where[] = "SOL.`customer_id`='0'";//调试用，显示所有模板
            $where[] = "SOL.`customer_id`='".$filter['customer_id']."'";
        }
        if (count($where))
            $sql .= " WHERE ".implode(" AND ", $where);
            $sql .=" order by SOL.`date_added` desc";
        $q = $this->db->query($sql);
        return $q->rows;
    }
//获取业务员对应商铺下的方案-------------
    public function getall($filter){
        $customer_id=$filter['customer_id'];
        $q=$this->info($customer_id);
        $this->load->model('ssl/customer');
        $list = $this->model_ssl_customer->_getUserIdByTel($q['telephone']);
         $listinfo=$this->getList($filter);
        if($list['userId']!==0){
            if(!empty($filter['name'])){
                $allpoint=$this->allpoint($list['userId'],$filter['name']);
                return $allpoint;
            }else{
                $allpoint=$this->allpoint($list['userId']);
            }

             foreach ($listinfo as $key => $value) {
               $listinfo[$key]['company_name']="个人方案";
             }
            if($customer_id==3867||$customer_id==4440||$customer_id==51||$customer_id==6544||$customer_id==4449){
                 $template=$this->gettemplate();
                 foreach ($template as $key => $value) {
                    $template[$key]['company_name']="模板:".$value['solution_name'];
                 }
            }
            $allscheme=array_merge($listinfo,$allpoint);
            if(isset($template)){
                $allsoul=array_merge($allscheme,$template);
                 return $allsoul;
            }else{
                 return $allscheme;
            }

        }else{
             foreach ($listinfo as $key => $value) {
               $listinfo[$key]['company_name']=$q['company_name'];
            }
            return $listinfo;
        }
    }
//-------------------------------------
     public function info($customer_id) {
        $sql = "SELECT customer_id,telephone,company_name FROM `".DB_PREFIX."customer` WHERE `customer_id`='".$customer_id."'";
        $q = $this->db->query($sql);
        return $q->row;
    }
//---------------------------------------
     public function allpoint($user_id,$name=null){
        $sql="SELECT  SOL.`solution_id`,SOL.`solution_name`,SOL.`shelf_length`,SOL.`status`,c.company_name FROM `".DB_PREFIX.$this->_defaultDB."` AS SOL LEFT JOIN `".DB_PREFIX."customer` AS c on c.customer_id=SOL.customer_id WHERE c.user_id='".$user_id."'";
        if(!empty($name)){
            $sql.=" AND c.company_name LIKE '%".$name."%'";
        }
        $q = $this->db->query($sql);
        return $q->rows;
    }
    //获取业务员手下的店铺
     public function  getallshop($filter){
        $customer_id=$filter['customer_id'];
        $q=$this->customerinfo($customer_id);
        $this->load->model('ssl/customer');
        $list = $this->model_ssl_customer->_getUserIdByTel($q['telephone']);

        if($list['userId']!==0){
            if(!empty($filter['name'])){
                $allpoint=$this->alluserpoint($list['userId'],$filter['name']);
            }else{
                $allpoint=$this->alluserpoint($list['userId']);
            }
            if($customer_id==3867||$customer_id==4440||$customer_id==51||$customer_id==6544||$customer_id==4449){
                    $template=$this->gettemp();
                    $temp[0]['shop_name']="模板";
                    $temp[0]['shelf_length']=$template;
                    $temp[0]['template']=1;
            }
            if(isset($temp)){
                $allsoul=array_merge($allpoint,$temp);
                 return $allsoul;
            }else{
                 return $allpoint;
            }

        }
    }

    //业务员手下加盟的店铺
    public function alluserpoint($user_id,$name=null){
        if(!empty($name)){
            $sql="SELECT c.`shop_id`,shop.`shop_name`,shop.`status`,c.company_name,c.customer_id,c.fullname FROM `shop` AS shop LEFT JOIN `customer` AS c   on c.customer_id=shop.customer_id WHERE shop.shop_name LIKE '%".$name."%' limit 100";
        }else{
            $sql="SELECT c.`shop_id`,shop.`shop_name`,shop.`status`,c.company_name,c.customer_id,c.fullname FROM `shop` AS shop LEFT JOIN `customer` AS c   on c.customer_id=shop.customer_id WHERE c.user_id='".$user_id."'";
        }

        $q = $this->db->query($sql);
        return $q->rows;
    }


    public function customerinfo($customer_id) {
        $sql = "SELECT customer_id,telephone,company_name FROM `".DB_PREFIX."customer` WHERE `customer_id`='".$customer_id."'";
        $q = $this->db->query($sql);
        return $q->row;
    }

    public function addNew($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function modifyByPK($pk, $info) {
        if ($pk && count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "UPDATE `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp)." WHERE `solution_id`='".$pk."'";
            $this->db->query($sql);
        }
    }

		//获取购买货架分类拼接名称
	public function getCategoryName($data){

		$category = array();
		foreach($data as $v){
			$category[] = $v['category_id'];
		}
		$category = array_unique($category);

		$cids = implode(',', $category);
		$where['category_id'] = array('in',$cids);
		$result = M('category_description')->where($where)->getField('name',true);
		$result = implode('/',$result);

		return $result;

	}

    //获取模板方案-----------------
    public function gettemplate(){
        $sql = "SELECT  SOL.`solution_id`,SOL.`solution_name`,SOL.`shelf_length`,SOL.`status` FROM `".DB_PREFIX.$this->_defaultDB."` AS SOL WHERE SOL.is_template=1";
        $q = $this->db->query($sql);
        return $q->rows;
    }
    //模板
    public function gettemp(){
         $sql = "SELECT  shelf_id as shelf_length
                FROM `".DB_PREFIX.$this->_defaultDB."` AS SOL
                LEFT JOIN shelf ON shelf.solution_id=SOL.solution_id
                WHERE SOL.is_template=1";
        $q = $this->db->query($sql);
        return $q->num_rows;
    }
}
