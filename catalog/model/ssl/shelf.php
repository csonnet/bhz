<?php
class ModelSslShelf extends Model {
    private $_defaultDB = 'shelf';

    public function getList($filter) {
        $sql = "SELECT SH.`shelf_id`,SH.`shelf_name`,SH.`status`,SH.`date_added`,SH.`date_enter`,SH.`date_out` FROM `".DB_PREFIX.$this->_defaultDB."` AS SH";
        $where = array();
        if ($filter['customer_id']) {
            $where[] = "SH.`customer_id`='0'";//调试用，显示所有模板
            //$where[] = "SOL.`customer_id`='".$filter['customer_id']."'";
        }
        if ($filter['solution_id']) {
            $where[] = "SH.`solution_id`='".$filter['solution_id']."'";
        }
        if (count($where))
            $sql .= " WHERE ".implode(" AND ", $where);
        $q = $this->db->query($sql);
        $list = $q->rows;
        foreach ($list as $k=>$v) {
            $list[$k]['productList'] = $this->_getShelfProducts($v['shelf_id']);
        }
        return $list;
    }

    public function addNew($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function addNewProduct($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX."shelf_product` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
        return false;
    }

    public function modifyByPK($pk, $info) {
        if ($pk && count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "UPDATE `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp)." WHERE `shelf_id`='".$pk."'";
            $this->db->query($sql);
        }
    }


    public function shelfinshop($shop_id) {
        $sql = "SELECT `shelf_id` FROM `shelf` WHERE `shop_id`='".$shop_id."' AND `status`='1'";
         //$sql = "SELECT `shelf_id` FROM `shelf` WHERE `customer_id`=51 AND `status`='1'";
        $q = $this->db->query($sql);

         $info=$q->row;
        // foreach ($info as $key => $value) {
        //     $arr[$key]=$value['shelf_id'];
        // }
        // $shelf['shelfs']=implode(',',$arr);
         $shelf['shelfs']=$info['shelf_id'];
        $shelf['num']=$q->num_rows;

        return $shelf;
    }
//-----------------------------------
      public function getEnterShelfInSolution($solutionId) {
        $sql = "SELECT `shelf_id` FROM `shelf` WHERE `solution_id`='".$solutionId."' AND `status`='1'";
        $q = $this->db->query($sql);
        return intval($q->row['shelf_id']);
    }

    protected function _getShelfProducts($shelfId) {
        return array();
    }
}
