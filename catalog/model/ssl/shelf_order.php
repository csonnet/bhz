<?php
class ModelSslShelfOrder extends Model {
    private $_defaultDB = 'shelf_order';

    public function getList($filter) {
    }

    public function addNew($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function addNewProduct($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX."shelf_order_product` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
        return false;
    }

    public function getByPK($shelfOrderId) {
        $ret = array();
        $sql = "SELECT * FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `shelf_order_id`='".$shelfOrderId."'";
        $q = $this->db->query($sql);
        $ret = $q->row;
        $sql = "SELECT * FROM `".DB_PREFIX."shelf_order_product` WHERE `shelf_order_id`='".$shelfOrderId."'";
        $q = $this->db->query($sql);
        $ret['products'] = $q->rows;
        return $ret;
    }

    public function getCustomerInfoByShelfOrderId($shelfOrderId) {
        $sql = "SELECT `customer_id`,`order_id`,`date_customer_check`,`date_user_check` FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `shelf_order_id`='".$shelfOrderId."'";
        $q = $this->db->query($sql);
        $customerId = intval($q->row['customer_id']);
        $orderId = intval($q->row['order_id']);
        $dateCustomerCheck = ('0000-00-00 00:00:00' == $q->row['date_customer_check'])?0:strtotime($q->row['date_customer_check']);
        $dateUserCheck = ('0000-00-00 00:00:00' == $q->row['date_user_check'])?0:strtotime($q->row['date_user_check']);
        $sql = "SELECT `customer_group_id`,`fullname`,`email`,`telephone`,`fax`,`user_id`,`logcenter_id` FROM `".DB_PREFIX."customer` WHERE `customer_id`='".$customerId."'";
        $q = $this->db->query($sql);
        $customerGroupId = intval($q->row['customer_group_id']);
        $fullName = trim($q->row['fullname']);
        $email = trim($q->row['email']);
        $telephone = trim($q->row['telephone']);
        $fax = trim($q->row['fax']);
        $userId = intval($q->row['user_id']);
        $logcenterId = intval($q->row['logcenter_id']);
        return compact('customerId', 'orderId', 'dateCustomerCheck', 'dateUserCheck', 'customerGroupId', 'fullName', 'email', 'telephone', 'fax', 'userId', 'logcenterId');
    }

    public function modifyByPK($pk, $info) {
        if ($pk && count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "UPDATE `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp)." WHERE `shelf_order_id`='".$pk."'";
            $this->db->query($sql);
        }
    }

    public function solutionEnter($pk) {
        var_dump($pk);die;
        $sql = "UPDATE `".DB_PREFIX."shelf` SET `status`='1' WHERE `shelf_id` IN (SELECT `shelf_ids` FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `shelf_order_id`='".$pk."')";
        $this->db->query($sql);
        $sql = "UPDATE `solution` SET `status`='1' WHERE `solution_id` IN (SELECT `solution_id` FROM `".DB_PREFIX."shelf` WHERE `shelf_id` IN (SELECT `shelf_ids` FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `shelf_order_id`='".$pk."'))";
        $this->db->query($sql);
    }
    //修改货架状态
     public function shelfEnter($pk) {
        foreach ($pk as $key => $value) {
            $sql = "UPDATE `".DB_PREFIX."shelf` SET `status`='1' WHERE `shelf_id`=".$value;
            $this->db->query($sql);
        }

    }
}
