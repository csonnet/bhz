<?php
class ModelSslOrder extends Model {
    private $_defaultDB = 'order';

    public function getList($filter) {
    }

    public function addNew($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$this->_defaultDB."` SET ".implode(',', $temp);
            // echo $sql;
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function addNewProduct($info) {
        if (count($info)) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX."order_product` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function addNewInTable($info, $tableName) {
        if (count($info) && '' != $tableName) {
            $temp = array();
            foreach ($info as $col=>$val) {
                $temp[] = "`".$col."`='".$this->db->escape($val)."'";
            }
            $sql = "INSERT INTO `".DB_PREFIX.$tableName."` SET ".implode(',', $temp);
            $this->db->query($sql);
            return $this->db->getLastId();
        }
    }

    public function getCompanyByAddressId($addressId) {
        $sql = "SELECT `company` FROM `".DB_PREFIX."address` WHERE `address_id`='".$addressId."'";
        $q = $this->db->query($sql);
        return $q->row['company'];
    }

    public function getCountryIdByName($name) {
        $sql = "SELECT `country_id` FROM `".DB_PREFIX."country` WHERE `name`='".$this->db->escape($name)."'";
        $q = $this->db->query($sql);
        return $q->row['country_id'];
    }
    
    public function getZoneIdByName($countryId, $name) {
        $sql = "SELECT `zone_id` FROM `".DB_PREFIX."zone` WHERE `country_id`='".$countryId."' AND `name`='".$this->db->escape($name)."'";
        $q = $this->db->query($sql);
        return $q->row['zone_id'];
    }

    public function getCityIdByName($zoneId, $name) {
        $sql = "SELECT `city_id` FROM `".DB_PREFIX."city` WHERE `zone_id`='".$zoneId."' AND `name`='".$this->db->escape($name)."'";
        $q = $this->db->query($sql);
        return $q->row['city_id'];
    }
}
