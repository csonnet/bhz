<?php
class ModelSslProduct extends Model {
    private $_defaultDB = 'product';

    public function getByPK($pk) {

        if ($pk) {
            $sql = "SELECT `name`,`description` FROM `".DB_PREFIX."product_description` WHERE `product_id`='".$pk."' AND `language_id`='1'";
            $q = $this->db->query($sql);
            $ret['name'] = $q->row['name'];
            $ret['description'] = $q->row['description'];
            $sql = "SELECT `model`,`image`,`product_class1`,`product_class2`,`product_class3`,`sku`  FROM `".DB_PREFIX.$this->_defaultDB."` WHERE `product_id`='".$pk."'";
            $q = $this->db->query($sql);
            $ret['model'] = $q->row['model'];
            $ret['image'] = $q->row['image'];
            $ret['product_class1'] = $q->row['product_class1'];
            $ret['product_class2'] = $q->row['product_class2'];
            $ret['product_class3'] = $q->row['product_class3'];
            $ret['sku'] = $q->row['sku'];
            $sql = "SELECT POV.`product_option_value_id`,POV.`product_option_id`,POV.`option_id`,POV.`option_value_id`,O.`type` AS `option_type` FROM `".DB_PREFIX."product_option_value` AS POV LEFT JOIN `".DB_PREFIX."option` AS O ON (O.`option_id`=POV.`option_id`) WHERE POV.`product_id`='".$pk."'";
            $q = $this->db->query($sql);
            $ret['productOptionValueId'] = $q->row['product_option_value_id'];
            $ret['productOptionId'] = $q->row['product_option_id'];
            $ret['optionId'] = $q->row['option_id'];
            $ret['optionValueId'] = $q->row['option_value_id'];
            $ret['optionType'] = $q->row['option_type'];
            $sql = "SELECT `name` FROM `".DB_PREFIX."option_description` WHERE `option_id`='".$ret['optionId']."' AND `language_id`='1'";
            $q = $this->db->query($sql);
            $ret['optionName'] = $q->row['name'];
            $sql = "SELECT `name` FROM `".DB_PREFIX."option_value_description` WHERE `option_value_id`='".$ret['optionValueId']."' AND `language_id`='1'";
            $q = $this->db->query($sql);
            $ret['optionValueName'] = $q->row['name'];
            return $ret;
        }
        return array();
    }
    //获取单品所有图片信息
    public function  getimages($id){
       $images = $this->db->query("SELECT pi.image FROM  product_image as pi where product_id=".$id)->rows;
       return $images;
    }
}
