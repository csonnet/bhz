<?php
class ModelApiLogcenters extends Model {
    //获取物流中心默认的发货仓的基本信息
    public function getDefaultWarehouse($logcenterId) {
        $sql = "SELECT `warehouse_id`,`lat`,`lng`,`status`,`area_in_china` FROM `warehouse` WHERE `warehouse_id`=(SELECT `default_warehouse` FROM `logcenters` WHERE `logcenter_id`='".$logcenterId."')";
        $res = $this->db->query($sql);
        return $res->row;
    }
}
