<?php
class ModelApiWarehouse extends Model {
    //获取大区内总仓编号
    public function getMainWarehouseInArea($areaInChina) {
        $sql = "SELECT `warehouse_id`,`status` FROM `warehouse` WHERE `type`='1' AND `area_in_china`='".$areaInChina."'";
        $res = $this->db->query($sql);
        return $res->row;
    }

    //获取大区范围内，有特殊商品的配货仓信息，根据和默认发货仓的距离升序排序后返回
    public function getAndSortAndUniqueSpecialWarehouseInArea($defaultWHInfo) {
        //获取大区内仓库列表（有全区域配货商品，并且不等于默认发货仓）
        $ret = array();
        $sql = "SELECT `warehouse_id`,`lat`,`lng` FROM `warehouse` AS WH WHERE `warehouse_id`!='".$defaultWHInfo['warehouse_id']."' AND `type`='2' AND `status`='1' AND `area_in_china`='".$defaultWHInfo['area_in_china']."' AND EXISTS (SELECT `warehouse_id` FROM `warehouse_special_products` AS WSD WHERE WSD.`warehouse_id`=WH.`warehouse_id`)";
        $res = $this->db->query($sql);
        foreach ($res->rows as $row) {
            $ret[$row['warehouse_id']] = pow(($row['lat']-$defaultWHInfo['lat']), 2)+pow(($row['lng']-$defaultWHInfo['lng']), 2);
        }
        asort($ret, 1);
        return $ret;
    }
}
