<?php
class ModelCatalogVendor extends Model {
	public function getVendor($vendor_id) {
	 //  $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m   LEFT JOIN " . DB_PREFIX . "manufacturer_description m2d ON (m.manufacturer_id = m2d.manufacturer_id) LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m.manufacturer_id = '" . (int)$manufacturer_id . "' AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
	
		// return $query->row;
		// //return $sql;

		$where1['vendor_id']=$vendor_id;
		$vendors = M('vendors');
		$data = $vendors
		->where($where1)
		->find();

		$sql2=$this->db->query("SELECT p.image FROM vendor v LEFT JOIN product p ON v.vproduct_id=p.product_id WHERE ( p.image!='' ) AND vendor =".$vendor_id." limit 3")->rows;
		$a=array();
		$a = array_pad($a,3,'');
		$data['products_image'] = $a;
		foreach ($sql2 as $key2 => $value2) {
		if($value2){
			$data['products_image'][$key2]=$value2['image'];	
			}
													
		}
		return $data;


	}

	public function getTotalNewProductByVendorId($vendor_id){
		$cache = new Cache('file', 86400);
		$data = $cache->get('vendor.'.$vendor_id.'.newproduct');
		if(!($data)){
			$vendor = M('vendor');
			$where1['vendor'] = $vendor_id;

			$data = $vendor
			->where($where1)
			->where('date_sub(curdate(), INTERVAL 30 DAY) <= date(date_add)')
			->count();
			$cache->set('vendor.'.$vendor_id.'.newproduct',$data);
		}		
		return $data;
	}

	public function getVendorList($vendor_id) {

		$where['vendor']=$vendor_id;
		$vendor = M('vendor');

		$flag = $vendor
		->where($where)
		->find();
		if($flag){
			$data = $vendor
			// ->group('v.vproduct_id')
			// ->field('sum(op.quantity) as hot_product')
			// ->where('v.vendor ='.$vendor_id)
			->where($where)
			->limit('4')
			->select();

			$cache = new Cache('file', 86400);
			$product_total = $cache->get('vendor.'.$vendor_id.'.totalproduct');
			if(!($product_total)){
				$product_total= $vendor
				->where($where)
				->count();
				$cache->set('vendor.'.$vendor_id.'.totalproduct',$product_total);
			}

			$product_hot=$vendor
			->alias('v')
			->join('LEFT JOIN order_product op ON v.vproduct_id = op.product_id')
			->group('v.vproduct_id')
			->field('count(v.vproduct_id) as hot_product')
			// ->field('sum(op.quantity) as hot_product')
			->where('v.vendor ='.$vendor_id)
			->find();

			if($product_hot){
				$data['product_hot']=$product_hot['hot_product'];
			}
			if($product_total){
				$data['product_total']=$product_total;
			}

		}
		
		return $data;
	}

	public function getVendors($data = array(), $homepage=false) {
		if ($data) {
			// $sql = "SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_description md ON (m.manufacturer_id = md.manufacturer_id AND md.language_id = '" . (int)$this->config->get('config_language_id') . "') LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') ."'";
			$sort_data = array(
				'vendor_name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$order = $data['sort'];
			} else {
				$order = " vendor_name ";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$order .= " DESC ";
			} else {
				$order .= " ASC ";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if (!isset($data['start']) || $data['start'] < 0) {
					$data['start'] = 0;
				}

				if (!isset($data['limit']) || $data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$limit =  (int)$data['start'] . "," . (int)$data['limit'];
			}

			$vendors = M('vendors');
			$sql1 = $vendors
			->order($order)
			->limit($limit)
			->field('vendor_id,vendor_name,vendor_image_main,vendor_image,tax_id')
			->select();


			
			foreach ($sql1 as $key => $value1) {

						$data2[$key]['vendor_name']=$value1['vendor_name'];
						if($homepage) {
							$data2[$key]['vendor_image']=$value1['vendor_image_main'];	
						} else {
							$data2[$key]['vendor_image']=$value1['vendor_image'];	
						}

						$data2[$key]['tax_id']=$value1['tax_id'];
						
						$data2[$key]['vendor_id']=$value1['vendor_id'];
						$sql2=$this->db->query("SELECT p.image FROM vendor v LEFT JOIN product p ON v.vproduct_id=p.product_id WHERE ( p.image!='' ) AND vendor =".$value1['vendor_id']." limit 3")->rows;
						$a=array();
						$a = array_pad($a,3,'');
						$data2[$key]['products_image'] = $a;
						foreach ($sql2 as $key2 => $value2) {
							if($value2){
								$data2[$key]['products_image'][$key2]=$value2['image'];	
							}
													
						}							
			}
			return $data2;

		} else {
			$vendor_data = $this->cache->get('vendor.' . (int)$this->config->get('config_store_id'));

			if (!$vendor_data) {
				// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_description md ON (m.manufacturer_id = md.manufacturer_id AND md.language_id = '" . (int)$this->config->get('config_language_id') . "') LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY name");
				$vendor = M('vendors');
				$sql = $vendor
				->order("vendor_name")
				->select();

				$vendor_data = $query->rows;

				$this->cache->set('vendor.' . (int)$this->config->get('config_store_id'), $vendor_data);
			}
			
			return $vendor_data;
		}
	}
	public function getVendorByProductId($product_id){
		$vendor = M('vendor');
		$where['vproduct_id'] = $product_id;
		$query = $vendor
		->alias('v')
		->join('LEFT JOIN vendors vs ON ( v.vendor = vs.vendor_id)')
		->field('vs.vendor_name,vs.vendor_id,vs.vendor_image')
		->where($where)
		->select();

		if($query){
			return $query['0']; 
		}else{
			return false;
		}
	}
	public function checkIs_in_wishlist($vendor_id){
		$cw = M('customer_wishlist');
		$where['vendor_id'] = $vendor_id;
		$where['customer_id']  = $this->session->data['customer_id'];	
		$query = $cw
		->where($where)
		->find();
		if($query){
			return true;
		}else{
			return false;
		}
	}

	private function getProductsByVendorId($data){
		$vendor = M('vendor');
		$query = $vendor
		->alias('v')
		->join('LEFT JOIN product p ON p.product_id = v.vproduct_id')
		->join('LEFT JOIN product_description pd ON pd.product_id = p.product_id ')
		->field('p.product_id , p.image , pd.name, p.jan, p.upc')
		->where('v.vendor ='.$data['vendor_id'])
		->limit($data['limit'])
		->select();
		return $query;
	}

	public function getTopVendor($num =2,$limit = 3){
		$this->load->model('catalog/product');
		$vendors = M('vendors');
		$query = $vendors
		->order('sort_order desc')
		->limit($num)
		->field('vendor_name,vendor_id,vendor_image')
		->select();
		$i = 0;
		foreach ($query as $key => $value) {
			$data  = array(
			'vendor_id' 	=> $value['vendor_id'],
			'limit'			    => $limit,
			);

			$query[$i++]['products'] = $this->getProductsByVendorId($data); 
		}
		return $query;

	}
}