<?php

class ModelCatalogXunsearch extends Model {
	
	//添加搜索日志
	public function addSearchLog($search,$count){

		$sl = M('search_log');
		$now = date('Y-m-d h-i-s');
		
		$data['customer_id'] = $this->customer->getId();
        $data['search_text'] = trim($search);	
		$data['search_count'] = 1;
		$data['search_result'] = $count;
		$data['date_added'] = $now;

		$where['search_text'] = $data['search_text'];
		$where['customer_id'] = $data['customer_id'];

		$save['search_result'] = $count;
		$save['date_added'] = $now;

		$log = $sl->where($where)->select();

		if($log){
			$sl->where($where)->setInc('search_count',1);
			$sl->where($where)->save($save);
		}
		else{
			$sl->data($data)->add();
		}

	}

}

?>