<?php
class ModelPaymentWxyijipay extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/wx_yijipay');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('pp_standard_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('wx_yijipay_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('wx_yijipay_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		//判断是否移動端
		$this->load->helper('mobile');
		if (is_weixin()) {
			$status = true;
		}else{
			$status = false;
		}
		
		

		// $currencies = array(
		// 	'CNY',
		// );

		// if (!in_array(strtoupper($this->currency->getCode()), $currencies)) {
		// 	$status = false;
		// }
		
		

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'wx_yijipay',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('wx_yijipay_sort_order')
			);
		}

		return $method_data;
	}

	public function getOrder($orderId){
		$model = M('order');
		$field = array('order_id','store_name','payment_company','customer_id','fullname','payment_fullname','total','order_status_id','is_pay');
		$where = array('order_id'=> $orderId); 
		// 'order_status_id' => 0,is_pay => 0;
		$query = $model->field($field)->where($where)->find();
		return $query;
	}
}