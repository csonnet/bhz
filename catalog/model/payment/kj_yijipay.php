<?php
class ModelPaymentKjYijipay extends Model {
  public function getMethod($address, $total) {
    $this->load->language('payment/kj_yijipay');

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('kj_yijipay_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

    if ($this->config->get('kj_yijipay_total') > 0 && $this->config->get('kj_yijipay_total') > $total) {
      $status = false;
    } elseif (!$this->config->get('kj_yijipay_geo_zone_id')) {
      $status = true;
    } elseif ($query->num_rows) {
      $status = true;
    } else {
      $status = false;
    }

    $method_data = array();

// $this->load->helper('mobile');
// 		if (is_mobile()) {
// 			$status = false;
// 		}else{
// 			$status = true;
// 		}

    if ($status) {
      $method_data = array(
        'code'       => 'kj_yijipay',
        'title'      => $this->language->get('text_title'),
        'terms'      => '',
        'sort_order' => $this->config->get('kj_yijipay_sort_order')
      );
    }

    return $method_data;
  }

  public function getUserId($user_id){
    $model = M('customer');
    $where = array('customer_id' => $user_id);
    $field = array('customer_id','yiji_userid','fullname');
    $query = $model->field($field)->where($where)->find();
    return $query;
  }
}