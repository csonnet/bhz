<?php
/**
 * ppmNewRuleRegisterUser	注册易极付会员	接口
 * Date: 2016/11/24 14:34
 */
    class ModelPaymentYijiRegister extends Model{

        public function getUserId($telephone){
                //echo $telephone;exit;

                $this->load->helper('yijiSignHelper');
                $this->load->helper('dto/RegisterUser.class');
                $this->load->helper('YijipayClient');

                $objReq = new RegisterUser();

                //请求公共部分
                $objReq->setMerchOrderNo("T20176232323623" .$telephone);
                $objReq->setOrderNo("RIDT201761322134234".$telephone);
                $objReq->setSignType("MD5");
                //必填参数
                $objReq->setUserName("BHZ_".$telephone);
                $objReq->setRegisterUserType("PERSONAL");
                $objReq->setMobile($telephone);
                //构建请求
                $config = $this->getConfig();
                $client = new YijiPayClient($config);
                
                $response = $client->execute($objReq);
                $res = json_decode($response,true);
                
                //print_r($response);exit;

                if(isset($res['userId']) && !empty($res['userId'])){
                    $arr = $res['userId'];
                }else{
                    $arr = 0;
                }

                //print_r($arr);exit;

                return $arr;
        }

        public function getConfig(){
        return array(
        'partnerId' => '20161201020011981315', //商户ID
        'md5Key' => 'db3d0a81a79b6832c439bbf5ec92b77c', //商户Key
        
        //网关
        'gatewayUrl' => "https://api.yiji.com/gateway.html" //生产环境
        // 'gatewayUrl' => "https://openapi.yijifu.net/gateway.html"	//测试环境
        );
    }

}
