<?php

class ModelPaymentYijicode extends Model{

    public function getOrder($orderID){
            $model=M('order');
            $field=array('order_id','total','order_status_id','payment_method','is_pay');
            $where=array('order_id'=>$orderID);
            $res=$model->field($field)->where($where)->find();
            return $res;  
    }

    public function getProductByorder($orderID){

        $product_id=$this->getProductIdbyorderId($orderID);
        $op = M('product_to_category');
		// $where['product_id'] = $product_id;
        $where['product_id'] = array(65);
		$query = $op
		->alias('op')
		->join('LEFT JOIN category_description c ON c.category_id = op.category_id')
		->where($where)
		->field('op.category_id,c.name')
		->select();
		return $query;
    }

    public function getProductIdbyorderId($orderID){
         $op = M('order_product');
            $where['order_id'] = $orderID;
            $query = $op
            ->where($where)
            ->field('product_id')
            ->find();
        return $query;
    }

    public function changeorderispay($orderID){
            $model=M('order');
            $data=array('is_pay'=>1);
            $where=array('order_id'=>$orderID,'is_pay'=>0);
            $res=$model->data($data)->where($where)->save();
    }

    public function updatemoney($res,$order_id){
        $model=M('money_in');
        $data = array(
            'received'=>$res,
            'status'=>2,
            'pay_time'=>date('Y-m-d H:i:s')
            );
            $money_in_id =  $model->data($data)->where(array('refer_id'=>$order_id,'refer_type_id'=>1))->save();
    }

    public function getOrderById($orderID){
        $model=M('order');
        $where=array('order_id'=>$orderID,'is_pay'=>1);
        $res=$model->where($where)->getField('`order`.total');
        return $res;  
    }
}
            
?>