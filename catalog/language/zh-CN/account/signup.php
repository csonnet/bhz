<?php
// Heading 
$_['heading_title']        = '品牌厂商注册';
$_['heading_title_signup'] = '您的账户已经被注册！';

// Text
$_['text_account']         = '商家账户';
$_['text_register']        = '商家注册';
$_['text_signup_plan']     = '注册带有佣金费率 - ';
$_['text_account_already'] = '如果您已经有了账户，请直接登录 <a href="%s">登录页面</a>.';
$_['text_your_details']    = '个人明细';
$_['text_your_address']    = '您的地址';
$_['text_your_password']   = '您的密码';
$_['text_agree']           = '我已阅读并同意 <a href="%s" class="agree"><b>%s</b></a>';
$_['text_approval_required'] = '<p>感谢你注册 %s!</p><p>您将收到我们发生的邮件信息。</p><p>如果您有任何疑问, 请联系 <a href="%s">店主</a>.</p>';
$_['text_approval'] 	   = '<p>感谢您注册 %s!</p><p> 您的帐户已经创建，您可以登录使用您的用户名和密码，通过以下 <a href="%s">登陆页面</a></p>';
$_['text_success']  	   = '成功';
$_['text_close_sign_up']   = '我们暂时关闭卖家注册。请稍后再回来。敬请关注！';
$_['text_wait']  	   	   = '正在处理，请稍等 ...';
$_['text_bank_info']  	   = '银行信息';
$_['text_complete_payment']= '一旦完成付款。请以电邮通知我们，注册的用户名和银行的交易号码。';

$_['button_sign_up']  	   = '立即注册';

// Entry
$_['entry_username']       = '用户名';
$_['entry_firstname']      = '名字';
$_['entry_lastname']       = '姓氏';
$_['entry_email']          = 'E-Mail';
$_['entry_bank_name']      = '银行名称';
$_['entry_iban']           = '银行账号';
$_['entry_swift_bic']      = 'SWIFT/BIC';
$_['entry_bank_address']   = '开户行全称';
$_['entry_company_id']     = '公司编码';
$_['entry_tax_id']         = '税编码';
$_['entry_plan']           = '选择佣金类型';
$_['text_products']    	   = '商品';
$_['text_paypal']    	   = 'PayPal';
$_['text_bank']    	   	   = '银行';

$_['entry_paypal']         = '支付宝账号';
$_['entry_payment_method'] = '付款方式';
$_['entry_telephone']      = '电话';
$_['entry_fax']            = '传真';
$_['entry_company']        = '公司名称';
$_['entry_address_1']      = '地址 1';
$_['entry_address_2']      = '地址 2';
$_['entry_postcode']       = '邮编';
$_['entry_city']           = '城市';
$_['entry_country']        = '国家';
$_['entry_zone']           = '省/区';
$_['entry_product_limit']  = '商品限制';
$_['entry_store_url']      = '公司网站';
$_['entry_store_description']  	= '告诉我们更多关于你公司的介绍';
$_['entry_password']       		= '输入密码';
$_['entry_confirm']        		= '密码确认';

$_['help_paypal']          = '确保你使用你的支付宝电子邮件地址，否则你不会收到付款';
$_['help_company']         = '这是商店前台卖家名称';
$_['help_product_limit']   = '商品的数量可以增加到帐户';

// Error
$_['error_exists']         = '警告：E-Mail 地址已经被注册！';
$_['error_username_exists']= '警告：用户名已经被注册！';
$_['error_firstname']      = '名字必须在 1 至 32 个字符之间！';
$_['error_lastname']       = '姓氏必须在 1 至 32 个字符之间！';
$_['error_email']          = 'E-Mail 地址不显示是有效的！';
$_['error_paypal']         = '支付宝地址似乎没有有效！';
$_['error_telephone']      = '电话必须在 3 至 32 个字符之间！';
$_['error_password']       = '密码必须介于 4 至 20 个字符之间！';
$_['error_confirm']        = '密码确认不匹配密码！';
$_['error_address_1']      = '地址1必须介于 1 和 128个字符之间！';
$_['error_city']           = '城市必须介于2和128个字符！';
$_['error_company']        = '公司必须在2至128个字符之间！';
$_['error_postcode']       = '邮政编码必须是2和10之间的字符！';
$_['error_country']        = '请选择一个国家！';
$_['error_zone']           = '请选择区域！';
$_['error_agree']          = '警告：您必须同意 %s!';
?>