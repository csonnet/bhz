<?php
// Text
$_['text_success']           = '您的订单已经成功修改';
$_['date_format_short']      = 'd/m/Y';

// Error
$_['error_permission']       = '警告：你还没有权限访问API！';
$_['error_customer']         = '客户的细节需要设置！';
$_['error_payment_address']  = '付款地址！';
$_['error_payment_method']   = '付款方式要求！';
$_['error_shipping_address'] = '需装运地址！';
$_['error_shipping_method']  = '装运方法要求！';
$_['error_stock']            = '标有不可用***数量或不在库存的商品！';
$_['error_minimum']          = '最小订单数 %s 是 %s!';
$_['error_not_found']        = '警告：找不到订单！';