<?php
// Text
$_['text_subject']  = '%s - 感谢你的注册';
$_['text_renew_subject']  = '帐户更新成功';
$_['text_welcome']  = '欢迎并感谢您注册 %s!';
$_['text_renew_message']  = '这是通知你，该帐户已成功更新。 <br/>请登录您的帐户，以了解更多信息。';
$_['text_login']    = '您的帐户已经创建，您可以登录使用您的用户名和密码通过以下网址：';
$_['text_approval'] = '您的帐户必须在登录前批准。一旦获得批准，您可以登录使用您的用户名和密码通过以下网址：';
$_['text_services'] = '登录后，您可以管理帐户。';
$_['text_thanks']   = '谢谢,';
$_['text_signup_information']   = '<u><b>账户信息</b></u>';
$_['text_signup_username']   	= '<b>用户 : </b>';
$_['text_signup_plan']   		= '<b>注册等级 : </b>';
$_['text_signup_date']   		= '<b>注册日期 : </b>';
$_['text_signup_expire_date']   = '<b>失效日期 : </b>';

$_['text_subject1'] = '新成员 %s 已经加入品牌厂商';
$_['text_to'] 		= '您好 %s,';
$_['text_join']     = '通知您 %s 从 %s 已注册加入品牌厂商计划';
$_['text_system'] 	= '<span class="help">这是系统生成的消息。请不要回答。</span>';
?>