<?php
// Text
$_['text_title']				= '支票 / 钱 订单';
$_['text_instruction']			= '支票 / 钱 订单 说明';
$_['text_payable']				= '应付: ';
$_['text_address']				= '发送: ';
$_['text_payment']				= '你的命令将不装运，直到我们收到付款。';