<?php
// Text
$_['text_title']				= '银行转账';
$_['text_instruction']			= '银行转账介绍';
$_['text_description']			= '请将总金额转移至以下银行帐户。';
$_['text_payment']				= '你的命令将不装运，直到我们收到付款。';
$_['text_bank_name']    		= '银行名称: ';
$_['text_iban']     			= '银行账号：';
$_['text_swift_bic']   			= 'SWIFT/BIC : ';