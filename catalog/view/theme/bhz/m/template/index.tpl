<!DOCTYPE html>
<html lang="zh">
<!--<![endif]-->
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
  <meta name="description" content="">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="format-detection" content="telephone=no" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <title>百货栈</title>

  <!-- <base href="/bhz/" /> -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="" rel="icon" />
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/swiper.min.css">  
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/m/css/icomoon.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/ngDialog.min.css"> 
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/ngDialog-theme-default.min.css"> 
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/m/css/main.css?v=20170562">
  <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/swiper.min.js"></script>
  <script src="catalog/view/theme/bhz/m/libs/plugins.js" type="text/javascript"></script> 
  <script src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js"> </script>
  <script src="catalog/view/theme/bhz/vendor/js/angular.min.js"> </script>
  <script src="catalog/view/theme/bhz/vendor/js/angular-route.min.js"> </script>
  <script src="catalog/view/theme/bhz/vendor/js/echarts/common.min.js"> </script>
  <script src="catalog/view/theme/bhz/vendor/js/echarts/theme/shine.js"> </script>

</head>
  <body class="bhz-mobile">
    <a name="top"></a>
    <div ng-include="'catalog/view/theme/bhz/m/modules/includes/header.html'" ng-if="!withoutHeader"></div>

    <div ng-cloak class="content-wrapper">
      <div ng-view></div>
    </div>

    <div ng-include="'catalog/view/theme/bhz/m/modules/includes/footer.html'" ng-if="!withoutFooter"></div>

    <script src="catalog/view/theme/bhz/vendor/js/require.js"  type="text/javascript"></script>
    <script type="text/javascript">
      require(['catalog/view/theme/bhz/m/rconfig'], function(){
        require.config({
          urlArgs: "v=20171113"
        });
        require(['angularAMD', 'modules/app', 'modules/includes'], function(angularAMD, BHZApp){
          angularAMD.bootstrap(BHZApp);
        });
      })
    </script>

	<script>
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "https://hm.baidu.com/hm.js?698db73ff1622b8f03cbda78933392c5";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
	</script>

</body>
</html>
