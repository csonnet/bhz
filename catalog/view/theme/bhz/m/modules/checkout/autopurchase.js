'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

	angularAMD.controller('AutoPurchaseCtrl', ['$scope', '$rootScope', 'ngDialog', '$http', '$cookies', '$location',
	function($scope, $rootScope, ngDialog, $http, $cookies, $location) {
		
		$scope.productDisplay = {
			"sort": 'orderedNums'
		}

		$scope.changeSort = function(newSort) {
			$scope.productDisplay.sort = newSort;
			$scope.products = '';
			$scope.productAi = 0;
			loadProducts();
		}
		
		var page = 1;
		var limit = 20;
		var hasMore = true;
		var productLoading = false;
		$scope.products = '';
		loadProducts();
		
		/*数量变动*/
		$scope.changeQuantity = function(product, action) {

			var inputQ = $("input[name=buyCount"+product.id+"]").val();
			var result;

			var quantityParams = {};
			if(action != null){

				if(inputQ == product.minimum && action == -1){
					BHZ.MessageBox.show("不能再减少了");
					return;
				}

				if(action == 1){
					result = parseInt(inputQ) + parseInt(product.minimum);
					if(result % product.minimum != 0){
						result = result - result % product.minimum;
					}
				}

				if(action == -1){
					result = parseInt(inputQ) - parseInt(product.minimum);
					if(result % product.minimum != 0){
						result = result - result % product.minimum;
					}		
				}

				$("input[name=buyCount"+product.id+"]").val(result);

			}

		}
		/*数量变动*/
		
		/*智能补货*/
		$scope.aiCreate = function() {
			
			$scope.productAi = 1;
			productLoading = true;
			$http.get("index.php?route=rest/auto_purchase/aiPurchase"+"&page="+page+"&limit="+limit).then(
			function(result) {

				if(result.data.success == true){
					$scope.products = result.data.products;
				}

			});

		}
		/*智能补货*/

		/*批量添加进货单*/
		$scope.toPay = function(method){

			var gproduct = [];
			$(".check-box").each(function(){	

				if(this.checked != false){
					
					if($(this).parent().find("input[name='is_single']").val()==1){

						var id = $(this).parent().find("input[name='p_id']").val();
						var gps = $scope.products;

						for(var i = 0;i<gps.length;i++){
							if(id == gps[i]['id']){
								gps[i]['buyCount'] = $("input[name='buyCount"+id+"']").val();
								gproduct.push(gps[i]);
							}						
						}

					}

				}

			});

			if(gproduct.length==0){
				BHZ.MessageBox.show("请至少选择一件可单卖商品");
			}
			else{
			
				for(var i=0;i<gproduct.length;i++){
				
					$scope.addSingleCart(method,gproduct[i]);

				};

				if(method == 2){
					$location.url("/cart");
				}

			}

		}
		/*批量添加进货单*/

		/*单独添加进货单*/
		$scope.addSingleCart = function(type,product){

			var option = {};
			if(product.options && product.options.length>0){
			  option[product.options[0].product_option_id] = product.options[0].option_value[0].product_option_value_id;
			}
			
			var params = {
				"product_id":product.id,
				"show_cart":product.show_cart,
				"quantity":product.buyCount,
				"option":option
			}

			$http.post("index.php?route=rest/cart/cart",params).then(
				function(result) {
					if(result.data.success){
						if(type == 1){
							BHZ.MessageBox.show("加入进货单成功！");
						}
					}
					else{
						BHZ.MessageBox.show("加入进货单失败");
					}
				}
			)

		}
		/*单独添加进货单*/

		function loadProducts() {
			
			var sort = $scope.productDisplay.sort;
			productLoading = true;
			$http.get("index.php?route=rest/auto_purchase/"+sort+"&page="+page+"&limit="+limit).then(
				
				function(result) {

					if(result.data.success == true){

						if (1 == page){
							$scope.products = result.data.products;
						}else{
							angular.forEach(result.data.products, function(product){
							$scope.products.push(product);
						})
					}

					if (result.data.products.length < limit) {
						$('#LoadingPlace').hide();
						hasMore = false;
					}else{
						$('#LoadingPlace').show();
						hasMore = true;
					}

					productLoading = false;

				}

			});
		}

    }]);

});
