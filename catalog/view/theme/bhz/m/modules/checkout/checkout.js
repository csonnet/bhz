'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('CheckoutCtrl', ['$scope', 'ngDialog', '$http', '$cookies', '$location',
    function($scope, ngDialog, $http, $cookies, $location) {

      $scope.isForFree = false;
      $scope.showAddressList = false;

	  /*余额信息*/
		function getBalance(){
		
			$http.get('index.php?route=balance/balance/getBalance').then(function(result) {
			
				if(result.data && result.data.success){
					$scope.balance = result.data.customerBalance;
				}

			})

		}
		/*余额信息*/

      function refreshCheckoutData() {

        $http.get('index.php?route=rest/simple_checkout/checkout').then(function(result) {

          if (result.data && result.data.success) {
            $scope.checkoutData = result.data
            $scope.cart = $scope.checkoutData.cart
            var totals = $scope.cart.totals
            $scope.subTotals = totals.slice(0, totals.length - 1)
            $scope.total = totals[totals.length - 1]

            $scope.payment_info = $scope.checkoutData.payment_info
            $scope.selected_payment = $scope.payment_info.payment_methods['kj_yijipay'];

            $scope.ship_address = $scope.checkoutData.ship_address
            $scope.ship_addresses = $scope.ship_address.addresses
           // console.log($scope.ship_addresses)

            $scope.selected_address = $scope.ship_addresses[$scope.checkoutData.ship_address.selected_address]

            $scope.delivery_info = $scope.checkoutData.delivery_info
            $scope.rewards = $scope.checkoutData.rewards

			//是否免息账期
			$scope.is_magfin = $scope.checkoutData.is_magfin
			$scope.log_name = $scope.checkoutData.log_name
			$scope.log_user = $scope.checkoutData.log_user
			$scope.log_telephone = $scope.checkoutData.log_telephone
          }
        })
      }

      function favourchange(){
        var submitData = {};
        submitData.payment_code = $scope.selected_payment.code;
        
        $http.post('index.php?route=rest/simple_checkout/getCheckout',submitData).then(function(result) {
          if (result.data && result.data.success) {
            $scope.cart.totals = result.data.cart.totals
            var totals = $scope.cart.totals
            $scope.subTotals = totals.slice(0, totals.length - 1)
            $scope.total = totals[totals.length - 1]
          }
        })
      }

      $scope.changeAddress = function() {
        $scope.showAddressList = true;
      }

      $scope.selectAddress = function(address) {
        $scope.selected_address = address;
        $scope.showAddressList = false;
      }

      $scope.selectPayment = function(payment) {

		$("#balance-layer").hide();

		if(payment['code'] == "magfin"){
			if($scope.is_magfin == "1"){
				BHZ.MessageDialog.show("您暂时不能使用该支付方式，请联系您所在<font color='blue'>"+$scope.log_name+"</font><br/>业务经理：<font color='blue'>"+$scope.log_user+"</font>，<br/>电话：<font color='blue'>"+$scope.log_telephone+"</font>");
			}
			else{
				$scope.selected_payment = payment;
			}
		}
		else if(payment['code'] == 'balance'){
			$("#balance-layer").show();
			$scope.selected_payment = payment;
		}
		else{
			$scope.selected_payment = payment;
		}

    favourchange();
      }

      $scope.submit = function() {
        var submitData = {}

		if($scope.selected_payment.code == 'balance' && $scope.balance-$scope.total.price < 0){
          BHZ.MessageDialog.show("余额不足请去充值，或使用其他支付方式");
          return;
        }

        submitData.address_id = $scope.selected_address.address_id
        submitData.payment_code = $scope.selected_payment.code;
        submitData.payment_method = $scope.selected_payment.title;

        $http.post('index.php?route=rest/simple_checkout/checkout', submitData)
          .then(function(result) {
            if (result.data && result.data.success) {
              if(result.data.data.is_cod || result.data.data.pay_code == 'magfin' || result.data.data.pay_code == 'balance'){
                $location.path('/orders');
              }else if(result.data.data.redirect) {
                window.location.href = result.data.data.redirect;           
              }
			  else if(result.data.data.redirect) {
                window.location.href = result.data.data.redirect;           
              } else {
                $location.path('/payment/card');
              }
            } else {
              $('body').html(result.data);
            }
          })
      }

      $scope.showAllPayment = function () {
        $('.select-box').removeClass('hidden');
        $('.other-payment-method').addClass('hidden');
      }

      refreshCheckoutData();
	  getBalance();

    }
  ]);



});
