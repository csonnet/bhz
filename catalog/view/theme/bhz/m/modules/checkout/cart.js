'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('CartCtrl', ['$scope', '$rootScope', 'ngDialog', '$http', '$cookies', '$location',
    function($scope, $rootScope, ngDialog, $http, $cookies, $location) {

      $scope.sampleCheckout = false;
      var myuser_id = $cookies.get('myuser_id');
      // console.log(myuser_id+'111');

      $scope.myuserId = myuser_id;

      function refreshCartData() {

        return $http.get('index.php?route=rest/cart/cart').then(function(result) {

          if (result.data && result.data.success) {
            $scope.cartData = result.data.data
            if($scope.cartData.error_warning){
              BHZ.MessageDialog.show($scope.cartData.error_warning);
            }
            $scope.products = $scope.cartData.products
            if(!$scope.products || $scope.products.length == 0){
              $rootScope.withoutFooter = false;
            }else{
              $rootScope.withoutFooter = true;
            }
            var totals = $scope.cartData.totals;
            if (totals) {
              $scope.subTotals = totals.slice(0, totals.length - 1)
              $scope.total = totals[totals.length - 1]
            }
            $scope.coupon = $scope.cartData.avaiable_coupon;
            $scope.couponCode = $scope.cartData.coupon;

            if($scope.cartData.is_sample_checkout) {
              $scope.sampleCheckout = true;
            }

            if($scope.cartData.error_warning){
              return false
            }

            return true
          }
        })
      }

      $scope.changeQuantity = function(product, action) {
        if($scope.sampleCheckout){
          return
        }
        var quantityParams = {};
        if(action != null){
          if(product.quantity == product.minimum && action == -1){
            BHZ.MessageBox.show("不能再减少了");
            return;
          }
          if(action == 1){
            product.quantity = parseInt(product.quantity) + parseInt(product.minimum);
	    if(product.quantity % product.minimum != 0){
		product.quantity = product.quantity - product.quantity % product.minimum;
	    }
          }else if(product.quantity < product.minimum*2 && action == -1){
            product.quantity = product.minimum;
          }else{
            if(product.quantity % product.minimum != 0){
		product.quantity = product.quantity - product.quantity % product.minimum;
	    }else{
		product.quantity = parseInt(product.quantity) - parseInt(product.minimum);
	    }
          }
        }
        quantityParams[product.key] = product.quantity;
        if(product.quantity && product.quantity > 0){
          $scope.updateCart(quantityParams);
        }
      }

      $scope.updateCart = function(quantityParams){
       $http.put('index.php?route=rest/cart/cart', {
         quantity: quantityParams
       }).then(function(result) {
         if (result.data && result.data.success) {
           refreshCartData();
         }
       }) 
      }

      $scope.deleteProduct = function(product) {
        ngDialog.openConfirm({
          template: 'confirmDeleteDialog.html',
          className: 'ngdialog-theme-default'
        }).then(function(value) {
          $http.delete('index.php?route=rest/cart/cart&product_id=', {
            data: {
              product_id: product.key
            }
          }).then(function(result) {
            if (result.data && result.data.success) {
              refreshCartData();
            }
          })
        }, function(value) {
          console.log('cancel')
        });
      }

      $scope.setSampleCheckout = function() {

        if (!$scope.sampleCheckout) {
          $http.post('index.php?route=rest/simple_checkout/setSampleCheckoutFlag', {
              "sample_checkout": "1"
            })
            .then(function(result) {
              if (result.data.success) {
                $scope.sampleCheckout = true;
              }
              refreshCartData();
            })
        } else {
          $http.post('index.php?route=rest/simple_checkout/setSampleCheckoutFlag', {
              "sample_checkout": 0
            })
            .then(function(result) {
              if (result.data.success) {
                $scope.sampleCheckout = false;
              }
              refreshCartData();
            })
        }
      }

      $scope.toggleCoupon = function(coupon){

        if($scope.couponCode && coupon.code == $scope.couponCode){
          $http.delete('index.php?route=rest/cart/coupon',{
            coupon: $scope.coupon.code
          }).then(function(result){
            if(result.data && result.data.success){
              refreshCartData();
            }
          })
        }else{
          $http.post('index.php?route=rest/cart/coupon',{
            coupon: $scope.coupon.code
          }).then(function(result){
            if(result.data && result.data.success){
              refreshCartData();
            }
          })
        }

      }

      $scope.gotoCheckout = function() {

        refreshCartData().then(function(result){
          if(result){
            $location.path("/checkout");
          }
        })
      }

      $scope.loanorder = function() {

        refreshCartData().then(function(result){
          if(result){
            $location.path("/loanorder");
          }
        })
      }

      $scope.emptyCart = function() {
        $http.delete('index.php?route=rest/cart/emptycart',{
          
          }).then(function(result){
            if(result.data && result.data.success){
              refreshCartData();
            }
          })
      }
      /* @author sonicsjh */
      $scope.initCartByHistory = function(){
        $http.get('index.php?route=rest/cart/initCartByHistory').then(function(result){
          if (result.data && result.data.success){
            refreshCartData();
          }else{
            alert(result.data.error.warning);
          }
        })
      }

      refreshCartData();
    }
  ]);
});
