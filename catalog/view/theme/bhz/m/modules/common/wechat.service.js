'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.service('WechatService', ['$http', '$q', '$cookies', '$location', 
    function($http, $q, $cookies, $location ) {

      console.log(wx)
      this.getJSSDKReady = function(){
        return $http.get('index.php?route=feed/rest_api/getSignPackage'+'&url='+encodeURIComponent($location.absUrl())).then(function(res){
          wx.error(function(res){
            alert(JSON.stringify(res));
          });
          var data = res.data;
          wx.config({
            debug: false,
            appId: data.appId,
            timestamp: parseInt(data.timestamp),
            nonceStr: data.nonceStr,
            signature: data.signature,
            jsApiList: ['chooseImage',
                        'uploadImage']
          });
        });
      }

      // $scope.changeLiscenceImage = function(){
        // wx.chooseImage({
        //   count: 1,
        //   success: function (res) {
        //    var localIds = res.localIds;
        //    if(!localIds || !localIds[0]){
        //      return;
        //    }
        //    wx.uploadImage({
        //      localId: localIds[0], 
        //      isShowProgressTips: 1,
        //      success: function (res) {
        //        var serverId = res.serverId;
        //        $http({
        //          method: 'post',
        //          url: 'index.php?route=rest/account/saveLicenseImage',
        //          data: {'media_id': serverId},
        //        }).success(function (data, status, headers, config) {
        //         $scope.profile.license_image = data.img_url;
        //        }).error(function(data){
        //         //...
        //       });
        //      }
        //    });
        //   }
        // });
      // };

    }
  ]);



});
