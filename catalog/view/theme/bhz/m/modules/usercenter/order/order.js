'use strict';

define([
  'angular',
  'angularAMD'
], function(ng, angularAMD) {

  angularAMD.controller('orderListCtrl', ['$scope', '$http', '$location', 'ngDialog',
    function($scope, $http, $location, ngDialog) {
      var searchObj = $location.search();
      $scope.show_all_payment = false;

      $scope.selection={
        "tab": searchObj.status ? searchObj.status : '全部'
      }

      $scope.switchTab = function(status){
        $scope.selection.tab = status;
      }

      updateOrders()

      function updateOrders(){
        $http.get("index.php?route=rest/order/orders").then(
            function(result) {
              if (result.data.success) {
                $scope.orderList = result.data.data.orders;
                /*if($scope.orderList){
                  var orderArr = [];
                  for(var key in $scope.orderList){
                    angular.forEach($scope.orderList[key],function(orderItem){
                      orderArr.push(orderItem);
                    })
                  }
                  $scope.orderList['全部'] = sortOrderList(orderArr);
                }*/
              }
            }
        )
      }

      $scope.gotoOrder = function(order_id){
        $location.url("/order/"+order_id);
      }

      $scope.showAllPayments = function(){
        $scope.show_all_payment = true;
      }

      $scope.gotoLuckydraw = function(order){
        if(order.luckydraw.left_times>0) {
          var order_id = order.order_id;
          var luckydraw_id = order.luckydraw.luckydraw_id
          $location.url("/order/"+order_id+"/luckydraw/"+luckydraw_id);  
        }
      }

      $scope.goToFastpay = function(order_id,payment_code){
        var submitData = {};
        submitData.order_id = order_id;
        submitData.payment_code = payment_code;
        $http.post('index.php?route=rest/simple_checkout/fastpay', submitData)
          .then(function(result) {
            if (result.data && result.data.success) {
              if(result.data.data.is_cod){
                $location.path('/rest/cart');
              }else{
                $location.path('/payment/card');                
              }
            } else {
              if('kj_yijipay' == result.data.payment||'wx_yijipay' == result.data.payment){
                window.location.href = result.data.redirect;
              }else{
                $('body').html(result);
              }  
            }
          })
      }

      $scope.confirmReceived = function(order_id){
        ngDialog.openConfirm({
          template: 'confirmReceivedDialog.html',
          className: 'ngdialog-theme-default'
        }).then(function(value) {
          $http.get('index.php?route=feed/rest_api/confirmShipment&order_id='+order_id)
          .then(function(result) {
             updateOrders()
          })
        }, function(value) {
          console.log('cancel')
        });
      }
      /* @author sonicsjh */
      $scope.copyOrder = function(order_id){
        $http.get('index.php?route=rest/cart/copyOrder&order_id='+order_id).then(function(result){
          if (result.data && result.data.success){
            $location.path("/cart");
          }else{
            alert(result.data.error.warning);
          }
        })
      }
      
      function sortOrderList(array){
        var len=array.length,i,j,tmp; 
        for(var i=0;i<len-1;i++){  
          for(var j=i+1;j<len;j++){  
            if(parseInt(array[i].order_id)<parseInt(array[j].order_id)){  
              var temp=array[i];  
              array[i]=array[j];  
              array[j]=temp;  
            }  
          }  
        }
        return array;  
      }
    }
  ]);

  angularAMD.controller('orderCtrl', ['$scope', '$routeParams', '$http', '$location', 'ngDialog',
    function($scope, $routeParams, $http, $location, ngDialog) {
      var orderId = $routeParams.order_id;
      if(!orderId){
        alert("缺少订单id..");return;
      }
      
      function getOrderInfo(){
        $http.get("index.php?route=rest/order/orders&id="+orderId).then(
            function(result) {
              if (result.data.success) {
                $scope.orderInfo = result.data.data;
              }
            }
        )
      }

	  $scope.goToFastpay = function(order_id,payment_code){
        var submitData = {};
        submitData.order_id = order_id;
        submitData.payment_code = payment_code;
        $http.post('index.php?route=rest/simple_checkout/fastpay', submitData)
          .then(function(result) {
            if (result.data && result.data.success) {
              if(result.data.data.is_cod){
                $location.path('/rest/cart');
              }else{
                $location.path('/payment/card');                
              }
            } else {
              if('kj_yijipay' == result.data.payment||'wx_yijipay' == result.data.payment){
                window.location.href = result.data.redirect;
              }else{
                $('body').html(result);
              }  
            }
          })
      }

      $scope.printBill = function(){
        if($scope.orderInfo.bill_status == 0){
          ngDialog.openConfirm({
            template: 'confirmPrintDialog.html',
            className: 'ngdialog-theme-default'
          }).then(function(value) {
            $http.post('index.php?route=rest/account/saveIs_invoice&order_id='+$scope.orderInfo.order_id, {
              is_invoice_id: 1
            }).then(function(result) {
              if (result.data && result.data.success) {
                getOrderInfo();
              }
            })
          }, function(value) {
            console.log('cancel')
          });
        }
      }

      $scope.gotoReview = function(order_id,order_product_id){
        $location.url("order/"+order_id+"/review/"+order_product_id);
      }

      getOrderInfo();

    }
  ]);

  angularAMD.controller('reviewCtrl', ['$scope', '$routeParams', '$http', '$location',
    function($scope, $routeParams, $http, $location) {
      var orderId = $routeParams.order_id;
      var orderProductId = $routeParams.order_product_id;
      if(!orderId){
        alert("缺少订单id..");return;
      }

      $scope.review = {
        rating:5,
        is_anony:false
      };
      
      $http.get("index.php?route=rest/order/orders&id="+orderId).then(
          function(result) {
            if (result.data.success) {
              $scope.orderInfo = result.data.data;
              if($scope.orderInfo.products){
                for(var i=0;i<$scope.orderInfo.products.length;i++){
                  if($scope.orderInfo.products[i].order_product_id == orderProductId){
                    $scope.product = $scope.orderInfo.products[i];
                    break;
                  }
                }
              }
            }
          }
      )

      $scope.gotoReview = function(){
        if(!$scope.review.text){
          BHZ.MessageDialog.show("请输入评价内容","知道了");
          return;
        }
        if($scope.review.text.length<0 || $scope.review.text.length>999){
          BHZ.MessageDialog.show("评价内容长度为1~1000个字符","知道了");
          return;
        }
        if(!$scope.review.rating){
          BHZ.MessageDialog.show("请选择评分","知道了");
          return;
        }
        var params = {
          text:$scope.review.text,
          rating:$scope.review.rating,
          is_anony:$scope.review.is_anony ? 1 : 0,
          order_product_id:$scope.product.order_product_id
        }
        $http.post("index.php?route=feed/rest_api/reviews&id="+$scope.product.product_id,params).then(
          function(result) {
            if (result.data.success) {
              BHZ.MessageBox.show("评价成功,即将自动跳转到订单详情页");
              setTimeout(function(){
                $location.path("/order/"+$scope.orderInfo.order_id);
                $scope.$apply();
              },1000)
            }
          }
        )
      }
    }
  ]);

  angularAMD.controller('orderLuckydrawCtrl', ['$scope', '$routeParams', '$http', '$location',
    function($scope, $routeParams, $http, $location) {
      var orderId = $routeParams.order_id;
      var luckydrawId = $routeParams.luckydraw_id;
      var turnplate={
          restaraunts:[],       //大转盘奖品名称
          colors:[],          //大转盘奖品区块对应背景颜色
          outsideRadius:192,      //大转盘外圆的半径
          textRadius:155,       //大转盘奖品位置距离圆心的距离
          insideRadius:68,      //大转盘内圆的半径
          startAngle:0,       //开始角度
          
          bRotate:false       //false:停止;ture:旋转
      };
      $scope.hit_price = '';

      $http.get("index.php?route=rest/order/getLuckydraw&id="+orderId).then(
          function(result) {
            if (result.data.success) {
              $scope.luckydraw = result.data.luckydraw;
              turnplate.restaraunts = ['谢谢参与'];
              turnplate.colors = ['#FFF4D6'];
              var idx = false;
              var color = '#FFF4D6';
              angular.forEach($scope.luckydraw.products, function(product){
                turnplate.restaraunts.push(product.price_name+' '+product.product_name);
                color = idx?"#FFF4D6":"#FFFFFF";
                idx = !idx;
                turnplate.colors.push(color);
              });
              $scope.drawRouletteWheel();
            }
          }
      )

      $scope.rotateTimeOut = function (){
        $('#wheelcanvas').rotate({
          angle:0,
          animateTo:2160,
          duration:8000,
          callback:function (){
            alert('网络超时，请检查您的网络设置！');
          }
        });
      };

      //旋转转盘 item:奖品位置; txt：提示语;
      $scope.rotateFn = function (item, left_times, txt){
        var angles = item * (360 / turnplate.restaraunts.length) - (360 / (turnplate.restaraunts.length*2));
        if(angles<270){
          angles = 270 - angles; 
        }else{
          angles = 360 - angles + 270;
        }
        $('#wheelcanvas').stopRotate();
        $('#wheelcanvas').rotate({
          angle:0,
          animateTo:angles+1800,
          duration:8000,
          callback:function (){
            if(item==1) {
              alert(txt);
            } else {
              $scope.hit_price = txt;
              $scope.$apply();
              alert('恭喜您抽中了' + txt + '！');
            }
            turnplate.bRotate = !turnplate.bRotate;
            if(left_times<=0) {
              $location.url("/orders");
              $scope.$apply();
            }
          }
        });
      };

      $scope.drawRouletteWheel = function () {   
        var canvas = document.getElementById("wheelcanvas");    
        if (canvas.getContext) {
          //根据奖品个数计算圆周角度
          var arc = Math.PI / (turnplate.restaraunts.length/2);
          var ctx = canvas.getContext("2d");
          //在给定矩形内清空一个矩形
          ctx.clearRect(0,0,422,422);
          //strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式  
          ctx.strokeStyle = "#FFBE04";
          //font 属性设置或返回画布上文本内容的当前字体属性
          ctx.font = '16px Microsoft YaHei';      
          for(var i = 0; i < turnplate.restaraunts.length; i++) {       
            var angle = turnplate.startAngle + i * arc;
            ctx.fillStyle = turnplate.colors[i];
            ctx.beginPath();
            //arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）    
            ctx.arc(211, 211, turnplate.outsideRadius, angle, angle + arc, false);    
            ctx.arc(211, 211, turnplate.insideRadius, angle + arc, angle, true);
            ctx.stroke();  
            ctx.fill();
            //锁画布(为了保存之前的画布状态)
            ctx.save();   
            
            //----绘制奖品开始----
            ctx.fillStyle = "#E5302F";
            var text = turnplate.restaraunts[i];
            var line_height = 17;
            //translate方法重新映射画布上的 (0,0) 位置
            ctx.translate(211 + Math.cos(angle + arc / 2) * turnplate.textRadius, 211 + Math.sin(angle + arc / 2) * turnplate.textRadius);
            
            //rotate方法旋转当前的绘图
            ctx.rotate(angle + arc / 2 + Math.PI / 2);
            
            /** 下面代码根据奖品类型、奖品名称长度渲染不同效果，如字体、颜色、图片效果。(具体根据实际情况改变) **/
            if(text.indexOf(" ")>0){//流量包
              var texts = text.split(" ");
              for(var j = 0; j<texts.length; j++){
                ctx.font = j == 0?'bold 20px Microsoft YaHei':'16px Microsoft YaHei';
                if(j == 0){
                  ctx.fillText(texts[j]+" ", -ctx.measureText(texts[j]+" ").width / 2, j * line_height);
                }else{
                  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
                }
              }
            }else if(text.indexOf(" ") == -1 && text.length>6){//奖品名称长度超过一定范围 
              text = text.substring(0,6)+"||"+text.substring(6);
              var texts = text.split("||");
              for(var j = 0; j<texts.length; j++){
                ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
              }
            }else{
              //在画布上绘制填色的文本。文本的默认颜色是黑色
              //measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
              ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
            }
            
            //添加对应图标
            // if(text.indexOf("闪币")>0){
            //   var img= document.getElementById("shan-img");
            //   img.onload=function(){  
            //     ctx.drawImage(img,-15,10);      
            //   }; 
            //   ctx.drawImage(img,-15,10);  
            // }else if(text.indexOf("谢谢参与")>=0){
            //   var img= document.getElementById("sorry-img");
            //   img.onload=function(){  
            //     ctx.drawImage(img,-15,10);      
            //   };  
            //   ctx.drawImage(img,-15,10);  
            // }
            //把当前画布返回（调整）到上一个save()状态之前 
            ctx.restore();
            //----绘制奖品结束----
          }     
        } 
      }

      $('.pointer').click(function (){
        if($scope.luckydraw.left_times<=0) {
          alert('您的抽奖机会已用完');
          return;
        }
        if(turnplate.bRotate)return;
        turnplate.bRotate = !turnplate.bRotate;

        $http.get("index.php?route=rest/order/tryLuck&id="+orderId).then(
          function(result) {
            if (result.data.success) {
              $scope.luckydraw = result.data.luckydraw;
              var item = result.data.luckydraw.item;
              var left_times = result.data.luckydraw.left_times;
              $scope.rotateFn(item+1, left_times, turnplate.restaraunts[item]);
            }
          }
        )
        //获取随机数(奖品个数范围内)
        //奖品数量等于10,指针落在对应奖品区域的中心角度[252, 216, 180, 144, 108, 72, 36, 360, 324, 288]
      });

    }
  ]);


});
