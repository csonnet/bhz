'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('UsercenterCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
    function($scope, $rootScope, $http, $cookies, $location ) {
      $http.get('index.php?route=rest/account/account').then(function(result){
        if(result.data.success){
          $scope.userinfo = result.data.data;
        }
      })

      $scope.gotoOrders = function(status){
        $location.url("/orders");
        $location.search({status:status});
      }

	  $scope.gotoPeriods = function(status){
        $location.url("/periods");
        $location.search({status:status});
      }
	  
		//2条最新消息
		$http.get("index.php?route=message/message/getMyMessage").then(function(result){
			if (result.data.success) {
				$scope.messages = result.data.messages;
			}
		})

      $http.get("index.php?route=rest/order/orders").then(
          function(result) {
            if (result.data.success) {
              $scope.orderList = result.data.data.orders;
            }
          }
      )

    }
  ]);



});
