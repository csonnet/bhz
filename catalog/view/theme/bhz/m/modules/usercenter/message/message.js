'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

	angularAMD.controller('MessageCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
		function($scope, $rootScope, $http, $cookies, $location ) {

			var limit = 10000;

			getMyMessage();

			//获取我的消息
			function getMyMessage(){
				
				var baseUrl = "index.php?route=message/message/getMyMessage&limit="+limit;

				$http.get(baseUrl).then(function(result){

					if(result.data.success){
						$scope.myMessages = result.data.messages;
					}

				})

			}

			$scope.back = function(){
				window.history.back();
			}
      
		}
	]);

	angularAMD.controller('MessageInfoCtrl', ['$routeParams', '$scope', '$rootScope', '$http', '$cookies', '$location', 
		function($routeParams, $scope, $rootScope, $http, $cookies, $location ) {

			var sendId = $routeParams.send_id;

			if(!sendId){
				alert("缺少消息ID!");
				return;
			}

			$http.get("index.php?route=message/message/getMessageInfo&id="+sendId).then(function(result){

				if(result.data.success){
					$scope.messageInfo = result.data.messageInfo;
					hasRead(sendId);
				}

			})
			
			//已读
			function hasRead(id){

				var postdata = {};
				postdata.id = id;

				$http.post('index.php?route=message/message/hasRead',postdata)
				.then(function(result){
				});

			}

			$scope.back = function(){
				window.history.back();
			}
      
		}
	]);

});
