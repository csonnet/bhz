'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('WishlistCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
    function($scope, $rootScope, $http, $cookies, $location ) {

      $scope.type = "product";

      function refreshData(){
        $http.get('index.php?route=rest/wishlist/wishlist&product').then(function(result){
          if(result.data.success){
            console.log(result.data);

            $scope.products = result.data.data.products;
          }
        })

        $http.get('index.php?route=rest/wishlist/wishlist&vendor').then(function(result){
          if(result.data.success){
            console.log(result.data);

            $scope.stores = result.data.data.vendor
          }
        })
      }

      $scope.changeType = function(type){
        $scope.type = type;
      }

      $scope.removeProductFromWishlist = function(productId){
        $http.delete('index.php?route=rest/wishlist/wishlist/&id='+productId).then(function(result){
          if(result.data.success){
            refreshData();
          }
        })
      }


      $scope.removeStoreFromWishlist = function(storeId){
        $http.delete('index.php?route=rest/wishlist/wishlist/&manu_id='+storeId).then(function(result){
          if(result.data.success){
            refreshData();
          }
        })
      }

      $scope.gotoVendor = function(vendor_id){
        $location.path('/vendor/' + vendor_id)
      }

      refreshData();
    }
  ]);



});
