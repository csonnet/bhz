'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

  angularAMD.controller('MOrdersListCtrl', ['$scope', '$http', '$location', 'ngDialog',
    function($scope, $http, $location, ngDialog) {
      var searchObj = $location.search();
      $scope.showFilter = false;

      $scope.selection={
        "tab": searchObj.status ? searchObj.status : 'all'
      }

      $scope.switchTab = function(status){
        if ('filter' != status) {
          $scope.selection.tab = status;
          $scope.showFilter = false;
          if (1 == page[status]) {
            loadOrders();
          }
        }else{
          $scope.showFilter = true;
        }
      }

      var page = {};
      page.all = 1;
      page.receiptWithOutPay = 1;
      page.logcenterShipping = 1;
      page.warehouseShipping = 1;
      page.warehousePrepare = 1;
      page.unstart = 1;
      var limit = 10;
      var hasMore = {};
      hasMore.all = true;
      hasMore.receiptWithOutPay = true;
      hasMore.logcenterShipping = true;
      hasMore.warehouseShipping = true;
      page.warehousePrepare = true;
      hasMore.unstart = true;
      var orderLoading = false;
      $scope.orderList = {};
      loadOrders();

      var $w = $(window);
      $w.unbind("scroll");
      $w.bind("scroll", function(){
        if($w.scrollTop()+$w.height() > $("#LoadingPlace").offset().top && hasMore[$scope.selection.tab] && !orderLoading){
          loadOrders();
        }
      });

      function loadOrders() {
        var selectedTab = $scope.selection.tab;
        orderLoading = true;
        $http.get("index.php?route=rest/manager/orders&page="+page[selectedTab]+"&limit="+limit+"&status="+selectedTab).then(
          function(result) {
            if (result.data.success) {
              if (1 == page[selectedTab]){
                $scope.orderList[selectedTab] = result.data.orders;
              }else{
                angular.forEach(result.data.orders, function(orderInfo){
                  $scope.orderList[selectedTab].push(orderInfo);
                })
              }
              if (result.data.orders.length < limit) {
                $('#LoadingPlace').hide();
                hasMore[selectedTab] = false;
              }else{
                $('#LoadingPlace').show();
              }
              page[selectedTab]++;
            }
            orderLoading = false;
          }
        );
      }

      $scope.gotoOrder = function(order_id){
        $location.url("/usercenter/manager/order/"+order_id);
      }

	 $scope.back = function(){
		window.history.back();
	  }

    }
  ]);

  angularAMD.controller('MOrdersInfoCtrl', ['$scope', '$routeParams', '$http', '$location', 'ngDialog',
    function($scope, $routeParams, $http, $location, ngDialog) {
		
		var ordersInfo = $location.search();

		$scope.selection={
			"tab": ordersInfo.status ? ordersInfo.status : 1
		}
		$scope.switchTab = function(status){
			$scope.selection.tab = status;
		}

      var orderId = $routeParams.order_id;

      if(!orderId){
        alert("缺少订单id..");return;
      }

      $http.get("index.php?route=rest/manager/orders&id="+orderId).then(
        function(result) {
          if (result.data.success) {
            $scope.orderInfo = result.data.order;
          }
      });

		$scope.back = function(){
			window.history.back();
		}

    }
  ]);
});
