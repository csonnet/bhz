'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

  angularAMD.controller('MOrdersListByAreaCtrl', ['$scope', '$http', '$location', 'ngDialog',
    function($scope, $http, $location, ngDialog) {
      var searchObj = $location.search();
      $scope.showFilter = false;

      $scope.selection={
        "tab": searchObj.area ? searchObj.area : '0'
      }

      $scope.switchTab = function(area){
        $scope.selection.tab = area;
        page = 1;
        loadOrders();
      }

      var page = 1;
      var limit = 10;
      var hasMore = true;
      var orderLoading = false;
      $scope.orderList = '';
      loadOrders();

      var $w = $(window);
      $w.unbind("scroll");
      $w.bind("scroll", function(){
        if($w.scrollTop()+$w.height() > $("#LoadingPlace").offset().top && hasMore && !orderLoading){
          loadOrders();
        }
      });

      function loadOrders() {
        var selectedTab = $scope.selection.tab;
        orderLoading = true;
        $http.get("index.php?route=rest/manager/ordersByArea&page="+page+"&limit="+limit+"&areaId="+selectedTab).then(
          function(result) {
            $scope.tabList = result.data.tabList;
            if (1 == page){
              $scope.orderList = result.data.orders;
            }else{
              angular.forEach(result.data.orders, function(orderInfo){
                $scope.orderList.push(orderInfo);
              })
            }
            if (result.data.orders.length < limit) {
              $('#LoadingPlace').hide();
              hasMore = false;
            }else{
              $('#LoadingPlace').show();
              hasMore = true;
            }
            page++;
            orderLoading = false;
          }
        );
      }

      $scope.gotoOrder = function(order_id){
        return false;
        $location.url("/usercenter/manager/ordersByArea/"+order_id);
      }

	 $scope.back = function(){
		window.history.back();
	  }

    }
  ]);

  angularAMD.controller('MOrdersInfoCtrl', ['$scope', '$routeParams', '$http', '$location', 'ngDialog',
    function($scope, $routeParams, $http, $location, ngDialog) {
		
		var ordersInfo = $location.search();

		$scope.selection={
			"tab": ordersInfo.status ? ordersInfo.status : 1
		}
		$scope.switchTab = function(status){
			$scope.selection.tab = status;
		}

      var orderId = $routeParams.order_id;

      if(!orderId){
        alert("缺少订单id..");return;
      }

      $http.get("index.php?route=rest/manager/orders&id="+orderId).then(
        function(result) {
          if (result.data.success) {
            $scope.orderInfo = result.data.order;
          }
      });

		$scope.back = function(){
			window.history.back();
		}

    }
  ]);
});
