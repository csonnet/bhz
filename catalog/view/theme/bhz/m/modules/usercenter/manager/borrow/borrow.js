'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {
	
	//我的借货单
	angularAMD.controller('MBorrowCtrl', ['$scope', '$http', '$cookies', '$location', '$compile', 
		function($scope, $http, $cookies, $location, $compile ) {

			var limit = 10;

			var $w = $(window);
			$(".content-wrapper").css("min-height",$w.height());
			$(".filter-panel").css("min-height",$w.height());

			var borrow = $location.search();
			
			$scope.selection={
				"tab": borrow.status ? borrow.status : 1
			}
			$scope.switchTab = function(status){

				$scope.selection.tab = status;
				$("#LoadingPlace").html('');

				if(status == 'search'){
					$scope.showSearch();
				}

			}
			
			getToDo();
			getMyLoan();
			
			//获取待我操作的借货单
			function getToDo(){

				var baseUrl = "index.php?route=manager/mloan/getToDo&limit="+limit;

				$http.get(baseUrl).then(function(result){
					console.log(result);
					if(result.data.success){	
						$scope.verifyloans = result.data.verifyloans;
						$scope.signloans = result.data.signloans;
						$scope.returnloans = result.data.returnloans;
					}

				});

			}
			
			//获取我发起的借货单
			function getMyLoan(){
				
				var baseUrl = "index.php?route=manager/mloan/getMyLoan&limit="+limit;

				$http.get(baseUrl).then(function(result){
					if(result.data.success){	
						$scope.myloans = result.data.myloans;
					}
				});

			}

			//筛选弹出
			$scope.showSearch = function(){
				$scope.showSearchFlag = true;
			}
			
			//筛选隐藏
			$scope.closeSearch = function(){
				$scope.showSearchFlag = false;
			}

			//弹出审核
			$scope.showMemo = function(result){

				if(result == 0){
					$scope.dialogbtn ="拒绝";
				}
				else if(result == 1){
					$scope.dialogbtn ="通过";
				}

				$(".memo_div").fadeIn();

			}

			//关闭弹出
			$scope.hideConfirm = function(){
				$(".confirm_div").fadeOut();
			}
			
		}
	]);
	
	//
	angularAMD.controller('MBorrowDetailCtrl', ['$scope', '$http', '$cookies', '$location', '$compile', 
		function($scope, $http, $cookies, $location, $compile ) {

			var borrow = $location.search();
			
			$scope.selection={
				"tab": borrow.status ? borrow.status : 1
			}
			$scope.switchTab = function(status){
				$scope.selection.tab = status;
			}

			getWarehouses();
			getLogcenters();
			
			//获取仓库
			function getWarehouses(){
				
				var baseUrl = "index.php?route=public/public/getWarehouses";

				$http.get(baseUrl).then(function(result){
					if(result.data.success){	
						$scope.warehouses = result.data.warehouses;
					}
				});

			}
			
			//获取物流中心
			function getLogcenters(){
				
				var baseUrl = "index.php?route=public/public/getLogcenters";

				$http.get(baseUrl).then(function(result){
					if(result.data.success){	
						$scope.logcenters = result.data.logcenters;
					}
				});

			}

			//添加商品
			$scope.addProduct = function(){

				var temp = '<div class="monitor-win">'+
									'<p>'+
										'<span style="position:absolute;right:6%;">'+
											'<a style="margin:2px 2px;" class="destory-btn" href="javascript:void(0);"  ng-click="removeAdd($event)">删除</a>'+
										'</span>'+
									'</p>'+
									'<table class="tab_table" style="clear:both;">'+
										'<tr>'+
											'<td class="t_title_long">商品编码：</td>'+
											'<td colspan="3">'+
												'<input name="product_code" type="text" value="" />'+
											'</td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title_long">商品名称：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title_long">条形码：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title_long">数量：</td>'+
											'<td colspan="3">'+
												'<input type="text" value="" />'+
											'</td>'+
										'</tr>'+
									'</table>'+
								'</div>';

				var content = $compile(temp)($scope);
				$("#product-p").prepend(content);

			}

			//添加审批人
			$scope.addPerson = function(){

				//demo 测试用
				var json = new Array('it.admin','张俊雅','刘颂颂','123456789012');

				var option;
				for(var i=0;i<json.length;i++){
					option += '<option>'+json[i]+'</option>';
				}

				var temp = '<div class="monitor-win">'+
									'<p>'+
										'<span style="position:absolute;right:6%;">'+
											'<a style="margin:2px 2px;" class="destory-btn" href="javascript:void(0);"  ng-click="removeAdd($event)">删除</a>'+
										'</span>'+
									'</p>'+
									'<table class="tab_table" style="clear:both;">'+
										'<tr>'+
											'<td class="t_title_long">审批人会员名：</td>'+
											'<td colspan="3">'+
												'<select>'+option+'</select>'+
											'</td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title_long">审批人全名：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
									'</table>'+
								'</div>';

				var content = $compile(temp)($scope);
				$("#person-p").prepend(content);

			}

			//删除添加的div
			$scope.removeAdd = function(event){
				$(event.target).parent().parent().parent().remove();
			}
			
			$scope.back = function(){
				window.history.back();
			}

		}
	]);

});
