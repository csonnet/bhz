'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

  angularAMD.controller('MReportBySaleCtrl', ['$scope', '$http', '$location', 'ngDialog',
    function($scope, $http, $location, ngDialog) {
      var myOrderBar = '';
      var myCustomerPie = '';
      var searchObj = $location.search();
      $scope.userName = searchObj.userName;
      loadGatherInfo();

      function loadGatherInfo() {
        $http.get("index.php?route=rest/manager/saleReport&userId="+searchObj.userId).then(
          function(result) {
            if (result.data.success) {
              $scope.saleGatherInfo = result.data.gatherInfo;

              //初始化柱状图
              myOrderBar = echarts.init(document.getElementById('myOrderBar'), 'shine');
              var myBarOption = {
                title: {
                  text: '日销售统计',
                  left: '40%',
                  top: 15
                },
                legend: {
                  right: 20,
                  top: 0,
                  data: $scope.saleGatherInfo.bar.legendData
                },
                barWidth: '60%',
                xAxis: {
                  data: $scope.saleGatherInfo.bar.xData
                },
                yAxis: {},
                series: $scope.saleGatherInfo.bar.series
              };
              myOrderBar.setOption(myBarOption);

              //初始化饼状图
              myCustomerPie = echarts.init(document.getElementById('myCustomerPie'), 'shine');
              var myPieOption = {
                title: {
                  text: '合作客户结构',
                  left: '40%',
                  top: 15
                },
                legend: {
                  x: 10,
                  y: 20,
                  orient: 'vertical',
                  data: $scope.saleGatherInfo.pie.legendData
                },
                series: [{
                  type: 'pie',
                  stillShowZeroSum: false,
                  label: {
                    normal: {
                      show: false,
                      position: 'inner',
                    },
                    emphasis: {
                      show: true,
                      formatter: "数量：{c}\n\r占比：{d}%",
                    }
                  },
                  radius: '70%',
                  center: ['50%', '60%'],
                  data: $scope.saleGatherInfo.pie.seriesData
                }]
              }
              myCustomerPie.setOption(myPieOption);
            }
          }
        );
      }
      window.onresize = function (){
        myOrderBar.resize();
        myCustomerPie.resize();
      }
      $scope.gotoPage = function(pageUrl, areaId) {
        return false;
        $location.url("/usercenter/manager/"+pageUrl);
        $location.search({area:$scope.selection.tab});
      }
      $scope.getUserData = function (userId){
        return false;
        $location.url("/usercenter/manager/reportBySale");
        $location.search({userId:userId});
      }
    }
  ]);
});
