'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

  angularAMD.controller('ManagerCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
    function($scope, $rootScope, $http, $cookies, $location ) {
      var myOrderBar = '';
      var myCustomerPie = '';
      $http.get('index.php?route=rest/account/account').then(function(result){
        if(result.data.success){
          if (result.data.data.MG_userId < 1) {
            $location.url("/usercenter");
          }
          $scope.userinfo = result.data.data;

          //初始化柱状图
          myOrderBar = echarts.init(document.getElementById('myOrderBar'), 'shine');
          var myBarOption = {
            title: {
              text: '日销售统计',
              left: '40%',
              top: 15
            },
            legend: {
              right: 20,
              top: 0,
              data: $scope.userinfo.MG_saleGatherInfo.bar.legendData
            },
            barWidth: '60%',
            xAxis: {
              data: $scope.userinfo.MG_saleGatherInfo.bar.xData
            },
            yAxis: {},
            series: $scope.userinfo.MG_saleGatherInfo.bar.series
          };
          myOrderBar.setOption(myBarOption);

          //初始化饼状图
          myCustomerPie = echarts.init(document.getElementById('myCustomerPie'), 'shine');
          var myPieOption = {
            title: {
              text: '合作客户结构',
              left: '40%',
              top: 15
            },
            legend: {
              x: 10,
              y: 20,
              orient: 'vertical',
              data: $scope.userinfo.MG_saleGatherInfo.pie.legendData
            },
            series: [{
              type: 'pie',
              stillShowZeroSum: false,
              label: {
                normal: {
                  show: false,
                  position: 'inner',
                },
                emphasis: {
                  show: true,
                  formatter: "数量：{c}\n\r占比：{d}%",
                }
              },
              radius: '70%',
              center: ['50%', '60%'],
              data: $scope.userinfo.MG_saleGatherInfo.pie.seriesData
            }]
          }
          myCustomerPie.setOption(myPieOption);
        }
      })
      window.onresize = function (){
        myOrderBar.resize();
        myCustomerPie.resize();	
      }
      $scope.gotoPage = function(pageUrl, status) {
        $location.url("/usercenter/manager/"+pageUrl);
        $location.search({status:status});
      }

		//2条最新消息
		$http.get("index.php?route=message/message/getMyMessage").then(function(result){
			if (result.data.success) {
				$scope.messages = result.data.messages;
			}
		})

    }
  ]);
	
	angularAMD.controller('MSendCtrl', ['$scope', '$http', '$cookies', '$location', 
		function($scope, $http, $cookies, $location ) {
			
			var $w = $(window);
			$w.unbind("scroll");
			//$w.unbind("touchend");
			$(".content-wrapper").css("min-height",$w.height());
			$(".filter-panel").css("min-height",$w.height());
			
			var stockOut = $location.search();
			
			var moreLoading = true;
			
			var logcenter_id;
			var sendStart,signStart,rejectStart,overStart;
			var limit = 10;

			$scope.selection={
				"tab": stockOut.status ? stockOut.status : 'send'
			}
			$scope.switchTab = function(status){

				$scope.selection.tab = status;
				if(status == 'search'){
					$scope.showSearch();
				}

				$("#LoadingPlace").html('');
			}
			
			getStockOut();

			//获取业务员全部出库单
			function getStockOut(){

				var baseUrl = "index.php?route=manager/mstock/getStockOut&limit="+limit;

				$http.get(baseUrl).then(function(result){

					if(result.data.success){	
						$scope.outList = result.data.outList;
						sendStart = result.data.sendCount;
						signStart = result.data.signCount;
						rejectStart = result.data.rejectCount;
						overStart = result.data.overCount;
						logcenter_id = result.data.logcenter_id;
					}

				})

			}
			
			//加载更多单据
			function loadMoreOut(tab){

				var status,start;

				switch(tab){
					case 'send':
						status = 2;
						start = sendStart;
						break;
					case 'sign':
						status = 3;
						start = signStart;
						break;
					case 'reject':
						status = 5;
						start = rejectStart;
						break;
					case 'over':
						status = 4;
						start = overStart;
						break;
					default:
						break;
				}
				
				var baseUrl = 'index.php?route=manager/mstock/loadMoreOut&tab='+tab+'&logcenter_id='+logcenter_id+'&status='+status+'&start='+start+'&limit='+limit;

				$http.get(baseUrl).then(function(result){

					if(result.data.success && result.data.outList){	

						angular.forEach(result.data.outList[tab],function(productItem){
							$scope.outList[tab].push(productItem);
						})

						switch(tab){
							case 'send':
								sendStart += result.data.count;
								break;
							case 'sign':
								signStart += result.data.count;
								break;
							case 'reject':
								rejectStart += result.data.count;
								break;
							case 'over':
								overStart += result.data.count;
								break;
							default:
								break;
						}

					}
					else{
						$("#LoadingPlace").html('已全部加载完毕');
					}

					moreLoading = true;

				});

			}
			
			//点击搜索确定
			$scope.searchSend = function(){

				var postdata = {};
				postdata.out_id = $("#out_id").val();
				postdata.refer_id = $("#refer_id").val();
				postdata.status = $("#status").val();

				$http.post('index.php?route=manager/mstock/searchSend',postdata)
				.then(function(result){
					$scope.searchList = result.data.searchList;
				});

				$scope.closeSearch();

			}
			
			//点击配送按钮
			$scope.checkSend = function(out_id,refer_id){

				var postdata = {};
				postdata.out_id = out_id;
				postdata.refer_id = refer_id;

				$http.post('index.php?route=manager/mstock/checkSend',postdata)
				.then(function(result){

					if (result.data && result.data.success) {
						
						getStockOut();
						$scope.searchSend();
						BHZ.MessageDialog.show(result.data.message);	

					}

				});

			}

			//点击签收按钮
			$scope.checkSign = function(out_id,refer_id){

				var postdata = {};
				postdata.out_id = out_id;
				postdata.refer_id = refer_id;

				$http.post('index.php?route=manager/mstock/checkSign',postdata)
				.then(function(result){

					if (result.data && result.data.success) {
						
						getStockOut();
						$scope.searchSend();
						BHZ.MessageDialog.show(result.data.message);	

					}

				})

			}

			//点击拒收按钮
			$scope.checkReject = function(out_id,refer_id){

				var postdata = {};
				postdata.out_id = out_id;
				postdata.refer_id = refer_id;

				$http.post('index.php?route=manager/mstock/checkReject',postdata)
				.then(function(result){

					if (result.data && result.data.success) {
						
						getStockOut();
						$scope.searchSend();
						BHZ.MessageDialog.show(result.data.message);	

					}

				})

			}

			//筛选弹出
			$scope.showSearch = function(){
				$scope.showSearchFlag = true;
			}
			
			//筛选隐藏
			$scope.closeSearch = function(){
				$scope.showSearchFlag = false;
			}

			//上拉加载
			$w.bind("scroll", function(){ //touchend

				if($w.scrollTop()+$w.height() > ($("#LoadingPlace").offset().top) && moreLoading){

					moreLoading = false;
					
					$("#LoadingPlace").html('加载中...');

					var tab = $scope.selection.tab;

					loadMoreOut(tab);

				}
        
			});

			$scope.back = function(){
				window.history.back();
			}

		}
	]);
	
	//业绩分析
	angularAMD.controller('MChartCtrl', ['$scope', '$http', '$cookies', '$location', 
		function($scope, $http, $cookies, $location ) {
			
			var areaChart = $location.search();
			
			$scope.selection={
				"tab": areaChart.status ? areaChart.status : 'area1'
			}
			$scope.switchTab = function(status){
				$scope.selection.tab = status;
			}
			
			var myOrderBar = '';
			var myCustomerPie = '';
			$http.get('index.php?route=rest/account/account').then(function(result){
				if(result.data.success){
				  if (result.data.data.MG_userId < 1) {
					$location.url("/usercenter");
				  }
				  $scope.userinfo = result.data.data;

				  //初始化柱状图
				  myOrderBar = echarts.init(document.getElementById('myOrderBar'), 'shine');
				  var myBarOption = {
					title: {
					  text: '日销售统计',
					  left: '40%',
					  top: 15
					},
					legend: {
					  right: 20,
					  top: 0,
					  data: $scope.userinfo.MG_saleGatherInfo.bar.legendData
					},
					barWidth: '60%',
					xAxis: {
					  data: $scope.userinfo.MG_saleGatherInfo.bar.xData
					},
					yAxis: {},
					series: $scope.userinfo.MG_saleGatherInfo.bar.series
				  };
				  myOrderBar.setOption(myBarOption);

				  //初始化饼状图
				  myCustomerPie = echarts.init(document.getElementById('myCustomerPie'), 'shine');
				  var myPieOption = {
					title: {
					  text: '合作客户结构',
					  left: '40%',
					  top: 15
					},
					legend: {
					  x: 10,
					  y: 20,
					  orient: 'vertical',
					  data: $scope.userinfo.MG_saleGatherInfo.pie.legendData
					},
					series: [{
					  type: 'pie',
					  stillShowZeroSum: false,
					  label: {
						normal: {
						  show: false,
						  position: 'inner',
						},
						emphasis: {
						  show: true,
						  formatter: "数量：{c}\n\r占比：{d}%",
						}
					  },
					  radius: '70%',
					  center: ['50%', '60%'],
					  data: $scope.userinfo.MG_saleGatherInfo.pie.seriesData
					}]
				  }
				  myCustomerPie.setOption(myPieOption);
				}
			  })
			  window.onresize = function (){
				myOrderBar.resize();
				myCustomerPie.resize();	
			  }

			$scope.back = function(){
				window.history.back();
			}

		}
	]);
	
	//出入库单管理
	angularAMD.controller('MStockCtrl', ['$scope', '$http', '$cookies', '$location', '$compile',
		function($scope, $http, $cookies, $location, $compile ) {

			var $w = $(window);
			$(".content-wrapper").css("min-height",$w.height());
			$(".filter-panel").css("min-height",$w.height());
			
			var stock = $location.search();
			
			$scope.selection={
				"tab": stock.status ? areaChart.status : 1
			}
			$scope.switchTab = function(status){
				
				$scope.selection.tab = status;

				if(status == 'search'){
					$scope.showSearch();
				}

			}
			
			//筛选弹出
			$scope.showSearch = function(){
				$scope.showSearchFlag = true;
			}
			
			//筛选隐藏
			$scope.closeSearch = function(){
				$scope.showSearchFlag = false;
			}

			//添加商品
			$scope.addProduct = function(){

				var temp = '<div class="monitor-win">'+
									'<p>'+
										'<span style="position:absolute;right:6%;">'+
											'<a style="margin:2px 2px;" class="destory-btn" href="javascript:void(0);"  ng-click="removeProduct($event)">删除</a>'+
										'</span>'+
									'</p>'+
									'<table class="tab_table" style="clear:both;">'+
										'<tr>'+
											'<td class="t_title">商品编码：</td>'+
											'<td colspan="3">'+
												'<input type="text" value="" />'+
											'</td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title">商品名称：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title">条形码：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title">选项：</td>'+
											'<td colspan="3"></td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title">单价：</td>'+
											'<td colspan="3">18.0000</td>'+
										'</tr>'+
										'<tr>'+
											'<td class="t_title">数量：</td>'+
											'<td colspan="3">'+
												'<input type="text" value="" />'+
											'</td>'+
										'</tr>'+
									'</table>'+
								'</div>';

				var content = $compile(temp)($scope);
				$("#product-p").prepend(content);

			}
			
			//删除商品
			$scope.removeProduct = function(event){
				$(event.target).parent().parent().parent().remove();
			}
			
			$scope.back = function(){
				window.history.back();
			}

		}
	]);

});
