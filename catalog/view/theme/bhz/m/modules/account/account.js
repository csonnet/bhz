'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('LoginCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location',
    function($scope, $rootScope, $http, $cookies, $location) {

      $scope.username = "owen";

      $scope.login = function() {
        $http.post("index.php?route=rest/login/login", $scope.userInfo).then(
          function(result) {
            if (result.data.success) {
                 $rootScope.bhz_user = result.data.data;
                 if(result.data.data.is_check == '1'){
                    $cookies.putObject('bhz_user', result.data.data, { path: '/' });
                    $cookies.putObject('myuser_id', result.data.data.myuser_id, { path: '/' });
                    $cookies.putObject('customer_id', result.data.data.customer_id, { path: '/' });
                    $cookies.putObject('is_receive', result.data.data.is_receive, { path: '/' });
                    $location.path("/home");
                 }else{
                    $scope.show_mubu();
                 }
              }
            }
          )

        $scope.show_mubu = function(){
          $(".pc_xy").fadeIn();
        }

        $scope.hideMubu = function(){
          $(".pc_xy").fadeOut();
          $http.post("index.php?route=rest/login/setIsCheck", $rootScope.bhz_user).then(
              function(result) {
              if (result.data.success){
                $cookies.putObject('bhz_user', result.data.data, { path: '/' });
                $location.path("/home");
              }
            }
          )
        }
      }
    }
  ]);

  angularAMD.controller('RegisterCtrl', ['$scope', '$http', '$location',
    function($scope, $http, $location) {

      $scope.wating = false;
      $scope.smsSend = false;

      $scope.getRegisterSMS = function() {
        if ($scope.wating) {
          return;
        }
        if (!$scope.userInfo.telephone) {
          BHZ.MessageDialog.show('手机号不能为空');
          return;
        }
        $scope.wating = true;
        $scope.counting = 60;
        timer();
        var params = {
          telephone: $scope.userInfo.telephone
        }
        $http.post("index.php?route=rest/sms/create_mobile_code", params).then(
          function(result) {
            if (result.data.success) {
              $scope.smsSend = true;
            }
          }
        )
      }

      $scope.register = function() {
        if (!$scope.userInfo.sms_code) {
          //BHZ.MessageDialog.show('请输入验证码');
          //return;
        }

        $http.post("index.php?route=rest/register/register", $scope.userInfo).then(
          function(result) {
            if (result.data.success) {
              BHZ.MessageBox.show("注册成功");
              $location.path('/usercenter');
            }
          }
        )
      }

      function timer() {
        if ($scope.counting > 0) {
          $scope.counting--;
          $(".counting").html($scope.counting);
          setTimeout(timer, 1000);
        } else {
          $scope.$apply(function() {
            $scope.wating = false;
          })
        }

      }

      $scope.xy_value = false;

      $scope.check_value = function(){
        $scope.xy_value = !$scope.xy_value;
      }

      $scope.show_mubu = function(){
        $(".pc_xy").fadeIn();
      }

      $scope.hideMubu = function(){
        $(".pc_xy").fadeOut();
      }
    }
  ]);

  angularAMD.controller('PasswordCtrl', ['$scope', '$http', '$location',
    function($scope, $http, $location) {
      $scope.step = 1;

      $scope.info = {};

      $scope.checkMobile = function(){
        if (!$scope.info.telephone) {
          BHZ.MessageDialog.show('手机号不能为空');
          return;
        }

        if(!BHZ.checkMobileFormat($scope.info.telephone)){
          BHZ.MessageDialog.show('手机号格式不正确');
          return;
        }
        $scope.step = 2;
        $scope.info.sms_code = "";
      }

      $scope.getSMS = function() {

        if ($scope.wating) {
          return;
        }

        $scope.wating = true;
        $scope.counting = 60;
        timer();

        var params = {
          telephone: $scope.info.telephone
        }
        $http.post("index.php?route=rest/sms/create_mobile_code", params).then(
          function(result) {
            if (result.data.success) {
              $scope.step = 2;
            }
          }
        )
      }

      $scope.verifySMS = function() {
        if (!$scope.info.sms_code) {
          BHZ.MessageDialog.show('验证码不能为空');
          return;
        }
        $scope.step = 3;
        $scope.info.password = ""
      }

      $scope.updatePassword = function() {
        if (!$scope.info.password) {
          BHZ.MessageDialog.show('密码不能为空');
          return;
        }
        $http.post("index.php?route=rest/forgottenphone/forgottenphone", $scope.info)
          .then(function(result) {
            if (result.data.success) {
              BHZ.MessageDialog.show('新密码设置成功');
              $location.path('/usercenter');
            }
          })
      }

      function timer() {
        if ($scope.counting > 0) {
          $scope.counting--;
          $(".counting").html($scope.counting);
          setTimeout(timer, 1000);
        } else {
          $scope.$apply(function() {
            $scope.wating = false;
          })
        }

      }
    }
  ]);



});