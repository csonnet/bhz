'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

	angularAMD.controller('pageCtrl', ['$scope', '$http', '$location', '$timeout', '$cookies',
		function($scope, $http, $location, $timeout, $cookies) {
			
			$scope.gotoCategory = function(category_id){
				if(category_id){
					var sc = window.sessionStorage;
					sc.setItem("currentFirst", category_id);
					$location.url('/categories').search({category_id:category_id});
				}
			}

		}
	]);

});