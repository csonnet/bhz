'use strict';

define(['angular', 'angularAMD', 'angular-route'], function(ng, angularAMD) {

  var BHZRoute = ng.module('BHZRoute', ['ngRoute']);

  BHZRoute.constant("BHZConfig", {
    MODULE_PATH: 'catalog/view/theme/bhz/m/modules/'
  });

  BHZRoute.config(function($routeProvider, $locationProvider, BHZConfig) {
    $routeProvider
      .when("/login", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/login.html',
        controllerUrl: 'modules/account/account',
        controller: 'LoginCtrl',
        title: '用户登录'
      }))
      .when("/register", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/register.html',
        controllerUrl: 'modules/account/account',
        controller: 'RegisterCtrl',
        title: '百货栈注册'
      }))
      .when("/password", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/password.html',
        controllerUrl: 'modules/account/account',
        controller: 'PasswordCtrl',
        title: '找回密码'
      }))
      .when("/usercenter", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/usercenter.html',
        controllerUrl: 'modules/usercenter/usercenter',
        controller: 'UsercenterCtrl',
        title: '个人中心'
      }))
      .when("/usercenter/manager", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/manager.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'ManagerCtrl',
        title: '业务管理'
      }))
	  .when("/usercenter/manager/send", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/send.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MSendCtrl',
        title: '我的送货单',
		withoutHeader: true,
		withoutFooter: true
      }))
    //我的 -> 业务管理 -> 我的订单（列表）
    .when("/usercenter/manager/ordersList", angularAMD.route({
      templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/ordersList.html',
      controllerUrl: 'modules/usercenter/manager/orders',
      controller: 'MOrdersListCtrl',
      title: '我的订单',
      withoutHeader: true,
	  withoutFooter: true
    }))
    //我的 -> 业务管理 -> 我的订单（详情）
    .when('/usercenter/manager/order/:order_id', angularAMD.route({
      templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/ordersInfo.html',
      controllerUrl: 'modules/usercenter/manager/orders',
      controller: 'MOrdersInfoCtrl',
      title: '订单详情',
	  withoutHeader: true,
	  withoutFooter: true
     }))
    //我的 -> 业务管理 -> 我的会员（列表）
    .when("/usercenter/manager/customerList", angularAMD.route({
      templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/customerList.html',
      controllerUrl: 'modules/usercenter/manager/customer',
      controller: 'MCustomerListCtrl',
      title: '我的会员'
    }))
	  //业绩分析
	  .when("/usercenter/manager/reportByArea", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/reportByArea.html',
        controllerUrl: 'modules/usercenter/manager/reportByArea',
        controller: 'MReportByAreaCtrl',
        title: '业绩分析'
      }))
	  //业绩分析
	  .when("/usercenter/manager/reportBySale", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/reportBySale.html',
        controllerUrl: 'modules/usercenter/manager/reportBySale',
        controller: 'MReportBySaleCtrl',
        title: '个人业绩分析'
      }))
    //我的 -> 业务管理 -> 区域订单（列表）
    .when("/usercenter/manager/ordersListByArea", angularAMD.route({
      templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/ordersListByArea.html',
      controllerUrl: 'modules/usercenter/manager/ordersByArea',
      controller: 'MOrdersListByAreaCtrl',
      title: '区域订单',
    }))
	  //我的借货单
	  .when("/usercenter/manager/borrow", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/borrow/borrow.html',
        controllerUrl: 'modules/usercenter/manager/borrow/borrow',
        controller: 'MBorrowCtrl',
        title: '我的借货单',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //添加借货单
	  .when("/usercenter/manager/addborrow", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/borrow/addborrow.html',
        controllerUrl: 'modules/usercenter/manager/borrow/borrow',
        controller: 'MBorrowDetailCtrl',
        title: '添加借货单',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //借货详细
	  .when("/usercenter/manager/borrowdetail", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/borrow/borrowdetail.html',
        controllerUrl: 'modules/usercenter/manager/borrow/borrow',
        controller: 'MBorrowDetailCtrl',
        title: '借货详细',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //入库管理
	  .when("/usercenter/manager/stockin", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/stockin.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockInCtrl',
        title: '入库管理',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //入库详细
	  .when("/usercenter/manager/indetail", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/indetail.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockCtrl',
        title: '入库详细',
		withoutHeader: true,
		withoutFooter: true
      }))
       //添加入库单
	  .when("/usercenter/manager/addin", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/addin.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockCtrl',
        title: '添加入库单',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //出库管理
	  .when("/usercenter/manager/stockout", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/stockout.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockCtrl',
        title: '出库管理',
		withoutHeader: true,
		withoutFooter: true
      }))
	  //出库详细
	  .when("/usercenter/manager/outdetail", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/outdetail.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockCtrl',
        title: '出库详细',
		withoutHeader: true,
		withoutFooter: true
      }))
       //添加出库单
	  .when("/usercenter/manager/addout", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/manager/addout.html',
        controllerUrl: 'modules/usercenter/manager/manager',
        controller: 'MStockCtrl',
        title: '添加出库单',
		withoutHeader: true,
		withoutFooter: true
      }))
      .when("/usercenter/profile", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/profile.html',
        controllerUrl: 'modules/usercenter/profile',
        controller: 'ProfileCtrl',
        title: '完善个人资料'
      }))
      .when('/usercenter/address', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/address/list.html',
        controllerUrl: 'modules/usercenter/address/address',
        controller: 'AddressListCtrl',
        title: '地址管理'
       }))
      .when('/usercenter/address/add', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/address/edit.html',
        controllerUrl: 'modules/usercenter/address/address',
        controller: 'AddressCtrl',
        title: '新建收货地址'
       }))
      .when('/usercenter/address/:address_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/address/edit.html',
        controllerUrl: 'modules/usercenter/address/address',
        controller: 'AddressCtrl',
        title: '新建收货地址'
       }))

	   //通知消息
	   .when('/usercenter/message', angularAMD.route({ //我的消息
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/message/message.html',
        controllerUrl: 'modules/usercenter/message/message',
        controller: 'MessageCtrl',
        title: '我的消息',
        withoutHeader: true,
        withoutFooter: true
       }))
	   .when('/usercenter/message/:send_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/message/messageInfo.html',
        controllerUrl: 'modules/usercenter/message/message',
        controller: 'MessageInfoCtrl',
        title: '消息详情',
        withoutHeader: true,
        withoutFooter: true
      }))

	  //余额充值
	   .when('/usercenter/balance', angularAMD.route({ //我的余额
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/balance.html',
        controllerUrl: 'modules/usercenter/balance/balance',
        controller: 'BalanceCtrl',
        title: '我的余额',
        withoutHeader: true,
        withoutFooter: true
       }))
	  .when('/usercenter/recharge', angularAMD.route({ //立即充值
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/recharge.html',
        controllerUrl: 'modules/usercenter/balance/balance',
        controller: 'RechargeCtrl',
        title: '立即充值',
        withoutHeader: true,
        withoutFooter: true
       }))
	   .when('/usercenter/deposit', angularAMD.route({ //定金充值
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/deposit.html',
        controllerUrl: 'modules/usercenter/balance/balance',
        controller: 'RechargeCtrl',
        title: '定金充值',
        withoutHeader: true,
        withoutFooter: true
       }))

      .when('/usercenter/wishlist', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/wishlist/wishlist.html',
        controllerUrl: 'modules/usercenter/wishlist/wishlist',
        controller: 'WishlistCtrl',
        title: '收藏夹'
      }))
      .when('/usercenter/coupon', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/coupon/coupon.html',
        controllerUrl: 'modules/usercenter/coupon/coupon',
        controller: 'CouponCtrl',
        title: '优惠券'
      }))
      .when('/order/:order_id/luckydraw/:luckydraw_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/luckydraw.html',
        controllerUrl: 'modules/usercenter/order/order',
        controller: 'orderLuckydrawCtrl',
        title: '幸运抽奖',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
      .when('/categories', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'product/categories.html?v=20171106',
        controllerUrl: 'modules/product/product',
        controller: 'categoriesCtrl',
        title: '商品分类'
      }))
      .when('/products', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'product/products.html?v=20171106',
        controllerUrl: 'modules/product/product',
        controller: 'productsCtrl',
        title: '商品列表'
      }))
      .when('/product/:product_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'product/product.html',
        controllerUrl: 'modules/product/product',
        controller: 'productCtrl',
        title: '商品详情',
        withoutHeader: true,
        withoutFooter: true
      }))
      .when('/search', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'includes/search.html',
        controllerUrl: 'modules/includes/search',
        controller: 'searchCtrl',
        title: '搜索',
        withoutHeader: true
      }))
      .when('/home', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'home.html',
        controllerUrl: 'modules/home',
        controller: 'homeCtrl',
        title: '首页'
      }))
      .when('/cart', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/cart.html',
        controllerUrl: 'modules/checkout/cart',
        controller: 'CartCtrl',
        withoutFooter: true
      }))
      .when('/autopurchase', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/autopurchase.html',
        controllerUrl: 'modules/checkout/autopurchase',
        controller: 'AutoPurchaseCtrl',
        withoutFooter: true
      }))
      .when('/checkout', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/checkout.html',
        controllerUrl: 'modules/checkout/checkout',
        controller: 'CheckoutCtrl',
        withoutFooter: true
      }))
      .when('/loanorder', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/loanorder.html',
        controllerUrl: 'modules/checkout/loanorder',
        controller: 'LoanorderCtrl',
        withoutFooter: true
      }))
      .when('/checkout/address', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/address/list.html',
        controllerUrl: 'modules/checkout/address/address',
        controller: 'AddressListCtrl',
        title: '选择管理'
       }))
      .when('/payment/card', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'payment/cardlist.html',
        controllerUrl: 'modules/payment/payment',
        controller: 'CardListCtrl',
        title: '支付'
      }))
      .when('/payment/card/add', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'payment/addcard.html',
        controllerUrl: 'modules/payment/payment',
        controller: 'AddCardCtrl',
        title: '支付'
      }))
      .when('/vendors', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'vendor/vendor_list.html',
        controllerUrl: 'modules/vendor/vendor',
        controller: 'vendorsCtrl',
        title: '金牌店铺'
       }))
      .when('/vendor/:vendor_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'vendor/vendor.html',
        controllerUrl: 'modules/vendor/vendor',
        controller: 'vendorCtrl',
        title: '店铺介绍'
       }))
      .when('/vendor/:vendor_id/details', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'vendor/vendor_details.html',
        controllerUrl: 'modules/vendor/vendor',
        controller: 'vendorDetailsCtrl',
        title: '店铺介绍'
       }))
      .when('/orders', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/orderList.html?v=20160903',
        controllerUrl: 'modules/usercenter/order/order',
        controller: 'orderListCtrl',
        title: '全部订单'
       }))
	   .when('/periods', angularAMD.route({ //用户中心账期
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/period/periodList.html',
        controllerUrl: 'modules/usercenter/period/period',
        controller: 'periodListCtrl',
        title: '全部账期'
       }))
      .when('/order/:order_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/order.html',
        controllerUrl: 'modules/usercenter/order/order',
        controller: 'orderCtrl',
        title: '订单详情'
       }))
      .when('/order/:order_id/review/:order_product_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/review.html',
        controllerUrl: 'modules/usercenter/order/order',
        controller: 'reviewCtrl',
        title: '发表评价',
        withoutFooter: true
       }))
      .when('/pages/freesample', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/freesample.html',
        title: '免费拿样'
      }))
      .when('/pages/aboutus', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/aboutus.html',
        title: '关于百货栈'
      }))
      .when('/pages/point', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/point.html',
        title: '积分规则'
      }))
	  .when('/pages/caipiao', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/caipiao.html',
		controllerUrl: 'modules/pages/pages',
        controller: 'pageCtrl',
        title: '彩票',
		withoutFooter: true
      }))
	  .when('/pages/activity', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/activity.html',
		controllerUrl: 'modules/pages/pages',
        controller: 'pageCtrl',
        title: '活动'
      }))
	  .when('/pages/igoods', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/igoods.html',
		controllerUrl: 'modules/pages/pages',
        controller: 'pageCtrl',
        title: '智能配货宣传'
      }))
    .when('/pages/introduce', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/introduce.html',
        controllerUrl: 'modules/pages/introduce',
        controller: 'pageCtrl',
        title: '618中年红包宣传页'
      }))
	  .when('/knowledge/:knowledge_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'knowledge/knowledge.html',
        controllerUrl: 'modules/knowledge/knowledge',
        controller: 'knowledgeCtrl',
        title: '货架知识',
        withoutHeader: true,
        withoutFooter: true
       }))
	   .when('/analysis/:customer_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'analysis/analysis.html',
        controllerUrl: 'modules/analysis/analysis',
        controller: 'analysisCtrl',
        title: '业务分析',
        withoutHeader: true,
        withoutFooter: true
       }))
     .when('/shelfanalysis/:shelf_id/:solution_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'shelfanalysis/shelfanalysis.html',
        controllerUrl: 'modules/shelfanalysis/shelfanalysis',
        controller: 'shelfanalysisCtrl',
        title: '业务分析',
        withoutHeader: true,
        withoutFooter: true
       }))
     .when('/excel/:shelf_id/:solution_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'excel/excel.html',
        controllerUrl: 'modules/excel/excel',
        controller: 'excelCtrl',
        title: 'excel导出',
        withoutHeader: true,
        withoutFooter: true
       }))
      .otherwise({ redirectTo: '/home' })

      //$locationProvider.html5Mode(true);

  })

  return BHZRoute;
})
