'use strict';

define([
  'angular',
  'angularAMD'
], function(ng, angularAMD) {

  angularAMD.controller('knowledgeCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {

		$http.get("index2.php?route=rest/auto_purchase/getKnowCategory").then(
			function(result) {
				$scope.knowClasses = result.data.category;
			}
		);

		var temp = $routeParams.knowledge_id;
		var postData = temp.split("-");
		$scope.selectClass = postData[0];
		$scope.selectTab = postData[1];
		includeUrl($scope.selectClass,$scope.selectTab);
		
		//选分类
		$scope.switchClass = function(tab){
			$scope.selectClass = tab;
			includeUrl(tab,$scope.selectTab);
		}

		//选标签
		$scope.switchTab = function(tab){
			$scope.selectTab = tab;
			includeUrl($scope.selectClass,tab);
		}

		$scope.returnWx = function(){
			wx.miniProgram.redirectTo({
				url:'/pages/customer/ai/ai',
				success: function(){
					console.log('success')
				},
				fail: function(){
					console.log('fail');
				},
				complete:function(){
					console.log('complete');
				}
			});
		}
		
		//要展示的页面
		function includeUrl(choseClass,choseTab){
			$scope.includeUrl = "knowledge/"+choseClass+"/"+choseClass+"-"+choseTab+".html";
		}
      
    }
  ]);

});
