'use strict';

define([
  'angular',
  'angularAMD'
], function(ng, angularAMD) {

  angularAMD.controller('analysisCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {

		$scope.customer_id = $routeParams.customer_id;
		
		var trendId = 'myTrend';
		var nearId = 'myNear';
		var hotId = 'hotProducts';
		$http.get('index.php?route=wxapp/analysis/getAnalysis&customer_id='+$scope.customer_id).then(function(res){

			$scope.customerName = res.data.customer.name;
			$scope.productCount = res.data.customer.productCount;
			$scope.shelfCount = res.data.customer.shelfCount;
			$scope.profit = res.data.customer.profit;
			
			/*业绩走势图*/
			var tempTrend = res.data.trend;
			var trendX = [],trendData = [];
			for(var i in tempTrend){
				trendX.push(tempTrend[i].yearMonth.substring(5));
				var trendinfo = {};
				trendinfo.month = tempTrend[i].yearMonth;
				trendinfo.value = Number(tempTrend[i].total).toFixed(2);
				trendinfo.num = tempTrend[i].num;
				trendData.push(trendinfo);
			}
			getMyTrend(trendId,trendX,trendData);
			/*业绩走势图*/
			
			/*附近商铺*/
			var tempNear = res.data.near;
			var nearData = [];
			for(var i in tempNear){
				var nearinfo = {};
				nearinfo.name = tempNear[i].fullname;
				nearinfo.dist = Number(tempNear[i].dist).toFixed(1);
				nearinfo.value = Number(tempNear[i].total).toFixed(2);
				nearData.push(nearinfo);
			}
			getMyNear(nearId,nearData);
			/*附近商铺*/

			/*商品排行*/
			var tempHot = res.data.hot;
			var hotData = [];
			for(var i in tempHot){
				var hotinfo = {};
				hotinfo.name = tempHot[i].name;
				hotinfo.type = tempHot[i].type;
				hotinfo.value = tempHot[i].nub;
				hotData.push(hotinfo);
			}
			getHotProducts(hotId,hotData);
			/*商品排行*/

		});

		//业绩走势折线图
		function getMyTrend(id,x,data){
			
			//初始化
			var myTrend = echarts.init(document.getElementById(id),'shine');
			var option = {
				title: {
					text: '近12个月业绩走势',
					left: 'center',
					top: 15
				},
				tooltip: {
					trigger: 'axis',
					formatter:function(d){
						var month = d[0].data.month;
						var num = d[0].data.num;
						var value = d[0].data.value;
						var res = month+"月<br/>"+
							"采购额："+value+"元<br/>"+
							"订单数："+num;
						return res;
					}
				},
				xAxis: {
					type: 'category',
					axisLabel : {
						interval: 0, //解决X轴显示不全
						formatter: function(value)
						{
							return value+"月";
						}
					},
					data: x
				},
				yAxis: {
					type: 'value'
				},
				series: [{
					type: 'line',
					data: data		
				}]
			};
			myTrend.setOption(option);
		
		}

		//附近店铺比较柱状图
		function getMyNear(id,data){
			
			//初始化
			var myNear = echarts.init(document.getElementById(id),'shine');
			var option = {
				title: {
					text: '近3个月附近商铺业绩比较',
					left: 'center',
					top: 15
				},
				tooltip: {
					trigger: 'axis',
					position: function(point, params, dom, rect, size){
						//其中params为当前鼠标的位置
						return [params[0]-220,'10%'];
					 },
					axisPointer : {
						type : 'shadow'        
					},
					formatter:function(d){
						var name = d[0].data.name;
						var dist = d[0].data.dist;
						var value = d[0].data.value;
						var res = name+"的商铺<br/>"+
							"距离："+dist+"米内<br/>"+
							"近三月销售业绩："+value+"元";
						return res;
					}
				},
				xAxis: {
					type: 'value'
				},
				yAxis: {
					type: 'category',
					axisLabel : {
						interval: 0 //解决X轴显示不全
					},
					data: ['我的','商铺1','商铺2','商铺3','商铺4','商铺5','商铺6','商铺7']
				},
				series: [{
					type: 'bar',
					itemStyle:{
						normal:{
							color:'#ff6600'
						}
					},
					data: data		
				}]
			};
			myNear.setOption(option);
		
		}

		//商品排行柱状图
		function getHotProducts(id,data){
			
			//初始化
			var hotProducts = echarts.init(document.getElementById(id),'shine');
			var option = {
				title: {
					text: '商品排行TOP 10(近三月)',
					left: 'center',
					top: 15
				},
				tooltip: {
					trigger: 'axis',
					position: function(point, params, dom, rect, size){
						//其中params为当前鼠标的位置
						return [params[0]-220,'10%'];
					 },
					axisPointer : {
						type : 'shadow'        
					},
					formatter:function(d){
						var top = d[0].dataIndex+1;
						var type = d[0].data.type;
						var name = d[0].data.name;
						var value = d[0].data.value;
						var res ="排名："+top+"<br/>"+
							"品名："+type+"<br/>"+
							"商品名称："+name+"<br/>"+
							"购买量："+value;
						return res;
					}
				},
				yAxis: {
					type: 'value'
				},
				xAxis: {
					type: 'category',
					axisLabel : {
						interval: 0 //解决X轴显示不全
					},
					data: [1,2,3,4,5,6,7,8,9,10]
				},
				series: [{
					type: 'bar',
					itemStyle:{
						normal:{
							color:'#5bc0de'
						}
					},
					data: data	
				}]
			};
			hotProducts.setOption(option);
		
		}
		
		//底部菜单栏跳转
		$scope.returnWx = function(url){
			wx.miniProgram.redirectTo({
				url:url,
				success: function(){
					console.log('success')
				},
				fail: function(){
					console.log('fail');
				},
				complete:function(){
					console.log('complete');
				}
			});
		}

    }
  ]);

});

