'use strict';

define(['modules/app'], function(jfApp) {

  jfApp.controller('FooterCtrl', ['$scope', '$http', '$location', 
    function($scope, $http, $location) {
      $http.get('index.php?route=rest/account/managerCheck').then(function(result){
        $scope.managerCheck = result.data.managerCheck;
      });
  }])
});