'use strict';

define([
  'angular',
  'angularAMD'
], function(ng, angularAMD) {

  angularAMD.controller('shelfanalysisCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {

    $scope.shelf_id = $routeParams.shelf_id;
    $scope.solution_id = $routeParams.solution_id;
    var trendId = 'myTrend';
    var nearId = 'myNear';
    var hotId = 'hotProducts';
    $http.get('index.php?route=wxapp/shelfanalysis/shelfanalysis&shelf_id='+$scope.shelf_id+'&solution_id='+$scope.solution_id).then(function(res){

        var result = res.data;
         $scope.shelfname = res.data.info.shelfname;
         $scope.category = res.data.info.meta_title;
         $scope.price = res.data.info.price;
         $scope.num = res.data.info.num;
         var cunt = res.data.info.dan;
         var shu=[];
         for(var i=1;i<=cunt;i++){
            shu[i-1]=i;
         }



         var hotData = [];
          for(var i in result){
                if(i!=="info"){
                    var hotinfo = {};
                    hotinfo.name = result[i].name;
                    hotinfo.type = result[i].meta_title;
                    hotinfo.value = result[i].num;
                    hotData.push(hotinfo);
                }
          }
           getHotProducts(hotId,hotData,shu);

      // /*业绩走势图*/
      // var tempTrend = res.data.trend;
      // var trendX = [],trendData = [];
      // for(var i in tempTrend){
      //   trendX.push(tempTrend[i].yearMonth.substring(5));
      //   var trendinfo = {};
      //   trendinfo.month = tempTrend[i].yearMonth;
      //   trendinfo.value = Number(tempTrend[i].total).toFixed(2);
      //   trendinfo.num = tempTrend[i].num;
      //   trendData.push(trendinfo);
      // }
     //getMyTrend(trendId,trendX,trendData);
      /*业绩走势图*/

      /*附近商铺*/
      // var tempNear = res.data.near;
      // var nearData = [];
      // for(var i in tempNear){
      //   var nearinfo = {};
      //   nearinfo.name = tempNear[i].fullname;
      //   nearinfo.dist = Number(tempNear[i].dist).toFixed(1);
      //   nearinfo.value = Number(tempNear[i].total).toFixed(2);
      //   nearData.push(nearinfo);
      // }
      // getMyNear(nearId,nearData);
      /*附近商铺*/

      /*商品排行*/
      // var tempHot = res.data.hot;
      // var hotData = [];
      // for(var i in tempHot){
      //   var hotinfo = {};
      //   hotinfo.name = tempHot[i].name;
      //   hotinfo.type = tempHot[i].type;
      //   hotinfo.value = tempHot[i].nub;
      //   hotData.push(hotinfo);
      // }
      // getHotProducts(hotId,hotData);
      /*商品排行*/

    });



    //商品排行柱状图
    function getHotProducts(id,data,shu){

      //初始化
      var hotProducts = echarts.init(document.getElementById(id),'shine');
      var option = {
        title: {
          text: '进货商品排行',
          left: 'center',
          top: 15
        },
        tooltip: {
          trigger: 'axis',
          position: function(point, params, dom, rect, size){
            //其中params为当前鼠标的位置
            return [params[0]-220,'10%'];
           },
          axisPointer : {
            type : 'shadow'
          },
          formatter:function(d){
            var top = d[0].dataIndex+1;
            var type = d[0].data.type;
            var name = d[0].data.name;
            var value = d[0].data.value;
            var res ="排名："+top+"<br/>"+
              "品名："+type+"<br/>"+
              "商品名称："+name+"<br/>"+
              "购买量："+value;
            return res;
          }
        },
        yAxis: {
          type: 'value'
        },
        xAxis: {
          type: 'category',
          axisLabel : {
            interval: 0 //解决X轴显示不全
          },
          data: shu
        },
        series: [{
          type: 'bar',
          itemStyle:{
            normal:{
              color:'#5bc0de'
            }
          },
          data: data
        }]
      };
      hotProducts.setOption(option);

    }

    //底部菜单栏跳转
    $scope.returnWx = function(url){
      wx.miniProgram.redirectTo({
        url:url,
        success: function(){
          console.log('success')
        },
        fail: function(){
          console.log('fail');
        },
        complete:function(){
          console.log('complete');
        }
      });
    }

    }
  ]);

});

