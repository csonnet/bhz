'use strict';

define([
  'angular',
  'angularAMD',
  'angular-route',
  'angular-animate',
  'angular-cookies',
  'angular-touch',
  'ng-dialog',
  'modules/route'
], function(ng, angularAMD) {

  var app = ng.module('BHZApp', ['ngRoute', 'ngAnimate', 'ngCookies', 'ngTouch', 'ngDialog', 'BHZRoute']);

  app.constant("BHZConfig", {
    MODULE_PATH: 'catalog/view/theme/bhz/m/modules/'
  });

  app.config(function($provide, $httpProvider) {

    // Intercept http calls.
    $provide.factory('MyHttpInterceptor', function($q) {
      return {
        // On request success
        request: function(config) {
          //console.log(config); // Contains the data about the request before it is sent.
          // Return the config or wrap it in a promise if blank.
          return config || $q.when(config);
        },

        // On request failure
        requestError: function(rejection) {
          // console.log(rejection); // Contains the data about the error on the request.
          // Return the promise rejection.
          return $q.reject(rejection);
        },

        // On response success
        response: function(response) {
          // console.log(response); // Contains the data from the response.
          if (response.status === 401 || response.error && response.error.status === 401) {
            BHZ.MessageBox.show("正在跳转登录...");
            setTimeout(function() {
              window.location.hash = '#/login';
              // window.location.hash = '#/login?redirect_url=' + encodeURIComponent(window.location.href);
            }, 1000);
            return $q.reject(response);
          }

          //show the error message when like get simple data
          if (response.data.success == false || response.data.error) {
            var error_msg = typeof response.data.error == 'object' ? response.data.error.warning : response.data.error;
            BHZ.MessageDialog.show(error_msg, "知道了");
            return $q.reject(response);
          }
          // Return the response or promise.
          return response || $q.when(response);
        },

        // On response failture
        responseError: function(rejection) {
          console.log(rejection); // Contains the data about the error.
          if (rejection.status === 401 || rejection.error && rejection.error.status === 401) {
            BHZ.MessageBox.show("正在跳转登陆...");
            setTimeout(function() {
              window.location.hash = '#/login';
              // window.location.hash = '#/login?redirect_url=' + encodeURIComponent(window.location.href);
            }, 1000);
            //window.location.href = window.M_SERVER + '/login?redirect_url=' + encodeURIComponent(window.location.href);
          }
          // Return the promise rejection.
          return $q.reject(rejection);
        }
      };
    });

    // Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push('MyHttpInterceptor');

  });

  app.run(
    ['$rootScope', '$location', '$cookies', '$http', '$window',
      function($rootScope, $location, $cookies, $http, $window) {

        $rootScope.baseURI = document.baseURI;
        $rootScope.isWeixin = window.isWeixin;
        $rootScope.$on("$routeChangeSuccess", function(event, current, previous) {
          if (current) {
            //set page title
            $rootScope.page_title = current.title || '百货栈';
            $rootScope.withoutHeader = current.withoutHeader;
            $rootScope.withoutFooter = current.withoutFooter;
          }
          $window.scrollTo(0,0);
          $(".cover").hide();

          $rootScope.footerUrl = $location.url();
        });

        $rootScope.jump = function(path,withoutParams){
          if(withoutParams){
            $location.url(path);
          }else{
            $location.path(path);
          }
        };
        
        $rootScope.bhz_user = $cookies.getObject('bhz_user');

        $rootScope.logout = function(){
          $http.post('index.php?route=rest/logout/logout')
          .then(function(result){
            if(result.data.success){
              BHZ.MessageBox.show("退出成功");
              $location.path('/home');
            }
          })
        }

        $rootScope.gotoProduct = function(productId){
          $location.path('/product/'+productId);
        }

      }
    ]);

  return app;

});
