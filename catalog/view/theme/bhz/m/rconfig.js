requirejs.config({
  waitSeconds: 0,
  baseUrl: "catalog/view/theme/bhz",
  paths: {
    'jquery': 'vendor/js/jquery-2.1.4.min',
    'text': 'vendor/js/text',
    'json': 'vendor/js/json',
    'angular': 'vendor/js/angular.min',
    'angularAMD': 'vendor/js/angularAMD.min',
    'ngload':'vendor/js/ngload.min',
    'angular-route': 'vendor/js/angular-route.min',
    'angular-cookies': 'vendor/js/angular-cookies',
    'angular-animate': 'vendor/js/angular-animate.min',
    'angular-touch': 'vendor/js/angular-touch.min',
    'angular-carousel': 'vendor/js/angular-carousel.min',
    'ng-dialog': 'vendor/js/ngDialog',
    'spin': 'vendor/js/spin.min',
    'ng-file-upload': 'vendor/js/ng-file-upload.min',
    'jquery-easing':'vendor/js/jquery.easing',
    'jquery-timeline':'vendor/js/jquery.timeline',
    'jquery-cookie':'vendor/js/jquery.cookie',
    'common': 'common',
    'data': 'data',
    'modules': 'm/modules'
  },
  shim: {
    'angular': {
      'exports': 'angular',
      'deps': ['jquery']
    },
    'angularAMD': ['angular'],
    'angular-route': ['angular'],
    'angular-cookies': ['angular'],
    'angular-animate': ['angular','angular-route'],
    'angular-touch': ['angular'],
    'angular-carousel': ['angular-touch'],
    'ng-dialog': ['angular'],
    'ng-file-upload':['angular'],
    'angularAMD': ['angular'],
    'ngload':['angularAMD'],
    'jquery-cookie': ['jquery']
  },
});