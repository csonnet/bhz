<!DOCTYPE html>
<html lang="zh">
<!--<![endif]-->
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
  <meta name="description" content="">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="format-detection" content="telephone=no" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <title>百货栈</title>

  <!-- <base href="/bhz/" /> -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="" rel="icon" />
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/swiper.min.css">  
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/ngDialog.min.css"> 
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/vendor/css/ngDialog-theme-default.min.css"> 
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/pc/css/main.css?v=20170517">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/bhz/pc/css/common.css">
  <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/swiper.min.js"></script>
  <script src="catalog/view/theme/bhz/pc/libs/plugins.js" type="text/javascript"></script> 
  
</head>
  <body class="bhz-pc">
    <!--[if lt IE 8]>
      <p class="browserupgrade">你正在使用一个 <strong>过时的</strong> 浏览器. 请 <a href="http://browsehappy.com/">升级你的浏览器</a> 来获得更佳的体验.</p>
    <![endif]-->
    <a name="top"></a>
    <div ng-include="'catalog/view/theme/bhz/pc/modules/includes/header.html'" ng-if="!withoutHeader"></div>

    <div class="content-wrapper">
      <div ng-view></div>
    </div>

    <div ng-include="'catalog/view/theme/bhz/pc/modules/includes/footer.html'" ng-if="!withoutFooter"></div>
  
    <!--[if lt IE 9]>
      <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/es5-shim.min.js"></script>
      <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/json3.min.js"></script>
      <script type="text/javascript" src="catalog/view/theme/bhz/vendor/js/html5shiv.js"></script>
    <![endif]-->

    <script type="text/javascript" type="text/javascript" src="catalog/view/theme/bhz/vendor/js/selectivizr.js"></script>
    <!--[if (gte IE 6)&(lte IE 8)]>
      <script type="text/javascript" type="text/javascript" src="catalog/view/theme/bhz/vendor/js/requestAnimationFrame.js"></script>
    <![endif]-->
    <script src="catalog/view/theme/bhz/vendor/js/require.js"  type="text/javascript"></script>
    <script type="text/javascript">
      require(['catalog/view/theme/bhz/pc/rconfig'], function(){
        require.config({
          urlArgs: "v=20170828"
        });
        require(['angularAMD', 'modules-pc/app', 'modules-pc/includes'], function(angularAMD, BHZApp){
          angularAMD.bootstrap(BHZApp);
        });
      })
    </script>

	<script>
		var _hmt = _hmt || [];
		(function() {
		  var hm = document.createElement("script");
		  hm.src = "https://hm.baidu.com/hm.js?786fffd914d8cb5c19f781e4ac59866c";
		  var s = document.getElementsByTagName("script")[0]; 
		  s.parentNode.insertBefore(hm, s);
		})();
	</script>

</body>
</html>




