'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('PagesCtrl', ['$scope', '$rootScope', '$location',
    function($scope, $rootScope, $location) {

      $scope.navs = [{
        title: '采购指南',
        url: '/page/purchase'
      }, {
        title: '配送说明',
        url: '/page/delivery'
      }, {
        title: '在线支付',
        url: '/page/payment'
      }, {
        title: '积分规则',
        url: '/page/point'
      }, {
        title: '优惠券',
        url: '/page/coupon'
      }, {
        title: '退换货',
        url: '/page/return'
      }, {
        title: '商家入驻',
        url: '/page/vendor'
      }, {
        title: '免费拿样',
        url: '/page/freesample'
      }, {
        title: '关于百货栈',
        url: '/page/about'
      }, {
        title: '联系我们',
        url: '/page/contact'
      }]

      $scope.pageUrl = $location.path();

      $scope.currentPage = getCurrentPage($scope.pageUrl);

      function getCurrentPage(url) {
        for(var i = 0, l = $scope.navs.length; i < l; i++){
          var nav = $scope.navs[i];
          if(nav.url == url){
            return nav;
          }
        }
      }
    }

  ]);

});
