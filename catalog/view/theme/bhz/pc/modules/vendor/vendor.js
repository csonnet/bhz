'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('vendorsCtrl', ['$scope', '$http', '$location',
    function($scope, $http, $location) {

      $http.get("index.php?route=feed/rest_api/vendors").then(
          function(result) {
            if (result.data.success) {
              $scope.vendorList = result.data.data;
              console.log($scope.vendorList);
            }
          }
      )

      $scope.gotoVendor = function(vendor_id){
        $location.url("/vendor/"+vendor_id);
      }

    }
  ]);

  angularAMD.controller('vendorCtrl', ['$scope', '$routeParams', '$sce', '$http', '$location',
    function($scope, $routeParams, $sce, $http, $location) {
      var vendorId = $routeParams.vendor_id;
      if(!vendorId){
        alert("缺少品牌id..");return;
      }
      
      $http.get("index.php?route=feed/rest_api/vendors&id="+vendorId).then(
          function(result) {
            if (result.data.success) {
              $scope.vendorInfo = result.data.data;
              $scope.vendorInfo.default_banner = "catalog/view/theme/bhz/pc/images/vendor_banner.png";
              $scope.vendorInfo.vendor_description = $sce.trustAsHtml($scope.vendorInfo.vendor_description);
            }
          }
      )

      $http.get("index.php?route=feed/rest_api/products&vendor="+vendorId).then(
          function(result) {
            if (result.data.success) {
              $scope.vendorProducts = result.data.data;
            }
          }
      )

      $scope.wishlist = function(){
        if($scope.vendorInfo.is_in_wishlist){
          $http.delete('index.php?route=rest/wishlist/wishlist&manu_id=' + vendorId).then(function(result){
            if(result.data.success){
              $scope.vendorInfo.is_in_wishlist =  false;
              BHZ.MessageBox.show("取消关注成功！");
            }
          })
        }else{
          $http.post('index.php?route=rest/wishlist/wishlist&manu_id=' + vendorId).then(function(result){
            if(result.data.success){
              $scope.vendorInfo.is_in_wishlist =  true;
              BHZ.MessageBox.show("关注成功");
            }
          })
        }
      }

      $scope.gotoVendorDetails = function(){
        $location.url("/vendor/"+vendorId+"/details");
      }

    }
  ]);

  angularAMD.controller('vendorDetailsCtrl', ['$routeParams', '$scope', '$sce', '$http', '$location', '$timeout', '$window',
    function($routeParams, $scope, $sce, $http, $location, $timeout, $window) {
      var vendorId = $routeParams.vendor_id;
      if(!vendorId){
        alert("缺少品牌id..");return;
      }

      $http.get("index.php?route=feed/rest_api/vendors&id="+vendorId).then(
          function(result) {
            if (result.data.success) {
              $scope.vendorInfo = result.data.data;
              $scope.vendorInfo.vendor_description = $sce.trustAsHtml($scope.vendorInfo.vendor_description);
            }
          }
      )
      
    }
  ]);


});
