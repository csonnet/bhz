'use strict';

define(['modules-pc/app'], function(jfApp) {

  jfApp.controller('CategoryCtrl', ['$scope', '$http', '$location', 
    function($scope, $http, $location) {
      var url = $location.url();
      if(url.indexOf("home") == -1){
        $scope.closeCategoryList = true; 
      }

      $scope.currCategoryId = $location.search().category;

      $scope.gotoProducts = function(category_id){
        $location.url('/products').search({category:category_id});
      }
      
    	$http.get("index.php?route=feed/rest_api/categories&parent=0&level=3").then(
          function(result) {
            if (result.data.success) {
              $scope.categories = result.data.data;
              var categoriesObj = {};
              if($scope.categories && $scope.categories.length > 0){
                angular.forEach($scope.categories, function(value, key) {
                  categoriesObj[value.category_id] = value;
                })
              }
              $scope._categories = categoriesObj;
            }
          }
      ) 
  }])

});