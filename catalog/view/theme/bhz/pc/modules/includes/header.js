'use strict';

define(['modules-pc/app'], function(jfApp) {

  jfApp.controller('HeaderCtrl', ['$scope', '$location', '$cookieStore', '$http',
    function($scope, $location, $cookieStore, $http) {
    	$scope.params = {
    		search:""
    	};

      $scope.showMQRFlag = false;

      var searchObj = $location.search();
      $scope.params = {
        search : searchObj.search ? decodeURIComponent(searchObj.search) : ''
      }

      var url = $location.url();
      if(url.indexOf("home") != -1){
          $scope.topbanner = true; 
      }

    	$scope.recentSearch = $cookieStore.get("recentSearch") ? $cookieStore.get("recentSearch").split(",") : [];

    	$scope.search = function($event){
        if ($event) {
          var keyCode = $event.which || $event.keyCode;
          //只有在键盘敲入为Enter(value = 13)时主动去查询商品
          if (keyCode !== 13) {
            return;
          }
        }
    		if($scope.params.search){
            if($scope.recentSearch.indexOf($scope.params.search) == -1){
                $scope.recentSearch.push($scope.params.search);
                $cookieStore.put("recentSearch",$scope.recentSearch.toString())
            }
        		$location.search({"search":encodeURIComponent($scope.params.search)});
        }else{
          $location.search({});
        }
        $location.path("/products");
    	}

      $scope.showMQR = function(){
        if($("#MQR").length>0){
          $scope.showMQRFlag = !$scope.showMQRFlag;
        }
      }

	  //未读数量
		$http.get("index.php?route=message/message/noReadCount").then(function(result){
			
			if(result.data.success){
				$scope.noReadCount = result.data.noReadCount;
			}

		})

      var topSlider,initSwiperFlag;
      if($scope.topbanner){
        $http.get("index.php?route=feed/rest_api/pc_homepage").then(
            function(result) {
              if (result.data.success !== false) {
                $scope.home = result.data;
              }
            }
          )
      }

      $scope.initSwiper = function() {
          // if(!initSwiperFlag){
          //   initSwiperFlag = true;
          //   $timeout(function() {
          //     topSlider = new Swiper('#TopSlider', {
          //       loop: true,
          //       spaceBetween: 0,
          //       pagination: '#TopSwiperPagination',
          //       paginationClickable: true,
          //       autoplay: 4000
          //     });
          //   })
          // }
          console.log();
      }
  }]);

  // angularAMD.directive('onNgRepeatEnd', function ($timeout) {
  //   return {
  //     restrict: 'A',
  //     link: function($scope, element, attr) {
  //       if ($scope.$last == true) {
  //         $timeout(function() {
  //           $scope.$eval(attr.onNgRepeatEnd);
  //         });
  //       }
  //     }
  //   };
  // });

});