'use strict';

define(['modules-pc/app'], function(jfApp) {

  jfApp.controller('UserNavCtrl', ['$scope', '$http', '$location', 
    function($scope, $http, $location) {
      $scope.currUrl = $location.path();
  }])

});