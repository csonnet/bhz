'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
], function(ng, angularAMD) {

	angularAMD.controller('MessageCtrl', ['$scope', '$http', '$cookies', '$location', '$routeParams', 'ngDialog',
		function($scope, $http, $cookies, $location, $routeParams, ngDialog) {
		
			var limit = 10000;

			getMyMessage();

			//获取我的消息
			function getMyMessage(){
					
				var baseUrl = "index.php?route=message/message/getMyMessage&limit="+limit;

				$http.get(baseUrl).then(function(result){

					if(result.data.success){
						$scope.myMessages = result.data.messages;
					}

				})

			}
			
			//查看详细消息
			$scope.showInfo = function(sendId){

				var options = {
					templateUrl: 'catalog/view/theme/bhz/pc/modules/usercenter/message/info.html',
					controller: 'MessageInfoCtrl',
					className: 'ngdialog-theme-default jf-dialog',
					cache: false,
					data:{
						id:sendId
					}
				};

				ngDialog.open(options).closePromise.then(function(dialogClose){
					getMyMessage();
				});

			}	

		}
	]);

	angularAMD.controller('MessageInfoCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', '$routeParams',
		function($scope, $rootScope, $http, $cookies, $location) {
			
			$scope.init = function(){
				
				if ($scope.ngDialogData.id){

					$http.get("index.php?route=message/message/getMessageInfo&id="+$scope.ngDialogData.id).then(function(result){
						if(result.data.success){
							$scope.minfo = result.data.messageInfo;
						}
					});

					hasRead($scope.ngDialogData.id);

				}

			}
			
			//已读
			function hasRead(id){

				var postdata = {};
				postdata.id = id;

				$http.post('index.php?route=message/message/hasRead',postdata)
				.then(function(result){
				});

			}

		}
	]);

});
