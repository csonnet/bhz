'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies',
  'json!data/china_address.json'
], function(ng, angularAMD, cooki, address_json) {

  angularAMD.controller('AddressListCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', '$routeParams', 'ngDialog',
    function($scope, $rootScope, $http, $cookies, $location, $routeParams, ngDialog) {

        function reload(){
          $http.get('index.php?route=rest/account/address').then(
            function(result){
              $scope.addressList = result.data.data.addresses;
            }
          )
        }

        $scope.updateAddress = function(address_id){
          var options = {
            templateUrl: 'catalog/view/theme/bhz/pc/modules/usercenter/address/address.html',
            controller: 'AddressCtrl',
            className: 'ngdialog-theme-default jf-dialog',
            cache: false,
            data:{
              address_id:address_id
            }
          };
          ngDialog.open(options).closePromise.then(function(dialogClose){
            console.log(dialogClose);
            if(dialogClose.value && dialogClose.value.refreshList){
              reload();
            }
          });
        }

        $scope.deleteAddress = function(id){
          ngDialog.openConfirm({
            template: 'confirmDeleteDialog.html',
            className: 'ngdialog-theme-default'
          }).then(function(value) {
            $http.delete('index.php?route=rest/account/address/address&id='+id).then(
              function(result){
                if(result.data.success){
                  BHZ.MessageBox.show("删除成功");
                  reload();
                }
              }
            )
          }, function(value) {
            console.log('cancel')
          });
        }

        reload();
    }
  ])



  angularAMD.controller('AddressCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', '$routeParams',
    function($scope, $rootScope, $http, $cookies, $location) {

      $scope.address = {};

      $scope.address_json = address_json;

      $scope.is_default = false;

      $scope.init = function(){
        if ($scope.ngDialogData.address_id) {
          $http.get('index.php?route=rest/account/address/address&id=' + $scope.ngDialogData.address_id).then(
            function(result){
              if (result.data.success) {
                $scope.address = result.data.data;
                //country = province
                //zone = city
                //city = area
                $scope.address.default = $scope.address.is_default == '1'? true: false;
                $scope.is_default = $scope.address.is_default == '1'? true: false;
                
                $scope.address.origin_country_id = $scope.address.country_id;
                $scope.address.origin_zone_id = $scope.address.zone_id;
                $scope.address.origin_city_id = $scope.address.city_id;
                
                $scope.address.country_id = $scope.address.origin_country_id;
                delete $scope.address.origin_country_id;
                $scope.setProvince();
              }
            }
          )
        }else{
          $scope.address.country_id = "1";
          $scope.setProvince();
        }
      }

      $scope.setProvince = function() {
        var province_id = $scope.address.country_id;
        for (var i = 0, l = $scope.address_json.length; i < l; i++) {
          if (province_id == $scope.address_json[i].id) {
            $scope.selectedProvince = $scope.address_json[i];
            break;
          }
        }

        if($scope.address.origin_zone_id){
          $scope.address.zone_id = $scope.address.origin_zone_id;
          delete $scope.address.origin_zone_id;
        }else{
          $scope.address.zone_id = "";
        }

        $scope.setCity();
      };

      $scope.setCity = function() {
        var zone_id = $scope.address.zone_id;
        for (var i = 0, l = $scope.selectedProvince.zone.length; i < l; i++) {
          if (zone_id == $scope.selectedProvince.zone[i].id) {
            $scope.selectedCity = $scope.selectedProvince.zone[i];
            break;
          }
        }

        if($scope.address.origin_city_id){
          $scope.address.city_id = $scope.address.origin_city_id;
          delete $scope.address.origin_city_id;
        }else{
          $scope.address.city_id = "";
        }
        
      }

      $scope.submit = function() {
        // console.log($scope.address);
        $scope.address.default = $scope.address.default? '1': '0';
        if($scope.address.address_id){
          //edit
          $http.put('index.php?route=rest/account/address'+"&id="+$scope.address.address_id, $scope.address).then(
            function(result) {
              if (result.data.success) {
                $scope.closeThisDialog({"refreshList":true});
              }
            }
          )
        }else{
          $http.post('index.php?route=rest/account/address', $scope.address).then(
            function(result) {
              if (result.data.success) {
                $scope.closeThisDialog({"refreshList":true});
              }
            }
          )
        }
      }
    }
  ]);




});
