'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

	angularAMD.controller('BalanceCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
		function($scope, $rootScope, $http, $cookies, $location ){
			
			var searchObj = $location.search();
			$scope.selection={
				"tab": searchObj.status ? searchObj.status : '充值历史'
			}
			$scope.switchTab = function(status){
				$scope.selection.tab = status;
			}

			getRecharge();
			getDeposit();
			getTrade();
			
			//获取充值记录
			function getRecharge(){

				$http.get('index.php?route=balance/balance/getRecharge').then(function(result){

					if(result.data.success){	
						$scope.customerBalance = result.data.customerBalance;
						$scope.rechargeOrder = result.data.rechargeOrder;
					}
				})

			}

			//获取定金记录
			function getDeposit(){

				$http.get('index.php?route=balance/balance/getDeposit').then(function(result){

					if(result.data.success){	
						$scope.depositOrder = result.data.depositOrder;
					}
				})

			}
			
			//获取交易记录
			function getTrade(){

				$http.get('index.php?route=balance/balance/getTrade').then(function(result){

					if(result.data.success){	
						$scope.tradeOrder = result.data.tradeOrder;
					}
				})

			}
			
			/*作废充值*/
			$scope.destoryRecharge = function(rechargeid){
				if(confirm("确认要作废吗?")){
					var postdata = {};
					postdata.rechargeid = rechargeid;

					$http.post('index.php?route=balance/recharge/destoryRecharge',postdata)
					.then(function(result){

						if (result.data && result.data.success) {

							BHZ.MessageDialog.show("充值单据已经作废");
							getRecharge();
							getDeposit();

						}

					})
				}
			}
			/*作废充值*/

			/*再次充值*/
			$scope.rechargeAgain = function(id,code){	

				var postdata = {};
				postdata.recharge_id = id;
				postdata.payment_code = code;
				
				$http.post('index.php?route=balance/recharge/rechargeAgain', postdata)
				.then(function(result) {

					if(result.data && result.data.success) {

						if(result.data.data.redirect){
							window.location.href = result.data.data.redirect;  
						}
						else{
							BHZ.MessageDialog.show("百货栈暂不支持该支付方式");
							return;               
						}

					}else{
						$('body').html(result.data);
					}

				})

			}
			/*再次充值*/

		}
	]);
	
	angularAMD.controller('RechargeCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 
		function($scope, $rootScope, $http, $cookies, $location ){
			$http.get('index.php?route=rest/account/account').then(function(result){
				if(result.data.success){

					$scope.customerInfo = result.data.data;

					if($location.$$path == '/usercenter/deposit'){
						$scope.customerInfo.money = 1000;
						$scope.customerInfo.balance_type = 2;
					}
					else{
						$scope.customerInfo.balance_type = 1;
					}

				}
			})

			ListMethodData();
			
			/*遍历支付方式*/
			function ListMethodData() {

				$http.get('index.php?route=balance/recharge/getMethod').then(function(result) {
					if (result.data && result.data.success) {

						$scope.checkoutData = result.data

						//支付信息
						$scope.payment_info = $scope.checkoutData.payment_info;

					}
				})

			}
			/*遍历支付方式*/
			
			/*选中支付方式*/
			$scope.selectPayment = function(payment){

				$scope.selected_payment = payment;

			}
			/*选中支付方式*/
			
			/*充值第一步*/
			$scope.gotoRecharge = function() {

				var submitData = {};
				
				if(!$scope.customerInfo.telephone){
					BHZ.MessageDialog.show("请输入账号或手机号");
					return;
				}
				if(!$scope.customerInfo.money){
					BHZ.MessageDialog.show("请输入充值金额");
					return;
				}
				if(!$scope.selected_payment){
					BHZ.MessageDialog.show("请选择支付方式");
					return;
				}
				
				submitData.telephone = $scope.customerInfo.telephone;
				submitData.money = $scope.customerInfo.money;
				submitData.payment_code = $scope.selected_payment.code;
				submitData.payment_method = $scope.selected_payment.title;
				submitData.balance_type = $scope.customerInfo.balance_type;

				$http.post('index.php?route=balance/recharge/checkout', submitData)
				.then(function(result){	

					if (result.data && result.data.success) {

						$scope.confirm = result.data.rechargeInfo;
						$scope.showConfirm();

					}

				})

			}
			/*充值第一步*/
			
			/*充值第二步*/
			$scope.confirmRecharge = function(){
				
				$http.post('index.php?route=balance/recharge/confirmRecharge', $scope.confirm)
				.then(function(result) {

					if(result.data && result.data.success) {

						if(result.data.data.redirect){
							window.location.href = result.data.data.redirect;  
						}
						else{
							BHZ.MessageDialog.show("百货栈暂不支持该支付方式");
							return;               
						}

					}else{
						$('body').html(result.data);
					}

				})

			}
			/*充值第二步*/
			
			//点击同意条款
			$scope.checkAgree = function(){
				
				if($('.bhz-checkbox').hasClass('on')){
					$('.bhz-checkbox').removeClass('on');
					$('.agree-btn').addClass('btn_disabled');
					$('.agree-btn').attr('disabled',true);
				}
				else {
					$('.bhz-checkbox').addClass('on');
					$('.agree-btn').removeClass('btn_disabled');
					$('.agree-btn').attr('disabled',false);
				}

			}
			
			//弹出同意条款
			$scope.show_mubu = function(){
				$(".pc_xy").fadeIn();
			}
			
			//关闭同意条款
            $scope.hideMubu = function(){
				$(".pc_xy").fadeOut();
			}

			//弹出确认充值
			$scope.showConfirm = function(){
				$(".confirm_div").fadeIn();
			}

			//关闭确认充值
			$scope.hideConfirm = function(){
				$(".confirm_div").fadeOut();
			}

		}
	]);

	angularAMD.controller('BalanceReturnCtrl', ['$scope',  '$http', '$location',
		function($scope,  $http, $location) {
      
        
		}
	]);

});