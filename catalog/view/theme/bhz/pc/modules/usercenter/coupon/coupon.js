'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('CouponCtrl', ['$scope', '$rootScope', '$http',
    function($scope, $rootScope, $http) {

      var coupons = [];

      $http.get('index.php?route=rest/account/coupon').then(function(result) {
        if (result.data.success) {
          $scope.coupons = result.data.data;
        }
      })
    }
  ]);



});
