  'use strict';

define([
  'angular',
  'angularAMD',
  'ng-file-upload',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('ProfileCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$location', 'Upload', 'ngDialog', 
    function($scope, $rootScope, $http, $cookies, $location, Upload , ngDialog ) {

      var defaultAvatar= "catalog/view/theme/bhz/m/images/default-avarta.png";
      $scope.requiredOnly = false;
      var license_code = new Array();
      var shop_code = new Array();

      $http.get('index.php?route=rest/account/account').then(function(result){
        if(result.data.success){
          $scope.profile = result.data.data;
          $scope.profile.avatar = $scope.profile.avatar || defaultAvatar;
          $scope.profile.code="123";
          //$scope.profile.license_image=result.data.data.license_images[0].img_url;
          console.log($scope.profile);
          //处理license
          if($scope.profile.license_images && $scope.profile.license_images.length > 0){
            license_code.push($scope.profile.license_images[0].code);
          }
          //处理shopImage
          if($scope.profile.shop_images && $scope.profile.shop_images.length > 0){
            angular.forEach($scope.profile.shop_images,function(shopImg){
              shop_code.push(shopImg.code);
            })
          }else{
            $scope.profile.shop_images = new Array();
          }
        }
      }) 

      $scope.managerAddress = function(){
        $location.path("/usercenter/address");
      }

      $scope.changeLiscenceImage = function(file,type){ 
         if(file&&file.length>0){
            if (type=='license') {
              var dialog = ngDialog.open({
                template: 'loading.html',
                className: 'ngdialog-theme-default',
                showClose: false,
                closeByEscape: false,
                closeByDocument: false,
                closeByNavigation: false
              });
              Upload.upload({
                url: 'index.php?route=feed/rest_api/upload',
                file: file[0]
              }).then(function(response){
                  $scope.profile.license_images = new Array();
                  $scope.profile.license_images.push(response.data);
                  license_code = new Array();
                  license_code.push(response.data.code);
                  setTimeout(function(){dialog.close();},500);
              },function(error){ 
                  setTimeout(function(){dialog.close();},500);
              });
            }else if (type=='shop') {
               if(file.length>10 || (file.length + $scope.profile.shop_images.length) > 10){
                  BHZ.MessageDialog.show("店铺实景图不能超过10张");
                  return;
               }
               var dialog = ngDialog.open({
                template: 'loading.html',
                className: 'ngdialog-theme-default',
                showClose: false,
                closeByEscape: false,
                closeByDocument: false,
                closeByNavigation: false
              });
              angular.forEach(file,function(fileItem,key){
                Upload.upload({
                  url: 'index.php?route=feed/rest_api/upload',
                  file: fileItem
                }).then(function(response){
                  $scope.profile.shop_images.push(response.data);
                  shop_code.push(response.data.code);
                  if(key == file.length - 1){
                    setTimeout(function(){dialog.close();},1000);
                  }
                },function(error){
                  setTimeout(function(){dialog.close();},1000);
                })
              })
            } 
         }
      };    

       $scope.clearAllShopImages = function(){
        ngDialog.openConfirm({
          template: 'confirmDeleteDialog.html',
          className: 'ngdialog-theme-default'
        }).then(function(value) {
          $scope.profile.shop_images = new Array();
          shop_code = new Array();
        }, function(value) {
          console.log('cancel')
        });
      }

      $scope.submit = function(){
          if(!$scope.profile.fullname){
            BHZ.MessageDialog.show("请输入联系人姓名");
            return;
          }
          if(!$scope.profile.company_name){
            BHZ.MessageDialog.show("请输入公司名称");
            return;
          }
          if(!$scope.profile.shop_type){
            BHZ.MessageDialog.show("请选择终端类型");
            return;
          }
          if(license_code.length<1){
            BHZ.MessageDialog.show("请上传营业执照照片");
            return;
          }else{
            $scope.profile.license_code = license_code; 
          }
          if(shop_code.length<1 || shop_code.length>10){
            //BHZ.MessageDialog.show("请上传店铺实景图照片，至少一张，最多十张");
            //return;
          }else{
            $scope.profile.shop_code = shop_code; 
          } 

          BHZ.MessageDialog.show("您的个人信息修改后，您将处于审核状态，此段时间内您将无法下单，我们会尽快完成您的修改请求。如需紧急处理，请联系400-099-6388");
          $http.post('index.php?route=rest/account/account', $scope.profile)
            .then(function(result){
              if(result.data.success){
                BHZ.MessageBox.show("保存成功");
                $location.path('/usercenter');
              }
          })
      }
    }
  ]);

});
