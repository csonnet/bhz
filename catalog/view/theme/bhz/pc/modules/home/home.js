'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('homeCtrl', ['$scope', '$http', '$location', '$timeout',
    function($scope, $http, $location, $timeout) {

      var mainSlider,adBanner1,initSwiperFlag;

		//����ĳ���ֶ�����
		function compare(property){
			return function(a,b){
				var value1 = a[property];
				var value2 = b[property];
				return value1 - value2;
			}
		}

      $http.get("index.php?route=feed/rest_api/pc_homepage").then(
          function(result) {
            if (result.data.success !== false) {

				$scope.home = result.data;

				$scope.sortList = result.data.products;
				$scope.sortList.sort(compare('class_sort'));

            }
          }
        )

      $scope.initSwiper = function() {
        if(!initSwiperFlag){
          initSwiperFlag = true;
          $timeout(function() {
            mainSlider = new Swiper('#MainSlider', {
              loop: true,
              spaceBetween: 0,
              pagination: '#MainSwiperPagination',
              paginationClickable: true,
              autoplay: 4000
            });
            adBanner1 = new Swiper('#AdSlider', {
              spaceBetween: 0,
              autoplay: 4000
            });
          })
        }
      }

      $scope.gotoCategory = function(category_id){
        if(category_id){
          $location.url('/categories').search({category_id:category_id});
        }
      }

      $scope.gotoProducts = function(category_id){
        if(category_id){
          $location.url('/products').search({category:category_id});
        }
      }
    
      $scope.initCartByHistoryAtHomePage = function(){
        $http.get('index.php?route=rest/cart/initCartByHistory').then(function(result){
          if (result.data && result.data.success){
            $location.path("/cart");
          }
        })
      }

    }
  ]);

  angularAMD.directive('onNgRepeatEnd', function ($timeout) {
    return {
      restrict: 'A',
      link: function($scope, element, attr) {
        if ($scope.$last == true) {
          $timeout(function() {
            $scope.$eval(attr.onNgRepeatEnd);
          });
        }
      }
    };
  });



});
