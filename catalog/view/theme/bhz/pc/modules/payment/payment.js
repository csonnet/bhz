'use strict';

define([
  'angular',
  'angularAMD',
  'angular-cookies'
], function(ng, angularAMD) {

  angularAMD.controller('CardListCtrl', ['$scope', 'ngDialog', '$http', '$cookies', '$location',
    function($scope, ngDialog, $http, $cookies, $location) {

      $scope.selectedCard;
      $scope.validCode;

      $http.get('index.php?route=rest/kuaiqian/getCardsList').then(function(result) {

        if (result.data && result.data.success) {
          $scope.cards = result.data.data;
        }
      })

      $scope.addCard = function() {
        $location.path('payment/card/add')
      }

      $scope.selectCard = function(card){
        $scope.selectedCard = card;
      }

      $scope.getDynNum = function() {
        if(!$scope.selectedCard){
          return;
        }
        if ($scope.wating) {
          return;
        }
        var param = {
          'storablePan': $scope.selectedCard.storable_card_no
        }
        $http.post("index.php?route=rest/kuaiqian/getDynNum", param).then(
          function(result) {
            if (result.data.success) {
              $scope.smsSend = true;
            }
          }
        )

        $scope.wating = true;
        $scope.counting = 60;
        timer();
      }

      $scope.pur = function(){
        if($scope.submitting){
          return;
        }
        $http.post("index.php?route=rest/kuaiqian/pur", $scope.card).then(
          function(result) {
            $scope.submitting = false;
            if (result.data.success) {
              BHZ.MessageBox.show("支付成功");
              $location.path("/orders");
            }
          }
        )
        $scope.submitting = true;
      }

      $scope.cancelPay = function(){
        $scope.selectedCard = null;
        $scope.validCode = "";
      }

      function timer() {
        if ($scope.counting > 0) {
          $scope.counting--;
          $(".counting").html($scope.counting);
          setTimeout(timer, 1000);
        } else {
          $scope.$apply(function() {
            $scope.wating = false;
          })
        }

      }

    }
  ]);



  angularAMD.controller('AddCardCtrl', ['$scope', 'ngDialog', '$http', '$cookies', '$location',
    function($scope, ngDialog, $http, $cookies, $location) {

      $scope.type = 'normal';

      $scope.changeType = function(type) {
        $scope.type = type;
      }

      $scope.card = {};

      $scope.card.first = 1;

      $scope.getDynNum = function() {
        if ($scope.wating) {
          return;
        }
        if (!$scope.card.cardHolderName) {
          BHZ.MessageDialog.show('姓名不能为空');
          return;
        }
        if (!$scope.card.pan) {
          BHZ.MessageDialog.show('卡号不能为空');
          return;
        }
        if ($scope.type == "credit") {
          if (!$scope.card.cvv2) {
            BHZ.MessageDialog.show('cvv不能为空');
            return;
          }
          if (!$scope.card.expiredDate) {
            BHZ.MessageDialog.show('过期时间不能为空');
            return;
          }
        }
        if (!$scope.card.cardHolderId) {
          BHZ.MessageDialog.show('身份证不能为空');
          return;
        }
        if (!$scope.card.phoneNO) {
          BHZ.MessageDialog.show('手机号不能为空');
          return;
        }

        $scope.wating = true;
        $scope.counting = 60;
        timer();

        $http.post("index.php?route=rest/kuaiqian/getDynNum", $scope.card).then(
          function(result) {
            if (result.data.success) {
              BHZ.Message
              $scope.smsSend = true;
            }
          }
        )
      }


      $scope.pur = function(){
        if($scope.submitting){
          return;
        }
        $http.post("index.php?route=rest/kuaiqian/pur", $scope.card).then(
          function(result) {
            $scope.submitting = false;
            if (result.data.success) {
              BHZ.MessageBox.show("支付成功");
              $location.path("/orders")
            }
          }
        )
        $scope.submitting = true;
      }

      function timer() {
        if ($scope.counting > 0) {
          $scope.counting--;
          $(".counting").html($scope.counting);
          setTimeout(timer, 1000);
        } else {
          $scope.$apply(function() {
            $scope.wating = false;
          })
        }

      }

    }
  ]);




  angularAMD.controller('PaymentReturnCtrl', ['$scope',  '$http', '$location',
    function($scope,  $http, $location) {
      
        
    }
  ]);
})
