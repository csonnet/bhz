'use strict';

define(['angular', 'angularAMD', 'angular-route'], function(ng, angularAMD) {

  var BHZRoute = ng.module('BHZRoute', ['ngRoute']);

  BHZRoute.constant("BHZConfig", {
    MODULE_PATH: 'catalog/view/theme/bhz/pc/modules/'
  }) ;
  
  BHZRoute.config(function($routeProvider, $locationProvider, BHZConfig) {
    $routeProvider
      .when('/home', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'home/home.html',
        controllerUrl: 'modules-pc/home/home',
        controller: 'homeCtrl',
        title: '首页',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true
      }))
      .when('/products', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'product/products.html',
        controllerUrl: 'modules-pc/product/product',
        controller: 'productsCtrl',
        title: '商品列表页',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true
      }))
      .when('/product/:product_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'product/product.html',
        controllerUrl: 'modules-pc/product/product',
        controller: 'productCtrl',
        title: '商品详情页',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true
      }))
      .when('/vendor/:vendor_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'vendor/vendor.html',
        controllerUrl: 'modules-pc/vendor/vendor',
        controller: 'vendorCtrl',
        title: '店铺介绍',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true
       }))

      //个人中心
      .when("/usercenter", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/usercenter.html',
        controllerUrl: 'modules-pc/usercenter/usercenter',
        controller: 'UsercenterCtrl',
        title: '个人中心',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
      }))
      .when("/usercenter/profile", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/profile.html',
        controllerUrl: 'modules-pc/usercenter/profile',
        controller: 'ProfileCtrl',
        title: '完善个人资料',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
      }))
      .when('/usercenter/address', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/address/address_list.html',
        controllerUrl: 'modules-pc/usercenter/address/address',
        controller: 'AddressListCtrl',
        title: '地址管理',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))

	   //通知消息
	   .when('/usercenter/message', angularAMD.route({ //我的消息
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/message/message.html',
        controllerUrl: 'modules-pc/usercenter/message/message',
        controller: 'MessageCtrl',
        title: '我的消息',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
	   
	   //余额充值
	   .when('/usercenter/balance', angularAMD.route({ //我的余额
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/balance.html',
        controllerUrl: 'modules-pc/usercenter/balance/balance',
        controller: 'BalanceCtrl',
        title: '我的余额',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
	  .when('/usercenter/recharge', angularAMD.route({ //立即充值
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/recharge.html',
        controllerUrl: 'modules-pc/usercenter/balance/balance',
        controller: 'RechargeCtrl',
        title: '立即充值',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
	   .when('/usercenter/deposit', angularAMD.route({ //定金充值
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/deposit.html',
        controllerUrl: 'modules-pc/usercenter/balance/balance',
        controller: 'RechargeCtrl',
        title: '定金充值',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
	   .when('/usercenter/balance/return', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/balance/return.html',
        controllerUrl: 'modules-pc/usercenter/balance/balance',
        controller: 'BalanceReturnCtrl',
        withTopHeader: true,
        title: '收银台'
      }))
	   
      .when('/usercenter/wishlist', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/wishlist/wishlist.html',
        controllerUrl: 'modules-pc/usercenter/wishlist/wishlist',
        controller: 'WishlistCtrl',
        title: '收藏夹',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
      }))
      .when('/usercenter/coupon', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/coupon/coupon.html',
        controllerUrl: 'modules-pc/usercenter/coupon/coupon',
        controller: 'CouponCtrl',
        title: '优惠券',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
      }))
      .when('/orders', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/orderList.html?v=20160903',
        controllerUrl: 'modules-pc/usercenter/order/order',
        controller: 'orderListCtrl',
        title: '全部订单',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
	   .when('/periods', angularAMD.route({ //用户中心账期
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/period/periodList.html',
        controllerUrl: 'modules-pc/usercenter/period/period',
        controller: 'periodListCtrl',
        title: '全部账期',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
      .when('/order/:order_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/order.html',
        controllerUrl: 'modules-pc/usercenter/order/order',
        controller: 'orderCtrl',
        title: '订单详情',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
      .when('/order/:order_id/review/:order_product_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/review.html',
        controllerUrl: 'modules-pc/usercenter/order/order',
        controller: 'reviewCtrl',
        title: '发表评价',
        withoutFooter: true,
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))
      .when('/order/:order_id/luckydraw/:luckydraw_id', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'usercenter/order/luckydraw.html',
        controllerUrl: 'modules-pc/usercenter/order/order',
        controller: 'orderLuckydrawCtrl',
        title: '幸运抽奖',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        redHeader: true
       }))

      //登录注册相关页面路由
      .when("/login", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/login.html',
        controllerUrl: 'modules-pc/account/account',
        controller: 'loginCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        title: '登录',
        isLoginOrRegister: true
      }))
      .when("/register", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/register.html',
        controllerUrl: 'modules-pc/account/account',
        controller: 'registerCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        title: '注册',
        isLoginOrRegister: true
      }))
      .when("/password", angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'account/password.html',
        controllerUrl: 'modules-pc/account/account',
        controller: 'passwordCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        title: '找回密码'
      }))

      //购物车结算相关页面路由
      .when('/cart', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/cart.html',
        controllerUrl: 'modules-pc/checkout/cart',
        controller: 'CartCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        title: '我的进货单',
        withoutFooter: true
      }))
      .when('/checkout', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'checkout/checkout.html',
        controllerUrl: 'modules-pc/checkout/checkout',
        controller: 'CheckoutCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        title: '结算',
        withoutFooter: true
      }))
      .when('/payment/card', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'payment/cardlist.html',
        controllerUrl: 'modules-pc/payment/payment',
        controller: 'CardListCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        title: '收银台-支付'
      }))
      .when('/payment/card/add', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'payment/addcard.html',
        controllerUrl: 'modules-pc/payment/payment',
        controller: 'AddCardCtrl',
        withTopHeader: true,
        withSubtitleLogo: true,
        withSearch: true,
        withCart: true,
        title: '收银台-添加银行卡'
      }))
      .when('/payment/return', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'payment/return.html',
        controllerUrl: 'modules-pc/payment/payment',
        controller: 'PaymentReturnCtrl',
        withTopHeader: true,
        title: '收银台'
      }))
      .when('/page/purchase', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/purchase.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '采购指南'
      }))
      .when('/page/delivery', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/delivery.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '配送说明'
      }))
      .when('/page/payment', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/payment.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '在线支付'
      }))
      .when('/page/point', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/point.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '积分规则'
      }))
      .when('/page/coupon', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/coupon.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '优惠券'
      }))
      .when('/page/return', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/return.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '退换货'
      }))
      .when('/page/vendor', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/vendor.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '商家入住'
      }))
      .when('/page/freesample', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/freesample.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '免费拿样'
      }))
      .when('/page/about', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/about.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '关于百货栈'
      }))
      .when('/page/contact', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/contact.html',
        controllerUrl: 'modules-pc/pages/pages',
        controller: 'PagesCtrl',
        title: '关于百货栈'
      }))
	  .when('/pages/caipiao', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/caipiao.html',
        controllerUrl: 'modules-pc/home/home',
        controller: 'homeCtrl',
        title: '彩票'
      }))
	  .when('/pages/activity', angularAMD.route({
        templateUrl: BHZConfig.MODULE_PATH + 'pages/activity.html',
		controllerUrl: 'modules-pc/home/home',
        controller: 'homeCtrl',
        title: '活动'
      }))
  })

  return BHZRoute;
})