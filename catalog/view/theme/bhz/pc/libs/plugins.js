(function($, W) {
  var BHZ = BHZ || {};

  BHZ.MessageDialog = {
    show:function(message,buttonText){
      var buttonText = buttonText || "知道了";
      if ($(".bhz-message-dialog").length == 0) {
        var wlHtml = "<div class='bhz-message-dialog'><div class='message-content'></div><div class='button-content'></div></div>";
        $("body").append(wlHtml);
      }
      if ($(".cover").length == 0){
        $("body").append("<div class='cover' style='display:none'></div>");
      }
      $('.message-content').html(message);
      $('.button-content').html(buttonText);
      $(".cover").show();
      $('.bhz-message-dialog').fadeIn('fast');
      $('.button-content').click(function(event) {
        $(".cover").hide();
        $('.bhz-message-dialog').fadeOut('fast');
      });
    }
  };


  //show message
  BHZ.MessageBox = {
    show: function(message){
      if ($(".bhz-message-box").length == 0) {
        var wlHtml = "<div class='bhz-message-box'><span class='message-content'></span></div>";
        $("body").append(wlHtml);
      }

      $('.message-content').html(message);
      $('.bhz-message-box').fadeIn('fast');

      setTimeout(function() {
        $('.bhz-message-box').fadeOut('fast');
      }, 1000)
    }
  };

  BHZ.Timer = function(selector, duration){
    this.selector = selector;
    this.duration = duration;
  };
  BHZ.Timer.prototype = {
    timing: function(){
      //when duration > 0 ,begin to countdown and not allowed to send sms again
      if(this.duration > 0){
        this.duration--;
        $(this.selector).html("("+this.duration+")");
        var that = this;
        setTimeout(function(){
          that.timing();
        }, 1000);
      }else{
        //when countdown over set can send sms 
        $(this.selector).html('');
        if($(this.selector).parent(".btn_getcode").length > 0){
          $(this.selector).parent('.btn_getcode').removeClass("disabled");
        }
      }
    }
  };

  BHZ.checkMobileFormat = function(mobile){
    if (!mobile.match(/^0?(13[0-9]|15[0-9]|18[0-9]|14[0-9]|17[0-9])[0-9]{8}$/)) {
      return;
    }
    return true;
  }

  
  function is_weixin(){  
    var ua = navigator.userAgent.toLowerCase();  
    if(ua.match(/MicroMessenger/i)=="micromessenger") {  
        return true;  
    } else {  
        return false;  
    }  
  }

  window.isWeixin = is_weixin();

  window.BHZ = BHZ;

})(jQuery, window);

// Production steps of ECMA-262, Edition 5, 15.4.4.14
// Reference: http://es5.github.io/#x15.4.4.14
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {

    var k;

    // 1. Let O be the result of calling ToObject passing
    //    the this value as the argument.
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }

    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get
    //    internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If len is 0, return -1.
    if (len === 0) {
      return -1;
    }

    // 5. If argument fromIndex was passed let n be
    //    ToInteger(fromIndex); else let n be 0.
    var n = +fromIndex || 0;

    if (Math.abs(n) === Infinity) {
      n = 0;
    }

    // 6. If n >= len, return -1.
    if (n >= len) {
      return -1;
    }

    // 7. If n >= 0, then Let k be n.
    // 8. Else, n<0, Let k be len - abs(n).
    //    If k is less than 0, then let k be 0.
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

    // 9. Repeat, while k < len
    while (k < len) {
      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the
      //    HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      //    i.  Let elementK be the result of calling the Get
      //        internal method of O with the argument ToString(k).
      //   ii.  Let same be the result of applying the
      //        Strict Equality Comparison Algorithm to
      //        searchElement and elementK.
      //  iii.  If same is true, return k.
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}