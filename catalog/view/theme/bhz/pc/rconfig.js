requirejs.config({
  waitSeconds: 0,
  baseUrl: "catalog/view/theme/bhz",
  paths: {
    'jquery': 'vendor/js/jquery-1.11.3.min',
    'text': 'vendor/js/text',
    'json': 'vendor/js/json',
    'angular': 'vendor/js/pc/angular.min',
    'angularAMD': 'vendor/js/pc/angularAMD',
    'ngload':'vendor/js/ngload.min',
    'angular-route': 'vendor/js/pc/angular-route.min',
    'angular-cookies': 'vendor/js/pc/angular-cookies.min',
    'angular-animate': 'vendor/js/pc/angular-animate.min',
    'angular-touch': 'vendor/js/angular-touch.min',
    'angular-carousel': 'vendor/js/angular-carousel.min',
    'ng-dialog': 'vendor/js/ngDialog',
    'spin': 'vendor/js/spin.min',
    'ng-file-upload': 'vendor/js/ng-file-upload.min',
    'jquery-easing':'vendor/js/jquery.easing',
    'jquery-timeline':'vendor/js/jquery.timeline',
    'jquery-cookie':'vendor/js/jquery.cookie',
    'common': 'common',
    'data': 'data',
    'modules': 'm/modules',
    'modules-pc': 'pc/modules'
  },
  shim: {
    'angular': {
      'exports': 'angular',
      'deps': ['jquery']
    },
    'angularAMD': ['angular'],
    'angular-route': ['angular'],
    'angular-cookies': ['angular'],
    'angular-animate': ['angular','angular-route'],
    'angular-touch': ['angular'],
    'angular-carousel': ['angular-touch'],
    'ng-dialog': ['angular'],
    'ng-file-upload':['angular'],
    'angularAMD': ['angular'],
    'ngload':['angularAMD'],
    'jquery-cookie': ['jquery']
  },
});