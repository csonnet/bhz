<?php
class ModelMarketingMessage extends Model {

	//获取业务区域
	public function getSaleAreas(){

		$where['status'] = 1;
		$sa = M('sales_area')->where($where)->select();

		return $sa;

	}
	
	//获取仓库
	public function getWareHouses(){

		$where['status'] = 1;
		$wh = M('warehouse')->where($where)->select();

		return $wh;

	}
	
	//获取用户autocomplete
	public function getCustomers($data){

		$where['fullname'] = array('like','%'.$data['filter_customer'].'%');
		$start = $data['start'];
		$limit = $data['limit'];

		$customers = M("customer")->where($where)->limit($start,$limit)->select();

		return $customers;

	}
	
	//发送消息
	public function sendMessage($data){

		$add['title'] = $data['title'];
		$add['content'] = $data['content'];
		$add['type'] = 3;
		$add['status'] = 1;
		$nd_id = M('notice_description')->data($add)->add();

		if($data['method'] == 'all'){		

			$persons = $this->getAllCustomer();

			foreach($persons as $ps){
				$data['persons'][] = $ps['customer_id'];
			}

		}

		foreach($data['persons'] as $p){

			$add2['notice_id'] = $nd_id;
			$add2['customer_id'] = $p;
			$add2['date_added'] = date('Y-m-d H:i:s');
			$add2['is_del'] = 0;
			M('notice')->data($add2)->add();

		}

	}
	
	//获取全部启用用户id
	public function getAllCustomer(){
		
		$where['status'] = array('neq',0);
		$ids = M('customer')->where($where)->field('customer_id')->select();

		return $ids;

	}
	
	//获取区域业务员
	public function saCustomers($data){

		if(empty($data)){
			return;
		}
		
		$cid = array();
		
		if($data != 'all'){
			$sa = implode(',',$data);
			$where['u.sales_area_id'] = array('in',$sa);
			$where['u.status'] = 1;
		}
		$where['u.user_group_id'] = array('in','63');
		$customer = M('customer')
			->alias('c')
			->join('user u on c.telephone =u.contact_tel','left')
			->join('user_group ug on u.user_group_id = ug.user_group_id','left')
			->where($where)
			->field('c.customer_id')->select();

		foreach($customer as $c){
			$cid[] = $c['customer_id'];
		}

		return $cid;

	}

	//获取仓库管理
	public function whCustomers($data){
		
		if(empty($data)){
			return;
		}

		$cid = array();
		
		if($data != 'all'){
			$lgp = implode(',',$data);
			$where['u.logcenter_permission'] = array('in',$lgp);
			$where['u.status'] = 1;
		}
		$where['u.user_group_id'] = array('in','60,65');
		$customer = M('customer')
			->alias('c')
			->join('user u on c.telephone =u.contact_tel','left')
			->join('user_group ug on u.user_group_id = ug.user_group_id','left')
			->where($where)
			->field('c.customer_id')->select();

		foreach($customer as $c){
			$cid[] = $c['customer_id'];
		}

		return $cid;

	}

	//获取全部内部人员
	public function adminAll(){
		
		$where['u.contact_tel'] = array('exp','is not null');
		$where['u.status'] = 1;
		$customer = M('customer')
			->alias('c')
			->join('user u on c.telephone =u.contact_tel','left')
			->where($where)
			->field('c.customer_id')->select();

		foreach($customer as $c){
			$cid[] = $c['customer_id'];
		}

		return $cid;

	}
	
	//获取部分内部人员
	public function adminCustomer(){
		
		$where['u.contact_tel'] = array('exp','is not null');
		$where['u.contact_tel'] = array('not in','13816354916,13661896330');
		$where['u.user_group_id'] = array('not in','50,51');
		$where['u.status'] = 1;
		$customer = M('customer')
			->alias('c')
			->join('user u on c.telephone =u.contact_tel','left')
			->where($where)
			->field('c.customer_id')->select();

		foreach($customer as $c){
			$cid[] = $c['customer_id'];
		}

		return $cid;

	}
	
	//自动发送消息
	public function sendAutoMessage($nid,$data){

		switch($nid){			
			case 2: //下架商品
				$this->changeProductStatus(2,$data);
				break;
			case 3: //上架商品
				$this->changeProductStatus(3,$data);
				break;
			default;
				break;
		}

	}
	
	//商品上下架通知
	public function changeProductStatus($nid,$data){

		$where['pov.product_id'] = $data['product_id'];
		$product = M('product_option_value')
			->alias('pov')
			->join('product_description pd on pov.product_id = pd.product_id','left')
			->where($where)
			->field('pov.product_code as code,pd.name as product')
			->select();
		
		//替换字符
		$notice = M('notice_description')->where('notice_id='.$nid)->select();
		$content = str_replace('{{product}}',$product[0]['product'],$notice[0]['content']);
		$content = str_replace('{{code}}',$product[0]['code'],$content);
		
		//发送用户
		$customers = array();
		switch($notice[0]['send_to']){		
			case 1:
				$customers = $this->saCustomers('all');
				break;
			case 2:
				$customers = $this->whCustomers('all');
				break;
			case 3:
				$sa = $this->saCustomers('all');
				$wh = $this->whCustomers('all');
				$customers = array_merge($sa,$wh);
				$customers = array_flip(array_flip($customers));
				break;
			case 4: //内部人员
				$customers = $this->adminCustomer();
			default:
				break;
		}

		foreach($customers as $v){

			$add['notice_id'] = $nid;
			$add['customer_id'] = $v;
			$add['template_data'] = $content;
			$add['date_added'] = date('Y-m-d H:i:s');
			
			M('notice')->data($add)->add();

		}

	}
	
}
