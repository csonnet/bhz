 <?php

require_once(DIR_SYSTEM . 'xunsearch/php/lib/XS.php');

class ModelCatalogXunsearch extends Model {
	
	//添加全文索引
	public function addDoc($id,$data){

		if($data['status'] == 1){

			$class1;
			$class2;
			$class3;
			
			if($data['product_class1']){
				$class1 = M('category_description')->where('category_id='.$data['product_class1'])->getField('name');
			}

			if($data['product_class2']){
				$class2 = M('category_description')->where('category_id='.$data['product_class2'])->getField('name');
			}

			if($data['product_class3']){
				$class3 = M('category_description')->where('category_id='.$data['product_class3'])->getField('name');
			}

			$category = $class1." ".$class2." ".$class3;

			$xs = new XS('bhz');
			$index = $xs->index;
		
			$data = array(
				'product_id' => $id,
				'name' => $data['product_description'][1]['name'],
				'category' => $category,
				'ptype' => $data['p_type'],
				'model' => $data['model'],
				'manufacturer' => $data['manufacturer'],
				'description' => $data['product_description'][1]['description'],
				'status' => $data['status']
			);
				
			// 创建文档对象
			$doc = new XSDocument;
			$doc->setFields($data);

			// 添加索引
			$index->add($doc);

		}
	
	}

	//编辑全文索引
	public function editDoc($id,$data){

		if($data['status'] != 1){

			$this->delDoc($id);

		}
		else{

			$class1;
			$class2;
			$class3;
			
			if($data['product_class1']){
				$class1 = M('category_description')->where('category_id='.$data['product_class1'])->getField('name');
			}

			if($data['product_class2']){
				$class2 = M('category_description')->where('category_id='.$data['product_class2'])->getField('name');
			}

			if($data['product_class3']){
				$class3 = M('category_description')->where('category_id='.$data['product_class3'])->getField('name');
			}

			$category = $class1." ".$class2." ".$class3;

			$xs = new XS('bhz');
			$index = $xs->index;
		
			$data = array(
				'product_id' => $id,
				'name' => $data['product_description'][1]['name'],
				'category' => $category,
				'ptype' => $data['p_type'],
				'model' => $data['model'],
				'manufacturer' => $data['manufacturer'],
				'description' => $data['product_description'][1]['description'],
				'status' => $data['status']
			);
				
			// 创建文档对象
			$doc = new XSDocument;
			$doc->setFields($data);

			// 编辑索引
			$index->update($doc);

		}
	
	}

	//删除全文索引
	public function delDoc($id){

		$xs = new XS('bhz');
		$index = $xs->index;

		// 删除索引
		$index->del($id);

	}

}
