<?php
class ModelCatalogLgiLogcenterProfile extends Model {
  
  public function editLogcenterProfile($user_id, $data) {
    $Vendor = M("Logcenters");
    $Vendor->where(array('user_id'=>$user_id))->data($data)->save();

    if (isset($data['logcenter_image'])) {
      $data2 = array(
        'image' => $data['logcenter_image'],
        'fullname' => $data['firstname'],
        'user_id' =>  (int)$user_id
      );
      $User= M("User");
      $user_id = $User->data($data2)->save();
    }

    $this->cache->delete('logcenter');
  }
  
  public function getLogcenterProfile($user_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "logcenters WHERE user_id = '" . (int)$user_id . "'");
    return $query->row; 
  }
}
?>