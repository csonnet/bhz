<?php
class ModelSaleRemitMoney extends Model {
    //添加支付申请
    public function addRemitHistory($data) {
        $sql = "INSERT INTO `remit_info_history` SET ";
        foreach ($data as $col=>$val) {
            $sql .= "`".$col."`='".$this->db->escape($val)."',";
        }
        $this->db->query(substr($sql, 0, -1));
        return $this->db->getLastId();
    }

    //更新支付申请
    public function updateRemitHistory($pk, $data) {
        $temp = array();
        foreach ($data as $col=>$val) {
            $temp[] = "`".$col."`='".$this->db->escape($val)."'";
        }
        $sql = "UPDATE `remit_info_history` SET ".implode(',', $temp)." WHERE `remit_id`='".$pk."'";
        $this->db->query($sql);
    }

    //添加收款人（员工）
    public function addEmploy($data){
        $sql = "INSERT INTO `remit_info` SET ";
        foreach ($data as $col=>$val) {
            $sql .= "`".$col."`='".$this->db->escape($val)."',";
        }
        $this->db->query(substr($sql, 0, -1));
        return $this->db->getLastId();
    }

    //修改收款人（员工）
    public function updateEmploy($pk, $data) {
        $temp = array();
        foreach ($data as $col=>$val) {
            $temp[] = "`".$col."`='".$this->db->escape($val)."'";
        }
        $sql = "UPDATE `remit_info` SET ".implode(',', $temp)." WHERE `employ_id`='".$pk."'";
        $this->db->query($sql);
    }

    //autocomplete获取收款人（员工）信息
    public function getEmploy($data = array()) {
        $sql = "SELECT * FROM `".DB_PREFIX."remit_info`";

        if (!empty($data['filter_name']))
            $sql .= " WHERE employ_name LIKE '%".$this->db->escape($data['filter_name'])."%'";

        $sort_data = array(
            'employ_name',
            'employ_id',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data))
            $sql .= " ORDER BY `".$data['sort']."`";
        else
            $sql .= " ORDER BY `employ_id`";

        if (isset($data['order']) && ($data['order'] == 'DESC'))
            $sql .= " DESC";
        else
            $sql .= " ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0)
                $data['start'] = 0;
            if ($data['limit'] < 1)
                $data['limit'] = 20;
            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    //获取支付申请总数（用于分页）
    public function getRemittotal($filter){
        $sql = "SELECT COUNT(*) AS total FROM `".DB_PREFIX."remit_info_history` AS RIH LEFT JOIN `remit_info` AS RI ON (RIH.`employ_id`=RI.`employ_id`)";
        if ('' != $filter['keywords'])
            $sql .= " WHERE RIH.`card_num` LIKE '%".$this->db->escape($filter['keywords'])."%' OR RI.`employ_name` LIKE '%".$this->db->escape($filter['keywords'])."%'";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    //获取支付申请分页数据
    public function getRemitHistorys($filter){
        $sql = "
        SELECT
            RIH.`remit_id`,RIH.`amount`,RIH.`card_num`,RIH.`comment`,RIH.`date_add`,RIH.`username`,RIH.`status`,RIH.`card_type`,
            RI.`employ_name`,RI.`section`
        FROM
            `".DB_PREFIX."remit_info_history` AS RIH
            LEFT JOIN `remit_info` AS RI ON (RIH.`employ_id`=RI.`employ_id`)
        ";
        if ('' != $filter['keywords'])
            $sql .= " WHERE RIH.`card_num` LIKE '%".$this->db->escape($filter['keywords'])."%' OR RI.`employ_name` LIKE '%".$this->db->escape($filter['keywords'])."%'";
        $sql .= "ORDER BY `remit_id` DESC LIMIT ".$filter['start'].",".$filter['limit'];
        $query = $this->db->query($sql);
        return $query->rows;
    }

/*
    public function addRemitHistory($data){
        $model = M('remit_info_history');
        $field = 'employ_id,user_id,amount,card_num,date_add,user_comment,status,username';
        $query = $model->field($field)->data($data)->add();
        return $query;
    }

    public function addEmploy($data){
        $model = M('remit_info');
        $field = 'employ_name,section,bank_name,card_num,date_add';
        $query = $model->field($field)->data($data)->add();
        return $query;
    }
*/
}
