<?php
class ModelSaleLogcenterBill extends Model {
  public function getList($page = 1, $filter, $sort_data){
    $lblist = M('logcenter_bill lb')
    ->join('logcenters l on l.logcenter_id = lb.logcenter_id','left')
    ->order($sort_data['sort'] . ' ' . $sort_data['order'])
    ->where($filter)->page($page)->limit($this->config->get('config_limit_admin'))->select();
    // var_dump(M()->getLastSql());die();
    return $lblist;
  }

  public function getAllList($filter, $sort_data){
    $lblist = M('logcenter_bill lb')
    ->join('logcenters l on l.logcenter_id = lb.logcenter_id','left')
    ->order($sort_data['sort'] . ' ' . $sort_data['order'])
    ->where($filter)->select();
    // var_dump(M()->getLastSql());die();
    return $lblist;
  }
  public function getListCount($filter){
    $count = M('logcenter_bill lb')->where($filter)->count();
    return $count;
  }

  public function getLogcenterBill($id){
    $logcenter_bill = M('logcenter_bill')->find($id);
    if($logcenter_bill){
      $logcenter = M('logcenters')->where('logcenter_id='.$logcenter_bill['logcenter_id'])->find();
      $logcenter_bill['logcenter_name'] = $logcenter['logcenter_name'];
    }
    return $logcenter_bill;
  }
  public function getLogcenterBillOrders($id){
    $where['logcenter_bill_id'] = $id;
    $orders = M('logcenter_bill_orders lbo')
      ->join('`order` o on o.order_id=lbo.order_id')
      ->where($where)
      ->select();
    return $orders;
  }

  public function getLogcenterBillHistories($id){
    $where['logcenter_bill_id'] = $id;
    $logcenter_bill_histories = M('logcenter_bill_history')
      ->where($where)
      ->select();
    return $logcenter_bill_histories;
  }


  public function getLogcenterBillOrdersInfo($id) {
    $where['logcenter_bill_id'] = $id;
    $orders_info = M('logcenter_bill_orders lbo')
      ->join('`order` o on o.order_id=lbo.order_id')
      ->join('order_product op on op.order_id=o.order_id')
      ->join('left join vendor v on v.vproduct_id=op.product_id')
      ->join('left join vendors vs on vs.vendor_id=v.vendor')
      ->field('o.date_added, o.order_id, o.date_modified,o.shipping_company, 
        vs.vendor_id, vs.vendor_name, op.name as product_name, 
        "" as sku, op.quantity, op.price, op.total , "" as comment')
      ->order('o.order_id ASC')
      ->where($where)
      ->select();
    return $orders_info;
  }

  public function deleteBill($id) {
    $logcenter_bill = M('logcenter_bill');
    $logcenter_bill->where(array('logcenter_bill_id'=>$id))->delete();
  }

  public function setStock($po_id, $to_save_stock) {
    //添加入库历史记录
    $stock_history = M('po_product_stock_history');
    foreach ($to_save_stock as $po_product_id => $qty) {
      $stock_history->data(array('po_product_id'=>$po_product_id, 'qty'=>$qty, 'date_added'=>date('Y-m-d H:i:s')))->add();
    }
    //修改po单的delivered_qty
    $po_product = M('po_product');
    foreach ($to_save_stock as $po_product_id => $qty) {
      $po_product->where(array('id'=>$po_product_id, 'po_id'=>$po_id))->setInc('delivered_qty', $qty);
    }
  }

  public function findOrderWithoutVendor($orders) {
    $order_query = M('order_product op')
    ->join('left join vendor v on v.vproduct_id=op.product_id')
    ->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor is null')
    ->field('v.vendor as vendor_id, op.name as name')
    ->select();
    return $order_query;
  }

  public function getVendorIdsFromOrders($orders) {
    $order_query = M('order_product op')
    ->join('vendor v on v.vproduct_id=op.product_id')
    ->where('op.order_id in (' . implode(',', $orders) . ')')
    ->field('v.vendor as vendor_id')
    ->group('v.vendor')
    ->select();
    return $order_query;
  }

  public function getPoDataFromOrders($orders, $vendor_id=0) {
    $order_query = M('order_product op')
    ->join('left join order_option oo on oo.order_product_id=op.order_product_id')
    ->join('vendor v on v.vproduct_id=op.product_id')
    ->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor="' . $vendor_id . '"')
    ->group('op.product_id, oo.product_option_value_id')
    ->field('v.vendor as vendor_id, op.product_id as product_id, v.product_cost as unit_price, 
      v.product_cost*op.quantity as price, op.name as name, oo.product_option_value_id as option_id, oo.value as option_name, "" as sku, "" as comment, SUM(op.quantity) as qty')
    ->select();
    return $order_query;
  }

  public function setPoGeneratedFlag($orders) {
    foreach ($orders as $order_id) {
      $Order = M('order');
      $Order->where(array('order_id'=>$order_id))->setInc('po_generated');
    }
  }

  public function changeDifference($id, $difference) {
    $logcenter_bill = M('logcenter_bill');
    $logcenter_bill->data(array('logcenter_bill_id'=>$id, 'difference'=>$difference))->save();
  }

  public function changeLogcenterBillStatus($logcenter_bill_id, $status) {
    $logcenter_bill = M('logcenter_bill');
    $logcenter_bill->data(array('logcenter_bill_id'=>$logcenter_bill_id, 'status'=>$status))->save();
  }

  public function getBilledOrders($cur_month, $end_month) {
    $map['year_month'] = array('between',array($end_month, $cur_month));
    // $map['date_added'] = array('between',array($end_month, $cur_month));
    // $map['lb.status'] = array('gt', 0);
    $query = M('logcenter_bill lb')
      ->join('logcenter_bill_orders lbo on lb.logcenter_bill_id=lbo.logcenter_bill_id')
      ->where($map)
      ->field('lbo.order_id')
      ->select();
    return $query;
  }

  public function saveLogcenterBill($data, $order_ids) {
    $logcenter_bill_id = M('logcenter_bill')->add($data);
    foreach ($order_ids as $order_id) {
      M('logcenter_bill_orders')->add(array('logcenter_bill_id'=>$logcenter_bill_id, 'order_id'=>$order_id));
    }

    return $logcenter_bill_id;
  }

  public function addLogcenterBillHistory($logcenter_bill_id, $user_id, $operator_name, $comment) {
    $logcenterBillHistory = M('logcenter_bill_history');
    $data = array('logcenter_bill_id'=>$logcenter_bill_id, 'user_id'=>$user_id, 
      'operator_name'=>$operator_name, 'comment'=>$comment, 
      'notify'=>0, 'date_added'=>date('Y-m-d H:i:s'));
    $logcenterBillHistory->data($data)->add();
  }

}