<?php
class ModelSaleVendorBill extends Model {
  public function getList($page = 1, $filter, $sort_data){
      $filter['year_month'] = array('LT', '20170930000000');
    $vblist = M('vendor_bill vb')
    ->join('vendors vs on vs.vendor_id = vb.vendor_id','left')
    ->order($sort_data['sort'] . ' ' . $sort_data['order'])
    ->where($filter)->page($page)->limit($this->config->get('config_limit_admin'))->select();
    //var_dump(M()->getLastSql());die();
    return $vblist;
  }
  public function getAllList($filter, $sort_data){
      $filter['year_month'] = array('LT', '20170930000000');
    $vblist = M('vendor_bill vb')
    ->join('vendors vs on vs.vendor_id = vb.vendor_id','left')
    ->order($sort_data['sort'] . ' ' . $sort_data['order'])
    ->where($filter)->select();
    // var_dump(M()->getLastSql());die();
    return $vblist;
  }
  public function getListCount($filter){
      $filter['year_month'] = array('LT', '20170930000000');
    $count = M('vendor_bill vb')->where($filter)->count();
    return $count;
  }

  public function getVendorBill($id){
    $vendor_bill = M('vendor_bill')->find($id);
    if($vendor_bill){
      $vendor = M('vendors')->where('vendor_id='.$vendor_bill['vendor_id'])->find();
      $vendor_bill['vendor_name'] = $vendor['vendor_name'];
    }
    return $vendor_bill;
  }

  public function getVendorBillPoCount($id){
    $where['vendor_bill_id'] = $id;
    $po = M('vendor_bill_po vbp')
      ->where($where)
      ->select();
    return $po;
  }

  public function getVendorBillPo($id){
    $where['vendor_bill_id'] = $id;
    $po = M('vendor_bill_po vbp')
      ->join('`po` po on po.id=vbp.po_id')
      ->join('left join po_product pp on po.id=pp.po_id')
      ->where($where)
      ->select();
    return $po;
  }

  public function getVendorBillHistories($id){
    $where['vendor_bill_id'] = $id;
    $vendor_bill_histories = M('vendor_bill_history')
      ->where($where)
      ->select();
    return $vendor_bill_histories;
  }

  public function getVendorByVendorBillId($id) {
    $where['vendor_bill_id'] = $id;
    $vb_info = M('vendor_bill vb')
      ->where($where)
      ->find();
    if(!empty($vb_info)) {
      $query = M('vendors')->where(array('vendor_id'=>$vb_info['vendor_id']))->find();
      return $query;
    }
    return null;
  }

  public function deleteBill($id) {
    $vendor_bill = M('vendor_bill');
    $vendor_bill->where(array('vendor_bill_id'=>$id))->delete();
  }

  public function getVendorIdsFromPo($orders) {
    $order_query = M('order_product op')
    ->join('vendor v on v.vproduct_id=op.product_id')
    ->where('op.order_id in (' . implode(',', $orders) . ')')
    ->field('v.vendor as vendor_id')
    ->group('v.vendor')
    ->select();
    return $order_query;
  }

  public function setPoGeneratedFlag($orders) {
    foreach ($orders as $order_id) {
      $Order = M('order');
      $Order->where(array('order_id'=>$order_id))->setInc('po_generated');
    }
  }

  public function changeDifference($id, $difference) {
    $vendor_bill = M('vendor_bill');
    $vendor_bill->data(array('vendor_bill_id'=>$id, 'difference'=>$difference))->save();
  }

  public function changeVendorBillStatus($vendor_bill_id, $status) {
    $vendor_bill = M('vendor_bill');
    $vendor_bill->data(array('vendor_bill_id'=>$vendor_bill_id, 'status'=>$status))->save();
  }

  public function getBilledPo($cur_month, $end_month) {
    $map['date_added'] = array('between',array($end_month, $cur_month));
    // $map['date_added'] = array('between',array($end_month, $cur_month));
    // $map['vb.status'] = array('gt', 0);
    $query = M('vendor_bill vb')
      ->join('vendor_bill_po vbp on vb.vendor_bill_id=vbp.vendor_bill_id')
      ->where($map)
      ->field('vbp.po_id')
      ->select();
      // echo M('vendor_bill vb')->getLastSql();
    return $query;
  }

  public function saveVendorBill($data, $po_ids) {
    $vendor_bill_id = M('vendor_bill')->add($data);
    foreach ($po_ids as $po_id) {
      M('vendor_bill_po')->add(array('vendor_bill_id'=>$vendor_bill_id, 'po_id'=>$po_id));
    }
    return $vendor_bill_id;
  }

  public function addVendorBillHistory($vendor_bill_id, $user_id, $operator_name, $comment) {
    $vendorBillHistory = M('vendor_bill_history');
    $data = array('vendor_bill_id'=>$vendor_bill_id, 'user_id'=>$user_id, 
      'operator_name'=>$operator_name, 'comment'=>$comment, 
      'notify'=>0, 'date_added'=>date('Y-m-d H:i:s'));
    $vendorBillHistory->data($data)->add();
  }

  public function getVendors($vendor_id){
    $model = M('vendors');
    $field = array('vendor_id','vendor_name','bank_id','card_id','card_name');
    $query = $model->field($field)->find($vendor_id);
    return $query;
  }

}