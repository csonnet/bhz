<?php

class ModelSaleReturnOrder extends Model {
	public function getList($page = 1, $filter){
		$polist = M('ro')
	    ->join('(select user_id,fullname as user_name from user) u on u.user_id = ro.user_id','left')
		->join('(select count(1) as count, ro_id,packing_no from ro_product group by ro_id) ppc on ppc.ro_id = ro.id','left')
		->join('(select warehouse_id, name as warehouse_name from warehouse) w on w.warehouse_id = ro.warehouse_id','left')
        ->join('(select customer_id,company_name,telephone from customer) c on (ro.market=c.customer_id)')
		->order('ro.id DESC')
		->where($filter)->page($page)->limit($this->config->get('config_limit_admin'))->select();
		return $polist;
	}

	public function getRoOperationBySku($filter){
		$map['rp.sku'] = $filter['sku'];
		$map['_string'] = "ro.deliver_time>='".$filter['filter_date_start']."' AND ro.deliver_time<'".$filter['filter_date_end']."'";
		$map['ro.status'] = count(getRoStatus())-1;
		$map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$ro_operation = M('ro_product')
		->alias('rp')
		->join('left join `ro` ON ro.id=rp.ro_id')
		->join('left join `logcenters` lg ON ro.user_id=lg.user_id')
		->where($map)
		->field('qty,name,unit_price,option_name,packing_no,deliver_time')
		->order('option_name,deliver_time')
		->select();
		return $ro_operation;
	}

	public function getAllList($filter){
		$polist = M('ro')
		//->join('(select vendors.user_id,  vendor_name from vendors) v on v.user_id = ro.vendor_id','left')
		->join('(select count(1) as count, ro_id from ro_product group by ro_id) ppc on ppc.ro_id = ro.id','left')
		->join('(select user_id, company as logcenter_company, address_1 as logcenter_address, logcenter_name from logcenters) lg on lg.user_id = ro.user_id','left')
		->order('ro.id DESC')
		->where($filter)->select();
		return $polist;
	}
	public function getAllProductList($filter){
		$rolist = M('ro_product')
		->alias('rp')
		->join('left join `ro` ON ro.id=rp.ro_id')
		->join('left join `logcenters` lg ON lg.user_id = ro.user_id')
		->join('left join `warehouse` wh ON wh.warehouse_id = ro.warehouse_id')
		->join('left join `product` p ON p.product_id=rp.product_id')
		->join('left join `vendor` v ON v.vproduct_id=rp.product_id')
		->join('left join `vendors` vs ON vs.vendor_id=v.vendor')	
		->field('rp.name,rp.sku,rp.option_name,rp.qty,ro.date_added,ro.deliver_time,p.sku,p.model,ro.status,p.price as price,rp.unit_price,p.product_id,rp.ro_id,vs.vendor_name,rp.packing_no,lg.logcenter_name,wh.name AS wh_name,ro.market,ro.included_order_ids')
		->order('ro.id DESC')	
		->where($filter)
		->select();
		return $rolist;
	}
	public function getListCount($filter){
		$count = M('ro')
		->join(' `warehouse` w on w.warehouse_id = ro.warehouse_id','left')
        ->join(' customer c on (ro.market=c.customer_id)', 'left')
		->where($filter)
		->count();
		return $count;
	}
	public function getProducts($fitler){
		$res = M('product')->join('product_description pd on pd.product_id = product.product_id')
		->field('product.*, pd.*, vendor.product_cost')->join('vendor on vendor.vproduct_id = product.product_id')
		->where($fitler)
		->where(array('pd.language_id'=>(int)$this->config->get('config_language_id')))
		->limit(5)->select();
		return $res;
	}
	public function getRo($id){
		$ro = M('ro')->find($id);

		if($ro){
			$user = M('user')->find($ro['user_id']);
			$ro['creator'] = $user;

			$warehouse_info = M('warehouse')->where('warehouse_id='.$ro['warehouse_id'])->find();
			$ro['warehouse_info'] = $warehouse_info;

			$customer_info = M('customer')->where('customer_id='.$ro['market'])->getField('company_name');
			$ro['market_name'] = $customer_info;

		}
		return $ro;
	}
	public function getRoProducts($id){
		$where['ro_id'] = $id;
		$products = M('ro_product')->where($where)->select();
		return $products;
	}
	public function getRoHistory($id){
		$where['ro_id'] = $id;
		$history = M('ro_history')->where($where)->select();
		return $history;
	}
	
	public function getRoStockIn($id){
		$where['si.refer_id'] = $id;
		$where['si.refer_type_id'] = 3;
		$stockin = M('stock_in')
			->alias('si')
			->join(' `warehouse` w on w.warehouse_id = si.warehouse_id','left')
			->join(' `user` u on u.user_id = si.user_id','left')
			->join(' `inout_state` ios on ios.id = si.status','left')
			->field('si.*,w.name as warehouse_name,u.fullname as user_name,ios.name as status_name')
			->where($where)->select();
		return $stockin;
	}

	public function isCreateStock($id){
		$where['si.refer_id'] = $id;
		$where['si.refer_type_id'] = 3;
		$where['si.status'] = array('neq',0);
		$stockin = M('stock_in')
			->alias('si')
			->join(' `warehouse` w on w.warehouse_id = si.warehouse_id','left')
			->join(' `user` u on u.user_id = si.user_id','left')
			->join(' `inout_state` ios on ios.id = si.status','left')
			->field('si.*,w.name as warehouse_name,u.fullname as user_name,ios.name as status_name')
			->where($where)->select();
		return $stockin;
	}

	public function getVendors($filter_name=''){
		return M('vendors')->limit(5)->where(array('vendor_name'=>array('like', "%$filter_name%")))->select();
	}
	public function addRo($data, $user_id){
		$ro_data['user_id'] = $user_id;
		$ro_data['date_added'] = date('Y-m-d H:i:s', time());
		$ro_data['status'] = 0;
		$ro_data['warehouse_id'] = $data['warehouse_id'];
		$ro_data['market'] = $data['market'];
		$ro_data['included_order_ids'] = $data['included_order_ids'];
		$total = 0;
		foreach ($data['products'] as $key => $product) {
			$total += $product['qty']*$product['unit_price'];
		}
		$ro_data['total'] = $total;

		if(!$data['id']){
			$ro_id = M('ro')->add($ro_data);
		}
		foreach ($data['products'] as $key => $product) {
			$product_data['ro_id'] = $ro_id;
			$product_data['product_id'] = $product['product_id'];
			$product_data['qty'] = $product['qty'];
			$product_data['delivered_qty'] = $product['delivered_qty']?$product['delivered_qty']:0;
			$product_data['unit_price'] = $product['unit_price'];
			$product_data['price'] = $product_data['qty']* $product_data['unit_price'];
			$product_data['status'] = $product['status']?$product['status']:0;
			$product_data['name'] = $product['name'];
			$product_data['product_code'] = $product['product_code'];
			$product_data['sku'] = $product['sku'];
			$product_data['comment'] = $product['comment'];
			$product_data['sort_order'] = 0;
			$product_data['option_id'] = $product['option']['product_option_value_id'];
			$product_data['option_name'] = $product['option']['name'];
			$product_data['packing_no'] = M('product')->where('product_id ='.$product['product_id'])->getField('packing_no');
			M('ro_product')->add($product_data);
		}
		return $ro_id;
	}

	public function deleteRo($id) {
		$where['ro_id'] = $id;
		$products = M('ro_product');
		$products->where($where)->delete();
		$ro = M('ro');
		$ro->where(array('id'=>$id))->delete();
	}

	public function setStock($ro_id, $to_save_stock) {

		//修改ro单的qty
		$ro_product = M('ro_product');
		$ro = M('ro');

		foreach ($to_save_stock as $ro_product_id => $qty) {

			$rp = $ro_product->where(array('id'=>$ro_product_id, 'ro_id'=>$ro_id))->select();
			$rototal = $ro->where('id='.$ro_id)->getField('total');
			
			//修改ro_product表
			$data['qty'] = $rp[0]['qty']+$qty;
			$data['price'] = floatval($data['qty']*$rp[0]['unit_price']);
			$ro_product->where(array('id'=>$ro_product_id, 'ro_id'=>$ro_id))->save($data);
			
			//修改ro表
			$rodata['total'] = floatval($rototal+$qty*$rp[0]['unit_price']);
			$ro->where('id='.$ro_id)->save($rodata);

			//添加入库历史记录
			$comment = "操作数量："."<br/>".$rp[0]['name']." ： 	".$qty;
			$this->addRoHistory($ro_id, $this->user->getId(), $this->user->getUserName(), $comment);
		}
	}

	public function changeRoStatus($ro_id, $ro_status) {
		$ro = M('ro');
		$ro->data(array('id'=>$ro_id, 'status'=>$ro_status))->save();
	}

	public function findOrderWithoutVendor($orders) {
		$order_query = M('order_product op')
		->join('left join vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor is null')
		->field('v.vendor as vendor_id, op.name as name')
		->select();
		return $order_query;
	}

	public function getUserIdByVendorId($vendor_id) {
		$query = M('vendors')
		->where('vendor_id='.$vendor_id)
		->field('user_id')
		->find();
		if($query) {
			return $query['user_id'];		
		} else {
			return 0;
		}
	}

	public function getVendorIdsFromOrders($orders) {
		$order_query = M('order_product op')
		->join('vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ')')
		->field('v.vendor as vendor_id')
		->group('v.vendor')
		->select();
		return $order_query;
	}

	public function getPoDataFromOrders($orders, $vendor_id=0) {
		$order_query = M('order_product op')
		->join('left join order_option oo on oo.order_product_id=op.order_product_id')
		->join('vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor="' . $vendor_id . '"')
		->group('op.product_id, oo.product_option_value_id')
		->field('v.vendor as vendor_id, op.product_id as product_id, v.product_cost as unit_price, 
			v.product_cost*op.quantity as price, op.name as name, oo.product_option_value_id as option_id, oo.value as option_name, "" as sku, "" as comment, SUM(op.quantity) as qty, op.order_id')
		->select();
		return $order_query;
	}

	public function setPoGeneratedFlag($orders) {
		foreach ($orders as $order_id) {
			$Order = M('order');
			$Order->where(array('order_id'=>$order_id))->setInc('po_generated');
		}
	}

	public function getToBillPo($cur_month, $end_month, $billed_po_ids, $vendor_id = 0) {
		$complete_status = array('4','6'); // 4=>'已收货', 6=>'审核完成'
		$map['date_added'] = array('between',array($end_month, $cur_month));
		$map['status'] = array('in', $complete_status);
		if(!empty($billed_po_ids)) {
			$map['id'] = array('not in', $billed_po_ids);	
		}
		$map['vendor_id'] = array('eq', $vendor_id);
		$query = M('po')->where($map)->select();
		return $query;
	}

	public function getVendorIdsFromPo($cur_month, $end_month, $billed_po_ids) {
    $complete_status = array('4','6'); // 4=>'已收货', 6=>'审核完成'
		$map['date_added'] = array('between',array($end_month, $cur_month));
		$map['status'] = array('in', $complete_status);
		if(!empty($billed_po_ids)) {
			$map['id'] = array('not in', $billed_po_ids);	
		}
		$query = M('po')->where($map)->group('vendor_id')->field('vendor_id')->select();
		return $query;
  }

  public function addRoHistory($ro_id, $user_id, $operator_name, $comment) {
  	$roHistory = M('ro_history');
  	$data = array('ro_id'=>$ro_id, 'user_id'=>$user_id, 
  		'operator_name'=>$operator_name, 'comment'=>$comment, 
  		'notify'=>0, 'date_added'=>date('Y-m-d H:i:s'));
  	$roHistory->data($data)->add();
  }

  public function setPoTotal($id, $new_total) {
  	$po = M('po');
		$po->where(array('id'=>$id, 'status'=>3))->data(array('total'=>$new_total))->save();
  }

  public function addProductInitiai($ro_id){
  	$ro_product_model = M('ro_product');
  	$map['ro.id'] = $ro_id;
  	$ro_product_data = $ro_product_model
  	->alias('rp')
  	->join('left join `ro` ON ro.id=rp.ro_id')
  	->join('left join `logcenters` lg ON lg.user_id=ro.user_id')
  	->field('rp.product_id,rp.option_id,lg.logcenter_id,rp.qty')
  	->where($map)
  	->select();

  	$product_initiai_model = M('product_initiai');
  	foreach ($ro_product_data as $key => $value) {
  		$initiai_map['option_id'] = $value['option_id']?$value['option_id']:-1;
  		$initiai_map['product_id'] = $value['product_id'];
  		$initiai_map['logcenter_id'] = $value['logcenter_id'];
  		$initiai_data = $product_initiai_model->where($initiai_map)->find();
  		if($initiai_data){
  			$edit_initiai_data['real_initiai'] = $value['qty'] + $initiai_data['real_initiai'];
  			$product_initiai_model->data($edit_initiai_data)->where($initiai_map)->save();
  		}else{
  			$edit_initiai_data['real_initiai'] = $value['qty'];
  			$edit_initiai_data['option_id'] = $value['option_id']?$value['option_id']:-1;
  			$edit_initiai_data['product_id'] = $value['product_id'];
  			$edit_initiai_data['logcenter_id'] = $value['logcenter_id'];
  			$edit_initiai_data['date_added'] = date('Y-m-d h:i:s',time());
  			$edit_initiai_data['initial_number'] = 0 ;
  			$product_initiai_model->data($edit_initiai_data)->add();
  		}
  	}
  }

  public function addCoupons($data){
  	$coupon_model = M('coupon');
  	$customer_coupon_model = M('customer_coupon');
  	$coupon_data = array(
  		'type' 		=> 'F',
  		'logged'	=> 1,
  		'shipping'	=> 0,
  		'date_start'=> '2017-02-10',
  		'date_end'	=> '2017-03-10',
  		'uses_total'=> 1,
  		'uses_customer'=> '1',
  		'status'	=> 1,
  		'date_added'=> date('Y-m-d H-i-s',time()),
  		);
  	foreach ($data as $key => $value) {
  		$coupon_data['code'] = substr(sha1(uniqid(mt_rand(), true)), 0, 10); 

  		$coupon_data['name'] = $value['reward_name'];
  		$coupon_data['discount'] = $value['discount'];
  		$coupon_data['total'] = $value['total'];
  		$coupon_id = $coupon_model->data($coupon_data)->add();
  		$customer_coupon_data['coupon_id'] = $coupon_id;
  		$customer_coupon_data['customer_id'] = $value['customer_id'];
  		$customer_coupon_model->data($customer_coupon_data)->add();
  	}

  }

    /* 
     * 根据 ro.market 返回超市名称+联系电话
     */
    public function getMarketInfoByMarket($customerId) {
        $sql = "SELECT `telephone`,`company_name` FROM `".DB_PREFIX."customer` WHERE `customer_id`='".$customerId."'";
        $query = $this->db->query($sql);
        return $query->row['company_name'].'（'.$query->row['telephone'].'）';
    }

    /* 
     * 根据 ro.included_order_ids 返回超市名称+联系电话
     */
    public function getMarketInfoByOrderIds($orderIds) {
        $orderId = intval(array_shift(explode(',', $orderIds)));
        $sql = "SELECT `telephone`,`company_name` FROM `".DB_PREFIX."customer` WHERE `customer_id`=(SELECT `customer_id` FROM `order` WHERE `order_id`='".$orderId."')";
        $query = $this->db->query($sql);
        return $query->row['company_name'].'（'.$query->row['telephone'].'）';
    }
}