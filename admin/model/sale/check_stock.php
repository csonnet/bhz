<?php
class ModelSaleCheckStock extends Model {
	public function getList1($page, $filter){

		$order_product = $this->getOrderOptionStock($filter);

		$po_product = $this->getPoOptionStock($filter);

		$trim_product = $this->getTrimOptionStock($filter);

		$ro_product = $this->getRoOptionStock($filter);

		$src_requisition_product = $this->getSrcRequisitionOptionStock($filter);
	
		$des_requisition_product = $this->getDesRequisitionOptionStock($filter);

		$product_list = $this->getFilterOptionProductsList($page,$filter);

		foreach ($product_list as $keyp => $valuep) {
  			$product_list[$keyp]['src_requisition'] = 0;
			$product_list[$keyp]['des_requisition'] = 0;
			$product_list[$keyp]['trim'] = 0;
			$product_list[$keyp]['ro'] = 0;
			$product_list[$keyp]['po'] = 0;
			$product_list[$keyp]['order'] = 0;
			$initiai_map = array(
				'product_id' => $valuep['product_id'],
				'option_id'	 => $valuep['product_option_value_id']?$valuep['product_option_value_id']:array('in',array(-1,0)),
				'logcenter_id'=>$filter['filter_logcenter']?$filter['filter_logcenter']:'',
				);

			$initiai = M('product_initiai')->where($initiai_map)->getField('sum(initial_number)');

			$product_list[$keyp]['initiai'] = $initiai?$initiai:0;
			
  			foreach ($src_requisition_product as $key1 => $value1) {
  			
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$product_list[$keyp]['src_requisition'] += $value1['requisitionp_qty'];
				}
			}

			foreach ($order_product as $key5 => $value5) {
				if(($valuep['product_id'] == $value5['product_id'])&&($valuep['product_option_value_id'] == $value5['option_id'])){
					$product_list[$keyp]['order'] += $value5['orderp_qty'];
				}
			}

			foreach ($des_requisition_product as $key1 => $value1) {
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$product_list[$keyp]['des_requisition'] += $value1['requisitionp_qty'];
				}
			}

			foreach ($trim_product as $key2 => $value2) {
				if(($valuep['product_id'] == $value2['product_id'])&&($valuep['product_option_value_id'] == $value2['option_id'])){
					$product_list[$keyp]['trim'] += $value2['trimp_qty'];
				}
			}

			foreach ($ro_product as $key3 => $value3) {
				if(($valuep['product_id'] == $value3['product_id'])&&($valuep['product_option_value_id'] == $value3['option_id'])){
					$product_list[$keyp]['ro'] += $value3['rop_qty'];
				}
			}

			foreach ($po_product as $key4 => $value4) {
				if(($valuep['product_id'] == $value4['product_id'])&&($valuep['product_option_value_id'] == $value4['option_id'])){
					$product_list[$keyp]['po'] += $value4['pop_qty'];
				}
			}
			$product_list[$keyp]['change'] = $product_list[$keyp]['po']+$product_list[$keyp]['ro'] +$product_list[$keyp]['des_requisition']+$product_list[$keyp]['trim']-$product_list[$keyp]['src_requisition']-$product_list[$keyp]['order'];
			$product_list[$keyp]['last'] = $product_list[$keyp]['change'] + $product_list[$keyp]['initiai'];

  		}

		// foreach ($product_data as $keyp => $valuep) {
		// 	$product_data[$keyp]['src_requisition'] = 0;
		// 	$product_data[$keyp]['des_requisition'] = 0;
		// 	$product_data[$keyp]['trim'] = 0;
		// 	$product_data[$keyp]['ro'] = 0;
		// 	$product_data[$keyp]['po'] = 0;
		// 	$product_data[$keyp]['order'] = 0;
		// 	foreach ($src_requisition_product_data as $key1 => $value1) {
		// 		if($valuep['product_id'] == $value1['product_id']){
		// 			$product_data[$keyp]['src_requisition'] = $value1['requisitionp_qty'];
		// 		}
		// 	}

		// 	foreach ($order_product_data as $key5 => $value5) {
		// 		if($valuep['product_id'] == $value5['product_id']){
		// 			$product_data[$keyp]['order'] = $value5['orderp_qty'];
		// 		}
		// 	}

		// 	foreach ($des_requisition_product_data as $key1 => $value1) {
		// 		if($valuep['product_id'] == $value1['product_id']){
		// 			$product_data[$keyp]['des_requisition'] = $value1['requisitionp_qty'];
		// 		}
		// 	}

		// 	foreach ($trim_product_data as $key2 => $value2) {
		// 		if($valuep['product_id'] == $value2['product_id']){
		// 			$product_data[$keyp]['trim'] = $value2['trimp_qty'];
		// 		}
		// 	}

		// 	foreach ($ro_product_data as $key3 => $value3) {
		// 		if($valuep['product_id'] == $value3['product_id']){
		// 			$product_data[$keyp]['ro'] = $value3['rop_qty'];
		// 		}
		// 	}

		// 	foreach ($po_product_data as $key4 => $value4) {
		// 		if($valuep['product_id'] == $value4['product_id']){
		// 			$product_data[$keyp]['po'] = $value4['pop_qty'];
		// 		}
		// 	}
		// 	$product_data[$keyp]['change'] = $product_data[$keyp]['po']+$product_data[$keyp]['ro'] +$product_data[$keyp]['des_requisition']+$product_data[$keyp]['trim']-$product_data[$keyp]['src_requisition']-$product_data[$keyp]['order'];
		// }

		return $product_list;
	}

	public function getList2($page, $filter){
		$filter['filter_date_end'] = $filter['filter_date_start'];
		$filter['filter_date_start'] = $this->getStartTime($filter['filter_logcenter']);

		$order_product_data = $this->getOrderOptionStock($filter);

		$po_product_data = $this->getPoOptionStock($filter);

		$trim_product_data = $this->getTrimOptionStock($filter);
		
		$ro_product_data = $this->getRoOptionStock($filter);
		
		$src_requisition_product_data = $this->getSrcRequisitionOptionStock($filter);
		
		$des_requisition_product_data = $this->getDesRequisitionOptionStock($filter);

		$product_data = $this->getFilterOptionProductsList($page,$filter);

		foreach ($product_data as $keyp => $valuep) {
			$product_data[$keyp]['src_requisition'] = 0;
			$product_data[$keyp]['des_requisition'] = 0;
			$product_data[$keyp]['trim'] = 0;
			$product_data[$keyp]['ro'] = 0;
			$product_data[$keyp]['po'] = 0;
			$product_data[$keyp]['order'] = 0;
			foreach ($src_requisition_product_data as $key1 => $value1) {
				if($valuep['product_id'] == $value1['product_id']){
					$product_data[$keyp]['src_requisition'] = $value1['requisitionp_qty'];
				}
			}

			foreach ($order_product_data as $key5 => $value5) {
				if($valuep['product_id'] == $value5['product_id']){
					$product_data[$keyp]['order'] = $value5['orderp_qty'];
				}
			}

			foreach ($des_requisition_product_data as $key1 => $value1) {
				if($valuep['product_id'] == $value1['product_id']){
					$product_data[$keyp]['des_requisition'] = $value1['requisitionp_qty'];
				}
			}

			foreach ($trim_product_data as $key2 => $value2) {
				if($valuep['product_id'] == $value2['product_id']){
					$product_data[$keyp]['trim'] = $value2['trimp_qty'];
				}
			}

			foreach ($ro_product_data as $key3 => $value3) {
				if($valuep['product_id'] == $value3['product_id']){
					$product_data[$keyp]['ro'] = $value3['rop_qty'];
				}
			}

			foreach ($po_product_data as $key4 => $value4) {
				if($valuep['product_id'] == $value4['product_id']){
					$product_data[$keyp]['po'] = $value4['pop_qty'];
				}
			}
			$product_data[$keyp]['change'] = $product_data[$keyp]['po']+$product_data[$keyp]['ro'] +$product_data[$keyp]['des_requisition']+$product_data[$keyp]['trim']-$product_data[$keyp]['src_requisition']-$product_data[$keyp]['order'];
		}
		return $product_data;
	}

	public function getList($page = 1,$filter){
		$getList1 = $this->getList1($page,$filter);

		$getList2 = $this->getList2($page,$filter);

		foreach ($getList1 as $key => $value) {

			$map['logcenter_id'] = $filter['filter_logcenter'];
			$map['product_id'] = $value['product_id'];
			$map['option_id'] = $value['product_option_value_id']?$value['product_option_value_id']:array('in',array(-1,0));
			$initiai = M('product_initiai')->where($map)->getField('sum(initial_number)');
			$initiai = $initiai?$initiai:0;
			$getList1[$key]['initiai'] = $initiai + $getList2[$key]['change'];
			$getList1[$key]['balance'] = $getList1[$key]['initiai'] + $getList1[$key]['change'];
		}
		return $getList1;
	}

	public function getFilterProducts($page,$filter){


		$product_model = M('product');
		// $products_map['pd.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$products_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		
		$products_map['vs.vendor_name'] = array('like','%'.$filter['filter_vendor_name'].'%');
		//var_dump($filter);die();
		$product_data = $product_model
		->alias('p')
		->join('left join product_description pd ON p.product_id=pd.product_id')
		->join('left join vendor v ON v.vproduct_id=p.product_id')
		->join('left join vendors vs ON v.vendor=vs.vendor_id')
		->join('left join `product_special` ps ON ps.product_id=p.product_id')
		->field('p.product_id,p.sku,p.price,pd.name,v.product_cost,ps.price as special,ifnull((select sum(real_initiai) from `product_initiai` pi where pi.product_id=p.product_id and logcenter_id='.$filter['filter_logcenter'].' group by pi.product_id),0) as total_real_initiai')
		->page($page,$this->config->get('config_limit_admin'))
		->where($products_map)
		->order("ifnull((select sum(real_initiai) from `product_initiai` pi where pi.product_id=p.product_id and logcenter_id=".$filter['filter_logcenter']." group by pi.product_id),0) " .$filter['order'])
		
		->select();

		return $product_data;
	}

	public function getFilterOptionProductsList($page,$filter){
		$product_model = M('product');
		// $products_map['pd.name'] = array('like','%'.$filter['filter_product_name'].'%');
		$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		$products_map['vs.vendor_name'] = array('like','%'.$filter['filter_vendor_name'].'%');
		if(is_array($filter['filter_sku'])){
			$products_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}

		$nooption_data = [];
		$option_data = [];
		
		/*默认没有option选项*/
		/*$nooption_data = $product_model
		->alias('p')
		->join('left join product_description pd ON p.product_id=pd.product_id')
		->join('left join vendor v ON v.vproduct_id=p.product_id')
		->join('left join vendors vs ON v.vendor=vs.vendor_id')
		->join('left join `product_special` ps ON ps.product_id=p.product_id')
		->field('p.product_id,p.sku,p.price,pd.name,v.product_cost,ps.price as special,-1 as product_option_value_id')
		->page($page,$this->config->get('config_limit_admin'))
		->where($products_map)
		->order("ifnull((select sum(real_initiai) from `product_initiai` pi where pi.product_id=p.product_id and logcenter_id=".$filter['filter_logcenter']." group by pi.product_id),0) " .$filter['order'])
		->select();*/
		/*默认没有option选项*/

		/*有option选项*/
		$option_data = $product_model
		->alias('p')
		->join('left join product_description pd ON p.product_id=pd.product_id')
		->join('left join vendor v ON v.vproduct_id=p.product_id')
		->join('left join vendors vs ON v.vendor=vs.vendor_id')
		->join('left join `product_special` ps ON ps.product_id=p.product_id')
		->join('left join product_option_value pov ON pov.product_id=p.product_id')
		->join('left join option_value_description ovd ON ovd.option_value_id = pov.option_value_id')
		->field('p.product_id,p.sku,p.price,pd.name,v.product_cost,ps.price as special,ifnull(pov.product_option_value_id,-1) as product_option_value_id,pov.option_value_id,ovd.name as option_name')
		->page($page,$this->config->get('config_limit_admin'))
		->where($products_map)
		->order("ifnull((select sum(real_initiai) from `product_initiai` pi where pi.product_id=p.product_id and pi.option_id=pov.product_option_value_id and logcenter_id=".$filter['filter_logcenter']." group by pi.product_id),0) " .$filter['order'])
		->select();
		/*有option选项*/

		$product_data = array_merge($nooption_data,$option_data);

		return $product_data;
	}

	public function getFilterOptionProducts($filter){
		$product_model = M('product');
		// $products_map['pd.name'] = array('like','%'.$filter['filter_product_name'].'%');
		$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		$products_map['vs.vendor_name'] = array('like','%'.$filter['filter_vendor_name'].'%');
		if(is_array($filter['filter_sku'])){
			$products_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$product_data = $product_model
		->alias('p')
		->join('left join product_description pd ON p.product_id=pd.product_id')
		->join('left join vendor v ON v.vproduct_id=p.product_id')
		->join('left join vendors vs ON v.vendor=vs.vendor_id')
		->join('left join product_option_value pov ON pov.product_id=p.product_id')
		->join('left join option_value_description ovd ON ovd.option_value_id = pov.option_value_id')
		->field('p.product_id,p.sku,p.price,pd.name,v.product_cost,pov.product_option_value_id,pov.option_value_id,ovd.name as option_name')
		->where($products_map)
		->select();

		return $product_data;
	}

	public function getDesRequisitionStock($filter){
		$requisition_product_model = M('requisition_product');
		$des_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		// $des_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$des_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$des_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$des_requisition_map['requisition.status'] = count(getRequisitionStatus());
		$des_requisition_map['requisition.des_logcenter'] = $filter['filter_logcenter'];
		$des_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty')
		->where($des_requisition_map)
		->group('requisitionp.product_id')
		->select();
		return $des_requisition_product_data;
	}

	public function getDesRequisitionOptionStock($filter){
		$requisition_product_model = M('requisition_product');
		$des_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		// $des_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$des_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$des_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$des_requisition_map['requisition.status'] = count(getRequisitionStatus());
		$des_requisition_map['requisition.des_logcenter'] = $filter['filter_logcenter'];
		$des_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty,ifnull(requisitionp.option_id,-1) as option_id')
		->where($des_requisition_map)
		->group('requisitionp.product_id,requisitionp.option_id')
		->select();
		return $des_requisition_product_data;
	}

	public function getSrcRequisitionStock($filter){
		$requisition_product_model = M('requisition_product');
		$src_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		$status_array = array(count(getRequisitionStatus()),count(getRequisitionStatus())-1);
		$src_requisition_map['requisition.status'] = array('in',$status_array);
		// $src_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$src_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$src_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$src_requisition_map['requisition.src_logcenter'] = $filter['filter_logcenter'];
		$src_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty')
		->where($src_requisition_map)
		->group('requisitionp.product_id')
		->select();
		return $src_requisition_product_data;
	}

	public function getSrcRequisitionOptionStock($filter){
		$requisition_product_model = M('requisition_product');
		$src_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		$status_array = array(count(getRequisitionStatus()),count(getRequisitionStatus())-1);
		$src_requisition_map['requisition.status'] = array('in',$status_array);
		// $src_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$src_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$src_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$src_requisition_map['requisition.src_logcenter'] = $filter['filter_logcenter'];
		$src_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty,ifnull(requisitionp.option_id,-1) as option_id')
		->where($src_requisition_map)
		->group('requisitionp.product_id,requisitionp.option_id')
		->select();

		return $src_requisition_product_data;
	}

	public function getRoStock($filter){
		$ro_product_model = M('ro_product');
		$ro_map['_string'] = "ro.deliver_time>='".$filter['filter_date_start']."' AND ro.deliver_time<'".$filter['filter_date_end']."'";
		// $ro_map['rop.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$ro_map['rop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$ro_map['rop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$ro_map['ro.status'] = count(getRoStatus())-1;
		$ro_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$ro_product_data = $ro_product_model
		->alias('rop')
		->join("left join `ro` ON ro.id=rop.ro_id")
		->join('left join `product` p ON rop.product_id=p.product_id')
		->join('left join `logcenters` lg ON ro.user_id=lg.user_id')
		->field('p.*,ro.*,rop.*,sum(rop.qty) as rop_qty')
		->where($ro_map)
		->group('rop.product_id')
		->select();
		return $ro_product_data;
	}

	public function getRoOptionStock($filter){
		$ro_product_model = M('ro_product');
		$ro_map['_string'] = "ro.deliver_time>='".$filter['filter_date_start']."' AND ro.deliver_time<'".$filter['filter_date_end']."'";
		// $ro_map['rop.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$ro_map['rop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$ro_map['rop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$ro_map['ro.status'] = count(getRoStatus())-1;
		$ro_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$ro_product_data = $ro_product_model
		->alias('rop')
		->join("left join `ro` ON ro.id=rop.ro_id")
		->join('left join `product` p ON rop.product_id=p.product_id')
		->join('left join `logcenters` lg ON ro.user_id=lg.user_id')
		->field('p.*,ro.*,rop.*,sum(rop.qty) as rop_qty,ifnull(rop.option_id,-1) as option_id')
		->where($ro_map)
		->group('rop.product_id,rop.option_id')
		->select();
		foreach ($ro_product_data as $key => $value) {
			if($value['option_id'] == '0'){
				$ro_product_data[$key]['option_id'] = '-1';
			} 
		}
		return $ro_product_data;
	}

	public function getTrimStock($filter){
		$trim_product_model = M('trim_product');
		$trim_map['_string'] = "trim.deliver_time>='".$filter['filter_date_start']."' AND trim.deliver_time<'".$filter['filter_date_end']."'";
		$trim_map['trim.status'] = count(getTrimOrderStatus());
		// $trim_map['trimp.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$trim_map['trimp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$trim_map['trimp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$trim_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$trim_product_data = $trim_product_model
		->alias('trimp')
		->join("left join `trim` ON trim.id=trimp.trim_id")
		->join('left join `product` p ON trimp.product_id=p.product_id')
		->join('left join `logcenters` lg ON trim.user_id=lg.user_id')
		->field('p.*,trim.*,trimp.*,sum(trimp.qty) as trimp_qty')
		->where($trim_map)
		->group('trimp.product_id')
		
		->select();
	
		return $trim_product_data;
	}

	public function getTrimOptionStock($filter){
		$trim_product_model = M('trim_product');
		$trim_map['_string'] = "trim.deliver_time>='".$filter['filter_date_start']."' AND trim.deliver_time<'".$filter['filter_date_end']."'";
		$trim_map['trim.status'] = count(getTrimOrderStatus());
		// $trim_map['trimp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$trim_map['trimp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$trim_map['trimp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$trim_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$trim_product_data = $trim_product_model
		->alias('trimp')
		->join("left join `trim` ON trim.id=trimp.trim_id")
		->join('left join `product` p ON trimp.product_id=p.product_id')
		->join('left join `logcenters` lg ON trim.user_id=lg.user_id')
		->field('p.*,trim.*,trimp.*,sum(trimp.qty) as trimp_qty,ifnull(trimp.option_id,-1) as option_id')
		->where($trim_map)
		->group('trimp.product_id,trimp.option_id')
		->select();
		foreach ($trim_product_data as $key => $value) {
			if($value['option_id'] == '0'){
				$trim_product_data[$key]['option_id'] = '-1';
			} 
		}
		return $trim_product_data;
	}

	public function getOrderStock($filter){
		$order_product_model = M('order_product');
		// $order_map['_string'] = "orderp.shipped_time>='".$filter['filter_date_start']."' AND orderp.shipped_time<'".$filter['filter_date_end']."'";

		$order_map['_string'] = "o.date_added>='".$filter['filter_date_start']."' AND o.date_added<'".$filter['filter_date_end']."'";

		// $order_map['orderp.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$order_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$order_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$order_map['o.logcenter_id'] = $filter['filter_logcenter'];
		//1待付款,2待发货,3待收货,5完成,7退换货,11,已退款,13拒付,16,无效,17部分发货,18全部发货,19已结束发货
		$order_map['o.order_status_id'] = array('in',array(1,2,3,5,11,17,18,19));
		
		//$order_map['orderp.ship_status'] = 1;
		$order_product_data = $order_product_model
		->alias('orderp')
		->join("left join `order` o ON o.order_id=orderp.order_id")
		->join('left join `product` p ON orderp.product_id=p.product_id')
		->field('p.*,o.*,orderp.*,sum(orderp.quantity) as orderp_qty')
		->where($order_map)
		->group('orderp.product_id')
		
		->select();	

		//var_dump($order_product_data);die();
		return $order_product_data;
	}

	public function getOrderOptionStock($filter){

		$order_product_model = M('order_product');
		// $order_map['_string'] = "orderp.shipped_time>='".$filter['filter_date_start']."' AND orderp.shipped_time<'".$filter['filter_date_end']."'";
		$order_map['_string'] = "o.date_added>='".$filter['filter_date_start']."' AND o.date_added<'".$filter['filter_date_end']."'";
		// $order_map['orderp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$order_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$order_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$order_map['o.logcenter_id'] = $filter['filter_logcenter'];
		//1待付款,2待发货,3待收货,5完成,7退换货,11,已退款,13拒付,16,无效,17部分发货,18全部发货,19已结束发货
		$order_map['o.order_status_id'] = array('in',array(2,3,5,11,17,18,19));
		$order_product_data = $order_product_model
		->alias('orderp')
		->join("left join `order` o ON o.order_id=orderp.order_id")
		->join("left join `order_option` oo ON oo.order_product_id=orderp.order_product_id")
		->join('left join `product` p ON orderp.product_id=p.product_id')
		->field('p.*,o.*,orderp.*,sum(orderp.quantity) as orderp_qty,ifnull(oo.product_option_value_id,-1) as option_id')
		->where($order_map)
		->group('orderp.product_id,oo.product_option_value_id')
		
		->select();	
		foreach ($order_product_data as $key => $value) {
			if($value['option_id'] == '0'){
				$order_product_data[$key]['option_id'] = '-1';
			}
		}
		
		return $order_product_data;
	}

	public function getPoStock($filter){
		$po_product_model = M('po_product');
		$po_map['_string'] = "po.deliver_time>='".$filter['filter_date_start']."' AND po.deliver_time<'".$filter['filter_date_end']."'";
		// $po_map['pop.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$po_map['pop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$po_map['pop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$po_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$po_map['po.status'] = array('gt',2);
		$po_product_data = $po_product_model
		->alias('pop')
		->join("left join `po` ON po.id=pop.po_id")
		->join('left join `product` p ON pop.product_id=p.product_id')
		->join('left join `logcenters` lg ON po.user_id=lg.user_id')
		->field('p.*,po.*,pop.*,sum(pop.delivered_qty) as pop_qty')
		->where($po_map)
		->group('pop.product_id')
		
		->select();
		
		return $po_product_data;
	}

	public function getPoOptionStock($filter){
		$po_product_model = M('po_product');
		$po_map['_string'] = "po.deliver_time>='".$filter['filter_date_start']."' AND po.deliver_time<'".$filter['filter_date_end']."'";
		// $po_map['pop.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$po_map['pop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$po_map['pop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$po_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$po_map['po.status'] = array('gt',2);
		$po_product_data = $po_product_model
		->alias('pop')
		->join("left join `po` ON po.id=pop.po_id")
		->join('left join `product` p ON pop.product_id=p.product_id')
		->join('left join `logcenters` lg ON po.user_id=lg.user_id')
		->field('p.*,po.*,pop.*,sum(pop.delivered_qty) as pop_qty,ifnull(pop.option_id,-1) as option_id')
		->where($po_map)
		
		->group('pop.product_id,pop.option_id')
		->select();
		foreach ($po_product_data as $key => $value) {
			if($value['option_id'] == '0'){
				$po_product_data[$key]['option_id'] = '-1';
			} 
		}
		return $po_product_data;
	}

	public function getAllList($filter){
		$requisitionlist = M('requisition')
		->join('(select vendors.user_id,  vendor_name from vendors) v on v.user_id = requisition.vendor_id','left')
		->join('(select count(1) as count, requisition_id from requisition_product group by requisition_id) ppc on ppc.requisition_id = requisition.id','left')
		->join('(select user_id, company as logcenter_company, address_1 as logcenter_address, logcenter_name from logcenters) lg on lg.user_id = requisition.user_id','left')
		->order('requisition.id DESC')
		->where($filter)->select();
		return $requisitionlist;
	}
	public function getAllProductList($filter){
		$requisitionlist = M('po_product')
		->alias('pop')
		->join('LEFT JOIN `po` po ON po.id=pop.po_id')
		->join('(select vendors.user_id,  vendor_name from vendors) v on v.user_id = po.vendor_id','left')
		->join('(select count(1) as count, po_id from po_product group by po_id) ppc on ppc.po_id = po.id','left')
		->join('(select user_id, company as logcenter_company, address_1 as logcenter_address, logcenter_name from logcenters) lg on lg.user_id = po.user_id','left')
		->order('po.id DESC')
		->group('pop.product_id,po.id')
		->where($filter)->select();
		return $requisitionlist;
	}
	public function getListCount($filter){

		$product_model = M('product');

		if(is_array($filter['filter_sku'])){
			$products_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$products_map['vs.vendor_name'] = array('like','%'.$filter['filter_vendor_name'].'%');
		$count = $product_model
		->alias('p')
		->join('left join product_description pd ON p.product_id=pd.product_id')
		->join('left join vendor v ON v.vproduct_id=p.product_id')
		->join('left join vendors vs ON v.vendor=vs.vendor_id')
		//->join('left join `product_special` ps ON ps.product_id=p.product_id')
		->join('left join product_option_value pov ON pov.product_id=p.product_id')
		//->join('left join option_value_description ovd ON ovd.option_value_id = pov.option_value_id')
		->where($products_map)
		//->fetchsql(true)
		->count();
		return $count;
	}
	public function getProducts($fitler){
		$res = M('product')->join('product_description pd on pd.product_id = product.product_id')
		->field('product.*, pd.*, vendor.product_cost')->join('vendor on vendor.vproduct_id = product.product_id')
		->where($fitler)
		->where(array('pd.language_id'=>(int)$this->config->get('config_language_id')))
		->limit(5)->select();
		return $res;
	}
	public function getRequisition($id){
		$requisition = M('requisition')->find($id);
		if($requisition){
			$src_logcenter_id = $requisition['src_logcenter'];
			$des_logcenter_id = $requisition['des_logcenter'];
			$user = M('user')->find($requisition['user_id']);
			$requisition['creator'] = $user;
			$map['logcenter_id'] = $src_logcenter_id;
			$src_logcenter = M('logcenters')->where($map)->find();
			//var_dump($logcenter_info);die();
			$src_zone = M('zone')->where('zone_id='.$src_logcenter_id)->getField('name');
			$src_logcenter['shipping_zone_city'] = $src_zone.$src_logcenter['city'];
			$requisition['src_logcenter_info'] = $src_logcenter;

			$map['logcenter_id'] = $des_logcenter_id;
			$des_logcenter = M('logcenters')->where($map)->find();
			$des_zone = M('zone')->where('zone_id='.$des_logcenter_id)->getField('name');
			$des_logcenter['shipping_zone_city'] = $des_zone.$des_logcenter['city'];
			$requisition['des_logcenter_info'] = $des_logcenter;
		}

		return $requisition;
	}
	public function getRequisitionProducts($id){
		$where['requisition_id'] = $id;
		$products = M('requisition_product')->where($where)->select();
		return $products;
	}
	public function getRequisitionHistory($id){
		$where['requisition_id'] = $id;
		$history = M('requisition_history')->where($where)->select();
		return $history;
	}
	public function getVendors($filter_name=''){
		return M('vendors')->limit(5)->where(array('vendor_name'=>array('like', "%$filter_name%")))->select();
	}
	public function addRequisition($data, $user_id){
		$requisition_data['user_id'] = $user_id;
		$requisition_data['date_added'] = date('Y-m-d H:i:s', time());
		$requisition_data['status'] = 1;
		$requisition_data['vendor_id'] = $data['vendor']['user_id'];
		$requisition_data['des_logcenter'] = $data['logcenter']['logcenter_id'];
		$requisition_data['src_logcenter'] = $this->user->getLP();
		$requisition_data['deliver_time'] = $data['deliver_time'];
		$requisition_data['included_order_ids'] = $data['included_order_ids'];
		$total = 0;
		foreach ($data['products'] as $key => $product) {
			$total += $product['qty']*$product['unit_price'];
		}
		$requisition_data['total'] = $total;

		if(!$data['id']){
			$requisition_id = M('requisition')->add($requisition_data);
		}
		foreach ($data['products'] as $key => $product) {
			$product_data['requisition_id'] = $requisition_id;
			$product_data['product_id'] = $product['product_id'];
			$product_data['qty'] = $product['qty'];
			$product_data['delivered_qty'] = $product['delivered_qty']?$product['delivered_qty']:0;
			$product_data['unit_price'] = $product['unit_price'];
			$product_data['price'] = $product_data['qty']* $product_data['unit_price'];
			$product_data['status'] = $product['status']?$product['status']:0;
			$product_data['name'] = $product['name'];
			$product_data['requisition_status'] = $product['requisition_status'];
			$product_data['sku'] = $product['sku'];
			$product_data['comment'] = $product['comment'];
			$product_data['sort_order'] = 0;
			$product_data['option_id'] = $product['option']['product_option_value_id'];
			$product_data['option_name'] = $product['option']['name'];
			$product_data['packing_no'] = M('product')->where('product_id ='.$product['product_id'])->getField('packing_no');
			M('requisition_product')->add($product_data);
		}
		return $requisition_id;
	}

	public function deleteRequisition($id) {
		$where['requisition_id'] = $id;
		$products = M('requisition_product');
		$products->where($where)->delete();
		$requisition = M('requisition');
		$requisition->where(array('id'=>$id))->delete();
	}

	public function setStock($requisition_id, $to_save_stock) {
		//添加入库历史记录
		
		//修改ro单的delivered_qty
		$requisition_product = M('requisition_product');
		foreach ($to_save_stock as $requisition_product_id => $qty) {
			$data['qty'] = $qty;
			$requisition_product->where(array('id'=>$requisition_product_id, 'requisition_id'=>$requisition_id))->setInc('qty', $qty);
			$requisition_product_name = $requisition_product->where(array('id'=>$requisition_product_id, 'requisition_id'=>$requisition_id))->getField('name');
			$comment = "操作数量："."<br/>".$requisition_product_name." ： 	".$qty;
			$this->addRequisitionHistory($requisition_id, $this->user->getId(), $this->user->getUserName(), $comment);
		}
	}

	public function updateRequisitionAcceptTime($id){
		$requisitionModel = M('requisition');
		$data['deliver_time'] = date('Y-m-d h:i:s',time());
		$where['id'] = $id;
		$requisitionModel
		->where($where)
		->data($data)
		->save();
	}

	public function setComment($requisition_id, $to_save_comment) {
		//添加入库历史记录

		//修改ro单的delivered_qty
		$requisition_product = M('requisition_product');
		foreach ($to_save_comment as $requisition_product_id => $comment) {
			$data['comment'] = $comment;
			$requisition_product
			->where(array('id'=>$requisition_product_id, 'requisition_id'=>$requisition_id))
			->data($data)
			->save();
			
		}
	}

	public function changeRequisitionStatus($requisition_id, $requisition_status) {
		$requisitionlist = M('requisition');
		$requisitionlist->data(array('id'=>$requisition_id, 'status'=>$requisition_status))->save();
	}

	public function findOrderWithoutVendor($orders) {
		$order_query = M('order_product op')
		->join('left join vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor is null')
		->field('v.vendor as vendor_id, op.name as name')
		->select();
		return $order_query;
	}

	public function getUserIdByVendorId($vendor_id) {
		$query = M('vendors')
		->where('vendor_id='.$vendor_id)
		->field('user_id')
		->find();
		if($query) {
			return $query['user_id'];		
		} else {
			return 0;
		}
	}

	public function getVendorIdsFromOrders($orders) {
		$order_query = M('order_product op')
		->join('vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ')')
		->field('v.vendor as vendor_id')
		->group('v.vendor')
		->select();
		return $order_query;
	}

	public function getPoDataFromOrders($orders, $vendor_id=0) {
		$order_query = M('order_product op')
		->join('left join order_option oo on oo.order_product_id=op.order_product_id')
		->join('vendor v on v.vproduct_id=op.product_id')
		->where('op.order_id in (' . implode(',', $orders) . ') AND v.vendor="' . $vendor_id . '"')
		->group('op.product_id, oo.product_option_value_id')
		->field('v.vendor as vendor_id, op.product_id as product_id, v.product_cost as unit_price, 
			v.product_cost*op.quantity as price, op.name as name, oo.product_option_value_id as option_id, oo.value as option_name, "" as sku, "" as comment, SUM(op.quantity) as qty, op.order_id')
		->select();
		return $order_query;
	}

	public function setPoGeneratedFlag($orders) {
		foreach ($orders as $order_id) {
			$Order = M('order');
			$Order->where(array('order_id'=>$order_id))->setInc('po_generated');
		}
	}

	public function getToBillPo($cur_month, $end_month, $billed_po_ids, $vendor_id = 0) {
		$complete_status = array('4','6'); // 4=>'已收货', 6=>'审核完成'
		$map['date_added'] = array('between',array($end_month, $cur_month));
		$map['status'] = array('in', $complete_status);
		if(!empty($billed_po_ids)) {
			$map['id'] = array('not in', $billed_po_ids);	
		}
		$map['vendor_id'] = array('eq', $vendor_id);
		$query = M('po')->where($map)->select();
		return $query;
	}

	public function getVendorIdsFromPo($cur_month, $end_month, $billed_po_ids) {
    $complete_status = array('4','6'); // 4=>'已收货', 6=>'审核完成'
		$map['date_added'] = array('between',array($end_month, $cur_month));
		$map['status'] = array('in', $complete_status);
		if(!empty($billed_po_ids)) {
			$map['id'] = array('not in', $billed_po_ids);	
		}
		$query = M('po')->where($map)->group('vendor_id')->field('vendor_id')->select();
		return $query;
  }

  public function addRequisitionHistory($requisition_id, $user_id, $operator_name, $comment) {
  	$requisitionHistory = M('requisition_history');
  	$data = array('requisition_id'=>$requisition_id, 'user_id'=>$user_id, 
  		'operator_name'=>$operator_name, 'comment'=>$comment, 
  		'notify'=>0, 'date_added'=>date('Y-m-d H:i:s'));
  	$requisitionHistory->data($data)->add();
  }

  public function setPoTotal($id, $new_total) {
  	$requisition = M('po');
		$requisition->where(array('id'=>$id, 'status'=>3))->data(array('total'=>$new_total))->save();
  }

  public function getOptionProductsByProductId($filter){

  	$getList1 = $this->getOptionProductsByProductIdFunction($filter);

  	$filter['filter_date_end'] = $filter['filter_date_start'];
		$filter['filter_date_start'] = $this->getStartTime($filter['filter_logcenter']);
  	$getList2 = $this->getOptionProductsByProductIdFunction($filter);
  	
  	foreach ($getList1 as $key => $value) {
			$map['logcenter_id'] = $filter['filter_logcenter'];
			$map['product_id'] = $value['product_id'];
			$map['option_id'] = $value['product_option_value_id']?$value['product_option_value_id']:array('in',array(-1,0));
			$initiai = M('product_initiai')->where($map)->getField('initial_number');
			$initiai = $initiai?$initiai:0;
			$getList1[$key]['initiai'] = $initiai + $getList2[$key]['change'];
			$getList1[$key]['balance'] = $getList1[$key]['initiai'] + $getList1[$key]['change'];
		}

	return $getList1;
  }

  public function getOptionProductsByProductIdFunction($filter){

  	$ro_stock = $this->getRoOptionStock($filter);

  	$trim_stock = $this->getTrimOptionStock($filter);
  
  	$order_stock = $this->getOrderOptionStock($filter);

  	$po_stock = $this->getPoOptionStock($filter);

  	$src_requisition_stock = $this->getSrcRequisitionOptionStock($filter);

  	$des_requisition_stock = $this->getDesRequisitionOptionStock($filter);

  	$stock = $this->getFilterOptionProducts($filter);
  	foreach ($stock as $keyp => $valuep) {
  			$stock[$keyp]['src_requisition'] = 0;
			$stock[$keyp]['des_requisition'] = 0;
			$stock[$keyp]['trim'] = 0;
			$stock[$keyp]['ro'] = 0;
			$stock[$keyp]['po'] = 0;
			$stock[$keyp]['order'] = 0;
			
  			foreach ($src_requisition_stock as $key1 => $value1) {
  			
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$stock[$keyp]['src_requisition'] = $value1['requisitionp_qty'];
				}
			}

			foreach ($order_stock as $key5 => $value5) {
				if(($valuep['product_id'] == $value5['product_id'])&&($valuep['product_option_value_id'] == $value5['option_id'])){
					$stock[$keyp]['order'] = $value5['orderp_qty'];
				}
			}

			foreach ($des_requisition_stock as $key1 => $value1) {
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$stock[$keyp]['des_requisition'] = $value1['requisitionp_qty'];
				}
			}

			foreach ($trim_stock as $key2 => $value2) {
				if(($valuep['product_id'] == $value2['product_id'])&&($valuep['product_option_value_id'] == $value2['option_id'])){
					$stock[$keyp]['trim'] = $value2['trimp_qty'];
				}
			}

			foreach ($ro_stock as $key3 => $value3) {
				if(($valuep['product_id'] == $value3['product_id'])&&($valuep['product_option_value_id'] == $value3['option_id'])){
					$stock[$keyp]['ro'] = $value3['rop_qty'];
				}
			}

			foreach ($po_stock as $key4 => $value4) {
				if(($valuep['product_id'] == $value4['product_id'])&&($valuep['product_option_value_id'] == $value4['option_id'])){
					$stock[$keyp]['po'] = $value4['pop_qty'];
				}
			}
			$stock[$keyp]['change'] = $stock[$keyp]['po']+$stock[$keyp]['ro'] +$stock[$keyp]['des_requisition']+$stock[$keyp]['trim']-$stock[$keyp]['src_requisition']-$stock[$keyp]['order'];
  	}
  	return $stock;
  	
  }
  public function getSkuByModel($model){

  	$product_model = M('product');
  	$map['model'] = array('like','%'.$model.'%');
  	$sku_data = $product_model
  	->where($map)
  	->field('sku')
  	->select();
  	foreach ($sku_data as $key => $value) {
  		$save_data[] = $value['sku']; 
  	}
  	return $save_data;
  }

  public function getRoStock2($filter){
		$ro_product_model = M('ro_product');
		$ro_map['_string'] = "ro.deliver_time>='".$filter['filter_date_start']."' AND ro.deliver_time<'".$filter['filter_date_end']."'";
		// $ro_map['rop.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$ro_map['rop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$ro_map['rop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$ro_map['ro.status'] = count(getRoStatus())-1;
		$ro_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		
		$ro_product_data = $ro_product_model
		->alias('rop')
		->join("left join `ro` ON ro.id=rop.ro_id")
		->join('left join `product` p ON rop.product_id=p.product_id')
		->join('left join `logcenters` lg ON ro.user_id=lg.user_id')
		->field('p.*,ro.*,rop.*,sum(rop.qty) as rop_qty,ifnull(rop.option_id,-1) as option_id')
		->where($ro_map)
	
		->group('rop.product_id,option_id')
		->select();
		return $ro_product_data;
	}

	public function getTrimStock2($filter){
		$trim_product_model = M('trim_product');
		$trim_map['_string'] = "trim.deliver_time>='".$filter['filter_date_start']."' AND trim.deliver_time<'".$filter['filter_date_end']."'";
		$trim_map['trim.status'] = count(getTrimOrderStatus());
		// $trim_map['trimp.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$trim_map['trimp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$trim_map['trimp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$trim_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$trim_product_data = $trim_product_model
		->alias('trimp')
		->join("left join `trim` ON trim.id=trimp.trim_id")
		->join('left join `product` p ON trimp.product_id=p.product_id')
		->join('left join `logcenters` lg ON trim.user_id=lg.user_id')
		->field('p.*,trim.*,trimp.*,sum(trimp.qty) as trimp_qty,ifnull(trimp.option_id,-1) as option_id')
		->where($trim_map)
		->group('trimp.product_id,option_id')
		->select();
		//print_r($trim_product_data);die();
		return $trim_product_data;
	}

	public function getPoStock2($filter){
		$po_product_model = M('po_product');
		$po_map['_string'] = "po.deliver_time>='".$filter['filter_date_start']."' AND po.deliver_time<'".$filter['filter_date_end']."'";
		// $po_map['pop.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$po_map['pop.sku'] = array('in',$filter['filter_sku']);
		}else{
			$po_map['pop.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$po_map['lg.logcenter_id'] = $filter['filter_logcenter'];
		$po_map['po.status'] = array('gt',2);
		$po_product_data = $po_product_model
		->alias('pop')
		->join("left join `po` ON po.id=pop.po_id")
		->join('left join `product` p ON pop.product_id=p.product_id')
		->join('left join `logcenters` lg ON po.user_id=lg.user_id')
		->field('p.*,po.*,pop.*,sum(pop.delivered_qty) as pop_qty,ifnull(pop.option_id,-1) as option_id')
		->where($po_map)
		
		->group('pop.product_id,option_id')
		->select();
		return $po_product_data;
	}

  	public function getOptionProducts($filter){
	  	$product_model = M('product');
	  	$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		$products_map['vs.vendor_name'] = array('like','%'.$filter['filter_vendor_name'].'%');
		if(is_array($filter['filter_sku'])){
			$products_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$products_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
	  	$product_data = $product_model
			->alias('p')
			->join('left join product_description pd ON p.product_id=pd.product_id')
			->join('left join vendor v ON v.vproduct_id=p.product_id')
			->join('left join `product_special` ps ON ps.product_id=p.product_id')
			->join('left join vendors vs ON v.vendor=vs.vendor_id')
			->join('left join product_option_value pov ON pov.product_id=p.product_id')
			->join('left join option_value_description ovd ON ovd.option_value_id = pov.option_value_id')
			->field('p.product_id,p.product_code,p.sku,p.price,pd.name,v.product_cost,ifnull(pov.product_option_value_id,-1) as product_option_value_id,pov.option_value_id,ovd.name as option_name,ps.price as special')
			->where($products_map)
			->select();
			return $product_data;
	  }

  public function getDesRequisitionStock2($filter){
		$requisition_product_model = M('requisition_product');
		$des_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		// $des_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');
		if(is_array($filter['filter_sku'])){
			$des_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$des_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$des_requisition_map['requisition.status'] = count(getRequisitionStatus());
		$des_requisition_map['requisition.des_logcenter'] = $filter['filter_logcenter'];
		$des_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty,ifnull(requisitionp.option_id,-1) as option_id')
		->where($des_requisition_map)
		->group('requisitionp.product_id')
		->select();
		return $des_requisition_product_data;
	}

	public function getSrcRequisitionStock2($filter){
		$requisition_product_model = M('requisition_product');
		$src_requisition_map['_string'] = "requisition.deliver_time>='".$filter['filter_date_start']."' AND requisition.deliver_time<'".$filter['filter_date_end']."'";
		$status_array = array(count(getRequisitionStatus()),count(getRequisitionStatus())-1);
		$src_requisition_map['requisition.status'] = array('in',$status_array);
		// $src_requisition_map['requisitionp.name'] = array('like','%'.$filter['filter_product_name'].'%');

		if(is_array($filter['filter_sku'])){
			$src_requisition_map['requisitionp.sku'] = array('in',$filter['filter_sku']);
		}else{
			$src_requisition_map['requisitionp.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		$src_requisition_map['requisition.src_logcenter'] = $filter['filter_logcenter'];
		$src_requisition_product_data = $requisition_product_model
		->alias('requisitionp')
		->join("left join `requisition` ON requisition.id=requisitionp.requisition_id")
		->join('left join `product` p ON requisitionp.product_id=p.product_id')
		->field('p.*,requisition.*,requisitionp.*,sum(requisitionp.qty) as requisitionp_qty,ifnull(requisitionp.option_id,-1) as option_id')
		->where($src_requisition_map)
		->group('requisitionp.product_id,option_id')
		->select();

		return $src_requisition_product_data;
	}

  public function getOrderStock2($filter){
		$order_product_model = M('order_product');

		$order_map['_string'] = "o.date_added>='".$filter['filter_date_start']."' AND o.date_added<'".$filter['filter_date_end']."'";

		if(is_array($filter['filter_sku'])){
			$order_map['p.sku'] = array('in',$filter['filter_sku']);
		}else{
			$order_map['p.sku'] = array('like','%'.$filter['filter_sku'].'%');
		}
		//$order_map['o.logcenter_id'] = $filter['filter_logcenter'];
		//1待付款,2待发货,3待收货,5完成,7退换货,11,已退款,13拒付,16,无效,17部分发货,18全部发货,19已结束发货
		$order_map['o.order_status_id'] = array('in',array(1,2,3,5,11,17,18,19));
		$order_map['o.logcenter_id'] = $filter['filter_logcenter'];

		//$order_map['orderp.ship_status'] = 1;
		$order_product_data = $order_product_model
		->alias('orderp')
		->join("left join `order` o ON o.order_id=orderp.order_id")
		->join('left join `product` p ON orderp.product_id=p.product_id')
		->join('left join `order_option` oo ON oo.order_product_id=orderp.order_product_id')
		->field('p.*,o.*,orderp.*,sum(orderp.quantity) as orderp_qty,ifnull(oo.product_option_value_id,-1) as option_id')
		->where($order_map)

		->group('orderp.product_id,option_id')
		->select();

		foreach ($order_product_data as $key => $value) {
			
				if($value['option_id']=='0'){
					$order_product_data[$key]['option_id'] = '-1';
					
				}
			}
			

		return $order_product_data;
	}

  public function getAllListProductsOption($filter){
  	$product_list = $this->getOptionProducts($filter);
  	$ro_product = $this->getRoStock2($filter);
  	$trim_product = $this->getTrimStock2($filter);
  	$po_product = $this->getPoStock2($filter);
  	$order_product = $this->getOrderStock2($filter);
  	$src_requisition_product = $this->getSrcRequisitionStock2($filter);
  	$des_requisition_product = $this->getDesRequisitionStock2($filter);
  	foreach ($product_list as $keyp => $valuep) {
            $product_list[$keyp]['product_id'] = $valuep['product_id'];
            $product_list[$keyp]['product_code'] = $valuep['product_code'].'0';
  			$product_list[$keyp]['src_requisition'] = 0;
			$product_list[$keyp]['des_requisition'] = 0;
			$product_list[$keyp]['trim'] = 0;
			$product_list[$keyp]['ro'] = 0;
			$product_list[$keyp]['po'] = 0;
			$product_list[$keyp]['order'] = 0;
			$initiai_map = array(
				'product_id' => $valuep['product_id'],
				'option_id'	 => $valuep['product_option_value_id']?$valuep['product_option_value_id']:array('in',array(-1,0)),
				'logcenter_id'=>$filter['filter_logcenter']?$filter['filter_logcenter']:'',
				);
			$initiai = M('product_initiai')->where($initiai_map)->getField('sum(initial_number)');

			$product_list[$keyp]['initiai'] = $initiai?$initiai:0;
			
  			foreach ($src_requisition_product as $key1 => $value1) {
  			
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$product_list[$keyp]['src_requisition'] += $value1['requisitionp_qty'];
				}
			}

			foreach ($order_product as $key5 => $value5) {
				if(($valuep['product_id'] == $value5['product_id'])&&($valuep['product_option_value_id'] == $value5['option_id'])){
					$product_list[$keyp]['order'] += $value5['orderp_qty'];
				}
			}

			foreach ($des_requisition_product as $key1 => $value1) {
				if(($valuep['product_id'] == $value1['product_id'])&&($valuep['product_option_value_id'] == $value1['option_id'])){
					$product_list[$keyp]['des_requisition'] += $value1['requisitionp_qty'];
				}
			}

			foreach ($trim_product as $key2 => $value2) {
				if(($valuep['product_id'] == $value2['product_id'])&&($valuep['product_option_value_id'] == $value2['option_id'])){
					$product_list[$keyp]['trim'] += $value2['trimp_qty'];
				}
			}

			foreach ($ro_product as $key3 => $value3) {
				if(($valuep['product_id'] == $value3['product_id'])&&($valuep['product_option_value_id'] == $value3['option_id'])){
					$product_list[$keyp]['ro'] += $value3['rop_qty'];
				}
			}

			foreach ($po_product as $key4 => $value4) {
				if(($valuep['product_id'] == $value4['product_id'])&&($valuep['product_option_value_id'] == $value4['option_id'])){
					$product_list[$keyp]['po'] += $value4['pop_qty'];
				}
			}
			$product_list[$keyp]['change'] = $product_list[$keyp]['po']+$product_list[$keyp]['ro'] +$product_list[$keyp]['des_requisition']+$product_list[$keyp]['trim']-$product_list[$keyp]['src_requisition']-$product_list[$keyp]['order'];
			$product_list[$keyp]['last'] = $product_list[$keyp]['change'] + $product_list[$keyp]['initiai'];
  		}

  		return $product_list;

	}

	public function getStartTime($filter_logcenter){
    $filter_time_array = getLogcenterInitiaiTime();

    $filter_time = $filter_time_array[$filter_logcenter];
    $filter_time = $filter_time?$filter_time:'2016-11-18';
    return $filter_time;
  }
}