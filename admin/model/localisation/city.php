<?php
class ModelLocalisationCity extends Model {
  public function addCity($data) {
    $this->db->query("INSERT INTO " . DB_PREFIX . "city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', zone_id = '" . (int)$data['zone_id'] . "'");

    $this->cache->delete('city');
  }

  public function editCity($city_id, $data) {
    $this->db->query("UPDATE " . DB_PREFIX . "city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', zone_id = '" . (int)$data['zone_id'] . "' WHERE city_id = '" . (int)$city_id . "'");

    $this->cache->delete('city');
  }

  public function deleteCity($city_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "'");

    $this->cache->delete('city');
  }

  public function getCity($city_id) {
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "'");

    return $query->row;
  }

  public function getCitiesByZoneId($zone_id) {
    $city_data = $this->cache->get('city.' . (int)$zone_id);

    if (!$city_data) {
      $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE zone_id = '" . (int)$zone_id . "' AND status = '1' ORDER BY name");

      $city_data = $query->rows;

      $this->cache->set('city.' . (int)$zone_id, $city_data);
    }

    return $city_data;
  }

  public function getTotalCitys() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "city");

    return $query->row['total'];
  }

  public function getTotalCitysByZoneId($zone_id) {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "city WHERE zone_id = '" . (int)$zone_id . "'");

    return $query->row['total'];
  }
}