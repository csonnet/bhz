<?php 

class ModelStockRefund extends Model {
	public function getstatus(){
		$sql = "select * from money_out_status";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getrefertype(){
		$sql = "select * from refer_type";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function addout($data,$user_id,$username){
		$data['trader_type']=2;
		$data['refer_type_id']=7;
		$data['status']=1;
		$data['operator_id']=$user_id;
		$data['operator']=$username;
		$data['date_added'] = date('Y-m-d H:i:s');
		$id = M("money_out")->data($data)->add();
	}

	public function getList($page, $filter){
    	$list = M('money_out')
		->join('left join  `money_type` r on r.money_type_id=money_out.refer_type_id')
		->join('left join  `money_out_status` s on s.money_out_status_id=money_out.status')
		->field('money_out.*,r.type_name as name,s.name as status_name')
		->order('money_out.date_added DESC')
		->where($filter)
		->page($page)->limit($this->config->get('config_limit_admin'))->select();
		return $list; 

    }

    public function getListCount($filter){
    	$total = M('money_out')
		->where($filter)
		->count();
		return $total; 
    }

    public function cancel($money_out_id){
		return M('money_out')->where(array('money_out_id'=>$money_out_id))->data(array('status'=>3,'date_modified'=>date('Y-m-d H:i:s')))->save();
	}
	public function recover($money_out_id){
		return M('money_out')->where(array('money_out_id'=>$money_out_id))->data(array('status'=>1,'date_modified'=>date('Y-m-d H:i:s')))->save();
	}
	public function pay($money_out_id){
		return M('money_out')->where(array('money_out_id'=>$money_out_id))->data(array('status'=>2,'date_modified'=>date('Y-m-d H:i:s'),'pay_time'=>date('Y-m-d H:i:s')))->save();
	}
	public function addMoHistory($money_out_id, $user_id,$comments){
		$username = M('user')->where(array('user_id'=>$user_id))->getField('username');
		$data = array(
					'money_out_id'=>$money_out_id, 
		  			'user_id'=>$user_id,
		  			'operator_name'=>$username,
		  		 	'comment'=>$comments, 
					'date_added'=>date('Y-m-d H:i:s'));
		M('money_out_history')->data($data)->add();
	}

	public function getOutById($money_out_id){
		return M('money_out')->join('left join  `refer_type` r on r.refer_type_id=money_out.refer_type_id')
		->join('left join  `money_out_status` s on s.money_out_status_id=money_out.status')
		->field('money_out.*,r.name,s.name as status_name')->
		where(array('money_out_id'=>$money_out_id))->find();
	}
	
	public function editout($data,$money_out_id){
		$data['date_modified'] = date('Y-m-d H:i:s');
		$data['status'] = 2;
		M("money_out")->where(array('money_out_id'=>$money_out_id))->data($data)->save();
		M("money_out")->getlastsql();
	}

	public function getProduct($in_id,$refer_type_id,$refer_id){
		$polist =M('stock_in')
		->where(array('stock_in.in_id' => $in_id, 'stock_in.refer_type_id' => $refer_type_id, 'stock_in.refer_id' => $refer_id))
		->field('stock_in.*')
		->find();
		if (!empty($polist)) {
			$where =' in_id='.$polist['in_id'].' and op.option_value_id=pov.option_value_id' ;
			$polist['product']=M('stock_in_detail')
			->join('left join product_option_value pov on pov.product_code=stock_in_detail.product_code')
			->join('left join product pro on pro.product_id=pov.product_id')
			->join('left join product_description pd on pd.product_id=pov.product_id')
			->join('left join  inventory i on pov.product_code=i.product_code')
			->join('left join  option_value_description op on op.option_id=pov.option_id')
			->distinct('pov.product_code')
			->field('stock_in_detail.*,pd.name,pd.product_id,pov.product_code,op.name as oname,pro.sku,i.position1,i.position2')
			 ->where($where)
			// ->group('pov.product_code')
			->select();
		}
		
		// echo M('stock_in_detail') ->getlastsql();
		return $polist;
	}
	
	public function getHistory($money_out_id){
		return M('money_out_history')->where(array('money_out_id'=>$money_out_id))->select();
	}

	public function getAutoCompletePo($filter_data){
		return M('po')
			->join('left join vendors vendors on vendors.user_id=po.vendor_id')
			-> where($filter_data)
			-> field('po.id,po.vendor_id,vendors.company')
			->select();
	}

	public function getAutoCompleteOr($filter_data){
		return M('order')
			-> where($filter_data)
			-> field('`order`.payment_company,`order`.customer_id,`order`.order_id')->limit(10)
			->select();
	}
	
}