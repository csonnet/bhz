<?php
class ModelStockWarehouse extends Model {
	
	//获取仓库总数量
	public function getTotalWarehouses($data = array()) {

		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "warehouse";
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	//获取多个仓库数据
	public function getWarehouses($data = array()) {
		
		if($data){

			$sql = "SELECT * FROM " . DB_PREFIX . "warehouse wh ORDER BY type ASC";

			if(isset($data['start']) || isset($data['limit'])){

				if($data['start'] < 0){
					$data['start'] = 0;
				}

				if($data['limit'] < 1){
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

			}

			$query = $this->db->query($sql);
			return $query->rows;

		}
		else{

			$warehouses_data = $this->cache->get('warehouse');
			if(!$warehouses_data){
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse wh");
				$warehouses_data = $query->rows;
				$this->cache->set('warehouse', $warehouses_data);
			}
			
			return $warehouses_data;

		}
	}
	
	//获取指定仓库数据
	public function getWarehouse($warehouse_id) {

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse WHERE warehouse_id = '" . (int)$warehouse_id . "'");

		return $query->row;

	}
	
	//启用仓库
	public function EnableWarehouse($stock_id) {
		
		$this->db->query("UPDATE " . DB_PREFIX . "warehouse wh SET wh.status = '1' WHERE wh.warehouse_id = '".(int)$stock_id."'");

	}

	//停用仓库
	public function DisableWarehouse($stock_id) {
		
		$this->db->query("UPDATE " . DB_PREFIX . "warehouse wh SET wh.status = '0' WHERE wh.warehouse_id = '".(int)$stock_id."'");

	}
	
	//添加仓库
	public function addWarehouse($data){
		
		$data['date_added'] = date('Y-m-d H:i:s');

		$Warehouse = M("warehouse");
		$Warehouse->data($data)->add();

	}
	
	//修改仓库
	public function editWarehouse($warehouse_id,$data){
		
		$data['warehouse_id'] = (int)$warehouse_id;
		$Warehouse = M("warehouse");
		$Warehouse->data($data)->save();

	}

	//获取残次品仓所属仓库
	public function getParentWarehouses() {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "warehouse WHERE type != 3 ORDER BY type ASC";

		$query = $this->db->query($sql);
		return $query->rows;

	}

	public function getWarehouseByName($name){

		$sql = "SELECT * FROM " . DB_PREFIX . "warehouse WHERE `name` LIKE '%".$name."%' ORDER BY `order` DESC";

		$query = $this->db->query($sql);
		return $query->rows;
	}

    /*
     * 获取所有收货仓列表，返回 仓库pk，是否收货 关联数组
     * @author sonicsjh
     */
    public function getAvailableWarehouse() {
        $sql = "SELECT * FROM `".DB_PREFIX."warehouse` WHERE `type`!='3' AND `status`='1' ORDER BY `type` ASC,`warehouse_id` ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    /*
     * 根据仓库编号，获取仓库和残次品pk列表
     * @author sonicsjh
     */
    public function getAllWarehouseIdsByWarehouseId($warehouseId) {
        $sql = "SELECT `warehouse_id` FROM `".DB_PREFIX."warehouse` WHERE (`warehouse_id`='".$warehouseId."' OR `parent_warehouse`='".$warehouseId."') AND `status`=1";
        $query = $this->db->query($sql);
        $ret = array();
        foreach ($query->rows as $row) {
            $ret[] = $row['warehouse_id'];
        }
        return $ret;
    }
}
?>
