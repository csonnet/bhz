<?php 

class ModelStockCost extends Model {
	public function getCostUses(){
		$sql = "select * from cost_use_type";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCostType(){
		$sql = "select * from cost_type";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getStatus(){
		$sql = "select * from cost_status";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function addcost($datas,$user){
		$data['user_id']=$user;
		$data['is_return']=$datas['is_return'];
		$data['status']=0;
		$data['cost_use_type_id']=$datas['cost_use_type_id'];
		$data['comment']=$datas['comment'];
		$data['days']=strtotime($datas['days']);
		$data['end_date']=$datas['end_date'];
		$data['start_date']=$datas['start_date'];
		$data['return_date']=$datas['return_date'];
		$data['cost_type_id']=$datas['cost_type_id'];
		$data['cost_status_id']=1;
		$data['cost']=$datas['cost'];
		$data['date_added'] = date('Y-m-d H:i:s');
		$id = M("cost")->data($data)->add();
		foreach ($datas['auditors'] as $key => $value) {
			$data1['auditor_use_id'] = $value['user_id'];
			$data1['cost_id'] = $id;
			$data1['auditor_use_name'] = $value['fullname'];
			$data1['auditor_use_tel'] = $value['contact_tel'];
			$data1['date_added'] = date('Y-m-d H:i:s');

			if ($key==0) {var_dump($key);
				$data1['auditor_status'] = 1;
			}
 			M("cost_auditor")->data($data1)->add();
			unset($data1['auditor_status']);
		}
		return $id;
	}

	public function editcost($datas,$user,$cost_id){
		$data['user_id']=$user;
		$data['is_return']=$datas['is_return'];
		$data['status']=0;
		$data['cost_use_type_id']=$datas['cost_use_type_id'];
		$data['comment']=$datas['comment'];
		$data['days']=strtotime($datas['days']);
		$data['end_date']=$datas['end_date'];
		$data['start_date']=$datas['start_date'];
		$data['return_date']=$datas['return_date'];
		$data['cost_type_id']=$datas['cost_type_id'];
		$data['cost_status_id']=1;
		$data['cost']=$datas['cost'];
		$data['date_added'] = date('Y-m-d H:i:s');
		 M("cost")->data($data)->where(array('cost_id'=>$cost_id))->save();
		M("cost_auditor")->where(array('cost_id'=>$cost_id))->delete();
			foreach ($datas['auditors'] as $key => $value) {
			$data1['auditor_use_id'] = $value['user_id'];
			$data1['cost_id'] = $cost_id;
			$data1['auditor_use_name'] = $value['fullname'];
			$data1['auditor_use_tel'] = $value['contact_tel'];
			$data1['date_added'] = date('Y-m-d H:i:s');

			if ($key==0) {var_dump($key);
				$data1['auditor_status'] = 1;
			}
 			M("cost_auditor")->data($data1)->add();
			unset($data1['auditor_status']);
		}
		return $cost_id;
	}

	
 

	public function getList($page, $filter){
    	$list = M('cost')
		->join('left join  `cost_type` r on r.cost_type_id=cost.cost_type_id')
		->join('left join  `cost_status` s on s.cost_status_id=cost.cost_status_id')
		->join('left join  `cost_use_type` p on p.cost_use_type_id=cost.cost_use_type_id')
		->join('left join  `user` u on u.user_id=cost.user_id')
		->field('cost.*,r.cost_name,s.status_name,p.cost_use_name,u.fullname')
		->order('cost.date_added DESC')
		->where($filter)
		->page($page)->limit($this->config->get('config_limit_admin'))->select();
		return $list; 

    }

    public function getListCount($filter){
    	$total = M('cost')
		->where($filter)
		->count();
		return $total; 
    }
	
	 public function getUser($filter){
    	$user = M('user')
    	->field('fullname,user_id,contact_tel')
		->where($filter)
		->limit(5)
		->select();
		return $user; 
    }

    public function addHistory($id, $user_id,$comments){
		$username = M('user')->where(array('user_id'=>$user_id))->getField('username');
		$data = array(
					'cost_id'=>$id, 
		  			'user_id'=>$user_id,
		  			'operator_name'=>$username,
		  		 	'comment'=>$comments, 
					'date_added'=>date('Y-m-d H:i:s'));
		M('cost_history')->data($data)->add();
	}

	public function getcost($cost_id){
		$data = M('cost')
		->join('left join  `cost_type` r on r.cost_type_id=cost.cost_type_id')
		->join('left join  `cost_status` s on s.cost_status_id=cost.cost_status_id')
		->join('left join  `cost_use_type` p on p.cost_use_type_id=cost.cost_use_type_id')
		->join('left join  `user` u on u.user_id=cost.user_id')
		->field('cost.*,r.cost_name,s.status_name,p.cost_use_name,u.fullname')
		->order('cost.date_added DESC')
		->where(array('cost_id'=>$cost_id))->find();
		$data['auditor'] = M('cost_auditor')->where(array('cost_id'=>$cost_id))->select();
		return $data;
	}

	public function getstockHistory($cost_id){
		$data = M('cost_history')->where(array('cost_id'=>$cost_id))->select();
		return $data;
	}

	public function delete($cost_id){
		M('cost')->where(array('cost_id'=>$cost_id))->delete();
		M('cost_history')->where(array('cost_id'=>$cost_id))->delete();
		M('cost_auditor')->where(array('cost_id'=>$cost_id))->delete();
		return true;
	}
}