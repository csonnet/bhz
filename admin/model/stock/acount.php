<?php 
class Modelstockacount extends Model {


	public function getList($page,$filter,$filter_date_start,$filter_date_end){
		 $money_in = M('money_in')
	    ->join('left join  `customer` c on c.customer_id=money_in.payer_id')
	    ->join('left join  `user`  on c.user_id=user.user_id')
		->field('sum(money_in.receivable) as receivable,sum(money_in.received) as received,money_in.payer_name,money_in.payer_id,user.fullname,money_in.trader_type')
		->where($filter)
		->group('payer_id')
		->page($page)->limit($this->config->get('config_limit_admin'))->select();
		foreach ($money_in as $key => $value) {
			 $filter['money_out.date_added'] = array('between', array($filter_date_start.' 00:00:00', $filter_date_end.' 23:59:59'));
			$filter1['payee_id'] =array('eq',$value['payer_id']) ;
			$filter1['trader_type'] = array('eq',$value['trader_type']);
			$list[$key]['receivable'] = $value['receivable'];
			$list[$key]['received'] = $value['received'];
			$list[$key]['payer_id'] = $value['payer_id'];
			$list[$key]['payer_name'] = $value['payer_name'];
			$list[$key]['fullname'] = $value['fullname'];
			$list[$key]['money_out'] = M('money_out')
				->field('sum(money_out.payables) as payables ,sum(money_out.paid) as paid')
				->where($filter1)
				->find();
		}
		return $list;
	}

	public function getListCount($filter){
    	$total = M('money_in')
	    ->join('left join  `customer` c on c.customer_id=money_in.payer_id')
	    ->join('left join  `user`  on c.user_id=user.user_id')
		->where($filter)
		->group('payer_id')->select();
		return $total; 
    }



	public function getMoneyIn($filter){

	    $money_in=M('money_in')
	    		->join('left join  `money_type` t on t.money_type_id=money_in.refer_type_id')
				->join('left join  `money_in_status` s on s.money_in_status_id=money_in.status')
				->field('money_in.*, t.type_name,s.name as status_name')
				-> where($filter)->select();
	    return $money_in;
	}


	public function getMoneyOut($filter1){

	    $money_out =M('money_out')
	    		->join('left join  `refer_type` r on r.refer_type_id=money_out.refer_type_id')
				->join('left join  `money_out_status` s on s.money_out_status_id=money_out.status')
				->field('money_out.*,r.name,s.name as status_name')->where($filter1)->select();

	    return $money_out;
	}
	

	public function getAutoCompleteUser($filter_name){
		$where = "(`fullname` LIKE '%".$filter_name."%'  OR recommended_code LIKE'%".$filter_name."%')  AND `recommended_code` <> 0 AND `user_group_id` = 63 "; 
	    $user = M('user')
				->field('fullname,recommended_code,user_id')->where($where)->limit(10)->select();
			 // echo M('user')->getLastsql();

	    return $user;
	}

}