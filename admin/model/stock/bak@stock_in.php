<?php
class ModelStockStockIn extends Model {
 	public function getList($page = 1, $filter){
		$polist = M('stock_in')
		->join('left join user v on v.user_id=stock_in.user_id')
		->join('left join warehouse w on w.warehouse_id=stock_in.warehouse_id')
		->join('left join refer_type r on r.refer_type_id=stock_in.refer_type_id')
		->join('left join inout_state i on i.id=stock_in.status')
		->join('left join stock_in_detail rd on rd.in_id=stock_in.in_id')
		->distinct('stock_in.in_id')
		->order('stock_in.in_id DESC')
		->where($filter)
		->field('stock_in.*,v.username, w.name as warehouse_name ,r.name as refer_type,i.name as status_name')
		->page($page)->limit($this->config->get('config_limit_admin'))->select();
		   // echo M('stock_in')->getlastsql();
		return $polist;
	}
	public function getAllList($filter){
		$polist = M('stock_in')
		->join('left join user v on v.user_id=stock_in.user_id')
		->join('left join warehouse w on w.warehouse_id=stock_in.warehouse_id')
		->join('left join refer_type r on r.refer_type_id=stock_in.refer_type_id')
		->join('left join inout_state i on i.id=stock_in.status')
		->join('left join stock_in_detail rd on rd.in_id=stock_in.in_id')
		->distinct('stock_in.in_id')
		->order('stock_in.in_id DESC')
		->where($filter)
		->field('stock_in.*,v.username, w.name as warehouse_name ,r.name as refer_type,i.name as status_name')
		->select();
		   // echo M('stock_in')->getlastsql();
		return $polist;
	}

	public function getListCount($filter){
		$count = M('stock_in')
		->join('left join user v on v.user_id=stock_in.user_id')
		->join('left join warehouse w on w.warehouse_id=stock_in.warehouse_id')
		->join('left join refer_type r on r.refer_type_id=stock_in.refer_type_id')
		->join('left join inout_state i on i.id=stock_in.status')
		->join('left join stock_in_detail rd on rd.in_id=stock_in.in_id')
		->distinct(true)
		->group('stock_in.in_id')
		->where($filter)
		->select();
		// echo M('stock_in')->getlastsql();

		return $count;
	}

	public function getstock($stock_id){
		$polist =M('stock_in')
		->join('left join user v on v.user_id=stock_in.user_id')
		->join('left join warehouse w on w.warehouse_id=stock_in.warehouse_id')
		->join('left join refer_type r on r.refer_type_id=stock_in.refer_type_id')
		->join('left join inout_state i on i.id=stock_in.status')
		->where(array('in_id' => $stock_id ))
		->field('stock_in.*,v.username, w.name as warehouse_name ,r.name as refer_type,w.warehouse_id,i.name as status_name')
		->find();
		$polist['product']=M('stock_in_detail')
		->join('left join product_option_value pov on pov.product_code=stock_in_detail.product_code')
		->join('left join product pro on pro.product_id=pov.product_id')
		->join('left join product_description pd on pd.product_id=pov.product_id')
		->join('left join  inventory i on pov.product_code=i.product_code')
		->distinct('pov.product_code')
		->field('stock_in_detail.*,pd.name,pd.product_id,pov.product_code,pro.sku,i.position1,i.position2')
		->where(array('in_id' => $polist['in_id'] ))
		// ->group('pov.product_code')
		->select();
		//echo M('stock_in_detail') ->getlastsql();
		return $polist;
	}

	public function getWarehouse(){
		$warehouse = M('warehouse')->field('warehouse_id,name')->where('(type=1 or type =2 and status=1 )')
		->select();
		return $warehouse;
	}

	public function searchPo($bill){
		$where['po_id'] = $bill;
		$products = M('po_product')
					->join('left join po p on p.id=po_product.po_id')
					->join('left join warehouse w on w.warehouse_id=p.warehouse_id')
					->field('po_product.*,w.name as warehouse_name ')
					->where($where)->select();
		return $products;
	}

	public function searchSo($bill){
		$where['po_id'] = $bill;
		$products = M('ro_product')->where($where)->select();
		return $products;
	}

	public function addstock($data){
		$stockin = M('stock_in');
	  	$datan = array('refer_id'=>$data['refer_id'], 
	  		'refer_type_id'=>$data['refer_type'],
	  		 'warehouse_id'=>$data['warehouse'], 
	  		'user_id'=>$_SESSION['default']['user_id'], 
	  		'buy_money'=>0, 
	  		'status'=>$data['status'], 
	  		'comment'=>$data['comment'], 
	  		'date_added'=>date('Y-m-d H:i:s'),
	  		'in_date'=>date('Y-m-d H:i:s'));
	  		$stockid = $stockin->data($datan)->add();
	  		$total=0;
	  	foreach ($data['products'] as $key => $value) {
	  		$stockind = M('stock_in_detail');
		  	$datad = array('in_id'=>$stockid, 
		  		'product_code'=>$value['product_code'],
		  		 'product_quantity'=>$value['product_quantity'], 
		  		 'product_price'=>$value['product_price'], 
		  		 'products_money'=>$value['product_price']*$value['product_quantity'], 
				'date_added'=>date('Y-m-d H:i:s'));
		  	$total =$total+ $value['product_price']*$value['product_quantity'];
		  	$stockind->data($datad)->add();

	  	}
		$stockin->where(array('in_id'=>$stockid))->data(array('buy_money'=>$total))->save();
		return 	$stockid;
	}

	public function editstock($data){
		// var_dump($data);
		$stockind = M('stock_in_detail');
		$stockind->where(array('in_id'=>$data['in_id']))->delete();
	  	$total=0;
	  	foreach ($data['products'] as $key => $value) {
	  		
		  	$datad = array('in_id'=>$data['in_id'], 
		  		'product_code'=>$value['product_code'],
		  		 'product_quantity'=>$value['product_quantity'], 
		  		 'product_price'=>$value['product_price'], 
		  		 'products_money'=>$value['product_price']*$value['product_quantity'], 
				'date_added'=>date('Y-m-d H:i:s'));
		  	$total =$total+ $value['product_price']*$value['product_quantity'];
		  	$stockind->data($datad)->add();

	  	}
		M('stock_in')->where(array('in_id'=>$data['in_id']))->data(array('buy_money'=>$total))->save();	
	}
  	

  	public function savestock($data){
		$stockind = M('stock_in_detail');
		$stockind->where(array('in_id'=>$data['in_id']))->delete();
	  	$total=0;
	  	foreach ($data['products'] as $key => $value) {
	  		
		  	$datad = array('in_id'=>$data['in_id'], 
		  		'product_code'=>$value['product_code'],
		  		 'product_quantity'=>$value['product_quantity'], 
		  		 'product_price'=>$value['product_price'], 
		  		 'products_money'=>$value['product_price']*$value['product_quantity'], 
				'date_added'=>date('Y-m-d H:i:s'));
		  	$total =$total+ $value['product_price']*$value['product_quantity'];
		  	$stockind->data($datad)->add();

	  	}
		M('stock_in')->where(array('in_id'=>$data['in_id']))->data(array('buy_money'=>$total,
			'status'=>2,
			'in_date'=>date('Y-m-d H:i:s')
			))->save();
	}

  	public function cancel($in_id){
		return M('stock_in')->where(array('in_id'=>$in_id))->data(array('status'=>0))->save();
	}

	public function recover($in_id){
		return M('stock_in')->where(array('in_id'=>$in_id))->data(array('status'=>1))->save();
	}

	public function referType(){
					
		$refer_type = M('refer_type')->field('refer_type_id,name')->select();
		return $refer_type;
	}

	public function getstatus(){
					
		$refer_type = M('inout_state')->select();
		return $refer_type;
	}

	public function getproduct($data){
		$sql = "SELECT pd.name,pov.product_code FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN product_description pd ON pov.product_id = pd.product_id WHERE 1 AND pd.name LIKE '%".$data['filter_name']."%' OR pov.product_code LIKE '%".$data['filter_name']."%' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function addInventory($warehouse_id,$data){
		foreach ($data['products'] as $key => $value) {
			$productNum = M('inventory')
			->field('available_quantity,product_code,account_quantity')
			->where(array('product_code' => $value['product_code'],'warehouse_id'=>$warehouse_id))
			->find();
			if (empty($productNum)) {
				$data = array(
					'warehouse_id'=>$warehouse_id, 
		  			'product_code'=>$value['product_code'],
		  		 	'available_quantity'=>$value['product_quantity'], 
		  		 	'account_quantity'=>$value['product_quantity'], 
					'date_added'=>date('Y-m-d H:i:s'));
			  	M('inventory')->data($data)->add();
			}else{
				$total=(int)$productNum['available_quantity']+(int)$value['product_quantity'];
				$totalc=(int)$productNum['account_quantity']+(int)$value['product_quantity'];
				M('inventory')-> where(array('product_code' => $value['product_code'],'warehouse_id'=>$warehouse_id))->data(array('available_quantity'=>$total,'account_quantity'=>$totalc,'date_modified'=>date('Y-m-d H:i:s')))->save();
			}
		}
	}
	
	public function getstockHistory($stock_id){
		return M('stock_in_history')->where(array('in_id'=>$stock_id))->select();
		
	}

	public function addStHistory($in_id, $user_id,$comments){
		$username = M('user')->where(array('user_id'=>$user_id))->getField('username');
		$data = array(
					'in_id'=>$in_id, 
		  			'user_id'=>$user_id,
		  			'operator_name'=>$username,
		  		 	'comment'=>$comments, 
					'date_added'=>date('Y-m-d H:i:s'));
		M('stock_in_history')->data($data)->add();
	}
}
?>