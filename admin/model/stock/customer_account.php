<?php 

class Modelstockcustomeraccount extends Model {
	public function getList($page = 1, $filter, $sort_data){
		$vblist = M('customer c')
		->join('user u on u.user_id = c.user_id','left')
		->order('c.date_added desc')
		->field('c.telephone,c.customer_id, c.company_name,u.fullname')
		->where($filter)->page($page)->limit($this->config->get('config_limit_admin'))->select();
		// echo M()->getLastSql();
		return $vblist;
	}
	public function getListCount($filter){
		$count = M('customer c')
		->join('user u on u.user_id = c.user_id','left')
		->order('c.date_added desc')
		->field('c.telephone, c.company_name,u.fullname')
		->where($filter)->count();
		return $count;
	}

    public function addCustomerAcc($data){
    	$where = "customer_id=".$data['customer_id'];
    	$customer  = M('customer')
		->where($where)
		->field('telephone,company_name,customer_id')
		->find();
		if ($customer['customer_id']) {
			M('customer')
			->where($where)
			->data(array('is_magfin'=>0))
			->save();
		}
		
 		
    }  

    public function getAutoCompleteCus($filter_data){
    	$where = " telephone like '%".$filter_data."%' OR company_name like '%".$filter_data."%'";
    	$customer = M('customer')
		->where($where)
		->limit(10)
		->field('telephone,company_name,customer_id')
		->select();
		// echo M('customer') ->getlastsql();
		return $customer; 
    } 

    public function delete($customer_id){
		if (!empty($customer_id)) {
			$where = "customer_id=".$customer_id;
			M('customer')
			->where($where)
			->data(array('is_magfin'=>1))
			->save();
			
		}
		
		return $res; 
    } 


    public function getCustomerAccount(){
    	$customer =  M('customer c')
		->join('user u on u.user_id = c.user_id','left')
		->where(array('is_magfin'=>0))
		->order('c.date_added desc')
		->field('c.telephone,u.contact_tel')->select();
		return $customer; 
    } 


    public function getSmsLog($templateId){
    	$date = date("Ymd",strtotime("-1 day")).'000000';
    	$where = 'sl.template_id='.$templateId.' and sl.date_added >'.$date;
    	$log = M('sms_log sl')
		->where($where)
		->order('sl.date_added desc')
		->field('sl.*')->select();
		return $log; 
    }
 
}