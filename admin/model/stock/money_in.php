<?php 
class ModelStockMoneyIn extends Model {


	public function updatemoney($res,$order_id){
		"UPDATE `money_in` SET `status`='2',`pay_time`='" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE `refer_id` = ".$order_id." AND `refer_type_id` = 1";
        $this->db->query(" UPDATE `money_in` SET `received`='".$res['total']."',`status`='2',`pay_time`='" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE `refer_id` = ".$order_id." AND `refer_type_id` = 1");
	}
	public function getOrderById($orderID){
		$sql = "SELECT `order`.total FROM" . DB_PREFIX . " `order` WHERE `order_id` =".$orderID." LIMIT 1"; 
		$query=$this->db->query($sql);
		return $query->row; 
    }

   public function getMoneyOrder($orderID){
    	 $model=M('order');
            $where=array('order_id'=>$orderID);
            $res=$model->field('`order`.order_id, `order`.ori_total as total, `order`.customer_id, `order`.payment_company,`order`.shipping_address')
					->where($where)->find();
         return $res;
    }

	public function addmoney($res,$user, $UserName){
        $model=M('money_in');

		$data = array(
			'payer_id'=>$res['customer_id'], 
			'payer_name'=>$res['payer_name'], 
			'refer_type_id'=>$res['money_type'], 
			'refer_id'=>$res['refer_id'], 
			'receivable'=>$res['receivable'], 
			'refer_total'=>$res['ori_total'], 
			'received'=>$res['received'], 
			'status'=>$res['status'], 
			'comment'=>$res['comment'], 
			'date_added'=>date('Y-m-d H:i:s'),
			'operator'=>$UserName,
			'operator_id'=>$user
	  		);
			if ($res['status']==2) {
				$data['pay_time']=date('Y-m-d H:i:s');
			}
 			$money_in_id =  $model->data($data)->add();
 			return $money_in_id;
	}


    public function getList($page, $filter){
    	$Monlist = M('money_in')
		->join('left join  `money_type` t on t.money_type_id=money_in.refer_type_id')
		->join('left join  `money_in_status` s on s.money_in_status_id=money_in.status')
		->field('money_in.*, t.type_name,s.name as status_name')
		->order('money_in.money_in_id DESC')
		->where($filter)
		->page($page)->limit($this->config->get('config_limit_admin'))->select();
		// echo M('money_in')->getlastsql();
		return $Monlist; 
    }

    public function getListCount($filter){
    	$total = M('money_in')
		->join('left join  `money_type` t on t.money_type_id=money_in.refer_type_id')
		->where($filter)
		->count();
		return $total; 
    }

    public function getstatus(){
    	$status = M('money_in_status')
		->select();
		return $status; 
    }
    public function cancel($money_in_id){
		return M('money_in')->where(array('money_in_id'=>$money_in_id))->data(array('status'=>4,'date_modified'=>date('Y-m-d H:i:s')))->save();
	}
	public function recover($money_in_id){
		return M('money_in')->where(array('money_in_id'=>$money_in_id))->data(array('status'=>1,'date_modified'=>date('Y-m-d H:i:s')))->save();
	}
	public function pay($money_in_id){
		return M('money_in')->where(array('money_in_id'=>$money_in_id))->data(array('status'=>2,'date_modified'=>date('Y-m-d H:i:s'),'pay_time'=>date('Y-m-d H:i:s')))->save();
	}
	public function getInById($money_in_id){
		$money_in = M('money_in')
		->join('left join  `money_type` money_type on money_type.money_type_id=money_in.refer_type_id')
		->field('money_in.*,money_type.type_name')
		-> where(array('money_in_id'=>$money_in_id))->find();
		return $money_in;

	}

	public function getIn($money_in_id){
		$money_in =  M('money_in')
		->join('left join  `money_type` t on t.money_type_id=money_in.refer_type_id')
		->join('left join  `money_in_status` s on s.money_in_status_id=money_in.status')
		->field('money_in.*, t.type_name,s.name as status_name')->where(array('money_in_id'=>$money_in_id))->find();
		$money_in['stock'] = M('stock_out')
			->field('out_id,sale_money,out_date')
			 ->where(array('refer_id'=>$money_in['refer_id'],'refer_type_id'=>1,'status'=>2))
			->select();
		return $money_in;

	}
	public function getHistory($money_in_id){
		return M('money_in_history')->where(array('money_in_id'=>$money_in_id))->select();
	}

	public function editin($res,$money_in_id){
		$model=M('money_in');
			$received = (float)$res['received']+(float)$res['addreceived'];

			if ($received>=(float)$res['receivable']) {
				$data = array(
				'received'=>$received, 
				'status'=>2, 
				'date_modified'=>date('Y-m-d H:i:s'),
				'pay_time'=>date('Y-m-d H:i:s'),
		  		);
			}else{
				$data = array(
				'received'=>$received, 
				'status'=>5, 
				'date_modified'=>date('Y-m-d H:i:s'),
				'pay_time'=>date('Y-m-d H:i:s'),
		  		);
			}
		

 			$money_in_id =  $model->data($data)->where(array('money_in_id'=>$money_in_id))->save();
	}

	public function confirm($money_in_id){
		$order_id = M('money_in')->where(array('money_in_id'=>$money_in_id))->getField('refer_id');
		$order_model = M('order');
		$map['order_id'] = $order_id;
		$data['is_pay'] = 1;
		$order_model 
		->where($map)
		->data($data)
		->save();
		
	}
	public function getMoneyType($in_out){
		return M('money_type')->where(array('in_out'=>$in_out))->select();
	}
	
	public function getAutoCompleteOrder($filter_data){
		$data =  M('order')
		->where($filter_data)->field('order_id,payment_company,customer_id,ori_total')->limit(10)->select();
		foreach ($data as $key => $value) {
			$data[$key]['receivable'] = M('stock_out')->where(array('refer_id'=>$value['order_id'],'refer_type_id'=>1,'status'=>2))->getField('sum(sale_money)');
		}
		return $data;
	}
	public function finishstock($money_in_id){
		 M('money_in')->where(array('money_in_id'=>$money_in_id))->data(array('status'=>2,'date_modified'=>date('Y-m-d H:i:s'),'pay_time'=>date('Y-m-d H:i:s')))->save();
	}


	public function getCustom(){
		return  M('customer')->field('ip')
		 ->select();
	}

	public function addHistory($money_in_id, $user_id,$comments){
		$username = M('user')->where(array('user_id'=>$user_id))->getField('username');
		$data = array(
					'money_in_id'=>$money_in_id, 
		  			'user_id'=>$user_id,
		  			'operator_name'=>$username,
		  		 	'comment'=>$comments, 
					'date_added'=>date('Y-m-d H:i:s'));
		M('money_in_history')->data($data)->add();
	}

	public function dr(){
		$sql = "SELECT `order`.order_id, `order`.ori_total, `order`.is_pay, `order`.total, `order`.customer_id, `order`.payment_company, `order`.shipping_address, `order`.date_added, `order`.paid_time, `order`.date_added, `order`.date_added,
	(SELECT SUM(s.sale_money) FROM stock_out AS s WHERE s.refer_id = `order`.`order_id` AND refer_type_id = 1 AND `status` = 2 ) AS po FROM `order` WHERE
	(`order`.date_added BETWEEN '2017-08-01'AND '9999-12-31') AND `order`.order_status_id != 0 AND `order`.order_status_id != 3 AND `order`.order_status_id != 16"; 
	$query=$this->db->query($sql);
	return $query->rows;
	}
	public function in($data){
		return M('money_in')->data($data)->add();
	}
	
}