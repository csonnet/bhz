<?php
class ModelAreaArea extends Model {
	
	//获取区域
	public function getArea($data){

		$start = $data['start'];
		$limit = $data['limit'];
		$where['name'] = array('like','%'.$data['filter_name'].'%');

		switch($data['type']){
			
			case 'country':
				$table = 'country';
				break;
			case 'zone':
				$table = 'zone';
				break;
			case 'city':
				$table = 'city';
				break;
			default:
				break;

		}
		
		$area = M($table)->where($where)->limit($start,$limit)->select();

		return $area;

	}
	
	//获取只分配省市
	public function getOnlyCountries($product_id){
		
		$where['product_id'] = $product_id;
		$oc = M('product_to_area')->where($where)->getField('only_countries');
		
		if($oc){
			$where_c['country_id'] = array('in',$oc);
			$oc_group = M('country')->where($where_c)->select();
		}
		else{
			return;
		}

		return $oc_group;

	}

	//获取只分配市区
	public function getOnlyZones($product_id){
		
		$where['product_id'] = $product_id;
		$oz = M('product_to_area')->where($where)->getField('only_zones');
		
		if($oz){
			$where_z['zone_id'] = array('in',$oz);
			$oz_group = M('zone')->where($where_z)->select();
		}
		else{
			return;
		}

		return $oz_group;

	}

	//获取不分配省市
	public function getNoCountries($product_id){
		
		$where['product_id'] = $product_id;
		$nc = M('product_to_area')->where($where)->getField('no_countries');
		
		if($nc){
			$where_c['country_id'] = array('in',$nc);
			$nc_group = M('country')->where($where_c)->select();
		}
		else{
			return;
		}

		return $nc_group;

	}

	//获取不分配市区
	public function getNoZones($product_id){
		
		$where['product_id'] = $product_id;
		$nz = M('product_to_area')->where($where)->getField('no_zones');
		
		if($nz){
			$where_z['zone_id'] = array('in',$nz);
			$nz_group = M('zone')->where($where_z)->select();
		}
		else{
			return;
		}

		return $nz_group;

	}

}
?>
