<?php
Class ModelReportV2Customer extends Model {
    public function getList($filter) {
        $where = "WHERE 1";
        if ('0' != $filter['date']){
            $where .= " AND RC.`report_year`='".intval(substr($filter['date'], 0, 4))."' AND RC.`report_month`='".intval(substr($filter['date'], 4, 2))."'";
        }
        if ($filter['user']){
            $where .= " AND RC.`user_id`='".$filter['user']."'";
        }
        $sql = "SELECT RC.*,U.`username`,U.`fullname` FROM `".DB_PREFIX."report_customer` AS RC LEFT JOIN `".DB_PREFIX."user` AS U ON (RC.`user_id`=U.`user_id`) ".$where." ORDER BY `report_year` DESC,`report_month` DESC,`user_id` ASC,`customer_shop_type` ASC";
        $result = $this->db->query($sql);
        return $result->rows;
    }

    public function getUserInReport() {
        $sql = "SELECT `user_id`,`username`,`fullname` FROM `".DB_PREFIX."user` WHERE `user_group_id`='63' AND `status`='1' AND `recommended_code`!=''";
        $result = $this->db->query($sql);
        return $result->rows;
    }
}
