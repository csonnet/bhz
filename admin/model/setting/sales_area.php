<?php
class ModelSettingSalesArea extends Model {
    public function getAllSalesArea() {
        $sql = "SELECT * FROM `".DB_PREFIX."sales_area` ORDER BY `sales_area_id` ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

}
