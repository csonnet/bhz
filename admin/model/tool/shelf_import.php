<?php
class ModelToolShelfImport extends Model {
    //导入商品基本信息
    public function importData($data, $batchNumber, $customerId=0) {
        $solutionId = $this->_solutionCheck($data, $batchNumber, $customerId);
        $shelfId = $this->_shelfCheck($data, $solutionId);
        $shelfProductId = $this->_shelfProductCheck($data, $shelfId);
        return compact('solutionId', 'shelfId', 'shelfProductId');
    }

    //基于方案编号，更新属性
    public function updateData($solutionList) {
        foreach ($solutionList as $solutionId=>$v) {
            $this->_shelfUpdate($solutionId);
            $this->_solutionUpdate($solutionId);
        }
    }

    protected function _solutionCheck($data, $batchNumber, $customerId) {
        $sql = "SELECT `solution_id` FROM `solution` WHERE `customer_id`='".$customerId."' AND `solution_name`='".$this->db->escape($data['solutionName'])."' AND `solution_category`='".$this->db->escape($data['solutionCategory'])."' AND `batch_number`='".$batchNumber."'";
        $query = $this->db->query($sql);
        $solutionId = (int)$query->row['solution_id'];
        if ($solutionId < 1) {
            $sql = "INSERT INTO `solution` SET `customer_id`='".$customerId."',`solution_name`='".$this->db->escape($data['solutionName'])."',`is_template`='1',`solution_category`='".$this->db->escape($data['solutionCategory'])."',`status`='1',`batch_number`='".$batchNumber."',`date_added`='".date('Y-m-d H:i:s')."'";
            $this->db->query($sql);
            $solutionId = $this->db->getLastId();
        }
        return $solutionId;
    }

    protected function _shelfCheck($data, $solutionId) {
        $sql = "SELECT `shelf_id` FROM `shelf` WHERE `solution_id`='".$solutionId."' AND `shelf_name`='".$this->db->escape($data['shelfName'])."' AND `shelf_type`='".$this->db->escape($data['shelfType'])."'";
        $query = $this->db->query($sql);
        $shelfId = (int)$query->row['shelf_id'];
        if ($shelfId < 1) {
            $sql = "INSERT INTO `shelf` SET `solution_id`='".$solutionId."',`shelf_name`='".$this->db->escape($data['shelfName'])."',`shelf_type`='".$this->db->escape($data['shelfType'])."',`grade_start_search`='".$this->db->escape($data['gradeSearch']['start'])."',`grade_end_search`='".$this->db->escape($data['gradeSearch']['end'])."'";
            $this->db->query($sql);
            $shelfId = $this->db->getLastId();
        }
        return $shelfId;
    }

    protected function _shelfProductCheck($data, $shelfId) {
        $sql = "SELECT `shelf_product_id` FROM `shelf_product` WHERE `shelf_id`='".$shelfId."' AND `shelf_layer`='".$this->db->escape($data['shelfLayer'])."' AND `layer_position`='".$this->db->escape($data['layerPosition'])."'";
        $query = $this->db->query($sql);
        $shelfProductId = (int)$query->row['shelf_product_id'];
        if ($shelfProductId < 1) {
            $sql = "INSERT INTO `shelf_product` SET `shelf_id`='".$shelfId."',`product_id`='".$this->db->escape($data['productCode']['id'])."',`product_code`='".$this->db->escape($data['productCode']['code'])."',`group_name`='".$this->db->escape($data['groupName'])."',`attr_list`='".$this->db->escape(json_encode($data['attr']))."',`shelf_layer`='".$this->db->escape($data['shelfLayer'])."',`layer_position`='".$this->db->escape($data['layerPosition'])."',`show_qty`='".$data['showQty']."',`min_order_qty`='".$data['showQty']."'";
            $this->db->query($sql);
            $shelfProductId = $this->db->getLastId();
        }else{
            return -1;
        }
        return $shelfProductId;
    }

    protected function _shelfUpdate($solutionId) {
        $sql = "SELECT `shelf_id` FROM `shelf` WHERE `solution_id`='".$solutionId."'";
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $temp = array();
            $sql = "SELECT `product_class1`,`product_class2`,`product_class3` FROM `product` WHERE `product_id` IN (SELECT `product_id` FROM `shelf_product` WHERE `shelf_id`='".$row['shelf_id']."')";
            $q = $this->db->query($sql);
            foreach ($q->rows as $r) {
                $temp['class1'][$r['product_class1']] = 1;
                $temp['class2'][$r['product_class2']] = 1;
                $temp['class3'][$r['product_class3']] = 1;
            }
            $sql = "UPDATE `shelf` SET `shelf_class1`=',".implode(',', array_keys($temp['class1'])).",',`shelf_class2`=',".implode(',', array_keys($temp['class2'])).",',`shelf_class3`=',".implode(',', array_keys($temp['class3'])).",' WHERE `shelf_id`='".$row['shelf_id']."'";
            $this->db->query($sql);
        }
    }

    protected function _solutionUpdate($solutionId) {
        $sql = "SELECT `shelf_type`,`shelf_class1`,`shelf_class2`,`shelf_class3`,`shop_type`,`pay_grades`,`grade_start_search`,`grade_end_search` FROM `shelf` WHERE `solution_id`='".$solutionId."'";
        $query = $this->db->query($sql);
        $temp = array();
        $shelfLength = 0;
        foreach ($query->rows as $row) {
            $temp['shelf_types'][$row['shelf_type']] = 1;
            $temp['shelf_class1'] = $this->_initClass($row['shelf_class1'], $temp['shelf_class1']);
            $temp['shelf_class2'] = $this->_initClass($row['shelf_class2'], $temp['shelf_class2']);
            $temp['shelf_class3'] = $this->_initClass($row['shelf_class3'], $temp['shelf_class3']);
            $temp['shop_type'][$row['shop_type']] = 1;
            $temp['pay_grades'][$row['pay_grades']] = 1;
            $temp['grade_start_search'] = ($temp['grade_start_search'])?min($temp['grade_start_search'], $row['grade_start_search']):$row['grade_start_search'];
            $temp['grade_end_search'] = ($temp['grade_end_search'])?max($temp['grade_end_search'], $row['grade_end_search']):$row['grade_end_search'];
            $shelfLength++;
        }
        $sql = "
        UPDATE
            `solution`
        SET
            `shelf_types`=',".implode(',', array_keys($temp['shelf_types'])).",',
            `shelf_class1`=',".implode(',', array_keys($temp['shelf_class1'])).",',
            `shelf_class2`=',".implode(',', array_keys($temp['shelf_class2'])).",',
            `shelf_class3`=',".implode(',', array_keys($temp['shelf_class3'])).",',
            `shop_types`=',".implode(',', array_keys($temp['shop_type'])).",',
            `pay_grades`=',".implode(',', array_keys($temp['pay_grades'])).",',
            `shelf_length`='".$shelfLength."',
            `grade_start_search`='".$temp['grade_start_search']."',
            `grade_end_search`='".$temp['grade_end_search']."'
        WHERE
            `solution_id`='".$solutionId."'";
        $this->db->query($sql);
    }

    protected function _initClass($list, $ret=array()) {
        $temp = explode(',', $list);
        foreach ($temp as $v) {
            if ('' != $v) {
                $ret[$v] = 1;
            }
        }
        return $ret;
    }
}
