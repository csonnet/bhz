<?php
class ModelToolLycTool extends Model {
	
	//重建商城商品索引
	public function rebuildProductIndex(){

		require_once(DIR_SYSTEM . 'xunsearch/php/lib/XS.php');

		$results = $this->getIndexData();

		$xs = new XS('bhz');
		$index = $xs->index;

		$index->clean(); //清空索引
		$doc = new XSDocument; // 创建文档对象
		
		// 宣布开始重建索引
		$index->beginRebuild();

		foreach($results as $data){

			$doc->setFields($data);
			$index->add($doc);

		}

		// 告诉服务器重建完毕
		$index->endRebuild();	

	}
	
	//获取商城商品索引必要信息
	public function getIndexData(){

		$data = array();
		$where['p.status'] = 1;
		$products = M('product')
			->alias('p')
			->join('product_description pd on p.product_id = pd.product_id','left')
			->join('category_description cd1 on p.product_class1 = cd1.category_id','left')
			->join('category_description cd2 on p.product_class2 = cd2.category_id','left')
			->join('category_description cd3 on p.product_class3 = cd3.category_id','left')
			->join('manufacturer m ON m.manufacturer_id=p.manufacturer_id','left')
			->where($where)
			->field('p.product_id,p.status,pd.name,pd.description,cd1.name as class1,cd2.name as class2,cd3.name as class3,p.model,p.p_type as ptype,m.name as manufacturer')
			->select();

		foreach($products as $v){

			if($v['class3'] == null){
				$category = $v['class1']." ".$v['class2'];
			}
			else{
				$category = $v['class1']." ".$v['class2']." ".$v['class3'];
			}
			
			$data[] = array(
			
				'product_id' => $v['product_id'],
				'name' => $v['name'],
				'category' => $category,
				'ptype' => $v['ptype'],
				'model' => $v['model'],
				'manufacturer' => $v['manufacturer'],
				'description' => $v['description'],
				'status' => $v['status']

			);

		}

		return $data;

	}

	//重建小程序搜索商铺商品索引
	public function rebuildWxappShopProducts(){

		require_once(DIR_SYSTEM . 'xunsearch/php/lib/XS.php');

		$results = $this->getShopProductsData();

		$xs = new XS('wxapp');
		$index = $xs->index;

		$index->clean(); //清空索引
		$doc = new XSDocument; // 创建文档对象
		
		// 宣布开始重建索引
		$index->beginRebuild();

		foreach($results as $data){

			$doc->setFields($data);
			$index->add($doc);

		}

		// 告诉服务器重建完毕
		$index->endRebuild();	

	}

	//获取小程序商铺商品索引必要信息
	public function getShopProductsData(){

		$data = array();
		$products = M('shop_inventory')
			->alias('si')
			->join('product p on si.product_id = p.product_id','left')
			->join('product_description pd on si.product_id = pd.product_id','left')
			->join('category_description cd1 on p.product_class1 = cd1.category_id','left')
			->join('category_description cd2 on p.product_class2 = cd2.category_id','left')
			->join('category_description cd3 on p.product_class3 = cd3.category_id','left')
			->join('manufacturer m ON m.manufacturer_id=p.manufacturer_id','left')
			->where($where)
			->field('si.shop_inventory_id,si.shop_id,si.status as status,pd.name,pd.description,cd1.name as class1,cd2.name as class2,cd3.name as class3,p.model,p.p_type as ptype,m.name as manufacturer')
			->select();

		foreach($products as $v){

			if($v['class3'] == null){
				$category = $v['class1']." ".$v['class2'];
			}
			else{
				$category = $v['class1']." ".$v['class2']." ".$v['class3'];
			}
			
			$data[] = array(
			
				'shop_inventory_id' => $v['shop_inventory_id'],
				'shop_id' => $v['shop_id'],
				'name' => $v['name'],
				'category' => $category,
				'ptype' => $v['ptype'],
				'model' => $v['model'],
				'manufacturer' => $v['manufacturer'],
				'description' => $v['description'],
				'status' => $v['status']

			);

		}

		return $data;

	}

	//获取用户
	public function getCustomers($data = array()){
		
		$sql = "SELECT customer_id,telephone FROM " . DB_PREFIX . "customer WHERE 1 ";

		if (!empty($data['filter_tel'])) {
			$sql .= "AND telephone LIKE '%".$data['filter_tel']."%' ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;

	}

}
?>