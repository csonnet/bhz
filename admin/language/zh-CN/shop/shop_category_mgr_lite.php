<?php

$_['category_mgr_heading_title'] = '超市分类树管理';

// Text
$_['text_category'] = '分类';

// Column
$_['column_image']  = '图像';
$_['column_name']  = '商品名称';
$_['column_model']  = '型号';
$_['column_price']  = '价格';
$_['column_quantity']  = '数量';
$_['column_status']  = '状态';
$_['column_action']  = '操作';

// Button
$_['button_category_edit']   = '编辑分类';
$_['button_category_add']   = '添加分类';
$_['button_category_collapse'] = '全部收起';
$_['button_category_expand'] = '全部展开';
$_['button_product_edit']   = '编辑产品';

// Error
$_['category_mgr_error_permission'] = '警告：你没有权限修改此模块';

?>