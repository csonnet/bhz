<?php
// Heading
$_['heading_title']       = '优惠券';

// Text
$_['text_success']        = '成功：您有修改过的优惠券！';
$_['text_list']           = '优惠券列表';
$_['text_add']            = '添加优惠券';
$_['text_edit']           = '编辑优惠券';
$_['text_percent']        = '百分比';
$_['text_amount']         = '固定金额';
$_['text_none']           = ' --- 无 --- ';

// Column
$_['column_name']         = '优惠券名称';
$_['column_code']         = '代码';
$_['column_discount']     = '优惠';
$_['column_date_start']   = '开始日期';
$_['column_date_end']     = '结束日期';
$_['column_status']       = '状态';
$_['column_order_id']     = '订单ID';
$_['column_customer']     = '客户';
$_['column_amount']       = '总额';
$_['column_date_added']   = '添加日期';
$_['column_action']       = '管理';

// Entry
$_['entry_name']          = '优惠券名称';
$_['entry_code']          = '代码';
$_['entry_type']          = '类型';
$_['entry_discount']      = '优惠';
$_['entry_logged']        = '客户登陆';
$_['entry_shipping']      = '免运费';
$_['entry_total']         = '总额';
$_['entry_category']      = '分类';
$_['entry_product']       = '商品';
$_['entry_date_start']    = '开始日期';
$_['entry_date_end']      = '结束日期';
$_['entry_uses_total']    = '优惠券使用总数';
$_['entry_uses_customer'] = '每人使用次数';
$_['entry_status']        = '状态';
$_['entry_vendor_name']   = '品牌厂商名称';
$_['button_add']          = '添加New';

// Help
$_['help_code']           = '客户进入的代码得到的折扣。';
$_['help_type']           = '百分比或固定金额。';
$_['help_logged']         = '用户必须登录使用该优惠券。';
$_['help_total']          = '必须在有效期之前到达的总金额。';
$_['help_category']       = '选择所有商品在选定类别。';
$_['help_product']        = '选择特定商品的优惠券将适用于。选择没有商品，以适用于整个购物车。';
$_['help_uses_total']     = '优惠券可用于任何客户的最大次数。不限制留空';
$_['help_uses_customer']  = '优惠券可用于单个客户的最大次数。不限制留空';

// Error
$_['error_permission']    = '警告：您没有权限修改优惠券！';
$_['error_exists']        = '警告：优惠券代码已在使用中！';
$_['error_name']          = '优惠券名称必须介于 3 至 128 个字符之间！';
$_['error_code']          = '代码必须介于 3 至 10 个字符之间！';