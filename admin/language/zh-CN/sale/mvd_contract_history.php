<?php
// Heading
$_['heading_title']           = '合同历史';

// Text
$_['text_success']            = '成功：您已经修改了注册历史！';
$_['text_wait']               = '请等待！';
$_['text_month']              = '本月';
$_['text_year']               = '本年';
$_['text_s']               	  = '(s)';
$_['text_yes']                = '是';
$_['text_no']                 = '否';
$_['text_pending']            = '待处理';
$_['text_completed']          = '已完成';
$_['text_inactive'] 		  = '无效';
$_['text_active'] 			  = '管理';
$_['text_expired']            = '逾期';
$_['text_products']    	      = '商品';
$_['text_list']       		  = '合同历史列表';
$_['date_format_short2']      = 'Y-m-d';

// Column
$_['column_contract_id'] 		= '合同I';
$_['column_username'] 			= '用户';
$_['column_vendor_name'] 		= '品牌厂商名称';
$_['column_signup_plan'] 		= '注册带佣金级别 / 服务';
$_['column_signup_duration'] 	= '期 | (月数/年数)';
$_['column_signup_amount'] 		= '价格';
$_['column_status'] 			= '状态';
$_['column_date_start'] 		= '注册日期';
$_['column_date_end'] 			= '下个日期';
$_['column_paid_date'] 			= '付款日期';
$_['column_paid_status'] 		= '已付款';
?>