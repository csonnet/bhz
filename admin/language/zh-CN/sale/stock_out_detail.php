<?php
// Heading
$_['heading_title']            	 = '出库单详情';

// Text
$_['text_list']                  = '出库单详情列表';


//Column
$_['column_product_name']            = '商品名称';
$_['column_product_code']            = '商品编码';
$_['column_product_quantity']        = '商品数量';
$_['column_product_price']           = '商品单价';
$_['column_products_money']          = '商品总价';