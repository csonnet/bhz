<?php
// Heading
$_['heading_title']            	 = '出库单';

// Text
$_['text_list']                  = '出库单列表';
$_['text_edit']                  = '编辑出库单';
$_['text_confirm']               = '是否无效';
$_['text_confirm2']              = '是否恢复';
$_['text_confirm3']              = '是否完成';
$_['text_stock_out_detail']      = '出库单详情';
$_['text_stock_out_delete']      = '无效出库单';
$_['text_stock_out_recover']     = '恢复出库单';
$_['text_stock_out_edit']        = '修改出库单';
$_['text_stock_out_add']         = '添加出库单';
$_['text_stock_out_complete']    = '完成出库单';

// Column
$_['column_out_id']              = '出库单 ID';
$_['column_refer_id']            = '销售单 ID';
$_['column_refer_type']          = '单据类型';
$_['column_warehouse']           = '仓库';
$_['column_user']                = '用户';
$_['column_money']               = '总价';
$_['column_status']              = '状态';
$_['column_out_date']            = '出库时间';
$_['column_date_added']          = '创建时间';
$_['column_action']              = '操作';