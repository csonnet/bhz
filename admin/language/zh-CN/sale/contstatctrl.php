<?php
// Heading
$_['heading_title']           = '合同状态修改';

// Text
$_['text_success']            = '成功：你有修改合同状态修改！';
$_['text_email_success']      = '成功：您已成功发送电子邮件给品牌厂商！';
$_['text_wait']               = '请等待';
$_['text_month']              = '本月';
$_['text_year']               = '年';
$_['text_yes']                = '是';
$_['text_no']                 = '否';
$_['text_days'] 			  = ' 天数';
$_['text_pending']            = '待处理';
$_['text_completed']          = '已完成';
$_['text_inactive'] 		  = '无效';
$_['text_active'] 			  = '管理';
$_['text_expired']            = '逾期';
$_['text_list']               = '合同状态修改列表';
$_['date_format_short2']      = 'Y-m-d';

// Column
$_['column_contract_id'] 		= '合同ID';
$_['column_username'] 			= '用户';
$_['column_vendor_name'] 		= '品牌厂商名称';
$_['column_signup_plan'] 		= '注册有佣金级别 / 服务';
$_['column_signup_duration'] 	= '期 | (月数/年数)';
$_['column_signup_amount'] 		= '价格';
$_['column_status'] 			= '状态';
$_['column_remaining_days'] 	= '剩余天数';
$_['column_date_start'] 		= '注册日期';
$_['column_date_end'] 			= '到期日';
$_['column_paid_date'] 			= '付款日期';
$_['column_paid_status'] 		= '已付款';

// Button
$_['button_update'] 			= '更新';
$_['button_delete'] 			= '删除';
$_['button_sendEmail'] 			= '发邮件通知';

// Error
$_['error_permission']        = '警告：你没有许可合同状态修改！';
?>