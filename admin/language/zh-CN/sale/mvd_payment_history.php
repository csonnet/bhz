<?php
// Heading
$_['heading_title']           = '付款历史';

// Text
$_['text_wait']               = '请等待！';
$_['text_list']               = '支付历史列表';
$_['date_format_short2']      = 'Y-m-d';

// Column
$_['column_vendor_name'] 	  = '品牌厂商名称';
$_['column_payment_amount']   = '支付金额';
$_['column_payment_date'] 	  = '支付日期';
$_['column_order_product'] 	  = '商品订单 [订单 ID - 商品名称]';

// Error
$_['error_permission']        = '警告：你没有许可付款历史！';
$_['error_warning']           = '警告：请检查表格上的错误！';

?>