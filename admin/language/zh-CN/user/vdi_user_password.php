<?php
// Heading
$_['heading_title']     = '修改密码';

// Text
$_['text_success']      = '成功：您修改了用户！';
$_['text_list']         = '用户列表';
$_['text_add']          = '添加用户';
$_['text_edit']         = '编辑用户';

//mvd
$_['entry_folder_path']     	= '目录路径';
$_['entry_folder_path_remove']  = '目录路径';
$_['entry_vendor']     			= '品牌厂商名称';
$_['entry_category']   			= '分类权限';
$_['entry_store']      			= '商店权限';
$_['entry_expired_date']  		= '逾期时间'; 
$_['text_change_password'] 		= '成功： 密码已经修改！';
$_['text_remove']               = '检查移除 <b>%s</b> 文件夹';
$_['help_folder_delete']        = '<font color="red">警告：删除 <b> %s </b>文件夹将永久和不能恢复。</font>';
$_['txt_pending_approval']  	= '待审批'; 
$_['txt_start_date']  			= '开始日期';
$_['txt_end_date']  			= '结束日期';
$_['text_none']                 = ' --- 无 --- ';

$_['help_folder_path']     		= '创建一个品牌厂商文件夹，以隔离所有上传的文件在品牌厂商。';
$_['help_folder_path_remove']  	= '警告：删除此文件夹将删除该文件夹中的所有文件';
$_['help_expired_date']  		= '用户帐户过期日期。>空白或把它设置到00 00 0000 \“\”不限。>日期格式：（mm-dd-yyyy）'; 		
$_['help_map_vendor_profile']   = '此用户帐户不能映射到多个品牌厂商配置文件';
//mvd

// Column
$_['column_username']   = '用户名称';
$_['column_status']     = '状态';
$_['column_date_added'] = '添加日期';
$_['column_action']     = '管理';

// Entry
$_['entry_username']   	= '用户名称';
$_['entry_user_group'] 	= '用户组';
$_['entry_password']   	= '密码';
$_['entry_confirm']    	= '确定';
$_['entry_firstname']  	= '名字';
$_['entry_lastname']   	= '姓氏';
$_['entry_email']      	= 'E-Mail';
$_['entry_image']      	= '图片';
$_['entry_status']     	= '状态';

// Error
$_['error_permission'] 	= '警告：您还没有权限修改用户！';
$_['error_account']    	= '警告：您不能删除您自己的帐户！';
$_['error_exists']     	= '警告：用户已经在使用！';
$_['error_username']   	= '用户名称必须在 3 至 20 个字符之间';
$_['error_password']   	= '密码必须在 4 至 20 个字符之间！';
$_['error_confirm']    	= '确认密码和输入密码不一致！';
$_['error_firstname']  	= '名字必须在 1 至 32 个字符之间！';
$_['error_lastname']   	= '姓氏必须在 1 至 32 字符之间！';