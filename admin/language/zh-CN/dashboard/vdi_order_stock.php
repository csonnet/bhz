<?php
// Heading
$_['heading_title']                = '最近订单及库存状况';

// Text
$_['text_new_order_product']    = ' 被添加到新订单编号 <a href="order_id=%s">%s</a>.';
$_['text_low_stock_product']    = '<i class="fa fa-exclamation-triangle" style="color:red"></i> <a href="product_id=%s">%s</a> 最低库存 <span class="label label-danger">%s</span> 状态.';

?>