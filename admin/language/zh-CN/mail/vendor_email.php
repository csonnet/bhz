<?php
// Text
$_['text_vendor_email']  		= '自动电子邮件';
$_['text_vendor_email_subject'] = '自动邮箱 - ';
$_['text_vendor_order_id'] 		= '订单 ID: ';
$_['text_title']     			   = '欢迎 ';
$_['text_vendor_contact'] 		= '联系品牌厂商';
$_['text_shipping_address'] 	= '货运地址';
$_['text_order_details'] 		  = '订单明细';
$_['text_date_ordered'] 		  = '订单添加日期';
$_['text_email']  				    = 'Email: ';
$_['text_telephone']  			  = '电话: ';
$_['text_order_status']     	= '订单状态: ';
$_['text_payment_method']   	= '支付方式：';
$_['text_vendor_auto_msg']    = '这是从系统自动生成的消息，请不要回复。谢谢你。';

// Column
$_['column_product']        = '商品';
$_['column_model']          = '型号';
$_['column_quantity']       = '数量';
$_['column_unit_price']     = '单价';
$_['column_total']          = '总计';
$_['column_shipping']       = '货运基于重量';
$_['column_subtotal']       = '小计';
?>
