<?php
// Heading
$_['heading_title']      = '添加/删除商品电子邮件通知';

// Text
$_['text_to']    		= '您好 %s,';
$_['text_subject_add']  = '新商品 %s 提交从 %s';
$_['text_subject_edit'] = '%s 编辑商品 %s';
$_['text_subject_approve'] = '%s 被批准';
$_['text_subject_expire'] = '您的帐户将过期 %s';

$_['text_message_add']  = '这是告诉你，已经提交了一个新的商品<b>%s</b> 在这个商店.';
$_['text_message_edit'] = '这是通知你的商品 <b>%s</b> 已编辑。请再次检查！';
$_['text_message_approve'] = '这是通知你的商品 <b>%s</b> 被批准.';
$_['text_message_expire'] = '这是通知您，您的帐户将过期 <b>%s</b>.<br/> 请尽快续约，以避免账户关闭。<br/><br/>请登录到您的帐户，去个人详细 -> 历史合同 -> 续签， 合同，续合同。';

$_['text_thanks']    	= '感谢您，';
$_['text_system']    	= '<span class="help">这是系统生成的消息。请不要回答。</span>';
?>
