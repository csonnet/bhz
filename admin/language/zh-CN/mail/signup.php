<?php
// Text
$_['text_dear']  	= '您好 %s';
$_['text_subject1'] = '%s - 您已被选中参加我们的品牌厂商计划';
$_['text_welcome1'] = '欢迎和感谢您对 %s 的品牌厂商计划感兴趣！';
$_['text_subject']  = '%s - 感谢您的注册';
$_['text_url']  	= '网址 : ';
$_['text_info']     = '<b><u>登录凭证</u></b>';
$_['text_username'] = '用户 : ';
$_['text_password'] = '密码 : ';
$_['text_welcome']  = '欢迎并感谢注册 %s！';
$_['text_login']    = '您的帐户已经创建，您可以登录使用您的登录凭据通过以下网址:';
$_['text_services'] = '登录后，您可以管理帐户。';
$_['text_thanks']   = '谢谢,';
?>