<?php
// Heading
$_['heading_title']         = '会员';

// Text
$_['text_success']          = '成功: 已修改会员！';
$_['text_list']             = '会员列表';
$_['text_add']              = '添加会员';
$_['text_edit']             = '编辑会员';
$_['text_default']          = '默认';
$_['text_balance']          = '账户';

// Column
$_['column_name']           = '会员姓名';
$_['column_email']          = 'E-Mail';
$_['column_telephone']      = '电话';
$_['column_company_name']   = '公司名称';
$_['column_customer_group'] = '会员等级';
$_['column_status']         = '状态';
$_['column_date_added']     = '添加日期';
$_['column_comment']        = '备注';
$_['column_description']    = '描述';
$_['column_amount']         = '金额';
$_['column_points']         = '奖励积分';
$_['column_ip']             = 'IP';
$_['column_total']          = '账户总数量';
$_['column_action']         = '操作';
$_['column_need_review']         = '需要审核';
$_['column_is_magfin']         = '需要审核免息账期';
$_['column_logcenter_id']         = '物流营销中心';
$_['column_recommended_code']         = '推荐码';

// Entry
$_['entry_customer_group']  = '会员等级';
$_['entry_fullname']       = '姓名';
$_['entry_email']           = 'E-Mail';
$_['entry_telephone']       = '电话';
$_['entry_fax']             = '传真';
$_['entry_newsletter']      = '订阅邮件';
$_['entry_status']          = '状态';
$_['entry_approved']        = '审核';
$_['entry_safe']            = '安全';
$_['entry_password']        = '密码';
$_['entry_confirm']         = '确认密码';
$_['entry_company']         = '公司';
$_['entry_address']       = '地址';
$_['entry_city']            = '县区';
$_['entry_postcode']        = '邮政编码';
$_['entry_country']         = '省市';
$_['entry_zone']            = '市县';
$_['entry_default']         = '默认地址';
$_['entry_comment']         = '备注';
$_['entry_description']     = '描述';
$_['entry_points']          = '奖励积分';
$_['entry_name']            = '会员姓名';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = '添加日期';
$_['entry_date_start']      = '最早添加日期';
$_['entry_date_end']      = '最晚添加日期';
$_['entry_company_name']      = '公司名称';
$_['entry_license_image']      = '营业执照';
$_['entry_need_review']      = '需要审核';
$_['entry_is_magfin']      = '需要审核免息账期';
$_['entry_logcenter_id']      = '物流营销中心';
$_['entry_recommended_code'] = '推荐码';
$_['entry_shipping_telephone']      = '收货人电话';

// Help
$_['help_safe']             = 'Set to true to avoid this customer from being caught by the anti-fraud system';
$_['help_points']           = '适用减号-以减少奖励积分';

// Error
$_['error_warning']         = '警告: 详细检查相关错误！';
$_['error_permission']      = '警告: 无权限修改会员！';
$_['error_exists']          = '警告: E-Mail 已被注册使用了！';
$_['error_fullname']        = '姓名必须为1-32字符！';
$_['error_email']           = 'E-Mail 无效！';
$_['error_telephone']       = '电话必须为3-32字符！';
$_['error_password']        = '密码密续为4-20字符！';
$_['error_confirm']         = '确认密码与密码不一致！';
$_['error_address']         = '地址必须为3-128字符！';
$_['error_city']            = '清选择县区！';
$_['error_postcode']        = '邮政编码必须为2-10字符！';
$_['error_country']         = '清选择省市！';
$_['error_zone']            = '清选择市县！';
$_['error_custom_field']    = '%s 必填！';
$_['error_shipping_telephone']         = '收货人电话必须在 6 至 20 个字符之间！';

/*账户余额*/
$_['entry_balance'] = '账户余额';
$_['entry_amount'] = '金额';
/*账户余额*/
