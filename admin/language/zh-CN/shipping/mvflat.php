<?php
// Heading
$_['heading_title']    = '多品牌厂商/托运人固定费率';

// Text
$_['text_edit']         = '编辑多品牌厂商/托运人固定货运费';
$_['text_shipping']     = '货运';
$_['text_success']      = '成功：你有改进的平板率航运！';
$_['text_select_all']   = '全选';
$_['text_unselect_all'] = '全部取消';

// Entry
$_['entry_shipping']   = '货运办法';
$_['entry_tax_class']  = '税类型';
$_['entry_geo_zone']   = '区域群组';
$_['entry_status']     = '状态';
$_['entry_sort_order'] = '排序';

//help
$_['help_shipping']    = '商品页面首选的装运方式必须与所选装运方法相匹配';

// Error
$_['error_permission'] = '警告：您没有权限修改固定航运费率！';
?>