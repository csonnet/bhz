<?php
// Heading
$_['heading_title']    = '多品牌厂商/托运人多单位费率';

// Text
$_['text_edit']         = '编辑多品牌厂商/托运人货运费率';
$_['text_shipping']     = '货运';
$_['text_success']      = '成功：您已经修改了货运费率!';
$_['text_select_all']   = '全选';
$_['text_unselect_all'] = '全部取消';

// Entry
$_['entry_shipping']   = '首选的运输方法：<br /><span class="help"><font color="red">* </font>商品页面选择运输方法必须匹配选择的送货方式</span>';
$_['entry_tax_class']  = '税类型：';
$_['entry_geo_zone']   = '区域群组：';
$_['entry_status']     = '状态：';
$_['entry_sort_order'] = '排序：';

// Error
$_['error_permission'] = '警告：您没有权限修改平板率航运！';
?>