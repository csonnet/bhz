<?php

// Heading
$_['heading_title'] = '入库单';

//Text
$_['text_list'] = '入库单';
$_['text_view_product'] = '查看库存商品';
$_['text_enabled'] = '启用';
$_['text_disabled'] = '停用';
$_['text_confirm_enable'] = '确认启用吗?';
$_['text_confirm_disable'] = '确认停用吗?';
$_['text_success'] = '成功: 你已经修改了仓库!';
$_['text_add'] = '新增仓库';
$_['text_edit'] = '修改仓库';
$_['text_normal_type'] = '普通仓库';
$_['text_defect_type'] = '残次品仓库';

// Entry
$_['entry_warehouse_name'] = '名称';
$_['entry_country'] = '省市';
$_['entry_zone'] = '区县';
$_['entry_city'] = '市区';
$_['entry_address'] = '地址';
$_['entry_postcode'] = '邮编';
$_['entry_type'] = '类型';
$_['entry_status'] = '状态';

// Column
$_['column_warehouse_name'] = '名称';
$_['column_type'] = '类型';
$_['column_status'] = '状态';
$_['column_action'] = '管理';

//Button
$_['buttons_enable'] = '批量启用';
$_['buttons_disable'] = '批量停用';

?>