<?php

// Heading
$_['heading_title'] = '库存查询';

//Text
$_['text_list'] = '库存查询列表';
$_['text_edit'] = '编辑库存';

//Entry
$_['entry_product_name'] = '商品编码或名称';
$_['entry_warehouse_type'] = '仓库类型';
$_['entry_warehouse'] = '仓库';
$_['entry_product_name'] = '商品名称';
$_['entry_product_code'] = '商品编码';
$_['entry_option_name'] = '选项';
$_['entry_sku'] = '条形码';
$_['entry_vendor_name'] = '供应商';
$_['entry_warehouse_position'] = '库位';
$_['entry_position1'] = '库位1';
$_['entry_position2'] = '库位2';
$_['entry_safe_quantity'] = '安全库存';

// Column
$_['column_warehouse_name'] = '仓库';
$_['column_position1'] = '库位1';
$_['column_position2'] = '库位2';
$_['column_product_code'] = '商品编码';
$_['column_product_sku'] = '条形码';
$_['column_product_name'] = '商品名称';
$_['column_product_option'] = '选项';
$_['column_vendor_name'] = '供应商';
$_['column_account_quantity'] = '财务数量';
$_['column_available_quantity'] = '可用数量';
$_['column_safe_quantity'] = '安全库存';
$_['column_action'] = '管理';

?>
