<?php

// Heading
$_['heading_title']           = '省市';

// Text
$_['text_success']            = '成功: 已修改省市！';
$_['text_list']               = '省市列表';
$_['text_add']                = '添加省市';
$_['text_edit']               = '编辑省市';

// Column
$_['column_name']             = '省市名称';
$_['column_iso_code_2']       = 'ISO Code (2)';
$_['column_iso_code_3']       = 'ISO Code (3)';
$_['column_action']           = '操作';

// Entry
$_['entry_name']              = '省市名称';
$_['entry_iso_code_2']        = 'ISO Code (2)';
$_['entry_iso_code_3']        = 'ISO Code (3)';
$_['entry_address_format']    = '地址格式';
$_['entry_postcode_required'] = '需要邮政编码';
$_['entry_status']            = '状态';

// Help
$_['help_address_format']     = '姓名 = {fullname}<br />公司 = {company}<br />地址 = {address}<br />县区 = {city}<br />邮政编码 = {postcode}<br />市县 = {zone}<br />地区编码 = {zone_code}<br />省市 = {country}';

// Error
$_['error_permission']        = '警告: 无权限修改省市！';
$_['error_name']              = '省市名称必须介于3-128字符之间！';
$_['error_default']           = '警告: 不能删除此省市，该省市已关联为默认商店所在省市！';
$_['error_store']             = '警告: 不能删除此省市，该省市已关联到 %s 个商店！!';
$_['error_address']           = '警告: 不能删除此省市，该省市已关联到 %s 个地址簿！';
$_['error_affiliate']         = '警告: 不能删除此省市，该省市已关联到 %s 个推广会员！';
$_['error_zone']              = '警告: 不能删除此省市，该省市已关联到 %s 个地区！';
$_['error_zone_to_geo_zone']  = '警告: 不能删除此省市，该省市已关联到 %s 区域群组中的地区！';