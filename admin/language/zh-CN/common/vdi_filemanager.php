<?php
// Heading
$_['heading_title']    = '图片管理';

// Text
$_['text_uploaded']    = '成功：您的文件已经被上传！';
$_['text_directory']   = '成功：目录被创建！';
$_['text_delete']      = '成功：你的文件或者目录已经被删除！';

// Entry
$_['entry_search']     = '搜索..';
$_['entry_folder']     = '文件夹名称';

// Error
$_['error_permission'] = '警告：权限禁止！';
$_['error_filename']   = '警告：文件名必须在3 至 255 字符之间！';
$_['error_folder']     = '警告：文件夹名称必须在3 至 255 字符之间！';
$_['error_exists']     = '警告：文件或者目录已经存在！';
$_['error_directory']  = '警告 目录不存在！';
$_['error_filetype']   = '警告：非法的文件类型！';
$_['error_upload']     = '警告：文件不能被上传，由于其它原因！';
$_['error_delete']     = '警告：您不能删除这个目录！';