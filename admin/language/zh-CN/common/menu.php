<?php
// Text
$_['text_pim']   = '多图片上传管理';
$_['text_analytics']                   = '搜索引擎';
$_['text_affiliate']                   = '加盟管理';
$_['text_ground']                   = '地推管理';
$_['text_api']                         = 'API接口';
$_['text_attribute']                   = '属性';
$_['text_attribute_group']             = '属性组';
$_['text_backup']                      = '备份 / 恢复';
$_['text_export']           = 'Excel格式数据导入导出';
$_['text_banner']                      = '横幅广告';
$_['text_captcha']                     = '验证码';
$_['text_catalog']                     = '商品目录';
$_['text_category']                    = '商品分类';
$_['text_country']                     = '省市';
$_['text_coupon']                      = '折扣券';
$_['text_currency']                    = '货币';
$_['text_customer']                    = '会员';
$_['text_customer_group']              = '会员等级';
$_['text_custom_field']                = '自定义字段';
$_['text_dashboard']                   = '仪表盘';
$_['text_design']                      = '规划设计';
$_['text_download']                    = '下载文件管理';
$_['text_error_log']                   = '错误日志';
$_['text_extension']                   = '扩展功能';
$_['text_feed']                        = '输出共享';
$_['text_filter']                      = '筛选';
$_['text_fraud']                       = '反欺诈';
$_['text_geo_zone']                    = '区域群组';
$_['text_information']                 = '文章管理';
$_['text_installer']                   = '安装扩展功能';
$_['text_language']                    = '语言设置';
$_['text_layout']                      = '布局';
$_['text_localisation']                = '参数设置';
$_['text_location']                    = '线下商店';
$_['text_contact']                     = '邮件通知';
$_['text_marketing']                   = '市场推广';
$_['text_modification']                = '代码调整';
$_['text_manufacturer']                = '制造商';
$_['text_module']                      = '模组管理';
$_['text_option']                      = '选项';
$_['text_order']                       = "订单";
$_['text_order_status']                = '订单状态';
$_['text_payment']                     = '支付管理';
$_['text_paypal']                      = 'PayPal(贝宝)';
$_['text_paypal_search']               = '检索交易记录';
$_['text_product']                     = '商品管理';
$_['text_reports']                     = '报告';
$_['text_report_sale_order']           = '销售报告';
$_['text_report_sale_tax']             = '税种统计';
$_['text_report_sale_shipping']        = '配送报告';
$_['text_report_sale_return']          = '退货报告';
$_['text_report_sale_coupon']          = '折扣券报告';
$_['text_report_product_viewed']       = '商品浏览统计';
$_['text_report_product_purchased']    = '商品购买统计';
$_['text_report_customer_activity']    = '会员活动';
$_['text_report_customer_online']      = '在线会员';
$_['text_report_customer_order']       = '会员订单统计';
$_['text_report_customer_reward']      = '奖励积分统计';
$_['text_report_customer_credit']      = '账户余额';
$_['text_report_affiliate']            = '加盟佣金统计';
$_['text_report_affiliate_activity']   = '加盟会员活动';
$_['text_review']                      = '评论';
$_['text_return']                      = '商品退换';
$_['text_return_action']               = '退换操作';
$_['text_return_reason']               = '退换原因';
$_['text_return_status']               = '退换状态';
$_['text_sale']                        = '销售管理';
$_['text_shipping']                    = '配送管理';
$_['text_setting']                     = '网店设置';
$_['text_sms']	                       = '短信接口';
$_['text_stock_status']                = '库存状态';
$_['text_system']                      = '系统设置';
$_['text_tax']                         = '商品税种';
$_['text_tax_class']                   = '稅率类別';
$_['text_tax_rate']                    = '税率';
$_['text_tools']                       = '工具';
$_['text_total']                       = '订单总计';
$_['text_upload']                      = '上传文件';
$_['text_user']                        = '管理员';
$_['text_users']                       = '管理员管理';
$_['text_user_group']                  = '管理员群组';
$_['text_voucher']                     = '礼品券';
$_['text_voucher_theme']               = '礼品券主题';
$_['text_weight_class']                = '重量单位';
$_['text_length_class']                = '尺寸单位';
$_['text_zone']                        = '市县设置';
$_['text_recurring']                   = '分期付款';
$_['text_order_recurring']             = '分期付款订单';
$_['text_paypal_manage']               = 'PayPal';
$_['text_paypal_search']               = '检索';
$_['text_others']               	   = '其它';
$_['text_url_alias']               	   = 'SEO KeyWord 管理';
$_['text_weidian']	 			       = '微店管理';
$_['text_wdcategory'] 			       = '微店分类';
$_['text_wdproduct'] 			       = '微店商品';
$_['text_config'] 				       = '微店参数设置';
$_['text_add_vendors']                = '品牌厂商管理';
$_['text_vendor_tool']                = '品牌厂商';
$_['text_setup']                = '设置';
$_['text_vendor_setting']                = '全局设置';
$_['text_vendor_personal']                = '账户详情';
$_['text_vendor_update_profile']                = '更新账户';
$_['text_vendor_update_password']                = '修改密码';
$_['text_marketing_tools']                = '营销工具';
$_['text_luckydraw']                = '幸运抽奖';
$_['text_purchase_order']        = "采购单";
$_['text_add_logcenters']                = '账户管理';
$_['text_logcenter_tool']                = '物流营销中心';
$_['text_return_main_order']			= '退货单';
$_['text_trim_stock']					= '损益单';
$_['text_requisition_main']					= '调拨单';
$_['text_check_stock']						= '库存查看';
$_['text_remit_money'] = '余额打款';
$_['text_lend_order'] = '借货单';


/*库存相关*/
$_['text_inventory_manage'] = "仓库管理";
$_['text_stock_out'] = "出库";
$_['text_stock_in'] = "入库";
$_['text_warehouse'] = "仓库";
$_['text_stock_search'] = "库存查询";

$_['text_lyctool'] = '我的工具';
/*库存相关*/

/*应收应付相关*/
$_['text_money'] = "财务管理";
$_['text_money_in'] = "应收款项";
$_['text_money_out'] = "应付款项";
$_['text_customer_account'] = "账期客户管理";
$_['text_acount'] = "超市对账单";
$_['text_factory'] = "工厂对账单";
$_['text_distributor'] = "经销商对账";
$_['text_vdi_factory'] = "工厂对账单";
$_['text_refund'] = "退款";
$_['text_cost'] = "费用";

/*应收应付相关*/

/*采购相关*/
$_['text_purchase'] = "采购管理";
/*采购相关*/