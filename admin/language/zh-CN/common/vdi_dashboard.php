<?php
// Heading
$_['heading_title']                = '仪表盘';

//mvd
$_['text_total_product']           = '所有商品';
$_['text_total_shipping']          = '总运费';
$_['text_total_product_approval']  = '等待批准的商品';
$_['text_total_product_pendding']  = '待批准商品';
$_['text_total_vendor_approval']   = '品牌厂商帐户等待批准';
//mvd

// Text
$_['text_order_total']             = '所有订单';
$_['text_customer_total']          = '所有客户';
$_['text_sale_total']              = '所有销售';
$_['text_online_total']            = '在线人数';
$_['text_map']                     = '世界地图';
$_['text_sale']                    = '销售分析';
$_['text_activity']                = '最近的活动';
$_['text_recent']                  = '最新订单';
$_['text_order']                   = '订单';
$_['text_customer']                = '客户';
$_['text_day']                     = '今日';
$_['text_week']                    = '本周';
$_['text_month']                   = '本月';
$_['text_year']                    = '本年';
$_['text_view']                    = '查看更多...';

// Error
$_['error_install']                = '警告：安装文件夹仍存在，应删除安全原因！';