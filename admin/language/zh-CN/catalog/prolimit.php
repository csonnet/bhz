<?php
// Heading
$_['heading_title']      	= '商品限制';

// Text
$_['text_success']       	= '成功： 您已经修改了商品限制！';
$_['text_add']           	= '添加商品限制';
$_['text_edit']          	= '编辑商品限制';
$_['text_list']          	= '商品限制列表';

// Column
$_['column_name']			= '包名称';
$_['column_quantity'] 		= '数量';
$_['column_total_vendors'] 	= '全部品牌厂商';
$_['column_sort_order'] 	= '排序';
$_['column_action']     	= '管理';

// Entry
$_['entry_name']			= '包名称';
$_['entry_quantity']		= '数量';
$_['entry_sort_order']  	= '排序';

//help
$_['help_quantity']  		= '请为该包指定最多数量的商品。例如： <b>999999</b> 无限 ';

// Error
$_['error_permission']  	= '警告：您没有权限修改商品限制！';
$_['error_delete']     		= '警告：此包名称不能被删除，因为它目前被分配给 % 的品牌厂商！';
$_['error_required_data']  	= '警告：请填写必填项';
$_['error_package_name']	= '类型名称必须在 3 至 64 个字符！';
$_['error_product_limit']   = '数量必须是数字类型！';

?>
