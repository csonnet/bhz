<?php
// Heading
$_['heading_title']          = '信息';

// Text
$_['text_success']           = '成功：您已经修改了信息！';
$_['text_list']              = '信息列表';
$_['text_add']               = '添加信息';
$_['text_edit']              = '编辑信息';
$_['text_default']           = '默认';

// Column
$_['column_title']           = '信息标题';
$_['column_sort_order']	     = '排序';
$_['column_action']          = '管理';

// Entry
$_['entry_title']            = '信息标题';
$_['entry_description']      = '描述';
$_['entry_store']            = '商店';
$_['entry_meta_title'] 	     = 'Meta 标题';
$_['entry_meta_keyword'] 	 = 'Meta 关键字';
$_['entry_meta_description'] = 'Meta 描述';
$_['entry_keyword']          = 'SEO 关键字';
$_['entry_bottom']           = '底部';
$_['entry_status']           = '状态';
$_['entry_sort_order']       = '排序';
$_['entry_layout']           = '布局覆盖';


// Help
$_['help_keyword']           = '不要使用空格，而不是替换空格-确保关键字是全局唯一的。';
$_['help_bottom']            = '显示在底部。';

// Error
$_['error_warning']          = '警告：请检查表格上的错误！';
$_['error_permission']       = '警告：您没有修改信息的权限！';
$_['error_title']            = '信息标题必须介于 3 至 64 个字符之间！';
$_['error_description']      = '描述必须超过3个字符！';
$_['error_meta_title']       = 'Meta 标题必须大于3，小于255个字符！';
$_['error_account']          = '警告：此信息页不能被删除，因为它是当前分配的商店帐户项！';
$_['error_checkout']         = '警告：此信息页不能被删除，因为它是当前分配的商店结帐项！';
$_['error_affiliate']        = '警告：此信息页不能被删除，因为它目前被分配为商店附属条款！';
$_['error_return']           = '警告：此信息页不能被删除，因为它当前被分配为商店返回项！';
$_['error_store']            = '警告：无法删除此信息页作为其目前使用的%s 商店！';