<?php
// Heading
$_['heading_title']          = '属性';

// Text
$_['text_success']           = '成功: 您已经修改了属性!';
$_['text_list']              = '属性列表';
$_['text_add']               = '添加属性';
$_['text_edit']              = '编辑属性';

// Column
$_['column_name']            = '属性名称';
$_['column_attribute_group'] = '属性群组';
$_['column_sort_order']      = '排序';
$_['column_action']          = '管理';
$_['column_vendor_name']     = '品牌厂商名称';

// Entry
$_['entry_name']            = '属性名称';
$_['entry_attribute_group'] = '属性群组';
$_['entry_sort_order']      = '排序';

//mvds
$_['entry_vendor_name']		= '品牌厂商名称：';
$_['error_warning']     	= '警告：您没有权限修改属性!';
//mvde

// Error
$_['error_permission']      = '警告：您没有权限修改属性!';
$_['error_name']            = '属性名称必须在 3 至 64 字符！';
$_['error_product']         = '警告：属性不能被删除，已经有 %s 商品在使用！';