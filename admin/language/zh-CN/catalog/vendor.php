<?php
// Heading
$_['heading_title']      	= '品牌厂商';
$_['heading_title_profile'] = '更新品牌厂商介绍';

// Text
$_['text_success']       	= '成功：你已经修改了品牌厂商！';
$_['text_success_profile']  = '成功：您已经更新了介绍 !';
$_['text_default']       	= '默认';
$_['text_image_manager'] 	= '图片管理';
$_['text_disabled']      	= ' --- 禁用 --- ';
$_['text_browse']           = '浏览文件';
$_['text_clear']            = '清理图片';
$_['text_fixed_rate']    	= '固定金额';
$_['text_percentage']    	= '百分比';
$_['text_pf']    		 	= '百分比 + 固定费率';
$_['text_fp']    		 	= '固定费 + 百分比率';
$_['text_month']    	 	= '月';
$_['text_year']    		 	= '年';
$_['text_subs_fee']    	 	= '认购金额 : $';
$_['text_products']    	 	= ' 商品';
$_['text_view_vendor_products'] = '浏览商品';
$_['text_add']           	= '添加品牌厂商';
$_['text_edit']          	= '编辑品牌厂商';
$_['text_list']          	= '品牌厂商列表';
$_['text_change_password']  = '成功：密码修改完成！';
$_['text_remove']           = '检查删除 <b>%s</b> 文件夹。';
$_['txt_pending_approval']  = '待审批';
$_['txt_disabled_approval'] = '禁用';  
$_['txt_start_date']  	    = '开始日期';
$_['txt_end_date']  		= '结束日期';
$_['text_none']             = ' --- 无 --- ';
$_['txt_pending_approval']  = '待审批'; 

// Column
$_['column_image']       		= '图片';
$_['column_vendor_name']  		= '品牌厂商名称';
$_['column_vendor_commission']  = '佣金比率';
$_['column_total_products'] 	= '所有商品';
$_['column_vendor_approved']  	= '已审批';
$_['column_status']  			= '状态';
$_['column_action']      		= '管理';

// Entry
$_['entry_username1']     = '用户名';
$_['entry_vendor_name']   = '品牌厂商名称';
$_['entry_user_account']  = '用户账户';
$_['entry_company']       = '公司名称';
$_['entry_contact_name']  = '联系人';
$_['entry_firstname']     = '联系人名字';
$_['entry_lastname']      = '姓名';
$_['entry_telephone']     = '电话';
$_['entry_commission']    = '佣金';
$_['entry_limit']    	  = '商品限制';
$_['entry_fax']     	  = '传真';
$_['entry_email']         = 'Email';
$_['entry_paypal_email']  = 'PayPal Email';
$_['entry_company_id']    = '公司ID';
$_['entry_iban']     	  = '银行账号';
$_['entry_bank_name']     = '银行名称';
$_['entry_bank_addr']     = '银行地址';
$_['entry_swift_bic']     = '其他账号';
$_['entry_tax_id']     	  = 'Tax ID';
$_['entry_description']   = '企业简介';
$_['entry_address_1']     = '地址';
$_['entry_address_2']     = '地址 2';
$_['entry_postcode']      = '邮编';
$_['entry_city']          = '区县';
$_['entry_country']       = '省市';
$_['entry_zone']          = '市区';
$_['entry_store_url']     = 'Store Url';
$_['entry_image']         = '品牌logo';
$_['entry_image_main']	  = '主页图像';
$_['entry_status']     	  = '状态';
$_['entry_user_group'] 	  = '用户群';
$_['entry_accept_paypal'] = '接受 PayPal';
$_['entry_sort_order']    		= '排序';
$_['entry_folder_path']     	= '文件夹路径';
$_['entry_folder_path_remove']  = '文件夹路径';
$_['entry_vendor']     			= '品牌厂商名称';
$_['entry_category']   			= '分类权限';
$_['entry_store']      			= '商店权限';
$_['entry_expired_date']  		= '过期日期'; 
$_['entry_accept_bank_transfer']= '接受银行转账';
$_['entry_accept_cheques']      = '接受支票';
$_['entry_password']   			= '密码';
$_['entry_confirm']    			= '确认';

//help
$_['help_user_account']  = '选择用户帐户和地图创建品牌厂商配置文件来管理品牌厂商商品';
$_['help_username1']  	 = '创建新的帐户仅当不想使用现有的用户帐户';
$_['help_commission']    = '为每一个成功交易设置佣金百分比';
$_['help_limit']    	 = '可将总的商品设置为添加到该品牌厂商';
$_['help_email']         = '通知通过电子邮件发送的电子邮件地址时激活';
$_['help_paypal_email']  = '商店管理员通过PayPal电子邮件账户支付品牌厂商t';
$_['help_image']         = '图片上传将用作公司标志';
$_['help_folder_path']     		= '创建一个品牌厂商文件夹，以隔离所有上传的文件在品牌厂商。';
$_['help_folder_path_remove']  	= '警告：删除此文件夹将删除该文件夹中的所有文件';
$_['help_expired_date']  		= '用户帐户过期日期。 <br/> 把它设置到空白或00 00 0000的\“\”不限。 <br/> 日期格式 : (yyyy-mm-dd)'; 		
$_['help_map_vendor_profile']   = '此用户帐户不能映射到多个品牌厂商配置文件';
$_['help_folder_delete']    	= '<font color="red">警告 : 删除这 <b> %s </b>文件夹将永久和不能恢复.</font>';
$_['help_password']         	= '输入密码，如果你想包括用户名和密码的电子邮件通知。';

//tab
$_['tab_general']    = '通用';
$_['tab_finance']    = '财务';
$_['tab_address']    = '地址';
$_['tab_commission'] = '佣金';
$_['tab_payment']    = '支付';
$_['tab_shipping']   = '货运';
$_['tab_setting']    = '设置';

// Error
$_['error_exists']         = '警告：E-Mail地址已经被注册！';
$_['error_username_exists']= '警告：名称已经被使用！';
$_['error_permission']     = '警告：您没有权限修改品牌厂商！';
$_['error_vendor_name']    = '品牌厂商名称必须介于3和64个字符之间！';
$_['error_username1']      = '用户名必须介于3和20个字符之间！';
$_['error_mapping_validation']    = '选定的用户帐户必须不映射到其他品牌厂商或物流营销中心！';
$_['error_vendor_email']   		  = '电子邮件必须在 3 至 64 个字符之间!';
$_['error_account_validate']   	  = '您必须从现有的用户帐户中选择或填写用户名 <b>设置</b> 选项卡来创建新的品牌厂商配置文件！';
$_['error_vendor_firstname']      = '第一名必须介于 1 至 32个字符之间！';
$_['error_vendor_lastname']       = '姓必须在 1 至 32 个字符之间！';
$_['error_vendor_email']          = '电子邮件地址不显示是有效的！';
$_['error_vendor_paypal_email']   = '贝宝电子邮件地址似乎没有有效！';
$_['error_vendor_telephone']      = '电话必须在 3 至 32 个字符之间！';
$_['error_vendor_address_1']      = '地址3必须介于 1 至 128个字符之间！';
$_['error_vendor_city']           = '城市必须介于 2 至 128个字符！';
$_['error_vendor_postcode']       = '邮政编码必须是 2 至 10 之间的字符！';
$_['error_vendor_country']        = '请选择国家！';
$_['error_vendor_zone']           = '请选择 省/区!';
$_['error_required_data']  		  = '尚未输入所需数据。检查错误！';
$_['error_vendor']     	   		  = '警告：该品牌厂商不能被删除，因为它是目前分配到%s的商品！';
$_['error_password']   			  = 'Password must be between 4 and 20 characters!';
$_['error_confirm']    			  = '密码和密码确认不匹配！';
$_['error_link_account']    	  = '品牌厂商配置文件不能链接到管理员用户帐户！';
$_['error_link_account2']    	  = '品牌厂商的配置文件必须链接到唯一的用户帐户！';
?>
