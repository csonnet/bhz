<?php
// Heading
$_['heading_title']     = '下载';

// Text
$_['text_success']      = '成功：您已经修改了下载模块！';
$_['text_list']         = '下载列表';
$_['text_add']          = '添加下载';
$_['text_edit']         = '编辑下载';
$_['text_upload']       = '您的文件已经被成功上传！';

//mvds
$_['entry_vendor_name']= '品牌厂商名称：';
$_['column_vendor_name']= '品牌厂商名称';
//mvde

// Column
$_['column_name']       = '下载名称';
$_['column_date_added'] = '添加日期';
$_['column_action']     = '管理';

// Entry
$_['entry_name']        = '下载名称';
$_['entry_filename']    = '文件名称';
$_['entry_mask']        = '扰码';

// Help
$_['help_filename']     = '你可以上传通过上传按钮或使用FTP上传下载目录，输入以下内容。';
$_['help_mask']         = '建议该文件名和掩码是不同的，以阻止人们试图直接链接到您的下载。';

// Error
$_['error_permission']  = '警告：你还没有权限修改下载！';
$_['error_name']        = '下载名称必须是在3 至 64 字符之间！';
$_['error_upload']      = '请选择要上传的文件！';
$_['error_filename']    = '文件名称必须是 3 至 128 字符之间！';
$_['error_exists']      = '文件不存在！';
$_['error_mask']        = '扰码必须在3 至 128 个字符之间！';
$_['error_filetype']    = '无效的文件类型！';
$_['error_product']     = '警告：下载不能删除，因为有 %s 商品在使用！';