<?php
// Heading
$_['heading_title'] 		= '我的介绍';

// Text
$_['text_success']       	= '成功：您已经修改了品牌厂商！';
$_['text_success_profile']  = '成功：您已经更新了介绍！';
$_['text_default']       	= '默认';
$_['text_image_manager'] 	= '图片管理';
$_['text_disabled']      	= ' --- 禁用 --- ';
$_['text_browse']           = '浏览文件';
$_['text_clear']            = '清除图片';
$_['text_fixed_rate']    	= '固定费';
$_['text_percentage']    	= '百分比';
$_['text_pf']    		 	= '比分比 + 固定比例';
$_['text_fp']    		 	= '固定费 + 百分比率';
$_['text_month']    	 	= '月';
$_['text_year']    		 	= '年';
$_['text_subs_fee']    	 	= '认购金额 : $';
$_['text_products']    	 	= ' 商品';
$_['text_edit']          	= '编辑介绍';

// Entry
$_['entry_company']       = '公司名称';
$_['entry_contact_name']  = '联系名称';
$_['entry_firstname']     = '姓名';
$_['entry_lastname']      = '姓氏';
$_['entry_telephone']     = '电话';
$_['entry_fax']     	  = '传真';
$_['entry_email']         = 'Email';
$_['entry_paypal_email']  = 'PayPal Email';
$_['entry_company_id']    = '公司 ID';
$_['entry_iban']     	  = '银行账号';
$_['entry_bank_name']     = '银行名称';
$_['entry_bank_addr']     = '银行地址';
$_['entry_swift_bic']     = 'SWIFT/BIC';
$_['entry_tax_id']     	  = 'Tax ID';
$_['entry_description']   = '描述';
$_['entry_address_1']     = '地址';
$_['entry_address_2']     = '地址 2';
$_['entry_postcode']      = '邮编';
$_['entry_city']          = '区县';
$_['entry_country']       = '省市';
$_['entry_zone']          = '市区';
$_['entry_store_url']     = '商店地址';
$_['entry_image']         = '图片';
$_['entry_accept_paypal'] = '访问 PayPal';
$_['entry_accept_bank_transfer']    = '访问银行转账';
$_['entry_accept_cheques']    		= '访问支票';
$_['entry_sort_order']    = '排序';

//help
$_['help_email']         = '通知通过电子邮件发送的电子邮件地址时激活';
$_['help_paypal_email']  = '通过这个支付宝电子邮件帐户的商店管理员支付品牌厂商';
$_['help_image']         = '图片上传将用作公司标志';

//tab
$_['tab_general']    = '通用';
$_['tab_finance']    = '财务';
$_['tab_address']    = '地址';
$_['tab_payment']    = '支付';
$_['tab_shipping']   = '货运';

// Error
$_['error_exists']         = '警告：电子邮件地址已注册！';
$_['error_permission']     = '警告：您没有权限修改品牌厂商！';
$_['error_vendor_name']    = '品牌厂商名称必须介于 3 至 64 个字符之间！';
$_['error_vendor_email']   = '电子邮件必须在 3至 64 个字符之间！';
$_['error_vendor_firstname']      = '第一名必须介于 1 至 32 个字符之间！';
$_['error_vendor_lastname']       = '姓必须在 1 至 32 个字符之间！';
$_['error_vendor_email']          = '电子邮件地址不显示是有效的！';
$_['error_vendor_paypal_email']   = '贝宝电子邮件地址似乎没有有效！';
$_['error_vendor_telephone']      = '电话必须在 3 至 32 个字符之间！';
$_['error_vendor_address_1']      = '地址3必须介于 1 至 128 个字符之间！';
$_['error_vendor_city']           = '城市必须介于 2 至 128个字符！';
$_['error_vendor_postcode']       = '邮政编码必须是 2 至 10 之间的字符！';
$_['error_vendor_country']        = '请选择国家！';
$_['error_vendor_zone']           = '请选择省/区!';
$_['error_required_data']  		  = '尚未输入所需数据。检查错误！';
$_['error_vendor']     	   		  = '警告该品牌厂商不能被删除，因为它是目前分配到 %s 的商品！';
?>
