<?php
// Heading
$_['heading_title']      	= '快速状态修改';

// Text
$_['text_success']       	= '成功： 你修改了快速的状态修改!';
$_['text_nil']         		= 'Nil';
$_['text_enabled']          = '可用';
$_['text_disabled']         = '禁用';
$_['text_list']         	= '快速状态列表';

// Column
$_['column_username']		= '用户名';
$_['column_vendor_name']	= '品牌厂商名称';
$_['column_flname'] 		= '名称';
$_['column_company'] 		= '公司';
$_['column_telephone'] 		= '电话';
$_['column_email'] 			= 'Email';
$_['column_total'] 			= '全部商品';
$_['column_due_date'] 		= '到期日';
$_['column_status'] 		= '状态';

$_['help_status'] 			= '启用/禁用用户和所有商品的状态';

$_['button_update']  		= '更新';

// Error
$_['error_permission']  	= '警告 你没有权限修改状态权限！';
?>
