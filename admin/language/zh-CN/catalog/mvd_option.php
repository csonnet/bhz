<?php
// Heading
$_['heading_title']       = '选项';

// Text
$_['text_success']        = '成功： You have modified 选项!';
$_['text_list']           = '选项 List';
$_['text_add']            = '添加选项';
$_['text_edit']           = '编辑选项';
$_['text_choose']         = 'Choose';
$_['text_select']         = 'Select';
$_['text_radio']          = 'Radio';
$_['text_checkbox']       = 'Checkbox';
$_['text_image']          = '图片';
$_['text_input']          = 'Input';
$_['text_text']           = 'Text';
$_['text_textarea']       = 'Textarea';
$_['text_file']           = 'File';
$_['text_date']           = 'Date';
$_['text_datetime']       = 'Date &amp; Time';
$_['text_time']           = 'Time';

// Column
$_['column_name']         = '选项 Name';
$_['column_sort_order']   = '排序';
$_['column_action']       = '管理';
$_['column_vendor_name']  = '品牌厂商名称';

// Entry
$_['entry_name']          = '选项 Name';
$_['entry_type']          = '类型';
$_['entry_option_value']  = '选项 Value Name';
$_['entry_image']         = '图片';
$_['entry_sort_order']    = '排序';

//mvds
$_['entry_vendor_name']		= '品牌厂商名称：';
$_['error_warning']      	= '警告：您还没有权限修改选项！';
//mvde

// Error
$_['error_permission']    = '警告：您还没有权限修改！';
$_['error_name']          = '选项名称必须在 1 至 128 字符之间！';
$_['error_type']          = '警告：选项必填！';
$_['error_option_value']  = '选项值名称必须在 1 至 128 字符之间！';
$_['error_product']       = '警告：选项不能被删除，它被 %s 商品使用！';