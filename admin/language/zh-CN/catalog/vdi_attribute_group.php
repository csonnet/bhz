<?php
// Heading
$_['heading_title']     = '属性组';

// Text
$_['text_success']      = '成功： You have modified 属性组!';
$_['text_list']         = 'Attribute Group List';
$_['text_add']          = '添加属性组';
$_['text_edit']         = '编辑属性组';
$_['text_protected']    = '保护';

// Column
$_['column_name']       = '属性组名称';
$_['column_sort_order'] = '排序';
$_['column_action']     = '管理';

// Entry
$_['entry_name']        = '属性组名称';
$_['entry_sort_order']  = '排序';

// Error
$_['error_warning']     = '警告：您没有权限来修改受保护的属性组！';
$_['error_permission']  = '警告：您没有修改属性组的权限！';
$_['error_name']        = '属性组名称必须介于 3 至 64 个字符之间！';
$_['error_attribute']   = '警告：此属性组不能被删除，因为它目前被分配到 %s 的属性！';
$_['error_product']     = '警告：此属性组不能被删除，因为它目前被分配到 %s 的商品！';
$_['error_group']       = '警告：此属性组不能被删除，因为它是受保护的！';