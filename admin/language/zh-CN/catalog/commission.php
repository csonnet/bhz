<?php
// Heading
$_['heading_title']      = '佣金';

// Text
$_['text_success']       = '成功: 您已经修改了佣金！';
$_['text_fixed_rate']    = '固定费率';
$_['text_percentage']    = '百分比';
$_['text_pf']    		 = '百分比 + 固定费率';
$_['text_fp']    		 = '固定费 + 百分比率';
$_['text_month']    	 = '月';
$_['text_year']    		 = '年';
$_['text_subs_fee']    	 = '认购金额：';
$_['text_add']           = '添加佣金';
$_['text_edit']          = '编辑佣金';
$_['text_list']          = '佣金列表';

// Column
$_['column_name']			= '佣金名称';
$_['column_type']  			= '佣金类型';
$_['column_commission'] 	= '佣金比例';
$_['column_total_vendors'] 	= '所有品牌厂商';
$_['column_sort_order'] 	= '排序';
$_['column_action']     	= '管理';

// Entry
$_['entry_name']		= '佣金名称';
$_['entry_type']		= '佣金类型';
$_['entry_duration']	= '订阅时间';
$_['entry_subscription']= '认购金额';
$_['entry_commission']	= '佣金费率';
$_['entry_limit']  		= '商品限制';
$_['entry_sort_order']  = '排序';

//help
$_['help_duration']		= '此项目适用于月/年订阅';
$_['help_subscription']	= '注册时，订阅金额需要支付给商店管理';
$_['help_commission']	= '<b>百分比 (P)%:</b> 1.00-100 , <b>固定 (F):</b> 1.00 , <b>(P)% + (F) :</b> 10:1 , <b>(F) + (P)% :</b> 1:10';

// Error
$_['error_permission']  			= '警告: 您没有权限修改佣金!';
$_['error_delete']     				= '警告: 此佣金不能被删除，它被 %s 品牌厂商使用！';
$_['error_required_data']  			= '警告: 请填写需要填写的数据！';
$_['error_commission_name']  		= '类型名称必须在3 至 64 个字符！';
$_['error_commission']   			= '佣金必须是数字！';

?>