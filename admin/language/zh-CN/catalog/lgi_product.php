<?php
// Heading
$_['heading_title']          = '商品';

// Text
$_['text_success']           = '成功：复制了商品!';
$_['text_list']              = '商品列表';
$_['text_add']               = '添加商品';
$_['text_edit']              = '编辑商品';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = '默认';
$_['text_option']            = '选项';
$_['text_option_value']      = '选项值';
$_['text_percent']           = '百分比';
$_['text_amount']            = '固定金额';

//mvds
$_['column_vendor']                 = '品牌厂商名称';
$_['column_sku']                    = '条形码';
$_['entry_vendor_country_origin']     = '国家归属';
$_['entry_vendor_product_cost']       = '商品成本';
$_['entry_vendor_shipping_method']        = '包含货运';
$_['entry_vendor_preferred_shipping_method']= '优先货运';
$_['entry_vendor_shipping_cost']      = '运费成本';
$_['entry_vendor_total']            = '总计';
$_['entry_vendor_company']            = '公司名称';
$_['entry_vendor_contact_name']       = '联系人姓名';
$_['entry_vendor_telephone']          = '电话';
$_['entry_vendor_fax']              = '传真';
$_['entry_vendor_email']              = 'Email';
$_['entry_vendor_paypal_email']       = 'Paypal Email';
$_['entry_vendor_description']        = '描述';
$_['entry_vendor_address']            = '地址';
$_['entry_vendor_country_zone']           = '省市';
$_['entry_vendor_store_url']          = '商店网址';
$_['entry_vendor_product_url']        = '商品网址';
$_['entry_vendor_name']           = '品牌厂商名称';
$_['entry_vendor_wholesale']          = '批发';
$_['entry_shipping_courier']          = '货运速递';
$_['entry_shipping_cost']             = '固定费率';
$_['entry_shipping_geozone']          = '区域群组';
$_['entry_shipping_rate']             = '货运费率';
$_['tab_vendor']                    = '品牌厂商';
$_['tab_shipping']                  = '货运';
$_['button_add_shipping']               = '添加货运';
$_['heading_pro2ven_title']         = '品牌厂商商品'; 
$_['txt_pending_approval']          = '待审批'; 
$_['error_max_warning']               = '警告：您已经达到最大数量商品！';
$_['help_vendor_country_origin']      = '货物已从产地发货';
$_['help_vendor_shipping_method']         = '托运方式包括托运人';
$_['help_vendor_preferred_shipping_method'] = '您首选的装运方法';
$_['help_vendor_total']             = '商品成本 + 货运成本<br />商品成本用于利润报告';
//mvde

// Column
$_['column_name']            = '商品名称';
$_['column_model']           = '型号';
$_['column_image']           = '图片';
$_['column_price']           = '价格';
$_['column_quantity']        = '数量';
$_['column_status']          = '状态';
$_['column_action']          = '管理';

// Entry
$_['entry_name']             = '商品名称';
$_['entry_description']      = '描述';
$_['entry_meta_title']       = 'Meta标题';
$_['entry_meta_keyword']   = 'Meta关键字';
$_['entry_meta_description'] = 'Meta描述';
$_['entry_keyword']          = 'SEO关键字';
$_['entry_model']            = '型号';
$_['entry_sku']              = '条形码';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = '位置';
$_['entry_shipping']         = '需要货运';
$_['entry_manufacturer']     = '品牌';
$_['entry_store']            = '商店';
$_['entry_date_available']   = '上架日期';
$_['entry_quantity']         = '数量';
$_['entry_minimum']          = '最小数量';
$_['entry_stock_status']     = '缺货状态';
$_['entry_price']            = '价格';
$_['entry_tax_class']        = '税类型';
$_['entry_points']           = '积分';
$_['entry_option_points']    = '积分';
$_['entry_subtract']         = '库存减少';
$_['entry_weight_class']     = '重量单位';
$_['entry_weight']           = '重量';
$_['entry_dimension']        = '体积 (L x W x H)';
$_['entry_length_class']     = '长度单位';
$_['entry_length']           = '长';
$_['entry_width']            = '宽';
$_['entry_height']           = '高';
$_['entry_image']            = '图片';
$_['entry_customer_group']   = '客户群';
$_['entry_date_start']       = '开始日期';
$_['entry_date_end']         = '结束日期';
$_['entry_priority']         = '优先';
$_['entry_attribute']        = '属性';
$_['entry_attribute_group']  = '属性组';
$_['entry_text']             = '文本';
$_['entry_option']           = '选项';
$_['entry_option_value']     = '选项值';
$_['entry_required']         = '必填';
$_['entry_status']           = '状态';
$_['entry_sort_order']       = '排序';
$_['entry_category']         = '分类';
$_['entry_filter']           = '筛选';
$_['entry_download']         = '下载';
$_['entry_related']          = '相关商品';
$_['entry_tag']            = '商品标签';
$_['entry_reward']           = '积分';
$_['entry_layout']           = '布局覆盖';
$_['entry_recurring']        = '简介';

// Help
$_['help_keyword']           = '不要使用空格，而不是替换空格-确保关键字是全局唯一的。';
$_['help_sku']               = '库存单位';
$_['help_upc']               = '通用商品代码';
$_['help_ean']               = '欧洲物品编号';
$_['help_jan']               = '日语物品编号';
$_['help_isbn']              = '国际标准图书编号';
$_['help_mpn']               = '制造商零件编号';
$_['help_manufacturer']      = '(自动完成)';
$_['help_minimum']           = '强制最小订购量';
$_['help_stock_status']      = '当商品不在库存时的状态';
$_['help_points']            = '购买该项目所需的积分。如果你不希望这个商品用积分请填写0。';
$_['help_category']          = '(自动完成)';
$_['help_filter']            = '(自动完成)';
$_['help_download']          = '(自动完成)';
$_['help_related']           = '(自动完成)';
$_['help_tag']             = '逗号分隔';

// Error
$_['error_warning']          = '警告：请仔细检查错误！';
$_['error_permission']       = '警告：您还没有权限修改商品！';
$_['error_name']             = '商品名称必须在 3 至 255 字符之间！';
$_['error_meta_title']       = 'Meta 标题必须在3 至 255 字符之间！';
$_['error_model']            = '商品型号必须在 1 至 64 字符之间！';