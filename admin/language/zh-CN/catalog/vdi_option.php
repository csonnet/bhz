<?php
// Heading
$_['heading_title']       = '选项';

// Text
$_['text_success']        = '成功：您已经修改了选项！';
$_['text_list']           = '选项列表';
$_['text_add']            = '添加选项';
$_['text_edit']           = '编辑选项';
$_['text_choose']         = '已选';
$_['text_select']         = '选择';
$_['text_radio']          = '单选';
$_['text_checkbox']       = '多选';
$_['text_image']          = '图片';
$_['text_input']          = '输入';
$_['text_text']           = '文本';
$_['text_textarea']       = '文本区';
$_['text_file']           = '文件';
$_['text_date']           = '日期';
$_['text_datetime']       = '日期 &amp; 时间';
$_['text_time']           = '时间';
$_['text_protected']      = '保护';

// Column
$_['column_name']         = '选项名称';
$_['column_sort_order']   = '排序';
$_['column_action']       = '管理';

// Entry
$_['entry_name']          = '选项名称';
$_['entry_type']          = '类型';
$_['entry_option_value']  = '选项值名称';
$_['entry_image']         = '图片';
$_['entry_sort_order']    = '排序';

// Error
$_['error_warning']       = '警告：您没有权限修改保护选项！';
$_['error_permission']    = '警告：您没有权限修改选项！';
$_['error_name']          = '选项名称必须在 1 至 128 字符之间！';
$_['error_type']          = '警告 Option Values required!';
$_['error_option_value']  = '选项值名称必须在 1 至 128 字符之间！';
$_['error_product']       = '警告：此选项不能被删除，因为它目前被分配到 %s 的商品！';
$_['error_option']        = '警告：选项不能被删除，已经被保护！';