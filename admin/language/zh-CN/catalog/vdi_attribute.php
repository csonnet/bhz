<?php
// Heading
$_['heading_title']          = '属性';

// Text
$_['text_success']           = '成功：您已经修改了属性！';
$_['text_list']              = '属性列表';
$_['text_add']               = '添加属性';
$_['text_edit']              = '修改属性';
$_['text_protected']         = '保护';

// Column
$_['column_name']            = '属性名称';
$_['column_attribute_group'] = '属性组';
$_['column_sort_order']      = '排序';
$_['column_action']          = '管理';

// Entry
$_['entry_name']            = '属性名称';
$_['entry_attribute_group'] = '属性组';
$_['entry_sort_order']      = '排序';

// Error
$_['error_warning']         = '警告：您没有权限修改受保护的属性！';
$_['error_permission']      = '警告：您没有修改属性的权限！';
$_['error_name']            = '属性名必须介于3和64个字符之间！';
$_['error_product']         = '警告：此属性不能被删除，因为它目前被分配到 %s 的商品！';
$_['error_attribute']       = '警告：此属性不能被删除，因为它是受保护的！';