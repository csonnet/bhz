<?php
// Heading
$_['heading_title']      = '快递';

// Text
$_['text_success']       = '成功: 您已经修改了当前快递！';
$_['text_default']       = '默认';
$_['text_image_manager'] = '图片管理';
$_['text_disabled']      = ' --- 不可用 --- ';
$_['text_browse']        = '浏览文件';
$_['text_clear']         = '清理图片';
$_['text_add']           = '添加快递';
$_['text_edit']          = '编辑快递';
$_['text_list']          = '快递列表';

// Column
$_['column_image']       	= '图片';
$_['column_courier_name']  	= '快递名称';
$_['column_description']  	= '估算交货时间';
$_['column_total_products'] = '所有商品';
$_['column_sort_order']  	= '排序';
$_['column_action']      	= '管理';

// Entry
$_['entry_courier_name']   = '快递名称';
$_['entry_courier_image']  = '快递 Image';
$_['entry_description']    = '估算交货时间';
$_['entry_sort_order']     = '排序';

// Error
$_['error_permission']   	= '警告: 您没有权限修改快递!';
$_['error_courier_name']   	= '快递名称必须在 3 至 64 个字符之间！';
$_['error_description']   	= '快递描述必须在 3 至 256 个字符之间！';
$_['error_required_data'] 	= '需要的数据填写错误，请检查错误！';
$_['error_courier']     	= '警告：您不能删除快递，有 %s 商品正在使用！';
?>
