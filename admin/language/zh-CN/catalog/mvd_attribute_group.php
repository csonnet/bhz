<?php
// Heading
$_['heading_title']     = '属性群组';

// Text
$_['text_success']      = '成功：您已经成功修改属性群组！';
$_['text_list']         = '属性群组列表';
$_['text_add']          = '添加属性群组';
$_['text_edit']         = '编辑属性群组';

// Column
$_['column_name']       = '属性群组名称';
$_['column_sort_order'] = '排序';
$_['column_action']     = '管理';
$_['column_vendor_name']= '品牌厂商名称';

// Entry
$_['entry_name']        = '属性群组名称';
$_['entry_sort_order']  = '排序';

//mvds
$_['entry_vendor_name']	= '品牌厂商名称：';
$_['error_warning']     = '警告：您还没有权限修改属性群组！';
//mvde

// Error
$_['error_permission']  = '警告：您还没有权限修改属性群组！';
$_['error_name']        = '属性群组名称必须在 3 至 64 个字符之间！';
$_['error_attribute']   = '警告：属性群组不能被删除，当前群组已经被 %s 属性使用！';
$_['error_product']     = '警告：属性群组不能被删除，有商品 %s 在使用！';