<?php
// Heading
$_['heading_title']          = '分类';

// Text
$_['text_success']           = '成功：您已经修改了分类！';
$_['text_list']              = '分类列表';
$_['text_add']               = '添加分类';
$_['text_edit']              = '编辑分类';
$_['text_default']           = '默认';

// Column
$_['column_name']            = '分类名称';
$_['column_sort_order']      = '排序';
$_['column_action']          = '管理';

// Entry
$_['entry_name']             = '分类名称';
$_['entry_description']      = '描述';
$_['entry_meta_title'] 	     = 'Meta 标题';
$_['entry_meta_keyword'] 	 = 'Meta 关键字';
$_['entry_meta_description'] = 'Meta 描述';
$_['entry_keyword']          = 'SEO 关键字';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = '筛选';
$_['entry_store']            = '商店';
$_['entry_image']            = '图片';
$_['entry_top']              = '顶部';
$_['entry_column']           = '列';
$_['entry_sort_order']       = '排序';
$_['entry_status']           = '状态';
$_['entry_layout']           = 'Layout Override';

// Help
$_['help_filter']            = '(自动完成)';
$_['help_keyword']           = '不要使用空格，而不是替换空格-确保关键字是全局唯一的。';
$_['help_top']               = '显示在顶部菜单栏。只为顶级父类工作。';
$_['help_column']            = '用于底部3类的列数。只为顶级父类工作。';

// Error
$_['error_warning']          = '警告：请仔细检查错误！';
$_['error_permission']       = '警告：您还没有修改分类权限！';
$_['error_name']             = '分类名称必须在 2 至 32 个字符之间！';
$_['error_meta_title']       = 'Meta 标题必须在 2 至 255 字符之间！';