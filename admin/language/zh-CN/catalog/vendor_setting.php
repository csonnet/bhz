<?php
// Heading
$_['heading_title']    = '全局品牌厂商管理';

// Text
$_['text_success']     = '成功: 您已经修改了品牌厂商管理！';

// Entry
$_['entry_checkout_order_status'] 	= '结账订单状态';
$_['entry_history_order_status']    = '历史订单状态';
$_['entry_order_status_availability']  = '订单状态可用';
$_['entry_multivendor_order_status']   = '订单状态通知';
$_['entry_new_order_message']   	= '新订单信息';
$_['entry_history_order_message']   = '历史订单信息';
$_['entry_product_notification']   	= '商品状态通知';
$_['entry_order_id']  				= '显示订单ID';
$_['entry_order_status']    		= '显示订单状态';
$_['entry_payment_method']    		= '显示支付方式';
$_['entry_shipping_address']    	= '显示货运地址';
$_['entry_cust_email']    			= '显示客户的Email';
$_['entry_cust_telephone']    		= '显示客户电话';
$_['entry_vendor_address']    		= '显示品牌厂商地址';
$_['entry_vendor_email']    		= '显示品牌厂商Email';
$_['entry_vendor_telephone']    	= '显示品牌厂商电话';
$_['entry_status']    				= '状态';
$_['entry_desgin_tab']    			= '显示设计表单';
$_['entry_reward_points']			= '显示奖励积分';
$_['entry_vendor_tab']				= '显示品牌厂商表单';
$_['entry_menu_bar']    			= '允许类别菜单栏选项';
$_['entry_vendor_product_approval'] = '商品审批';
$_['entry_vendor_product_edit_approval'] = '商品编辑审批';
$_['entry_bulk_products_activation'] = '批量商品激活';
$_['entry_category_menu'] 			= '显示分类目录';
$_['entry_vendor_invoice_address'] 	= '品牌厂商 / 商店管理地址';
$_['entry_order_detail'] 			= '显示订单明细';
$_['entry_payment_detail']			= '显示支付明细';
$_['entry_shipping_detail']			= '显示货运明细';
$_['entry_product']    				= '显示商品';
$_['entry_order_history']			= '显示订单历史';
$_['entry_order_history_update']	= '允许订单状态变更';
$_['entry_allow_notification']		= '允许订单状态通知';
$_['entry_multi_store_activated']   = '激活多店的特征';
$_['entry_multi_payment_gateway']   = '支付网关';
$_['entry_sign_up'] 				= '品牌厂商注册';
$_['entry_policy']					= '品牌厂商协议';
$_['entry_vendor_approval']			= '自动审批';
$_['entry_commission']				= '佣金';
$_['entry_product_limit']			= '商品限制';
$_['entry_category']   				= '分类权限';
$_['entry_store']      				= '商店权限';
$_['entry_signup_paypal_email']		= 'PayPal Email';
$_['entry_paypal_sandbox']			= 'Sandbox Mode';
$_['entry_show_plan']				= '品牌厂商注册佣金计划';
$_['entry_bank_order_status']		= '订单状态';
$_['entry_bank_instruction']		= '银行转账介绍';
$_['entry_signup_payment_method']	= '默认的支付方式';
$_['entry_stock_threshold']			= '库存阀值';

//help
$_['help_new_order_message']   		= '请键入您的新订单消息触发到品牌厂商';
$_['help_history_order_message']   	= '请键入您的历史订单信息，触发品牌厂商';
$_['help_checkout_order_status'] 	= '启用系统触发电子邮件品牌厂商当顾客结账';
$_['help_history_order_status']     = '启用系统触发电子邮件品牌厂商当管理员更新订单状态的历史';
$_['help_order_status_availability']= '选择品牌厂商的订单状态在销售订单';
$_['help_multivendor_order_status'] = '启用系统触发邮件给客户时，所有的品牌厂商满足相同的订单状态';
$_['help_product_notification']   	= '允许系统触发的电子邮件时，新商品添加/编辑，由品牌厂商或通过商店管理';
$_['help_order_id']  				= '启用订单ID电子邮件给品牌厂商';
$_['help_order_status']    		    = '启用采购订单状态在品牌厂商的电子邮件显示';
$_['help_payment_method']    		= '启用支付品牌厂商的电子邮件显示的方法';
$_['help_shipping_address']    	    = '启用客户送货地址显示在品牌厂商的电子邮件';
$_['help_cust_email']    			= '启用客户邮件在电子邮件给品牌厂商';
$_['help_cust_telephone']    		= '启用客户电话品牌厂商的电子邮件显示';
$_['help_vendor_address']    		= '启用品牌厂商地址显示在电子邮件';
$_['help_vendor_email']    			= '启用邮件在电子邮件给品牌厂商';
$_['help_vendor_telephone']    		= '启用品牌厂商电话在邮件的显示';
$_['help_stock_threshold'] 			= '显示通知品牌厂商的仪表板时，商品数量达到阈值';

$_['help_desgin_tab']    			= '使商品设计标签，分类和信息页面';
$_['help_reward_points']			= '使奖励积分的商品页面';
$_['help_vendor_tab']				  = '使品牌厂商标签品牌厂商帐户商品页面';
$_['help_menu_bar']    				= '启用此选项将允许品牌厂商设置类别在商店前面的顶部菜单栏';
$_['help_vendor_product_approval'] 	= '商品需要商店管理员批准的提交';
$_['help_vendor_product_edit_approval'] = '编辑商品需要商店管理员批准';
$_['help_bulk_products_activation'] = '自动启用/禁用品牌厂商商品在店门前，当用户帐户状态改变';
$_['help_category_menu'] 			     = '显示或隐藏菜单类品牌厂商的帐户登录';

$_['help_vendor_invoice_address'] 	= '选择 <b>是</b> 显示品牌厂商地址， <b>否</b> 显示管理地址在发票上';
$_['help_order_detail'] 			= '品牌厂商可以查看订单明细';
$_['help_payment_detail']			= '品牌厂商可以查看详细的付款';
$_['help_shipping_detail']			= '品牌厂商可以查看详细的运输';
$_['help_product']    				= '品牌厂商可以查看商品';
$_['help_order_history']			= '品牌厂商可以查看历史订单';
$_['help_order_history_update']		= '品牌厂商可以更新订单状态';
$_['help_allow_notification']		= '品牌厂商发出通知要求所有品牌厂商没有完全满足预先设定的订单状态';
$_['help_multi_store_activated']   	= '客户直接付款给多店中的品牌厂商，通过 <b>贝宝, 支票和银行转账</b>';
$_['help_multi_payment_gateway']   	= '禁用默认网关支付比其他主要商店 <b>贝宝，货到付款，银行转账和支付</b>';

$_['help_sign_up'] 					= '供货商在前台注册';
$_['help_policy']					  = '默认的注册协议';
$_['help_vendor_approval']	= '允许自动审批注册的品牌厂商';
$_['help_commission']				= '默认的佣金费率当关闭佣金计划';
$_['help_product_limit']			= '默认最大商品数量给注册品牌厂商的';
$_['help_category']   				= '默认分类给品牌厂商的';
$_['help_store']      				= '默认商店给注册品牌厂商';
$_['help_signup_paypal_email']		= '贝宝邮件收取注册费';
$_['help_show_plan']				= '显示佣金在前台注册页面';
$_['help_paypal']					= '用在线测试（沙盒）支付服务';

//text
$_['text_yes']        			= '是';
$_['text_no']         			= '否';
$_['text_order_id']         	= '订单 ID';
$_['text_order_status']         = '订单状态';
$_['text_date_ordered']  		= '订单日期';
$_['text_payment_method']  		= '支付方式';
$_['text_email']         		= 'Email';
$_['text_telephone']       		= '电话';
$_['text_payment_method']  		= '支付方式';
$_['text_customer_contact']     = '客户联系';
$_['text_vendor_address']     	= '品牌厂商地址';
$_['text_shipping_address']     = '货运地址';
$_['text_fixed_rate']    	    = '固定金额';
$_['text_percentage']    	    = '百分比';
$_['text_pf']    		 		= '百分比 + 固定费率';
$_['text_fp']    		 		= '固定费 + 百分比费率';
$_['text_disabled']    	    	= '禁用';
$_['text_none']    	    		= '--- 无 ---';
$_['text_products']    	 		= ' 商品';
$_['text_edit']          		= '编辑全局品牌厂商设置';
$_['text_new_order_message']    = '你有新订单。';
$_['text_history_order_message']= '订单状态已经被修改用货运跟踪码。';
$_['text_paypal']        		= 'PayPal';
$_['text_bank']        			= '银行转账';


//tab
$_['tab_mail_setting']  		= 'E-mail模板';
$_['tab_catalog']  			= '总体设置';
$_['tab_sales']  				= '销售订单';
$_['tab_store']  				= '多商店';
$_['tab_notification']  		= '通知';
$_['tab_payment']  				= '付款';
$_['tab_signup']  				= '注册';

// Error
$_['error_permission'] 			= '警告: 你没有权限修改全局设置';
$_['error_code_new_message']   	= '新订单信息需要填写';
$_['error_code_history_message']= '历史订单信息需要填写';
?>
