<?php
// Heading
$_['heading_title']    = '天翼189短信接口';

// Text
$_['text_sms']         = '短信接口';
$_['text_success']     = '成功: 已修改天翼1889短信接口！';
$_['text_edit']        = '编辑天翼189短信接口';

// Entry
$_['app_id']      = 'App ID';
$_['app_secret']     = 'App Secret';
$_['template_id']    = 'Template ID';
$_['template_id_co']    = 'Template ID CO';
$_['template_id_sp']    = 'Template ID SP';
$_['template_id_sv']    = 'Template ID SV';
$_['template_id_tc']    = 'Template ID TC';
$_['template_id_dc']    = 'Template ID DC';
$_['template_id_reg']    = '新用户注册优惠券提醒ID';
$_['template_id_ex']    = '优惠券快到期提醒ID';
$_['entry_status']    = '状态';

// Error
$_['error_permission']  = '警告: 无权修改天翼189短信接口！';
$_['error_appid']    = 'App ID 必填！';
$_['error_appsecret']  = 'App Secret必填！';
$_['error_templateid']   = 'Template ID必填！';
$_['error_templateid_co']   = 'Template ID必填！';
$_['error_templateid_sp']   = 'Template ID必填！';
$_['error_templateid_sv']   = 'Template ID必填！';
$_['error_templateid_tc']   = 'Template ID必填！';
$_['error_templateid_dc']   = 'Template ID必填！';
$_['error_templateid_reg']   = '不能为空！';
$_['error_templateid_ex']   = '不能为空！';