<?php

// Heading
$_['heading_title']    = '免费拿样';

// Text
$_['text_total']       = '订单总计';
$_['text_success']     = '成功: 已修改免费拿样！';
$_['text_edit']        = '编辑免费拿样';

// Entry
$_['entry_status']     = '状态';
$_['entry_sort_order'] = '排序';

// Error
$_['error_permission'] = '警告: 无权限修改免费拿样！';