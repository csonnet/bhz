<?php
// Heading
$_['heading_title']     		= '销售报告';

// Text
$_['text_year']         		= '本年';
$_['text_month']        		= '本月';
$_['text_week']         		= '本周';
$_['text_day']          		= '今日';
$_['text_all_status']   		= '所有状态';
$_['text_all_vendors']  		= '所有品牌厂商';
$_['text_yes']          		= '是';
$_['text_no']          			= '否';
$_['text_gross_incomes']		= '商店总收入';
$_['text_commission']    		= '商店佣金';
$_['text_shipping']    			= '货运';
$_['text_coupon']    			= '优惠券';
$_['text_amount_pay_vendor']	= '支付给品牌厂商金额';
$_['text_vendor_revenue']		= '总收入';
$_['text_wait']             	= '请等待！';
$_['text_vendor_earning'] 		= '品牌厂商余额';
$_['text_payment_history'] 		= '最新10向品牌厂商付款的历史';
$_['text_vendor_payment_history'] 	= '最近收到的10从商店';
$_['text_success']              = '成功： 您已经删除付款历史！';
$_['text_paypal_standard']      = '标准贝宝支付';
$_['text_subscription']    	 	= '订阅';
$_['text_list']    	 			    = '销售报告';
$_['title_payment_type']        = '付款类型经理';
$_['title_gross_revenue']        = '总收入 + 货运 = ';

// Column
$_['column_date_added'] 		= '添加日期';
$_['column_order_id']   		= '订单ID';
$_['column_product_name']   	= '商品名称';
$_['column_unit_price']   		= '单价';
$_['column_quantity']       	= '数量';
$_['column_commission']      	= '佣金';
$_['column_amount']      		= '品牌厂商金额';
$_['column_total']      		= '总计';
$_['column_transaction_status'] = '状态';
$_['column_paid_status']      	= '已付款';
$_['column_vendor_name'] 		= '品牌厂商名称';
$_['column_payment_amount'] 	= '支付金额';
$_['column_payment_date'] 		= '支付日期';
$_['column_payment_type'] 		= '支付类型';
$_['column_order_product'] 		= '商品订单 [订单 ID - 商品名称]';
$_['button_Paypal'] 			= '品牌厂商支付';
$_['button_addPayment'] 		= '添加付款记录';

// Entry
$_['entry_date_start']   = '开始日期 :';
$_['entry_date_end']     = '结束日期 :';
$_['entry_group']        = '排序 :';
$_['entry_order_status'] = '订单状态 :';
$_['entry_status']       = '已付款 :';
?>