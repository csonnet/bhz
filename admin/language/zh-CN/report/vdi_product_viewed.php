<?php
// Heading
$_['heading_title']    = '商品浏览报告';

// Text
$_['text_list']        = '商品浏览列表';
$_['text_success']     = '成功：您已重置该商品查看报告！';

// Column
$_['column_name']      = '商品名称';
$_['column_model']     = '型号';
$_['column_viewed']    = '已浏览';
$_['column_percent']   = '百分比';

// Error
$_['error_permission'] = '警告：您没有权限重置商品查看报告！';