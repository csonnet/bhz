<?php
// Heading
$_['heading_title']					= '多品牌厂商/多托运人支付贝宝标准（多店））';

// Text
$_['text_payment']					= '支付';
$_['text_success']					= '成功：您已经修改了多品牌厂商/多托运人账户明细！';
$_['text_edit']                     = '编辑贝宝标准支付';
$_['text_pp_standard']				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="贝宝专业接口" title="贝宝网站付款专业" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= '授权';
$_['text_sale']						= '销售';

// Entry
$_['entry_email']					= 'E-Mail';
$_['entry_test']					= 'Sandbox Mode';
$_['entry_transaction']	  = 'Transaction Method';
$_['entry_debug']					= 'Debug Mode';
$_['entry_total']					= '总计';
$_['entry_canceled_reversal_status'] = '取消撤销';
$_['entry_completed_status']		= '完成';
$_['entry_denied_status']			= '禁止';
$_['entry_expired_status']			= '过期';
$_['entry_failed_status']			= '失败';
$_['entry_pending_status']			= '待处理';
$_['entry_processed_status']		= '已处理';
$_['entry_refunded_status']			= '已退款';
$_['entry_reversed_status']			= '回退';
$_['entry_voided_status']			= '已作废';
$_['entry_geo_zone']				= '区域群组';
$_['entry_status']					= '状态';
$_['entry_sort_order']				= '排序';

// Tab
$_['tab_general']					= '总体';
$_['tab_status']					= '订单状态';

// Help
$_['help_test']						= '使用活的或测试的网关服务器处理事务？';
$_['help_debug']					= '将附加信息记录到系统日志中';
$_['help_total']					= '结帐总订单必须达到在此付款方式变得活跃';

// Error
$_['error_permission']				= '警告：您没有权限修改支付多品牌厂商/多托运人！';
$_['error_email']					= 'E-Mail 必填！';