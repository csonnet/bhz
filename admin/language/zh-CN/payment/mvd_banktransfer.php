<?php
// Heading
$_['heading_title']					= '多品牌厂商/多托运人银行转账(多店)';

// Text
$_['text_payment']					= '支付';
$_['text_success']					= '成功：您已经修改了多品牌厂商/多托运人银行转账明细！';
$_['text_edit']             = '编辑银行转账';

// Entry
$_['entry_total']					  = '总计';
$_['entry_order_status']		= '订单状态';
$_['entry_geo_zone']				= '区域群组';
$_['entry_status']					= '状态';
$_['entry_sort_order']		  = '排序';

// Help
$_['help_total']					= '结帐总订单必须达到在此付款方式激活。';

// Error
$_['error_permission']				= '警告：您没有权限修改支付多品牌厂商/下降托运人银行转帐！';
