<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">

                  <div class="form-group">
                    <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                    <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
                  </div>
                      <div class="form-group">
                    <label class="control-label" for="input-recommended_code">推荐码</label>
                    <input type="text" name="filter_recommended_code" value="<?php echo $filter_recommended_code; ?>" placeholder="推荐码" id="input-recommended_code" class="form-control" />
                  </div>
            </div>
            <div class="col-sm-4">
              <!-- <div class="form-group">
                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                  <option value="0" >二维码绑定</option>
                  <option value="1">购买</option>
                </select>
              </div> -->
              <div class="form-group">
                <label class="control-label" for="input-date-start">购买开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-4">

                  <div class="form-group">
                    <label class="control-label" for="input-date-modified">上架开始时间</label>
                    <div class="input-group date">
                      <input type="text" name="filter_date_end" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>
                  <button type="button" id="Month-buy" class="btn btn-primary pull-right"><i class="fa fa-download"></i>导出货架信息</button>
                <button type="button" id="button-filter" class="btn btn-primary pull-left" style="margin-left: 400px;" ><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>

            </div>
          </div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </td>
                  <td class="text-left">状态</td>
                  <td class="text-left">货架ID</td>
                  <td class="text-left">会员</td>
                  <td class="text-left">业务员</td>
                  <td class="text-left">超市名</td>
                  <td class="text-left">品类</td>
                  <td class="text-left">货架名称</td>
                  <td class="text-left">货架规格</td>
                  <td class="text-left">商品数目</td>
                  <td class="text-left">进货金额</td>
                  <td class="text-left">购买日期</td>
                  <td class="text-left">上架时间</td>
                  <td class="text-left">二维码绑定时间</td>
                  <td class="text-left"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($orders) { ?>
                <?php foreach ($orders as $order) { ?>
                <tr>
                  <td class="text-center">
                    <?php if (in_array($order['shelf_order_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['shelf_order_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['shelf_order_id']; ?>" />
                    <?php } ?>
                  </td>
                    <td class="text-left" <?php if($order['qr_code']=="已绑定"){echo "style='color:red;'";}?>><?php echo $order['qr_code'];?></td>
                    <td class="text-left"><?php echo $order['shelf_id']; ?></td>
                    <td class="text-left"><?php echo $order['fullname']; ?></td>
                    <td class="text-left"><?php echo $order['user_id']; ?></td>
                    <td class="text-left"><?php echo $order['company_name']; ?></td>
                    <td class="text-left"><?php echo $order['name']; ?></td>
                    <td class="text-left"><?php echo $order['shelf_name']; ?></td>
                    <td class="text-left"><?php echo $order['shelf_type']; ?></td>
                    <td class="text-left"><?php echo $order['quantity'];?></td>
                    <td class="text-left"><?php echo $order['price']; ?></td>
                    <td class="text-left"><?php echo $order['buytime']; ?></td>
                    <td class="text-left"><?php echo $order['date_added']; ?></td>
                    <td class="text-left"><?php echo $order['date_map']; ?></td>

                    <td class="text-left">
                    <a href="<?php echo $view.'&order_id='.$order['order_id']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                  <!--   <button type="button" value="<?php echo $order['shelf_order_id']; ?>" id="button-delete<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button> -->
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = 'index.php?route=shelf/goodsshelf&token=<?php echo $token; ?>';

  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }


    location = url;
});




//--></script>
  <script type="text/javascript"><!--


//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months",
  minViewMode: "months",
  pickTime: false
});

$('#button-logcenter-bill').on('click', function(e) {
  var logcenter_bill_month = $('#logcenter_bill_month').val();
  if(!logcenter_bill_month || isNaN(Date.parse(logcenter_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/order/genLogcenterBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+logcenter_bill_month;
});

$('#button-export-month-order').on('click', function(e) {
  var logcenter_bill_month = $('#logcenter_bill_month').val();
  if(!logcenter_bill_month || isNaN(Date.parse(logcenter_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/order/monthlyExport') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+logcenter_bill_month;
});
//导出货架信息
$('#Month-buy').click(function(){
    var url = 'index.php?route=shelf/goodsshelf/shelfinfoexcel&token=<?php echo $token; ?>';
     location = url;
});
//--></script></div>
<?php echo $footer; ?>
