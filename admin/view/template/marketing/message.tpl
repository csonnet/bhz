<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">
	
    <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
      		<h1><?php echo $heading_title; ?></h1>
      		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
      		</ul>
		</div>
  	</div>
    <!--顶部-->
    
    <!--内容-->
	<div class="container-fluid">
    	
        <!--消息区-->
    	<?php if ($error_warning) { ?>
    	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" form="form-backup" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
        <!--消息区-->
        
        <div class="panel panel-default">
        
			<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-pencil"></i> 消息管理</h3>
   			</div>
            
            <div class="panel-body">
            	
                <ul class="nav nav-tabs">
                	<li class="active"><a href="#tab-diy" data-toggle="tab">自定义通知</a></li>
            		<!--<li><a href="#tab-order" data-toggle="tab">销售通知</a></li>
            		<li><a href="#tab-show" data-toggle="tab">活动通知</a></li> 	-->	
 				</ul>
                
                <div class="tab-content">
                	
                    <!--自定义通知-->
					<div class="tab-pane active" id="tab-diy">
                    	
                        <form class="form-horizontal send-form">
                        
                        <div class="form-group required">	
							<label class="col-sm-2 control-label">标题</label>
                			<div class="col-sm-10">
                    			<input name="title" class="form-control" placeholder="标题"></textarea>
                        	</div>
                    	</div>
                        
                    	<div class="form-group required">	
							<label class="col-sm-2 control-label">内容</label>
                			<div class="col-sm-10">
                    			<textarea name="content" class="form-control" style="margin-bottom:5px;" rows="2" placeholder="内容"></textarea>
                        	</div>
                    	</div> 
                        
                        <div class="form-group">	
							<label class="col-sm-2 control-label">发送给</label>
                            <div class="col-sm-10" style="padding-top:9px;">
                            	<input class="send-check all-check" name="all_send" type="checkbox" />全部用户
                                <input class="send-check all-admin" name="all_admin" type="checkbox" />内部用户
                            </div>
                      	</div>
                        
                        <div class="form-group send-div">	
							<label class="col-sm-2 control-label">发送业务员</label>
                            <div class="col-sm-10" style="padding-top:9px;">
                            	<?php foreach ($saleAreas as $sa) { ?>
                            	<span class="span-list"><input class="send-check sa-check" data-id="<?php echo $sa['sales_area_id']; ?>" name="all_salearea[]" type="checkbox" /><?php echo $sa['name']; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="form-group send-div">	
							<label class="col-sm-2 control-label">发送仓库管理员</label>
                            <div class="col-sm-10" style="padding-top:9px;">
                            	<?php foreach ($warehouses as $w) { ?>
                            	<span class="span-list"><input class="send-check wh-check" data-id="<?php echo $w['warehouse_id']; ?>" name="all_warehouse[]" type="checkbox" /><?php echo $w['name']; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="form-group send-div">	
							<label class="col-sm-2 control-label">发送个别用户</label>
                			<div class="col-sm-10">
                            	<input name="add_customer" type="text" value="" placeholder="输入用户名或手机号" class="form-control bhz-input" /> 
                                <table class="table table-bordered table-hover bhz-table" style="margin-top:10px;">
                     				<thead>
                                        <tr>
                                        	<th width="33%" class="text-center">用户ID</th>
                                            <th class="text-center">用户名</th>
                                            <th width="33%" class="text-center">操作</th>
                                        </tr>
                      				</thead>
                  					<tbody id="send-person">
                    				</tbody>
                  				</table>
                        	</div>
                    	</div>
                        
                        <div class="text-right">
                        	<a id="send-btn" href="javascript:void(0);" data-toggle="tooltip" title="发送" class="btn btn-success">发送通知</a>
                        </div>
                        
                        </form>
                        
                    </div>
                    <!--自定义通知-->
                	
                    <!--销售通知-->
					<div class="tab-pane" id="tab-order">
                    		
                    	<div class="table-responsive">
                        	<span class="help-notice">注：通配符{{name}}表示用户名，{{order}}表示订单号</span>
                    		<table class="table table-bordered table-hover bhz-table">
                     			<thead>
                              		<tr>
                            			<th width="10%" class="text-center">发送状态</th>
                                        <th width="40%" class="text-left">发送内容</th>
                                        <th width="15%" class="text-left">发送给</th>
                                        <th width="15%" class="text-left">发送短信给</th>
                                        <th width="20%" class="text-center">操作</th>
                              		</tr>
                      			</thead>
                  				<tbody>
                                	<tr>
                                    	<td class="text-center">审核完成</td>
                                        <td>
                                        	<textarea class="message-area">{{name}}下的{{order}}订单已完成审核，总仓备货中。</textarea>
                                       	</td>
                                        <td>
                                        	<p><input class="pro-check" type="checkbox" /> 用户</p>
                                            <p><input class="pro-check" type="checkbox" /> 业务员</p>
                                        </td>
                                        <td>
                                        	<p><input class="pro-check" type="checkbox" /> 用户</p>
                                            <p><input class="pro-check" type="checkbox" /> 业务员</p>
                                        </td>
                                        <td class="text-center">
                                        	<a href="" data-toggle="tooltip" title="保存" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                        	<a href="" data-toggle="tooltip" title="停用" class="btn btn-danger"><i class="fa fa-ban"></i></a>
                                        </td>
                                    </tr>
                    			</tbody>
                  			</table>
                    	</div>
                            
                    </div>
                    <!--销售通知-->
                    
                    <!--活动通知-->
					<div class="tab-pane" id="tab-show">
                    	
                        <div class="table-responsive">
                        	<span class="help-notice">注：通配符{{name}}表示用户名，{{day}}表示天数</span>
                    		<table class="table table-bordered table-hover bhz-table">
                     			<thead>
                              		<tr>
                            			<th width="10%" class="text-center">发送状态</th>
                                        <th width="40%" class="text-left">发送内容</th>
                                        <th width="15%" class="text-left">发送给</th>
                                        <th width="15%" class="text-left">发送短信给</th>
                                        <th width="20%" class="text-center">操作</th>
                              		</tr>
                      			</thead>
                  				<tbody>
                                	<tr>
                                    	<td class="text-center">注册送优惠券</td>
                                        <td>
                                        	<textarea class="message-area">感谢注册百货栈，您已获价值100元“现金抵用券”{{day}}天内有效，请尽快使用。</textarea>
                                       	</td>
                                        <td>
                                        	<p><input class="pro-check" type="checkbox" /> 用户</p>
                                        </td>
                                        <td>
                                        	<p><input class="pro-check" type="checkbox" /> 用户</p>
                                        </td>
                                        <td class="text-center">
                                        	<a href="" data-toggle="tooltip" title="保存" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                        	<a href="" data-toggle="tooltip" title="停用" class="btn btn-danger"><i class="fa fa-ban"></i></a>
                                        </td>
                                    </tr>
                    			</tbody>
                  			</table>
                    	</div>
                        
                    </div>
                    <!--活动通知-->  
            
            </div>
            
		</div>
        
	</div>
	<!--内容-->
  
</div>

<script src="view/javascript/lyc/message.js" type="text/javascript"></script>

<script type="text/javascript">
	
	<!--用户autocomplete-->
	$('input[name=\'add_customer\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=marketing/message/customerAuto&token=<?php echo $token; ?>&filter_customer=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['fullname'],
							value: item['customer_id'],
							name: item['fullname']
						}
					}));
				}
			});
		},
		'select': function(item) {
			
			var repeat = 0;
			var temp;
			$("#send-person tr td.send-id").each(function(){		
				if($(this).attr('data-value') == item['value']){
					repeat = 1;
					return false;
				}            
            });
			
			if(repeat == 0){
				temp = "<tr>"+
								   "<td class='text-center send-id' data-value='"+item['value']+"'>"+item['value']+"</td>"+
								   "<td class='text-center'>"+item['name']+"</td>"+
								   "<td class='text-center'><button type='button' title='删除' class='btn btn-danger del-send'><i class='fa fa-trash-o'></i></button></td>"+
								   "</tr>";
				$("#send-person").append(temp);
			}
			
			$('input[name=\'add_customer\']').val('');
			
		}
	});
	<!--用户autocomplete-->
	
	<!--删除发送者-->
	$("#send-person").on('click','.del-send',function(){
		$(this).parent().parent().remove();
	});
	<!--删除发送者-->
	
	<!--发送通知-->
	$("#send-btn").on('click',function(e){
		
		e.preventDefault();
		
		var ptemp = [];
		var persons = "";
		
		var said = [];
		var sa = "";
		
		var whid = [];
		var wh = "";
		
		var method = "normal";
		var title = $("input[name='title']").val();
		var content = $("textarea[name='content']").val();
		
		if($(".all-check").is(':checked')){
			method = "all";
		}
		else if($(".all-admin").is(':checked')){
			method = "admin";
		}
		else{
			
			//发送业务员
			$(".sa-check").each(function(){
            	if(this.checked){
					said.push(parseInt($(this).attr('data-id')));
				}
            });
			
			//发送仓库管理员
			$(".wh-check").each(function(){
            	if(this.checked){
					whid.push(parseInt($(this).attr('data-id')));
				}
            });
			
			//格式化业务区域或仓库
			if(said.length>0 || whid.length>0){
				method = "special";
				sa = JSON.stringify(said);
				wh = JSON.stringify(whid);
			}
			
			$("#send-person tr td.send-id").each(function(){
				ptemp.push(parseInt($(this).attr('data-value')));
			});	
			persons = JSON.stringify(ptemp); //将数组转为JSON字符串
		}
			
		$.ajax({
			url: 'index.php?route=marketing/message/sendMessage&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {"title":title, "content":content, "method":method, "sa":sa, "wh":wh, "persons":persons},
			beforeSend: function() {
				$('#send-btn').button('loading');
			},
			complete: function() {
				$('#send-btn').button('reset');
			},
			success: function(json) {

				$(".alert").remove();

				if (json['error']) {
					$('#tab-diy').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
				}

				if (json['success']) {
					$('#tab-diy').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
					
					$("input[name='title']").val('');
					$("textarea[name='content']").val('');
				
				}
				
			}
				
		});
		
		
	});
	<!--发送通知-->
	
</script>

<?php echo $footer; ?>