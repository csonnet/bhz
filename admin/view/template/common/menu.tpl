<ul id="menu">
  <?php if($permissions['home']){ ?><li id="dashboard"><a href="<?php echo $home; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li><?php } ?>
  <li id="catalog"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
    <ul>
      <?php if($permissions['category']){ ?><li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li><?php } ?>
      <?php if($permissions['category_mgr_lite']){ ?><li><a href="<?php echo $category_mgr_lite; ?>"><?php echo $category_mgr_lite_heading_title; ?></a></li><?php } ?>
      <?php if($permissions['product']){ ?><li class="hidden"><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li><?php } ?>
      <?php if($permissions['mvd_product']){ ?><li><a href="<?php echo $mvd_product; ?>"><?php echo $text_product; ?></a></li><?php } ?>
      <?php if($permissions['recurring']){ ?><li><a class="hidden" href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li><?php } ?>
      <?php if($permissions['filter']){ ?><li><a href="<?php echo $filter; ?>"><?php echo $text_filter; ?></a></li><?php } ?>
      <li><a class="parent"><?php echo $text_attribute; ?></a>
        <ul>
          <?php if($permissions['attribute']){ ?><li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?></a></li><?php } ?>
          <?php if($permissions['attribute_group']){ ?><li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li><?php } ?>
        </ul>
      </li>
      <?php if($permissions['option']){ ?><li><a href="<?php echo $option; ?>"><?php echo $text_option; ?></a></li><?php } ?>
      <?php if($permissions['manufacturer']){ ?><li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li><?php } ?>
      <?php if($permissions['download']){ ?><li class="hidden"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><?php } ?>
      <?php if($permissions['review']){ ?><li><a href="<?php echo $review; ?>"><?php echo $text_review; ?></a></li><?php } ?>
      <?php if($permissions['information']){ ?><li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li><?php } ?>
      <?php if($permissions['pim']){ ?><li><a href="<?php echo $pim; ?>"><?php echo $text_pim; ?></a></li><?php } ?>
      <?php if($permissions['url_alias']){ ?><li class="hidden"><a href="<?php echo $url_alias; ?>"><?php echo $text_url_alias; ?></a></li><?php } ?>
    </ul>
  </li>
  <li id="extension"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span><?php echo $text_extension; ?></span></a>
    <ul>
      <?php if($permissions['installer']){ ?><li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li><?php } ?>
      <?php if($permissions['modification']){ ?><li><a class="hidden" href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li><?php } ?>
      <?php if($permissions['analytics']){ ?><li><a class="hidden" href="<?php echo $analytics; ?>"><?php echo $text_analytics; ?></a></li><?php } ?>
      <?php if($permissions['captcha']){ ?><li><a class="hidden" href="<?php echo $captcha; ?>"><?php echo $text_captcha; ?></a></li><?php } ?>
      <?php if($permissions['feed']){ ?><li><a class="hidden" href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li><?php } ?>
      <?php if($permissions['fraud']){ ?><li><a class="hidden" href="<?php echo $fraud; ?>"><?php echo $text_fraud; ?></a></li><?php } ?>
      <?php if($permissions['module']){ ?><li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li><?php } ?>
      <?php if($permissions['payment']){ ?><li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li><?php } ?>
      <?php if($permissions['shipping']){ ?><li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li><?php } ?>
      <!--<?php if($permissions['total']){ ?><li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li><?php } ?>-->
      <?php if($permissions['sms']){ ?><li><a href="<?php echo $sms; ?>"><?php echo $text_sms; ?></a></li><?php } ?>
    </ul>
  </li>
<!-- 超市商品 -->
  <li id="catalog"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span>超市商品目录</span></a>
    <ul>
      <?php if($permissions['shop_category']){ ?><li><a href="<?php echo $shop_category; ?>">超市分类</a></li><?php } ?>
      <!-- <?php if($permissions['category_mgr_lite']){ ?><li><a href="<?php echo $category_mgr_lite; ?>">超市分类树管理</a></li><?php } ?> -->
      <?php if($permissions['mvd_product']){ ?><li><a href="<?php echo $mvd_product; ?>">超市商品</a></li><?php } ?>
      <?php if($permissions['shop_filter']){ ?><li><a href="<?php echo $shop_filter; ?>">超市筛选</a></li><?php } ?>
    </ul>
  </li>
<!-- 超市商品 -->
  <li id="design"><a class="parent"><i class="fa fa-television fa-fw"></i> <span><?php echo $text_design; ?></span></a>
    <ul>
      <?php if($permissions['layout']){ ?><li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li><?php } ?>
      <?php if($permissions['banner']){ ?><li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li><?php } ?>
    </ul>
  </li>

	<!--进销存管理-->
	<li id="stock">
  		<a class="parent"><i class="fa fa-database fa-fw"></i> <span><?php echo $text_inventory_manage; ?></span></a>
		<ul>
			<?php if($permissions['warehouse']){ ?><li><a href="<?php echo $warehouse; ?>"><?php echo $text_warehouse; ?></a></li><?php } ?>
			<?php if($permissions['stock_search']){ ?><li><a href="<?php echo $stock_search; ?>"><?php echo $text_stock_search; ?></a></li><?php } ?>
            <?php if($permissions['stock_in']){ ?><li><a href="<?php echo $stock_in; ?>"><?php echo $text_stock_in; ?></a></li><?php } ?>
			<?php if($permissions['stock_out']){ ?><li><a href="<?php echo $stock_out; ?>"><?php echo $text_stock_out; ?></a></li><?php } ?>
			<?php if($permissions['trim_main_stock']){ ?><li><a href="<?php echo $trim_main_stock; ?>"><?php echo $text_trim_stock; ?></a></li><?php } ?>
			<?php if($permissions['requisition_main']){ ?><li><a href="<?php echo $requisition_main; ?>"><?php echo $text_requisition_main; ?></a></li><?php } ?>
            <?php if($permissions['lend_order']){ ?><li><a href="<?php echo $lend_order; ?>"><?php echo $text_lend_order; ?></a></li><?php } ?>
		</ul>
	</li>
	<!--进销存管理-->

<!--应收应付管理-->
  <li id="money">
      <a class="parent"><i class="fa fa-database fa-fw"></i> <span><?php echo $text_money; ?></span></a>
    <ul>
      <?php if($permissions['money_in']){ ?><li><a href="<?php echo $money_in; ?>"><?php echo $text_money_in; ?></a></li><?php } ?>
      <?php if($permissions['money_out']){ ?><li><a href="<?php echo $money_out; ?>"><?php echo $text_money_out; ?></a></li><?php } ?>
      <!-- <?php if($permissions['acount']){ ?><li><a href="<?php echo $acount; ?>"><?php echo $text_acount; ?></a></li><?php } ?> -->
      <?php if($permissions['factory']){ ?><li><a href="<?php echo $factory; ?>"><?php echo $text_factory; ?></a></li><?php } ?>
      <?php if($permissions['distributor']){ ?><li><a href="<?php echo $distributor; ?>"><?php echo $text_distributor; ?></a></li><?php } ?>
      <?php if($permissions['customer_account']){ ?><li><a href="<?php echo $customer_account; ?>"><?php echo $text_customer_account; ?></a></li><?php } ?>
  <!--     <?php if($permissions['vdi_factory']){ ?><li><a href="<?php echo $vdi_factory; ?>"><?php echo $text_vdi_factory; ?></a></li><?php } ?> -->
      <?php if($permissions['refund']){ ?><li><a href="<?php echo $refund; ?>"><?php echo $text_refund; ?></a></li><?php } ?>
      <?php if($permissions['cost']){ ?><li><a href="<?php echo $cost; ?>"><?php echo $text_cost; ?></a></li><?php } ?>
      <?php if($permissions['auditor']){ ?><li><a href="<?php echo $auditor; ?>"><?php echo $text_auditor; ?></a></li><?php } ?>

    </ul>
  </li>
  <!--应收应付管理-->

  <!--原始订单-->
  <li id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
    <ul>
      <?php if($permissions['goodsshelf']){ ?><li><a href="<?php echo $goodsshelf; ?>">货架</a></li><?php } ?>
      <?php if($permissions['order']){ ?><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><?php } ?>
      <?php if($permissions['main_purchase_order']){ ?><li><a href="<?php echo $main_purchase_order; ?>">采购单</a></li><?php } ?>
      <li><a href="<?php echo URL('sale/purchase_order','token='.$token) ?>">采购单</a></li>

      <?php if($permissions['order_recurring']){ ?><li><a class="hidden" href="<?php echo $order_recurring; ?>"><?php echo $text_order_recurring; ?></a></li><?php } ?>
      <?php if($permissions['order_distributor']){ ?><li><a href="<?php echo $order_distributor; ?>"><?php echo $text_order; ?></a></li><?php } ?>
      <?php if($permissions['return']){ ?><li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li><?php } ?>
      <?php if($permissions['return_main_order']){ ?><li><a href="<?php echo $return_main_order; ?>"><?php echo $text_return_main_order; ?></a></li><?php } ?>
      <!--<?php if($permissions['check_main_stock']){ ?><li><a href="<?php echo $check_main_stock; ?>"><?php echo $text_check_stock; ?></a></li><?php } ?>-->
       <?php if($permissions['remit_money']){ ?><li><a href="<?php echo $remit_money; ?>"><?php echo $text_remit_money; ?></a></li><?php } ?>
      <li><a class="parent hidden"><?php echo $text_voucher; ?></a>
        <ul>
          <?php if($permissions['voucher']){ ?><li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li><?php } ?>
          <?php if($permissions['voucher_theme']){ ?><li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent hidden"><?php echo $text_paypal ?></a>
        <ul>
          <?php if($permissions['paypal_search']){ ?><li><a href="<?php echo $paypal_search ?>"><?php echo $text_paypal_search ?></a></li><?php } ?>
        </ul>
      </li>
    </ul>
  </li>
  <!--原始订单-->

  <!--采购管理-->
  <li id="sale"><a class="parent"><i class="fa fa-opencart fa-fw"></i> <span><?php echo $text_purchase; ?></span></a>
    <ul>
      <?php if($permissions['bak.main_purchase_order']){ ?><li><a href="<?php echo $main_purchase_order; ?>">采购单</a></li><?php } ?>
    </ul>
  </li>
  <!--采购管理-->

  <li id="bill"><a class="parent"><i class="fa fa-cny fa-fw"></i> <span>对账单</span></a>
    <ul>
      <!-- <?php if($permissions['logcenter_bill']){ ?><li><a href="<?php echo $logcenter_bill; ?>">物流营销对账单</a></li><?php } ?> -->
      <?php if($permissions['vendor_bill']){ ?><li><a href="<?php echo $vendor_bill; ?>">品牌厂商对账单</a></li><?php } ?>
    </ul>
  </li>
  <li id="customer"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_customer; ?></span></a>
    <ul>
      <?php if($permissions['customer']){ ?><li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li><?php } ?>
      <?php if($permissions['customer_distributor']){ ?><li><a href="<?php echo $customer_distributor; ?>"><?php echo $text_customer; ?></a></li><?php } ?>
      <?php if($permissions['customer_group']){ ?><li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li><?php } ?>
      <?php if($permissions['custom_field']){ ?><li><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li><?php } ?>
    </ul>
  </li>
  <li id="mvd_vendors"><a class="parent"><i class="fa fa-users fa-fw"></i> <span><?php echo $text_vendor_tool; ?></span></a>
    <ul>
    <?php if($permissions['mvd_add_vendors']){ ?><li><a href="<?php echo $mvd_add_vendors; ?>"><?php echo $text_add_vendors; ?></a></li><?php } ?>
    <?php if($permissions['mvd_add_courier']){ ?><li class="hidden"><a href="<?php echo $mvd_add_courier; ?>"><?php echo $text_add_courier; ?></a></li><?php } ?>
    <li><a class="parent"><?php echo $text_catalog; ?></a>
      <ul>
       <?php if($permissions['mvd_product']){ ?><li><a href="<?php echo $mvd_product; ?>"><?php echo $text_product; ?></a></li><?php } ?>
       <li><a class="parent"><?php echo $text_attribute; ?></a>
        <ul>
          <?php if($permissions['mvd_attribute']){ ?><li><a href="<?php echo $mvd_attribute; ?>"><?php echo $text_attribute; ?></a></li><?php } ?>
          <?php if($permissions['mvd_attribute_group']){ ?><li><a href="<?php echo $mvd_attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li><?php } ?>
        </ul>
       </li>
       <?php if($permissions['mvd_option']){ ?><li><a href="<?php echo $mvd_option; ?>"><?php echo $text_option; ?></a></li><?php } ?>
       <?php if($permissions['mvd_download']){ ?><li class="hidden"><a href="<?php echo $mvd_download; ?>"><?php echo $text_download; ?></a></li><?php } ?>
       <?php if($permissions['mvd_information']){ ?><li class="hidden"><a href="<?php echo $mvd_information; ?>"><?php echo $text_information; ?></a></li><?php } ?>
      </ul>
    </li>
    <li class="hidden"><a class="parent"><?php echo $text_vendor_sales; ?></a>
      <ul>
      <?php if($permissions['mvd_ven_transaction']){ ?><li><a href="<?php echo $mvd_ven_transaction; ?>"><?php echo $text_vendor_transaction; ?></a></li><?php } ?>
      <?php if($permissions['mvd_payment_history']){ ?><li><a href="<?php echo $mvd_payment_history; ?>"><?php echo $text_payment_history; ?></a></li><?php } ?>
      <?php if($permissions['mvd_contract_history']){ ?><li><a href="<?php echo $mvd_contract_history; ?>"><?php echo $text_contract_history; ?></a></li><?php } ?>
      <?php if($permissions['mvd_coupon']){ ?><li><a href="<?php echo $mvd_coupon; ?>"><?php echo $text_coupon; ?></a></li><?php } ?>
      </ul>
    </li>
    <li class="hidden"><a class="parent"><?php echo $text_vendor_tools; ?></a>
      <ul>
      <?php if($permissions['mvd_prostatctrl']){ ?><li><a href="<?php echo $mvd_prostatctrl; ?>"><?php echo $text_quicker_status_modifier; ?></a></li><?php } ?>
      <?php if($permissions['mvd_contstatctrl']){ ?><li><a href="<?php echo $mvd_contstatctrl; ?>"><?php echo $text_contract_status_modifier; ?></a></li><?php } ?>
      </ul>
    </li>
    <li><a class="parent hidden"><?php echo $text_setup; ?></a>
      <ul>
      <?php if($permissions['mvd_limit']){ ?><li class="hidden"><a href="<?php echo $mvd_limit; ?>"><?php echo $text_package_limit; ?></a></li><?php } ?>
      <?php if($permissions['mvd_commission']){ ?><li class="hidden"><a href="<?php echo $mvd_commission; ?>"><?php echo $text_vendor_commission; ?></a></li><?php } ?>
      <?php if($permissions['mvd_setting']){ ?><li class="hidden" style=""><a href="<?php echo $mvd_setting; ?>"><?php echo $text_vendor_setting; ?></a></li><?php } ?>
      </ul>
    </li>
    </ul>
  </li>
  <li id="mvd_logcenters"><a class="parent"><i class="fa fa-truck fa-fw"></i> <span><?php echo $text_logcenter_tool; ?></span></a>
    <ul>
      <?php if($permissions['mvd_add_logcenters']){ ?><li><a href="<?php echo $mvd_add_logcenters; ?>"><?php echo $text_add_logcenters; ?></a></li><?php } ?>
    </ul>
  </li>
  <li><a class="parent"><i class="fa fa-globe fa-fw"></i> <span><?php echo $text_marketing_tools; ?></span></a>
    <ul>
      <?php if($permissions['luckydraw']){ ?><li><a href="<?php echo $luckydraw; ?>"><?php echo $text_luckydraw; ?></a></li><?php } ?>
    </ul>
  </li>
  <li><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
    <ul>
      <?php if($permissions['marketing']){ ?><li><a class="hidden" href="<?php echo $marketing; ?>"><?php echo $text_marketing; ?></a></li><?php } ?>
      <?php if($permissions['affiliate']){ ?><li><a class="hidden" href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li><?php } ?>
      <!-- <?php if($permissions['ground']){ ?><li><a href="<?php echo $ground; ?>"><?php echo $text_ground; ?></a></li><?php } ?> -->
      <?php if($permissions['coupon']){ ?><li><a class="hidden" href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li><?php } ?>
      <?php if($permissions['contact']){ ?><li><a class="hidden" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li><?php } ?>

      <!--消息管理-->
      <?php if($permissions['message']){ ?><li><a href="<?php echo $message; ?>">消息管理</a></li><?php } ?>
      <!--消息管理-->

    </ul>
  </li>
  <li><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span>统计报表</span></a>
    <ul>
      <?php if($permissions['reportV2_customer']){ ?><li><a href="<?php echo $reportV2_customer; ?>">业务员业绩统计</a></li><?php } ?>
      <?php if($permissions['buyer']){ ?><li><a href="<?php echo $buyer; ?>">采销员业绩统计</a></li><?php } ?>
      <?php if($permissions['supplier']){ ?><li><a href="<?php echo $supplier; ?>">供应商分析统计</a></li><?php } ?>
    </ul>
  </li>
  <li id="system"><a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php echo $text_system; ?></span></a>
    <ul>
      <?php if($permissions['setting']){ ?><li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li><?php } ?>
      <li><a class="parent"><?php echo $text_users; ?></a>
        <ul>
          <?php if($permissions['user']){ ?><li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li><?php } ?>
          <?php if($permissions['user_group']){ ?><li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li><?php } ?>
          <?php if($permissions['api']){ ?><li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_localisation; ?></a>
        <ul>
          <?php if($permissions['location']){ ?><li class="hidden"><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li><?php } ?>
          <?php if($permissions['language']){ ?><li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li><?php } ?>
          <?php if($permissions['currency']){ ?><li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li><?php } ?>
          <?php if($permissions['stock_status']){ ?><li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li><?php } ?>
          <?php if($permissions['order_status']){ ?><li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li><?php } ?>
          <li class="hidden"><a class="parent"><?php echo $text_return; ?></a>
            <ul>
              <?php if($permissions['return_status']){ ?><li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li><?php } ?>
              <?php if($permissions['return_action']){ ?><li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li><?php } ?>
              <?php if($permissions['return_reason']){ ?><li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li><?php } ?>
            </ul>
          </li>
          <?php if($permissions['country']){ ?><li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li><?php } ?>
          <?php if($permissions['zone']){ ?><li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li><?php } ?>
          <?php if($permissions['geo_zone']){ ?><li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li><?php } ?>
          <li><a class="parent hidden"><?php echo $text_tax; ?></a>
            <ul>
              <?php if($permissions['tax_class']){ ?><li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li><?php } ?>
              <?php if($permissions['tax_rate']){ ?><li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li><?php } ?>
            </ul>
          </li>
          <?php if($permissions['length_class']){ ?><li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li><?php } ?>
          <?php if($permissions['weight_class']){ ?><li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_tools; ?></a>
        <ul>
          <?php if($permissions['upload']){ ?><li><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li><?php } ?>
          <?php if($permissions['backup']){ ?><li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li><?php } ?>
          <?php if($permissions['export']){ ?><li><a href="<?php echo $export; ?>"><?php echo $text_export; ?></a></li><?php } ?>
          <?php if($permissions['error_log']){ ?><li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li><?php } ?>
	  <!--lyc的工具-->
	  <?php if($permissions['lyctool']){ ?><li><a href="<?php echo $lyctool; ?>">lyc的工具</a></li><?php } ?>
	  <!--lyc的工具-->
	        <?php if($permissions['other_orders']){ ?><li><a href="<?php echo $other_orders; ?>">外场订单导入</a></li><?php } ?>
	        <?php if($permissions['shelf_import']){ ?><li><a href="<?php echo $shelf_import; ?>">智能货架导入</a></li><?php } ?>
        </ul>
      </li>
    </ul>
  </li>
  <li class="hidden" id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
    <ul>
      <li><a class="parent"><?php echo $text_sale; ?></a>
        <ul>
          <?php if($permissions['report_sale_order']){ ?><li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li><?php } ?>
          <?php if($permissions['report_sale_tax']){ ?><li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li><?php } ?>
          <?php if($permissions['report_sale_shipping']){ ?><li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li><?php } ?>
          <?php if($permissions['report_sale_return']){ ?><li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li><?php } ?>
          <?php if($permissions['report_sale_coupon']){ ?><li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_product; ?></a>
        <ul>
          <?php if($permissions['report_product_viewed']){ ?><li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li><?php } ?>
          <?php if($permissions['report_product_purchased']){ ?><li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_customer; ?></a>
        <ul>
          <?php if($permissions['report_customer_online']){ ?><li><a href="<?php echo $report_customer_online; ?>"><?php echo $text_report_customer_online; ?></a></li><?php } ?>
          <?php if($permissions['report_customer_activity']){ ?><li><a href="<?php echo $report_customer_activity; ?>"><?php echo $text_report_customer_activity; ?></a></li><?php } ?>
          <?php if($permissions['report_customer_order']){ ?><li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li><?php } ?>
          <?php if($permissions['report_customer_reward']){ ?><li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li><?php } ?>
          <?php if($permissions['report_customer_credit']){ ?><li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li><?php } ?>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_marketing; ?></a>
        <ul>
          <?php if($permissions['report_marketing']){ ?><li><a href="<?php echo $report_marketing; ?>"><?php echo $text_marketing; ?></a></li><?php } ?>
          <?php if($permissions['report_affiliate']){ ?><li><a href="<?php echo $report_affiliate; ?>"><?php echo $text_report_affiliate; ?></a></li><?php } ?>
          <?php if($permissions['report_affiliate_activity']){ ?><li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li><?php } ?>
        </ul>
      </li>
    </ul>
  </li>
  <li class="hidden" id="weidian"><a class="parent"><i class="fa fa-university"></i> <span><?php echo $text_weidian; ?></span></a>
    <ul>
      <?php if($permissions['wdcategory']){ ?><li><a href="<?php echo $wdcategory; ?>"><?php echo $text_wdcategory; ?></a></li><?php } ?>
      <?php if($permissions['wdproduct']){ ?><li><a href="<?php echo $wdproduct; ?>"><?php echo $text_wdproduct; ?></a></li><?php } ?>
      <?php if($permissions['config']){ ?><li><a href="<?php echo $config; ?>"><?php echo $text_config; ?></a></li><?php } ?>
    </ul>
  </li>
  <li class="hidden" id="others"><a class="parent"><i class="fa fa-key fa-fw"></i> <span><?php echo $text_others; ?></span></a>
  </li>
</ul>

<script type="text/javascript"><!--
  $('#menu li a.parent').each(function(index) {
    if($(this).next('ul').children('li').size() == 0) {
      $(this).parent('li').css('display', 'none');
    }
  })

  if($('#catalog ul li:not(:has(a.parent))').size() == 0) $('#catalog').css('display', 'none');
  if($('#extension ul li:not(:has(a.parent))').size() == 0) $('#extension').css('display', 'none');
  if($('#sale ul li:not(:has(a.parent))').size() == 0) $('#sale').css('display', 'none');
  if($('#system ul li:not(:has(a.parent))').size() == 0) $('#system').css('display', 'none');
  if($('#reports ul li:not(:has(a.parent))').size() == 0) $('#reports').css('display', 'none');
  if($('#mvd_vendors ul li:not(:has(a.parent))').size() == 0) $('#mvd_vendors').css('display', 'none');

//--></script>

