<div id="profile">
  <div>
    <?php if ($image) { ?>
    <img src="<?php echo $image; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $username; ?>" class="img-circle" />
    <?php } else { ?>
    <i class="fa fa-shopping-cart"></i>
    <?php } ?>
  </div>
  <div>
    <h4><?php echo $fullname . $logcenter_permission_level; ?></h4>
    <small><?php echo $user_group; ?></small><br ?>
    <?php if ($expiration_date) { ?>
    <small><?php echo $expiration_date; ?></small>
    <?php } ?>
  </div>
</div>
