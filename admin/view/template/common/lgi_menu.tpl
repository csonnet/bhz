<?php if(!$view_only) { ?> 
<ul id="menu">
<!--   <li id="catalog"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
    <ul>
      <li><a href="<?php echo $lgi_product; ?>"><?php echo $text_product; ?></a></li>
    </ul>
  </li> -->
  <li id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
    <ul>
      <li><a href="<?php echo $lgi_sale_order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo URL('sale/purchase_order','token='.$token) ?>">采购单</a></li>
<!--
      <li><a href="<?php echo URL('sale/return_main_order','token='.$token) ?>">退货单</a></li>
      <li><a href="<?php echo URL('sale/trim_main_stock','token='.$token) ?>">损益单</a></li>
      <li><a href="<?php echo URL('sale/requisition_main','token='.$token) ?>">调拨单</a></li>
      <li><a href="<?php echo URL('sale/check_logcenter_stock','token='.$token) ?>">库存查看</a></li>
-->
    </ul>
  </li>
	<!--进销存管理-->
	<li id="stock">
  		<a class="parent"><i class="fa fa-database fa-fw"></i> <span><?php echo $text_inventory_manage; ?></span></a>
		<ul>
			<li><a href="<?php echo URL('stock/stocksearch','token='.$token) ?>"><?php echo $text_stock_search; ?></a></li>
			<li><a href="<?php echo URL('stock/stock_in','token='.$token) ?>"><?php echo $text_stock_in; ?></a></li>
			<li><a href="<?php echo URL('sale/stock_out','token='.$token) ?>"><?php echo $text_stock_out; ?></a></li>
			<li><a href="<?php echo URL('sale/trim_main_stock','token='.$token) ?>"><?php echo $text_trim_stock; ?></a></li>
			<li><a href="<?php echo URL('sale/requisition_main','token='.$token) ?>"><?php echo $text_requisition_main; ?></a></li>
      <li><a href="<?php echo URL('sale/return_main_order','token='.$token) ?>">退货单</a></li>
		</ul>
	</li>
	<!--进销存管理-->
  <li id="bill"><a class="parent"><i class="fa fa-cny fa-fw"></i> <span>对账单</span></a>
    <ul>
      <li><a href="<?php echo URL('sale/lgi_logcenter_bill','token='.$token) ?>">物流营销对账单</a></li>
    </ul>
  </li>
  <li id="reports"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_vendor_personal; ?></span></a>
    <ul>
    <?php if ($expiration_date) { ?>
        <li><a href="<?php echo $lgi_contract_history; ?>"><?php echo $text_contract_history; ?></a></li>
    <?php } ?>
        <li><a href="<?php echo $lgi_update_vendor_profile; ?>"><?php echo $text_vendor_update_profile; ?></a></li>
    <li><a href="<?php echo $lgi_user_password; ?>"><?php echo $text_vendor_update_password; ?></a></li>
    </ul>
  </li>
  <li id="reports"><a class="parent"><i class="fa fa-bar-chart fa-fw"></i> <span>报告</span></a>
    <ul>
        <li><a href="<?php echo URL('report/product_purchased','token='.$token) ?>">商品购买报告</a></li>
    </ul>
  </li>
</ul>
<?php } else { ?>
<ul id="menu">
  <li id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
    <ul>
      <li><a href="<?php echo $lgi_sale_order; ?>"><?php echo $text_order; ?></a></li>
    </ul>
  </li>
</ul>
<?php } ?>
