<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-tianyi" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-tianyi" class="form-horizontal">
          
        
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-userid"><?php echo $app_id; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_appid" value="<?php echo $tianyi_appid; ?>" placeholder="<?php echo $app_id; ?>" id="input-userid" class="form-control" />
              <?php if ($error_appid) { ?>
                  <div class="text-danger"><?php echo $error_appid; ?></div>
              <?php } ?>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-account"><?php echo $app_secret; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_appsecret" value="<?php echo $tianyi_appsecret; ?>" placeholder="<?php echo $app_secret; ?>" id="input-account" class="form-control" />
              <?php if ($error_appsecret) { ?>
                  <div class="text-danger"><?php echo $error_appsecret; ?></div>
              <?php } ?>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid" value="<?php echo $tianyi_templateid; ?>" placeholder="<?php echo $template_id; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid) { ?>
                  <div class="text-danger"><?php echo $error_templateid; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id_co; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_co" value="<?php echo $tianyi_templateid_co; ?>" placeholder="<?php echo $template_id_co; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid_co) { ?>
                  <div class="text-danger"><?php echo $error_templateid_co; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id_sp; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_sp" value="<?php echo $tianyi_templateid_sp; ?>" placeholder="<?php echo $template_id_sp; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid_sp) { ?>
                  <div class="text-danger"><?php echo $error_templateid_sp; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id_sv; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_sv" value="<?php echo $tianyi_templateid_sv; ?>" placeholder="<?php echo $template_id_sv; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid_sv) { ?>
                  <div class="text-danger"><?php echo $error_templateid_sv; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id_tc; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_tc" value="<?php echo $tianyi_templateid_tc; ?>" placeholder="<?php echo $template_id_tc; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid_tc) { ?>
                  <div class="text-danger"><?php echo $error_templateid_tc; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $template_id_dc; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_dc" value="<?php echo $tianyi_templateid_dc; ?>" placeholder="<?php echo $template_id_dc; ?>" id="input-password" class="form-control" />
              <?php if ($error_templateid_dc) { ?>
                  <div class="text-danger"><?php echo $error_templateid_dc; ?></div>
              <?php } ?>
            </div>
          </div>
	
	  <!--新注册用户优惠券提醒-->
	  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-reg"><?php echo $template_id_reg; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_reg" value="<?php echo $tianyi_templateid_reg; ?>" placeholder="<?php echo $template_id_reg; ?>" id="input-reg" class="form-control" />
              <?php if ($error_templateid_reg) { ?>
                  <div class="text-danger"><?php echo $error_templateid_reg; ?></div>
              <?php } ?>
            </div>
          </div>
	  <!--新注册用户优惠券提醒-->
      
      <!--优惠券快到期提醒-->
	  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-ex"><?php echo $template_id_ex; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tianyi_templateid_ex" value="<?php echo $tianyi_templateid_ex; ?>" placeholder="<?php echo $template_id_ex; ?>" id="input-ex" class="form-control" />
              <?php if ($error_templateid_ex) { ?>
                  <div class="text-danger"><?php echo $error_templateid_ex; ?></div>
              <?php } ?>
            </div>
          </div>
	  <!--优惠券快到期提醒-->
                    
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="tianyi_status" id="input-status" class="form-control">
                <?php if ($tianyi_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 