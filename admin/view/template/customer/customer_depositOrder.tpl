<div class="table-responsive">
  <table class="table table-bordered table-hover bhz-table">
    <thead>
      <tr>
        <th width="20%" class="text-left">添加日期</th>
        <th width="10%" class="text-left">操作人</th>
        <th width="10%" class="text-left">操作人类型</th>
        <th width="10%" class="text-center">充值方式</th>
        <th width="10%" class="text-center">充值类型</th>
        <th width="10%" class="text-right">定金金额</th>
        <th width="10%" class="text-center">状态</th>
        <th width="20%" class="text-left">备注</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($depositOrders) { ?>
      <?php foreach ($depositOrders as $do) { ?>
      <tr>
        <td class="text-left"><?php echo $do['date_added']; ?></td>
        <td class="text-left"><?php echo $do['runner']; ?></td>
        <td class="text-left"><?php echo $do['rtype']; ?></td>
        <td class="text-center"><?php echo $do['payment_method']; ?></td>
        <td class="text-center"><?php echo $do['btype']; ?></td>
        <td class="text-right"><?php echo $do['money']; ?> 元</td>
        <td class="text-center"><?php echo $do['status_name']; ?></td>
        <td class="text-left"><?php echo $do['memo']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="99"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
