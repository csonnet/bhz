<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-customer').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div id="form-order"></div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-recommended-code"><?php echo $entry_recommended_code; ?></label>
                <input type="text" name="filter_recommended_code" value="<?php echo $filter_recommended_code; ?>" placeholder="<?php echo $entry_recommended_code; ?>" id="input-recommended-code" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label">最近下单时间（开始）</label>
                <div class="input-group date">
                  <input type="text" name="filter_last_date_start" value="<?php echo $filter_last_date_start; ?>" placeholder="最早最近下单时间" data-date-format="YYYY-MM-DD" id="input-last-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-customer-group"><?php echo $entry_customer_group; ?></label>
                <select name="filter_customer_group_id" id="input-customer-group" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($customer_groups as $customer_group) { ?>
                  <?php if ($customer_group['customer_group_id'] == $filter_customer_group_id) { ?>
                  <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              <!--最早添加日期-->
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_start; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
              	</div>
              </div>
              <!--最早添加日期-->
              <div class="form-group">
                <label class="control-label">最近下单时间（结束）</label>
                <div class="input-group date">
                  <input type="text" name="filter_last_date_end" value="<?php echo $filter_last_date_end; ?>" placeholder="最晚最近下单时间" data-date-format="YYYY-MM-DD" id="input-last-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>

            </div>
            <?php if(0) { ?>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-approved"><?php echo $entry_approved; ?></label>
                <select name="filter_approved" id="input-approved" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_approved) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <?php } ?>
                  <?php if (!$filter_approved && !is_null($filter_approved)) { ?>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </div>

            </div>
            <?php } ?>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-need-review"><?php echo $entry_need_review; ?></label>
                <select name="filter_need_review" id="input-need-review" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_need_review) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <?php } ?>
                  <?php if (!$filter_need_review && !is_null($filter_need_review)) { ?>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                <input type="text" name="filter_telephone" value="<?php echo $filter_telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              </div>
              <!--最晚添加日期-->
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_end; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
              	</div>
              </div>
               <!--最晚添加日期-->
              <div class="form-group">
                <label class="control-label" for="input-need-review">客户类型</label>
                <select name="filter_is_shelf_customer" id="input-need-review" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_is_shelf_customer) { ?>
                  <option value="1" selected="selected">货架客户</option>
                  <?php } else { ?>
                  <option value="1">货架客户</option>
                  <?php } ?>
                  <?php if (!$filter_is_shelf_customer && !is_null($filter_is_shelf_customer)) { ?>
                  <option value="0" selected="selected">普通客户</option>
                  <?php } else { ?>
                  <option value="0">普通客户</option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-company_name"><?php echo $entry_company_name; ?></label>
                <input type="text" name="filter_company_name" value="<?php echo $filter_company_name; ?>" placeholder="<?php echo $entry_company_name; ?>" id="input-company_name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-logcenter-id"><?php echo $entry_logcenter_id; ?></label>
                <select name="filter_logcenter_id" id="input-logcenter-id" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($logcenters as $logcenter) { ?>
                  <?php if ($logcenter['logcenter_id'] == $filter_logcenter_id) { ?>
                  <option value="<?php echo $logcenter['logcenter_id']; ?>" selected="selected"><?php echo $logcenter['logcenter_name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $logcenter['logcenter_id']; ?>"><?php echo $logcenter['logcenter_name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">下单总数</label>
                <input type="text" name="filter_order_sum" value="<?php echo $filter_order_sum; ?>" placeholder="下单总数" id="inout-order_sum" class="form-control" />
              </div>
              <div class="row">
                <div class="col-sm-6">

                  <button type="button" id="button-download" class="btn btn-primary pull-right"><i class="fa fa-download"></i> 导出</button>
                  <button type="button" id="Month-buy" class="btn btn-primary pull-right"><i class="fa fa-download"></i>导出月售</button>
                </div>
                <!-- <div class="col-sm-6">
                  <button type="button" id="button-upload" class="btn btn-primary pull-right"><i class="fa fa-download"></i> 更改终端类型</button>
                </div>-->
                <div class="col-sm-6">

                  <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                    <a  id="upload" data-toggle="tooltip" title="效正推荐码" class="btn btn-primary pull-right" style="margin-left:10px;"><i class="fa fa-calculator"></i>效正推荐码</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-customer">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.email') { ?>
                    <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'company_name') { ?>
                    <a href="<?php echo $sort_company_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_company_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_company_name; ?>"><?php echo $column_company_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'customer_group') { ?>
                    <a href="<?php echo $sort_customer_group; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_group; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_customer_group; ?>"><?php echo $column_customer_group; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.need_review') { ?>
                    <a href="<?php echo $sort_need_review; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_need_review; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_need_review; ?>"><?php echo $column_need_review; ?></a>
                    <?php } ?></td>
                  <td class="text-left">营业执照</td>
                  <td class="text-left"><?php if ($sort == 'c.telephone') { ?>
                    <a href="<?php echo $sort_telephone; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_telephone; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_telephone; ?>"><?php echo $column_telephone; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.logcenter_id') { ?>
                    <a href="<?php echo $sort_logcenter_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_logcenter_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_logcenter_id; ?>"><?php echo $column_logcenter_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'order_sum') { ?>
                  <a href="<?php echo $sort_order_sum; ?>" class="<?php echo strtolower($order); ?>">下单总数</a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_order_sum; ?>">下单总数</a>
                  <?php } ?></td>
                  <td class="text-left">客户类型</td>
                  <td class="text-left"><?php if ($sort == 'last_order_date') { ?>
                  <a href="<?php echo $sort_last_order_date?>" class="<?php echo strtolower($order); ?>">最近下单时间</a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_last_order_date?>">最近下单时间</a>
                  <?php } ?></td>
                  <td class="text-left"><?php echo $column_recommended_code; ?></td>
                  <td class="text-left"><?php if ($sort == 'c.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($customers) { ?>
                <?php foreach ($customers as $customer) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($customer['customer_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $customer['name']; ?></td>
                  <td class="text-left"><?php echo $customer['email']; ?></td>
                  <td style="width:10%" class="text-left"><?php echo $customer['company_name']; ?></td>
                  <td class="text-left"><?php echo $customer['customer_group']; ?></td>
                  <td class="text-left"><?php echo $customer['status']; ?></td>
                  <td class="text-left"><?php echo $customer['need_review']; ?></td>
                  <td class="text-left"><?php echo $customer['has_license_image']; ?></td>
                  <td class="text-left"><?php echo $customer['telephone']; ?></td>
                  <td class="text-left"><?php echo $customer['logcenter']; ?></td>
                  <td class="text-left"><?php echo $customer['order_sum']; ?></td>
                  <td class="text-left"><?php echo $customer['is_shelf_customer']; ?></td>
                  <td class="text-left"><?php echo $customer['last_order_date']; ?></td>
                  <td class="text-left"><?php echo $customer['recommended_code']; ?></td>
                  <td class="text-left"><?php echo $customer['date_added']; ?></td>
                  <td class="text-right"><?php if ($customer['approve']) { ?>
                    <a href="<?php echo $customer['approve']; ?>" data-toggle="tooltip" title="<?php echo $button_approve; ?>" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i></a>
                    <?php } else { ?>
                    <button type="button" class="btn btn-success" disabled><i class="fa fa-thumbs-o-up"></i></button>
                    <?php } ?>
                    <div class="btn-group" data-toggle="tooltip" title="<?php echo $button_login; ?>">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="index.php?route=customer/customer/login&token=<?php echo $token; ?>&customer_id=<?php echo $customer['customer_id']; ?>&store_id=0" target="_blank"><?php echo $text_default; ?></a></li>
                        <?php foreach ($stores as $store) { ?>
                        <li><a href="index.php?route=customer/customer/login&token=<?php echo $token; ?>&customer_id=<?php echo $customer['customer_id']; ?>&store_id=<?php echo $store['store_id']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
                        <?php } ?>
                      </ul>
                    </div>
                    <?php if ($customer['unlock']) { ?>
                    <a href="<?php echo $customer['unlock']; ?>" data-toggle="tooltip" title="<?php echo $button_unlock; ?>" class="btn btn-warning"><i class="fa fa-unlock"></i></a>
                    <?php } else { ?>
                    <button type="button" class="btn btn-warning" disabled><i class="fa fa-unlock"></i></button>
                    <?php } ?>
                    <a href="<?php echo $customer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=customer/customer&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_email = $('input[name=\'filter_email\']').val();

	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

	var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

	if (filter_recommended_code) {
		url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
	}

  var filter_company_name = $('input[name=\'filter_company_name\']').val();

  if (filter_company_name) {
    url += '&filter_company_name=' + encodeURIComponent(filter_company_name);
  }

	var filter_customer_group_id = $('select[name=\'filter_customer_group_id\']').val();

	if (filter_customer_group_id != '*') {
		url += '&filter_customer_group_id=' + encodeURIComponent(filter_customer_group_id);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

  var filter_logcenter_id = $('select[name=\'filter_logcenter_id\']').val();

  if (filter_logcenter_id != '*') {
    url += '&filter_logcenter_id=' + encodeURIComponent(filter_logcenter_id);
  }

  <?php if(0) { ?>
	var filter_approved = $('select[name=\'filter_approved\']').val();

	if (filter_approved != '*') {
		url += '&filter_approved=' + encodeURIComponent(filter_approved);
	}
  <?php } ?>

  var filter_need_review = $('select[name=\'filter_need_review\']').val();

  if (filter_need_review != '*') {
    url += '&filter_need_review=' + encodeURIComponent(filter_need_review);
  }

  var filter_is_shelf_customer = $('select[name=\'filter_is_shelf_customer\']').val();

  if (filter_is_shelf_customer != '*') {
    url += '&filter_is_shelf_customer=' + encodeURIComponent(filter_is_shelf_customer);
  }
	var filter_telephone = $('input[name=\'filter_telephone\']').val();

	if (filter_telephone) {
		url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
	}

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

  var filter_order_sum = $('input[name=\'filter_order_sum\']').val();

  if(filter_order_sum){
    url += '&filter_order_sum=' + encodeURIComponent(filter_order_sum);
  }

  var filter_last_date_start = $('input[name=\'filter_last_date_start\']').val();

  if(filter_last_date_start){
    url += '&filter_last_date_start=' + encodeURIComponent(filter_last_date_start);
  }

  var filter_last_date_end = $('input[name=\'filter_last_date_end\']').val();

  if(filter_last_date_end){
    url += '&filter_last_date_end=' + encodeURIComponent(filter_last_date_end);
  }

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['fullname'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_email\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_email=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['email'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_email\']').val(item['label']);
	}
});

$('input[name=\'filter_company_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_company_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          if(item['company_name'] == 'null' || item['company_name'] == '') {
            return;
          }
          return {
            label: item['company_name'],
            value: item['customer_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_company_name\']').val(item['label']);
  }
});

$('input[name=\'filter_telephone\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_telephone=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['telephone'],
            value: item['customer_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_telephone\']').val(item['label']);
  }
});

//$('#button-download').on('click', function() {
  //url = 'index.php?route=customer/customer/export&token=<?php echo $token; ?>';
  // url += getFilterInfo();
  //location = url;
//});
<!--导出-->
$('#button-download').on('click', function() {
	url = 'index.php?route=customer/customer/export&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_email = $('input[name=\'filter_email\']').val();

	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

  var filter_company_name = $('input[name=\'filter_company_name\']').val();

  if (filter_company_name) {
    url += '&filter_company_name=' + encodeURIComponent(filter_company_name);
  }

	var filter_customer_group_id = $('select[name=\'filter_customer_group_id\']').val();

	if (filter_customer_group_id != '*') {
		url += '&filter_customer_group_id=' + encodeURIComponent(filter_customer_group_id);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

  var filter_logcenter_id = $('select[name=\'filter_logcenter_id\']').val();

  if (filter_logcenter_id != '*') {
    url += '&filter_logcenter_id=' + encodeURIComponent(filter_logcenter_id);
  }

  <?php if(0) { ?>
	var filter_approved = $('select[name=\'filter_approved\']').val();

	if (filter_approved != '*') {
		url += '&filter_approved=' + encodeURIComponent(filter_approved);
	}
  <?php } ?>


  var filter_need_review = $('select[name=\'filter_need_review\']').val();

  if (filter_need_review != '*') {
    url += '&filter_need_review=' + encodeURIComponent(filter_need_review);
  }

  var filter_is_shelf_customer = $('select[name=\'filter_is_shelf_customer\']').val();

  if (filter_is_shelf_customer != '*') {
    url += '&filter_is_shelf_customer=' + encodeURIComponent(filter_is_shelf_customer);
  }

	var filter_telephone = $('input[name=\'filter_telephone\']').val();

	if (filter_telephone) {
		url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
	}

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	location = url;
});
<!--导出-->
<!--导出用户每月购买订单数量和金额-->
$('#Month-buy').click(function(){
    var url = 'index.php?route=customer/customer/statistics&token=<?php echo $token; ?>';
     location = url;
});
<!--导出-->

//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>

<script type="text/javascript">

$('#button-upload').on('click', function() {
  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=sale/return_logcenter_order/importCustomer&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },
        success: function(json) {
          if (json['error']) {
            $('#form-order').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          if (json['success']) {
            $('#form-order').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('#form-order').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

$('#upload').on('click', function() {
  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=customer/customer/importcode&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            window.location.reload();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
</script>

<?php echo $footer; ?>
