<div class="table-responsive">
  <table class="table table-bordered table-hover bhz-table">
    <thead>
      <tr>
        <th width="20%" class="text-left">添加日期</th>
        <th width="20%" class="text-left">交易单据号</th>
        <th width="10%" class="text-left">交易类型</th>
        <th width="20%" class="text-right">交易金额</th>
        <th width="30%" class="text-center">状态</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($tradeOrders) { ?>
      <?php foreach ($tradeOrders as $to) { ?>
      <tr>
        <td class="text-left"><?php echo $to['date_added']; ?></td>
        <td class="text-left"><?php echo $to['order_id']; ?></td>
        <td class="text-left">订单</td>
        <td class="text-right"><?php echo $to['total']; ?> 元</td>
        <td class="text-center"><?php echo $to['status_name']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="99"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
