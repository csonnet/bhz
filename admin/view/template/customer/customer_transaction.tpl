<div class="table-responsive">
  <table class="table table-bordered table-hover bhz-table">
    <thead>
      <tr>
        <th width="20%" class="text-left">添加日期</th>
        <th width="10%" class="text-left">操作人</th>
        <th width="10%" class="text-left">操作人类型</th>
        <th width="10%" class="text-center">充值方式</th>
        <th width="20%" class="text-right">充值金额</th>
        <th width="10%" class="text-center">状态</th>
        <th width="20%" class="text-left">备注</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($rechargeOrders) { ?>
      <?php foreach ($rechargeOrders as $ro) { ?>
      <tr>
        <td class="text-left"><?php echo $ro['date_added']; ?></td>
        <td class="text-left"><?php echo $ro['runner']; ?></td>
        <td class="text-left"><?php echo $ro['rtype']; ?></td>
        <td class="text-center"><?php echo $ro['payment_method']; ?></td>
        <td class="text-right"><?php echo $ro['money']; ?> 元</td>
        <td class="text-center"><?php echo $ro['status_name']; ?></td>
        <td class="text-left"><?php echo $ro['memo']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="99"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
