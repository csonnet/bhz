<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <button class="btn btn-primary" ng-click="update_stock_out('<?php echo $out_id?>','<?php echo $token ?>')"><i class="fa fa-save"></i> 保存</button>
      <a class="btn btn-default" href="<?php echo $return_url?>"><i class="fa fa-reply"></i> 返回</a></div>
      <h1>编辑出库单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 编辑出库单</h3>
      </div>
      <div class="panel-body">
	
	  <form method="post" enctype="multipart/form-data" class="form-horizontal">
    <fieldset>
    <legend>基本信息</legend>
		<div class="form-group required">
        <label class="col-sm-2 control-label">销售单ID</label>
        <div class="col-sm-10">
        <input id="refer_id" type="text" value="<?php echo $refer_id?>" class="form-control" placeholder="销售单ID">
        <div class="{{referidError?'text-danger':''}}">{{referidError}}</div>
        </div>
    </div>

  	<div class="form-group required">
    <label class="col-sm-2 control-label">单据类型</label>
    <div class="col-sm-10">
      <select id="refer_type" name="" class="form-control">
      <option value="empty"></option>
      <?php foreach($refer_type_arr as $val){?>
      <option value="<?php echo $val['refer_type_id']?>" <?php echo $refer_type[$val['refer_type_id']]?>><?php echo $val['name']?></option>
      <?php }?>
      </select>
      <div class="{{ReferTypeError?'text-danger':''}}">{{ReferTypeError}}</div>
    </div>      
    </div>

    <div class="form-group required">
    <label class="col-sm-2 control-label">出货仓库</label>
    <div class="col-sm-10">
      <select id="warehouse" class="form-control">
      <option value="empty"></option>
      <?php foreach($warehouse_arr as $val){?>
      <option value="<?php echo $val['warehouse_id']?>" <?php echo $warehouse_id[$val['warehouse_id']]?>><?php echo $val['name']?></option>
      <?php }?>
      </select>
      <input disabled id="sale_money" value="<?php echo $sale_money?>" type="hidden" class="form-control" placeholder="0">
      <div class="{{WarehouseError?'text-danger':''}}">{{WarehouseError}}</div>
    </div>      
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">备注</label>
        <div class="col-sm-10">
        <textarea name="comment" class="form-control" id="comment" rows="8"><?php echo $comment?></textarea>
        </div>
    </div>

      	
        </fieldset>
        <fieldset>
        <legend>相关产品</legend>
          <table class="table ">
            <thead>
              <tr>
                <td>商品编码</td>
                <td>商品名称</td>
                <td>商品数量</td>
                <td>条形码</td>
                <td>商品单价</td>
                <td>操作</td>
              </tr>
            </thead>
            <tbody>
            <tr ng-repeat="product in products">
              <td class="{{product.codeError?'has-error':''}}">
                <div class="dropdown">
                  <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
                    <input type="text" ng-disabled="{{product.disabled}}" ng-change="getProductsByCode(product.product_code, '<?php echo $token ?>')" ng-model="product.product_code" class="form-control" placeholder="商品编码" aria-describedby="basic-addon2">
                  </div>
                  <div class="text-danger">{{product.codeError}}</div>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProductSecano(p,product)">
                      <div class="checkbox">
                        <label>
                           {{p.code}} {{p.pdname}}
                        </label>
                      </div>
                    </li>
                  </ul>
                </div>
              </td>
              <td class="{{product.nameError?'has-error':''}}">
                <div class="dropdown">              
                  <div  class="input-group" aria-expanded="true">
                              <text style="min-width: 300px" disabled ng-model="product.name" class="form-control" placeholder="商品名称" aria-describedby="basic-addon2">
                              {{product.name}}
                              </text>
                            </div>
                            <div class="text-danger">{{product.nameError}}</div>
                </div>
              </td>
              <td class="{{product.numError?'has-error':''}}">
                <div class="dropdown">              
                  <div  class="input-group" aria-expanded="true">
                              <input type="text" ng-change="caulatetotalano(product)" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"  ng-model="product.product_quantity" class="form-control" placeholder="商品数量" aria-describedby="basic-addon2">
                            </div>
                            <div class="text-danger">{{product.numError}}</div>
                </div>
              </td>
              <td class="{{product.skuError?'has-error':''}}">
                <div class="dropdown">              
                  <div  class="input-group" aria-expanded="true">
                              <input type="text" disabled name="product_sku" ng-model="product.sku" class="form-control" placeholder="条形码" aria-describedby="basic-addon2">
                            </div>
                            <div class="text-danger">{{product.skuError}}</div>
                </div>
              </td>
              <td class="{{product.priceError?'has-error':''}}">
                <div class="dropdown">              
                  <div  class="input-group" aria-expanded="true">
                              <input type="text" ng-disabled="{{product.disabled}}" name="product_price" ng-model="product.product_price" class="form-control" placeholder="商品单价" aria-describedby="basic-addon2">
                              <input type="hidden" ng-model="product.products_money" class="form-control" placeholder="商品总价" aria-describedby="basic-addon2">
                            </div>
                            <div class="text-danger">{{product.priceError}}</div>
                </div>
              </td>
              <td>
                        <button type="button" ng-click="removeproductano($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                      </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
                  <td colspan="5"></td>
                  <td ><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
          </tfoot>
            </table>
            </fieldset>
	  </form>

	  <div class="row">
        </div>
      </div>
      </div>
      <!--历史操作-->
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-comment-o"></i> 出库单历史</h3>
        </div>
        <div class="panel-body">
          <div class="tab-content">
                
            <table class="table table-bordered bhz-table">
                <thead>
                  <tr>
                          <th class="text-left">添加时间</th>
                          <th class="text-left">操作员</th>
                          <th class="text-left">操作内容</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($histories) { ?>
                        <?php foreach ($histories as $history) { ?>
                        <tr>
                          <td class="text-left"><?php echo $history['date_added']; ?></td>
                          <td class="text-left"><?php echo $history['operator_name']; ?></td>
                          <td class="text-left"><?php echo $history['comment']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td class="text-center" colspan="99">无记录</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
              
            </div>
        </div>
      </div><!--列表-->
  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--

  $('.date').datetimepicker({
	  pickDate: true,
    pickTime: true
	});

  		$(function(){

  			var appElement = document.querySelector('[ng-controller=bhzAdminCtrl]');

	  		var $scope = angular.element(appElement).scope();

        $scope.getdetailproducts('<?php echo $out_id?>','<?php echo $token?>');

  		})




  //--></script>
<?php echo $footer; ?>