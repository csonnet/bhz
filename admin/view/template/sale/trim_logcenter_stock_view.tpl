<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!-- <a href="<?php echo URL('sale/purchase_order/export', 'token='.$token.'&ro_id='.$trim['id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a> -->
      </div>
      <h1>库存调整单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default margin-bottom-40">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 库存调整单</h3>
      </div>
      <div class="panel-body">
        <dl class="dl-horizontal col-xs-6">
          <dt>编号</dt>
          <dd><?php echo $trim['id'] ?></dd>
          <dt>创建者</dt>
          <dd><?php echo $trim['creator']['fullname']?></dd>
          <dt>创建时间</dt>
          <dd><?php echo $trim['date_added']?></dd>
          
          <dt>状态</dt>
          <dd class="ro_status"><?php echo getRoStatus()[$trim['status']]?></dd>

          <br>

          <dt>包含订单号</dt>
          <?php foreach($included_orders as $order_id) { ?>
          <dd><?php echo $order_id?></dd>
          <?php } ?>
        </dl>

        <dl class="dl-horizontal col-xs-6">
          <!-- <dt>供应商</dt>
          <dd><?php echo $trim['vendor']['vendor_name'].'  '.$trim['vendor']['firstname']?></dd>
          <dt>供应商联系电话</dt>
          <dd><?php echo $trim['vendor']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $trim['vendor']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $trim['vendor']['address_1'] ?></dd>

          <br>
 -->
          <dt>物流中心</dt>
          <dd><?php echo $trim['logcenter_info']['logcenter_name']?></dd>
          <dt>物流中心联系电话</dt>
          <dd><?php echo $trim['logcenter_info']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $trim['logcenter_info']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $trim['logcenter_info']['address_1'] ?></dd>

        </dl>
      </div>
      <table class="table">
        <thead>
          <tr>
            <td>商品</td>
            <td>选项</td>
            <td>箱入数</td>
            <td>数量</td>
            <td>调整库存原因</td>
            <td>备注</td>
            <td>操作数量</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($trim_products as $key => $product) { ?>
            <tr>
              <td><?php echo $product['name'] ?></td>
              <td><?php echo $product['option_name'] ?></td>
              <td><?php echo $product['packing_no'] ?></td>
              <td><?php echo $product['qty'] ?></td>
              <td><?php echo getTrimStatus()[$product['trim_status']] ; ?></td>
              <td><input type="text" class="form-control" ng-init="trims[<?php echo $product['id']?>].comment='<?php echo $product['comment'] ?>'" ng-model="trims[<?php echo $product['id']?>].comment"></td>
              <td>
                  <input type="number"  ng-model="trims[<?php echo $product['id']?>].stock" class="form-control" placeholder="数量" >
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php if ($need_logcenter_confirm) { ?>
      <button class="btn btn-danger pull-right" ng-click="trim_logcenter_stock('<?php echo $trim['id']?>', '<?php echo $token ?>')">调整数量</button>
      <?php } ?>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> 库存调整单历史</h3>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">添加时间</td>
                <td class="text-left">操作员</td>
                <td class="text-left">操作内容</td>
              </tr>
            </thead>
            <tbody>
              <?php if ($trim_histories) { ?>
              <?php foreach ($trim_histories as $history) { ?>
              <tr>
                <td class="text-left"><?php echo $history['date_added']; ?></td>
                <td class="text-left"><?php echo $history['operator_name']; ?></td>
                <td class="text-left"><?php echo $history['comment']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4">无记录</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php if ($need_logcenter_confirm) { ?>
          <button class="btn btn-danger pull-right" ng-click="trim_logcenter_confirm_stock('<?php echo $trim['id']?>', '<?php echo $token ?>')">确认库存调整</button>
          <?php } ?>
        </div>
      </div>
    </div>

  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<?php echo $footer; ?>
