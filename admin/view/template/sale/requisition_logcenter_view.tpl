<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!-- <a href="<?php echo URL('sale/purchase_order/export', 'token='.$token.'&ro_id='.$requisition['id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a> -->
      </div>
      <h1>调拨单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 调拨单</h3>
      </div>
      <div class="panel-body">
        <dl class="dl-horizontal col-xs-6">
          <dt>编号</dt>
          <dd><?php echo $requisition['id'] ?></dd>
          <dt>创建者</dt>
          <dd><?php echo $requisition['creator']['fullname']?></dd>
          <dt>创建时间</dt>
          <dd><?php echo $requisition['date_added']?></dd>
          
          <dt>状态</dt>
          <dd class="ro_status"><?php echo getRequisitionStatus()[$requisition['status']]?></dd>

          <br>

          <dt>包含订单号</dt>
          <?php foreach($included_orders as $order_id) { ?>
          <dd><?php echo $order_id?></dd>
          <?php } ?>
        </dl>

        <dl class="dl-horizontal col-xs-6">
          <!-- <dt>供应商</dt>
          <dd><?php echo $requisition['vendor']['vendor_name'].'  '.$requisition['vendor']['firstname']?></dd>
          <dt>供应商联系电话</dt>
          <dd><?php echo $requisition['vendor']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $requisition['vendor']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $requisition['vendor']['address_1'] ?></dd>

          <br> -->
          <dt>目的物流中心</dt>

          <dd><?php echo $requisition['des_logcenter_info']['logcenter_name']?></dd>
          <dt>物流中心联系电话</dt>
          <dd><?php echo $requisition['des_logcenter_info']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $requisition['des_logcenter_info']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $requisition['des_logcenter_info']['address_1'] ?></dd>
            <br>

          <dt>发货物流中心</dt>

          <dd><?php echo $requisition['src_logcenter_info']['logcenter_name']?></dd>
          <dt>物流中心联系电话</dt>
          <dd><?php echo $requisition['src_logcenter_info']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $requisition['src_logcenter_info']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $requisition['src_logcenter_info']['address_1'] ?></dd>

        </dl>
      </div>
      <table class="table">
        <thead>
          <tr>
            <td>商品</td>
            <td>选项</td>
            <td>箱入数</td>
            <td>数量</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($requisition_products as $key => $product) {?>
            <tr>
              <td><?php echo $product['name'] ?></td>
              <td><?php echo $product['option_name'] ?></td>
              <td><?php echo $product['packing_no'] ?></td>
              <td><?php echo $product['qty'] ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <div class="panel panel-default" style="margin-top:50px;">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> 调拨单历史</h3>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">添加时间</td>
                <td class="text-left">操作员</td>
                <td class="text-left">操作内容</td>
              </tr>
            </thead>
            <tbody>
              <?php if ($requisition_histories) { ?>
              <?php foreach ($requisition_histories as $history) { ?>
              <tr>
                <td class="text-left"><?php echo $history['date_added']; ?></td>
                <td class="text-left"><?php echo $history['operator_name']; ?></td>
                <td class="text-left"><?php echo $history['comment']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4">无记录</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php if (!empty($need_logcenter_confirm)) { ?>
          <button class="btn btn-danger pull-right" ng-click="requisition_logcenter_confirm('<?php echo $requisition['id']?>', '<?php echo $token ?>')">确认调拨</button>
          <?php } ?>
          <?php if (!empty($need_logcenter_accept)) { ?>
          <button class="btn btn-danger pull-right" ng-click="requisition_logcenter_accept('<?php echo $requisition['id']?>', '<?php echo $token ?>')">确认全部收货</button>
          <?php } ?>
        </div>
      </div>
    </div>

  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<?php echo $footer; ?>
