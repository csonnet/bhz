<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<div class="form-group col-sm-6">
					<a href="<?php echo URL('sale/return_main_order/add', 'token='.$token) ?>" data-toggle="tooltip" title="添加" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				</div>
			</div>
			<h1>退货单</h1>
			<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
    
    	<?php if ($error_warning) { ?>
    	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
        
		<div class="panel panel-default">
        
			<div class="panel-heading">
 				<h3 class="panel-title"><i class="fa fa-list"></i> 退货单</h3>
			</div>
            
			<div class="panel-body">
            	
                <div class="well">
                    <div class="row">
                    
                        <div class="col-sm-4">
                        
                            <div class="form-group">
                                <label class="control-label" for="input-date_start">创建开始时间</label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start" value="<?php echo $filter_date_start;?>" placeholder="搜索创建开始时间" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-company-name">退货超市</label>
                                <input type="text" name="filter_company_name" value="<?php echo $filter_company_name;?>" placeholder="退货超市" id="input-company-name" class="form-control" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-date_end">创建结束时间</label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索创建结束时间" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-telephone">客户电话</label>
                                    <input type="text" name="filter_telephone" value="<?php echo $filter_telephone;?>" placeholder="客户电话" id="input-telephone" class="form-control" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-status">状态</label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <?php  foreach ($status_array as $key => $value) {?>
                                    <option value="<?php echo $key; ?>" <?php if($filter_status==$key){echo 'selected="selected"'; } ?> ><?php echo $value ; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-return-warehouse">退货仓库</label>
                                <select name="filter_return_warehouse" id="return-warehouse" class="form-control">
                                    <option value="0">全部</option>
                                    <?php foreach ($warehouses as $value) { ?>
                                    <option value="<?php echo $value['warehouse_id']; ?>" <?php if ($value["warehouse_id"] == $filter_return_warehouse) { echo "selected='selected'"; }?> ><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <a id="button-export-tpl" data-toggle="tooltip" title="导出模板" class="btn btn-primary"><i class="fa fa-calculator"></i> 导出模板</a>
                                <a id="button-upload" data-toggle="tooltip" title="导入退货单" class="btn btn-primary"><i class="fa fa-calculator"></i> 导入退货单</a>
                                <a id="button-export-lists" data-toggle="tooltip" title="导出对账单" class="btn btn-primary"><i class="fa fa-calculator"></i> 导出退货单</a>
                                <button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i>筛选</button>
                            </div>
                        </div>
                        
                      </div>
            	</div>
                
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
           		<div class="table-responsive">
                
       				<table class="table table-bordered table-hover bhz-table">
                 		<thead>
                 			<tr>
                            	<th>编号</th>
                                <th>创建者</th>
                  				<th>创建时间</th>
                    			<th>退货仓库</th>
                  				<th>商品品种</th>
                  				<th>退货超市</th>
                   				<th>包含订单号</th>
                      			<th>退货单状态</th>
                    			<th>操作</th>
                			</tr>
                  		</thead>
                      	<tbody>
                 			<?php if ($ro_list) { ?>
                        	<?php foreach ($ro_list as $ro) { ?>
                    		<tr>
                 				<td><?php echo $ro['id'] ?></td>
                                <td><?php echo $ro['user_name'] ?></td>
                 				<td><?php echo $ro['date_added'] ?></td>
                                <td><?php echo $ro['warehouse_name'] ?></td>
                                <td><?php echo $ro['count'] ?></td>
                                <td><?php echo $ro['company_name'].'<br />'.$ro['telephone']; ?></td>
                                <td><?php echo $ro['included_order_ids'] ?></td>
                                <td class="ro_status" style="color:<?php echo $ro['color'] ?>"><?php echo getRoStatus()[$ro['status']]?></td>
                                <td>
                                    <a href="<?php echo URL('sale/return_main_order/view', 'token='.$token.'&ro_id='.$ro['id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                    <?php if($ro['status'] < 3){?>
                                    <button type="button" value="<?php echo $ro['id']; ?>" id="button-delete<?php echo $ro['ro_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="99">没有退货单数据</td>
                            </tr>
                            <?php } ?>
                        </tbody>
            		</table>
                
          		</div>
        		</form>
                
                <div class="row">
              		<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      				<div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
                
      		</div>
              
		</div>
        
	</div>
    
</div>

<script type="text/javascript"><!--
$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=sale/return_main_order/delete&token=<?php echo $token; ?>&ro_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          window.location.reload();
        }
        
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

	$('#button-filter').on('click', function() {
		
		url = 'index.php?route=sale/return_main_order&token=<?php echo $token; ?>';

		var filter_date_start = $('input[name=\'filter_date_start\']').val();
		if (filter_date_start) {
			url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
		}
		
		var filter_date_end = $('input[name=\'filter_date_end\']').val();
		if (filter_date_end) {
			url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
		}
		
		var filter_status = $('select[name=\'filter_status\']').val();
		if (filter_status) {
			url += '&filter_status=' + encodeURIComponent(filter_status);
		}

		var filter_company_name = $('input[name=\'filter_company_name\']').val();
		if (filter_company_name) {
			url += '&filter_company_name=' + encodeURIComponent(filter_company_name);
		}

		var filter_telephone = $('input[name=\'filter_telephone\']').val();
		if (filter_telephone) {
			url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
		}

        var filter_return_warehouse = $('select[name=\'filter_return_warehouse\']').val();
		if (filter_return_warehouse!=0) {
			url += '&filter_return_warehouse=' + encodeURIComponent(filter_return_warehouse);
		}

        location = url;
		
	});

$('.date').datetimepicker({
  pickTime: false
});


$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});

$('#button-export-tpl').on('click', function(e) {
	url = 'index.php?route=sale/return_logcenter_order/exportTpl&token=<?php echo $token; ?>';
	location = url;
});

$('#button-upload').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);   
      
      $.ajax({
        url: 'index.php?route=sale/return_logcenter_order/import&token=<?php echo $token; ?>',
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
                
          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            window.location.reload();
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

$('#button-vendor-bill').on('click', function(e) {
  var vendor_bill_month = $('#vendor_bill_month').val();
  if(!vendor_bill_month || isNaN(Date.parse(vendor_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/main_purchase_order/genVendorBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+vendor_bill_month;
});
$('#button-export-lists').on('click', function(e) {
  url = 'index.php?route=sale/return_main_order/exportLists&token=<?php echo $token; ?>';
  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_logcenter = $('input[name=\'filter_logcenter\']').val();

  if (filter_logcenter) {
    url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
  }

  // var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  // if (filter_vendor_name) {
  //   url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  // }

  location = url;
});
//--></script>
<?php echo $footer; ?>
