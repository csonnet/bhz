<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <?php if(isset($export)) { ?>
        <a href="<?php echo $export; ?>" data-toggle="tooltip" title="导出" class="btn btn-warning"><i class="fa fa-download"></i> 导出获奖名单</a>
        <?php } ?>
        <button ng-click="save_luckydraw('<?php echo $token ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> 保存</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-luckydraw-name">幸运抽奖名称</label>
              <div class="col-sm-10">
                <input type="text" name="luckydraw_name" ng-model="luckydraw_name" value="<?php echo $luckydraw_name; ?>" placeholder="幸运抽奖名称" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_luckydraw_name) { ?>
                <div class="text-danger"><?php echo $error_luckydraw_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-chance">中奖概率(0-100)</label>
              <div class="col-sm-10">
                <input type="text" name="chance" ng-model="chance" value="<?php echo $chance; ?>" placeholder="中奖概率" id="input-chance" class="form-control" />
                <?php if ($error_chance) { ?>
                <div class="text-danger"><?php echo $error_chance; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">状态</label>
              <div class="col-sm-10">
                <select name="status" ng-model="status" id="input-status" class="form-control">
                  <?php foreach ($statuses as $status_id=>$status_name) { ?>
                  <?php if (!is_null($status) && $status_id == $status) { ?>
                  <option value="<?php echo $status_id; ?>" selected="selected"><?php echo $status_name; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $status_id; ?>"><?php echo $status_name; ?></option>
                  <?php } ?>
                  <?php } ?>                
                </select>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-start-time">开始时间</label>
              <div class="col-sm-10">
                <div class="">
                  <input type="text" name="start_time" datetimez ng-model="start_time" value="<?php echo $start_time; ?>" placeholder="开始时间" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-start-time" class="form-control" />
                  <?php if ($error_start_time) { ?>
                  <div class="text-danger"><?php echo $error_start_time; ?></div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-end-time">结束时间</label>
              <div class="col-sm-10">
                <div class="">
                  <input type="text" name="end_time" datetimez ng-model="end_time" value="<?php echo $end_time; ?>" placeholder="结束时间" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-end-time" class="form-control" />
                  <?php if ($error_end_time) { ?>
                  <div class="text-danger"><?php echo $error_end_time; ?></div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend>奖品设置</legend>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left">奖项名</td>
                  <td class="text-left">奖品名</td>
                  <td class="text-left">排序</td>
                  <td class="text-left">已中奖</td>
                  <td class="text-left">总数</td>
                  <td class="text-left">操作</td>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="product in products">
                  <td class="{{product.priceNameError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.price_name" class="form-control" placeholder="奖项名">
                    </div>
                    <div class="text-danger">{{product.priceNameError}}</div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.product_name" class="form-control" placeholder="奖品名">
                    </div>
                    <div class="text-danger">{{product.productNameError}}</div>
                  </td>
                  <td class="{{product.sortOrderError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.sort_order" class="form-control" placeholder="排序">
                    </div>
                    <div class="text-danger">{{product.sortOrderError}}</div>
                  </td>
                  <td>
                    <div>{{product.hitted_number}}</div>
                  </td>
                  <td class="{{product.quantityError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.quantity" class="form-control" placeholder="总数">
                    </div>
                    <div class="text-danger">{{product.quantityError}}</div>
                  </td>
                  <td>
                    <button ng-show="!is_edit" type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5"></td>
                  <td><button ng-show="!is_edit" type="button" ng-click="products.push({quantity:1, sort_order:10});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>
          <fieldset>
            <legend>抽奖次数设置</legend>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left">订单金额</td>
                  <td class="text-left">抽奖次数</td>
                  <td class="text-left">操作</td>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="number in numbers">
                  <td class="{{number.amountError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="number.amount" class="form-control" placeholder="订单金额">
                    </div>
                    <div class="text-danger">{{number.amountError}}</div>
                  </td>
                  <td class="{{number.numberError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="number.number" class="form-control" placeholder="抽奖次数">
                    </div>
                    <div class="text-danger">{{number.numberError}}</div>
                  </td>
                  <td>
                    <button type="button" ng-click="removeNumber($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td ><button type="button" ng-click="numbers.push({number:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加次数"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--

$('#luckydraw-products').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 

  <script type="text/javascript"><!--
  <?php if(!empty($luckydraw_info)) { ?>
    var luckydraw_info = <?php echo json_encode($luckydraw_info);?>;
    var is_edit = true;
  <?php } else { ?>
    var luckydraw_info = {};
    var is_edit = false;
  <?php } ?>
// $('.date').datetimepicker({
//   pickTime: false
// });

// $('.time').datetimepicker({
//   pickDate: false
// });

// $('.datetime').datetimepicker({
//   pickDate: true,
//   pickTime: true
// });
//--></script>       
<?php echo $footer; ?> 