<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
			</div>
			<h1>退货单</h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
 				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
    
	<div class="container-fluid" ng-controller="lycCtrl">
    	
        <?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
        
		<div class="panel panel-default">
        
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> 退货单</h3>
			</div>
            
			<div class="panel-body">
				
                <dl class="dl-horizontal col-xs-6">
					<dt>编号</dt>
					<dd><?php echo $ro['id'] ?></dd>
					<dt>创建者</dt>
 					<dd><?php echo $ro['creator']['fullname']?></dd>
					<dt>创建时间</dt>
					<dd><?php echo $ro['date_added']?></dd>
					<dt>状态</dt>
  					<dd class="ro_status"><?php echo getRoStatus()[$ro['status']]?></dd>
                    
                     <br>

					<dt>包含订单号</dt>
					<?php foreach($included_orders as $order_id) { ?>
					<dd><?php echo $order_id?></dd>
					<?php } ?>
			</dl>

        	<dl class="dl-horizontal col-xs-6">
          		<dt>退货超市</dt>
          		<dd><?php echo $ro['market_name']?></dd>
          		<dt>入库仓库</dt>
          		<dd><?php echo $ro['warehouse_info']['name']?></dd>
          		<dt>仓库管理员</dt>
          		<dd><?php echo $ro['warehouse_info']['manager'] ?></dd>
          		<dt>联系电话</dt>
          		<dd><?php echo $ro['warehouse_info']['contact_tel'] ?></dd>
          		<dt>地址</dt>
         		<dd><?php echo $ro['warehouse_info']['address'] ?></dd>
			</dl>
    
		</div>
        
		<table class="table">
        	<thead>
    			<tr>
     				<td width="25%">商品</td>
                    <td>商品编码</td>
                    <td>条形码</td> 
                    <td>选项</td>
                    <td width="10%" class="text-center">箱入数</td>
                    <td width="10%" class="text-center">数量</td>
                    <td>修改数量</td>
            	</tr>
			</thead>
        	<tbody>
          		<?php foreach ($ro_products as $key => $product) {?>
            	<tr>
              		<td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['product_code'] ?></td>
                    <td><?php echo $product['sku'] ?></td>
              		<td><?php echo $product['option_name'] ?></td>
              		<td class="text-center"><?php echo $product['packing_no'] ?></td>
              		<td class="text-center"><?php echo $product['qty'] ?></td>
              		<td>
                  		<input type="number"  ng-model="returns[<?php echo $product['id']?>].stock" class="form-control" placeholder="数量" >
              		</td>
            	</tr>
          		<?php } ?>
        	</tbody>
      	</table>
      
		<button class="btn btn-danger pull-right" ng-click="change_return_quantity('<?php echo $ro['id']?>', '<?php echo $token ?>')">调整数量</button>
        
    </div>
    
    <br />
    <br />
    
    <!--历史操作-->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-comment-o"></i> 退货单历史</h3>
		</div>
		<div class="panel-body">
			<div class="tab-content">
            
				<table class="table table-bordered bhz-table">
  					<thead>
   						<tr>
                			<th class="text-left">添加时间</th>
                			<th class="text-left">操作员</th>
                			<th class="text-left">操作内容</th>
              			</tr>
            		</thead>
            		<tbody>
              			<?php if ($ro_histories) { ?>
              			<?php foreach ($ro_histories as $history) { ?>
              			<tr>
                			<td class="text-left"><?php echo $history['date_added']; ?></td>
                			<td class="text-left"><?php echo $history['operator_name']; ?></td>
                			<td class="text-left"><?php echo $history['comment']; ?></td>
              			</tr>
              			<?php } ?>
              			<?php } else { ?>
              			<tr>
                			<td class="text-center" colspan="99">无记录</td>
              			</tr>
              			<?php } ?>
            		</tbody>
          		</table>
          
  			</div>
		</div>
	</div>
    <!--历史操作-->
    
    <!--相关单据-->
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-arrows-h"></i> 相关入库单</h3>
		</div>
		<div class="panel-body">
			<div class="tab-content">
            
				<table class="table table-bordered bhz-table">
  					<thead>
   						<tr>
                			<th class="text-left">入库单号</th>
                			<th class="text-left">入库仓库</th>
                            <th class="text-left">操作人</th>
                            <th class="text-left">单据状态</th>
                            <th class="text-left">创建时间</th>
              			</tr>
            		</thead>
            		<tbody>
              			<?php if ($ro_stockin) { ?>
              			<?php foreach ($ro_stockin as $stockin) { ?>
              			<tr>
                			<td class="text-left"><?php echo $stockin['in_id']; ?></td>
                			<td class="text-left"><?php echo $stockin['warehouse_name']; ?></td>
                			<td class="text-left"><?php echo $stockin['user_name']; ?></td>
                            <td class="text-left"><?php echo $stockin['status_name']; ?></td>
                            <td class="text-left"><?php echo $stockin['date_added']; ?></td>
              			</tr>
              			<?php } ?>
              			<?php } else { ?>
              			<tr>
                			<td class="text-center" colspan="99">无关联单据</td>
              			</tr>
              			<?php } ?>
            		</tbody>
          		</table>
          
  			</div>
		</div>
	</div>
    <!--相关单据-->
    
    <?php if (!empty($need_warehouse)) { ?>
	<button class="btn btn-danger pull-right" ng-click="return_confirm('<?php echo $ro['id']?>', '<?php echo $token ?>')">提交审批</button>
	<?php } ?>
    
    <?php if (!empty($need_allow)) { ?>
	<button class="btn btn-danger pull-right" ng-click="return_confirm('<?php echo $ro['id']?>', '<?php echo $token ?>')">审批通过</button>
	<?php } ?>
    
    <?php if (!empty($need_finish)) { ?>
	<button class="btn btn-danger pull-right" ng-click="return_confirm('<?php echo $ro['id']?>', '<?php echo $token ?>')">确认完成</button>
	<?php } ?>
    
    <?php if (!empty($need_stockin)) { ?>
    <button class="btn btn-success pull-right" style="margin-right:5px;" ng-click="create_return_stock('<?php echo $ro['id']?>', '<?php echo $token ?>')">生成入库单</button>
    <?php } else { ?>
    <button class="btn disabled pull-right" style="margin-right:5px;">已生成入库单</button>
    <?php } ?>
    
  </div>
  
</div>

<?php echo $footer; ?>
