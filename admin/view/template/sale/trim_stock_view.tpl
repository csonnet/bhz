<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
			</div>
			<h1>损益单</h1>
      		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
        		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        		<?php } ?>
			</ul>
		</div>
	</div>
        
	<div class="container-fluid">
    
    	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
        
 		<div class="panel panel-default margin-bottom-40" ng-controller="lycCtrl">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> 损益单</h3>
			</div>
 			
            <div class="panel-body">
				<dl class="dl-horizontal col-xs-6">
					<dt>编号</dt>
					<dd><?php echo $trim['id'] ?></dd>
					<dt>创建者</dt>
  					<dd><?php echo $trim['creator']['fullname']?></dd>
					<dt>创建时间</dt>
					<dd><?php echo $trim['date_added']?></dd>
                    <dt>状态</dt>
          			<dd class="ro_status"><?php echo getTrimOrderStatus()[$trim['status']]?></dd>
                    
                    <br>

					<dt>包含订单号</dt>
					<?php foreach($included_orders as $order_id) { ?>
					<dd><?php echo $order_id?></dd>
					<?php } ?>
				</dl>

				<dl class="dl-horizontal col-xs-6">
                	<dt>仓库</dt>
          			<dd><?php echo $trim['warehouse_info']['name']?></dd>
                    <dt>仓库管理员</dt>
          			<dd><?php echo $trim['warehouse_info']['manager'] ?></dd>
          			<dt>联系电话</dt>
          			<dd><?php echo $trim['warehouse_info']['contact_tel'] ?></dd>
          			<dt>地址</dt>
					<dd><?php echo $trim['warehouse_info']['address'] ?></dd>
 				</dl>
			</div>
            
			<table class="table">
            <thead>
              <tr>
                <td>商品</td>
                <td>选项</td>
                <td>箱入数</td>
                <td>数量</td>
                <td>调整库存原因</td>
                <td>备注</td>
                <td>操作数量</td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($trim_products as $key => $product) { ?>
                <tr>
                  <td><?php echo $product['name'] ?></td>
                  <td><?php echo $product['option_name'] ?></td>
                  <td><?php echo $product['packing_no'] ?></td>
                  <td><?php echo $product['qty'] ?></td>
                  <td><?php echo getTrimStatus()[$product['trim_status']] ; ?></td>
                  <td><input type="text" class="form-control" ng-init="trims[<?php echo $product['id']?>].comment='<?php echo $product['comment']; ?>'" ng-model="trims[<?php echo $product['id']?>].comment"></td>
                  <td>
                      <input type="number"  ng-model="trims[<?php echo $product['id']?>].stock" class="form-control" placeholder="数量" >
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          	
            <?php if ($need_logcenter_confirm) { ?>
			<button class="btn btn-danger pull-right" ng-click="change_trim_quantity(1,'<?php echo $trim['id']?>', '<?php echo $token ?>')">调整数量</button>
			<?php } ?>
            
            <?php if ($need_main_allow) { ?>
			<button class="btn btn-danger pull-right" ng-click="change_trim_quantity(2,'<?php echo $trim['id']?>', '<?php echo $token ?>')">调整数量</button>
			<?php } ?>
            
		</div>

		<div class="panel panel-default" ng-controller="lycCtrl">
        
			<div class="panel-heading">
            	<h3 class="panel-title"><i class="fa fa-comment-o"></i> 损益单历史</h3>
          	</div>
          
			<div class="panel-body">
				<div class="tab-content">
                
					<table class="table table-bordered">
                <thead>
                  <tr>
                    <td class="text-left">添加时间</td>
                    <td class="text-left">操作员</td>
                    <td class="text-left">操作内容</td>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($trim_histories) { ?>
                  <?php foreach ($trim_histories as $history) { ?>
                  <tr>
                    <td class="text-left"><?php echo $history['date_added']; ?></td>
                    <td class="text-left"><?php echo $history['operator_name']; ?></td>
                    <td class="text-left"><?php echo $history['comment']; ?></td>
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="4">无记录</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              		
          			<?php if ($need_logcenter_confirm) { ?>
          			<button class="btn btn-danger pull-right" ng-click="confirm_trim(1,'<?php echo $trim['id']?>', '<?php echo $token ?>')">物流中心确认调整</button>
          			<?php } ?>
                    
          			<?php if ($need_main_finsh) { ?>
          			<button class="btn btn-danger pull-right" ng-click="confirm_trim(2,'<?php echo $trim['id']?>', '<?php echo $token ?>')">总后台确认调整</button>
          			<?php } ?>
                    
				</div>
			</div>
      
		</div>
    
	</div>
</div>

<?php echo $footer; ?>
