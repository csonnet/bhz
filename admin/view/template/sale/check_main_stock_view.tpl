<div style="width: 79.5%;margin-left: 256px;" class="panel panel-default">
  <table class="table">
    <thead>
      <tr>
        <td>条形码</td>
        <td>商品名称</td>
        <td>选项</td>
        <td>期初数</td>
        <td>采购数量</td>
        <td>销售数量</td>
        <td>退货数量</td>
        <td>调进数量</td>
        <td>调出数量</td>
        <td>调整数量</td>
        <td>库存变动</td>
        <td>实时库存</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($check_stock as $key => $product) {?>
        <tr>
          <td><?php echo $product['sku'] ?></td>
          <td><?php echo $product['name'] ?></td>
          <td><?php echo $product['option_name'] ?></td>
          <td><?php echo $product['initiai'] ?></td>
          <td><?php echo $product['po'] ?></td>
          <td><?php echo $product['order'] ?></td>
          <td><?php echo $product['ro'] ?></td>
          <td><?php echo $product['des_requisition'] ?></td>
          <td><?php echo $product['src_requisition'] ?></td>
          <td><?php echo $product['trim'] ?></td>
          <td><?php echo $product['change'] ?></td>
          <td><?php echo $product['balance'] ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

</div>

