<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_title; ?></h3>
        <a href="<?php echo $show_list; ?>"  data-toggle="tooltip"  class="btn btn-primary pull-right">查看记录</a>
      </div>

      <div class="panel-body">
        <div class="well">
          <div class="row">
              <div class="form-group">
                <form action="<?php echo $remit_url; ?>" method="post" id="form_remit_money">    
                     <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th class="text-left">收款人</th>
                          <th class="text-left">账户类型</th>
                          <th class="text-left">卡号</th>
                          <th class="text-left">银行名称</th>
                          <th class="text-left">部门</th>
                          <th class="text-left">金额</th>
                          <th class="text-left">备注</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-left">
                            <input type="text" placeholder="收款人" name="employ_name" class="form-control">
                            <input type="hidden" name="employ_id">
                          </td>
                          <td class="text-left">
                          <?php
                          $c = 0;
                          foreach ($typeList as $typeId=>$typeName) {
                            echo '<label for="cart_type_'.$typeId.'"><input type="radio" name="card_type" value="'.$typeId.'" id="cart_type_'.$typeId.'"';
                            if (0 == $c){
                                echo ' checked="checked"';
                            }
                            echo '>'.$typeName.'</label><br />';
                            $c++;
                          }
                          ?>
                          </td>
                          <td class="text-left">
                            <input type="text" placeholder="卡号" name="card_num" class="form-control">
                          </td>
                          <td class="text-left">
                            <input type="text" placeholder="银行名称" name="bank_name" class="form-control">
                          </td>
                          <td class="text-left">
                            <input type="text" placeholder="部门" name="section" class="form-control">
                          </td>
                          <td class="text-left">
                            <input type="text" placeholder="金额" name="amount" class="form-control">
                          </td>
                          <td class="text-left">
                            <input type="text" placeholder="备注" name="comment" class="form-control">
                          </td>
                        </tr>
                      </tbody>
                    </table>  
                </form>
                <a class="hidden" href="#" id="gotoyjf" target="_blank"></a>
              </div>
            </div>
        </div>
         <button type="button" class="pull-right btn btn-primary" id="remit-confirm">确认打款</button>    
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$("#remit-confirm").click(function(){
    if($("input[name='employ_name']").val()==''){
        alert('请选择员工姓名');
        return false;
    }
    if($("input[name='amount']").val()==''){
        alert('请输入金额');
        return false;
    }

    if (confirm('确定信息正确并进行打款么？')){
        $("#remit-confirm").attr('disabled','disabled');
        $.ajax({
            url: 'index.php?route=sale/remit_money/remit&token=<?php echo $token; ?>',
            type: 'post',
            dataType: 'json',
            data: 'employ_name='+$("input[name='employ_name']").val()+'&card_type='+$('input[name="card_type"]:checked').val()+'&card_num='+$("input[name='card_num']").val()+'&bank_name='+$("input[name='bank_name']").val()+'&section='+$("input[name='section']").val()+'&comment='+$("input[name='comment']").val()+'&amount='+$("input[name='amount']").val()+'&employ_id='+$('input[name=\'employ_id\']').val(),
            success: function(json) {
                $("#remit-confirm").removeAttr('disabled');
                if (json['success']){
                    var a = document.getElementById('gotoyjf');
                    a.href = json['redirect'];
                    a.click();
                }else{
                    alert(json['error']);
                }
            }
        });
    }
});

$('input[name=\'employ_name\']').autocomplete({
    'source': function(request, response) {
        $.ajax({
            url: 'index.php?route=sale/remit_money/autoComplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item['employ_name'],
                        value: item['employ_id'],
                        card_num: item['card_num'],
                        section: item['section'],
                        bank_name: item['bank_name'],
                        type: item['type'],
                    }
                }));
            }
        });
    },
    'select': function(item) {
        $('input[name="card_type"]').prop('checked', false);
        $('input[name=\'employ_name\']').val(item['label']);
        $('input[name=\'employ_id\']').val(item['value']);
        $('input[name="card_type"][value="'+item['type']+'"]').prop('checked', true);
        $('input[name=\'card_num\']').val(item['card_num']);
        $('input[name=\'section\']').val(item['section']);
        $('input[name=\'bank_name\']').val(item['bank_name']);
    }
});

//--></script></div>
<?php echo $footer; ?>