<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!-- <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button> -->
        <?php if($can_add_po){ ?>
<!--
        <a href="<?php echo URL('sale/purchase_order/autoPurchaseOrder', 'token='.$token) ?>" id="button-enable" form="form-product"  data-toggle="tooltip" title="批量启用产品" class="btn btn-info"><i class="fa fa-plus-circle"></i> 一键生成采购单</a>
-->
        <button id="btn-enables" toaction="<?php echo $buyer; ?>" type="button" data-toggle="tooltip" title="<?php echo $buttons_enable; ?>" class="btn btn-success">批量订购</button>
        <a href="<?php echo URL('sale/purchase_order/add', 'token='.$token) ?>" data-toggle="tooltip" title="添加" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="删除" class="btn btn-danger" onclick="confirm('确认删除采购单？') ? $('#form-order').submit() : false;"><i class="fa fa-trash-o"></i></button>
        <?php } ?>
        </div>
      <h1>采购单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 采购单</h3>
      </div>
      <div class="panel-body">
      <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">供应商</label>
                <input type="text" name="filter_vendor_name" value="<?php echo $filter_vendor_name; ?>" placeholder="供应商" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div> 
              <div class="form-group">
                <label class="control-label" for="input-total">条形码</label>
                <input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" placeholder="条形码" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <?php  foreach ($status_array as $key => $value) {?>
                  <option value="<?php echo $key; ?>" <?php if($filter_status==$key){echo 'selected="selected"'; } ?> ><?php echo $value ; ?></option>
                  <?php } ?>
                </select>
              </div>
<?php
if (false === $warehouseLocked) {
?>
              <div class="form-group">
                <label class="control-label" for="input-total">物流中心</label>
                <input type="text" name="filter_logcenter" value="<?php echo $filter_logcenter; ?>" placeholder="物流中心" id="input-total" class="form-control" />
              </div>
<?php
}
?>
              <div class="form-group">
                  <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
            
                <a id="button-export-lists" data-toggle="tooltip" title="导出对账单" class="btn btn-primary pull-right hidden" style="margin-left:10px;"><i class="fa fa-calculator"></i> 导出对账单</a>
              </div>
            </div>
          </div>
      </div>
        <form method="post"  enctype="multipart/form-data" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td>编号</td>
                  <td>创建时间</td>
                  <!-- <td>采购金额</td> -->
                  <td>供应商</td>
                  <td>商品品种</td>
                  <td>箱入数</td>
                  <td>采购单状态</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($po_list) { ?>
                <?php foreach ($po_list as $po) { ?>
                <tr style="color:<?php echo $po['all_color'];?>">
                  <td class="text-center"><?php if (in_array($po['id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $po['id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $po['id']; ?>" />
                    <?php } ?>
                  </td>
                    
                  <td><?php echo $po['id'] ?></td>
                  <td><?php echo $po['date_added'] ?></td>
                  
                  <td><?php echo $po['vendor_name'] ?></td>
                  <td><?php echo $po['count'] ?></td>
                  <td><?php echo $po['packing_no'] ?></td>
                  <td class="po_status"><?php echo getPoStatus()[$po['status']]?></td>
                  <td>
                  <a href="<?php echo URL('sale/purchase_order/view', 'token='.$token.'&po_id='.$po['id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                  <?php if($po['status']!=6) { ?>
                  <a href="<?php echo URL('sale/purchase_order/edit', 'token='.$token.'&po_id='.$po['id']) ?> " data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  <?php } ?>
                  <?php if(($po['status']==0||$po['status']==10) && $can_add_po) { ?>
                  <a href="<?php echo URL('sale/purchase_order/editnew', 'token='.$token.'&po_id='.$po['id']) ?> " data-toggle="tooltip" title="修改订购" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  <button type="button" value="<?php echo $po['id']; ?>" id="button-delete<?php echo $po['id']; ?>" data-loading-text="加载中..." data-toggle="tooltip" title="删除" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  <?php } ?>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="6">没有采购单数据</td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--

  $('#btn-enables').on('click', function(e){    
    if(confirm('确定订购吗？')){
      var actionurl = $(this).attr('toaction');
      $('#form-order').attr('action',actionurl);
      $('#form-order').submit();
    } 
  });
$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=sale/purchase_order/delete&token=<?php echo $token; ?>&po_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});
$('#button-filter').on('click', function() {
  url = 'index.php?route=sale/purchase_order&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_logcenter = $('input[name=\'filter_logcenter\']').val();

  if (filter_logcenter) {
    url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
  }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }
  var filter_sku = $('input[name=\'filter_sku\']').val();

  if (filter_sku) {
    url += '&filter_sku=' + encodeURIComponent(filter_sku);
  }

  location = url;
});

$('.date').datetimepicker({
  pickTime: false
});


$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});

$('#button-export-lists').on('click', function(e) {
  url = 'index.php?route=sale/purchase_order/exportLists&token=<?php echo $token; ?>';
  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_logcenter = $('input[name=\'filter_logcenter\']').val();

  if (filter_logcenter) {
    url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
  }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }
  var filter_sku = $('input[name=\'filter_sku\']').val();

  if (filter_sku) {
    url += '&filter_sku=' + encodeURIComponent(filter_sku);
  }

  location = url;
});
//--></script>
<?php echo $footer; ?>
