<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title_list; ?></h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_title_list; ?></h3>
                <a href="<?php echo $cancel; ?>"  data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default pull-right"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-keywords">关键字搜索</label>
                                <input type="text" name="filter_keywords" value="<?php echo $filter_keywords; ?>" placeholder="姓名 / 卡号" id="input-keywords" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <br />
                                <button type="button" id="button-resetFilter" class="btn btn-primary pull-right"><i class="fa"></i>所有记录</button>
                                <button type="button" id="button-filter" class="btn btn-primary pull-right" style='margin-right:10px;'><i class="fa fa-search"></i> 搜索</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-left">序号</th>
                    <th class="text-left">时间</th>
                    <th class="text-left">姓名</th>
                    <th class="text-left">账户类型</th>
                    <th class="text-left">部门</th>
                    <th class="text-left">卡号</th>
                    <th class="text-left">金额</th>
                    <th class="text-left">备注</th>
                    <th class="text-left">操作人</th>
                    <th class="text-left">打款状态</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($history_infos as $key=>$info) {
                ?>
                <tr>
                    <td><?php echo $info['remit_id'] ?></td>
                    <td><?php echo $info['date_add'] ?></td>
                    <td><?php echo $info['employ_name'] ?></td>
                    <td><?php echo $typeList[$info['card_type']]; ?></td>
                    <td><?php echo $info['section'] ?></td>
                    <td><?php echo $info['card_num'] ?></td>
                    <td><?php echo $info['amount'] ?></td>
                    <td><?php echo $info['comment'] ?></td>
                    <td><?php echo $info['username'] ?></td>
                    <td><?php echo $statusList[$info['status']]; ?></td>
                </tr>
                <?php
                }
                ?>
                </tbody>
            </table>  
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
<!--
function myTrim(str) {
    return str.replace(/^\s+|\s+$/gm,'');
}

$('#button-filter').on('click', function() {
    var keywords = myTrim($('#input-keywords').val());
    if ('' != keywords) {
        location = 'index.php?route=sale/remit_money/showList&keywords='+keywords+'&token=<?php echo $token; ?>';
    }
});

$('#button-resetFilter').on('click', function() {
    location = 'index.php?route=sale/remit_money/showList&token=<?php echo $token; ?>';
});
-->
</script>
