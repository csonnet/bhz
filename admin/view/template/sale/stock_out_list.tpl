<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
    <div class="pull-right">
    <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
    </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
   </div>
   <div id="form-order"></div>
   <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <div class="panel-body">
          <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">销售单ID</label>
                <input type="text" name="filter_refer_id" value="<?php echo $filter_refer_id; ?>" placeholder="销售单ID" id="input-refer-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label">商品名(商品编码)</label>
                <input type="text" name="filter_name_code" value="<?php echo $filter_name_code; ?>" placeholder="商品名(商品编码)" id="input-total" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label">操作员</label>
                <input type="text" name="filter_user_name" value="<?php echo $filter_user_name; ?>" placeholder="操作员" id="input-user-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">搜索单据创建开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_add_date_start" value="<?php echo $filter_add_date_start;?>" placeholder="搜索单据创建开始时间" data-date-format="YYYY-MM-DD" id="input-add-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label">搜索单据创建结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_add_date_end" value="<?php echo $filter_add_date_end;?>" placeholder="搜索订单创建结束时间" data-date-format="YYYY-MM-DD" id="input-add-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label">出库单ID</label>
                <input type="text" name="out_id" value="<?php echo $out_id; ?>" placeholder="出库单ID" id="input-refer-id" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">仓库名</label>
                <select name="filter_warehouse" id="input-warehouse" class="form-control">
                  <option value="empty"></option>
                  <?php foreach($warehouse_arr as $val){?>
                  <option value="<?php echo $val['warehouse_id']?>" <?php echo $filter_warehouse_array[$val['warehouse_id']]?> ><?php echo $val['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">单据状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="empty"></option>
                  <?php foreach($status_arr as $val){?>
                  <option value="<?php echo $val['id']?>" <?php echo $filter_status_array[$val['id']]?> ><?php echo $val['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> 筛选</button>
                <button type="button" id="button-exportlists" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出出库单</button>
                <button type="button" id="button-collect" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出汇总出库单</button>
              </div>
            </div>
        </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr style="background-color: white">
                  <td class="text-left"><?php if ($sort == 'so.out_id') { ?>
                    <a href="<?php echo $sort_out_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_out_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_out_id; ?>"><?php echo $column_out_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.refer_id') { ?>
                    <a href="<?php echo $sort_refer_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_refer_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_refer_id; ?>"><?php echo $column_refer_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.refer_type_id') { ?>
                    <a href="<?php echo $sort_refer_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_refer_type; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_refer_type; ?>"><?php echo $column_refer_type; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.warehouse_id') { ?>
                    <a href="<?php echo $sort_warehouse; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_warehouse; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_warehouse; ?>"><?php echo $column_warehouse; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.user_id') { ?>
                    <a href="<?php echo $sort_user; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_user; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_user; ?>"><?php echo $column_user; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.sale_money') { ?>
                    <a href="<?php echo $sort_money; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_money; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_money; ?>"><?php echo $column_money; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'so.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'o.logcenter_id') { ?>
                    <a href="<?php echo $sort_logcenter_id; ?>" class="<?php echo strtolower($order); ?>">目的地仓库</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_logcenter_id; ?>">目的地仓库</a>
                    <?php } ?></td>
                  <td class="text-left">
                    审核天数
                  </td>
                  <td class="text-left">
                    配货天数
                  </td>
                  <td class="text-left">
                    干线天数
                  </td>
                  <td class="text-left">
                    派送天数
                  </td>
                  <td class="text-left"><?php if ($sort == 'so.out_date') { ?>
                    <a href="<?php echo $sort_out_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_out_date; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_out_date; ?>"><?php echo $column_out_date; ?></a>
                    <?php } ?></td>
                  <td class="text-left">
                    派送时间
                  </td>
                  <td class="text-left">
                    签收时间
                  </td>
                  <td class="text-left"><?php if ($sort == 'so.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?></td>
                  <td class="text-left">
                    订单创建时间
                  </td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($stock_outs) { ?>
                <?php foreach ($stock_outs as $stock_out) { ?>
                  <?php
                  if ($warehouseLocked && $warehouseLocked == $stock_out['warehouse_id']) {
                    echo '<tr style="background-color: #FFFFFF;">';
                  }else{
                    echo '<tr style="background-color: #FFFFFF;">';
                  }
                  ?>
                
                  <td class="text-left">
                  <?php echo $stock_out['out_id']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['refer_id']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['rtname']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['whname']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['username']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['sale_money']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['tsname']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['send_ware_name']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['cta_day_times']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['atc_day_times']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['ctd_day_times']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['dtf_day_times']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['out_date']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['deliver_date']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['receive_date']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['date_added']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out['order_date_added']?>
                  </td>
                  <td class="text-right">
                  <?php
                  if ($warehouseLocked == $stock_out['warehouse_id']||$stock_out['canDeliverAndAccept']) {
                  ?>
                  <?php if($stock_out['status'] == 0 && $stock_out['refer_type_id'] == 5){?>
                  <button type="button" value="<?php echo $stock_out['out_id']; ?>" id="button-recover<?php echo $stock_out['out_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_recover; ?>" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i></button>
                  <?php }?>
                  <?php
                  }
                  ?>

                  <a href="<?php echo $stock_out['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>

                  <?php
                  if ($warehouseLocked == $stock_out['warehouse_id']||$stock_out['canDeliverAndAccept']) {
                  ?>
                  <?php if($stock_out['status'] == 1 || $stock_out['status'] == 0||$stock_out['canDeliverAndAccept']){?>
                  <a href="<?php echo $stock_out['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
                  <?php }?>
                  <?php if($stock_out['status'] == 1){?>
                  <button type="button" value="<?php echo $stock_out['out_id']; ?>" id="button-delete<?php echo $stock_out['out_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                  <button type="button" value="<?php echo $stock_out['out_id']; ?>" id="button-complete<?php echo $stock_out['out_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_complete; ?>" class="btn btn-success"><i class="fa fa-play"></i></button>
                  <?php }?>
                  <?php
                  }
                  ?>
                  <?php if($stock_out['status'] == 2 && $stock_out['canDeliverAndAccept'] && $stock_out['refer_type_id'] == 1){?>
                  <button type="button" value="<?php echo $stock_out['out_id']; ?>" id="button-deliver<?php echo $stock_out['out_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-info">派送货物</button>
                  <?php }?>
                  <?php if($stock_out['status'] == 3 && $stock_out['canDeliverAndAccept'] && $stock_out['refer_type_id'] == 1){?>
                  <button type="button" value="<?php echo $stock_out['out_id']; ?>" id="button-final-complete<?php echo $stock_out['out_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-info">完成签收</button>
                  <?php }?>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
  </div>
          <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
        </div>
   </div>
   <script>
   $('#button-filter').on('click', function(){
      url = 'index.php?route=sale/stock_out&token=<?php echo $token; ?>';

      var filter_refer_id = $('input[name=\'filter_refer_id\']').val();

      if(filter_refer_id){
        url += '&filter_refer_id=' + encodeURIComponent(filter_refer_id);
      }

      var out_id = $('input[name=\'out_id\']').val();

      if(out_id){
        url += '&out_id=' + encodeURIComponent(out_id);
      }

      var filter_name_code = $('input[name=\'filter_name_code\']').val();

      if(filter_name_code){
        url += '&filter_name_code=' + encodeURIComponent(filter_name_code);
      }

      var filter_user_name = $('input[name=\'filter_user_name\']').val();

      if(filter_user_name){
        url += '&filter_user_name=' + encodeURIComponent(filter_user_name);
      }

      var filter_warehouse = $('select[name=\'filter_warehouse\']').val();

      if(filter_warehouse != 'empty'){
        url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
      }

      var filter_add_date_start = $('input[name=\'filter_add_date_start\']').val();

      if(filter_add_date_start){
        url += '&filter_add_date_start=' + encodeURIComponent(filter_add_date_start);
      }

      var filter_add_date_end = $('input[name=\'filter_add_date_end\']').val();

      if(filter_add_date_end){
        url += '&filter_add_date_end=' + encodeURIComponent(filter_add_date_end);
      }

      var filter_status = $('select[name=\'filter_status\']').val();

      if(filter_status != 'empty'){
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      location = url;
   })

    $('.date').datetimepicker({
      pickDate: true
    });

    // Login to the API
    var token = '';

    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/login',
      type: 'post',
      data: 'key=<?php echo $api_key; ?>',
      dataType: 'json',
      crossDomain: true,
      success: function(json) {
            // $('.alert').remove();

            if (json['error']) {
            if (json['error']['key']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

                if (json['error']['ip']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
            }
            }

        if (json['token']) {
          token = json['token'];
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });

    $('button[id^=\'button-delete\']').on('click', function(e) {
      if (confirm('相关销售单有可能会被打回待审核状态，<?php echo $text_confirm; ?>')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/delete&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-recover\']').on('click', function(e) {
      if (confirm('<?php echo $text_confirm2; ?>')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/recover&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-complete\']').on('click', function(e) {
      if (confirm('<?php echo $text_confirm3; ?>')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/complete&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-deliver\']').on('click', function(e) {
      if (confirm('派送出库单')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/deliver&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-final-complete\']').on('click', function(e) {
      if (confirm('签收出库单')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/finalcomplete&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('#button-exportlists').on('click', function() {
      url = 'index.php?route=sale/stock_out/exportLists&token=<?php echo $token; ?>';

      var filter_refer_id = $('input[name=\'filter_refer_id\']').val();

      if(filter_refer_id){
        url += '&filter_refer_id=' + encodeURIComponent(filter_refer_id);
      }

      var filter_name_code = $('input[name=\'filter_name_code\']').val();

      if(filter_name_code){
        url += '&filter_name_code=' + encodeURIComponent(filter_name_code);
      }

      var filter_user_name = $('input[name=\'filter_user_name\']').val();

      if(filter_user_name){
        url += '&filter_user_name=' + encodeURIComponent(filter_user_name);
      }

      var filter_warehouse = $('select[name=\'filter_warehouse\']').val();

      if(filter_warehouse != 'empty'){
        url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
      }

      var filter_add_date_start = $('input[name=\'filter_add_date_start\']').val();

      if(filter_add_date_start){
        url += '&filter_add_date_start=' + encodeURIComponent(filter_add_date_start);
      }

      var filter_add_date_end = $('input[name=\'filter_add_date_end\']').val();

      if(filter_add_date_end){
        url += '&filter_add_date_end=' + encodeURIComponent(filter_add_date_end);
      }

      var filter_status = $('select[name=\'filter_status\']').val();

      if(filter_status != 'empty'){
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      location = url;
    })

    $('#button-collect').on('click', function() {
      url = 'index.php?route=sale/stock_out/exportCollect&token=<?php echo $token; ?>';

      var filter_refer_id = $('input[name=\'filter_refer_id\']').val();

      if(filter_refer_id){
        url += '&filter_refer_id=' + encodeURIComponent(filter_refer_id);
      }

      var filter_name_code = $('input[name=\'filter_name_code\']').val();

      if(filter_name_code){
        url += '&filter_name_code=' + encodeURIComponent(filter_name_code);
      }

      var filter_user_name = $('input[name=\'filter_user_name\']').val();

      if(filter_user_name){
        url += '&filter_user_name=' + encodeURIComponent(filter_user_name);
      }

      var filter_warehouse = $('select[name=\'filter_warehouse\']').val();

      if(filter_warehouse != 'empty'){
        url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
      }

      var filter_add_date_start = $('input[name=\'filter_add_date_start\']').val();

      if(filter_add_date_start){
        url += '&filter_add_date_start=' + encodeURIComponent(filter_add_date_start);
      }

      var filter_add_date_end = $('input[name=\'filter_add_date_end\']').val();

      if(filter_add_date_end){
        url += '&filter_add_date_end=' + encodeURIComponent(filter_add_date_end);
      }

      var filter_status = $('select[name=\'filter_status\']').val();

      if(filter_status != 'empty'){
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      location = url;
    })

   </script>