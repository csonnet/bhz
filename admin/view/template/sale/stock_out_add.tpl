<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
	  <button class="btn btn-primary" ng-click="save_stock_out('<?php echo $token ?>')"><i class="fa fa-save"></i> 保存</button>
	  <a id="button-upload" data-toggle="tooltip" title="导入出库商品" class="btn btn-primary"><i class="fa fa-calculator"></i> 导入出库商品</a>
      <a class="btn btn-default" href="<?php echo $return_url?>"><i class="fa fa-reply"></i> 返回</a>
      </div>
      <h1>添加出库单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 添加出库单</h3>
      </div>
      <div class="panel-body">
      <form method="post" enctype="multipart/form-data" class="form-horizontal" id="form-order">
		<fieldset>
		<legend>基本信息</legend>
	  	<div class="form-group required">
        <label class="col-sm-2 control-label">销售单ID</label>
        <div class="col-sm-10">
			<input id="refer_id" type="text" class="form-control" placeholder="销售单ID">
			<div class="{{referidError?'text-danger':''}}">{{referidError}}</div>
        </div>
      	</div>


		<div class="form-group required">
		<label class="col-sm-2 control-label">单据类型</label>
		<div class="col-sm-10">
			<select id="refer_type" name="" class="form-control">
			<option value="empty"></option>
			<?php foreach($refer_type_arr as $val){?>
			<option value="<?php echo $val['refer_type_id']?>"><?php echo $val['name']?></option>
			<?php }?>
			</select>
			<div class="{{ReferTypeError?'text-danger':''}}">{{ReferTypeError}}</div>
		</div>			
      	</div>

      	<div class="form-group required">
		<label class="col-sm-2 control-label">出货仓库</label>
		<div class="col-sm-10">
			<select id="warehouse" class="form-control">
			<option value="empty"></option>
			<?php foreach($warehouse_arr as $val){?>
			<option value="<?php echo $val['warehouse_id']?>"><?php echo $val['name']?></option>
			<?php }?>
			</select>
			<div class="{{WarehouseError?'text-danger':''}}">{{WarehouseError}}</div>
		</div>			
      	</div>

      	<div class="form-group required">
		<label class="col-sm-2 control-label">状态</label>
		<div class="col-sm-10">
			<select id="status" class="form-control">
			<option value="empty"></option>
			<?php foreach($status_arr as $val){?>
			<option value="<?php echo $val['id']?>"><?php echo $val['name']?></option>
			<?php }?>
			</select>
			<input disabled id="sale_money" type="hidden" class="form-control" placeholder="0">
			<div class="{{StatusError?'text-danger':''}}">{{StatusError}}</div>
		</div>			
      	</div>
      	<div class="form-group">
      	<label class="col-sm-2 control-label">备注</label>
      	<div class="col-sm-10">
		<textarea name="comment" class="form-control" id="comment" rows="8"></textarea>
      	</div>
      	</div>
		</fieldset>
		<fieldset>
        <legend>相关产品</legend>
      	<table class="table ">
			<thead>
				<tr>
					<td>商品编码</td>
					<td>商品名称</td>
					<td>商品数量</td>
					<td>条形码</td>
					<td>商品单价</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="product in products">
					<td class="{{product.codeError?'has-error':''}}">
	                  <div class="dropdown">
	                    <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
	                      <input type="text" ng-change="getProductsByCode(product.code, '<?php echo $token ?>')" ng-model="product.code" class="form-control" placeholder="商品编码" aria-describedby="basic-addon2">
	                    </div>
	                    <div class="text-danger">{{product.codeError}}</div>
	                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
	                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProductSec(p,product)">
	                        <div class="checkbox">
	                          <label>
	                             {{p.code}} {{p.pdname}}
	                          </label>
	                        </div>
	                      </li>
	                    </ul>
	                  </div>
	                </td>
					<td class="{{product.nameError?'has-error':''}}">
						<div class="dropdown">							
							<div  class="input-group" aria-expanded="true">
		                      <text style="min-width: 300px" disabled ng-model="product.name" class="form-control" placeholder="商品名称" aria-describedby="basic-addon2">
							  {{product.name}}
		                      </text>
		                    </div>
		                    <div class="text-danger">{{product.nameError}}</div>
						</div>
					</td>
					<td class="{{product.numError?'has-error':''}}">
						<div class="dropdown">							
							<div  class="input-group" aria-expanded="true">
		                      <input type="text" ng-change="caulatetotal(product)" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"  ng-model="product.num" class="form-control" placeholder="商品数量" aria-describedby="basic-addon2">
		                    </div>
		                    <div class="text-danger">{{product.numError}}</div>
						</div>
					</td>
					<td class="{{product.skuError?'has-error':''}}">
		                <div class="dropdown">              
		                  <div  class="input-group" aria-expanded="true">
		                              <input type="text" disabled name="product_sku" ng-model="product.sku" class="form-control" placeholder="条形码" aria-describedby="basic-addon2">
		                            </div>
		                            <div class="text-danger">{{product.skuError}}</div>
		                </div>
		              </td>
					<td class="{{product.priceError?'has-error':''}}">
						<div class="dropdown">							
							<div  class="input-group" aria-expanded="true">
		                      <input type="text" ng-model="product.price" class="form-control" placeholder="商品单价" aria-describedby="basic-addon2">
		                      <input type="hidden" disabled ng-model="product.total" class="form-control" placeholder="商品总价" aria-describedby="basic-addon2">
		                    </div>
		                    <div class="text-danger">{{product.priceError}}</div>
						</div>
					</td>
					<td>
	                  <button type="button" ng-click="removeproduct($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
	                </td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
	            <td colspan="5"></td>
	            <td ><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
	          	</tr>
			</tfoot>
      	</table>
      	</fieldset>
      </form>
      <div class="row">
        </div>
      </div>
    </div>
  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
  var $scope;

  	$(function(){

		var appElement = document.querySelector('[ng-controller=bhzAdminCtrl]');

		$scope = angular.element(appElement).scope();

	})

$('.date').datetimepicker({
  pickTime: false
});

$('#button-upload').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);   
      
      $.ajax({
        url: 'index.php?route=sale/stock_out/add_import&token=<?php echo $token; ?>',
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
                
          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            $scope.products = json['data'];
            $scope.$apply();
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<?php echo $footer; ?>