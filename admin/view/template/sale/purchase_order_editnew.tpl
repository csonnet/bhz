<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
   <!--      <a href="<?php echo URL('sale/purchase_order/export', 'token='.$token.'&po_id='.$po['id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a> -->
      </div>
      <h1>采购单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <br>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 采购单</h3>
      </div>
      <div class="panel-body">
        <dl class="dl-horizontal col-xs-6">
          <dt>编号</dt>
          <dd><?php echo $po['id'] ?></dd>
          <dt>创建者</dt>
          <dd><?php echo $po['creator']['fullname']?></dd>
          <dt>创建时间</dt>
          <dd><?php echo $po['date_added']?></dd>
          <dt>状态</dt>
          <dd class="po_status"><?php echo getPoStatus()[$po['status']]?></dd>
          <br>

          <dt>入库仓库</dt>
          <dd><?php echo $po['warehouse']['name'];?></dd>

        </dl>

        <dl class="dl-horizontal col-xs-6">
          <dt>供应商</dt>
          <dd><?php echo $po['vendor']['vendor_name'].'  '.$po['vendor']['firstname']?></dd>
          <dt>供应商联系电话</dt>
          <dd><?php echo $po['vendor']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $po['vendor']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $po['vendor']['address_1'] ?></dd>
        </dl>
      </div>
      <table class="table ">
            <thead>
              <tr>
                  <td>产品</td>
                  <td>商品编码</td>
                  <td>条形码</td>
                  <td>箱入数</td>
                  <td>选项</td>
                  <td>数量</td>
                  <td class="hidden">采购单价</td>
                  <td></td>
                </tr>
            </thead>
            <tbody>
              <tr ng-repeat="product in products">
                <td class="{{product.productError?'has-error':''}}">
                  <div class="dropdown">
                    <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
                      <input type="text" ng-change="getProduct(product.name, '<?php echo $token ?>')" ng-model="product.name" class="form-control" placeholder="产品" aria-describedby="basic-addon2">
                    </div>
                    <div class="text-danger">{{product.productError}}</div>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                        <div class="checkbox">
                          <label>
                             {{p.name}}
                          </label>
                        </div>
                      </li>
                    </ul>
                  </div>
                </td>
                <td>
                  <div>{{product.product_code}}</div>
                </td>
                <td>
                  <div>{{product.sku}}</div>
                </td>
                <td>
                  <div>{{product.packing_no}}</div>
                </td>
                <td class="{{product.optionError?'has-error':''}}">
                {{product.option_name}}
                 <!--  <select ng-change="change_price(product, product.option)" class="form-control" ng-model="product.option_name" ng-if="product.option_list" ng-options="item.name for item in product.option_list.product_option_value">
                    
                  </select> -->
                  <div class="text-danger">{{product.optionError}}</div>
                </td>
                <td class="{{product.qtyError?'has-error':''}}">
                  <input type="text"  ng-model="product.qty" class="form-control" placeholder="数量" >
                  <div class="text-danger">{{product.qtyError}}</div>
                </td>
                <td class="{{product.priceError?'has-error':''}} hidden">
                  <input type="text"  ng-model="product.unit_price" class="form-control" placeholder="单价" >
                </td>
                <td>
                  <button type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="6"></td>
                <td ><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>
      <button class="btn btn-danger pull-right" ng-click="confirm_po('<?php echo $po['id']?>', '<?php echo $token ?>', '<?php echo $po['warehouse']['warehouse_id'];?>', '<?php echo $po['status']?>')">确认采购</button>
    </div>

    <br><br><br>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

  <?php if(!empty($po_products)) { ?>
    var po_products = <?php echo json_encode($po_products);?>;
     // console.log(po_products);
    var is_edit = true;
  <?php } else { ?>
    var po_products = {};
    var is_edit = false;
  <?php } ?>
//--></script>
<?php echo $footer; ?>
