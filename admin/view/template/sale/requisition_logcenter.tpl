<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <h1>调拨单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 调拨单</h3>
      </div>
      <div class="panel-body">
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">

         <!-- <div class="dropdown col-xs-4 form-group {{vendorError?'has-error':''}}">
            <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
              <label class="control-label">供应商</label>
              <input type="text" ng-change="getVendor(vendor.vendor_name, '<?php echo $token ?>')" ng-model="vendor.vendor_name" class="form-control" placeholder="供应商" aria-describedby="basic-addon2" id="po_vendors">
            </div>
            <div class="text-danger">{{vendorError}}</div>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
              <li class="vertical-dropdown" ng-repeat="vendor in searchVendors" ng-click="selectVendor(vendor)">
                <div class="checkbox">
                  <label>
                     {{vendor.vendor_name}}
                  </label>
                </div>
              </li>
            </ul>
          </div> -->

          <div class="dropdown col-xs-4">
            <div class="form-group {{deliverTimeError?'has-error':''}}">
              <label class="control-label">调拨时间</label>
              <div class="input-group date">
                <input type="text" name="filter_date_added" ng-model="deliver_time" placeholder="调拨时间" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
              <div class="text-danger">{{deliverTimeError}}</div>
            </div>
          </div>

          <div class="dropdown col-xs-4 form-group {{vendorError?'has-error':''}}">
            <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
              <label class="control-label">目的物流中心</label>
              <input type="text" ng-change="getLogcenter(logcenter.logcenter_name, '<?php echo $token ?>')" ng-model="logcenter.logcenter_name" class="form-control" placeholder="目的物流中心" aria-describedby="basic-addon2" id="requisition_logcenters">
            </div>
            <div class="text-danger">{{logcenterNameError}}</div>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
              <li class="vertical-dropdown" ng-repeat="logcenter in searchLogcenters" ng-click="selectLogcenter(logcenter)">
                <div class="checkbox">
                  <label>
                     {{logcenter.logcenter_name}}
                  </label>
                </div>
              </li>
            </ul>
          </div>
          <!-- <div class="dropdown col-xs-4 hidden">
            <div class="form-group">
              <label class="control-label">调拨单状态</label>
              <select class="form-control" ng-model="status">
                <?php foreach (getRoStatus() as $key => $value) { ?>
                  <option value="<?php echo $key ?>"><?php echo $value ?></option>
                <?php } ?>
              </select>
            </div>
          </div> -->

          <div class="col-xs-6 form-group">
            <div  class="">
              <label class="control-label">包含订单号</label>
              <input type="text" ng-model="included_order_ids" class="form-control" placeholder="包含订单号, 请以逗号隔开">
            </div>
          </div>

          <table class="table ">
            <thead>
              <tr>
                  <td>产品</td>
                  <td>型号</td>
                  <td>箱入数</td>
                  <td>选项</td>
                  <td>数量</td>
                  <td class="hidden">调拨单价</td>
                  <td></td>
                </tr>
            </thead>
            <tbody>
              <tr ng-repeat="product in products">
                <td class="{{product.productError?'has-error':''}}">
                  <div class="dropdown">
                    <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
                      <input type="text" ng-change="getProduct(product.name, '<?php echo $token ?>')" ng-model="product.name" class="form-control" placeholder="产品" aria-describedby="basic-addon2">
                    </div>
                    <div class="text-danger">{{product.productError}}</div>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                        <div class="checkbox">
                          <label>
                             {{p.name}}
                          </label>
                        </div>
                      </li>
                    </ul>
                  </div>
                </td>
                <td>
                  <div>{{product.model}}</div>
                </td>
                <td>
                  <div>{{product.packing_no}}</div>
                </td>
                <td class="{{product.optionError?'has-error':''}}">
                  <select ng-change="change_price(product, product.option)" class="form-control" ng-model="product.option" ng-if="product.option_list" ng-options="item.name for item in product.option_list.product_option_value">
                    
                  </select>
                  <div class="text-danger">{{product.optionError}}</div>
                </td>
                <td class="{{product.qtyError?'has-error':''}}">
                  <input type="number"  ng-model="product.qty" class="form-control" placeholder="数量" >
                  <div class="text-danger">{{product.qtyError}}</div>
                </td>
                <td class="{{product.priceError?'has-error':''}} hidden">
                  <input type="text"  ng-model="product.unit_price" class="form-control" placeholder="单价" >
                </td>
                <td>
                  <button type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5"></td>
                <td ><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
              <tr>
                <td colspan="4"></td>
                <td class="hidden">总价:</td>
                <td class="hidden">
                  {{getTotalPrice()|currency:"￥":2}}
                </td>
              </tr>
            </tfoot>
          </table>
          <button class="btn btn-primary pull-right" ng-click="save_requisition('<?php echo $token ?>')">继续</button>
        </form>
        <div class="row">
        </div>
      </div>
    </div>
  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

setTimeout(function () {
  $('#po_vendors').trigger('change');
}, 1000);
//--></script>
<?php echo $footer; ?>
