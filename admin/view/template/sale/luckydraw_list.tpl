<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('确认删除未激活活动？') ? $('#form-luckydraw').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div id="form-order"></div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-luckydraw-name">抢购活动名称</label>
                <input type="text" name="filter_luckydraw_name" value="<?php echo $filter_luckydraw_name; ?>" placeholder="抢购活动名称" id="input-luckydraw-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($statuses as $status_id=>$status_name) { ?>
                  <?php if (!is_null($filter_status) && $status_id == $filter_status) { ?>
                  <option value="<?php echo $status_id; ?>" selected="selected"><?php echo $status_name; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $status_id; ?>"><?php echo $status_name; ?></option>
                  <?php } ?>
                  <?php } ?>                
                </select>
              </div>
              <div class="form-group">
                <!--导入抽奖按钮-->
                <div class="row">
                    <button type="button" id="button-reset" class="btn btn-primary pull-right"><i class="fa fa-rotate-left"></i> 重置</button>
                  
                    <a id="button-upload-reward" data-toggle="tooltip" title="导入抽奖" class="btn btn-primary pull-right"><i class="fa fa-calculator"></i> 导入抽奖</a>
                    
                    <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                              
                </div>
              </div>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'luckydraw_id') { ?>
                    <a href="<?php echo $sort_luckydraw_id; ?>" class="<?php echo strtolower($order); ?>">序号</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_luckydraw_id; ?>">序号</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'luckydraw_name') { ?>
                    <a href="<?php echo $sort_luckydraw_name; ?>" class="<?php echo strtolower($order); ?>">活动名</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_luckydraw_name; ?>">活动名</a>
                    <?php } ?></td>
                  <td class="text-left">开始时间</td>
                  <td class="text-left">结束时间</td>
                  <td class="text-left">中奖概率</td>
                  <td class="text-left"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">状态</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>">状态</a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($luckydraws) { ?>
                <?php foreach ($luckydraws as $luckydraw) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($luckydraw['luckydraw_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $luckydraw['luckydraw_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $luckydraw['luckydraw_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $luckydraw['luckydraw_id']; ?></td>
                  <td class="text-left"><?php echo $luckydraw['luckydraw_name']; ?></td>
                  <td class="text-left"><?php echo $luckydraw['start_time']; ?></td>
                  <td class="text-left"><?php echo $luckydraw['end_time']; ?></td>
                  <td class="text-left"><?php echo $luckydraw['chance']; ?></td>
                  <td class="text-left"><?php echo $luckydraw['status']; ?></td>
                  <td class="text-right"><a href="<?php echo $luckydraw['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="13"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#button-jump').on('click', function() {
  var url = 'index.php?route=sale/luckydraw&token=<?php echo $token; ?>';

  var pnum = $('input[name=\'pnum\']').val();

  if (pnum) {
    url += '&page=' + encodeURIComponent(pnum);
  }

  location = url;
});
//--></script> 

<script type="text/javascript"><!--
function getFilterInfo() {
  url = '';
  var filter_luckydraw_name = $('input[name=\'filter_luckydraw_name\']').val();
  if (filter_luckydraw_name) {
    url += '&filter_luckydraw_name=' + encodeURIComponent(filter_luckydraw_name);
  }
  var filter_status = $('select[name=\'filter_status\']').val();
  if (filter_status != '*') {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  } 
  return url;
}
$('#button-reset').on('click', function() {
  $('input').val('');
  $('select').val('*');
  $('#button-filter').click();
});

$('#button-filter').on('click', function() {
  url = 'index.php?route=sale/luckydraw&token=<?php echo $token; ?>';
  url += getFilterInfo();
  location = url;
});

//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_luckydraw_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=sale/luckydraw/luckydrawNameAutocomplete&token=<?php echo $token; ?>&filter_luckydraw_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['luckydraw_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_luckydraw_name\']').val(item['label']);
  }
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>



<script>
$('#button-upload-reward').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val(
      
    ) != '') {
      clearInterval(timer);   
      
      $.ajax({
        url: 'index.php?route=sale/return_logcenter_order/importReward&token=<?php echo $token; ?>',
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload-reward').button('loading');
        },
        complete: function() {
          $('#button-upload-reward').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
                
          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            window.location.reload();
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
           //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

//--></script></div>
<?php echo $footer; ?>