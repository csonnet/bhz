<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo URL('sale/purchase_order/export', 'token='.$token.'&po_id='.$po['id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a>
      </div>
      <h1>采购单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <br>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 采购单</h3>
      </div>
      <div class="panel-body">
        <dl class="dl-horizontal col-xs-6">
          <dt>编号</dt>
          <dd><?php echo $po['id'] ?></dd>
          <dt>创建者</dt>
          <dd><?php echo $po['creator']['fullname']?></dd>
          <dt>创建时间</dt>
          <dd><?php echo $po['date_added']?></dd>
          <dt>状态</dt>
          <dd class="po_status"><?php echo getPoStatus()[$po['status']]?></dd>
          <br>

          <dt>入库仓库</dt>
          <dd><?php echo $po['warehouse']['name'];?></dd>

        </dl>

        <dl class="dl-horizontal col-xs-6">
          <dt>供应商</dt>
          <dd><?php echo $po['vendor']['vendor_name'].'  '.$po['vendor']['firstname']?></dd>
          <dt>供应商联系电话</dt>
          <dd><?php echo $po['vendor']['telephone'] ?></dd>
          <dt>email</dt>
          <dd><?php echo $po['vendor']['email'] ?></dd>
          <dt>地址</dt>
          <dd>
            <?php echo $po['vendor']['address_1'] ?></dd>
        </dl>
      </div>
      <table class="table">
        <thead>
          <tr>
            <td>商品</td>
            <td>商品编码</td>
            <td>条形码</td>
            <td>选项</td>
            <td>订购数量</td>
            <td>已收货数量</td>
            <td>箱入数</td>
            <td>入库数量</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($po_products as $key => $product) {?>
            <tr>
              <td><?php echo $product['name'] ?></td>
              <td><?php echo $product['product_code'] ?></td>
              <td><?php echo $product['sku'] ?></td>
              <td><?php echo $product['option_name'] ?></td>
              <td><?php echo $product['qty'] ?></td>
              <td><?php echo $product['delivered_qty'] ?></td>
              <td><?php echo $product['packing_no'] ?></td>
              <td class="{{product.stockError?'has-error':''}}">
                  <input type="number"  ng-model="products[<?php echo $product['id']?>].stock" class="form-control" placeholder="数量" >
                  <div class="text-danger">{{products[<?php echo $product['id']?>].stockError}}</div>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <button class="btn btn-danger pull-right" ng-click="save_stock('<?php echo $po['id']?>', '<?php echo $token ?>')">保存入库记录</button>
    </div>

    <br><br><br>

        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> 采购单历史</h3>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">添加时间</td>
                <td class="text-left">操作员</td>
                <td class="text-left">操作内容</td>
              </tr>
            </thead>
            <tbody>
              <?php if ($po_histories) { ?>
              <?php foreach ($po_histories as $history) { ?>
              <tr>
                <td class="text-left"><?php echo $history['date_added']; ?></td>
                <td class="text-left"><?php echo $history['operator_name']; ?></td>
                <td class="text-left"><?php echo $history['comment']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4">无记录</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <br><br><br>
    
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-arrows-h"></i> 相关入库单</h3>
    </div>
    <div class="panel-body">
      <div class="tab-content">
            
        <table class="table table-bordered bhz-table">
            <thead>
              <tr>
                      <th class="text-left">入库单号</th>
                      <th class="text-left">入库仓库</th>
                            <th class="text-left">操作人</th>
                            <th class="text-left">单据状态</th>
                            <th class="text-left">创建时间</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if ($ro_stockin) { ?>
                    <?php foreach ($ro_stockin as $stockin) { ?>
                    <tr>
                      <td class="text-left"><?php echo $stockin['in_id']; ?></td>
                      <td class="text-left"><?php echo $stockin['warehouse_name']; ?></td>
                      <td class="text-left"><?php echo $stockin['username']; ?></td>
                            <td class="text-left"><?php echo $stockin['status_name']; ?></td>
                            <td class="text-left"><?php echo $stockin['date_added']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="99">无关联单据</td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
          
        </div>
    </div>
  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});


//--></script>
<?php echo $footer; ?>
