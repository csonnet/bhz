<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id">对账单编号</label>
                <input type="text" name="filter_logcenter_bill_id" value="<?php echo $filter_logcenter_bill_id; ?>" placeholder="对账单编号" id="input-logcenter-bill-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-logcenter_name">物流营销中心</label>
                <input type="text" name="filter_logcenter_name" value="<?php echo $filter_logcenter_name; ?>" placeholder="物流营销中心" id="input-logcenter-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-year-month">年月份</label>
                <div class="input-group year_month">
                  <input type="text" name="filter_year_month" value="<?php echo $filter_year_month; ?>" placeholder="年月份" data-date-format="YYYY-MM" id="input-year-month" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">金额</label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="金额" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group hidden">
                <label class="control-label" for="input-date-added">添加日期</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="添加日期" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-logcenter_bill-status">状态</label>
                <select name="filter_logcenter_bill_status" id="input-logcenter_bill-status" class="form-control">
                  <option value=""></option>
                  <?php foreach ($logcenter_bill_statuses as $logcenter_bill_status_id=>$logcenter_bill_status_name) { ?>
                  <?php if ($logcenter_bill_status_id == $filter_logcenter_bill_status && $filter_logcenter_bill_status!='') { ?>
                  <option value="<?php echo $logcenter_bill_status_id; ?>" selected="selected"><?php echo $logcenter_bill_status_name; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $logcenter_bill_status_id; ?>"><?php echo $logcenter_bill_status_name; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <a id="button-export-lists" data-toggle="tooltip" title="导出对账单" class="btn btn-primary pull-right" style="margin-left:10px;"><i class="fa fa-calculator"></i> 导出对账单</a>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> 筛选</button>
            </div>
          </div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-logcenter-bill">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <!-- <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td> -->
                  <td class="text-right"><?php if ($sort == 'lb.logcenter_bill_id') { ?>
                    <a href="<?php echo $sort_logcenter_bill; ?>" class="<?php echo strtolower($order); ?>">对账单编号</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_logcenter_bill; ?>">对账单编号</a>
                    <?php } ?></td>
                  <td class="text-left">物流中心名称</td>
                  <td class="text-left"><?php if ($sort == 'lb.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">状态</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>">状态</a>
                    <?php } ?></td>
                  <td class="text-right">总送货金额</td>
                  <td class="text-right">提成(%)</td>
                  <td class="text-right">物流费</td>
                  <td class="text-right">差额</td>
                  <td class="text-right">应付物流费</td>
                  <td class="text-left"><?php if ($sort == 'lb.year_month') { ?>
                    <a href="<?php echo $sort_year_month; ?>" class="<?php echo strtolower($order); ?>">年月份</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_year_month; ?>">年月份</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'lb.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>">添加日期</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>">添加日期</a>
                    <?php } ?></td>
                  <td class="text-left hidden"><?php if ($sort == 'lb.date_modified') { ?>
                    <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>">修改日期</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_modified; ?>">修改日期</a>
                    <?php } ?></td>
                  <td class="text-right">操作</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($logcenter_bills) { ?>
                <?php foreach ($logcenter_bills as $logcenter_bill) { ?>
                <tr>
                  <td class="text-right"><?php echo $logcenter_bill['logcenter_bill_id']; ?></td>
                  <td class="text-left"><?php echo $logcenter_bill['logcenter_name']; ?></td>
                  <td class="text-left"><?php echo $logcenter_bill['status_name']; ?></td>
                  <td class="text-right"><?php echo $logcenter_bill['order_total']; ?></td>
                  <td class="text-right"><?php echo $logcenter_bill['commission']; ?></td>
                  <td class="text-right"><?php echo $logcenter_bill['total']; ?></td>
                  <td class="text-right"><?php echo $logcenter_bill['difference']; ?></td>
                  <td class="text-right"><?php echo $logcenter_bill['total']+$logcenter_bill['difference']; ?></td>
                  <td class="text-right"><?php echo date('Y-m', strtotime($logcenter_bill['year_month'])); ?></td>
                  <td class="text-left"><?php echo $logcenter_bill['date_added']; ?></td>
                  <td class="text-left hidden"><?php echo $logcenter_bill['date_modified']; ?></td>
                  <td class="text-right">
                    <?php if($logcenter_bill['status']==0) { ?>
                    <button type="button" value="<?php echo $logcenter_bill['logcenter_bill_id']; ?>" id="button-delete<?php echo $logcenter_bill['logcenter_bill_id']; ?>" data-loading-text="加载中..." data-toggle="tooltip" title="删除" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    <?php } ?>
                    <a href="<?php echo URL('sale/logcenter_bill/view', 'token='.$token.'&logcenter_bill_id='.$logcenter_bill['logcenter_bill_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = 'index.php?route=sale/logcenter_bill&token=<?php echo $token; ?>';

  var filter_logcenter_bill_id = $('input[name=\'filter_logcenter_bill_id\']').val();

  if (filter_logcenter_bill_id) {
    url += '&filter_logcenter_bill_id=' + encodeURIComponent(filter_logcenter_bill_id);
  }

  var filter_logcenter_name = $('input[name=\'filter_logcenter_name\']').val();

  if (filter_logcenter_name) {
    url += '&filter_logcenter_name=' + encodeURIComponent(filter_logcenter_name);
  }

  var filter_logcenter_bill_status = $('select[name=\'filter_logcenter_bill_status\']').val();

  if (filter_logcenter_bill_status != '*') {
    url += '&filter_logcenter_bill_status=' + encodeURIComponent(filter_logcenter_bill_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_year_month = $('input[name=\'filter_year_month\']').val();

  if (filter_year_month) {
    url += '&filter_year_month=' + encodeURIComponent(filter_year_month);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_logcenter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/logcenter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['logcenter_name'],
            value: item['logcenter_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_logcenter_name\']').val(item['label']);
  }
});
//--></script>
  <script type="text/javascript"><!--
$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=sale/logcenter_bill/delete&token=<?php echo $token; ?>&logcenter_bill_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});

$('#button-export-lists').on('click', function(e) {
  url = 'index.php?route=sale/logcenter_bill/exportLists&token=<?php echo $token; ?>';

  var filter_logcenter_bill_id = $('input[name=\'filter_logcenter_bill_id\']').val();

  if (filter_logcenter_bill_id) {
    url += '&filter_logcenter_bill_id=' + encodeURIComponent(filter_logcenter_bill_id);
  }

  var filter_logcenter_name = $('input[name=\'filter_logcenter_name\']').val();

  if (filter_logcenter_name) {
    url += '&filter_logcenter_name=' + encodeURIComponent(filter_logcenter_name);
  }

  var filter_logcenter_bill_status = $('select[name=\'filter_logcenter_bill_status\']').val();

  if (filter_logcenter_bill_status != '*') {
    url += '&filter_logcenter_bill_status=' + encodeURIComponent(filter_logcenter_bill_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_year_month = $('input[name=\'filter_year_month\']').val();

  if (filter_year_month) {
    url += '&filter_year_month=' + encodeURIComponent(filter_year_month);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  location = url;
});
//--></script></div>
<?php echo $footer; ?>
