<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <?php if($status == 0){?>
      <button type="button" value="<?php echo $out_id; ?>" id="button-recover<?php echo $out_id; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="恢复出库单" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i></button>
      <?php }?>
      <?php if($status == 1){?>
      <button type="button" value="<?php echo $out_id; ?>" id="button-delete<?php echo $out_id; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="无效出库单" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
      <button type="button" value="<?php echo $out_id; ?>" id="button-complete<?php echo $out_id; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="完成出库单" class="btn btn-success"><i class="fa fa-play"></i></button>
      <?php }?>
      <a class="btn btn-info" href="<?php echo $print_url?>" target="_blank" data-original-title="打印表单" data-toggle="tooltip"><i class="fa fa-print"></i></a>
      <a class="btn btn-default" href="<?php echo $return_url?>"><i class="fa fa-reply"></i> 返回</a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
   </div>
   <div id="form-order"></div>
   <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <div class="panel-body">
          <dl class="dl-horizontal col-xs-3">
          <dt>销售单 ID</dt>
          <dd><?php echo $refer_id?></dd>
          <dt>单据类型</dt>
          <dd><?php echo $refer_type?></dd>
          <dt>仓库</dt>
          <dd><?php echo $warehouse?></dd>
          
          <dt>用户</dt>
          <dd><?php echo $username?></dd>

          <dt>总价（含优惠）</dt>
          <dd><?php echo $sale_money?></dd>
          <?php
          foreach ($OTList as $OTInfo) {
            echo '<dt style="color:red;">'.$OTInfo['title'].'</dt>';
            echo '<dd>'.$OTInfo['value'].'</dd>';
          }
          ?>
          </dl>

          <dl class="dl-horizontal col-xs-3">
          <dt>配送地址</dt>
          <?php if($refer_type_id == 1){?>
          <dd><?php echo $shipping_fullname?></dd>
          <dd><?php echo $shipping_company?></dd>
          <dd><?php echo $shipping_address?></dd>
          <dd><?php echo $shipping_city?></dd>
          <dd><?php echo $shipping_country?></dd>
          <dd><?php echo $shipping_zone?></dd>
          <?php }?>
          </dl>

          <dl class="dl-horizontal col-xs-3">
          <dt>状态</dt>
          <dd><?php echo $tsname?></dd>
          <dt>出库时间</dt>
          <dd><?php echo $out_date?></dd>
          <dt>备注</dt>
          <dd><?php echo $comment?></dd>
          </dl>

          <dl class="dl-horizontal col-xs-3">
          <dt>目的地仓库</dt>
          <?php if($refer_type_id == 1 || $refer_type_id == 4){?>
          <dd><?php echo $send_ware_name?></dd>
          <?php }?>
          <dt>管理员</dt>
          <?php if($refer_type_id == 1 || $refer_type_id == 4){?>
          <dd><?php echo $send_ware_manager?></dd>
          <?php }?>
          <dt>电话</dt>
          <?php if($refer_type_id == 1 || $refer_type_id == 4){?>
          <dd><?php echo $send_ware_contact_tel?></dd>
          <?php }?>
          <dt>地址</dt>
          <?php if($refer_type_id == 1 || $refer_type_id == 4){?>
          <dd><?php echo $send_ware_address?></dd>
          <?php }?>
          </dl>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_product_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_product_name; ?>"><?php echo $column_product_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p2.sku') { ?>
                    <a href="<?php echo $sort_sku; ?>" class="<?php echo strtolower($order); ?>">条形码</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sku; ?>">条形码</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'product_code') { ?>
                    <a href="<?php echo $sort_product_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_code; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_product_code; ?>"><?php echo $column_product_code; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'product_quantity') { ?>
                    <a href="<?php echo $sort_product_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_product_quantity; ?>"><?php echo $column_product_quantity; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'product_price') { ?>
                    <a href="<?php echo $sort_product_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_product_price; ?>"><?php echo $column_product_price; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'products_money') { ?>
                    <a href="<?php echo $sort_products_money; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_products_money; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_products_money; ?>"><?php echo $column_products_money; ?></a>
                    <?php } ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($stock_out_details) { ?>
                <?php foreach ($stock_out_details as $stock_out_detail) { ?>
                <tr>
                  <td class="text-left">
                  <?php echo $stock_out_detail['product_name']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out_detail['sku']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out_detail['product_code']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out_detail['product_quantity']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out_detail['product_price']?>
                  </td>
                  <td class="text-left">
                  <?php echo $stock_out_detail['products_money']?>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            <tfoot>
              <td colspan="6">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
              </td>
            </tfoot>
            </table>
            </div>

          <div class="row">
          </div>

        </div>
        <!--历史操作-->
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-comment-o"></i> 出库单历史</h3>
        </div>
        <div class="panel-body">
          <div class="tab-content">
                
            <table class="table table-bordered bhz-table">
                <thead>
                  <tr>
                          <th class="text-left">添加时间</th>
                          <th class="text-left">操作员</th>
                          <th class="text-left">操作内容</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($histories) { ?>
                        <?php foreach ($histories as $history) { ?>
                        <tr>
                          <td class="text-left"><?php echo $history['date_added']; ?></td>
                          <td class="text-left"><?php echo $history['operator_name']; ?></td>
                          <td class="text-left"><?php echo $history['comment']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td class="text-center" colspan="99">无记录</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
              
            </div>
        </div>
      </div><!--列表-->
   </div>
   <script>
    var token = '';

    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/login',
      type: 'post',
      data: 'key=<?php echo $api_key; ?>',
      dataType: 'json',
      crossDomain: true,
      success: function(json) {
            // $('.alert').remove();

            if (json['error']) {
            if (json['error']['key']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

                if (json['error']['ip']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
            }
            }

        if (json['token']) {
          token = json['token'];
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
   
    $('button[id^=\'button-delete\']').on('click', function(e) {
      if (confirm('相关销售单有可能会被打回待审核状态，是否无效')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/delete&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-recover\']').on('click', function(e) {
      if (confirm('是否恢复')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/recover&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });

    $('button[id^=\'button-complete\']').on('click', function(e) {
      if (confirm('是否完成')) {
        var node = this;

        $.ajax({
          url: '<?php echo $store; ?>index.php?route=api/stock_out/complete&token=' + token + '&out_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              alert(json['error']);
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if(json['success'])
              window.location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    });
   </script>