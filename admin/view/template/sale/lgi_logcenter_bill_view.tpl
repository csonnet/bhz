<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo URL('sale/lgi_logcenter_bill/export', 'token='.$token.'&logcenter_bill_id='.$logcenter_bill['logcenter_bill_id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a>
        <a href="<?php echo URL('sale/lgi_logcenter_bill', 'token='.$token) ?>" data-toggle="tooltip" title="返回" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1>物流营销中心对账单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 物流营销中心对账单</h3>
      </div>
      <div class="panel-body">
        <dl class="dl-horizontal col-xs-6">
          <dt>编号</dt>
          <dd><?php echo $logcenter_bill['logcenter_bill_id'] ?></dd>
          <dt>物流营销中心</dt>
          <dd><?php echo $logcenter_bill['logcenter_name']?></dd>
          <dt>创建时间</dt>
          <dd><?php echo $logcenter_bill['date_added']?></dd>
          <dt>对账月份</dt>
          <dd><?php echo date('Y-m', strtotime($logcenter_bill['year_month']))?></dd>
          <dt>状态</dt>
          <dd><?php echo getLogcenterBillStatus()[$logcenter_bill['status']]?></dd>
        </dl>

        <dl class="dl-horizontal col-xs-6">
          <dt>订单总金额</dt>
          <dd>{{<?php echo $logcenter_bill['order_total']?>|currency:"￥":2}}</dd>
          <br>
          <dt>物流费</dt>
          <dd>{{<?php echo $logcenter_bill['total']?>|currency:"￥":2}}</dd>
          <dt>差额</dt>
          <dd>{{<?php echo $logcenter_bill['difference']?>|currency:"￥":2}}</dd>
          <dt>应付物流费</dt>
          <dd>{{<?php echo $logcenter_bill['total']+$logcenter_bill['difference'] ?>|currency:"￥":2}}</dd>
        </dl>
      </div>
      <table class="table">
        <thead>
          <tr>
            <td>订单号</td>
            <td>收货人</td>
            <td>超市名称</td>
            <td>订单日期</td>
            <td>订单金额</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($logcenter_bill_orders as $key => $order) {?>
            <tr>
              <td><?php echo $order['order_id'] ?></td>
              <td><?php echo $order['fullname'] ?></td>
              <td><?php echo $order['shipping_company'] ?></td>
              <td><?php echo $order['date_added'] ?></td>
              <td><?php echo $order['total'] ?></td> 
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <!-- <h3 class="panel-title"><i class="fa fa-comment-o"></i> 对账单调整</h3> -->
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <div class="tab-pane active">
            <fieldset>
              <legend></legend>
              <form class="form-horizontal">
                <?php if(0) { ?>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-difference">修改差额</label>
                  <div class="col-sm-4">
                    <input name="difference" ng-model="difference" rows="8" id="input-difference" class="form-control" type="number"></input>
                  </div>
                  <div class="col-sm-2">
                    <button id="button-difference" data-loading-text="加载中..." ng-click="setLgBillDiff('<?php echo $logcenter_bill['logcenter_bill_id']?>', '<?php echo $token ?>')" class="btn btn-primary"><i class="fa fa-cny"></i> 修改差额</button>
                  </div>
                  <?php if($change_status_text) { ?>
                  <div class="col-sm-2 col-sm-offset-2">
                    <button id="button-change-status" data-loading-text="加载中..." ng-click="chgLgiLgBillStatus('<?php echo $logcenter_bill['logcenter_bill_id']?>', '<?php echo $token ?>')" class="btn btn-primary"><i class="fa fa-bell"></i> <?php echo $change_status_text; ?></button>
                  </div>
                  <?php } ?>
                </div>
                <?php } else { ?>
                  <?php if($change_status_text) { ?>
                  <div class="col-sm-2 col-sm-offset-10">
                    <button id="button-change-status" data-loading-text="加载中..." ng-click="chgLgiLgBillStatus('<?php echo $logcenter_bill['logcenter_bill_id']?>', '<?php echo $token ?>')" class="btn btn-primary"><i class="fa fa-bell"></i> <?php echo $change_status_text; ?></button>
                  </div>
                  <?php } ?>
                <?php } ?>
              </form>
            </fieldset>
          </div>
          </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-calendar"></i> 物流营销对账单历史</h3>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">添加时间</td>
                <td class="text-left">操作员</td>
                <td class="text-left">操作内容</td>
              </tr>
            </thead>
            <tbody>
              <?php if ($logcenter_bill_histories) { ?>
              <?php foreach ($logcenter_bill_histories as $history) { ?>
              <tr>
                <td class="text-left"><?php echo $history['date_added']; ?></td>
                <td class="text-left"><?php echo $history['operator_name']; ?></td>
                <td class="text-left"><?php echo $history['comment']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4">无记录</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<?php echo $footer; ?>
