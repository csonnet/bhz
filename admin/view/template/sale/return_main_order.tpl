<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">

    <div ng-controller="lycCtrl">

        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_save; ?>"   ng-click="savero('<?php echo $token ?>')"><i class="fa fa-check-circle"></i></button>
                    <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
                </div>
                <h1>退货单</h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <div class="container-fluid">

            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-list"></i> 退货单</h3>
                </div>

                <div class="panel-body">

                    <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">

                        <div class="dropdown col-xs-4">

                            <div class="form-group required {{warehouseError?'has-error':''}}">
                                <label class="control-label">选择入库仓库</label>
                                <select name="warehouse" id="return-warehouse" class="form-control">
                                    <option value="0">请选择</option>
                                    <?php foreach ($warehouses as $value) { ?>
                                    <option value="<?php echo $value['warehouse_id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="text-danger">{{warehouseError}}</div>
                            </div>

                        </div>

                        <div class="col-xs-4">
                            <div  class="form-group required {{marketError?'has-error':''}}">
                                <label class="control-label">退货超市</label>
                                <input name="market_name" type="text" id="market_name" class="form-control" placeholder="退货超市" />
                                <input name="market" type="hidden" id="market"  />
                                <div class="text-danger">{{marketError}}</div>
                            </div>
                        </div>

                        <div class="col-xs-4 form-group">
                            <div  class="">
                                <label class="control-label">包含订单号</label>
                                <input type="text" ng-model="included_order_ids" class="form-control" placeholder="包含订单号, 请以逗号隔开">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="25%">商品</th>
                                    <th>商品编码</th>
                                    <th>条形码</th>
                                    <th width="10%" class="text-center">箱入数</th>
                                    <th>选项</th>
                                    <th width="10%" class="text-center">数量</th>
                                    <th class="hidden">调拨单价</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="product in products">
                                    <td class="{{product.productError?'has-error':''}}">
                                        <div class="dropdown">
                                            <div  class="input-group" data-toggle="dropdown" aria-expanded="true" style="width:100%;">
                                                <input type="text" ng-change="getProduct(product.name, '<?php echo $token ?>')" ng-model="product.name" class="form-control" placeholder="商品" aria-describedby="basic-addon2">
                                            </div>
                                            <div class="text-danger">{{product.productError}}</div>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="width:100%;">
                                                <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                                                    <div class="checkbox">
                                                        <label>{{p.name}}</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{product.product_code}}</div>
                                    </td>
                                    <td>
                                        <div>{{product.sku}}</div>
                                    </td>
                                    <td class="text-center">
                                        <div>{{product.packing_no}}</div>
                                    </td>
                                    <td >
                                      <!--   <select ng-change="change_price(product, product.option)" class="form-control" ng-model="product.option" ng-if="product.option_list" ng-options="item.name for item in product.option_list.product_option_value"></select>
                                        <div class="text-danger">{{product.optionError}}</div> -->

                                           <div style="display: none;">{{product.product_option_value_id}}</div>
                                            <div>{{product.option_list}}</div>
                                    </td>
                                    <td class="{{product.qtyError?'has-error':''}}">
                                        <input type="text"  ng-model="product.qty" class="form-control" placeholder="数量" style="text-align:center;" />
                                        <div class="text-danger">{{product.qtyError}}</div>
                                    </td>
                                    <td class="{{product.priceError?'has-error':''}} hidden">
                                        <input type="text"  ng-model="product.unit_price" class="form-control" placeholder="单价" />
                                    </td>
                                    <td align="right">
                                        <button type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="99" align="right"><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                                </tr>
                            </tfoot>
                        </table>

                    </form>

                    <div class="row"></div>

                </div>

            </div>

        </div>

    </div>

</div>

<script>

	<!--退货超市autocomplete-->
	$('input[name=\'market_name\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=sale/return_main_order/marketAC&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['company_name']+'('+item['telephone']+')',
							value: item['customer_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'market_name\']').val(item['label']);
			$('input[name=\'market\']').val(item['value']);
		}
	});
	<!--退货超市autocomplete-->

</script>

<?php echo $footer; ?>
