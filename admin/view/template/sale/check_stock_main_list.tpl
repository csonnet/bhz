<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <div class="row">
            <div class="form-group">

          </div>
        </div>
      </div>
      <h1>库存查看</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 库存查看</h3>
      </div>
      <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">供应商</label>
                <input type="text" name="filter_vendor_name" value="<?php echo $filter_vendor_name; ?>" placeholder="供应商" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div> 
              <div class="form-group">
                <label class="control-label" for="input-total">条形码</label>
                <input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" placeholder="条形码" id="input-total" class="form-control" />
              </div>
              
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-total">物流中心</label>
                <select name="filter_logcenter" id="input-src-status" class="form-control">
                  <?php  foreach ($logcenters as $key => $value) {?>
                  <option value="<?php echo $key; ?>" <?php if($filter_logcenter==$key){echo 'selected="selected"'; } ?> ><?php echo $value ; ?></option>
                  <?php } ?>
                </select>
                
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">型号</label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="型号" id="input-total" class="form-control" />
              </div>
              <div class="form-group">
                <a id="button-export-tpl" data-toggle="tooltip" title="导出模板" class="btn btn-primary"><i class="fa fa-calculator"></i> 导出模板</a>

                <a id="button-upload" data-toggle="tooltip" title="导入调拨单" class="btn btn-primary"><i class="fa fa-calculator"></i> 导入库存</a>
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
                <button type="button" id="button-export-out" class="btn btn-primary pull-right"><i class="fa fa-search"></i>导出库存</button>

              </div>
            </div>
          </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead id="thead-scroll">
                <tr class="thead-scroll">
                  <td id="htd1">条形码</td>
                  <td>商品名称</td>
                  <td>选项</td>
                  <td>成本价</td>
                  <td>售价</td>
                  <td>建议零售价</td>
                  <td>期初数</td>
                  <td>采购数量</td>
                  <td>销售数量</td>
                  <td>退货数量</td>
                  <td>调进数量</td>
                  <td>调出数量</td>
                  <td>调整数量</td>
                  <td>库存变动</td>
                  <td><?php if ($sort == 'total_real_initiai') { ?>
                    <a href="<?php echo $sort_initiai; ?>" class="<?php echo strtolower($order); ?>">实时库存</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_initiai; ?>">实时库存</a>
                    <?php } ?></td>
                  <td>库存余额</td>
                  <td>操作</td>
                 <!--  <td>操作</td> -->
                </tr>
              </thead>
              <tbody id="tbody-scroll">
                <?php if ($product_list) { ?>
                <?php foreach ($product_list as $key => $product) { ?>
                <tr class="tbody-scroll">
                  <td id="td1"><?php echo $product['sku'] ?></td>
                  <td><?php echo $product['name'] ?></td>
                  <td><?php echo $product['option_name'] ?></td>
                  <td><?php echo $product['product_cost'] ?></td>
                  <td><?php echo $product['special'] ?></td>
                  <td><?php echo $product['price'] ?></td>
                  <td><?php echo $product['initiai'] ?></td>
                  <td><?php echo $product['po'] ?></td>
                  <td><?php echo $product['order'] ?></td>
                  <td><?php echo $product['ro'] ?></td>
                  <td><?php echo $product['des_requisition'] ?></td>
                  <td><?php echo $product['src_requisition'] ?></td>
                  <td><?php echo $product['trim'] ?></td>
                  <td><?php echo $product['change'] ?></td>
                  <td><?php echo $product['balance'] ?></td>
                  <td><?php $total = $product['product_cost']*$product['balance']; echo $total; ?></td>
                  <td>
                  <!-- <a onclick="viewHref= '<?php $sku_url=$view_url.'&filter_sku='.$product['sku']; echo  $sku_url; ?>'" href="javascript:void(0)" data-toggle="modal" data-target=".bs-example-modal-lg" title="查看" class="btn btn-info view_btn"><i class="fa fa-eye"></i></a> -->
                  <a href="<?php $road_url=$export_url.'&sku='.$product['sku']; echo  $road_url; ?>" data-toggle="modal" title="导出操作记录" class="btn btn-info"><i class="fa fa-print"></i></a>
                  </td>
                
                </tr>

                <?php } ?>

                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="6">没有库存查看数据</td>
                </tr>
                <?php } ?>

                <div id="view_page_cat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg"><div class="modal-content"></div></div>
                </div>


              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=sale/requisition_main/delete&token=<?php echo $token; ?>&requisition_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-da  nger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            window.location.reload();
        }
        
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('#button-export-tpl').on('click', function(e) {
  url = 'index.php?route=sale/check_main_stock/exportTpl&token=<?php echo $token; ?>';
  location = url;
});

$('#button-upload').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);   
      
      $.ajax({
        url: 'index.php?route=sale/check_main_stock/import&token=<?php echo $token; ?>',
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
                
          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            window.location.reload();
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

$('.view_btn').on('click', function() {
  $(this).parent().parent().addClass('red_word');
  $(this).parent().parent().siblings().removeClass("red_word");
  $.ajax({
    url: viewHref,
    type: 'post',
    dataType: 'html',
    success: function(html) {

        $('#view_page_cat').html(html);

      },

    });
  });

// $(document).ready(function(){

// $(".sticky-header").floatThead({scrollingTop:0});

// });

$('#button-filter').on('click', function() {
  url = 'index.php?route=sale/check_main_stock&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_logcenter = $('select[name=\'filter_logcenter\']').val();

  if (filter_logcenter) {
    url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_sku = $('input[name=\'filter_sku\']').val();

  if (filter_sku) {
    url += '&filter_sku=' + encodeURIComponent(filter_sku);
  }

  var filter_model = $('input[name=\'filter_model\']').val();

  if (filter_model) {
    url += '&filter_model=' + encodeURIComponent(filter_model);
  }

  // var filter_product_name = $('input[name=\'filter_product_name\']').val();

  // if (filter_product_name) {
  //   url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
  // }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }

  location = url;
});

$('#button-export-out').on('click', function() {
  url = 'index.php?route=sale/check_main_stock/exportAllList&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_logcenter = $('select[name=\'filter_logcenter\']').val();

  if (filter_logcenter) {
    url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_sku = $('input[name=\'filter_sku\']').val();

  if (filter_sku) {
    url += '&filter_sku=' + encodeURIComponent(filter_sku);
  }

  var filter_model = $('input[name=\'filter_model\']').val();

  if (filter_model) {
    url += '&filter_model=' + encodeURIComponent(filter_model);
  }

  // var filter_product_name = $('input[name=\'filter_product_name\']').val();

  // if (filter_product_name) {
  //   url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
  // }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }

  location = url;
});

$('.date').datetimepicker({
  pickTime: false
});


$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});

$('#button-vendor-bill').on('click', function(e) {
  var vendor_bill_month = $('#vendor_bill_month').val();
  if(!vendor_bill_month || isNaN(Date.parse(vendor_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/return_logcenter_order/genVendorBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+vendor_bill_month;
});
// $('#button-export-lists').on('click', function(e) {
//   url = 'index.php?route=sale/return_logcenter_order/exportLists&token=<?php echo $token; ?>';
//   var filter_date_start = $('input[name=\'filter_date_start\']').val();

//   if (filter_date_start) {
//     url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
//   }
//   var filter_product_name = $('input[name=\'filter_product_name\']').val();

//   if (filter_product_name) {
//     url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
//   }

//   var filter_date_end = $('input[name=\'filter_date_end\']').val();

//   if (filter_date_end) {
//     url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
//   }

//   var filter_logcenter = $('input[name=\'filter_logcenter\']').val();

//   if (filter_logcenter) {
//     url += '&filter_logcenter=' + encodeURIComponent(filter_logcenter);
//   }

//   var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

//   if (filter_vendor_name) {
//     url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
//   }

//   location = url;
// });
  // var b = $("#thead-scroll").offset().top;
// $(window).scroll(function(event){
//   var a = $(document).scrollTop();
//   var c = b-a;
  
//   if(c<0){
//     $("#thead-scroll").addClass('div-fixed');
//     $(".thead-scroll td").each(function(c,i){
//       var thead_td=$(this);
//       $(".tbody-scroll td").each(function(c,j){
//         var tbody_td=$(this);
//           thead_td.outerWidth(tbody_td.outerWidth());
//       })
//     })
//   }else{
//     $("#thead-scroll").removeClass('div-fixed');
//   }

//   var d = $('#td1').outerWidth();
//   console.log(d);
//   $("#htd1").css("width",d);
// });


$('input[name=\'filter_product_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/mvd_product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['name']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_product_name\']').val(item['label']);
  }
});
$('input[name=\'filter_sku\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/mvd_product/autocomplete&token=<?php echo $token; ?>&filter_sku=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['sku'],
            value: item['sku']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_sku\']').val(item['label']);
  }
});
//--></script>
<?php echo $footer; ?>
