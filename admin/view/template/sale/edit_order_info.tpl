<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_edit; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $text_order_detail; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_store; ?>" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_name; ?></a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_date_added; ?>" class="btn btn-info btn-xs"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_payment_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td>
                	<!--<?php echo $payment_method; ?>-->
                    <select name="payment_method" id="input-payment-method">
                        <?php foreach ($payment_methods as $value) { ?>
                        <?php if ($value['code'] == $payment_code) { ?>
                        <option value="<?php echo $value['code']; ?>" selected="selected"><?php echo $value['name']; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $value['code']; ?>"><?php echo $value['name']; ?></option>>
                        <?php } ?>
                        <?php } ?>
                    </select>
				</td>
              </tr>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_shipping_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i></button></td>
                <td><?php echo $shipping_method; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="推荐码" class="btn btn-info btn-xs"><i class="fa fa-street-view fa-fw"></i></button></td>
                <td>  <?php echo $recommended_code; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="是否付款" class="btn btn-info btn-xs"><i class="fa fa-exclamation fa-fw"></i></button></td>
                <td>  <?php echo $is_pay; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_customer_detail; ?></h3>
          </div>
          <table class="table">
            <tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_customer; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
              <td><?php if ($customer) { ?>
                <a href="<?php echo $customer; ?>" target="_blank"><?php echo $fullname; ?></a>
                <?php } else { ?>
                <?php echo $fullname; ?>
                <?php } ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_shipping_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-mobile fa-fw"></i></button></td>
              <td><input id="shipping_telephone" type="text" value="<?php echo $shipping_telephone; ?>" /></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_customer_group; ?>" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
              <td>
              	<!--<?php echo $customer_group; ?>-->
                 <select name="customer_group_id" id="input-customer-group">
					<?php foreach ($customer_groups as $value) { ?>
					<?php if ($value['customer_group_id'] == $customer_group_id) { ?>
					<option value="<?php echo $value['customer_group_id']; ?>" selected="selected"><?php echo $value['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $value['customer_group_id']; ?>"><?php echo $value['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
				</select>
               </td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_email; ?>" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-phone fa-fw"></i></button></td>
              <td><?php echo $telephone; ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_option; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr class="hidden">
                <td><?php echo $text_invoice; ?></td>
                <td id="invoice" class="text-right"><?php echo $invoice_no; ?></td>
                <td style="width: 1%;" class="text-center"><?php if (!$invoice_no) { ?>
                  <button id="button-invoice" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_generate; ?>" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_reward; ?></td>
                <td class="text-right"><?php echo $reward; ?></td>
                <td class="text-center"><?php if ($customer && $reward) { ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_affiliate; ?>
                  <?php if ($affiliate) { ?>
                  (<a href="<?php echo $affiliate; ?>"><?php echo $affiliate_fullname; ?></a>)
                  <?php } ?></td>
                <td class="text-right"><?php echo $commission; ?></td>
                <td class="text-center"><?php if ($affiliate) { ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_is_invoice; ?></td>
                <td id="is_invoice" class="text-right"><?php echo $is_invoice_text; ?></td>
                <td class="text-center"><?php if ($customer) { ?>
                  <?php if ($is_invoice == 1) { ?>
                  <button id="button-is_invoice-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } elseif ($is_invoice == 2) { ?>
                  <button id="button-is_invoice-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip"  class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left"><?php echo $text_payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?>
                <?php } ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo $payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td class="text-left"><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td>序号</td>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-left">品牌厂商</td>
              <td class="text-left"><?php echo $column_sku; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right">签收数量</td>
              <?php if($verify_status!=0){?>
              <td class="text-right">操作签收数量</td>
              <?php }else{?>
              <td class="text-right">操作数量</td>
              <?php }?>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td class="text-right"><!--<?php echo $column_operation; ?>--></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td><?php echo $product['order_ids']; ?></td>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                &nbsp;
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                <?php if ($option['type'] != 'file') { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } else { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                <?php } ?>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['vname']; ?></td>
              <td class="text-left"><?php echo $product['sku']; ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['sign_quantity']; ?></td>
              <?php if($verify_status!=0){?>
                <?php if(!empty($product['childproducts'])){ ?>
                  <td class="text-right"><input data-product_type="<?php echo $product['product_type'];?>" data-product-id="<?php echo $product['order_product_id'];?>" type="number" class="form-control product" value="<?php echo $product['sign_quantity'];?>" sign_quantity="<?php echo $product['sign_quantity']; ?>" product-name="<?php echo $product['name']; ?>" disabled ></td>
                    
                <?php }else{?>
                  <td class="text-right"><input data-product_type="<?php echo $product['product_type'];?>" data-product-id="<?php echo $product['order_product_id'];?>" type="number" class="form-control product" value="<?php echo $product['sign_quantity'];?>" sign_quantity="<?php echo $product['sign_quantity']; ?>" product-name="<?php echo $product['name']; ?>"></td>
                <?php }?>
     
              <?php }else{?>
               
                  <td class="text-right"><input data-product-id="<?php echo $product['order_product_id'];?>" type="number" class="form-control product" value="<?php echo $product['quantity'];?>" quantity="<?php echo $product['quantity']; ?>" product-name="<?php echo $product['name']; ?>"></td>
              <?php }?>

                

              <?php?>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
              <td class="text-center">
              <!--
                  <?php if ($customer && $product) { ?>
                  <?php if (!$product['ship_status']) { ?>
                  <button class="btn btn-danger btn-xs">未发货</i></button>
                  <?php } else { ?>
                  <button class="btn btn-success btn-xs">已发货</i></button>
                  <?php }?>
                  <?php }?>
              -->
              </td>
            </tr>
            <?php if((!empty($product['childproducts']))&&$verify_status!=0){?>
              <tr style="font-weight: bold;">
                <td></td>
                <td class="text-center" colspan="8">组合商品</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>序号</td>
                <td class="text-left"><?php echo $column_product; ?></td>
                <td class="text-left"><?php echo $column_sku; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                <?php if($verify_status!=0){?>
                <td class="text-right">操作签收数量</td>
                <?php }else{?>
                <td class="text-right">操作数量</td>
                <?php }?>
                 <td class="text-right">签收数量</td>
                <!-- <td class="text-right">未发货数量</td>
                <td class="text-right">缺货数量</td> -->
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
                <!--
                <td></td>
                -->
              </tr>
            <?php foreach($product['childproducts'] as $key2 => $product2){?>
              <tr>
              <td></td>
              <td><?php echo $product['order_ids'].'-'.$product2['order_ids']; ?></td>
              <td class="text-left"><?php echo $product2['pdname']?></td>
              <td class="text-left"><?php echo $product2['sku']?></td>
              <td class="text-left"><?php echo $product2['model']?></td>
              <td class="text-right"><?php echo $product2['quantity']?></td>
              <td class="text-right"><?php echo $product2['sign_quantity']?></td>
              <td class="text-right"><input data-product2-id="<?php echo $product2['order_product_group_id'];?>" type="number" class="form-control product<?php echo $product['order_product_id'];?>" value="<?php echo $product2['sign_quantity'];?>" sign_quantity="<?php echo $product2['sign_quantity']; ?>" product-name="<?php echo $product2['name']; ?>"></td>
              <td class="text-right">￥<?php echo round($product2['price'],2)?></td>
              <td class="text-right">￥<?php echo round($product2['total'],2)?></td>
              <td></td>
              </tr>
            <?php }?>
            <?php }?>
            <?php }?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="9" class="text-right"><?php echo $total['title']; ?></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
              <td class="text-right"></td>
            </tr>
            <?php } ?>
            <tr>
              <td colspan="9" class="text-right">签收金额</td>
              <td class="text-right"><?php echo $sign_price; ?></td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td colspan="9" class="text-right">收款金额</td>
              <td class="text-right"><?php echo $conform_price; ?></td>
              <td class="text-right"></td>
            </tr>
            <!--
            <tr>
              <td colspan="7" class="text-right"></td>
              <td class="text-center">
              </td>
              <td class="text-center">
                <?php if($total_ship_status){?>
                <button  class="btn btn-success btn-xs">已全部发货</i></button>
                <?php }else{ ?>
                <button  class="btn btn-danger btn-xs">尚有未发货</i></button>
                <?php }?>
              </td>
            </tr>
            -->
          </tbody>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div>
    <?php if($verify_status==0){?>
    <button id="button-save" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> 确认修改</button>
    <?php }else{?>
       <button id="button-sign" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> 确认修改签收</button>
    <?php }?>
  </div>

  <script type="text/javascript"><!--


$(document).delegate('#button-save', 'click', function() {
    var node = this;
    var products =[];
  $('.product').each(function(i,dom){
     c = $(dom).attr("quantity");
     a = $(dom).attr("data-product-id");
     b = $(dom).val();
     //d = $(dom).attr('product-name');
      products[i]={};
      products[i].num=b;
      products[i].id=a;
      products[i].oldnum=c;
      //products[i].name=d;
  });
     var _obj={};
     _obj.obj=products;
	 
	var status = {
		shipping_telephone: $("#shipping_telephone").val(),
		customer_group_id: $("#input-customer-group").val(),
		payment_code: $("#input-payment-method").val(),
		payment_method: $("#input-payment-method").find("option:selected").text()
	};
	_obj.status = status;

    $.ajax({
        url: 'index.php?route=sale/order/editOrder&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
        type: 'post',
        data:_obj,
        //contentType:'application/json',
        //processData:false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.alert').remove();

          if (json['error']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
          }

          if (json['success']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
            window.location.href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id="+json['order_id'];
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
	
});

$(document).delegate('#button-sign', 'click', function() {
    var node = this;
    var products =[];
    var z =0;
  $('.product').each(function(i,dom){
     c = $(dom).attr("sign_quantity");
     t = $(dom).attr("data-product_type");
     a = $(dom).attr("data-product-id");
     b = $(dom).val();
     //d = $(dom).attr('product-name');
     // var str = 'product'+a;
     // console.log(t);
     if (t==2) {
          // console.log(3);

        console.log($('.'+'product'+a));
        $('.'+'product'+a).each(function(j,domj){
          // console.log(4);
           c = $(domj).attr("sign_quantity");
           a = $(domj).attr("data-product2-id");
           b = $(domj).val();
           //d = $(dom).attr('product-name');
            products[z]={};
            products[z].num=b;
            products[z].pgid=a;
            products[z].oldnum=c;
            products[z].type=2;
            z++;

            //products[z].name=d;
        });
     }else{

        products[z]={};
        products[z].num=b;
        products[z].id=a;
        products[z].oldnum=c;
        products[z].type=1;
        z++;

     }
  
      //products[i].name=d;
  });

  // console.log(products);
     var _obj={};
     _obj.obj=products;
   
  var status = {
    shipping_telephone: $("#shipping_telephone").val(),
    customer_group_id: $("#input-customer-group").val(),
    payment_code: $("#input-payment-method").val(),
    payment_method: $("#input-payment-method").find("option:selected").text()
  };
  _obj.status = status;

    $.ajax({
        url: 'index.php?route=sale/order/editsign&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
        type: 'post',
        data:_obj,
        //contentType:'application/json',
        //processData:false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.alert').remove();

          if (json['error']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
          }

          if (json['success']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
            window.location.href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id="+json['order_id'];
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
  
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-is_invoice-add', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/saveIs_invoice&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: {is_invoice_id:"2"},
    dataType: 'json',
    beforeSend: function() {
      $('#button-is_invoice-add').button('loading');
    },
    complete: function() {
      $('#button-is_invoice-add').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                $('#is_invoice').html(json['success']);
                

        $('#button-is_invoice-add').replaceWith('<button id="button-is_invoice-remove" data-toggle="tooltip"  class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-is_invoice-remove', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/saveIs_invoice&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: {is_invoice_id:"1"},
    dataType: 'json',
    beforeSend: function() {
      $('#button-is_invoice-remove').button('loading');
    },
    complete: function() {
      $('#button-is_invoice-remove').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                $('#is_invoice').html(json['success']);
        $('#button-is_invoice-remove').replaceWith('<button id="button-is_invoice-add" data-toggle="tooltip"  class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var token = '';

// Login to the API
$.ajax({
	url: '<?php echo $store_url; ?>index.php?route=api/login',
	type: 'post',
	dataType: 'json',
	data: 'key=<?php echo $api_key; ?>',
	crossDomain: true,
	success: function(json) {
		$('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

        if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
	if (typeof verifyStatusChange == 'function'){
		if (verifyStatusChange() == false){
			return false;
		} else{
			addOrderInfo();
		}
	} else{
		addOrderInfo();
	}

	$.ajax({
		url: '<?php echo $store_url; ?>index.php?route=api/order/history&token=' + token + '&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&sent_comment_to_all=' + ($('input[name=\'sent_comment_to_all\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),

		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#button-pay').on('click', function() {
  $.ajax({
    url: '<?php echo $store_url; ?>index.php?route=api/order/confirmPay&token=' + token + '&order_id=<?php echo $order_id; ?>',
    type: 'post',
    dataType: 'json',
    data: '',

    beforeSend: function() {
      $('#button-history').button('loading');
    },
    complete: function() {
      $('#button-history').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
        $('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
        $('.confirm-pay').hide();
        $('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        window.location.reload();
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

function changeStatus(){
	var status_id = $('select[name="order_status_id"]').val();

	$('#openbay-info').remove();

	$.ajax({
		url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		dataType: 'html',
		success: function(html) {
			$('#history').after(html);
		}
	});
}

function addOrderInfo(){
	var status_id = $('select[name="order_status_id"]').val();

	$.ajax({
		url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		type: 'post',
		dataType: 'html',
		data: $(".openbay-data").serialize()
	});
}

$(document).ready(function() {
	changeStatus();
});

$('select[name="order_status_id"]').change(function(){
	changeStatus();
});
//--></script>
</div>
<?php echo $footer; ?>
