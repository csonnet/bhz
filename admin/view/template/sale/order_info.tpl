<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $edit_order; ?>" target="_blank" data-toggle="tooltip" title="编辑订单" class="btn btn-info"><i class="fa fa-edit"></i></a> 
        <!--<a href="<?php echo $export_order; ?>" target="_blank" data-toggle="tooltip" title="打印四联单" class="btn btn-info"><i class="fa fa-file-text"></i></a>--> 
        <a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></a> 
        <a href="<?php echo $shipping; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info hidden"><i class="fa fa-truck"></i></a>
        <a href="<?php echo $export; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></a> 
        <!-- <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> --> 
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $text_order_detail; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_store; ?>" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_name; ?></a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_date_added; ?>" class="btn btn-info btn-xs"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_payment_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td><?php echo $payment_method; ?></td>
              </tr>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_shipping_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i></button></td>
                <td><?php echo $shipping_method; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="推荐码" class="btn btn-info btn-xs"><i class="fa fa-street-view fa-fw"></i></button></td>
                <td>  <?php echo $recommended_code; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="是否付款" class="btn btn-info btn-xs"><i class="fa fa-exclamation fa-fw"></i></button></td>
                <td>  <?php echo $is_pay; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_customer_detail; ?></h3>
          </div>
          <table class="table">
            <tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_customer; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
              <td><?php if ($customer) { ?>
                <a href="<?php echo $customer; ?>" target="_blank"><?php echo $fullname; ?></a>
                <?php } else { ?>
                <?php echo $fullname; ?>
                <?php } ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_shipping_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-mobile fa-fw"></i></button></td>
              <td><?php echo $shipping_telephone; ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_customer_group; ?>" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
              <td><?php echo $customer_group; ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_email; ?>" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-phone fa-fw"></i></button></td>
              <td><?php echo $telephone; ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_option; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr class="hidden">
                <td><?php echo $text_invoice; ?></td>
                <td id="invoice" class="text-right"><?php echo $invoice_no; ?></td>
                <td style="width: 1%;" class="text-center"><?php if (!$invoice_no) { ?>
                  <button id="button-invoice" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_generate; ?>" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_reward; ?></td>
                <td class="text-right"><?php echo $reward; ?></td>
                <td class="text-center"><?php if ($customer && $reward) { ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_affiliate; ?>
                  <?php if ($affiliate) { ?>
                  (<a href="<?php echo $affiliate; ?>"><?php echo $affiliate_fullname; ?></a>)
                  <?php } ?></td>
                <td class="text-right"><?php echo $commission; ?></td>
                <td class="text-center"><?php if ($affiliate) { ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_is_invoice; ?></td>
                <td id="is_invoice" class="text-right"><?php echo $is_invoice_text; ?></td>
                <td class="text-center"><?php if ($customer) { ?>
                  <?php if ($is_invoice == 1) { ?>
                  <button id="button-is_invoice-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } elseif ($is_invoice == 2) { ?>
                  <button id="button-is_invoice-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip"  class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left"><?php echo $text_payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?>
                <?php } ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo $payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td class="text-left"><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td>序号</td>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-left">品牌厂商</td>
              <td class="text-left"><?php echo $column_sku; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right">签收数量</td>
              <td class="text-right">未发货数量</td>
              <td class="text-right">缺货数量</td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <!--
              <td class="text-right"><?php echo $column_operation; ?></td>
              -->
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $key => $product) { ?>
            <tr>
              <td><?php echo $product['order_ids']; ?></td>
              <td class="text-left"><?php echo $product['name']; ?></a>
              <!-- <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a> -->
                &nbsp;
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                <?php if ($option['type'] != 'file') { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } else { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                <?php } ?>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['vname']; ?></td>
              <td class="text-left"><?php echo $product['sku']; ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['sign_quantity']; ?></td>
              <td class="text-right">
              <?php
              if ($product['lack_quantity']) {
                echo '<span style="color:red;">'.$product['lack_quantity'].'</span>';
              }
              ?>
              </td>
              <td class="text-right">
              <?php
              if ($product['needed_quantity']) {
                echo '<span style="color:red;">'.$product['needed_quantity'].'</span>';
              }
              ?>
              </td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
              <!--
              <td class="text-center"><?php if ($customer && $product) { ?>
                  <?php if (!$product['ship_status']) { ?>
                  <button class="btn btn-danger btn-xs">未发货</i></button>
                  <?php } else { ?>
                  <button class="btn btn-success btn-xs">已发货</i></button>
                  <?php }?>
                  <?php }?>
              </td>
              -->
            </tr>
            <?php if(!empty($product['childproducts'])){?>
              <tr style="font-weight: bold;">
                <td></td>
                <td class="text-center" colspan="8">组合商品</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>序号</td>
                <td class="text-left"><?php echo $column_product; ?></td>
                <td class="text-left"><?php echo $column_sku; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                 <td class="text-right">签收数量</td>
                <td class="text-right">未发货数量</td>
                <td class="text-right">缺货数量</td>
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
                <!--
                <td></td>
                -->
              </tr>
            <?php foreach($product['childproducts'] as $key2 => $product2){?>
              <tr>
              <td></td>
              <td><?php echo $product['order_ids'].'-'.$product2['order_ids']; ?></td>
              <td class="text-left"><?php echo $product2['pdname']?></td>
              <td class="text-left"><?php echo $product2['sku']?></td>
              <td class="text-left"><?php echo $product2['model']?></td>
              <td class="text-right"><?php echo $product2['quantity']?></td>
              <td class="text-right"><?php echo $product2['sign_quantity']?></td>
              <td class="text-right">
              <?php
              if ($product2['lack_quantity']) {
                echo '<span style="color:red;">'.$product2['lack_quantity'].'</span>';
              }
              ?>
              </td>
              <td class="text-right">
              <?php
              if ($product2['needed_quantity']) {
                echo '<span style="color:red;">'.$product2['needed_quantity'].'</span>';
              }
              ?>
              </td>
              <td class="text-right">￥<?php echo round($product2['price'],2)?></td>
              <td class="text-right">￥<?php echo round($product2['total'],2)?></td>
              <td></td>
              </tr>
            <?php }?>
            <?php }?>
            <?php }?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="9" class="text-right"><?php echo $total['title']; ?></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
              <td class="text-right"></td>
            </tr>
            <?php } ?>
            <tr>
              <td colspan="9" class="text-right">签收金额</td>
              <td class="text-right"><?php echo $sign_price; ?></td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td colspan="9" class="text-right">收款金额</td>
              <td class="text-right"><?php echo $conform_price; ?></td>
              <td class="text-right"></td>
            </tr>
            <!--
            <tr>
              <td colspan="8" class="text-right"></td>
              <td class="text-center">
              </td>
              <td class="text-center">
                <?php if($total_ship_status){?>
                <button  class="btn btn-success btn-xs">已全部发货</i></button>
                <?php }else{ ?>
                <button  class="btn btn-danger btn-xs">尚有未发货</i></button>
                <?php }?>
              </td>
            </tr>
            -->
          </tbody>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-body">

        <div class="form-group">
          <label class="col-sm-2" for="input-remarks">签收金额和本订单应收金额差异说明:</label>
          <div >
           <span style="color: red;"><?php echo $remarks; ?></span> 
          </div>
        </div>
      </div>
    </div>
    <!--退款-->
    <?php if ($tradeno_status && '69' == $user_id) {;//仅限财务账号操作 ?>
    <div class="panel panel-default" id="refund">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i>  退款</h3>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <div class="tab-pane active">
            <fieldset>
              <legend></legend>
              <form class="form-horizontal" onsubmit="return false">
              <?php
              if (count($refundInfo['applied'])) {
              ?>
              <table class="table table-bordered">
              <thead>
              <tr>
                <td>申请时间</td>
                <td>类型</td>
                <td>退款金额</td>
                <td>退款状态</td>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($refundInfo['applied'] as $info) {
                  echo '<tr><td>'.$info['dateAdded'].'</td><td>'.$info['refundType'].'</td><td>'.$info['refundAmount'].'</td><td>'.$info['status'].'</td></tr>';
              }
              ?>
              </tbody>
              </table>
              <?php
              }
              ?>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="refund_amount">缺货退款</label>
                  <div class="col-sm-4">
                    <input name="refund_amount_1" id="refund_amount_1" class="form-control" type="text" value="<?php echo $refundInfo['unApplied']; ?>"></input>
                  </div>
                  <div class="col-sm-2">
                    <button data-loading-text="加载中..." class="btn btn-primary" onclick = "refundmoney(1)"><i class="fa fa-cny"></i> 确认退款</button>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="refund_amount">春节抽奖返现</label>
                  <div class="col-sm-4">
                    <input name="refund_amount_2" id="refund_amount_2" class="form-control" type="text" value="<?php echo $luckDrawAmount; ?>"></input>
                  </div>
                  <div class="col-sm-2">
                    <button data-loading-text="加载中..." class="btn btn-primary" onclick = "refundmoney(2)"><i class="fa fa-cny"></i> 确认退款</button>
                  </div>
                </div>
              </form>
            </fieldset>
          </div>
          </div>
      </div>
    </div>
    <?php } ?>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> <?php echo $text_history; ?></h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
          <li><a href="#tab-additional" data-toggle="tab"><?php echo $tab_additional; ?></a></li>
          <?php foreach ($tabs as $tab) { ?>
          <li><a href="#tab-<?php echo $tab['code']; ?>" data-toggle="tab"><?php echo $tab['title']; ?></a></li>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-history">
            <div id="history"></div>
            <br />
            <fieldset>
              <legend><?php echo $text_history_add; ?></legend>
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                  <div class="col-sm-10">
                    <select name="order_status_id" id="input-order-status" class="form-control">
                      <?php foreach ($order_statuses as $order_statuses) { ?>
                      <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-override"><span data-toggle="tooltip" title="<?php echo $help_override; ?>"><?php echo $entry_override; ?></span></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="override" value="1" id="input-override" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify"><?php echo $entry_notify; ?></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="notify" value="1" id="input-notify" />
                    <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="input-sent-comment-to-all"><span data-toggle="tooltip" title="<?php echo $help_sent_comment_to_all; ?>"><?php echo $entry_sent_comment_to_all; ?></span></label>
                        <div class="col-sm-10">
                        <input type="checkbox" name="sent_comment_to_all" value="1" id="input-sent-comment-to-all" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
              </form>
            </fieldset>
            <div class="text-right">
              <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
            </div>
          </div>

          
          <div class="confirm-pay tab-pane active <?php if(!$have_permission){ echo 'hidden';}?>" id="tab-history">
            <br />
            <fieldset>
              <legend>确认付款</legend>
            </fieldset>
            <div class="text-right">
              <button id="button-pay" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> 确认客户已付款</button>
            </div>
          </div>
          
          <div class="tab-pane" id="tab-additional">
            <?php if ($account_custom_fields) { ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td colspan="2"><?php echo $text_account_custom_field; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($account_custom_fields as $custom_field) { ?>
                <tr>
                  <td><?php echo $custom_field['name']; ?></td>
                  <td><?php echo $custom_field['value']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
            <?php if ($payment_custom_fields) { ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td colspan="2"><?php echo $text_payment_custom_field; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($payment_custom_fields as $custom_field) { ?>
                <tr>
                  <td><?php echo $custom_field['name']; ?></td>
                  <td><?php echo $custom_field['value']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
            <?php if ($shipping_method && $shipping_custom_fields) { ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td colspan="2"><?php echo $text_shipping_custom_field; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($shipping_custom_fields as $custom_field) { ?>
                <tr>
                  <td><?php echo $custom_field['name']; ?></td>
                  <td><?php echo $custom_field['value']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td colspan="2"><?php echo $text_browser; ?></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo $text_ip; ?></td>
                  <td><?php echo $ip; ?></td>
                </tr>
                <?php if ($forwarded_ip) { ?>
                <tr>
                  <td><?php echo $text_forwarded_ip; ?></td>
                  <td><?php echo $forwarded_ip; ?></td>
                </tr>
                <?php } ?>
                <tr>
                  <td><?php echo $text_user_agent; ?></td>
                  <td><?php echo $user_agent; ?></td>
                </tr>
                <tr>
                  <td><?php echo $text_accept_language; ?></td>
                  <td><?php echo $accept_language; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <?php foreach ($tabs as $tab) { ?>
          <div class="tab-pane" id="tab-<?php echo $tab['code']; ?>"><?php echo $tab['content']; ?></div>
          <?php } ?>
        </div>
      </div>
    </div>

    <!--相关单据-->
    <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-arrows-h"></i> 相关出库单</h3>
    </div>
    <div class="panel-body">
      <div class="tab-content">
            
        <table class="table table-bordered bhz-table">
            <thead>
              <tr>
                      <th class="text-left">出库单号</th>
                      <th class="text-left">出库仓库</th>
                            <th class="text-left">操作人</th>
                            <th class="text-left">总价</th>
                            <th class="text-left">单据状态</th>
                            <th class="text-left">出库时间</th>
                            <th class="text-left">创建时间</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($stock_out_data) { ?>
                    <?php foreach ($stock_out_data as $stock_out) { ?>
                    <tr style="cursor: pointer;" onmouseover="changecolor($(this))" onmouseleave="$(this).css('color',color)" onclick="location.href='<?php echo $stock_out['url']?>'">
                      <td class="text-left"><?php echo $stock_out['out_id']; ?></td>
                      <td class="text-left"><?php echo $stock_out['whname']; ?></td>
                      <td class="text-left"><?php echo $stock_out['username']; ?></td>
                      <td class="text-left"><?php echo $stock_out['sale_money']; ?></td>
                            <td class="text-left"><?php echo $stock_out['tsname']; ?></td>
                            <td class="text-left"><?php echo $stock_out['out_date']; ?></td>
                            <td class="text-left"><?php echo $stock_out['date_added']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="7">无关联单据</td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
          
        </div>
    </div>
  </div>
    <!--相关单据-->
  </div>
  <script type="text/javascript"><!--
$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['invoice_no']) {
				$('#invoice').html(json['invoice_no']);

				$('#button-invoice').replaceWith('<button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-add').replaceWith('<button id="button-reward-remove" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-is_invoice-add', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/saveIs_invoice&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: {is_invoice_id:"2"},
    dataType: 'json',
    beforeSend: function() {
      $('#button-is_invoice-add').button('loading');
    },
    complete: function() {
      $('#button-is_invoice-add').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                $('#is_invoice').html(json['success']);
                

        $('#button-is_invoice-add').replaceWith('<button id="button-is_invoice-remove" data-toggle="tooltip"  class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-is_invoice-remove', 'click', function() {
  $.ajax({
    url: 'index.php?route=sale/order/saveIs_invoice&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
    type: 'post',
    data: {is_invoice_id:"1"},
    dataType: 'json',
    beforeSend: function() {
      $('#button-is_invoice-remove').button('loading');
    },
    complete: function() {
      $('#button-is_invoice-remove').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                $('#is_invoice').html(json['success']);
        $('#button-is_invoice-remove').replaceWith('<button id="button-is_invoice-add" data-toggle="tooltip"  class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var token = '';

// Login to the API
$.ajax({
	url: '<?php echo $store_url; ?>index.php?route=api/login',
	type: 'post',
	dataType: 'json',
	data: 'key=<?php echo $api_key; ?>',
	crossDomain: true,
	success: function(json) {
		$('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

        if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
	if (typeof verifyStatusChange == 'function'){
		if (verifyStatusChange() == false){
			return false;
		} else{
			addOrderInfo();
		}
	} else{
		addOrderInfo();
	}

	$.ajax({
		url: '<?php echo $store_url; ?>index.php?route=api/order/history&token=' + token + '&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&sent_comment_to_all=' + ($('input[name=\'sent_comment_to_all\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),

		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#button-pay').on('click', function() {
  $.ajax({
    url: '<?php echo $store_url; ?>index.php?route=api/order/confirmPay&token=' + token + '&order_id=<?php echo $order_id; ?>&user_id=<?php echo $user_id; ?>&user_name='+encodeURIComponent('<?php echo $user_name; ?>'),
    type: 'post',
    dataType: 'json',
    data: '',

    beforeSend: function() {
      $('#button-history').button('loading');
    },
    complete: function() {
      $('#button-history').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
        $('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
        $('.confirm-pay').hide();
        $('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        window.location.reload();
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

function changeStatus(){
	var status_id = $('select[name="order_status_id"]').val();

	$('#openbay-info').remove();

	$.ajax({
		url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		dataType: 'html',
		success: function(html) {
			$('#history').after(html);
		}
	});
}

function refundmoney(type){
    if($('#refund_amount_'+type).val() == ''){
        if (1 == type){
            alert('请输入退款金额');
        }else{
            alert('请输入中奖金额');
        }
        return;
    }

    if (1 == type){
        if(!confirm('确认退款 '+$('#refund_amount_'+type).val()+'吗 ？')){
            return;
        };
    }else{
        if(!confirm('确认发奖 '+$('#refund_amount_'+type).val()+'吗 ？')){
            return;
        };
    }

    $.ajax({
        url: 'index.php?route=sale/order/refundmoney&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: 'money=' + $('#refund_amount_'+type).val() + '&type='+type+'&order_id=<?php echo $order_id?>',
        success: function(json) {
            if (json['error']) {
                $('#refund').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            if (json['success']) {
                $('#refund').before('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success']+ '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
        }
    });
}
  

function addOrderInfo(){
	var status_id = $('select[name="order_status_id"]').val();

	$.ajax({
		url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		type: 'post',
		dataType: 'html',
		data: $(".openbay-data").serialize()
	});
}

$(document).ready(function() {
	changeStatus();
});

$('select[name="order_status_id"]').change(function(){
	changeStatus();
});

var color;

function changecolor(div){
  if(typeof(color) == 'undefined'){
    color = div.css('color');
  }

  div.css('color','mediumslateblue');
}
//--></script>
</div>
<?php echo $footer; ?>
