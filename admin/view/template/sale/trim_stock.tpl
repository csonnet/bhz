<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">

	<div class="page-header">
        <div class="container-fluid">
        	<div class="pull-right">
                <a id="button-upload" data-toggle="tooltip" title="导入损益商品" class="btn btn-primary"><i class="fa fa-calculator"></i> 导入损益商品</a>
            	<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
			</div>
			<h1>损益单</h1>
          	<ul class="breadcrumb">
            	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
            	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            	<?php } ?>
			</ul>
        </div>
  	</div>

	<div class="container-fluid">

    	<?php if ($error_warning) { ?>
    	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>

        <div class="panel panel-default" ng-controller="lycCtrl">

			<div class="panel-heading">
            	<h3 class="panel-title"><i class="fa fa-list"></i> 新增损益单</h3>
          	</div>

            <div class="panel-body">

				<form method="post" enctype="multipart/form-data" target="_blank" id="form-order">

                <div class="col-xs-4 form-group required">
 					<label class="control-label">选择仓库</label>
              		<select name="warehouse" id="input-warehouse" class="form-control">
               			<option value="0">请选择</option>
                   		<?php foreach ($warehouses as $value) { ?>
                    	<option value="<?php echo $value['warehouse_id']; ?>"><?php echo $value['name']; ?></option>
                     	<?php } ?>
                	</select>
				</div>

				<div class="col-xs-4 form-group">
          			<label class="control-label">包含订单号</label>
            		<input type="text" ng-model="included_order_ids" class="form-control" placeholder="包含订单号, 请以逗号隔开">
         		</div>

				<table class="table">
     				<thead>
                 		<tr>
                      		<th width="25%">商品</th>
                          	<th>型号</th>
                          	<th width="10%" class="text-center">箱入数</th>
                          	<th>选项</th>
                          	<th width="10%" class="text-center">数量</th>
                          	<th>备注选项</th>
                          	<th>备注</th>
                          	<th class="hidden">库存调整单价</td>
                          	<th></th>
                        </tr>
                    </thead>
                    <tbody>
           				<tr ng-repeat="product in products">
                        	<td class="{{product.productError?'has-error':''}}">
                     			<div class="dropdown">
                          			<div  class="input-group" data-toggle="dropdown" aria-expanded="true" style="width:100%;">
                              			<input type="text" ng-change="getProduct(product.name, '<?php echo $token ?>')" ng-model="product.name" class="form-control" placeholder="商品" aria-describedby="basic-addon2">
                            		</div>
                            		<div class="text-danger">{{product.productError}}</div>
                            		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="width:100%;">
                              			<li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                                			<div class="checkbox">
                                          		<label>{{p.name}}</label>
                                			</div>
                              			</li>
                        			</ul>
                          		</div>
                        	</td>
                            <td>
                                <div>{{product.model}}</div>
                            </td>
                            <td class="text-center">
                                <div>{{product.packing_no}}</div>
                            </td>
                            <td >
                              <!-- <div>{{product.packing_no}}</div>
                                <select ng-change="change_price(product, product.option)" class="form-control" ng-model="product.option" ng-if="product.option_list" ng-options="item.name for item in product.option_list.product_option_value">

                              </select>
                                <div class="text-danger">{{product.optionError}}</div> -->

                               <div style="display: none;">{{product.product_option_value_id}}</div>
                                <div>{{product.option_list}}</div>
                            </td>
                            <td class="{{product.qtyError?'has-error':''}}">
                                <input type="number"  ng-model="product.qty" class="form-control" style="text-align:center;" placeholder="数量" >
                                <div class="text-danger">{{product.qtyError}}</div>
                            </td>
                            <td class="{{product.statusError?'has-error':''}}">
                                  <select class="form-control" id="input-status" ng-model="product.trim_status">
                                   <?php foreach( $trim_status_array as $key => $value ){ ?>
                                   <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                   <?php }?>
                                  </select>
                                  <div class="text-danger">{{product.statusError}}</div>
                            </td>
                            <td>
                              <input type="text" class="form-control" ng-model="product.comment">
                            </td>
                            <td align="right">
                              <button type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                            </td>
                      	</tr>
                    </tbody>
                    <tfoot>
               			<tr>
          					<td colspan="99" align="right"><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                 		</tr>
            		</tfoot>
				</table>
				<button class="btn btn-primary pull-right" ng-click="savetrim('<?php echo $token ?>')">继续</button>
                </form>
				<div class="row"></div>

			</div>

        </div>

	</div>
</div>
<script>
  var $scope;

  	$(function(){

		var appElement = document.querySelector('[ng-controller=bhzAdminCtrl]');

		$scope = angular.element(appElement).scope();

	})

$('#button-upload').on('click', function() {
  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=sale/trim_main_stock/add_import&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            $scope.products = json['data'];
            $scope.$apply();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
</script>
<?php echo $footer; ?>

