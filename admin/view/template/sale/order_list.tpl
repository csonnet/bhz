<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
       <!--  <button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button> -->
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
        <?php
        if (69 == $user_id) { //仅限财务顾俊账号使用
        ?>
        <a href="<?php echo $exportReceiptWithOutPay; ?>" target="_blank" data-toggle="tooltip" title="已签收未收款订单导出" class="btn btn-primary pull-right" style="margin-right:10px;"><i class="fa fa-print"> 已签收未收款订单导出</i></a>
        <?php
        }
        ?>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-6">
            <div class="form-group">
              <div class="input-group year_month">
                <input type="text" name="logcenter_bill_month" value="" placeholder="对账月份" data-date-format="YYYY-MM" id="logcenter_bill_month" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
                <a id="button-export-month-order" data-toggle="tooltip" title="导出对账订单" class="btn btn-primary pull-right" style="margin-left:10px;"><i class="fa fa-calculator"></i> 导出对账订单</a>
                <a id="button-logcenter-bill" data-toggle="tooltip" title="生成物流对账单" class="btn btn-primary pull-right"><i class="fa fa-calculator" ></i> 生成物流对账单</a>

              </div>
            </div>
            <a id="button-hand-order" data-toggle="tooltip" title="生成手工订单" class="btn btn-primary pull-right hidden" href="<?php echo $hand_order; ?>"><i class="fa fa-calculator" ></i> 生成手工订单</a>
          </div>
        </div>

      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-start">搜索订单开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start;?>" placeholder="搜索订单开始时间" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-product-code">订单包含商品（仅限唯一码）</label>
                <input type="text" name="filter_product_code" value="<?php echo $filter_product_code;?>" placeholder="订单包含商品" id="input-product-code" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_order_status == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total"><?php echo $entry_total; ?></label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">搜索订单结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索订单结束时间" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-order-status">是否是货架订单</label>
                <select name="filter_is_shelf_order" id="input-is_shelf_order" class="form-control">

                  <?php foreach ($array_is_shelf_order as $key => $value) { ?>
                  <option value="<?php echo $key; ?>" <?php if ($key == $filter_is_shelf_order) { echo "selected='selected'"; }?> ><?php echo $value; ?></option>
                  <?php } ?>

                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status">是否付款</label>
                <select name="filter_is_pay" id="input-is_pay" class="form-control">

                  <?php foreach ($array_is_pay as $key => $value) { ?>
                  <option value="<?php echo $key; ?>" <?php if ($key == $filter_is_pay) { echo "selected='selected'"; }?> ><?php echo $value; ?></option>
                  <?php } ?>

                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-recommended_code">推荐码</label>
                <input type="text" name="filter_recommended_code" value="<?php echo $filter_recommended_code; ?>" placeholder="推荐码" id="input-recommended_code" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label">审核状态</label>
                <select name="filter_verify_status" id="input-verify_status" class="form-control" />
                <option value="*"></option>
                <option value="0" <?php echo $filter_verify_status == '0'?'selected':''?>>未审核</option>
                <option value="1" <?php echo $filter_verify_status == '1'?'selected':''?>>已审核(缺货)</option>
                <option value="2" <?php echo $filter_verify_status == '2'?'selected':''?>>已审核</option>
                </select>
              </div>
              <?php if(false && $user_group_id == 10){//暂时关闭一键审核功能?>
              <button type="button" id="button-all-verify" class="btn btn-danger pull-left">一键审核</button>
              <?php }?>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
              <button type="button" id="button-exportlists" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出订单</button>
              <button type="button" id="button-collect" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出汇总订单</button>
              <button type="button" id="button-conbin" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出组合商品销售</button>
              <button type="button" id="button-special" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出指定单品订单</button>
            </div>
          </div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($orderClass); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'verify_status') { ?>
                    <a href="<?php echo $sort_verify_status; ?>" class="<?php echo strtolower($orderClass); ?>">审核状态</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_verify_status; ?>">审核状态</a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'o.order_id') { ?>
                    <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($orderClass); ?>"><?php echo $column_order_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'customer') { ?>
                    <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($orderClass); ?>"><?php echo $column_customer; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'o.total') { ?>
                    <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($orderClass); ?>">订单总价</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total; ?>">订单总价</a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'o.ori_total') { ?>
                    <a href="<?php echo $sort_ori_total; ?>" class="<?php echo strtolower($orderClass); ?>">原始总价</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_ori_total; ?>">原始总价</a>
                    <?php } ?></td>
                  <td>签收金额</td>
                  <td>在线退款</td>
                  <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($orderClass); ?>">下单日期</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>">下单日期</a>
                    <?php } ?></td>
                  <td>审核天数</td>
                  <td>履约天数</td>
                  <!-- <td class="text-left"><?php if ($sort == 'o.date_modified') { ?>
                    <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                    <?php } ?></td> -->
                  <!--
                  <td class="text-right">开票状态</td>
                  -->
                  <td class="text-right">支付方式</td>
                  <td class="text-right">是否付款</td>
                  <td class="text-right">是否是货架订单</td>
                  <td class="text-right">超市名称</td>
                  <td class="text-right">邀请码</td>
                  <td class="text-right">客户经理</td>
                  <td class="text-right">发货物流</td>
                  <td class="text-right">可发货(%)</td>
                  <td class="text-right">备注</td>
                  <!--
                  <td class="text-right">
                  <?php if ($sort == 'o.stock_out_rate_by_type') { ?>
                    <a href="<?php echo $sort_SOT; ?>" class="<?php echo strtolower($orderClass); ?>">配货<br />品类</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_SOT; ?>">配货<br />品类</a>
                    <?php } ?>
                  </td>
                  <td class="text-right">
                  <?php if ($sort == 'o.stock_out_rate_by_qty') { ?>
                    <a href="<?php echo $sort_SOQ; ?>" class="<?php echo strtolower($orderClass); ?>">配货<br />数量</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_SOQ; ?>">配货<br />数量</a>
                    <?php } ?>
                  </td>
                  <td class="text-right">
                  <?php if ($sort == 'o.stock_out_rate_by_price') { ?>
                    <a href="<?php echo $sort_SOP; ?>" class="<?php echo strtolower($orderClass); ?>">配货<br />总价</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_SOP; ?>">配货<br />总价</a>
                    <?php } ?>
                  </td>
                  <td class="text-right">
                  <?php if ($sort == 'o.delivery_rate_by_type') { ?>
                    <a href="<?php echo $sort_DT; ?>" class="<?php echo strtolower($orderClass); ?>">配送<br />品类</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_DT; ?>">配送<br />品类</a>
                    <?php } ?>
                  </td>
                  <td class="text-right">
                  <?php if ($sort == 'o.delivery_rate_by_qty') { ?>
                    <a href="<?php echo $sort_DQ; ?>" class="<?php echo strtolower($orderClass); ?>">配送<br />数量</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_DQ; ?>">配送<br />品类</a>
                    <?php } ?>
                  </td>
                  <td class="text-right">
                  <?php if ($sort == 'o.delivery_rate_by_price') { ?>
                    <a href="<?php echo $sort_DP; ?>" class="<?php echo strtolower($orderClass); ?>">配送<br />总价</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_DP; ?>">配送<br />品类</a>
                    <?php } ?>
                  </td>
                  -->
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($orders) { ?>
                <?php foreach ($orders as $order) { ?>
                <tr style="color:<?php echo $order['all_color']; ?>" >
                  <td class="text-center"><?php if (in_array($order['order_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="shipping_code[]" value="<?php echo $order['shipping_code']; ?>" /></td>
                  <td class="text-left"><span style="color: <?php echo $order['status_color']; ?> "><?php echo $order['status']; ?></span></td>
                  <td class="text-left"><?php echo $order['verify_status']?></td>
                  <td class="text-right"><?php echo $order['order_id']; ?></td>
                  <td class="text-left"><?php echo $order['customer']; ?></td>
                  <td class="text-right"><?php echo $order['total']; ?></td>
                  <td class="text-right"><?php echo $order['ori_total']; ?></td>
                  <td class="text-right"><?php echo $order['sign_price']; ?></td>
                  <td class="text-right">
                  <?php echo ($order['unAppliedRefundInfo'])?'<span style="color:red;">未申请：￥'.$order['unAppliedRefundInfo'].'</span><br />':''; ?>
                  <?php echo $order['appliedRefundInfo']; ?>
                  </td>
                  <td class="text-left"><?php echo $order['date_added']; ?></td>
                  <td class="text-left"><?php echo $order['first_cta_day_times'].'-'.$order['last_cta_day_times']?></td>
                  <td class="text-left"><?php echo $order['first_ctc_day_times'].'-'.$order['last_ctc_day_times'] ?></td>
                  <!-- <td class="text-left"><?php echo $order['date_modified']; ?></td> -->
                  <!-- <td class="text-right"><?php echo $order['bill_status']; ?></td> -->
                  <td class="text-right"><?php echo $order['payment_method']; ?></td>
                  <td class="text-right"><?php echo $order['is_pay']; ?></td>
                  <td class="text-right"><?php echo $order['is_shelf_order']; ?></td>
                  <td class="text-right"><?php echo $order['shipping_company']; ?></td>
                  <td class="text-right"><?php echo $order['recommended_code']; ?></td>
                  <td class="text-right"><?php echo $order['recommended_name']; ?></td>
                  <td class="text-right"><?php echo $order['logcenter_name']; ?></td>
                  <td class="text-right"><?php echo $order['total_percant']?></td>
                  <td class="text-right"><?php echo $order['remark']?></td>
                  <!--
                  <td class="text-right"><?php echo $order['stockOutRateByType']; ?>%</td>
                  <td class="text-right"><?php echo $order['stockOutRateByQty']; ?>%</td>
                  <td class="text-right"><?php echo $order['stockOutRateByPrice']; ?>%</td>
                  <td class="text-right"><?php echo $order['deliveryRateByType']; ?>%</td>
                  <td class="text-right"><?php echo $order['deliveryRateByQty']; ?>%</td>
                  <td class="text-right"><?php echo $order['deliveryRateByPrice']; ?>%</td>
                  -->
                  <td class="text-right">
                    <?php if(($order['order_status_id'] == 20 OR $order['order_status_id'] == 17) && $user_group_id == 10 && $order['hang_status'] != 1){?>
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-verify<?php echo $order['order_id']; ?>"   data-toggle="tooltip" class="btn btn-danger">审核</button>
                    <?php }?>
                    <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <?php if($user_id==260||$user_id==3340||$user_id==298||$user_id==69||$user_id==69||$user_id==1) { ?>
                    <!-- <a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-delete<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    <?php } ?>
                    <?php if($order['order_status_id'] == 20 && $user_group_id == 10 && $order['hang_status'] != 1){?>
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-hang<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-danger">挂起</button>
                    <?php }?>
                    <?php if($order['order_status_id'] == 20 && $user_group_id == 10 && $order['hang_status'] == 1){?>
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-cancel-hang<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-info">取消挂起</button>
                    <?php }?>
                    <?php if(($order['order_status_id'] == 2 || $order['order_status_id'] == 17 || $order['order_status_id'] == 20) && $user_group_id == 10){?>
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-end-deliver<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" class="btn btn-danger">结束配货</button>
                    </button>
                    <?php }?>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/order&token=<?php echo $token; ?>';

	var filter_order_id = $('input[name=\'filter_order_id\']').val();

	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}

  var filter_is_pay = $('select[name=\'filter_is_pay\']').val();

  if (filter_is_pay) {
    url += '&filter_is_pay=' + encodeURIComponent(filter_is_pay);
  }

  var filter_is_shelf_order = $('select[name=\'filter_is_shelf_order\']').val();

  if (filter_is_shelf_order) {
    url += '&filter_is_shelf_order=' + encodeURIComponent(filter_is_shelf_order);
  }

	var filter_customer = $('input[name=\'filter_customer\']').val();

	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}

	var filter_order_status = $('select[name=\'filter_order_status\']').val();

	if (filter_order_status != '*') {
		url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
	}

  var filter_verify_status = $('select[name=\'filter_verify_status\']').val();

  if (filter_verify_status != '*') {
    url += '&filter_verify_status=' + encodeURIComponent(filter_verify_status);
  }

	var filter_total = $('input[name=\'filter_total\']').val();

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

    var filter_product_code = $('input[name="filter_product_code"]').val();

    if (filter_product_code) {
        url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }

    location = url;
});

$('#button-exportlists').on('click', function() {
  url = 'index.php?route=sale/order/exportLists&token=<?php echo $token; ?>';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();

  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_is_pay = $('select[name=\'filter_is_pay\']').val();

  if (filter_is_pay) {
    url += '&filter_is_pay=' + encodeURIComponent(filter_is_pay);
  }

  var filter_is_shelf_order = $('select[name=\'filter_is_shelf_order\']').val();

  if (filter_is_shelf_order) {
    url += '&filter_is_shelf_order=' + encodeURIComponent(filter_is_shelf_order);
  }


  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

  var filter_order_status = $('select[name=\'filter_order_status\']').val();

  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }

  var filter_verify_status = $('select[name=\'filter_verify_status\']').val();

  if (filter_verify_status != '*') {
    url += '&filter_verify_status=' + encodeURIComponent(filter_verify_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

    var filter_product_code = $('input[name="filter_product_code"]').val();

    if (filter_product_code) {
        url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }

  location = url;
});

$('#button-special').on('click', function() {
  url = 'index.php?route=sale/order/exportSpecial&token=<?php echo $token; ?>';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();

  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_is_pay = $('select[name=\'filter_is_pay\']').val();

  if (filter_is_pay) {
    url += '&filter_is_pay=' + encodeURIComponent(filter_is_pay);
  }

  var filter_is_shelf_order = $('select[name=\'filter_is_shelf_order\']').val();

  if (filter_is_shelf_order) {
    url += '&filter_is_shelf_order=' + encodeURIComponent(filter_is_shelf_order);
  }


  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

  var filter_order_status = $('select[name=\'filter_order_status\']').val();

  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }

  var filter_verify_status = $('select[name=\'filter_verify_status\']').val();

  if (filter_verify_status != '*') {
    url += '&filter_verify_status=' + encodeURIComponent(filter_verify_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

    var filter_product_code = $('input[name="filter_product_code"]').val();

    if (filter_product_code) {
        url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }else{
        alert('请输入唯一码后导出！');
        return false;
    }

  location = url;
});

$('#button-conbin').on('click', function() {
  url = 'index.php?route=sale/order/exportConbin&token=<?php echo $token; ?>';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();

  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_is_pay = $('select[name=\'filter_is_pay\']').val();

  if (filter_is_pay) {
    url += '&filter_is_pay=' + encodeURIComponent(filter_is_pay);
  }

  var filter_is_shelf_order = $('select[name=\'filter_is_shelf_order\']').val();

  if (filter_is_shelf_order) {
    url += '&filter_is_shelf_order=' + encodeURIComponent(filter_is_shelf_order);
  }


  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

  var filter_order_status = $('select[name=\'filter_order_status\']').val();

  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }

  var filter_verify_status = $('select[name=\'filter_verify_status\']').val();

  if (filter_verify_status != '*') {
    url += '&filter_verify_status=' + encodeURIComponent(filter_verify_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

  location = url;
});
$('#button-collect').on('click', function() {
  url = 'index.php?route=sale/order/exportCollect&token=<?php echo $token; ?>';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();

  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_is_pay = $('select[name=\'filter_is_pay\']').val();

  if (filter_is_pay) {
    url += '&filter_is_pay=' + encodeURIComponent(filter_is_pay);
  }

  var filter_is_shelf_order = $('select[name=\'filter_is_shelf_order\']').val();

  if (filter_is_shelf_order) {
    url += '&filter_is_shelf_order=' + encodeURIComponent(filter_is_shelf_order);
  }


  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

  var filter_order_status = $('select[name=\'filter_order_status\']').val();

  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }

  var filter_verify_status = $('select[name=\'filter_verify_status\']').val();

  if (filter_verify_status != '*') {
    url += '&filter_verify_status=' + encodeURIComponent(filter_verify_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

    var filter_recommended_code = $('input[name=\'filter_recommended_code\']').val();

  if (filter_recommended_code) {
    url += '&filter_recommended_code=' + encodeURIComponent(filter_recommended_code);
  }

  location = url;
});

$('button[id^=\'button-all-verify\']').on('click', function(e) {
  if(confirm('确定吗？')){
    var node = this;

    var order_id_arr = new Array();

    $('input[name="selected[]"]:checked').each(function(index){
      order_id_arr[index] = $(this).val();
    })

    var order_id_str = order_id_arr.join(',');

    if(order_id_str.length != 0 ){
      $.ajax({
        url: '<?php echo $store; ?>index.php?route=api/order/verifyall&token=' + token + '&order_id_str=' + order_id_str + '&user_id=<?php echo $user_id?>',
        dataType: 'json',
        crossDomain: true,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          if (json['error']) {
            alert(json['error']);
            window.location.reload();
          }

          if (json['success']) {
            alert(json['success']);
            window.location.reload();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      })
    }else{
      alert('请选中至少一个订单');
    }
  }
})

//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['fullname'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}
});

$('input[name=\'filter_recommended_code\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_recommended_code=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          if(item['recommended_code']){
            return {
            label: item['recommended_code'],
            value: item['recommended_code']
            }
          }

        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_recommended_code\']').val(item['label']);
  }
});
//--></script>
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);

	var selected = $('input[name^=\'selected\']:checked');

	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}

	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);

			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

// Login to the API
var token = '';

$.ajax({
	url: '<?php echo $store; ?>index.php?route=api/login',
	type: 'post',
	data: 'key=<?php echo $api_key; ?>',
	dataType: 'json',
	crossDomain: true,
	success: function(json) {
        // $('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

		if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('button[id^=\'button-delete\']').on('click', function(e) {
	if (confirm('<?php echo $text_confirm; ?>')) {
		var node = this;

		$.ajax({
			url: '<?php echo $store; ?>index.php?route=api/order/delete&token=' + token + '&order_id=' + $(node).val(),
			dataType: 'json',
			crossDomain: true,
			beforeSend: function() {
				$(node).button('loading');
			},
			complete: function() {
				$(node).button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['error']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}
        window.location.reload();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
});

$('button[id^=\'button-verify\']').on('click', function(e) {
    if(confirm('确定吗？')){
        var node = this;
        $.ajax({
            url: '<?php echo $store; ?>index.php?route=api/order/verify_v2&token=' + token + '&order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
            dataType: 'json',
            crossDomain: true,
            beforeSend: function() {
                $(node).button('loading');
            },
            complete: function() {
                $(node).button('reset');
            },
            success: function(json) {
               if (2 == json['status']) {
                    alert(json['message']);
                    window.location.reload();
                }else if (1 == json['status']) {
                    alert(json['message']);
                    window.location.reload();
                }else if (0 == json['status']) {
                    if(confirm(json['message'])){
                        $.ajax({
                            url: '<?php echo $store; ?>index.php?route=api/order/verify_v2&token=' + token + '&order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>&how=anyway',
                            dataType: 'json',
                            crossDomain: true,
                            success: function(json) {
                                alert(json['message']);
                                window.location.reload();
                            }
                        })
                    }else{
                        window.location.reload();
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        })
    }
});

$('button[id^=\'button-hang\']').on('click', function(e) {
  if(confirm('确定吗？')){
    var node = this;

    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/order/hang&token=' + token + '&order_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        if (json['error']) {
          alert(json['error']);
        }

        if (json['success']) {
          window.location.reload();
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    })
  }
})

$('button[id^=\'button-cancel-hang\']').on('click',function(e){
  if(confirm('确定吗？')){
    var node = this;
    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/order/cancelhang&token=' + token + '&order_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        if (json['error']) {
          alert(json['error']);
        }

        if (json['success']) {
          window.location.reload();
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    })
  }
})

$('button[id^=\'button-end-deliver\']').on('click',function(e){
  var node = this;

  $.ajax({
    url: '<?php echo $store; ?>index.php?route=api/order/endeliver&token=' + token + '&order_id=' + $(node).val() + '&confirm=0',
    dataType: 'json',
    crossDomain: true,
    beforeSend: function() {
      $(node).button('loading');
    },
    success: function(json) {
      if (json['error']) {
        alert(json['error']);
      }

      if(json['success']){
        if(confirm(json['success'])){
          $.ajax({
            url: '<?php echo $store; ?>index.php?route=api/order/endeliver&token=' + token + '&order_id=' + $(node).val() + '&confirm=1&user_id=<?php echo $user_id?>',
            dataType: 'json',
            crossDomain: true,
            success: function(json) {
              if (json['success']) {
                alert(json['success']);
                window.location.reload();
              }
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          })
        }else{
          $(node).button('reset');
        }
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  })
})
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months",
  minViewMode: "months",
  pickTime: false
});

$('#button-logcenter-bill').on('click', function(e) {
  var logcenter_bill_month = $('#logcenter_bill_month').val();
  if(!logcenter_bill_month || isNaN(Date.parse(logcenter_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/order/genLogcenterBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+logcenter_bill_month;
});

$('#button-export-month-order').on('click', function(e) {
  var logcenter_bill_month = $('#logcenter_bill_month').val();
  if(!logcenter_bill_month || isNaN(Date.parse(logcenter_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('sale/order/monthlyExport') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+logcenter_bill_month;
});
//--></script></div>
<?php echo $footer; ?>
