<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-user').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name">关键字搜索：</label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="input-name" class="form-control" placeholder="用户名 / 使用人 / 联系电话" />
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-user-group-id">管理员群组</label>
                <select name="filter_user_group_id" id="input-user-group-id" class="form-control">
                    <option value='*'>全部</option>
                <?php
                foreach ($userGroup as $userGroupId=>$userGroupName) {
                    echo '<option value="'.$userGroupId.'"';
                    if ($filter_user_group_id === $userGroupId) {
                        echo ' selected';
                    }
                    echo '>'.$userGroupName.'</option>';
                }
                ?>
                </select>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                    <option value='*'>全部</option>
                    <option value='1'<?php if (1 === $filter_status) { echo ' selected';} ?>>启用</option>
                    <option value='0'<?php if (0 === $filter_status) { echo ' selected';} ?>>停用</option>
                </select>
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> 筛选</button>
              </div>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-user">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'username') { ?>
                    <a href="<?php echo $sort_username; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_username; ?>（用于登录）</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_username; ?>"><?php echo $column_username; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'fullname') { ?>
                    <a href="<?php echo $sort_fullname; ?>" class="<?php echo strtolower($order); ?>">使用人</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_fullname; ?>">使用人</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'contact_tel') { ?>
                    <a href="<?php echo $sort_contact_tel; ?>" class="<?php echo strtolower($order); ?>">联系电话</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_contact_tel; ?>">联系电话</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'user_group_id') { ?>
                    <a href="<?php echo $sort_user_group_id; ?>" class="<?php echo strtolower($order); ?>">管理员群组</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_user_group_id; ?>">管理员群组</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?></td>
                  <td class="text-right" style='width:80px;'><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($users) { ?>
                <?php foreach ($users as $user) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($user['user_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $user['username']; ?></td>
                  <td class="text-left"><?php echo $user['fullname']; ?></td>
                  <td class="text-left"><?php echo $user['tel']; ?></td>
                  <td class="text-left"><?php echo $user['userGroup']; ?></td>
                  <td class="text-left" <?php echo $statusStyle; ?>><?php echo $user['status']; ?></td>
                  <td class="text-left"><?php echo $user['date_added']; ?></td>
                  <td class="text-right"><a href="<?php echo $user['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

<script>
$('#button-filter').on('click', function() {
    var url = 'index.php?route=user/user&token=<?php echo $token; ?>&filter_name='+$('#input-name').val()+'&filter_user_group_id='+$('#input-user-group-id').val()+'&filter_status='+$('#input-status').val();
    location = url;
});
</script>
</div>
<?php echo $footer; ?>