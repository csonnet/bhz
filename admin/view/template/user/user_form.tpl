<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-username"><?php echo $entry_username; ?></label>
            <div class="col-sm-10">
              <input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
              <?php if ($error_username) { ?>
              <div class="text-danger"><?php echo $error_username; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-user-group"><?php echo $entry_user_group; ?></label>
            <div class="col-sm-10">
              <select name="user_group_id" id="input-user-group" class="form-control">
                <?php foreach ($user_groups as $user_group) { ?>
                <?php if ($user_group['user_group_id'] == $user_group_id) { ?>
                <option value="<?php echo $user_group['user_group_id']; ?>" selected="selected"><?php echo $user_group['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $user_group['user_group_id']; ?>"><?php echo $user_group['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group required id-div-recommended-code">
            <label class="col-sm-2 control-label" for="input-recommended-code">推荐码</label>
            <div class="col-sm-10">
              <input type="text" name="recommended_code" value="<?php echo $recommended_code; ?>" placeholder="推荐码" id="input-recommended-code" class="form-control" />   
            </div>
          </div>
          <div class="form-group id-div-sales-area" style='background:#EFEFEF;'>
            <label class="col-sm-2 control-label" for="input-recommended-code">所属营销区域</label>
            <div class="col-sm-10">
              <select name="sales_area_id" id="input-sales-area-id" class="form-control">
                  <option value="0"></option>
                <?php
                foreach ($salesAreaList as $salesArea) {
                    $saslesAreaSelected = '';
                    if ($salesArea['sales_area_id'] == $sales_area_id) {
                        $saslesAreaSelected = 'selected="selected"';
                    }
                    echo '<option value="'.$salesArea['sales_area_id'].'" '.$saslesAreaSelected.'>'.$salesArea['name'].'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-fullname"><?php echo $entry_fullname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="fullname" value="<?php echo $fullname; ?>" placeholder="<?php echo $entry_fullname; ?>" id="input-fullname" class="form-control" />
              <?php if ($error_fullname) { ?>
              <div class="text-danger"><?php echo $error_fullname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-tel"><?php echo $entry_tel; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tel" value="<?php echo $tel; ?>" placeholder="<?php echo $entry_tel; ?>" id="input-tel" class="form-control" />
            </div>
          </div>

          <div class="form-group" style='background:#EFEFEF;'>
            <label class="col-sm-2 control-label" for="input-tel">所属物流营销中心</label>
            <div class="col-sm-10">
              <select name="logcenter_permission" id="input-logcenter-permission" class="form-control">
                  <option value="0"></option>
                <?php
                foreach ($logcenterList as $logcenter) {
                    $logcenterSelected = '';
                    if ($logcenter['logcenter_id'] == $logcenter_permission) {
                        $logcenterSelected = 'selected="selected"';
                    }
                    echo '<option value="'.$logcenter['logcenter_id'].'" '.$logcenterSelected.'>'.$logcenter['name'].'</option>';
                }
                ?>
                </select>
            </div>
          </div>
          <div class="form-group" style='background:#EFEFEF;'>
            <label class="col-sm-2 control-label" for="input-tel">所属仓库</label>
            <div class="col-sm-10">
                <select name="warehouse_permission" id="input-warehouse-permission" class="form-control">
                  <option value="0"></option>
                <?php
                foreach ($warehouseList as $warehouse) {
                    $warehouseSelected = '';
                    if ($warehouse['warehouse_id'] == $warehouse_permission) {
                        $warehouseSelected = 'selected="selected"';
                    }
                    echo '<option value="'.$warehouse['warehouse_id'].'" '.$warehouseSelected.'>'.$warehouse['name'].'</option>';
                }
                ?>
                </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
            <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" autocomplete="off" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php  } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php  } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="0"><?php echo $text_disabled; ?></option>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <?php } else { ?>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <option value="1"><?php echo $text_enabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('select[name=\'user_group_id\']').change(function(){
    var user_group_id = $('select[name=\'user_group_id\']').val();
    var sales_group_id = <?php echo $sales_group_id ; ?>;
    if(user_group_id == sales_group_id){
      $('.id-div-recommended-code').show();
      $('.id-div-sales-area').show();
    }else{
      $('.id-div-recommended-code').hide(); 
      $('.id-div-sales-area').hide();
    }
  });
$('select[name=\'user_group_id\']').trigger("change");
</script>
<?php echo $footer; ?> 