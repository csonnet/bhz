<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	
     <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
        	
            <!--右上操作按钮-->
			<div class="pull-right">
            	<button type="submit" form="form-warehouse" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-check-circle"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
            <!--右上操作按钮-->
            
 			<h1><?php echo $heading_title; ?></h1>
            
 			<ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>       
        </div>
	</div>
    <!--顶部-->
    
    <!--内容-->
	<div class="container-fluid">
    	
        <!--警示消息区-->
    	<?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    	<?php } ?>
        <!--警示消息区-->
        
        <!--表单-->
		<div class="panel panel-default">
        	
            <!--标题-->
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
            <!--标题-->
            
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-warehouse" class="form-horizontal">
         		<div class="tab-content">
                	<div class="tab-pane active" id="tab-general">
                        
                        <!--仓库名称-->
                        <div class="form-group required">
                 			<label class="col-sm-2 control-label" for="input-warehouse-name"><?php echo $entry_warehouse_name; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_warehouse_name; ?>" id="input-warehouse-code" class="form-control" />
                  				<?php if ($error_warehouse_name) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_name; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--仓库名称-->
                        
                        <!--类型-->
                        <div class="form-group required">
                 			<label class="col-sm-2 control-label" for="input-type"><?php echo $entry_type; ?></label>
                            <div class="col-sm-5">
                            	<select name="type" id="input-type" class="form-control" onchange="hasParent(this.value);">
                					<?php if ($type==1) { ?>
                					<option value="1" selected="selected"><?php echo $text_main_type; ?></option>
                                    <option value="2"><?php echo $text_normal_type; ?></option>
                                    <option value="3"><?php echo $text_defect_type; ?></option>
                					<?php } elseif ($type==2) { ?>
                                    <option value="1"><?php echo $text_main_type; ?></option>
                                    <option value="2" selected="selected"><?php echo $text_normal_type; ?></option>
                                    <option value="3"><?php echo $text_defect_type; ?></option>
                                    <?php } elseif ($type==3) { ?>
                                    <option value="1"><?php echo $text_main_type; ?></option>
                                    <option value="2"><?php echo $text_normal_type; ?></option>
                                    <option value="3" selected="selected"><?php echo $text_defect_type; ?></option>	
                					<?php } ?>
              					</select>
            				</div>
                 		</div>
                         <!--类型-->
                         
                         <!--残次品仓所属仓库-->
                        <div id="parentwarehouse" class="form-group required">
                            <label class="col-sm-2 control-label" for="input-parent-warehouse"><?php echo $entry_parent_warehouse; ?></label>
							<div class="col-sm-5">
								<select name="parent_warehouse" id="input-parent-warehouse" class="form-control">
                                	<option value="0"><?php echo $text_no_select; ?></option>
                					<?php foreach ($parent_warehouses as $parent) { ?>
                					<?php if ($parent['warehouse_id'] == $parent_warehouse) { ?>
                					<option value="<?php echo $parent['warehouse_id']; ?>" selected="selected"><?php echo $parent['name']; ?></option>
                                    <?php } else { ?>
                					<option value="<?php echo $parent['warehouse_id']; ?>"><?php echo $parent['name']; ?></option>
                					<?php } ?>
                					<?php } ?>
                              	</select>
								<?php if ($error_warehouse_parent_warehouse) { ?>
                              	<div class="text-danger"><?php echo $error_warehouse_parent_warehouse; ?></div>
                              	<?php } ?>
							</div>
						</div>
                        <!--残次品仓所属区域仓-->
                        
                        <!--省市-->
                        <div class="form-group required">
							<label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            				<div class="col-sm-5">
         						<select name="country_id" id="input-country" class="form-control" onchange="country(this.value,'<?php echo $zone_id; ?>');">
                					<option value=""><?php echo $text_select; ?></option>
                					<?php foreach ($countries as $country) { ?>
                					<?php if ($country['country_id'] == $country_id) { ?>
                					<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                					<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                					<?php } ?>
                					<?php } ?>
              					</select>
              					<?php if ($error_warehouse_country) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_country; ?></div>
              					<?php } ?>
            				</div>
						</div>
                        <!--省市-->
                        
                        <!--区县-->
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
							<div class="col-sm-5">
								<select name="zone_id" id="input-zone" class="form-control" onchange="zone(this.value,'<?php echo $city_id; ?>');">
                              	</select>
								<?php if ($error_warehouse_zone) { ?>
                              	<div class="text-danger"><?php echo $error_warehouse_zone; ?></div>
                              	<?php } ?>
							</div>
						</div>
                        <!--区县-->
                        
                        <!--市区-->
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
							<div class="col-sm-5">
								<select name="city_id" id="input-city" class="form-control">
                              	</select>
								<?php if ($error_warehouse_city) { ?>
                              	<div class="text-danger"><?php echo $error_warehouse_city; ?></div>
                              	<?php } ?>
							</div>
						</div>
                        <!--市区-->
                        
                        <!--地址-->
                        <div class="form-group required">
                 			<label class="col-sm-2 control-label" for="input-address"><?php echo $entry_address; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="address" value="<?php echo $address; ?>" placeholder="<?php echo $entry_address; ?>" id="input-address" class="form-control" />
                  				<?php if ($error_warehouse_address) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_address; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--地址-->
                        
                        <!--仓库管理员-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label" for="input-manager"><?php echo $entry_manager; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="manager" value="<?php echo $manager; ?>" placeholder="<?php echo $entry_manager; ?>" id="input-manager" class="form-control" />
                  				<?php if ($error_warehouse_manager) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_manager; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--仓库管理员-->
                        
                        <!--联系电话-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label" for="input-contact-tel"><?php echo $entry_contact_tel; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="contact_tel" value="<?php echo $contact_tel; ?>" placeholder="<?php echo $entry_contact_tel; ?>" id="input-contact-tel" class="form-control" />
                  				<?php if ($error_warehouse_contact_tel) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_contact_tel; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--联系电话-->
                         
                         <!--优先级-->
                        <div class="form-group required">
                 			<label class="col-sm-2 control-label" for="input-order"><?php echo $entry_order; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="order" value="<?php echo $order; ?>" placeholder="<?php echo $entry_order; ?>" id="input-order" class="form-control" />
                  				<?php if ($error_warehouse_order) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_order; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--优先级-->
                        
                        <!--状态-->
                        <div class="form-group required">
                 			<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                            <div class="col-sm-5">
                            	<select name="status" id="input-status" class="form-control">
                					<?php if ($status) { ?>
                					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                					<?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>	
                					<?php } ?>
              					</select>
            				</div>
                 		</div>
                         <!--状态-->
                        
					</div>
 				</div>                  
            	</form>
			</div>
            
		</div>
        <!--表单-->
        
	</div>
    <!--内容-->
    
</div>

<script>
	
	<!--初始联动城市区-->
	if('<?php echo $country_id; ?>'){
		country('<?php echo $country_id; ?>','<?php echo $zone_id; ?>');
		zone('<?php echo $zone_id; ?>','<?php echo $city_id; ?>');
	}
	<!--初始联动城市区-->
	
	<!--初始判断仓库类型-->
	if($("#input-type").val() == 3){
		$("#parentwarehouse").show();	
	}else{
		$("#parentwarehouse").hide();	
	}
	<!--初始判断仓库类型-->

	//联动省市区县
	function country(country_id,zone_id) {
		$.ajax({
			url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + country_id,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'country_id\']').after('<i class="fa fa-circle-o-notch fa-spin"></i>'); //loading
			},
			complete: function() {
				$('.fa-spin').remove(); //移除loading
			},
			success: function(json) {
	
				html = '<option value=""><?php echo $text_select; ?></option>';
	
				if (json['zone'] && json['zone'] != '') {
					for (i = 0; i < json['zone'].length; i++) {
						html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	
						if (json['zone'][i]['zone_id'] == zone_id) {
							html += ' selected="selected"';
						}
	
						html += '>' + json['zone'][i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0"><?php echo $text_none; ?></option>';
				}
	
				$('select[name=\'zone_id\']').html(html);
				
				//触发区县change事件
				$('select[name=\'zone_id\']').trigger('change');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
	
	//联动区县市区
	function zone(zone_id,city_id) {
		$.ajax({
			url: 'index.php?route=localisation/zone/zone&token=<?php echo $token; ?>&zone_id=' + zone_id,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>'); //loading
			},
			complete: function() {
				$('.fa-spin').remove(); //移除loading
			},
			success: function(json) {
				html = '<option value=""><?php echo $text_select; ?></option>';
	
				if (json['city'] && json['city'] != ''){
					for (i = 0; i < json['city'].length; i++) {
						html += '<option value="' + json['city'][i]['city_id'] + '"';
	
						if (json['city'][i]['city_id'] == city_id) {
							html += ' selected="selected"';
						}
	
						html += '>' + json['city'][i]['name'] + '</option>';
					}
				}
				else{
					html += '<option value="0"><?php echo $text_none; ?></option>';
				}
	
				$('select[name=\'city_id\']').html(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
	
	function hasParent(type_id){
		if(type_id == 3){
			$("#parentwarehouse").show();
		}
		else{
			$("#parentwarehouse").hide();
			$("#input-parent-warehouse").val("0");
		}
	}

</script>

<?php echo $footer; ?>