<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	
    <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
            
      		<h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
    		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
        		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        		<?php } ?>
      		</ul>
            <!--面包屑导航-->
            
		</div>
	</div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
    	<!--警示消息区-->
    	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<!--警示消息区-->
    	
        <!--列表-->
  		<div class="panel panel-default">
        	
            <!--标题-->
      		<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      		</div>
			<!--标题-->
            
            <!--列表内容-->
    		<div class="panel-body">
            	
                <!--筛选区-->
        		<div class="well">
					<div class="row">
                    
                    	<!--第一列-->
						<div class="col-sm-4">
                        	
                            
                            
                            <!--筛选仓库-->
                            <div class="form-group">
                    			<label class="control-label" for="input-warehouse"><?php echo $entry_warehouse; ?></label>
                    			<select name="filter_warehouse" id="input-warehouse" class="form-control">
                                	<option value="0">全部</option>
                                	<?php foreach ($warehouses as $value) { ?>
                      				<option value="<?php echo $value['warehouse_id']; ?>" <?php if ($value["warehouse_id"] == $filter_warehouse) { echo "selected='selected'"; }?> ><?php echo $value['name']; ?></option>
                      				<?php } ?>
                    			</select>
                  			</div>
                            <!--筛选仓库-->
                            
                            <!--筛选条形码-->
							<div class="form-group">
                    			<label class="control-label" for="input-sku"><?php echo $entry_sku; ?></label>
                   				<input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-sku" class="form-control" />
      						</div>
                            <!--筛选条形码-->
                                  
                		</div>
                        <!--第一列-->
                        
                        <!--第二列-->
       					<div class="col-sm-4">
                        
                        	 <!--筛选商品编码或名称-->
							<div class="form-group">
                    			<label class="control-label" for="input-product-name"><?php echo $entry_product_name; ?></label>
                   				<input type="text" name="filter_product_name" value="<?php echo $filter_product_name; ?>" placeholder="<?php echo $entry_product_name; ?>" id="input-product-name" class="form-control" />
                                <input type="hidden" name="filter_product_code" id="input-product-code" value="" />
      						</div>
                            <!--筛选商品编码或名称-->

                            <div class="form-group">
                                <label class="control-label" for="input-vendor-name"><?php echo $entry_vendor_name; ?></label>
                                <input type="text" name="filter_vendor_name" value="<?php echo $filter_vendor_name; ?>" placeholder="<?php echo $entry_vendor_name; ?>" id="input-vendor-name" class="form-control" />
                                <input type="hidden" name="filter_vendor_id" id="input-vendor-id" value="" />
                            </div>
                		</div>
                        <!--第二列-->
                        
                        <!--第三列-->
                		<div class="col-sm-4">
                        	
                            <!--筛选库位-->
							<div class="form-group">
                    			<label class="control-label" for="input-warehouse-position"><?php echo $entry_warehouse_position; ?></label>
                   				<input type="text" name="filter_warehouse_position" value="<?php echo $filter_warehouse_position; ?>" placeholder="<?php echo $entry_warehouse_position; ?>" id="input-warehouse-position" class="form-control" />
      						</div>
                            <!--筛选库位-->
                                
                            <!--导出筛选按钮-->
							<div class="form-group btn-group">
                            	<button type="button" id="button-export" class="btn btn-primary pull-right"><i class="fa fa-print"></i> 导出库存</button>
								<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
							</div>
                            <!--导出筛选按钮-->
						</div>
                        <!--第三列-->
                        
					</div>
				</div>
        		<!--筛选区-->
            
      			<div class="table-responsive">
                    <table class="table table-bordered table-hover bhz-table">
                        <thead>
                            <tr>
                                <th><?php echo $column_warehouse_name; ?></th>
                                <th class="text-center"><?php echo $column_position1; ?></th>
				<th class="text-center"><?php echo $column_position2; ?></th>
                                <th><?php echo $column_product_name; ?></th>
                                <th><?php echo $column_product_code; ?></th>
                                <th><?php echo $column_product_sku; ?></th>
                                <th class="text-center"><?php echo $column_product_option; ?></th>
                                <th><?php echo $column_vendor_name; ?></th>
                                <th class="text-center"><?php echo $column_account_quantity; ?></th>
                                <th class="text-center"><?php echo $column_available_quantity; ?></th>
                                <th class="text-center"><?php echo $column_safe_quantity; ?></th>
                             	<th class="text-center"><?php echo $column_action; ?></th>     
                            </tr>
                      </thead>
                        <tbody>
                			<?php if ($inventories) { ?>
                			<?php foreach ($inventories as $inventory) { ?>
                			<tr>
                  				<td class="text-left"><?php echo $inventory['warehouse_name']; ?></td>
                                <td class="text-center"><?php echo $inventory['position1']; ?></td>
                                <td class="text-center"><?php echo $inventory['position2']; ?></td>
                                <td><?php echo $inventory['product_name']; ?></td>
                                <td><?php echo $inventory['product_code']; ?></td>
                                <td><?php echo $inventory['sku']; ?></td>
                                <td class="text-center"><?php echo $inventory['product_option']; ?></td>
                                <td><?php echo $inventory['vendor_name']; ?></td>
                                <td class="text-center"><?php echo $inventory['account_quantity']; ?></td>
                                <td class="text-center"><?php echo $inventory['available_quantity']; ?></td>
                                <td class="text-center"><?php echo $inventory['safe_quantity']; ?></td>
                  				<td class="text-center">
                                	<a target="_blank" href="<?php echo $inventory['view']; ?>" data-toggle="tooltip" title="<?php echo $text_view_product; ?>" class="btn btn-primary"><i class="fa fa-list"></i></a>
                                    <?php if($can_edit) { ?>
                                    <a target="_blank" href="<?php echo $inventory['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    <?php } ?>
								</td>
							</tr>
                			<?php } ?>
                			<?php } else { ?>
                            <tr>
								<td class="text-center" colspan="999"><?php echo $text_no_results; ?></td>
                            </tr>
                			<?php } ?>
						</tbody>
                    </table>
				</div>
            
				<div class="row">
      				<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      				<div class="col-sm-6 text-right"><?php echo $results; ?></div>
    			</div>
                
  			</div>
            <!--列表内容-->
            
		</div>
        <!--列表-->
        
	</div>
	<!--内容-->
  
</div>

<script type="text/javascript">
	
	<!--点击批量启用-->
	$('#btn-enables').on('click', function(e){		
		if(confirm('<?php echo $text_confirm_enable; ?>')){
			var actionurl = $(this).attr('toaction');
			$('#form-warehouse').attr('action',actionurl);
			$('#form-warehouse').submit();
		}	
	});
	<!--点击批量启用-->
	
	<!--点击批量停用-->
	$('#btn-disables').on('click', function(e){		
		if(confirm('<?php echo $text_confirm_disable; ?>')){
			var actionurl = $(this).attr('toaction');
			$('#form-warehouse').attr('action',actionurl);
			$('#form-warehouse').submit();
		}	
	});
	<!--点击批量停用-->
	
	<!--筛选-->
	$('#button-filter').on('click', function() {
		
		var url = 'index.php?route=stock/stocksearch&token=<?php echo $token; ?>';
		
		var filter_warehouse = $('select[name=\'filter_warehouse\']').val();
		if (filter_warehouse != '0') {
			url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
		}
		
		var filter_product_code = $('input[name=\'filter_product_code\']').val();
		if(filter_product_code){
			url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
		}
		
		var filter_warehouse_position = $('input[name=\'filter_warehouse_position\']').val();
		if (filter_warehouse_position) {
			url += '&filter_warehouse_position=' + encodeURIComponent(filter_warehouse_position);
		}
		
		var filter_sku = $('input[name=\'filter_sku\']').val();
		if(filter_sku){
			url += '&filter_sku=' + encodeURIComponent(filter_sku);
		}

        var filter_vendor = $('input[name="filter_vendor_id"]').val();
        if(filter_vendor){
            url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
        }
		
		location = url;
		
	});
	<!--筛选-->
	
	<!--商品编码或名称autocomplete-->
	$('input[name=\'filter_product_name\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=stock/stocksearch/autocomplete&token=<?php echo $token; ?>&filter_product=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['product_code']+"    "+item['product_name'],
							value: item['product_code'],
							name: item['product_name']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'filter_product_name\']').val(item['name']);
			$('input[name=\'filter_product_code\']').val(item['value']);
		}
	});
	<!--商品编码或名称autocomplete-->

    <!-- 供应商名称autocomplete -->
    $('input[name="filter_vendor_name"]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=stock/stocksearch/autocompleteVendor&token=<?php echo $token; ?>&filter_vendor=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['vendor_name'],
                            value: item['vendor_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name="filter_vendor_name"]').val(item['label']);
            $('input[name="filter_vendor_id"]').val(item['value']);
        }
    });
    <!-- 供应商名称autocomplete -->
	
	<!--获取url参数-->
	function GetUrlData(name)
	{
     	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     	var r = window.location.search.substr(1).match(reg);
     	if(r!=null)return  unescape(r[2]); return null;
	}
	<!--获取url参数-->
	
	<!--导出库存-->
	$('#button-export').on('click', function() {
		
		url = 'index.php?route=stock/stocksearch/exportInventory&token=<?php echo $token; ?>';
	
		var filter_warehouse =  GetUrlData('filter_warehouse');
		if (filter_warehouse) {
			url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
		}
		
		var filter_product_code = GetUrlData('filter_product_code');
		if(filter_product_code){
			url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
		}
		
		var filter_warehouse_position = GetUrlData('filter_warehouse_position');
		if (filter_warehouse_position) {
			url += '&filter_warehouse_position=' + encodeURIComponent(filter_warehouse_position);
		}
		
		var filter_sku = GetUrlData('filter_sku');
		if(filter_sku){
			url += '&filter_sku=' + encodeURIComponent(filter_sku);
		}
				
		location = url;
	});
	<!--导出库存-->

</script>
<?php echo $footer; ?>