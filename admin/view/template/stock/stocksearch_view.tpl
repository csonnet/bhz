<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
            	<a href="<?php echo $export_url; ?>" data-toggle="tooltip" title="导出库存历史" class="btn btn-info"><i class="fa fa-print"></i></a>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1>库存查询</h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
 				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
    
	<div class="container-fluid">
        
		<div class="panel panel-default">
        
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> 库存商品信息</h3>
			</div>
            
			<div class="panel-body">
				
                <dl class="dl-horizontal col-xs-6">
					<dt>商品名称</dt>
 					<dd><?php echo $inventory['product_name'] ?></dd>
                    <dt>选项</dt>
					<dd><?php echo $inventory['option_name'] ?></dd>
					<dt>商品编码</dt>
					<dd><?php echo $inventory['product_code'] ?></dd>
                    <dt>条形码</dt>
					<dd><?php echo $inventory['product_sku'] ?></dd>
                    
                    <br />
                    
                    <dt>财务数量</dt>
  					<dd><?php echo $inventory['account_quantity'] ?></dd>
                    <dt>可用数量</dt>
  					<dd><?php echo $inventory['available_quantity'] ?></dd>  
				</dl>
                
                <dl class="dl-horizontal col-xs-6">
                    <dt>仓库</dt>
					<dd><?php echo $inventory['warehouse_name'] ?></dd>
					<dt>库位1</dt>
  					<dd><?php echo $inventory['position1'] ?></dd>
                    <dt>库位2</dt>
  					<dd><?php echo $inventory['position2'] ?></dd>
			</dl>
    
		</div>
        
    </div>
    
    <br />
    <br />
    
    <!--历史操作-->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-comment-o"></i> 库存历史</h3>
		</div>
		<div class="panel-body">
			<div class="tab-content">
            
				<table class="table table-bordered bhz-table">
  					<thead>
   						<tr>
                			<th class="text-left">单号</th>
                            <th class="text-left">类型</th>
                            <th class="text-left">相关单据</th>
                            <th class="text-left">对财务数量影响</th>
                            <th class="text-left">对可用数量影响</th>
                			<th class="text-left">操作员</th>
                			<th class="text-left">创建时间</th>
                            <th class="text-left">状态</th>
              			</tr>
            		</thead>
            		<tbody>
              			<?php if ($inventory_histories) { ?>
              			<?php foreach ($inventory_histories as $value) { ?>
              			<tr>
                        	<td class="text-left"><?php echo $value['id']; ?></td>
                            <td class="text-left"><?php echo $value['type']; ?></td>
                            <?php if($value['refer_id']) { ?>
                            <td class="text-left"><?php echo $value['refer_name']; ?> ( <?php echo $value['refer_id']; ?> )</td>
                            <?php } else { ?>
                            <td class="text-left">-</td>
                            <?php } ?>
                            <td class="text-left">
                            	<?php if ($value['stock_status'] >= 2 || $value['trim_status'] == 3) { ?>
                            	<?php echo $value['calculate']; ?><?php echo $value['qty']; ?>
                                 <?php } else { ?>
                                 0
                                 <?php } ?>
                    		</td>
                			<td class="text-left"><?php echo $value['calculate']; ?><?php echo $value['qty']; ?></td>
                            <td class="text-left"><?php echo $value['user_name']; ?></td>
                            <td class="text-left"><?php echo $value['date_added']; ?></td>
                            <td class="text-left"><?php echo $value['status']; ?></td>
              			</tr>
              			<?php } ?>
              			<?php } else { ?>
              			<tr>
                			<td class="text-center" colspan="99">无记录</td>
              			</tr>
              			<?php } ?>
            		</tbody>
          		</table>
          
  			</div>

			<div class="row">
 				<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
			</div>

		</div>
	</div>
    <!--历史操作-->
    
  </div>
  
</div>

<?php echo $footer; ?>
