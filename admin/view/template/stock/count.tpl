<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start"  value="<?php echo $filter_date_start;?>" placeholder="搜索单据开始时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
              <div  class="form-group  {{marketError?'has-error':''}}">
                  <label class="control-label">业务员</label>
                  <input name="salesman_name" type="text" id="salesman_name" class="form-control" placeholder="业务员" />
                  <input name="salesman" type="hidden" id="salesman"  />
                 <!--  <div class="text-danger">{{marketError}}</div> -->
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索单据结束时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div> 

              <div class="form-group">
                <label class="control-label" for="input-status">差额是否为零</label>
                <select name="is_zero" id="is_zero" class="form-control">
                    <option value="1" <?php if($is_zero==1){echo 'selected="selected"'; } ?> >差额为零 </option>
                    <option value="2" <?php if($is_zero==2){echo 'selected="selected"'; } ?> >差额不为零 </option>
                    <option value="*" <?php if($is_zero=='*'){echo 'selected="selected"'; } ?>>全部 </option>
                </select>
              </div>
              
            </div>

            <div class="col-sm-4">
              <div  class="form-group  {{marketError?'has-error':''}}">
                  <label class="control-label">对账超市</label>
                  <input name="market_name" type="text" id="market_name" class="form-control" placeholder="对账超市" />
                  <input name="market" type="hidden" id="market"  />
                 <!--  <div class="text-danger">{{marketError}}</div> -->
              </div>
              <div class="form-group">
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>查询</button>
       <!--          <a id="button-export-lists" data-toggle="tooltip" title="" class="btn btn-primary pull-right" style="margin-left:10px;" data-original-title="导出对账单"><i class="fa fa-calculator"></i> 导出对账单</a> -->
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
<!--                   <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>对账超市</td>
                  <td>业务员</td>
                  <td>应收总额</td> 
                  <td>实收总额</td>
                  <td>应付总额</td>
                  <td>实付总额</td>
                  <td>差额</td>

                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($acount) { ?>
                  <?php foreach ($acount as $acount) { ?>

                  <tr>
                    <td><?php echo $acount['payer_name'] ?></td>
                    <td><?php echo $acount['fullname'] ?></td>
                    <td><?php echo $acount['receivable'] ?></td>
                    <td><?php echo $acount['received'] ?></td>
                    <td><?php echo $acount['money_out']['payables'] ?></td>
                    
                    <td><?php echo $acount['money_out']['paid'] ?></td>
                    <td><?php echo $acount['differ'] ?></td>
                    <td><input type="hidden" name="payer_id" value="<?php echo $acount['payer_id'] ?>">
                    <input type="hidden" name="money_out_id" value="<?php echo $acount['money_out_id'] ?>">
                    <a href="<?php echo URL('stock/acount/detail', 'token='.$token.'&payer_id='.$acount['payer_id'].'&money_out_id='.$acount['money_out_id'].'&filter_date_start='.$filter_date_start.'&filter_date_end='.$filter_date_end) ?> " data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-eye"></i></td>
                  </tr>
                  <?php } ?>

                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有采购单数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>

<script type="text/javascript">
  <!--对账超市autocomplete-->
  $('input[name=\'market_name\']').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=sale/return_main_order/marketAC&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['company_name']+'('+item['telephone']+')',
              value: item['customer_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $('input[name=\'market_name\']').val(item['label']);
      $('input[name=\'market\']').val(item['value']);
    }
  });
  <!--对账超市autocomplete-->

   <!--业务员-->
$('input[name=\'salesman_name\']').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=stock/acount/salesman&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['fullname']+'('+item['recommended_code']+')',
              value: item['user_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $('input[name=\'salesman_name\']').val(item['label']);
      $('input[name=\'salesman\']').val(item['value']);
    }
  });
  <!--业务员-->
  $('#button-filter').on('click', function() {
  url = 'index.php?route=stock/acount&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var salesman = $('input[name=\'salesman\']').val();

  if (salesman) {
    url += '&salesman=' + encodeURIComponent(salesman);
  }
  var is_zero = $('select[name=\'is_zero\']').val();

  if (is_zero) {
    url += '&is_zero=' + encodeURIComponent(is_zero);
  }
  var market = $('input[name=\'market\']').val();
    if(market){
      url += '&market=' + encodeURIComponent(market);
    }
  location = url;
});

$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>