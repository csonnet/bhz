<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="save_money('<?php echo $token ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> 保存</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
              <div class="form-group required">
               <label class="col-sm-2 control-label" for="input-status">相关单据类型</label>
              <div class="col-sm-10">
                <select name="money_type" ng-model="money_type" id="money_type" class="form-control">
                  <?php foreach ($money_type as $key=>$value) { ?>
                      <option value="<?php echo $value['money_type_id']; ?>"><?php echo $value['type_name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">相关单据号</label>
              <div class="col-sm-10">
                <input type="text" name="refer_id" ng-model="refer_id"  placeholder="相关单据号" id="refer_id" class="form-control" />
                <input type="hidden" name="customer_id" ng-model="customer_id"  id="customer_id" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">付款人</label>
              <div class="col-sm-10">
                <input type="text" name="payer_name" ng-model="payer_name"  placeholder="付款人" id="payer_name" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">相关单据金额</label>
              <div class="col-sm-10">
                 <input type="text" name="ori_total" ng-model="ori_total"  placeholder="相关单据金额" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">应收金额</label>
              <div class="col-sm-10">
                 <input type="text" name="receivable" ng-model="receivable"  placeholder="应收金额" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_price) { ?>
                    <div class="text-dangerreadonlyphp echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">实收金额</label>
              <div class="col-sm-10">
                 <input type="text" name="received" ng-model="received"  placeholder="实收金额" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
               <label class="col-sm-2 control-label" for="input-status">状态</label>
              <div class="col-sm-10">
                <select name="status" ng-model="status" id="input-status" class="form-control">
                  <?php foreach ($status as $key=>$value) { ?>
                      <option value="<?php echo $value['money_in_status_id']; ?>"><?php echo $value['name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">备注</label>
              <div class="col-sm-10">
                <textarea name="comment" ng-model="comment" class="form-control" id="comment" rows="8"></textarea>
              </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--

$('#luckydraw-products').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 

  <script type="text/javascript"><!--
  var stock_list=[];
  var money_in=[];
  var money_out=[];
<!--相关单据号autocomplete-->

  $('input[name=\'refer_id\']').autocomplete({

    'source': function(request, response) {

      var money_type = $('#money_type').val();
      if (money_type==1||money_type==4) {
        $.ajax({
          url: 'index.php?route=stock/money_in/searchOrder&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&money_type='+money_type,
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['refer_id'],
                value: item['refer_id'],
                payment_company: item['payment_company'],
                receivable: item['receivable'],
                ori_total: item['ori_total']
              }
            }));
          }
        });
      }
      
      
    },
    'select': function(item) {
      $('input[name=\'refer_id\']').val(item['label']);
      $('input[name=\'payer_name\']').val(item['payment_company']);
      $('input[name=\'customer_id\']').val(item['customer_id']);
      $('input[name=\'receivable\']').val(item['receivable']);
      $('input[name=\'ori_total\']').val(item['ori_total']);
    }
  });
  <!--相关单据号autocomplete-->
//--></script>  


<?php echo $footer; ?> 