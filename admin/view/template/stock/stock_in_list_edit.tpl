<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="save_stock('<?php echo $token ?>',<?php echo $stock_list['warehouse_id'] ?>,<?php echo $stock_list['refer_type_id'] ?>)" data-toggle="tooltip" title="完成" class="btn btn-primary complete" id="complete" ><i class="fa fa-save"></i> 完成</button>
        <button ng-click="save_edit('<?php echo $token ?>')" data-toggle="tooltip" title="保存" class="btn btn-primary"><i class="fa fa-save"></i> 保存</button>
        <a  href="<?php echo $cancel; ?>" data-toggle="tooltip" title="返回" class="btn btn-default"><i  class="fa fa-reply"></i> 返回</a>
        </div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
          <div class="panel-body">
            <dl class="dl-horizontal col-xs-6">
              <dt>入库单号</dt>
              <dd><?php echo $stock_list['in_id']; ?></dd>
              <dt>相关单据号</dt>
              <dd><?php echo $stock_list['refer_id']; ?></dd>
              <dt>相关单据类型</dt>
              <dd><?php echo $stock_list['refer_type']; ?></dd>
              <dt>入库仓库</dt>
              <dd><?php echo $stock_list['warehouse_name']; ?></dd>
              <dt>操作人</dt>
              <dd ><?php echo $stock_list['username']; ?></dd>
              <dt>本单据采购总金额</dt>
              <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$stock_list['buy_money']; ?></dd>
            
             </dl>

          <dl class="dl-horizontal col-xs-6">
            <dt>单据状态</dt>
            <dd><?php echo $stock_list['status_name']; ?></dd>
            <dt>入库时间</dt>
            <dd><?php echo $stock_list['in_date']; ?></dd>
            <dt>创建时间</dt>
            <dd><?php echo $stock_list['date_added']; ?></dd>
            <dd><input type="hidden" name="warehouse_name" ng-model="warehouse_id" value="<?php echo $stock_list['warehouse_id'] ?>"></dd>

          </dl>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>相关产品</legend>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left">商品编码</td>
                  <td class="text-left">商品名称</td>
                  <td class="text-left">条形码</td>
                  <td class="text-left">选项</td>
                  <td class="text-left">单价</td>
                  <td class="text-left">入库数量</td>
                  <td class="text-left">操作</td>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="product in products">
                  <td class="{{product.productError?'has-error':''}}">
                  <div class="dropdown">
                    <div  class="input-group"  data-toggle="dropdown" aria-expanded="true">
                      <input type="text"  maxlength="85" ng-change="getProduct(product.product_code, '<?php echo $token ?>')" ng-model="product.product_code" class="form-control" placeholder="商品编码" aria-describedby="basic-addon2">
                    </div>
                    <div class="text-danger">{{product.productError}}</div>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                        <div class="checkbox">
                          <label>
                          {{p.product_code}}{{p.name}}
                          </label>
                        </div>
                      </li>
                    </ul>
                  </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">{{product.name}}
                      <!-- <input type="text" ng-model="" size="60"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.sku}}
                      <!-- <input type="text" ng-model="product.sku"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.oname}}
                      <!-- <input type="text" ng-model="product.sku"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                 <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.product_price}}
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.product_quantity" class="form-control" placeholder="入库数量">
                    </div>
                    <div class="text-danger">{{product.productNameError}}</div>
                 </td>
                  <td>
                    <button ng-show="!is_edit" type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="6"></td>
                  <td><button ng-show="!is_edit" type="button" ng-click="products.push({quantity:1, sort_order:10});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
            
          </fieldset>
        </form>

            <!--历史操作-->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-comment-o"></i> 入库单历史</h3>
    </div>
    <div class="panel-body">
      <div class="tab-content">
            
        <table class="table table-bordered bhz-table">
            <thead>
              <tr>
                      <th class="text-left">添加时间</th>
                      <th class="text-left">操作员</th>
                      <th class="text-left">操作内容</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($histories) { ?>
                    <?php foreach ($histories as $history) { ?>
                    <tr>
                      <td class="text-left"><?php echo $history['date_added']; ?></td>
                      <td class="text-left"><?php echo $history['operator_name']; ?></td>
                      <td class="text-left"><?php echo $history['comment']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="99">无记录</td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
          
        </div>
    </div>
  </div>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--

$('#luckydraw-products').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 

  <script type="text/javascript"><!--
  <?php if(!empty($stock_list)) { ?>
    var stock_list = <?php echo json_encode($stock_list);?>;
    var money_out = {}; 
    var money_in = {}; 
  <?php } else { ?>
    var stock_list = {};
    var money_out = {}; 
    var money_in = {};
  <?php } ?>
//--></script>       
<?php echo $footer; ?> 