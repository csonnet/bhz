<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        </div>
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="row">
          <div class="col-sm-5 col-sm-offset-7">
            <div class="form-group">
              <div class="input-group year_month">
                <input type="text" name="distributor_bill_month" value="" placeholder="对账月份" data-date-format="YYYY-MM" id="distributor_bill_month" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
                <a id="button-distributor_bill" data-toggle="tooltip" title="生成经销商对账单" class="btn btn-primary pull-right"><i class="fa fa-calculator"></i> 生成经销商对账单</a>
              </div>
            </div>
          </div>
        </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id">对账单编号</label>
                <input type="text" name="filter_distributor_bill_id" value="<?php echo $filter_distributor_bill_id; ?>" placeholder="对账单编号" id="input-vendor-bill-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-vendor_name">经销商</label>
                <input type="text" name="filter_vendor_name" value="<?php echo $filter_vendor_name; ?>" placeholder="经销商" id="input-vendor-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-year-month">年月份</label>
                <div class="input-group year_month">
                  <input type="text" name="filter_year_month" value="<?php echo $filter_year_month; ?>" placeholder="年月份" data-date-format="YYYY-MM" id="input-year-month" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">金额</label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="金额" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group hidden">
                <label class="control-label" for="input-date-added">添加日期</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="添加日期" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-distributor_bill-status">状态</label>
                <select name="filter_distributor_bill_status" id="input-distributor_bill-status" class="form-control">
                  <option value=""></option>
                  <?php foreach ($distributor_bill_statuses as $distributor_bill_status_id=>$distributor_bill_status_name) { ?>
                  <?php if ($distributor_bill_status_id == $filter_distributor_bill_status && $filter_distributor_bill_status!='') { ?>
                  <option value="<?php echo $distributor_bill_status_id; ?>" selected="selected"><?php echo $distributor_bill_status_name; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $distributor_bill_status_id; ?>"><?php echo $distributor_bill_status_name; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <a id="button-export-lists" data-toggle="tooltip" title="导出对账单" class="btn btn-primary pull-right" style="margin-left:10px;"><i class="fa fa-calculator"></i> 导出对账单</a>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> 筛选</button>
            </div>
          </div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-vendor-bill">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <!-- <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td> -->
                  <td class="text-right"><?php if ($sort == 'lb.distributor_bill_id') { ?>
                    <a href="<?php echo $sort_distributor_bill; ?>" class="<?php echo strtolower($order); ?>">对账单编号</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_distributor_bill; ?>">对账单编号</a>
                    <?php } ?></td>
                  <td class="text-left">经销商名称</td>
                  <td class="text-left"><?php if ($sort == 'lb.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">状态</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>">状态</a>
                    <?php } ?></td>
                  <td class="text-right">实际收货金额</td>
                  <td class="text-right">差额</td>
                  <td class="text-right">应付货款</td>
                  <td class="text-left"><?php if ($sort == 'lb.year_month') { ?>
                    <a href="<?php echo $sort_year_month; ?>" class="<?php echo strtolower($order); ?>">年月份</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_year_month; ?>">年月份</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'lb.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>">添加日期</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>">添加日期</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'lb.date_modified') { ?>
                    <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>">修改日期</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_modified; ?>">修改日期</a>
                    <?php } ?></td>
                  <td class="text-right">操作</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($distributor_bills) { ?>
                <?php foreach ($distributor_bills as $distributor_bill) { ?>
                <tr>
                  <td class="text-right"><?php echo $distributor_bill['distributor_bill_id']; ?></td>
                  <td class="text-left"><?php echo $distributor_bill['vendor_name']; ?></td>
                  <td class="text-left"><?php echo $distributor_bill['status_name']; ?></td>
                  <td class="text-right"><?php echo $distributor_bill['total']; ?></td>
                  <td class="text-right"><?php echo $distributor_bill['difference']; ?></td>
                  <td class="text-right"><?php echo $distributor_bill['total']+$distributor_bill['difference']; ?></td>
                  <td class="text-right"><?php echo date('Y-m', strtotime($distributor_bill['year_month'])); ?></td>
                  <td class="text-left"><?php echo $distributor_bill['date_added']; ?></td>
                  <td class="text-left"><?php echo $distributor_bill['date_modified']; ?></td>
                  <td class="text-right">
                    <?php if($distributor_bill['status']==0) { ?>
                    <button type="button" value="<?php echo $distributor_bill['distributor_bill_id']; ?>" id="button-delete<?php echo $distributor_bill['distributor_bill_id']; ?>" data-loading-text="加载中..." data-toggle="tooltip" title="删除" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    <?php } ?>
                    <a href="<?php echo URL('stock/distributor/view', 'token='.$token.'&distributor_bill_id='.$distributor_bill['distributor_bill_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = 'index.php?route=stock/distributor&token=<?php echo $token; ?>';

  var filter_distributor_bill_id = $('input[name=\'filter_distributor_bill_id\']').val();

  if (filter_distributor_bill_id) {
    url += '&filter_distributor_bill_id=' + encodeURIComponent(filter_distributor_bill_id);
  }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }

  var filter_distributor_bill_status = $('select[name=\'filter_distributor_bill_status\']').val();

  if (filter_distributor_bill_status != '*') {
    url += '&filter_distributor_bill_status=' + encodeURIComponent(filter_distributor_bill_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_year_month = $('input[name=\'filter_year_month\']').val();

  if (filter_year_month) {
    url += '&filter_year_month=' + encodeURIComponent(filter_year_month);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/distributor/delete&token=<?php echo $token; ?>&distributor_bill_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('#button-export-lists').on('click', function(e) {
  // alert(11);
  url = 'index.php?route=stock/distributor/exportLists&token=<?php echo $token; ?>';

  var filter_distributor_bill_id = $('input[name=\'filter_distributor_bill_id\']').val();

  if (filter_distributor_bill_id) {
    url += '&filter_distributor_bill_id=' + encodeURIComponent(filter_distributor_bill_id);
  }

  var filter_vendor_name = $('input[name=\'filter_vendor_name\']').val();

  if (filter_vendor_name) {
    url += '&filter_vendor_name=' + encodeURIComponent(filter_vendor_name);
  }

  var filter_distributor_bill_status = $('select[name=\'filter_distributor_bill_status\']').val();

  if (filter_distributor_bill_status != '*') {
    url += '&filter_distributor_bill_status=' + encodeURIComponent(filter_distributor_bill_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_year_month = $('input[name=\'filter_year_month\']').val();

  if (filter_year_month) {
    url += '&filter_year_month=' + encodeURIComponent(filter_year_month);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  location = url;
});

$('#button-distributor_bill').on('click', function(e) {
  var distributor_bill_month = $('#distributor_bill_month').val();
  if(!distributor_bill_month || isNaN(Date.parse(distributor_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('stock/distributor/genNewVendorBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+distributor_bill_month;
});


//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});
//--></script></div>
<?php echo $footer; ?>
