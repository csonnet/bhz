<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="save_luckydraw('<?php echo $token ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> 保存</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-luckydraw-name">相关单据号</label>
              <div class="col-sm-10">
                <input type="text" name="refer_id" ng-model="refer_id" value="<?php echo $refer_id; ?>" placeholder="相关单据号" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">相关单据类型</label>
              <div class="col-sm-10">
                <select name="refer_type" ng-model="refer_type" id="input-refer_type" class="form-control">
                  <?php foreach ($refer_type as $key=>$value) { ?>
                     <option value="<?php echo $value['refer_type_id']; ?>"><?php echo $value['name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>

            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">入库仓库</label>
              <div class="col-sm-10">
                <select name="warehouse" ng-model="warehouse" id="input-warehouse" class="form-control">
                  <?php foreach ($warehouse as $key=>$value) { ?>
                      <option value="<?php echo $value['warehouse_id']; ?>"><?php echo $value['name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>

            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">状态</label>
              <div class="col-sm-10">
                <select name="status" ng-model="status" id="input-status" class="form-control">
                  <?php foreach ($status as $key=>$value) { ?>
                      <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>

          <div class="form-group">
                <label class="col-sm-2 control-label">备注</label>
                <div class="col-sm-10">
                  <textarea name="comment" ng-model="comment" class="form-control" id="comment" rows="8"></textarea>
                </div>
          </div>

          </fieldset>
          <fieldset>
            <legend>相关产品</legend>
            <table class="table table-bordered">
              <thead>
                <tr><td class="text-left">商品编码</td>
                  <td class="text-left">商品名称</td>
                  <td class="text-left">条形码</td>
                  <td class="text-left">选项</td>
                   <td class="text-left">单价</td>
                  <td class="text-left">入库数量</td>
                 
                  <td class="text-left">操作</td>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="product in products">
                  <td class="{{product.productError?'has-error':''}}">
                  <div class="dropdown">
                    <div  class="input-group"  data-toggle="dropdown" aria-expanded="true">
                      <input type="text"  maxlength="85" ng-change="getProduct(product.product_code, '<?php echo $token ?>')" ng-model="product.product_code" class="form-control" placeholder="商品编码" aria-describedby="basic-addon2">
                    </div>
                    <div class="text-danger">{{product.productError}}</div>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProduct(p,product)">
                        <div class="checkbox">
                          <label>
                          {{p.product_code}}{{p.name}}
                          </label>
                        </div>
                      </li>
                    </ul>
                  </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">{{product.name}}
                      <!-- <input type="text" ng-model="" size="60"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.sku}}
                      <!-- <input type="text" ng-model="product.sku"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.oname}}
                      <!-- <input type="text" ng-model="product.sku"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                    {{product.product_price}}
                      <!-- <input type="text" ng-model="product.sku"  disabled=＂disabled＂ class="form-control" placeholder="商品名称"> -->
                    </div>
                  </td>
                  <td class="{{product.productNameError?'has-error':''}}">
                    <div  class="input-group">
                      <input type="text" ng-model="product.product_quantity" class="form-control" placeholder="入库数量">
                    </div>
                    <div class="text-danger">{{product.productNameError}}</div>
                  </td>
                  <td>
                    <button ng-show="!is_edit" type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="6"></td>
                  <td><button ng-show="!is_edit" type="button" ng-click="products.push({quantity:1, sort_order:10});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--

$('#luckydraw-products').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 

  <script type="text/javascript"><!--
  var stock_list=[];
    var money_out = {}; 
    var money_in = {}; 
  
// $('.date').datetimepicker({
//   pickTime: false
// });

// $('.time').datetimepicker({
//   pickDate: false
// });

// $('.datetime').datetimepicker({
//   pickDate: true,
//   pickTime: true
// });
//--></script>  


<?php echo $footer; ?> 