<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="bhzAdmin" ng-controller="bhzAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">

	  <button class="btn btn-primary" ng-click="save_lend('<?php echo $token ?>')"><i class="fa fa-save"></i> 保存</button>
	  <a class="btn btn-default" href="<?php echo $return_url?>"><i class="fa fa-reply"></i> 返回</a>
	  </div>
      <h1>添加借货单</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      </div>
  </div>
  <div class="container-fluid">
  	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<div class="panel panel-default">
	  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 添加借货单</h3>
      </div>
      <div class="panel-body">
	  <form method="post" enctype="multipart/form-data"  class="form-horizontal" id="form-order">
		<fieldset>
			<legend>基本信息</legend>
			<div class="form-group required">
				<label class="col-sm-2 control-label">借货仓</label>
				<div class="col-sm-10">
					<select id="fwarehouse" class="form-control">
					<?php foreach($all_warehouses as $val){?>
					<option value="<?php echo $val['warehouse_id']?>" <?php echo $warehouse_id_selected[$val['warehouse_id']]?> ><?php echo $val['name']?></option>
					<?php }?>
					</select>
					<div class="{{FwarehouseError?'text-danger':''}}">{{FwarehouseError}}</div>
				</div>
			</div>

			<div class="form-group required">
				<lable class="col-sm-2 control-label">到货营销部</lable>
				<div class="col-sm-10">
					<select id="logcenter" class="form-control">
						<option ng-repeat="l in logcenter" ng-click="changelogcenter(l)" value="{{l.logcenter_id}}">{{l.logcenter_name}}</option>
					</select>
					<div class="{{TlogcenterError?'text-danger':''}}">{{TlogcenterError}}</div>
				</div>
			</div>

			<div class="form-group required">
				<lable class="col-sm-2 control-label">地址、人名、电话</lable>
				<div class="col-sm-4">
					<input id="lend_logcenter_address" class="form-control" type="text" value=""/>
					<div class="{{AddressError?'text-danger':''}}">{{AddressError}}</div>
				</div>
				<div class="col-sm-3">
					<input id="lend_logcenter_name" class="form-control" type="text" value=""/>
					<div class="{{NameError?'text-danger':''}}">{{NameError}}</div>
				</div>
				<div class="col-sm-3">
					<input id="lend_logcenter_tel" class="form-control" type="text" value=""/>
					<div class="{{TelError?'text-danger':''}}">{{TelError}}</div>
				</div>
			</div>

		</fieldset>
		<fieldset>
			<legend>借出商品</legend>
			<table class="table ">
			<thead>
				<tr>
					<td>商品编码</td>
					<td>商品名称</td>
					<td>商品数量</td>
					<td>条形码</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="product in products">
					<td class="{{product.codeError?'has-error':''}}">
	                  <div class="dropdown">
	                    <div  class="input-group" data-toggle="dropdown" aria-expanded="true">
	                      <input type="text" ng-change="getProductsByCode(product.code, '<?php echo $token ?>')" ng-model="product.code" class="form-control" placeholder="商品编码" aria-describedby="basic-addon2">
	                    </div>
	                    <div class="text-danger">{{product.codeError}}</div>
	                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
	                      <li class="vertical-dropdown" ng-repeat="p in searchProducts" ng-click="selectProductSec(p,product)">
	                        <div class="checkbox">
	                          <label>
	                             {{p.code}} {{p.pdname}}
	                          </label>
	                        </div>
	                      </li>
	                    </ul>
	                  </div>
	                </td>
					<td class="{{product.nameError?'has-error':''}}">
						<div class="dropdown">
							<div  class="input-group" aria-expanded="true">
		                      <text style="min-width: 300px" disabled ng-model="product.name" class="form-control" placeholder="商品名称" aria-describedby="basic-addon2">
							  {{product.name}}
		                      </text>
		                    </div>
		                    <div class="text-danger">{{product.nameError}}</div>
						</div>
					</td>
					<td class="{{product.numError?'has-error':''}}">
						<div class="dropdown">
							<div  class="input-group" aria-expanded="true">
		                      <input type="text" ng-change="caulatetotal(product)" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"  ng-model="product.num" class="form-control" placeholder="商品数量" aria-describedby="basic-addon2">
		                    </div>
		                    <div class="text-danger">{{product.numError}}</div>
						</div>
					</td>
					<td class="{{product.skuError?'has-error':''}}">
		                <div class="dropdown">
		                  <div  class="input-group" aria-expanded="true">
		                              <input type="text" disabled name="product_sku" ng-model="product.sku" class="form-control" placeholder="条形码" aria-describedby="basic-addon2">
		                            </div>
		                            <div class="text-danger">{{product.skuError}}</div>
		                </div>
		              </td>
					<td>
	                  <button type="button" ng-click="removeproduct($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除产品"><i class="fa fa-remove"></i></button>
	                </td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
	            <td colspan="5"></td>
	            <td ><button type="button" ng-click="products.push({qty:1});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加产品"><i class="fa fa-plus-circle"></i></button></td>
	          	</tr>
			</tfoot>
      		</table>
		</fieldset>
		<fieldset>

			<legend>审批人</legend>

			<table class="table ">

			<thead>

				<tr>
					<td>审批人会员名</td>
					<td>审批人全名</td>
					<td>操作</td>
				</tr>

			</thead>

			<tbody>

				<tr ng-repeat="signman in signmans">
					<td>
						<div>
							<select ng-model="signman.user_id" class="form-control"   class="form-control" ng-change="changeuser(signman,'<?php echo $token ?>')">
								<option ng-repeat="u in signman.users" ng-repeat="u in signman.users"  value="{{u.user_id}}">{{u.username}}</option>
							</select>
							<div class="text-danger">{{signman.IdError}}</div>
						</div>
					</td>
					<td>
						<div>
							<input type="text" class="form-control"  ng-value="truename"   placeholder="审批人全名" disabled >
						</div>
					</td>
					<td>
						<button type="button" ng-click="removesignman($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除审批人"><i class="fa fa-remove"></i></button>
					</td>

				</tr>
			</tbody>

			<tfoot>
				<tr>
	            	<td colspan="2"></td>
	            	<td ><button type="button" ng-click="addsignman()" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加审批人"><i class="fa fa-plus-circle"></i></button></td>
	          	</tr>
				</tfoot>

			</table>

		</fieldset>
		<fieldset>

			<div class="form-group">

				<lable class="col-sm-2 control-label">备注</lable>
				<div class="col-sm-10">
					<textarea class="form-control" rows="5" id="comment">

					</textarea>
				</div>

			</div>

		</fieldset>
	  </form>
      </div>
	</div>
  </div>
</div>
<script>

	var loan_order_id = <?php echo $loan_order_id?>;

	$(function(){

		var appElement = document.querySelector('[ng-controller=bhzAdminCtrl]');

		var $scope = angular.element(appElement).scope();

		$scope.getlogcenters('<?php echo $token?>',loan_order_id);

		$scope.getlendproducts('<?php echo $token?>',loan_order_id);

		$scope.getallusers('<?php echo $token?>',loan_order_id);


    })

</script>
