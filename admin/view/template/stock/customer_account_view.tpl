<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
             <div class="row">

            
          <div class="col-sm-5 col-sm-offset-7">
            <div class="pull-right">
              <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a>
            </div>
          </div>
        </div>
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
<div class="panel-body">
          <form action="" method="post" enctype="multipart/form-data" id="form-warehouse">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td>手机号</td>
                  <td>发送时间</td>
                  <td>发送状态</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($log_list) { ?>
                  <?php foreach ($log_list as $log) { ?>
                    <td><?php echo $log['telphone'] ?></td>
                    <td><?php echo $log['date_added'] ?></td>
                    <td><?php if($log['return_status']){echo '成功';}else{ echo '失败';}  ?></td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          </form>

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>
<?php echo $footer; ?>