<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="save_refund('<?php echo $token ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> 保存</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
              <div class="form-group required" >
               <label class="col-sm-2 control-label" for="input-status" style="color: #666666" >类型</label>
              <div class="col-sm-10" >
              <input type="text" name="payables" value="退款" id="input-luckydraw-name" class="form-control" readonly />
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">相关订单号</label>
              <div class="col-sm-10">
                <input type="text" name="refer_id" ng-model="refer_id"  placeholder="相关单据号" id="refer_id" class="form-control" />
                <input type="hidden" name="payee_id" ng-model="payee_id"  id="payee_id" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">收款人</label>
              <div class="col-sm-10">
                <input type="text" name="payee" ng-model="payee"  placeholder="收款人" id="payee" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
             <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">申请退款金额</label>
              <div class="col-sm-10">
                 <input type="text" name="payables" ng-model="payables"  placeholder="申请退款金额" id="input-luckydraw-name" class="form-control" />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">备注</label>
              <div class="col-sm-10">
                <textarea name="comment" ng-model="comment" class="form-control" id="comment" rows="8"></textarea>
              </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--

$('#luckydraw-products').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
//--></script> 

  <script type="text/javascript"><!--
  var stock_list=[];
  var money_in=[];
  var money_out=[];
<!--相关单据号autocomplete-->
  $('input[name=\'refer_id\']').autocomplete({
    'source': function(request, response) {
        $.ajax({
          
          url: 'index.php?route=stock/refund/searchOrder&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request)+'&money_type=7',
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              if (item['error']) {
                $('#money_error').html(item['error']);

              }else{
                $('#money_error').empty();
                return {
                  label: item['refer_id'],
                  payee: item['payee'],
                  value: item['refer_id'],
                  payee_id: item['payee_id'],
                }
              }
              
            }));
          }
        });
    },
    'select': function(item) {
      $('input[name=\'refer_id\']').val(item['label']);
      $('input[name=\'payee\']').val(item['payee']);
      $('input[name=\'payee_id\']').val(item['payee_id']);
    }
  });
  <!--相关单据号autocomplete-->
//--></script>  


<?php echo $footer; ?> 