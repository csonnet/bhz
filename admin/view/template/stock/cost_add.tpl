<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="save_cost('<?php echo $token ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">确认申请</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
              <div class="form-group required">
               <label class="col-sm-2 control-label" for="input-status">我是</label>
              <div class="col-sm-10">
                <select name="cost_use_type_id" ng-model="cost_use_type_id" id="cost_use_type_id" class="form-control">
                  <?php foreach ($cost_users as $key=>$value) { ?>
                      <option value="<?php echo $value['cost_use_type_id']; ?>"><?php echo $value['cost_use_name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>
            <div>
              <div class="form-group required" id="year_month">
                <label class="col-sm-2 control-label" for="input-year-month">报销月份</label>
                <div class="input-group year_month">
                  <input type="text" name="days" ng-model="days" id="days" ng-model="days"  placeholder="报销月份" data-date-format="YYYY-MM"   class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>

            <div class="form-group required">
               <label class="col-sm-2 control-label" for="input-status">报销类型</label>
              <div class="col-sm-10">
                <select name="cost_type_id" ng-model="cost_type_id" id="cost_type_id" class="form-control">
                  <?php foreach ($cost_type as $key=>$value) { ?>
                      <option value="<?php echo $value['cost_type_id']; ?>"><?php echo $value['cost_name']; ?></option>
                  <?php } ?>                
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" ng-model="start_date" id="start_date" ng-model="start_date"  placeholder="开始时间" data-date-format="YYYY-MM-DD"  class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date_start">结束时间</label>
                <div class="input-group date">
                  <input type="text" ng-model="end_date" id="end_date" ng-model="end_date"  placeholder="结束时间" data-date-format="YYYY-MM-DD"  class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">费用金额</label>
              <div class="col-sm-10">
                <input type="text" name="cost" ng-model="cost"  placeholder="费用金额" id="cost" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
               <label class="col-sm-2 control-label" for="input-status">是否需要归还</label>
              <div class="col-sm-10">
                <select name="is_return" ng-model="is_return" id="is_return" class="form-control">
                  <option value="0" selected="selected" >否</option>        
                  <option value="1">是</option>        
                </select>
              </div>
            </div>
            <div class="form-group" style="display: none;" id="return">
                <label class="col-sm-2 control-label" for="input-date_start">归还日期</label>
                <div class="input-group date">
                  <input type="text" ng-model="return_date" id="return_date" ng-model="return_date"  placeholder="归还日期" data-date-format="YYYY-MM-DD"  class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
            </div>

            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">备注</label>
              <div class="col-sm-10">
                <textarea name="comment" ng-model="comment" class="form-control" id="comment" rows="8"></textarea>
              </div>
            </div>
            <div class="form-group auditorrequired">
            <fieldset>
            <legend>请按照审核次序依次添加审核人</legend>
            <table class="table table-bordered" >
              <thead>
                <tr><td class="text-left">审核人姓名</td>
                  <td class="text-left">手机号</td>
                  <td class="text-left">操作</td>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="auditor in auditors">
                  <td class="{{auditor.auditorError?'has-error':''}}">
                  <div class="dropdown">
                    <div  class="input-group"  data-toggle="dropdown" aria-expanded="true">
                      <input type="text"  ng-change="getauditor(auditor.fullname, '<?php echo $token ?>')" ng-model="auditor.fullname" class="form-control" placeholder="审核人姓名" aria-describedby="basic-addon2">
                    </div>
                    <div class="text-danger">{{auditor.auditorError}}</div>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <li class="vertical-dropdown" ng-repeat="p in searchauditors" ng-click="selectauditor(p,auditor)">
                        <div class="checkbox">
                          <label>
                            {{p.fullname}}{{p.contact_tel}}
                          </label>
                        </div>
                      </li>
                    </ul>
                  </div>
                  </td>
                  <td class="{{auditor.auditorNameError?'has-error':''}}">
                    <div  class="input-group">{{auditor.auditor_use_tel}}
                      <input type="text" ng-model="auditor.contact_tel" size="60"  disabled=＂disabled＂ class="form-control" placeholder="手机号">
                    </div>
                  </td>
                  <td>
                    <button ng-show="!is_edit" type="button" ng-click="remove($index);" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="删除审核人"><i class="fa fa-remove"></i></button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td><button ng-show="!is_edit" type="button" ng-click="auditors.push({quantity:1, sort_order:10});" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="添加审核人"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>

          </fieldset>
        </div>
        </form>
      </div>
    </div>
  </div>


  <script type="text/javascript"><!--
  
  $('.date').datetimepicker({
  pickTime: false
  });
  $('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
  });
  $('#cost_use_type_id').click(function(){
    if($('#cost_use_type_id').val()==1){
      $('#year_month').show();

    }else{
      $('#year_month').hide();
    }
  });

  $('#is_return').click(function(){
    if($('#is_return').val()==1){
      $('#return').show();

    }else{
      $('#return').hide();
    }
  });
  var cost_list=[];
//--></script>  


<?php echo $footer; ?> 