<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	
    <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
        	
            <!--右上操作按钮-->
			<div class="pull-right">
            	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
                <button id="btn-enables" toaction="<?php echo $enable_action; ?>" type="button" data-toggle="tooltip" title="<?php echo $buttons_enable; ?>" class="btn btn-success"><i class="fa fa-play"></i></button>
        		<button id="btn-disables" toaction="<?php echo $disable_action; ?>" type="button" data-toggle="tooltip" title="<?php echo $buttons_disable; ?>" class="btn btn-danger"><i class="fa fa-ban"></i></button>
			</div>
            <!--右上操作按钮-->
            
      		<h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
    		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
        		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        		<?php } ?>
      		</ul>
            <!--面包屑导航-->
            
		</div>
	</div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
    	<!--警示消息区-->
    	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<!--警示消息区-->
    	
        <!--列表-->
  		<div class="panel panel-default">
        	
            <!--标题-->
      		<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      		</div>
			<!--标题-->
            
            <!--列表内容-->
    		<div class="panel-body">
            
    			<form action="" method="post" enctype="multipart/form-data" id="form-warehouse">
      			<div class="table-responsive">
                    <table class="table table-bordered table-hover bhz-table">
                        <thead>
                            <tr>
								<th width="5%" class="text-center">
                                	<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
								</th>
                                <th><?php echo $column_warehouse_name; ?></th>
                                <th width="10%" class="text-center"><?php echo $column_type; ?></th>
                                <th width="10%" class="text-center"><?php echo $column_order; ?></th> 
                             	<th width="10%" class="text-center"><?php echo $column_status; ?></th>
                             	<th width="25%" class="text-center"><?php echo $column_action; ?></th>     
                            </tr>
                      </thead>
                        <tbody>
                			<?php if ($warehouses) { ?>
                			<?php foreach ($warehouses as $warehouse) { ?>
                			<tr style="color:<?php echo $warehouse['tr_color']; ?>">
                  				<td class="text-center">
                         			<?php if ($warehouse['selected']) { ?>
                    				<input type="checkbox" name="selected[]" value="<?php echo $warehouse['warehouse_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                    				<input type="checkbox" name="selected[]" value="<?php echo $warehouse['warehouse_id']; ?>" />
                    				<?php } ?>
								</td>
                  				<td class="text-left"><?php echo $warehouse['name']; ?></td>
                                <td class="text-center"><?php echo $warehouse['type']; ?></td>
                                <td class="text-center"><?php echo $warehouse['order']; ?></td>
                                <td class="text-center"><?php echo $warehouse['status']; ?></td>
                  				<td class="text-center">
                                    <?php if($can_edit) { ?>
                                    <a href="<?php echo $warehouse['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    <?php } ?>
								</td>
							</tr>
                			<?php } ?>
                			<?php } else { ?>
                            <tr>
								<td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                            </tr>
                			<?php } ?>
						</tbody>
                    </table>
				</div>
				</form>
            
				<div class="row">
      				<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      				<div class="col-sm-6 text-right"><?php echo $results; ?></div>
    			</div>
                
  			</div>
            <!--列表内容-->
            
		</div>
        <!--列表-->
        
	</div>
	<!--内容-->
  
</div>

<script type="text/javascript">
	
	<!--点击批量启用-->
	$('#btn-enables').on('click', function(e){		
		if(confirm('<?php echo $text_confirm_enable; ?>')){
			var actionurl = $(this).attr('toaction');
			$('#form-warehouse').attr('action',actionurl);
			$('#form-warehouse').submit();
		}	
	});
	<!--点击批量启用-->
	
	<!--点击批量停用-->
	$('#btn-disables').on('click', function(e){		
		if(confirm('<?php echo $text_confirm_disable; ?>')){
			var actionurl = $(this).attr('toaction');
			$('#form-warehouse').attr('action',actionurl);
			$('#form-warehouse').submit();
		}	
	});
	<!--点击批量停用-->

</script>
<?php echo $footer; ?>