<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
             <div class="row">

            
          <div class="col-sm-5 col-sm-offset-7">
            <div class="pull-right">
              <a href="<?php echo $view; ?>" data-toggle="tooltip" title="查看" class="btn btn-primary">查看</a>
              <a  ng-click="confirm_action('<?php echo $token ?>')" id="confirm_action" name = "confirm_action" data-toggle="tooltip" title="批量确权" class="btn btn-primary">批量确权</a>
            </div>
            <div class="form-group">
              <div class="input-group">
                  <label class="control-label" for="input-product-name">手机号或超市名称</label>
                  <input type="text" name="search_tell" id="search_tell"  placeholder="手机号或超市名称" class="form-control" />
                  <input type="hidden" name="customer_id" id="customer_id"  />
                <a  ng-click="add_acount('<?php echo $token ?>')"  data-toggle="tooltip" title="添加账期客户" class="btn btn-primary pull-right"><i class="fa fa-calculator"></i> 添加账期客户</a>
              </div>
            </div>
          </div>
        </div>
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
<div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
            
              <div class="form-group">
                  <label class="control-label" for="input-product-name">ID</label>
                  <input type="text" name="custome_id" value="<?php echo $customer_id; ?>" placeholder="ID" id="custome_id" class="form-control" />

              </div>
              <div class="form-group">
                  <label class="control-label" for="input-product-name">业务员</label>
                  <input type="text" name="fullname" value="<?php echo $fullname; ?>" placeholder="业务员" id="fullname" class="form-control" />
                  
              </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label class="control-label" for="input-product-name">手机号</label>
                  <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="手机号" id="telephone" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label class="control-label" for="input-product-name">超市名称</label>
                  <input type="text" name="company_name" value="<?php echo $company_name; ?>" placeholder="超市名称" id="input-product-name" class="form-control" />
              </div>
              <div class="form-group">
                  
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
             <!--    <a id="button-export-lists" data-toggle="tooltip" title="" class="btn btn-primary pull-right" style="margin-left:10px;" data-original-title="导出对账单"><i class="fa fa-calculator"></i> 导出对账单</a> -->
              </div>
            </div>
          </div>
          <form action="" method="post" enctype="multipart/form-data" id="form-warehouse">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
       <!--            <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>ID</td>
                  <td>手机号</td>
                  <td>超市名称</td>
                  <td>业务员</td>
                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($account_list) { ?>
                  <?php foreach ($account_list as $account) { ?>
                    <td><?php echo $account['customer_id'] ?></td>
                    <td><?php echo $account['telephone'] ?></td>
                    <td><?php echo $account['company_name'] ?></td>
                    <td><?php echo $account['fullname'] ?></td>
                    <td>
                     <button value="<?php echo $account['customer_id']; ?>" id="button-delete<?php echo $account['customer_id']; ?>" type="button" data-toggle="tooltip" title="删除" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          </form>

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>



<script type="text/javascript">

$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/customer_account/delete&token=<?php echo $token; ?>&customer_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

  $('#button-filter').on('click', function() {
    // alert(11);
    url = 'index.php?route=stock/customer_account&token=<?php echo $token; ?>';

    var customer_id = $('input[name=\'custome_id\']').val();

    if (customer_id) {
      url += '&customer_id=' + encodeURIComponent(customer_id);
    }

    var fullname = $('input[name=\'fullname\']').val();

    if (fullname) {
      url += '&fullname=' + encodeURIComponent(fullname);
    }

    var telephone = $('input[name=\'telephone\']').val();

    if (telephone) {
      url += '&telephone=' + encodeURIComponent(telephone);
    }

    var company_name = $('input[name=\'company_name\']').val();

    if (company_name) {
      url += '&company_name=' + encodeURIComponent(company_name);
    }
    // alert(url);

    location = url;
  });
  $('input[name=\'search_tell\']').autocomplete({
    'source': function(request, response) {
        $.ajax({
          
          url: 'index.php?route=stock/customer_account/searchCustomer&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              if (item['error']) {
                $('#money_error').html(item['error']);

              }else{
                $('#money_error').empty();
                return {
                  label: item['company_name']+item['telephone'],
                  payee: item['company_name'],
                  value: item['customer_id'],
                  telephone: item['telephone'],
                }
              }
              
            }));
          }
        });
    },
    'select': function(item) {
      $('input[name=\'search_tell\']').val(item['label']);
      $('input[name=\'customer_id\']').val(item['value']);
    }
  });

</script>
<?php echo $footer; ?>