<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
      <div class="pull-right">
<a href="<?php echo URL('stock/stock_in/export', 'token='.$token.'&in_id='.$cost_list['in_id']) ?>" data-toggle="tooltip" title="导出" class="btn btn-primary"><i class="fa fa-print"></i></a>
<!--
<a href="<?php echo URL('sale/purchase_order/exportSimple', 'token='.$token.'&po_id='.$po['id']) ?>" data-toggle="tooltip" title="入库单  导出" class="btn btn-primary"><i class="fa fa-print"></i></a>
<a href="<?php echo URL('sale/main_purchase_order/exportShipping', 'token='.$token.'&po_id='.$po['id']) ?>" data-toggle="tooltip" title="标签" class="btn btn-primary"><i class="fa fa-flag"></i></a>
-->
               <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a>

      </div>
            <!--右上操作按钮-->
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
            <!--列表内容-->
          <div class="panel-body">
            <dl class="dl-horizontal col-xs-6">
              <dt>费用ID</dt>
              <dd><?php echo $cost_list['cost_id']; ?></dd>
              <dt>费用类型</dt>
              <dd><?php echo $cost_list['cost_name']; ?></dd>
              <dt>申请人</dt>
              <dd><?php echo $cost_list['fullname']; ?></dd>
              <dt>申请人类型</dt>
              <dd><?php echo $cost_list['cost_use_name']; ?></dd>
              <dt>费用金额</dt>
              <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$cost_list['cost']; ?></dd>
              <dt>审批后费用金额</dt>
              <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$cost_list['cost_auditor']; ?></dd>
            
             </dl>

          <dl class="dl-horizontal col-xs-6">
            <dt>单据开始时间</dt>
            <dd><?php echo $cost_list['start_date']; ?></dd>
            <dt>单据结束时间</dt>
            <dd><?php echo $cost_list['end_date']; ?></dd>
            <dt>报销月份</dt>
            <dd><?php echo date('Y-M',$cost_list['days']); ?></dd>
            <dt>状态</dt>
            <dd><?php echo $cost_list['status_name']; ?></dd>
            <dt>是否需要归还</dt>
            <?php if ($cost_list['status_name']==1) {?>
            <dd>是</dd>
            <?php }elseif ($cost_list['status_name']==1) { ?>
            <dd>否</dd>
            <?php } ?>
            <dt>归还时间</dt>
            <dd><?php echo $cost_list['return_date']; ?></dd>
            <dt>创建时间</dt>
            <dd><?php echo $cost_list['date_added']; ?></dd>

          </dl>
        </div>
     <table class="table">
          <thead>
            <tr>
              <td>审核人姓名</td>
              <td>审核人电话</td>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($cost_list['auditor'] as $key => $auditor) {?>
              <tr>
                <td><?php echo $auditor['auditor_use_name'] ?></td>
                <td><?php echo $auditor['auditor_use_tel'] ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>

            <!--列表内容-->
            
    </div>
            <!--历史操作-->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-comment-o"></i> 费用进度</h3>
    </div>
    <div class="panel-body">
      <div class="tab-content">
            
        <table class="table table-bordered bhz-table">
            <thead>
              <tr>
                      <th class="text-left">添加时间</th>
                      <th class="text-left">操作员</th>
                      <th class="text-left">操作内容</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($histories) { ?>
                    <?php foreach ($histories as $history) { ?>
                    <tr>
                      <td class="text-left"><?php echo $history['date_added']; ?></td>
                      <td class="text-left"><?php echo $history['operator_name']; ?></td>
                      <td class="text-left"><?php echo $history['comment']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="99">无记录</td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
          
        </div>
    </div>
  </div><!--列表-->
        
  </div>
  <!--内容-->
  
</div>


<?php echo $footer; ?>