<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
      <div class="pull-right">
              <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
      </div>
             <div class="row">
          <div class="col-sm-5 col-sm-offset-7">
            <div class="form-group">
              <div class="input-group year_month">
                <input type="text" name="vendor_bill_month" value="" placeholder="对账月份" data-date-format="YYYY-MM" id="vendor_bill_month" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
                <a id="button-vendor-bill" data-toggle="tooltip" title="生成品牌厂商对账单" class="btn btn-primary pull-right"><i class="fa fa-calculator"></i> 生成品牌厂商对账单</a>
              </div>
            </div>
          </div>
        </div>
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
<div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start"  value="<?php echo $filter_date_start;?>" placeholder="搜索单据开始时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            
              <div class="form-group">
                  <label class="control-label" for="input-product-name">收款人</label>
                  <input type="text" name="payee" value="<?php echo $payee; ?>" placeholder="收款人" id="input-product-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索单据结束时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div> 
              <div class="form-group">
                <label class="control-label" for="input-status">相关单据类型</label>
                <select name="filter_type" id="filter_type" class="form-control">
                  <?php  foreach ($money_type as $key => $value) {?>
                  <option value="<?php echo $value['money_type_id']; ?>" <?php if($filter_type==$value['money_type_id']){echo 'selected="selected"'; } ?> ><?php echo $value['type_name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <?php  foreach ($status_array as $key => $value) {?>
                  <option value="<?php echo $value['money_out_status_id']; ?>" <?php if($filter_status==$value['money_out_status_id']){echo 'selected="selected"'; } ?> ><?php echo $value['name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
               <div class="form-group">
                  <label class="control-label" for="input-product-name">相关单据号</label>
                  <input type="text" name="refer_id" value="<?php echo $refer_id; ?>" placeholder="相关单据号" id="input-product-name" class="form-control" />
                 
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
             <!--    <a id="button-export-lists" data-toggle="tooltip" title="" class="btn btn-primary pull-right" style="margin-left:10px;" data-original-title="导出对账单"><i class="fa fa-calculator"></i> 导出对账单</a> -->
              </div>
            </div>
          </div>
          <form action="" method="post" enctype="multipart/form-data" id="form-warehouse">
<!--           <div style="display: none;" class="order_total" id="order_total">
            <div>
              <span>应收总计:</span>    
              <span id="totalrec"><?php echo $total_payables;?></span> 
            </div> 
            <div>
              <span>请输入实收金额:</span>    
              <span><input type="text" name="paid"></span>
            <span><button id="btn-enables" toaction="<?php echo $enable_action; ?>" type="button" data-toggle="tooltip" title="<?php echo $buttons_enable; ?>" class="btn btn-success">确认支付</button></span> 
            </div> 
          </div> -->
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
       <!--            <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>应付款项ID</td>
                  <td>相关单据类型</td>
                  <td>相关单据号</td>
                  <td>相关单据金额</td>
                  <td>应付金额</td>
                  <td>实付金额</td>
                  <td>收款人</td>
                  <td>状态</td>
                  <td>支付时间</td>
                  <td>创建时间</td>
                  <td>创建人</td>
                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($money_list) { ?>
                  <?php foreach ($money_list as $money) { ?>
                  <tr style="color:<?php echo $money['all_color'];?>">
            <!--         <td class="text-center">
                      <input type="checkbox" name="selected[]" value="<?php echo $money['money_out_id'] ?>"  />
                    </td> -->
                    <td><?php echo $money['money_out_id'] ?></td>
                    <td><?php echo $money['name'] ?></td>
                    <td><?php echo $money['refer_id'] ?></td>
                    <td><?php echo $money['refer_total'] ?></td>
                    <td><?php echo $money['payables'] ?></td>
                    <td><?php echo $money['paid']?></td>
                    <td><?php echo $money['payee']?></td>
                    <td><?php echo $money['status_name']?></td>
                    <td><?php echo $money['pay_time'] ?></td>
                    <td><?php echo $money['date_added'] ?></td>
                    <td><?php echo $money['operator'] ?></td>
                    <td>
                    <a href="<?php echo URL('stock/money_out/view', 'token='.$token.'&money_out_id='.$money['money_out_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo URL('stock/money_out/edit', 'token='.$token.'&money_out_id='.$money['money_out_id']) ?> " data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php if($money['status']==1){ ?>
                      <button value="<?php echo $money['money_out_id']; ?>" id="button-cancel<?php echo $money['money_out_id']; ?>" type="button" data-toggle="tooltip" title="无效" class="btn btn-danger"><i class="fa fa-ban"></i></button>
                    <?php }?>
                    <?php if($money['status']==3){ ?>
                      <button value="<?php echo $money['money_out_id']; ?>" id="button-rec<?php echo $money['money_out_id']; ?>" data-toggle="tooltip" title="解除无效" class="btn btn-success"><i class="fa fa-play"></i></button>
                    <?php }?>
                    </td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          </form>

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>

<script type="text/javascript">
<?php if ($payee) {?>
  $('#order_total').show();
<?php }else{?>
  $('#order_total').hide();
<?php }?>
$('#btn-enables').on('click', function(e){    
    if(confirm('确认支付吗？')){
      var actionurl = $(this).attr('toaction');
      $('#form-warehouse').attr('action',actionurl);
      $('#form-warehouse').submit();
    } 
 });

$('button[id^=\'button-cancel\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/money_out/cancel&token=<?php echo $token; ?>&money_out_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('button[id^=\'button-rec\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/money_out/recover&token=<?php echo $token; ?>&money_out_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();
         window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('#button-filter').on('click', function() {
  url = 'index.php?route=stock/money_out&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_type = $('select[name=\'filter_type\']').val();

  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }
  var refer_id = $('input[name=\'refer_id\']').val();
    if(refer_id){
      url += '&refer_id=' + encodeURIComponent(refer_id);
    }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }
  var payee = $('input[name=\'payee\']').val();
    if(payee){
      url +='&payee='+encodeURIComponent(payee);
    }
  location = url;
});


$('#button-export-lists').on('click', function(e) {
  url = 'index.php?route=money/money_in/exportLists&token=<?php echo $token; ?>';
  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var warehouse_id = $('select[name=\'warehouse_id\']').val();

  if (warehouse_id) {
    url += '&warehouse_id=' + encodeURIComponent(warehouse_id);
  }
  var refer_type_id = $('select[name=\'refer_type_id\']').val();

  if (refer_type_id) {
    url += '&refer_type_id=' + encodeURIComponent(refer_type_id);
  }

  var username = $('input[name=\'username\']').val();

  if (username) {
    url += '&username=' + encodeURIComponent(username);
  }
  var filter_product_code = $('input[name=\'filter_product_code\']').val();
  // alert(filter_product_code);
    if(filter_product_code){
      url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }
      var filter_product_name = $('input[name=\'filter_product_name\']').val();
  // alert(filter_product_name);
    if(filter_product_name){
      url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
    }
  location = url;
});

$('#button-vendor-bill').on('click', function(e) {
  var vendor_bill_month = $('#vendor_bill_month').val();
  if(!vendor_bill_month || isNaN(Date.parse(vendor_bill_month))) {
    alert('请输入有效年月');
    return;
  }
  window.location.href = "<?php echo URL('stock/money_out/genNewVendorBill') ?>"+'&token='+"<?php echo $token;?>"+'&year_month='+vendor_bill_month;
});

$('.year_month').datetimepicker({
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  pickTime: false
});

$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>