<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	
     <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
        	
            <!--右上操作按钮-->
			<div class="pull-right">
            	<button type="submit" form="form-inventory" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-check-circle"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
            <!--右上操作按钮-->
            
 			<h1><?php echo $heading_title; ?></h1>
            
 			<ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>       
        </div>
	</div>
    <!--顶部-->
    
    <!--内容-->
	<div class="container-fluid">
    	
        <!--警示消息区-->
    	<?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    	<?php } ?>
        <!--警示消息区-->
        
        <!--表单-->
		<div class="panel panel-default">
        	
            <!--标题-->
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
            <!--标题-->
            
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-inventory" class="form-horizontal">
         		<div class="tab-content">
                	<div class="tab-pane active" id="tab-general">
                    	
                        <!--仓库-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label"><?php echo $entry_warehouse; ?></label>
                            <div class="col-sm-5 view-col"><?php echo $warehouse_name; ?></div>
                 		</div>
                        <!--仓库-->
                        
                        <!--商品名称-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label"><?php echo $entry_product_name; ?></label>
                            <div class="col-sm-5 view-col"><?php echo $product_name; ?></div>
                 		</div>
                        <!--商品名称-->
                        
                         <!--选项-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label"><?php echo $entry_option_name; ?></label>
                            <div class="col-sm-5 view-col"><?php echo $option_name; ?></div>
                 		</div>
                        <!--选项-->
                        
                        <!--商品编码-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label"><?php echo $entry_product_code; ?></label>
                            <div class="col-sm-5 view-col"><?php echo $product_code; ?></div>
                 		</div>
                        <!--商品编码-->
                        
                        <!--条形码-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label"><?php echo $entry_sku; ?></label>
                            <div class="col-sm-5 view-col"><?php echo $product_sku; ?></div>
                 		</div>
                        <!--条形码-->
                        
                        <!--库位1-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label" for="input-position1"><?php echo $entry_position1; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="position1" value="<?php echo $position1; ?>" placeholder="<?php echo $entry_position1; ?>" id="input-position1" class="form-control" />
                  				<?php if ($error_position1) { ?>
              					<div class="text-danger"><?php echo $error_position1; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--库位1-->
                        
                        <!--库位2-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label" for="input-position2"><?php echo $entry_position2; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="position2" value="<?php echo $position2; ?>" placeholder="<?php echo $entry_position2; ?>" id="input-position2" class="form-control" />
                  				<?php if ($error_warehouse_position2) { ?>
              					<div class="text-danger"><?php echo $error_warehouse_position2; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--库位2-->
                        
                        <!--安全库存-->
                        <div class="form-group">
                 			<label class="col-sm-2 control-label" for="input-safe-quantity"><?php echo $entry_safe_quantity; ?></label>
                            <div class="col-sm-5">
                    			<input type="text" name="safe_quantity" value="<?php echo $safe_quantity; ?>" placeholder="<?php echo $entry_safe_quantity; ?>" id="input-safe-quantity" class="form-control" />
                  				<?php if ($error_safe_quantity) { ?>
              					<div class="text-danger"><?php echo $error_safe_quantity; ?></div>
                         		<?php } ?>
            				</div>
                 		</div>
                        <!--安全库存-->
                        
					</div>
 				</div>                  
            	</form>
			</div>
            
		</div>
        <!--表单-->
        
	</div>
    <!--内容-->
    
</div>

<?php echo $footer; ?>