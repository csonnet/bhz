<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
      <div class="pull-right">
              <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
<!--                 <button id="btn-enables" toaction="<?php echo $enable_action; ?>" type="button" data-toggle="tooltip" title="批量作废" class="btn btn-success"><i class="fa fa-play"></i></button>
            <button id="btn-disables" toaction="<?php echo $disable_action; ?>" type="button" data-toggle="tooltip" title="批量解除作废" class="btn btn-danger"><i class="fa fa-ban"></i></button> -->
      </div>
            <!--右上操作按钮-->
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
<div class="panel-body">
    <div class="well">
        <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start"  value="<?php echo $filter_date_start;?>" placeholder="搜索单据开始时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
              </div>
              </div>
            <div class="form-group">
                <label class="control-label" for="input-status">费用类型</label>
                <select name="filter_type" id="filter_type" class="form-control">
                  <?php  foreach ($cost_type as $key => $value) {?>
                  <option value="<?php echo $value['cost_type_id']; ?>" <?php if($filter_type==$value['cost_type_id']){echo 'selected="selected"'; } ?> ><?php echo $value['cost_name'] ;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索单据结束时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
              </div>
              </div>   
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <?php  foreach ($status_array as $key => $value) {?>
                  <option value="<?php echo $value['cost_status_id']; ?>" <?php if($filter_status==$value['cost_status_id']){echo 'selected="selected"'; } ?> ><?php echo $value['name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
              </div>
            </div>
            </div>
          </div>
          <div class="table-responsive">
          <table class="table table-bordered table-hover">
          <thead>
                <tr>
                  <td>费用ID</td>
                  <td>费用类型</td>
                  <td>申请人</td>
                  <td>申请人类型</td>
                  <td>费用金额</td>
                  <td>审批后费用金额</td>
                  <td>单据开始时间</td>
                  <td>单据结束时间</td>
                  <td>报销月份</td>
                  <td>状态</td>
                  <td>是否需要归还</td>
                  <td>归还时间</td>
                  <td>创建时间</td>
                  <td>操作</td>
                </tr>
              </thead><tbody>
                  <?php if ($cost_list) { ?>
                  <?php foreach ($cost_list as $cost) { ?>
                  <tr>
                    <td><?php echo $cost['cost_id'] ?></td>
                    <td><?php echo $cost['cost_name'] ?></td>
                    <td><?php echo $cost['fullname'] ?></td>
                    <td><?php echo $cost['cost_use_name']?></td>
                    <td><?php echo $cost['cost']?></td>
                    <td><?php echo $cost['cost_auditor']?></td>
                    <td><?php echo $cost['start_date'] ?></td>
                    <td><?php echo $cost['end_date'] ?></td>
                    <td><?php echo date('Y-m',$cost['days']) ?>
                    </td><td><?php echo $cost['status_name'] ?></td>
                    <?php if($cost['is_return']==1) {?>
                       <td>是</td>
                    <?php }elseif($cost['is_return']==0) { ?>
                       <td>否</td>
                    <?php } ?>
                   
                    <td><?php echo $cost['return_date'] ?></td>
                    <td><?php echo $cost['date_added'] ?></td>
                    <td>
                     <a href="<?php echo URL('stock/cost/view', 'token='.$token.'&cost_id='.$cost['cost_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <?php if($cost['cost_status_id']==1){?>
                    <a href="<?php echo URL('stock/cost/edit', 'token='.$token.'&cost_id='.$cost['cost_id']) ?>" data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if($cost['cost_status_id']==1){ ?>
                      <button value="<?php echo $cost['cost_id']; ?>" id="button-delete<?php echo $cost['cost_id']; ?>" type="button" data-toggle="tooltip" title="删除" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    <?php }?>
                    </td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>

<script type="text/javascript">


$('#button-filter').on('click', function() {
  url = 'index.php?route=stock/cost&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_type = $('select[name=\'filter_type\']').val();

  if (filter_type) {
    url += '&filter_type=' + encodeURIComponent(filter_type);
  }


  location = url;
});

 $('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/cost/delete&token=<?php echo $token; ?>&cost_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});


$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>