<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
      <div class="pull-right">
              <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
<!--                 <button id="btn-enables" toaction="<?php echo $enable_action; ?>" type="button" data-toggle="tooltip" title="批量作废" class="btn btn-success"><i class="fa fa-play"></i></button>
            <button id="btn-disables" toaction="<?php echo $disable_action; ?>" type="button" data-toggle="tooltip" title="批量解除作废" class="btn btn-danger"><i class="fa fa-ban"></i></button> -->
      </div>
            <!--右上操作按钮-->
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
      <div class="panel panel-default">
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
<div class="panel-body">
    <div class="well">
        <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_start">开始时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start"  value="<?php echo $filter_date_start;?>" placeholder="搜索单据开始时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
<?php
if (false === $warehouseLocked){
?>
              <div class="form-group">
                <label class="control-label" for="input-total">入库仓库</label>
                <select name="warehouse_id"  class="form-control">
                  <?php  foreach ($Warehouse_name as $key => $value) {?>
                  <option value="<?php echo $value['warehouse_id']; ?>" <?php if($warehouse_id==$value['warehouse_id']){echo 'selected="selected"'; } ?> ><?php echo $value['name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
<?php
}
?>
            <!--筛选商品编码或名称-->
<!--               <div class="form-group">
                  <label class="control-label" for="input-product-name">商品名称</label>
                  <input type="text" name="filter_product_name" value="<?php echo $filter_product_name; ?>" placeholder="<?php echo $entry_product_name; ?>" id="input-product-name" class="form-control" />
                  <input type="hidden" name="filter_product_code" id="input-product-code" value="<?php echo $filter_product_code; ?>" />
              </div> -->
                            <!--筛选商品编码或名称-->
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date_end">结束时间</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end;?>" placeholder="搜索单据结束时间(创建时间)" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div> 
              <div class="form-group">
                <label class="control-label" for="input-status">相关单据类型</label>
                <select name="refer_type_id"  class="form-control">
                  <?php  foreach ($refer_type as $key => $value) {?>
                  <option value="<?php echo $value['refer_type_id']; ?>" <?php if($refer_type_id==$value['refer_type_id']){echo 'selected="selected"'; } ?> ><?php echo $value['name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-product-name">入库单号</label>
                  <input type="text" name="in_id" value="<?php echo $in_id; ?>" placeholder="<?php echo $entry_product_name; ?>" id="input-product-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">状态</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <?php  foreach ($status_array as $key => $value) {?>
                  <option value="<?php echo $value['id']; ?>" <?php if($filter_status==$value['id']){echo 'selected="selected"'; } ?> ><?php echo $value['name'] ; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total">操作人</label>
                <input type="text" name="username" value="<?php echo $username;?>" placeholder="操作人" id="input-total" class="form-control">
              </div>
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>筛选</button>
                <a id="button-export-lists" data-toggle="tooltip" title="" class="btn btn-primary pull-right" style="margin-left:10px;" data-original-title="导出对账单"><i class="fa fa-calculator"></i> 导出对账单</a>
                <a id="button-export-stock" data-toggle="tooltip" title="" class="btn btn-primary pull-right" style="margin-left:10px;" data-original-title="导出入库汇总明细"><i class="fa fa-calculator"></i> 导出入库汇总明细</a>
              </div>
            </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
<!--                   <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>入库单号</td>
                  <td>相关单据</td>
                  <td>入库仓库</td>
                  <td>操作人</td>
                  <td>本单据采购总金额</td>
                  <td>单据状态</td>
                  <td>入库时间</td>
                  <td>创建时间</td>
                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($stocl_list) { ?>
                  <?php foreach ($stocl_list as $stock) { ?>
                  <tr style="color:<?php echo $stock['all_color'];?>">
<!--                     <td class="text-center">
                      <input type="checkbox" name="selected[]" value=""  />
                    </td> -->
                    <td><?php echo $stock['in_id'] ?></td>
                    <td><?php echo $stock['refer_type'] ?>(<?php echo $stock['refer_id'] ?>)</td>
                    <td><?php echo $stock['warehouse_name'] ?></td>
                    <td><?php echo $stock['username'] ?></td>
                    <td><?php echo $stock['buy_money'] ?></td>
                    <td><?php echo $stock['status_name']?></td>
                    <td><?php echo $stock['in_date'] ?></td>
                    <td><?php echo $stock['date_added'] ?></td>
                    <td>
                    <a href="<?php echo URL('stock/stock_in/view', 'token='.$token.'&stock_id='.$stock['in_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <?php if($stock['status']==1){?>
                    <a href="<?php echo URL('stock/stock_in/edit', 'token='.$token.'&stock_id='.$stock['in_id']).'&refer_type='.$stock['refer_type_id']  ?>" data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if($stock['status']==1){ ?>
                      <button value="<?php echo $stock['in_id']; ?>" id="button-cancel<?php echo $stock['in_id']; ?>" type="button" data-toggle="tooltip" title="作废" class="btn btn-danger"><i class="fa fa-ban"></i></button>

                    <?php }elseif($stock['status']==0){ ?>
                      <button value="<?php echo $stock['in_id']; ?>" id="button-rec<?php echo $stock['in_id']; ?>" data-toggle="tooltip" title="解除作废" class="btn btn-success"><i class="fa fa-play"></i></button>
                    <?php }?>
                    </td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有采购单数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>

<script type="text/javascript">
  $('button[id^=\'button-cancel\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/stock_in/cancel&token=<?php echo $token; ?>&in_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();
        window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('button[id^=\'button-rec\']').on('click', function(e) {
  if (confirm('确定吗？')) {
    var node = this;
    $.ajax({
      url: 'index.php?route=stock/stock_in/recover&token=<?php echo $token; ?>&in_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();
         window.location.reload();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});

$('#button-filter').on('click', function() {
  url = 'index.php?route=stock/stock_in&token=<?php echo $token; ?>';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
   var in_id = $('input[name=\'in_id\']').val();

  if (in_id) {
    url += '&in_id=' + encodeURIComponent(in_id);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var warehouse_id = $('select[name=\'warehouse_id\']').val();

  if (warehouse_id) {
    url += '&warehouse_id=' + encodeURIComponent(warehouse_id);
  }
  var refer_type_id = $('select[name=\'refer_type_id\']').val();

  if (refer_type_id) {
    url += '&refer_type_id=' + encodeURIComponent(refer_type_id);
  }

  var username = $('input[name=\'username\']').val();

  if (username) {
    url += '&username=' + encodeURIComponent(username);
  }
  var filter_product_code = $('input[name=\'filter_product_code\']').val();
  // alert(filter_product_code);
    if(filter_product_code){
      url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }
      var filter_product_name = $('input[name=\'filter_product_name\']').val();
  // alert(filter_product_name);
    if(filter_product_name){
      url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
    }
  location = url;
});



$('#button-export-stock').on('click', function(e) {
  url = 'index.php?route=stock/stock_in/exportStock&token=<?php echo $token; ?>';
  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var warehouse_id = $('select[name=\'warehouse_id\']').val();

  if (warehouse_id) {
    url += '&warehouse_id=' + encodeURIComponent(warehouse_id);
  }
  var refer_type_id = $('select[name=\'refer_type_id\']').val();

  if (refer_type_id) {
    url += '&refer_type_id=' + encodeURIComponent(refer_type_id);
  }

  var username = $('input[name=\'username\']').val();

  if (username) {
    url += '&username=' + encodeURIComponent(username);
  }
  var filter_product_code = $('input[name=\'filter_product_code\']').val();
  // alert(filter_product_code);
    if(filter_product_code){
      url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }
      var filter_product_name = $('input[name=\'filter_product_name\']').val();
  // alert(filter_product_name);
    if(filter_product_name){
      url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
    }
  location = url;
});

$('#button-export-lists').on('click', function(e) {
  url = 'index.php?route=stock/stock_in/exportLists&token=<?php echo $token; ?>';
  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }
  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var warehouse_id = $('select[name=\'warehouse_id\']').val();

  if (warehouse_id) {
    url += '&warehouse_id=' + encodeURIComponent(warehouse_id);
  }
  var refer_type_id = $('select[name=\'refer_type_id\']').val();

  if (refer_type_id) {
    url += '&refer_type_id=' + encodeURIComponent(refer_type_id);
  }

  var username = $('input[name=\'username\']').val();

  if (username) {
    url += '&username=' + encodeURIComponent(username);
  }
  var filter_product_code = $('input[name=\'filter_product_code\']').val();
  // alert(filter_product_code);
    if(filter_product_code){
      url += '&filter_product_code=' + encodeURIComponent(filter_product_code);
    }
      var filter_product_name = $('input[name=\'filter_product_name\']').val();
  // alert(filter_product_name);
    if(filter_product_name){
      url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
    }
  location = url;
});

<!--商品编码或名称autocomplete-->
  $('input[name=\'filter_product_name\']').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=stock/stocksearch/autocomplete&token=<?php echo $token; ?>&filter_product=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['product_code']+"    "+item['product_name'],
              value: item['product_code'],
              name: item['product_name']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $('input[name=\'filter_product_name\']').val(item['name']);
      $('input[name=\'filter_product_code\']').val(item['value']);
    }
  });
  <!--商品编码或名称autocomplete-->


$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>