'use strict';

angular.module('luckydrawAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.directive('datetimez', function() {
  return {
      restrict: 'A',
      require : 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
         format: 'yyyy-mm-dd hh:ii:ss',
         pickDate: true,
         pickTime: true,
         autoclose: true
        }).on('changeDate', function(e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
  };
})
.controller('luckydrawAdminCtrl', function($scope, $http, $filter,$window) {

  if(stock_list.in_id) {
      $scope.products = stock_list.product;
  }else{
    $scope.products = [];
  }
  if(stock_list.in_id) {
    $scope.stocks = [];
  }else{
    $scope.stocks = [];
  }
  $scope.err_msg = false;
  $scope.remove = function(index){
    $scope.products.splice(index, 1);
  }
  $scope.removeNumber = function(index){
    $scope.numbers.splice(index, 1);
  }
  $scope.removestock = function(index){
    $scope.stocks.splice(index, 1);
  }
  $scope.save_money = function(token){
    if(!$scope.price){
      alert('请输入应收款总额');
      invalid = true;
      return;
    }
    $http.post('index.php?route=stock/money_in/save&token='+token, {
      order_id:$scope.order_id,
      price:$scope.price,
      status:$scope.status,
       stocks:$scope.stocks,
       comment:$scope.comment,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        // $window.location.href = 'index.php?route=money/money_in'+'&token='+token;  
      }
    })
  }

  $scope.save_luckydraw = function(token){

    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_quantity<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/stock_in/save&token='+token, {
      refer_id:$scope.refer_id,
      refer_type:$scope.refer_type,
      warehouse:$scope.warehouse,
      status:$scope.status,
      products:$scope.products
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }

  $scope.save_edit = function(token){

    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_price<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }

    $http.post('index.php?route=stock/stock_in/savedit&token='+token, {
      in_id:stock_list.in_id,
      products:$scope.products
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }

  $scope.getProduct = function(value, token){
    return $http.get('index.php?route=stock/stock_in/products&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchProducts = data;
    });
  }


  $scope.selectProduct = function(p,product){
    product.product_code =  p.product_code;
    product.name = p.name ;
    product.sku = p.sku ;
    product.oname = p.oname ;
    product.product_price = p.product_price ;
  }
  $scope.save_stock = function(token,warehouse_id){

    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_price<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/stock_in/savestock&token='+token+'&warehouse_id='+warehouse_id, {
      in_id:stock_list.in_id,
      products:$scope.products
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }
});