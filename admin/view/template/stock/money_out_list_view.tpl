<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	
    <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
        	
            <!--右上操作按钮-->
			<div class="pull-right">
			         <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a>

			</div>
            <!--右上操作按钮-->
            
      		<h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
    		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
        		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        		<?php } ?>
      		</ul>
            <!--面包屑导航-->
            
		</div>
	</div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
    	<!--警示消息区-->
    	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<!--警示消息区-->
    	
        <!--列表-->
  		<div class="panel panel-default">
        	
            <!--标题-->
      		<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      		</div>
			<!--标题-->
            
            <!--列表内容-->
	       	<div class="panel-body">
	        	<dl class="dl-horizontal col-xs-6">
		          <dt>应付款项ID</dt>
		          <dd><?php echo $money_out['money_out_id']; ?></dd>
		          <dt>相关单据号</dt>
		          <dd><?php echo $money_out['refer_id']; ?></dd>
		          <dt>相关单据类型</dt>
		          <dd ><?php echo $money_out['name']; ?></dd>
		          <dt>相关单据金额</dt>
		          <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$money_out['refer_total']; ?></dd>
	          	  <dt>应付金额</dt>
		          <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$money_out['payables']; ?></dd>
		          <dt>实付金额</dt>
		          <dd style="color:red;font-weight:bold" ;=""><?php echo '¥'.$money_out['paid']; ?></dd>
	           </dl>

	        <dl class="dl-horizontal col-xs-6">
				<dt>收款人</dt>
				<dd><?php echo $money_out['payee']; ?></dd>
				<dt>状态</dt>
				<dd><?php echo $money_out['status_name']; ?></dd>
				<dt>支付时间</dt>
				<dd><?php echo $money_out['pay_time']; ?></dd>
				<dt>创建时间</dt>
				<dd><?php echo $money_out['date_added']; ?></dd>
				<dt>创建人</dt>
				<dd><?php echo $money_out['operator']; ?></dd>
	        </dl>
	      </div>
		 <table class="table">
	        <thead>
	          <tr>
	            <td>入库单号</td>
	            <td>金额</td>
	            <td>入库时间</td>
	          </tr>
	        </thead>
	        <tbody>
	     		<?php foreach ($data['stock_in'] as $key => $value) {?>
	            <tr>
	              <td><?php echo $value['refer_id'] ?></td>
	              <td><?php echo $value['buy_money'] ?></td>
	              <td><?php echo $value['in_date'] ?></td>
	            </tr>
	          <?php } ?>
	        </tbody>
	      </table>

            <!--列表内容-->
            
		</div>
            <!--历史操作-->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-comment-o"></i> 历史记录</h3>
		</div>
		<div class="panel-body">
			<div class="tab-content">
            
				<table class="table table-bordered bhz-table">
  					<thead>
   						<tr>
                			<th class="text-left">添加时间</th>
                			<th class="text-left">操作员</th>
                			<th class="text-left">操作内容</th>
              			</tr>
            		</thead>
            		<tbody>
              			<?php if ($histories) { ?>
              			<?php foreach ($histories as $history) { ?>
              			<tr>
                			<td class="text-left"><?php echo $history['date_added']; ?></td>
                			<td class="text-left"><?php echo $history['operator_name']; ?></td>
                			<td class="text-left"><?php echo $history['comment']; ?></td>
              			</tr>
              			<?php } ?>
              			<?php } else { ?>
              			<tr>
                			<td class="text-center" colspan="99">无记录</td>
              			</tr>
              			<?php } ?>
            		</tbody>
          		</table>
          
  			</div>
		</div>
	</div><!--列表-->
        
	</div>
	<!--内容-->
  
</div>

<script type="text/javascript">
</script>
<?php echo $footer; ?>