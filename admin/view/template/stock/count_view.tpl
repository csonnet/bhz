<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  
    <!--顶部-->
  <div class="page-header">
    <div class="container-fluid">
          
            <!--右上操作按钮-->
      <div class="pull-right">
         <!--      <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a> -->
<!--                 <button id="btn-enables" toaction="<?php echo $enable_action; ?>" type="button" data-toggle="tooltip" title="批量作废" class="btn btn-success"><i class="fa fa-play"></i></button>
            <button id="btn-disables" toaction="<?php echo $disable_action; ?>" type="button" data-toggle="tooltip" title="批量解除作废" class="btn btn-danger"><i class="fa fa-ban"></i></button> -->
      </div>
            <!--右上操作按钮-->
            
          <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
            
    </div>
  </div>
    <!--顶部-->
    
    <!--内容-->
    <div class="container-fluid">
    
      <!--警示消息区-->
      <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!--警示消息区-->
      
        <!--列表-->
<div class="panel panel-default">
          
            <!--标题-->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
          </div>
      <!--标题-->
            
            <!--列表内容-->
          <div class="panel-body">
            <dl class="dl-horizontal col-xs-6">
              <dt>开始时间</dt>
              <dd><?php echo $filter_date_start; ?></dd>
              <dt>结束时间</dt>
              <dd><?php echo $filter_date_end; ?></dd>
            
             </dl>

          <dl class="dl-horizontal col-xs-6">
          </dl>
        </div>
          
            
<div class="panel-body">
        <div class="row">
                            <!--筛选商品编码或名称-->
            
          <div class="table-responsive">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> 应收</h3>
          </div>

            <table class="table table-bordered table-hover">
              <thead>
                 <tr>
<!--                   <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>应收款项ID</td>
                  <td>相关单据类型</td>
                  <td>相关单据号</td>
                  <td>相关单据金额</td>
                  <td>应收款金额</td>
                  <td>实收款金额</td>
                  <td>付款人</td>
                  <td>状态</td>
                  <td>支付时间</td>
                  <td>创建时间</td>
                  <td>创建人</td>
                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($money_in) { ?>
                  <?php foreach ($money_in as $money) { ?>
                  <tr style="color:<?php echo $money['all_color'];?>">
                    <td><?php echo $money['money_in_id'] ?></td>
                    <td><?php echo $money['type_name'] ?></td>
                    <td><?php echo $money['refer_id'] ?></td>
                    <td><?php echo $money['refer_total'] ?></td>
                    <td><?php echo $money['receivable'] ?></td>
                    <td><?php echo $money['received']?></td>
                    <td><?php echo $money['payer_name']?></td>
                    <td><?php echo $money['status_name']?></td>
                    <td><?php echo $money['pay_time'] ?></td>
                    <td><?php echo $money['date_added'] ?></td>
                    <td><?php echo $money['operator'] ?></td>
                    <td>
                    <a href="<?php echo URL('stock/money_in/view', 'token='.$token.'&money_in_id='.$money['money_in_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo URL('stock/money_in/edit', 'token='.$token.'&money_in_id='.$money['money_in_id']) ?> " data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php if($money['status']==1){ ?>
                      <button value="<?php echo $money['money_in_id']; ?>" id="button-cancel<?php echo $money['money_in_id']; ?>" type="button" data-toggle="tooltip" title="无效" class="btn btn-danger"><i class="fa fa-ban"></i></button>
                      <?php }?>
                    <?php if($money['status']==3){ ?>
                      <button value="<?php echo $money['money_in_id']; ?>" id="button-rec<?php echo $money['money_in_id']; ?>" data-toggle="tooltip" title="解除无效" class="btn btn-success"><i class="fa fa-play"></i></button>
                    <?php }?>
                    </td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有应收/td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>


          <div class="table-responsive">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> 应付</h3>
          </div>

           <table class="table table-bordered table-hover">
              <thead>
                 <tr>
       <!--            <th width="5%" class="text-center">
                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                  </th> -->
                  <td>应付款项ID</td>
                  <td>相关单据类型</td>
                  <td>相关单据号</td>
                  <td>相关单据金额</td>
                  <td>应付金额</td>
                  <td>实付金额</td>
                  <td>收款人</td>
                  <td>状态</td>
                  <td>支付时间</td>
                  <td>创建时间</td>
                  <td>创建人</td>
                  <td>操作</td>
                </tr>
              </thead>
                <tbody>
                  <?php if ($money_out) { ?>
                  <?php foreach ($money_out as $money) { ?>
                  <tr style="color:<?php echo $money['all_color'];?>">
            <!--         <td class="text-center">
                      <input type="checkbox" name="selected[]" value="<?php echo $money['money_out_id'] ?>"  />
                    </td> -->
                    <td><?php echo $money['money_out_id'] ?></td>
                    <td><?php echo $money['name'] ?></td>
                    <td><?php echo $money['refer_id'] ?></td>
                    <td><?php echo $money['refer_total'] ?></td>
                    <td><?php echo $money['payables'] ?></td>
                    <td><?php echo $money['paid']?></td>
                    <td><?php echo $money['payee']?></td>
                    <td><?php echo $money['status_name']?></td>
                    <td><?php echo $money['pay_time'] ?></td>
                    <td><?php echo $money['date_added'] ?></td>
                    <td><?php echo $money['operator'] ?></td>
                    <td>
                    <a href="<?php echo URL('stock/money_out/view', 'token='.$token.'&money_out_id='.$money['money_out_id']) ?> " data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo URL('stock/money_out/edit', 'token='.$token.'&money_out_id='.$money['money_out_id']) ?> " data-toggle="tooltip" title="编辑" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php if($money['status']==1){ ?>
                      <button value="<?php echo $money['money_out_id']; ?>" id="button-cancel<?php echo $money['money_out_id']; ?>" type="button" data-toggle="tooltip" title="无效" class="btn btn-danger"><i class="fa fa-ban"></i></button>
                    <?php }?>
                    <?php if($money['status']==3){ ?>
                      <button value="<?php echo $money['money_out_id']; ?>" id="button-rec<?php echo $money['money_out_id']; ?>" data-toggle="tooltip" title="解除无效" class="btn btn-success"><i class="fa fa-play"></i></button>
                    <?php }?>
                    </td>
                    
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>

          </div>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
            <!--列表内容-->
            
    </div>
        <!--列表-->
        
  </div>
  <!--内容-->
  
</div>

<script type="text/javascript">
  


$('.date').datetimepicker({
  pickTime: false
});
</script>
<?php echo $footer; ?>