<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" ng-app="luckydrawAdmin" ng-controller="luckydrawAdminCtrl">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button ng-click="edit_out('<?php echo $token ?>','<?php echo $money_out['money_out_id'] ?>')" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-success">确认支付 </button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> 返回</a></div>
        <h1><?php echo $heading_title; ?></h1>
            
            <!--面包屑导航-->
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
            <!--面包屑导航-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="{{err_msg?'alert alert-danger':''}}" ng-show="err_msg"><i class="fa fa-exclamation-circle"></i>{{err_msg}}
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-luckydraw" class="form-horizontal">   
          <fieldset>
            <legend>基本信息</legend>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">相关单据类型</label>
              <div class="col-sm-10">
                <input type="text" name="money_typeout" ng-model="money_typeout"  placeholder="相关单据类型" id="money_typeout" class="form-control" readonly />
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">相关单据号</label>
              <div class="col-sm-10">
                <input type="text" name="refer_id" ng-model="refer_id"  placeholder="相关单据号" id="refer_id" class="form-control" readonly />
                <input type="hidden" name="payee_id" ng-model="payee_id"  id="payee_id" class="form-control" />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required ">
              <label class="col-sm-2 control-label">收款人</label>
              <div class="col-sm-10">
                <input type="text" name="payee" ng-model="payee"  placeholder="收款人" id="payee" class="form-control" readonly />
                <?php if ($error_refer_id) { ?>
                    <div class="text-danger"><?php echo $error_refer_id; ?></div>
                <?php } ?>
              </div>
            </div>
             <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">应付总额</label>
              <div class="col-sm-10">
                 <input type="text" name="payables" ng-model="payables"  placeholder="应付总额" id="input-luckydraw-name" class="form-control" readonly />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">实付总额</label>
              <div class="col-sm-10">
                 <input type="text" name="paid" ng-model="paid"  placeholder="实付总额" id="input-luckydraw-name" class="form-control" readonly />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-refer_type">增加实付总额</label>
              <div class="col-sm-10">
                 <input type="text" name="paidadd" ng-model="paidadd"  placeholder="实付总额" id="input-luckydraw-name" class="form-control"  />
                <?php if ($error_price) { ?>
                    <div class="text-danger"><?php echo $error_price; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-status">备注</label>
              <div class="col-sm-10">
                <textarea name="comment" ng-model="comment" class="form-control" id="comment" rows="8"></textarea>
              </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
  </div>

<script type="text/javascript">
  <?php if(!empty($money_out)) { ?>
    var money_out = <?php echo json_encode($money_out);?>;
    var stock_list=[];
    var money_in=[];
  <?php }  ?>
</script>  


<?php echo $footer; ?> 