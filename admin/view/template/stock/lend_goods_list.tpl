<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
    <div class="container-fluid">
    <div class="pull-right">
    <a href="<?php echo $add; ?>" data-toggle="tooltip" title="添加借货单" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
    </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
   </div>
   <div id="form-order"></div>

	<div class="container-fluid">

		<?php if ($error_warning) { ?>

	      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	        <button type="button" class="close" data-dismiss="alert">&times;</button>
	      </div>
	      <?php } ?>
	      <?php if ($success) { ?>
	      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	        <button type="button" class="close" data-dismiss="alert">&times;</button>
	      </div>

      <?php } ?>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-list"></i> 借货单列表</h3>
        </div>

        <div class="panel-body">

			<div class="well">
          	<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
		                <label class="control-label">借货单ID</label>
		                <input type="text" name="filter_lend_id" value="<?php echo $filter_lend_id; ?>" placeholder="借货单ID" id="input-lend-id" class="form-control" />
		            </div>

		            <div class="form-group">
		                <label class="control-label">单据状态</label>
		                <select name="filter_status" id="input-status" class="form-control">
		                  <option value="empty" <?php echo $filter_status_array['empty']?>></option>
		                  <?php foreach($status_arr as $val){?>
		                  <option value="<?php echo $val['id']?>" <?php echo $filter_status_array[$val['id']]?> ><?php echo $val['status_name']?></option>
		                  <?php }?>
		                </select>
		            </div>

		             <div class="form-group">
		                <label class="control-label">搜索单据创建开始时间</label>
		                <div class="input-group date">
		                  <input type="text" name="filter_add_date_start" value="<?php echo $filter_add_date_start;?>" placeholder="搜索单据创建开始时间" data-date-format="YYYY-MM-DD" id="input-add-date-start" class="form-control" />
		                  <span class="input-group-btn">
		                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
		                  </span>
		                </div>
		              </div>
				</div>

				<div class="col-sm-4">

					<div class="form-group">
		                <label class="control-label">仓库名</label>
		                <select name="filter_warehouse" id="input-warehouse" class="form-control">
		                  <option value="empty" <?php echo $filter_warehouse_array['empty']?>></option>
		                  <?php foreach($warehouse_arr as $val){?>
		                  <option value="<?php echo $val['warehouse_id']?>" <?php echo $filter_warehouse_array[$val['warehouse_id']]?> ><?php echo $val['name']?></option>
		                  <?php }?>
		                </select>
		            </div>

		            <div class="form-group">
		                <label class="control-label">商品编码</label>
		                <input type="text" name="filter_name_code" value="<?php echo $filter_name_code; ?>" placeholder="商品编码" id="input-total" class="form-control" />
		            </div>

		            <div class="form-group">
		                <label class="control-label">搜索单据创建结束时间</label>
		                <div class="input-group date">
		                  <input type="text" name="filter_add_date_end" value="<?php echo $filter_add_date_end;?>" placeholder="搜索订单创建结束时间" data-date-format="YYYY-MM-DD" id="input-add-date-end" class="form-control" />
		                  <span class="input-group-btn">
		                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
		                  </span></div>
		                  <div class="form-group">
							<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> 筛选</button>
						  </div>
		            </div>
				</div>
          	</div>

          	<div class="table-responsive">

          		<table class="table table-bordered table-hover">

					<thead>

						<tr style="background-color: white">

							<td class="text-left"><?php if ($sort == 'lo.loan_order_id') { ?>
							<a href="<?php echo $sort_loan_order_id?>" class="<?php echo strtolower($order); ?>">借单号</a>
							<?php } else { ?>
							<a href="<?php echo $sort_loan_order_id?>">借单号</a>
							<?php } ?></td>
							<td class="text-left">操作人</td>
							<td class="text-left">发货仓</td>
							<td class="text-left">物流中心</td>
							<td class="text-left">联系人</td>
							<td class="text-left">电话</td>
							<td class="text-left">地址</td>
							<td class="text-left"><?php if ($sort == 'lo.status') { ?>
							<a href="<?php echo $sort_status?>" class="<?php echo strtolower($order); ?>">状态</a>
							<?php } else { ?>
							<a href="<?php echo $sort_status?>">状态</a>
							<?php } ?></td>
							<td class="text-left">创建时间</td>
							<td class="text-left">修改时间</td>
							<td class="text-left">操作</td>

					</thead>

					<tbody>

						<?php if (!empty($loan_orders)) { ?>
						<?php foreach ($loan_orders as $loan_order) {
            ?>
						<tr style="background-color: white">
							<td class="text-left">
							<?php echo $loan_order['loan_order_id']?>
							</td>
							<td class="text-left">
							  <?php echo $loan_order['username']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['fwarehouse']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['logcenter_name']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['shipping_name']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['shipping_tel']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['shipping_address']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['status']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['date_added']?>
							</td>
							<td class="text-left">
							<?php echo $loan_order['status_id']?>

							</td>
							<td class="text-left">
								<?php if($loan_order['status']=='初始'){?>
								<button type="button" value="<?php echo $loan_order['loan_order_id']; ?>" id="button-verify<?php echo $loan_order['loan_order_id']; ?>"   data-toggle="tooltip" class="btn btn-danger">审核</button>
								<?php }?>
								<a href="<?php echo $loan_order['view']; ?>" data-toggle="tooltip" title="查看" class="btn btn-info"><i class="fa fa-eye"></i></a>
								<a href="<?php echo $loan_order['copy']; ?>" data-toggle="tooltip" title="复制" class="btn btn-primary"><i class="fa fa-list"></i></a>
                <input type="hidden" value="<?php echo $token;?>">
								<button type="button" value="<?php echo $loan_order['loan_order_id']; ?>"  data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="关闭" class="btn btn-danger del"><i class="fa fa-trash-o"></i></button>

							</td>
						<tr>

						<?php } ?>

						<?php } else { ?>
			                <tr>
			                  <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
			                </tr>
		                <?php } ?>

					</tbody>

          		</table>

          	</div>

        </div>

        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
	 </div>
   </div>
</div>
<script>
	$('.date').datetimepicker({
      pickDate: true
    });

    // Login to the API
    var token = '';

    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/login',
      type: 'post',
      data: 'key=<?php echo $api_key; ?>',
      dataType: 'json',
      crossDomain: true,
      success: function(json) {
            // $('.alert').remove();

            if (json['error']) {
            if (json['error']['key']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

                if (json['error']['ip']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
            }
            }

        if (json['token']) {
          token = json['token'];
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });

    $('#button-filter').on('click', function(){
      url = 'index.php?route=stock/lend&token=<?php echo $token?>';

      var filter_lend_id = $('input[name=\'filter_lend_id\']').val();

      if(filter_lend_id){
        url += '&filter_lend_id=' + encodeURIComponent(filter_lend_id);
      }

      var filter_status = $('select[name=\'filter_status\']').val();

      if(filter_status){
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

      var filter_add_date_start = $('input[name=\'filter_add_date_start\']').val();

      if(filter_add_date_start){
        url += '&filter_add_date_start=' + encodeURIComponent(filter_add_date_start);
      }

      var filter_warehouse = $('select[name=\'filter_warehouse\']').val();

      if(filter_warehouse){
        url += '&filter_warehouse=' + encodeURIComponent(filter_warehouse);
      }

      var filter_name_code = $('input[name=\'filter_name_code\']').val();

      if(filter_name_code){
        url += '&filter_name_code=' + encodeURIComponent(filter_name_code);
      }

      var filter_add_date_end = $('input[name=\'filter_add_date_end\']').val();

      if(filter_add_date_end){
        url += '&filter_add_date_end=' + encodeURIComponent(filter_add_date_end);
      }

      location = url;
    });


    $('.del').click(function(){
        if (confirm('确认删除吗？')) {
                obj=$(this);
                var id=$(this).parent().parent().children().eq(0).html();
                 var loanorderid=parseInt(id);
                  var toke=$(this).prev().val();
                   $.ajax({
                      url:'http://localhost/admin/index.php?route=stock/lend/delloan&token='+toke,
                      data:{"loanorderid":loanorderid},
                      type:"POST",
                      dataType:"JSON",
                      success:function(data){
                        if(data['success']){
                            obj.parent().parent().remove();
                        }else{
                          alert(data['error']);
                        }
                      }

                    })

           }
    })
// =======
//     $('button[id^=\'button-delete\']').on('click', function(e) {
//     	if (confirm('确认关闭？')) {
//     		var node = this;

//     		$.ajax({

//     			url:'<?php echo $store; ?>index.php?route=api/lend/close&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
//     			dataType: 'json',
//           		crossDomain: true,
//           		beforeSend: function() {
// 	              $(node).button('loading');
// 	            },
// 	            complete: function() {
// 	              $(node).button('reset');
// 	            },
// 	            success: function(json) {
// 	            	$('.alert').remove();

// 		            if (json['error']) {
// 		              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
// 		            }

// 		            if (json['success']) {
// 		              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
// 		            }
// 		            window.location.reload();
// 	            },

// 	            error: function(xhr, ajaxOptions, thrownError) {
// 		          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
// 		        }
//     		})


//     	}
//     });

    $('button[id^=\'button-verify\']').on('click', function(e) {
    if(confirm('确定吗？')){
        var node = this;
        $.ajax({
            url: '<?php echo $store; ?>index.php?route=api/lend/verify_v2&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
            dataType: 'json',
            crossDomain: true,
            beforeSend: function() {
                $(node).button('loading');
            },
            complete: function() {
                $(node).button('reset');
            },
            success: function(json) {
               if (2 == json['status']) {
                    alert(json['message']);
                    window.location.reload();
                }else if (1 == json['status']) {
                    alert(json['message']);
                    window.location.reload();
                }else if (0 == json['status']) {
                    if(confirm(json['message'])){
                        $.ajax({
                            url: '<?php echo $store; ?>index.php?route=api/lend/verify_v2&token=' + token + '&order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>&how=anyway',
                            dataType: 'json',
                            crossDomain: true,
                            success: function(json) {
                                alert(json['message']);
                                window.location.reload();
                            }
                        })
                    }else{
                        window.location.reload();
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        })
    }
});

</script>
