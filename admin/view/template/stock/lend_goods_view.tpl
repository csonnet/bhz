<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">

	    <a class="btn btn-default" href="<?php echo $return_url?>"><i class="fa fa-reply"></i> 返回</a>
	  </div>
      <h1>借货单详情</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      </div>
  </div>
  <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>

      <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> 借货单详情</h3>
        </div>
        <div class="panel-body">
          <dl class="dl-horizontal col-xs-5">
            <dt>借货单 ID</dt>
            <input type="hidden" id="ajaxurl" value="<?php echo $ajaxurl;?>">
            <dd id="borrow"><?php echo $loan_order_id?></dd>
            <dt>操作人</dt>
            <dd><?php echo $username?></dd>
            <dt>借货仓</dt>
            <dd><?php echo $fwarehouse?></dd>
            <dt>到货营销部</dt>
            <dd><?php echo $logcenter?></dd>
            <dt>地址、人名、电话</dt>
            <dd><?php echo $shipping_address.'、'.$shipping_name.'、'.$shipping_tel?></dd>
            <dt>备注</dt>
            <dd><?php echo $comment?></dd>
          </dl>
          <dl class="dl-horizontal col-xs-5">
            <dt>状态</dt>
            <dd><?php echo $status?></dd>
            <!-- <dt>审核人</dt>
            <dd><?php echo $approvel?></dd>
            <dt>已审核人</dt>
            <dd><?php echo $approveled?></dd> -->
          </dl>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <td class="text-left">
                商品编码
              </td>
              <td class="text-left">
                商品名称
              </td>
              <td class="text-left">
                条形码
              </td>
              <td class="text-left">
                归还仓库
              </td>
              <td class="text-left">
                欠货数量
              </td>
              <td class="text-left">
                归还数量(*点击对应商品归还数量处进行还货)
              </td>

            </td>
            </thead>
            <tbody>
              <?php if ($lend_details) { ?>
                <?php foreach ($lend_details as $lend_detail) { ?>
                <tr>
                  <td class="text-left">
                    <?php echo $lend_detail['product_code']?>
                  </td>
                  <td class="text-left">
                    <?php echo $lend_detail['product_name']?>
                  </td>

                  <td class="text-left">
                    <?php echo $lend_detail['product_sku']?>
                  </td>
                  <td class="text-left">
                      <select class="form-control" id="warehouse_id"  >
                        <?php foreach ($warehouse as $key => $value) { ?>
                          <option value="<?php echo $value['warehouse_id'] ?>">
                            <?php echo $value['name'] ?>
                           </option>
                        <?php } ?>
                      </select>
                  </td>
                  <td class="text-left">
                    <?php echo $lend_detail[return_qty_bad]?>
                  </td>

                  <td class="text-left">

                    <div class="dianji">
                       <span><?php echo $lend_detail['return_qty_bad']?></span>
                       <input type="number" class="xiugai form-control" style="display: none;">
                    </div>
                  </td>

                </tr>
                <?php }?>
              <?php }?>

            </tbody>
          </table>
        </div>
      </div>

      <?php if($allowverifymark){?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-comment-o"></i> 审批</h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td>审核备注</td>
                  <td>操作</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="form-control" id="approvel_comment" type="text" value="<?php echo $approvel_comment?>"/>
                  </td>
                  <td>

                    <button class="btn btn-info" id="button-verify<?php echo $loan_order_id?>" type="button" data-original-title="" data-toggle="tooltip" data-loading-text="加载中..." value="<?php echo $loan_order_id?>">审核</button>
                    <button class="btn btn-danger" id="button-rverify<?php echo $loan_order_id?>" type="button" data-original-title="" data-toggle="tooltip" data-loading-text="加载中..." value="<?php echo $loan_order_id?>">拒绝审核</button>

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <?php }?>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-comment-o"></i> 借货单历史</h3>
        </div>
        <div class="panel-body">
          <div class="tab-content">

            <table class="table table-bordered bhz-table">
                <thead>
                  <tr>
                          <th class="text-left">添加时间</th>
                          <th class="text-left">操作员</th>
                          <th class="text-left">操作前数据差</th>
                          <th class="text-left">操作后数据差</th>
                          <th class="text-left">操作内容</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($histories) { ?>
                        <?php foreach ($histories as $history) { ?>
                        <tr>
                          <td class="text-left"><?php echo $history['date_added']; ?></td>
                          <td class="text-left"><?php echo $history['username']; ?></td>
                          <td class="text-left"><?php echo $history['lsbs_name']; ?></td>
                          <td class="text-left"><?php echo $history['lsas_name']; ?></td>
                          <td class="text-left"><?php echo $history['comment']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td class="text-center" colspan="99">无记录</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>

            </div>
        </div>
      </div>

  </div>
  <script>

    // Login to the API
    var token = '';

    $.ajax({
      url: '<?php echo $store; ?>index.php?route=api/login',
      type: 'post',
      data: 'key=<?php echo $api_key; ?>',
      dataType: 'json',
      crossDomain: true,
      success: function(json) {
            // $('.alert').remove();

            if (json['error']) {
            if (json['error']['key']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

                if (json['error']['ip']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
            }
            }

        if (json['token']) {
          token = json['token'];
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });

    $('button[id^=\'button-verify\']').on('click', function(e) {

      if (confirm('确认审核？')) {

        var node = this;

        var comment = $("#approvel_comment").val();

        $.ajax({

          url:'<?php echo $store; ?>index.php?route=api/lend/verify&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          type: 'post',
          data: 'comment='+comment,
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {

            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              window.location.reload();
            }

          },

            error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }

        })

      }

    })

    $('button[id^=\'button-rverify\']').on('click', function(e) {

      if (confirm('确认拒绝审核？')) {

        var node = this;

        var comment = $("#approvel_comment").val();

        $.ajax({

          url:'<?php echo $store; ?>index.php?route=api/lend/rverify&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
          type: 'post',
          data: 'comment='+comment,
          dataType: 'json',
          crossDomain: true,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {

            $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

              window.location.reload();
            }

          },

          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }

        })

      }

    })

    $('button[id^=\'button-receive\']').on('click', function(e) {

      var node = this;

      var product_code = new Array();

      var receive_qty = new Array();

      $('input[name=\'product_code\']').each(function(key){

        product_code[key] = $(this).val();

      })

      $('input[name=\'receive_qty\']').each(function(key){

        receive_qty[key] = $(this).val();

      })

      $.ajax({

        url:'<?php echo $store; ?>index.php?route=api/lend/receive&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
        type: 'post',
        data: {"product_code" : product_code,"receive_qty" : receive_qty},
        dataType: 'json',
        crossDomain: true,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {

          $('.alert').remove();

            if (json['error']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            if (json['success']) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

              window.location.reload();
            }

        },

        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

      })

    })

    $('button[id^=\'button-back\']').on('click', function(e){

      var node = this;

      var product_code = new Array();

      var return_qty_good = new Array();

      var return_qty_bad = new Array();

      $('input[name=\'product_code\']').each(function(key){

        product_code[key] = $(this).val();

      })

      $('input[name=\'return_qty_good\']').each(function(key){

        return_qty_good[key] = $(this).val();

      })

      $('input[name=\'return_qty_bad\']').each(function(key){

        return_qty_bad[key] = $(this).val();

      })

      $.ajax({

        url:'<?php echo $store; ?>index.php?route=api/lend/back&token=' + token + '&loan_order_id=' + $(node).val() + '&user_id=<?php echo $user_id?>',
        type: 'post',
        data: {"product_code" : product_code,"return_qty_good" : return_qty_good,"return_qty_bad" : return_qty_bad},
        dataType: 'json',
        crossDomain: true,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {

          $('.alert').remove();

          if (json['error']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          if (json['success']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

            window.location.reload();
          }

        },

        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

      })

    })

  </script>
