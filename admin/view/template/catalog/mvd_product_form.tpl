<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body" id='panelBody'>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
			<!--mvds-->
			<li><a href="#tab-vendor" data-toggle="tab"><?php echo $tab_vendor; ?></a></li>
			<!--mvde-->
            <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
            <li><a href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
            <li><a href="#tab-special" data-toggle="tab"><?php echo $tab_special; ?></a></li>
            <li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
            <li><a href="#tab-reward" data-toggle="tab"><?php echo $tab_reward; ?></a></li>
            <li><a href="#tab-product-group" data-toggle="tab"><?php echo $tab_product_group; ?></a></li>
            <li><a href="#tab-area" data-toggle="tab">分配区域</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  
                  <!--商品分类-->
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_product_class; ?></label>
                    
                    <div class="col-sm-2">
           				<select name="product_class1" id="select-class1" class="form-control" onchange="changeClass(this.value,'<?php echo $product_class2; ?>');">
                     		<option code="" value=""><?php echo $text_select; ?></option>
                			<?php foreach ($class1 as $value) { ?>
                			<?php if ($value['category_id'] == $product_class1) { ?>
                			<option code = "<?php echo $value['code']; ?>" value="<?php echo $value['category_id']; ?>" selected="selected"><?php echo $value['name']; ?></option>
                        	<?php } else { ?>
                			<option code = "<?php echo $value['code']; ?>" value="<?php echo $value['category_id']; ?>"><?php echo $value['name']; ?></option>
                			<?php } ?>
                			<?php } ?>
                  		</select>
                    </div>
                    
                    <div class="col-sm-2">
           				<select name="product_class2" id="select-class2" class="form-control" onchange="changeClass2(this.value,'<?php echo $product_class3; ?>');"></select>
                    </div>
                    
                    <div class="col-sm-2">
           				<select name="product_class3" id="select-class3" class="form-control"></select>
                    </div>
                    
                    <?php if(!$product_id) { ?>
                    <div class="col-sm-2">
           				<button id="create-code" type="button" class="btn btn-primary" onclick="createCode()" >生成商品编码</button>
                    </div>
                    <?php } ?>
                    
                  </div>
                  <!--商品分类-->
                  
                  <!--商品编码-->
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-pro-code"><?php echo $entry_product_code; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_code" value="<?php echo $product_code; ?>" <?php if($product_id) { ?>readonly<?php } ?> id="input-pro-code" class="form-control" />
                      <?php if ($error_product_code != null) { ?>
                      <div class="text-danger"><?php echo $error_product_code; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <!--商品编码-->
                  
                  <!--是否单卖-->
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-pro-code"><?php echo $entry_is_single; ?></label>
                    <div class="col-sm-10">
                      <select name="is_single" id="input-is-single" class="form-control">                 
		        		<option value="0" <?php if(!(int)$is_single){?>selected="selected"<?php }?> >否</option>
		        		<option value="1" <?php if((int)$is_single){?>selected="selected"<?php }?> >是</option>
		      		  </select>
                    </div>
                  </div>
                  <!--是否单卖-->

                <!--是否针对部分客户显示-->
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-pro-code">是否针对部分客户显示</label>
                    <div class="col-sm-10">
                      <select name="is_part" id="input-is-single" class="form-control">                 
                <option value="0" <?php if(!(int)$is_part){?>selected="selected"<?php }?> >否</option>
                <option value="1" <?php if((int)$is_part){?>selected="selected"<?php }?> >是</option>
                </select>
                    </div>
                  </div>
                  <!--是否针对部分客户显示-->
                  
                  <!--商品标签-->
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-pro-code">商品标签</label>
                    <div class="col-sm-10" style="padding-top:9px;">
                      <input class="pro-check" name="newpro" type="checkbox" <?php if($newpro == 1){?>checked<?php }?> />设为新品
                      <input class="pro-check" name="recommend" type="checkbox" <?php if($recommend == 1){?>checked<?php }?> />推荐商品
                      <input class="pro-check" name="clearance" type="checkbox" <?php if($clearance == 1){?>checked<?php }?> />清仓商品
                      <input class="pro-check" name="promotion" type="checkbox" <?php if($promotion == 1){?>checked<?php }?> />促销商品
                    </div>
                  </div>
                  <!--商品标签-->
                <!--缺货状态-->
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-show-new">缺货状态</label>
                    <div class="col-sm-10">
                      <select name="lack_status" id="input-lack_status" class="form-control">                 
                <option value="1" <?php if((int)$lack_status==1){?>selected="selected"<?php }?> >正常</option>
                <option value="2" <?php if((int)$lack_status==2){?>selected="selected"<?php }?> >暂时缺货</option>
                <option value="3" <?php if((int)$lack_status==3){?>selected="selected"<?php }?> >永久缺货</option>
                </select>
                    </div>
                  </div>
                  <!--缺货状态-->

                  <!--缺货原因-->
                 <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>">缺货原因</label>
                    <div class="col-sm-10">
                      <textarea name="lack_reason" rows="5" placeholder="缺货原因" id="input-meta-description; ?>" class="form-control"><?php echo $lack_reason; ?></textarea>
                    </div>
                  </div>
                  <!--缺货原因-->

                  
                  <!--是否显示新图标-->
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-show-new">显示新品图标</label>
                    <div class="col-sm-10">
                      <select name="show_new" id="input-show-new" class="form-control">                 
		        		<option value="0" <?php if(!(int)$show_new){?>selected="selected"<?php }?> >强制不显示</option>
		        		<option value="1" <?php if((int)$show_new){?>selected="selected"<?php }?> >允许显示</option>
		      		  </select>
                    </div>
                  </div>
                  <!--是否显示新图标-->

                  <!--是否显示清仓图标-->
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-show-clearance">显示清仓图标</label>
                    <div class="col-sm-10">
                      <select name="show_clearance" id="input-show-clearance" class="form-control">                 
		        		<option value="0" <?php if(!(int)$show_clearance){?>selected="selected"<?php }?> >强制不显示</option>
		        		<option value="1" <?php if((int)$show_clearance){?>selected="selected"<?php }?> >允许显示</option>
		      		  </select>
                    </div>
                  </div>
                  <!--是否显示清仓图标-->
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tag<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="<?php echo $help_tag; ?>"><?php echo $entry_tag; ?></span></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>" placeholder="<?php echo $entry_tag; ?>" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="是否是赠品">赠品</span></label>
		    <div class="col-sm-10">
		      <select name="gift_status" id="input-gift_status" class="form-control">                 
		        <option value="0" <?php if(!(int)$gift_status){?>selected="selected"<?php }?> >否</option>
		        <option value="1" <?php if((int)$gift_status){?>selected="selected"<?php }?> >是</option>
		      </select>
		    </div>
		  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_type; ?></label>
                    <div class="col-sm-10">
                      <select name="product_type" id="input-product-type" class="form-control">
                        <?php
                        foreach ($productTypeList as $k=>$v){
                            echo '<option value="'.$k.'"';
                            if ($k == $product_type)
                                echo ' selected="selected"';
                            echo '>'.$v.'</option>';
                        ?>
                        <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                </div>
                <?php } ?>
              </div>
            </div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-price">市场参考价</label>
                <div class="col-sm-10">
                  <input type="text" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
                  <?php if ($error_price) { ?>
                  <div class="text-danger"><?php echo $error_price; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-product-cost"><?php echo $entry_vendor_product_cost; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="product_cost" value="<?php echo $product_cost; ?>" placeholder="<?php echo $entry_vendor_product_cost; ?>" onKeyUp="total_cost()" id="input-product-cost" class="form-control" />
                  <?php if ($error_product_cost) { ?>
                  <div class="text-danger"><?php echo $error_product_cost; ?></div>
                  <?php } ?>
                </div>
              </div> 
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-sale_price">商品售价</label>
                <div class="col-sm-10">
                  <input type="text" name="sale_price" value="<?php echo $sale_price; ?>" placeholder="<?php echo $entry_vendor_sale_price; ?>" onKeyUp="total_cost()" id="input-product-cost" class="form-control" />
                  <?php if ($error_sale_price) { ?>
                  <div class="text-danger"><?php echo $error_sale_price; ?></div>
                  <?php } ?>
                </div>
              </div> 
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-product-cost">商品毛利</label>
                <div class="col-sm-10">
                  <?php echo $ml; ?></label>
                  <?php if ($error_ml) { ?>
                  <div class="text-danger"><?php echo $error_ml; ?></div>
                  <?php } ?>
                </div>
              </div> 
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-safe_quantity">无锡安全库存</label>
                <div class="col-sm-10">
                  <input type="text" name="safe_quantity" value="<?php echo $safe_quantity; ?>" placeholder="<?php echo $entry_vendor_sale_price; ?>" onKeyUp="total_cost()" id="input-product-cost" class="form-control" />
                  <?php if ($error_safe_quantity) { ?>
                  <div class="text-danger"><?php echo $error_safe_quantity; ?></div>
                  <?php } ?>
                </div>
              </div> 
              <div class="form-group" style='color:#FF0000;' id='groupSpecialPrice'>
                <label class="col-sm-2 control-label" for="input-product-cost">参考零售价</label>
                <div class="col-sm-10" id="input-product-special-price"></div>
              </div>
              <!--规格-->
        	  <div class="form-group">
        		<label class="col-sm-2 control-label" for="input-price">规格</label>
                <div class="col-sm-10">
                  <input type="text" name="p_type" value="<?php echo $p_type; ?>" placeholder="规格" id="input-p-type" class="form-control" />
                </div>
              </div> 
              <!--规格-->
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sku"><span data-toggle="tooltip" title="<?php echo $help_sku; ?>"><?php echo $entry_sku; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="sku" value="<?php echo $sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-sku" class="form-control" />
                </div>
              </div> 
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-middle-package"><span data-toggle="tooltip" title="<?php echo $help_middle_package; ?>"><?php echo $entry_middle_package; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="middle_package" value="<?php echo $middle_package; ?>" placeholder="<?php echo $entry_middle_package; ?>" id="input-middle-package" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="<?php echo $help_minimum; ?>"><?php echo $entry_minimum; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="minimum" value="<?php echo $minimum; ?>" placeholder="<?php echo $entry_minimum; ?>" id="input-minimum" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-addnum"><span data-toggle="tooltip" title="最小增加量">增加量</span></label>
                <div class="col-sm-10">
                  <input type="text" name="addnum" value="<?php echo $addnum; ?>" placeholder="增加量" id="input-addnum" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-weight"><?php echo $entry_weight; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="weight" value="<?php echo $weight; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-weight" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $entry_weight_class; ?></label>
                <div class="col-sm-10">
                  <select name="weight_class_id" id="input-weight-class" class="form-control">
                    <?php foreach ($weight_classes as $weight_class) { ?>
                    <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                <div class="col-sm-10">
                  <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>            
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_model; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                  <?php if ($error_model) { ?>
                  <div class="text-danger"><?php echo $error_model; ?></div>
                  <?php } ?>
                </div>
              </div>
			  <!--mvds-->
			  <input type="hidden" name="product_name" size="100" value="<?php echo $product_name; ?>" />
			  <input type="hidden" name="pending_status" size="100" value="<?php echo $status; ?>" />
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
          <?php if ($status != 3) { ?>
            <?php if ($status==5) { ?>
            <option value="5" selected="selected"><?php echo $txt_pending_approval; ?></option>
            <?php if($user_group_id==1||$user_group_id==67){?>
            <option value="1"><?php echo $text_enabled; ?></option>
            <option value="0"><?php echo $text_disabled; ?></option>

            <?php } ?>
					  <?php }elseif($status) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
             <?php if($user_group_id==1||$user_group_id==67){?>
            <option value="0"><?php echo $text_disabled; ?></option>

            <?php } ?>
					  <?php } else { ?>
					  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
             <?php if($user_group_id==1||$user_group_id==67){?>
            <option value="1"><?php echo $text_enabled; ?></option>

            <?php } ?>
					  <?php } ?>
					<?php } else { ?>
            <option value="5" ><?php echo $txt_pending_approval; ?></option>
					  <option value="3" selected="selected">初始</option>
					<?php } ?>
                  </select>
                </div>
               </div>
			  <!--mvde-->

              
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ean"><span data-toggle="tooltip" title="<?php echo $help_ean; ?>"><?php echo $entry_ean; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="ean" value="<?php echo $ean; ?>" placeholder="<?php echo $entry_ean; ?>" id="input-ean" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ean"><span data-toggle="tooltip" title="箱入数">箱入数</span></label>
                <div class="col-sm-10">
                  <input type="text" name="packing_no" value="<?php echo $packing_no; ?>" placeholder="箱入数" id="input-packing_no" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-min_purchase_no"><span data-toggle="tooltip" title="最小进货数">最小进货数</span></label>
                <div class="col-sm-10">
                  <input type="text" name="min_purchase_no" value="<?php echo $min_purchase_no; ?>" placeholder="最小进货数" id="input-min_purchase_no" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-item_no"><span data-toggle="tooltip" title="货号">货号</span></label>
                <div class="col-sm-10">
                  <input type="text" name="item_no" value="<?php echo $item_no; ?>" placeholder="货号" id="input-item_no" class="form-control" />
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-upc"><span data-toggle="tooltip" title="是否参加优惠活动">参加优惠活动</span></label>
                <div class="col-sm-10">
                  <select name="upc" id="input-upc" class="form-control">
                    <?php $upc_values = array('0'=>'不参加', '1'=>'628', '2'=>'爆款秒杀')?>
                    <?php foreach($upc_values as $k=>$val){?>
                      <?php if($k==$upc) { ?>
                      <option value="<?php echo $k;?>" selected="selected"><?php echo $val; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $k;?>"><?php echo $val; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-jan"><span data-toggle="tooltip" title="第几号优惠活动产品">优惠活动编号</span></label>
                <div class="col-sm-10">
                  <input type="text" name="jan" value="<?php echo $jan; ?>" placeholder="优惠活动编号" id="input-jan" class="form-control" />
                </div>
              </div>
              <div class="form-group hidden">
                <label class="col-sm-2 control-label" for="input-isbn"><span data-toggle="tooltip" title="<?php echo $help_isbn; ?>"><?php echo $entry_isbn; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="isbn" value="<?php echo $isbn; ?>" placeholder="<?php echo $entry_isbn; ?>" id="input-isbn" class="form-control" />
                </div>
              </div>
              <div class="form-group hidden">
                <label class="col-sm-2 control-label" for="input-mpn"><span data-toggle="tooltip" title="<?php echo $help_mpn; ?>"><?php echo $entry_mpn; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="mpn" value="<?php echo $mpn; ?>" placeholder="<?php echo $entry_mpn; ?>" id="input-mpn" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-location"><?php echo $entry_location; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="location" value="<?php echo $location; ?>" placeholder="<?php echo $entry_location; ?>" id="input-location" class="form-control" />
                </div>
              </div>
              
              <div class="form-group hidden">
                <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                <div class="col-sm-10">
                  <select name="tax_class_id" id="input-tax-class" class="form-control">
                    <option value="0"><?php echo $text_none; ?></option>
                    <?php foreach ($tax_classes as $tax_class) { ?>
                    <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
             <!--  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
                </div>
              </div> -->
              
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-subtract"><?php echo $entry_subtract; ?></label>
                <div class="col-sm-10">
                  <select name="subtract" id="input-subtract" class="form-control">
                    <?php if ($subtract) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="<?php echo $help_stock_status; ?>"><?php echo $entry_stock_status; ?></span></label>
                <div class="col-sm-10">
                  <select name="stock_status_id" id="input-stock-status" class="form-control">
                    <?php foreach ($stock_statuses as $stock_status) { ?>
                    <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_shipping; ?></label>
                <div class="col-sm-10">
                  <label class="radio-inline">
                    <?php if ($shipping) { ?>
                    <input type="radio" name="shipping" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <?php } else { ?>
                    <input type="radio" name="shipping" value="1" />
                    <?php echo $text_yes; ?>
                    <?php } ?>
                  </label>
                  <label class="radio-inline">
                    <?php if (!$shipping) { ?>
                    <input type="radio" name="shipping" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                    <?php } else { ?>
                    <input type="radio" name="shipping" value="0" />
                    <?php echo $text_no; ?>
                    <?php } ?>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
				</div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date-available"><?php echo $entry_date_available; ?></label>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <input type="text" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_dimension; ?></label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-4">
                      <input type="text" name="length" value="<?php echo $length; ?>" placeholder="<?php echo $entry_length; ?>" id="input-length" class="form-control" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-length-class"><?php echo $entry_length_class; ?></label>
                <div class="col-sm-10">
                  <select name="length_class_id" id="input-length-class" class="form-control">
                    <?php foreach ($length_classes as $length_class) { ?>
                    <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
              </div>
            </div>
			<!--mvds-->
			<div class="tab-pane" id="tab-vendor">
			  <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-vendor-id"><?php echo $entry_vendor_name; ?></label>
                <div class="col-sm-10">
                  <select name="vendor" id="input-vendor-id" class="form-control" onchange="getVendors();">
				    <option value="0" selected="selected"><?php echo $text_none; ?></option>
                    <?php foreach ($vendors as $vd) { ?>
                    <?php if ($vd['vendor_id'] == $vendor) { ?>
					<option value="<?php echo $vd['vendor_id']; ?>" selected="selected"><?php echo $vd['vendor_name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $vd['vendor_id']; ?>"><?php echo $vd['vendor_name']; ?></option>
					<?php } ?>
					<?php } ?>
                  </select>
                  <?php if ($error_vendor) { ?>
                  <div class="text-danger"><?php echo $error_vendor; ?></div>
                  <?php } ?>
                </div>
              </div>
        <?php if(0) { ?>
			  <div class="form-group required">
			  <label class="col-sm-2 control-label" for="input-ori-country"><span data-toggle="tooltip" title="<?php echo $help_vendor_country_origin; ?>"><?php echo $entry_vendor_country_origin; ?></span></label>
                <div class="col-sm-10">
			      <select name="ori_country" id="input-ori-country" class="form-control">
					<option value="0" selected="selected"><?php echo $text_none; ?></option>
					<?php foreach ($countries as $country) { ?>
					<?php if ($country['country_id'] == $ori_country) { ?>
					<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select>
				</div>
			  </div>
        <?php } ?>
			  
        <?php if(0) { ?>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-shipping-method"><span data-toggle="tooltip" title="<?php echo $help_vendor_shipping_method; ?>"><?php echo $entry_vendor_shipping_method; ?></span></label>
				<div class="col-sm-10">
				  <select name="shipping_method" id="input-shipping-method" class="form-control">
					<option value="0" selected="selected"><?php echo $text_none; ?></option>
					<?php foreach ($couriers as $courier) { ?>
					<?php if ($courier['courier_id'] == $shipping_method) { ?>
						<option value="<?php echo $courier['courier_id']; ?>" selected="selected"><?php echo $courier['courier_name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-preferred-method"><span data-toggle="tooltip" title="<?php echo $help_vendor_preferred_shipping_method; ?>"><?php echo $entry_vendor_preferred_shipping_method; ?></span></label>
				<div class="col-sm-10">
				  <select name="prefered_shipping" id="input-ori-country" class="form-control">
					<option value="0" selected="selected"><?php echo $text_none; ?></option>
					<?php foreach ($couriers as $courier) { ?>
					<?php if ($courier['courier_id'] == $prefered_shipping) { ?>
						<option value="<?php echo $courier['courier_id']; ?>" selected="selected"><?php echo $courier['courier_name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
          <label class="col-sm-2 control-label" for="input-shipping-cost"><?php echo $entry_vendor_shipping_cost; ?></label>
          <div class="col-sm-10">
            <input type="text" name="shipping_cost" value="<?php echo $shipping_cost; ?>" placeholder="<?php echo $entry_vendor_product_cost; ?>" onKeyUp="total_cost()" id="input-shipping-cost" class="form-control" />
          </div>
        </div>

			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vtotal"><span data-toggle="tooltip" title="<?php echo $help_vendor_total; ?>"><?php echo $entry_vendor_total; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="vtotal" value="<?php echo $vtotal; ?>" placeholder="<?php echo $help_vendor_total; ?>" onKeyUp="procost()" id="input-vtotal" class="form-control" />
                <?php if (!empty($paypal_email)) { ?>
				        <a style="text-decoration:none" href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&no_shipping=1&business=<?php echo $paypal_email; ?>&item_name=<?php echo $product_description[$language['language_id']]['name']; ?>&amount=<?php echo round($vtotal,2); ?>&currency_code=<?php echo $config['config_currency']; ?>" target="_blank"><img src="../image/catalog/paypal_button.png" height="18" border="0" /></a>
				<?php } ?>
				        </div>
        </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vendor-product-url"><?php echo $entry_vendor_product_url; ?></label>
                <div class="col-sm-10">
				  <textarea name="product_url" rows="5" placeholder="<?php echo $entry_vendor_product_url; ?>" class="form-control"><?php echo $product_url; ?></textarea>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vendor-wholesale"><?php echo $entry_vendor_wholesale; ?></label>
                <div class="col-sm-10">
				  <textarea name="wholesale" rows="5" placeholder="<?php echo $entry_vendor_wholesale; ?>" class="form-control"><?php echo $wholesale; ?></textarea>
                </div>
              </div>
        <?php } ?>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_vendor_company; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_vendor_company; ?>" id="input-company" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vname"><?php echo $entry_vendor_contact_name; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="vname" value="<?php echo $vname; ?>" placeholder="<?php echo $entry_vendor_contact_name; ?>" id="input-vname" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_vendor_telephone; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_vendor_telephone; ?>" id="input-telephone" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_vendor_fax; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_vendor_fax; ?>" id="input-fax" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_vendor_email; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_vendor_email; ?>" id="input-email" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vendor-description"><?php echo $entry_vendor_description; ?></label>
                <div class="col-sm-10">
				  <textarea name="vendor_description" rows="5" id="input-vendor-description" placeholder="<?php echo $entry_vendor_description; ?>" class="form-control" disabled><?php echo $vendor_description; ?></textarea>
                </div>
              </div>
        <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vendor-country-zone"><?php echo $entry_vendor_country_zone; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="vendor_country_zone" value="<?php echo $vendor_country_zone; ?>" placeholder="<?php echo $entry_vendor_country_zone; ?>" id="input-vendor-country-zone" class="form-control" disabled />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-vendor-address"><?php echo $entry_vendor_address; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="vendor_address" value="<?php echo $vendor_address; ?>" placeholder="<?php echo $entry_vendor_address; ?>" id="input-vendor-address" class="form-control" disabled />
                </div>
          </div>
			   <div class="form-group">
                <label class="col-sm-2 control-label" for="input-store-url"><?php echo $entry_vendor_store_url; ?></label>
                <div class="col-sm-10">
				  <input type="text" name="store_url" value="<?php echo $store_url; ?>" placeholder="<?php echo $entry_vendor_store_url; ?>" id="input-store-url" class="form-control" disabled />
                </div>
          </div>
			</div>
			<!--mvde-->
            <div class="tab-pane" id="tab-links">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-manufacturer"><span data-toggle="tooltip" title="<?php echo $help_manufacturer; ?>"><?php echo $entry_manufacturer; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="manufacturer" value="<?php echo $manufacturer ?>" placeholder="<?php echo $entry_manufacturer; ?>" id="input-manufacturer" class="form-control" />
                  <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                  <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_categories as $product_category) { ?>
                    <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                      <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="product-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_filters as $product_filter) { ?>
                    <div id="product-filter<?php echo $product_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_filter['name']; ?>
                      <input type="hidden" name="product_filter[]" value="<?php echo $product_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-download"><span data-toggle="tooltip" title="<?php echo $help_download; ?>"><?php echo $entry_download; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="download" value="" placeholder="<?php echo $entry_download; ?>" id="input-download" class="form-control" />
                  <div id="product-download" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_downloads as $product_download) { ?>
                    <div id="product-download<?php echo $product_download['download_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_download['name']; ?>
                      <input type="hidden" name="product_download[]" value="<?php echo $product_download['download_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                  <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_relateds as $product_related) { ?>
                    <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                      <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-attribute">
              <div class="table-responsive">
                <table id="attribute" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_attribute; ?></td>
                      <td class="text-left"><?php echo $entry_text; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $attribute_row = 0; ?>
                    <?php foreach ($product_attributes as $product_attribute) { ?>
                    <tr id="attribute-row<?php echo $attribute_row; ?>">
                      <td class="text-left" style="width: 40%;"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" />
                        <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                      <td class="text-left"><?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea>
                        </div>
                        <?php } ?></td>
                      <td class="text-left"><button type="button" onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $attribute_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="<?php echo $button_attribute_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-option">
              <?php if ($error_product_option) { ?>
              <div class="text-danger error_product_option"><?php echo $error_product_option; ?></div>
              <?php } ?>
              <div class="row">
                <div class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="option">
                    <?php $option_row = 0; ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <li><a href="#tab-option<?php echo $option_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option<?php echo $option_row; ?>\']').parent().remove(); $('#tab-option<?php echo $option_row; ?>').remove(); $('#option a:first').tab('show');"></i> <?php echo $product_option['name']; ?></a></li>
                    <?php $option_row++; ?>
                    <?php } ?>
                    <li>
                      <input type="text" name="option" value="" placeholder="<?php echo $entry_option; ?>" id="input-option" class="form-control" />
                    </li>
                  </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <?php $option_row = 0; ?>
                    <?php $option_value_row = 0; ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <div class="tab-pane" id="tab-option<?php echo $option_row; ?>">
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-required<?php echo $option_row; ?>"><?php echo $entry_required; ?></label>
                        <div class="col-sm-10">
                          <select name="product_option[<?php echo $option_row; ?>][required]" id="input-required<?php echo $option_row; ?>" class="form-control">
                            <?php if ($product_option['required']) { ?>
                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <?php if ($product_option['type'] == 'text') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'textarea') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <textarea name="product_option[<?php echo $option_row; ?>][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control"><?php echo $product_option['value']; ?></textarea>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'file') { ?>
                      <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'date') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-3">
                          <div class="input-group date">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'time') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group time">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'datetime') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group datetime">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
                      <div class="table-responsive">
                        <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover option-table">
                          <thead>
                            <tr>
                              <td class="text-left"><?php echo $entry_option_value; ?></td>
                               <td width="13%" class="text-right"><?php echo $entry_product_code; ?></td>
                              <td class="text-right"><?php echo $entry_quantity; ?></td>
                              <td width="8%" class="text-left"><?php echo $entry_subtract; ?></td>
                              <td class="text-right"><?php echo $entry_price; ?></td>
                              <td class="text-right"><?php echo $entry_option_points; ?></td>
                              <td class="text-right"><?php echo $entry_weight; ?></td>
                              <td class="text-right">后台库存</td>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                            <tr id="option-value-row<?php echo $option_value_row; ?>">
                              <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                  <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                  <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                  <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                  <?php } else { ?>
                                  <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                  <?php } ?>
                                  <?php } ?>
                                  <?php } ?>
                                </select>
                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                              <!--商品选项编码-->
                              <td class="text-left"><input class="form-control option-code" readonly name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_code]" type="text" value="<?php echo $product_option_value['product_code']; ?>" /></td>
                              <!--商品选项编码-->
                              <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                              <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][subtract]" class="form-control">
                                  <?php if ($product_option_value['subtract']) { ?>
                                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                  <option value="0"><?php echo $text_no; ?></option>
                                  <?php } else { ?>
                                  <option value="1"><?php echo $text_yes; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                  <?php } ?>
                                </select></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]" class="form-control">
                                  <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>
                                  <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]" class="form-control">
                                  <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>
                                  <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]" class="form-control">
                                  <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>
                                  <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>
							  <td class="text-right">
                              	<!--<button type="button" onclick="$(this).tooltip('destroy');$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>-->
                                <?php
                                echo $product_option_value['availableQty'];
                                ?>
                              </td>
                            </tr>
                            <?php $option_value_row++; ?>
                            <?php } ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="7"></td>
                              <td class="text-left"><button type="button" onclick="addOptionValue('<?php echo $option_row; ?>');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                        <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                        <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                      <?php } ?>
                    </div>
                    <?php $option_row++; ?>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane hidden" id="tab-recurring">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_recurring; ?></td>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-left"></td>
                    </tr>
                  </thead>
                  <tbody>
					<?php $recurring_row = 0; ?>
                    <?php foreach ($product_recurrings as $product_recurring) { ?>
                    <tr id="recurring-row<?php echo $recurring_row; ?>">
                      <td class="text-left"><select name="product_recurrings[<?php echo $recurring_row; ?>][recurring_id]" class="form-control">
                          <?php foreach ($recurrings as $recurring) { ?>
                          <?php if ($recurring['recurring_id'] == $product_recurring['recurring_id']) { ?>
                          <option value="<?php echo $recurring['recurring_id']; ?>" selected="selected"><?php echo $recurring['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-left"><select name="product_recurrings[<?php echo $recurring_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_recurring['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-left"><button type="button" onclick="$('#recurring-row<?php echo $recurring_row; ?>').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
					<?php $recurring_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addRecurring()" data-toggle="tooltip" title="<?php echo $button_recurring_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            
            <div class="tab-pane hidden" id="tab-discount">
              <div class="table-responsive">
                <table id="discount" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_quantity; ?></td>
                      <td class="text-right"><?php echo $entry_priority; ?></td>
                      <td class="text-right"><?php echo $entry_price; ?></td>
                      <td class="text-left"><?php echo $entry_date_start; ?></td>
                      <td class="text-left"><?php echo $entry_date_end; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $discount_row = 0; ?>
                    <?php foreach ($product_discounts as $product_discount) { ?>
                    <tr id="discount-row<?php echo $discount_row; ?>">
                      <td class="text-left"><select name="product_discount[<?php echo $discount_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][price]" value="<?php echo $product_discount['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left"><button type="button" onclick="$('#discount-row<?php echo $discount_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $discount_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="6"></td>
                      <td class="text-left"><button type="button" onclick="addDiscount();" data-toggle="tooltip" title="<?php echo $button_discount_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>

            <div class="tab-pane" id="tab-special">
              <div class="table-responsive">
                <?php if ($error_product_special) { ?>
                <div class="text-danger error_product_special"><?php echo $error_product_special; ?></div>
                <?php } ?>
                <table id="special" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">特价编号</td>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_priority; ?></td>
                      <td class="text-right"><?php echo $entry_price; ?></td>
                      <td class="text-left"><?php echo $entry_date_start; ?></td>
                      <td class="text-left"><?php echo $entry_date_end; ?></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td class="text-left">调试售价</td>
                      <td class="text-left"><?php echo $sale_price; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $special_row = 0; ?>
                    <?php foreach ($product_specials as $product_special) { ?>
                    <tr id="special-row<?php echo $special_row; ?>">
                      <td class="text-left">
                        <input type="text" name="product_special[<?php echo $special_row; ?>][product_special_id]" value="<?php echo $product_special['product_special_id']; ?>" placeholder="<?php echo $entry_quantity; ?>"   readonly="readonly" class="form-control" />
                      </td>
                      <td class="text-left"><select name="product_special[<?php echo $special_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left"><button type="button" onclick="$('#special-row<?php echo $special_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $special_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="6"></td>
                      <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_special_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_image; ?></td>
                      <td class="text-right"><?php echo $entry_sort_order; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($product_images as $product_image) { ?>
                    <tr id="image-row<?php echo $image_row; ?>">
                      <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                      <td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                      <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $image_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-reward">
              <div class="form-group">
                <label class="col-lg-2 control-label" for="input-points"><span data-toggle="tooltip" title="<?php echo $help_points; ?>"><?php echo $entry_points; ?></span></label>
                <div class="col-lg-10">
                  <input type="text" name="points" value="<?php echo $points; ?>" placeholder="<?php echo $entry_points; ?>" id="input-points" class="form-control" />
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_reward; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($customer_groups as $customer_group) { ?>
                    <tr>
                      <td class="text-left"><?php echo $customer_group['name']; ?></td>
                      <td class="text-right"><input type="text" name="product_reward[<?php echo $customer_group['customer_group_id']; ?>][points]" value="<?php echo isset($product_reward[$customer_group['customer_group_id']]) ? $product_reward[$customer_group['customer_group_id']]['points'] : ''; ?>" class="form-control" /></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane hidden" id="tab-design">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_store; ?></td>
                      <td class="text-left"><?php echo $entry_layout; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-left"><?php echo $text_default; ?></td>
                      <td class="text-left"><select name="product_layout[0]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($product_layout[0]) && $product_layout[0] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php foreach ($stores as $store) { ?>
                    <tr>
                      <td class="text-left"><?php echo $store['name']; ?></td>
                      <td class="text-left"><select name="product_layout[<?php echo $store['store_id']; ?>]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($product_layout[$store['store_id']]) && $product_layout[$store['store_id']] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-product-group"><!-- @auther sonicsjh-->
                <table id="productGroup" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <td class="text-left" style='width:40%;'>已选中商品</td>
                    <td class="text-center" style='width:20%;'>条件筛选</td>
                    <td class="text-left" style='width:40%;'>商品列表</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-left" style='vertical-align:top;'>
                        <div style='display:none;'>
                        <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <td width='100' class="text-center">数据汇总<br />（点击查看）<br />（<span style='color:#FF0000;'>X</span> 删除）</td>
                            <td id='groupChildTotal'>
                            <?php
                            foreach ($product_group as $childProduct){
                            ?>
                                <div id="selectProductDiv_<?php echo $childProduct['sku']; ?>" style="padding:3px 5px;margin:3px;border:1px solid #0000FF;float:left;"><a href="javascript:void(0);" onclick="gotoGroupChild('<?php echo $childProduct['sku']; ?>');" ><?php echo $childProduct['name']; ?><br /><?php echo $childProduct['code']; ?></a> <a href="javascript:void(0);" onclick="removeFromGroup('<?php echo $childProduct['sku']; ?>')"><span style="color:#FF0000;float:right;">X</span></a></div>
                            <?php
                            }
                            ?>
                            </td>
                        </thead>
                        </table>
                        </div>
                        <div style='height:615px;overflow-y:scroll;' id="productGroupSelectedDiv">
                        <table id="productGroupSelected" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-center" width='100'>图片</td>
                            <td>商品信息</td>
                            <td class="text-center" width='100'>状态</td>
                            <td class="text-center" width='100'>数量</td>
                            <td class="text-center" width='100'>单价</td>
                            <td class="text-center" width='100'>操作</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($product_group as $childProduct){
                            $statusStr = '<span style="color:#FF0000;">'.$text_disabled.'</span>';
                            if (1 == $childProduct['status']) {
                                $statusStr = '<span style="color:#0000FF;">'.$text_enabled.'</span>';
                            }else if  (5 == $childProduct['status']) {
                                $statusStr = '<span style="color:#FF0000;">'.$txt_pending_approval.'</span>';
                            }
                        ?>
                        <tr id="selectProduct_<?php echo $childProduct['sku']; ?>">
                            <td class="text-center"><img src="<?php echo $childProduct['image']; ?>" style="width:40px;height:40px;" /></td>
                            <td><?php echo $childProduct['name']; ?><br /><?php echo $childProduct['code']; ?></td>
                            <td class="text-center"><?php echo $statusStr; ?></td>
                            <td class="text-center">
                                <input type="text" name="groupChild[<?php echo $childProduct['key']; ?>]" id="groupChildQty_<?php echo $childProduct['sku']; ?>" value="<?php echo $childProduct['quantity']; ?>" class="form-control" maxlength="3" style="text-align:right;" />
                                <input type="hidden" id="selectProductPrice_<?php echo $childProduct['sku']; ?>" value="<?php echo $childProduct['product_price']; ?>" />
                                <input type="hidden" id="selectProductCost_<?php echo $childProduct['sku']; ?>" value="<?php echo $childProduct['product_cost']; ?>" />
                                <input type="hidden" id="selectProductSpecialPrice_<?php echo $childProduct['sku']; ?>" value="<?php echo $childProduct['product_special_price']; ?>" />
                                </td>
                            <td class="text-center"> <input type="text" name="groupChildPrice[<?php echo $childProduct['key']; ?>]" id="groupChildPriceQty_<?php echo $childProduct['sku']; ?>" value="<?php echo $childProduct['child_price']; ?>"  class="form-control" maxlength="3" style="text-align:right;" />
                            </td>
                            <td class="text-center"><a href="javascript:void(0);" onclick="removeFromGroup('<?php echo $childProduct['sku']; ?>');">移除</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        </table>
                        </div>
                    </td>
                    <td class="text-center" style='vertical-align:top;'>
                        <table id="productGroupFilter" class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td width='100'><?php echo $entry_product_code; ?></td>
                            <td><input type='text' class='form-control' id='GS_code'></td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_name; ?></td>
                            <td><input type='text' class='form-control' id='GS_name'></td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_sku; ?></td>
                            <td><input type='text' class='form-control' id='GS_sku'></td>
                        </tr>
                        <tr style='display:none;'>
                            <td><?php echo $entry_status; ?></td>
                            <td class='text-left'>
                            <select class="form-control" style='color:#0000FF;' id='GS_status' onchange='statusChangeInGS(this.value)'>
                                <option value="*" style='color:#0000FF;'></option>
                                <option value="1" style='color:#0000FF;' selected='selected'><?php echo $text_enabled; ?></option>
                                <option value="0" style='color:#FF0000;'><?php echo $text_disabled; ?></option>
                                <option value="5" style='color:#FF0000;'><?php echo $txt_pending_approval; ?></option>
                            </select>
                            </td>
                        </tr>
                        </tbody>
                        <tfooter>
                        <tr>
                            <td class='text-center' colspan='2'><button type='button' class='btn btn-primary' onclick='GS(1)'><span class='fa fa-search'>筛选</span></button></td>
                        </tr>
                        <tr>
                            <td class='text-center' colspan='2'>前台功能完成以前<br />系统自动保存所有<strong style='color:#FF0000;'>销售组合</strong>类商品的状态为<strong style='color:#FF0000;'>待审批</strong></td>
                        </tr>
                        </tfooter>
                        </table>
                    </td>
                    <td class="text-left" style='vertical-align:top;'>
                        <div style='height:615px;overflow-y:scroll;'>
                        <table id="productGroupUnSelected" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-center" width='100'>图片</td>
                            <td>商品信息</td>
                            <td class="text-center" width='100'>状态</td>
                            <td class="text-center" width='100'>数量</td>
                            <td class="text-center" width='100'>操作</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                        </div>
                    </td>
                </tr>
                </tbody>
                </table>
            </div>
           
         	<!--分配区域-->
            <div class="tab-pane" id="tab-area">
            	
                <div class="form-group lyc-group">
            		<label class="col-sm-2 control-label" for="input-category">只分配到这些区域</label>
             		<div class="col-sm-5">
                    	<p>省市</p>
              			<input type="text" name="only_country" value="" placeholder="省市" id="input-only-country" class="form-control" />
                  		<div id="only-country" class="well well-sm chose-area" style="height: 150px; overflow: auto;">
                        	
                            <?php foreach ($only_countries as $oc) { ?>
                    		<div id="only-country<?php echo $oc['country_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $oc['name']; ?>
                      			<input type="hidden" name="only_countries[]" value="<?php echo $oc['country_id']; ?>" />
                    		</div>
                    		<?php } ?>
                            
            			</div>
            		</div>
                    <div class="col-sm-5">
                    	<p>市区</p>
              			<input type="text" name="only_zone" value="" placeholder="市区" id="input-only-zone" class="form-control" />
                  		<div id="only-zone" class="well well-sm chose-area" style="height: 150px; overflow: auto;">
                        	
                            <?php foreach ($only_zones as $oz) { ?>
                    		<div id="only-zone<?php echo $oz['zone_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $oz['name']; ?>
                      			<input type="hidden" name="only_zones[]" value="<?php echo $oz['zone_id']; ?>" />
                    		</div>
                    		<?php } ?>
                            
            			</div>
            		</div>
        		</div>
            	
                <div class="form-group">
            		<label class="col-sm-2 control-label" for="input-category">不分配到这些区域</label>
             		<div class="col-sm-5">
                    	<p>省市</p>
              			<input type="text" name="no_country" value="" placeholder="省市" id="input-no-country" class="form-control" />
                  		<div id="no-country" class="well well-sm chose-area" style="height: 150px; overflow: auto;">
                        	
                            <?php foreach ($no_countries as $nc) { ?>
                    		<div id="no-country<?php echo $nc['country_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $nc['name']; ?>
                      			<input type="hidden" name="no_countries[]" value="<?php echo $nc['country_id']; ?>" />
                    		</div>
                    		<?php } ?>
                            
            			</div>
            		</div>
                    <div class="col-sm-5">
                    	<p>市区</p>
              			<input type="text" name="no_zone" value="" placeholder="市区" id="input-no-zone" class="form-control" />
                  		<div id="no-zone" class="well well-sm chose-area" style="height: 150px; overflow: auto;">
                        	
                            <?php foreach ($no_zones as $nz) { ?>
                    		<div id="no-zone<?php echo $nz['zone_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $nz['name']; ?>
                      			<input type="hidden" name="no_zones[]" value="<?php echo $nz['zone_id']; ?>" />
                    		</div>
                    		<?php } ?>
                            
            			</div>
            		</div>
        		</div>
                
            </div>
            <!--分配区域-->
            
          </div>
        </form>
      </div>
       <!--历史记录-->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-comment-o"></i> 商品变动历史</h3>
          </div>
          <div class="panel-body">
            <div class="tab-content">
                  
              <table class="table table-bordered bhz-table">
                  <thead>
                    <tr>
                            <th class="text-left">添加时间</th>
                            <th class="text-left">操作员</th>
                            <th class="text-left">操作内容</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php if ($histories) { ?>
                          <?php foreach ($histories as $history) { ?>
                          <tr>
                            <td class="text-left"><?php echo $history['date_added']; ?></td>
                            <td class="text-left"><?php echo $history['operator_name']; ?></td>
                            <td class="text-left"><?php echo $history['comment']; ?></td>
                          </tr>
                          <?php } ?>
                          <?php } else { ?>
                          <tr>
                            <td class="text-center" colspan="99">无记录</td>
                          </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                
              </div>
          </div>
        </div>
       <!--历史记录-->

    </div>
  </div>
<script type="text/javascript"><!--
	<?php foreach ($languages as $language) { ?>
        var temp = "#input-description"+<?php echo $language['language_id']; ?>; 
        $(temp).summernote({
            height: 300
        });
    <?php } ?>
//--></script> 
  <script type="text/javascript"><!--
// Manufacturer
$('input[name=\'manufacturer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				json.unshift({
					manufacturer_id: 0,
					name: '<?php echo $text_none; ?>'
				});
				
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['manufacturer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'manufacturer\']').val(item['label']);
		$('input[name=\'manufacturer_id\']').val(item['value']);
	}	
});

// Category
$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');
		
		$('#product-category' + item['value']).remove();
		
		$('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');	
	}
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Filter
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');
		
		$('#product-filter' + item['value']).remove();
		
		$('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');	
	}	
});

$('#product-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Downloads
$('input[name=\'download\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['download_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'download\']').val('');
		
		$('#product-download' + item['value']).remove();
		
		$('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');	
	}	
});

$('#product-download').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Related
$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');
		
		$('#product-related' + item['value']).remove();
		
		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');	
	}	
});

$('#product-related').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script> 
  <script type="text/javascript"><!--
var attribute_row = <?php echo $attribute_row; ?>;

function addAttribute() {
    html  = '<tr id="attribute-row' + attribute_row + '">';
	html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" placeholder="<?php echo $entry_attribute; ?>" class="form-control" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
	html += '  <td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><textarea name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"></textarea></div>';
    <?php } ?>
	html += '  </td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';
	
	$('#attribute tbody').append(html);
	
	attributeautocomplete(attribute_row);
	
	attribute_row++;
}

function attributeautocomplete(attribute_row) {
	$('input[name=\'product_attribute[' + attribute_row + '][name]\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/attribute/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							category: item.attribute_group,
							label: item.name,
							value: item.attribute_id
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'product_attribute[' + attribute_row + '][name]\']').val(item['label']);
			$('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').val(item['value']);
		}
	});
}

$('#attribute tbody tr').each(function(index, element) {
	attributeautocomplete(index);
});
//--></script> 
  <script type="text/javascript"><!--	
var option_row = <?php echo $option_row; ?>;

$('input[name=\'option\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						category: item['category'],
						label: item['name'],
						value: item['option_id'],
						type: item['type'],
						option_value: item['option_value']
					}
				}));
			}
		});
	},
	'select': function(item) {
		html  = '<div class="tab-pane" id="tab-option' + option_row + '">';
		html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';
		
		html += '	<div class="form-group">';
		html += '	  <label class="col-sm-2 control-label" for="input-required' + option_row + '"><?php echo $entry_required; ?></label>';
		html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
		html += '	      <option value="1"><?php echo $text_yes; ?></option>';
		html += '	      <option value="0"><?php echo $text_no; ?></option>';
		html += '	  </select></div>';
		html += '	</div>';
		
		if (item['type'] == 'text') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
			html += '	</div>';
		}
		
		if (item['type'] == 'textarea') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control"></textarea></div>';
			html += '	</div>';			
		}
		 
		if (item['type'] == 'file') {
			html += '	<div class="form-group" style="display: none;">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
			html += '	</div>';
		}
						
		if (item['type'] == 'date') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}
		
		if (item['type'] == 'time') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}
				
		if (item['type'] == 'datetime') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}
			
		if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image') {
			html += '<div class="table-responsive">';
			html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover option-table">';
			html += '  	 <thead>'; 
			html += '      <tr>';
			html += '        <td class="text-left"><?php echo $entry_option_value; ?></td>';
			html += '        <td class="text-left"><?php echo $entry_product_code; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_quantity; ?></td>';
			html += '        <td class="text-left"><?php echo $entry_subtract; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_price; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_option_points; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_weight; ?></td>';
			html += '        <td></td>';
			html += '      </tr>';
			html += '  	 </thead>';
			html += '  	 <tbody>';
			html += '    </tbody>';
			html += '    <tfoot>';
			html += '      <tr>';
			html += '        <td colspan="7"></td>';
			html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
			html += '      </tr>';
			html += '    </tfoot>';
			html += '  </table>';
			html += '</div>';
			
            html += '  <select id="option-values' + option_row + '" style="display: none;">';
			
            for (i = 0; i < item['option_value'].length; i++) {
				html += '  <option value="' + item['option_value'][i]['option_value_id'] + '">' + item['option_value'][i]['name'] + '</option>';
            }

            html += '  </select>';	
			html += '</div>';	
		}
		
		$('#tab-option .tab-content').append(html);
			
		$('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\')"></i> ' + item['label'] + '</li>');
		
		$('#option a[href=\'#tab-option' + option_row + '\']').tab('show');
		
		$('.date').datetimepicker({
			pickTime: false
		});
		
		$('.time').datetimepicker({
			pickDate: false
		});
		
		$('.datetime').datetimepicker({
			pickDate: true,
			pickTime: true
		});
				
		option_row++;
	}	
});
//--></script> 
  <script type="text/javascript"><!--		
var option_value_row = <?php echo $option_value_row; ?>;

function addOptionValue(option_row) {
	var no = $(".option-table").find('tbody tr').length;
	var option_code = $("#input-pro-code").val().toString()+no; 	
	html  = '<tr id="option-value-row' + option_value_row + '">';
	html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
	html += $('#option-values' + option_row).html();
	html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
	html+='<td class="text-left"><input readonly type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_code]" value="'+option_code+'" placeholder="<?php echo $entry_product_code; ?>" class="form-control option-code" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>'; 
	html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]" class="form-control">';
	html += '    <option value="1"><?php echo $text_yes; ?></option>';
	html += '    <option value="0"><?php echo $text_no; ?></option>';
	html += '  </select></td>';
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>';	
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#option-value' + option_row + ' tbody').append(html);
	$('[rel=tooltip]').tooltip();
	option_value_row++;
}
//--></script> 
<?php if(0) { ?>
  <script type="text/javascript"><!--
var discount_row = <?php echo $discount_row; ?>;

function addDiscount() {
	html  = '<tr id="discount-row' + discount_row + '">'; 
    html += '  <td class="text-left"><select name="product_discount[' + discount_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
    <?php } ?>
    html += '  </select></td>';		
    html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';
    html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
    html += '  <td class="text-left"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#discount-row' + discount_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';	
	
	$('#discount tbody').append(html);

	$('.date').datetimepicker({
		pickTime: false
	});
	
	discount_row++;
}
//--></script> 
<?php } ?>
<script type="text/javascript"><!--
var special_row = <?php echo $special_row; ?>;

function addSpecial() {
	html  = '<tr id="special-row' + special_row + '">'; 
    html += '  <td class="text-right"></td>';
    html += '  <td class="text-left"><select name="product_special[' + special_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
    <?php } ?>
    html += '  </select></td>';		
    html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
    html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#special tbody').append(html);

	$('.date').datetimepicker({
		pickTime: false
	});
		
	special_row++;
}
//--></script> 
  <script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
	html  = '<tr id="image-row' + image_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#images tbody').append(html);
	
	image_row++;
}
//--></script> 
<script type="text/javascript"><!--
var recurring_row = <?php echo $recurring_row; ?>;

function addRecurring() {
	recurring_row++;
	
	html  = '';
	html += '<tr id="recurring-row' + recurring_row + '">';
	html += '  <td class="left">';
	html += '    <select name="product_recurrings[' + recurring_row + '][recurring_id]" class="form-control">>';
	<?php foreach ($recurrings as $recurring) { ?>
	html += '      <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>';
	<?php } ?>
	html += '    </select>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <select name="product_recurrings[' + recurring_row + '][customer_group_id]" class="form-control">>';
	<?php foreach ($customer_groups as $customer_group) { ?>
	html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
	<?php } ?>
	html += '    <select>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <a onclick="$(\'#recurring-row' + recurring_row + '\').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '  </td>';
	html += '</tr>';
	
	$('#tab-recurring table tbody').append(html);
}
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>

<!--mvds-->
<script type="text/javascript"><!--
function getVendors() {
	$.ajax({
		url: 'index.php?route=catalog/mvd_product/vendor&token=<?php echo $token; ?>&vendor_id=' + $('#input-vendor-id').prop('value'),
		dataType: 'json',
		beforeSend: function() {
		$('select[name=\'vendor\']').after('<i class="fa fa-circle-o-notch fa-spin"></i>');
	},
			
	complete: function() {
		$('.fa-spin').remove();
	},
					
	success: function(data) {
		if (data['vendor_name']) {
			$("#input-company").val(data['vendor_name']);
			$("#input-vname").val(data['vname']);
			$("#input-telephone").val(data['telephone']);
			$("#input-fax").val(data['fax']);
			$("#input-email").val(data['email']);
			$("#input-paypal-email").val(data['paypal_email']);
			$("#input-vendor-description").val(data['vendor_description']);
			$("#input-vendor-address").val(data['address']);
			$("#input-vendor-country-zone").val(data['country_name'][0]+', '+data['zone_name'][0]);
			$("#input-store-url").val(data['store_url']);
		} else {
			$("#input-company").val('');
			$("#input-vname").val('');
			$("#input-telephone").val('');
			$("#input-fax").val('');
			$("#input-email").val('');
			$("#input-paypal-email").val('');
			$("#input-vendor-description").val('');
			$("#input-vendor-address").val('');
			$("#input-vendor-country-zone").val('');
			$("#input-store-url").val('');
		}
	}
	});
}
//--></script>
<?php if(0){ ?>
<script type="text/javascript"><!--
var shipping_row = <?php echo $shipping_row; ?>;
function addShipping() {
	html  = '<tr id="shipping-row' + shipping_row + '">'; 
    html += '  <td class="text-left"><select name="product_shipping[' + shipping_row + '][courier_id]" class="form-control">';
    <?php foreach ($couriers as $courier) { ?>
    html += '      <option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>';
    <?php } ?>
    html += '  </select></td>';		
    html += '  <td class="text-right"><input type="text" name="product_shipping[' + shipping_row + '][shipping_rate]" value="" placeholder="<?php echo $entry_shipping_rate; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><select name="product_shipping[' + shipping_row + '][geo_zone_id]" class="form-control">';
	<?php foreach ($geo_zones as $geo_zone) { ?>
	html += '      <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>';
	<?php } ?>
	html += '  </select></td>';	
    html += '  <td class="text-left"><button type="button" onclick="$(\'#shipping-row' + shipping_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	$('#shipping tbody').append(html);
	shipping_row++;
}
//--></script>
<script type="text/javascript"><!--
	function total_cost() {
		if ($('#input-product-cost').prop('value') != '' && $('#input-product-cost').prop('value') >= '0') {
			if ($('#input-shipping-cost').prop('value') != '' && $('#input-shipping-cost').prop('value') >='0') {
				var totalc = parseFloat($('#input-product-cost').prop('value')) + parseFloat($('#input-shipping-cost').prop('value'))
				$("#input-vtotal").val(totalc);
				$("#input-price").val(totalc);
			}
		}
	}
//--></script>
<?php } ?>
<script type="text/javascript"><!--
var reloadGroupPrice = '';

function statusChangeInGS(PS){
    $('#GS_status').attr('style', $('#GS_status > option[value="'+PS+'"]').attr('style'));
}

function GS(pageNum){
    var PC = $('#GS_code').val().trim();
    var PN = $('#GS_name').val().trim();
    var PS = $('#GS_sku').val().trim();
    if ('' == PC && '' == PN && '' == PS){
        alert('<?php echo $error_filter_empty; ?>');
        return false;
    }
    $.ajax({
        url: 'index.php?route=catalog/mvd_product/productSearchForGroup&token=<?php echo $token; ?>&filter_code='+PC+'&filter_name='+PN+'&filter_sku='+PS+'&page='+pageNum+'&this_product=<?php echo (int)$product_id; ?>',
        dataType: 'json',
        beforeSend: function() {
            $('#productGroupUnSelected > tbody').empty();
            $('#productGroupUnSelected').append('<tr><td><i class="fa fa-circle-o-notch fa-spin"></i></td></tr>');
        },
        complete: function() {
            //$('.fa-spin').remove();
        },
        success: function(result) {
            $('#productGroupUnSelected > tbody').empty();
            if (result.success){
                if (result.data.length > 0) {
                    $.each(result.data, function(i, n){
                        insertUnselectedProduct(n, PN, PC);
                    });
                }else{
                    $('#productGroupUnSelected').append('<tr><td colspan="4" class="text-left">没有找到商品！</td></tr>');
                    $('#GS_code').val('');
                    $('#GS_name').val('');
                    $('#GS_sku').val('');
                }
            }else{
                alert(result.info);
            }
        }
    });
}

function insertUnselectedProduct(product, nameCheck, codeCheck) {
    var statusStr = '<span style="color:#FF0000;"><?php echo $text_disabled; ?></span>';
    if (1 == product.status) {
        statusStr = '<span style="color:#0000FF;"><?php echo $text_enabled; ?></span>';
    }else if  (5 == product.status) {
        statusStr = '<span style="color:#FF0000;"><?php echo $txt_pending_approval; ?></span>';
    }

    var addTdStr = '<a href="javascript:void(0);" onclick="addToGroup(\''+product.sku+'\');">添加</a>';
    if ($('#selectProduct_'+product.sku+':visible').length > 0) {
        addTdStr = '';
    }

    var html = '';
    html += '<tr id="unSelectProduct_'+product.sku+'">';
    html += '<td class="text-center" id="imgTd_'+product.sku+'"><img src="'+product.image+'" style="width:40px;height:40px;" /></td>';
    html += '<td id="infoTd_'+product.sku+'">'+valueCheck(product.product_name, nameCheck)+'<br />'+valueCheck(product.product_code, codeCheck)+'</td>';
    html += '<td class="text-center" id="statusTd_'+product.sku+'">'+statusStr+'</td>';
    html += '<td class="text-center">';
    html += '<input type="text" id="quantityInput_'+product.sku+'" value="1" onkeydown="return mustInt(event);" onkeyup="numCheck(this.value, \'quantityInput_'+product.sku+'\')" class="form-control" maxlength="3" style="text-align:right;" />';
    html += '<input type="hidden" id="unSelectProductPrice_'+product.sku+'" value="'+product.product_price+'" />';
    html += '<input type="hidden" id="unSelectProductCost_'+product.sku+'" value="'+product.product_cost+'" />';
    html += '<input type="hidden" id="unSelectProductCost_'+product.sku+'" value="'+product.sale_price+'" />';
    html += '<input type="hidden" id="unSelectProductCost_'+product.sku+'" value="'+product.safe_quantity+'" />';
    html += '<input type="hidden" id="unSelectProductSpecialPrice_'+product.sku+'" value="'+product.product_special_price+'" />';
    html += '</td>';
    html += '<td class="text-center" id="addTd_'+product.sku+'">'+addTdStr+'</td>';
    html += '</tr>';
    $('#productGroupUnSelected').append(html);
}

function valueCheck(valueStr, checkStr) {
    if ('' == checkStr)
        return valueStr;
    return valueStr.replace(checkStr.toLowerCase(), '<span style="background:#00FFFF;">'+checkStr.toLowerCase()+'</span>');
}

function valueUnCheck(valueStr){
    valueStr = valueStr.replace('<span style="background:#00FFFF;">', '');
    valueStr = valueStr.replace('</span>', '');
    return valueStr;
}


function addToGroup(sku) {
    var productInfo = valueUnCheck($('#infoTd_'+sku).html());
    //var html = '<div id="selectProductDiv_'+sku+'" style="padding:3px 5px;margin:3px;border:1px solid #0000FF;float:left;"><a href="javascript:void(0);" onclick="gotoGroupChild(\''+sku+'\');" >'+productInfo+'</a> <a href="javascript:void(0);" onclick="removeFromGroup(\''+sku+'\')"><span style="color:#FF0000;float:right;">X</span></a></div>';
    //$('#groupChildTotal').prepend(html);

    if ($('#selectProduct_'+sku).length > 0) {
        $('#groupChildQty_'+sku).removeAttr('disabled');
        $('#groupChildQty_'+sku).val($('#quantityInput_'+sku).val());
        $('#selectProduct_'+sku).show();
        var html = $('#selectProduct_'+sku).clone();
        $('#selectProduct_'+sku).remove();
    }else{
        var html = '<tr id="selectProduct_'+sku+'">';
        html += '<td class="text-center">'+$('#imgTd_'+sku).html()+'</td>';
        html += '<td>'+productInfo+'</td>';
        html += '<td class="text-center">'+$('#statusTd_'+sku).html()+'</td>';
        html += '<td class="text-center">';
        html += '<input type="text" name="groupChild[_'+sku+']" id="groupChildQty_'+sku+'" value="'+$('#quantityInput_'+sku).val()+'" class="form-control" maxlength="3" style="text-align:right;" />';
        html += '<input type="hidden" id="selectProductPrice_'+sku+'" value="'+$('#unSelectProductPrice_'+sku).val()+'" />';
        html += '<input type="hidden" id="selectProductCost_'+sku+'" value="'+$('#unSelectProductCost_'+sku).val()+'" />';
        html += '<input type="hidden" id="selectProductSpecialPrice_'+sku+'" value="'+$('#unSelectProductSpecialPrice_'+sku).val()+'" />';
        html += '</td>';
        html += '<td class="text-center">';
        html += '<input type="text" name="groupChildPrice[_'+sku+']" id="groupChildPriceQty_'+sku+'" value="'+$('#quantityInput_'+sku).val()+'" class="form-control" maxlength="3" style="text-align:right;" />';
        html += '</td>';
        html += '<td class="text-center"><a href="javascript:void(0);" onclick="removeFromGroup(\''+sku+'\');">移除</a></td>';
        html += '</tr>';
    }
    $('#productGroupSelected').prepend(html);
    $('#addTd_'+sku).html('');
    resetTotal();
}

function removeFromGroup(sku) {
    $('#quantityInput_'+sku).val($('#groupChildQty_'+sku).val());
    //$('#selectProductDiv_'+sku).remove();
    $('#groupChildQty_'+sku).attr('disabled', true);
    $('#selectProduct_'+sku).hide();
    $('#addTd_'+sku).html('<a href="javascript:void(0);" onclick="addToGroup(\''+sku+'\');">添加</a>');
    resetTotal();
}

function resetTotal() {
    var tPrice = tCost = 0;
    var thisSKU = '';
    $.each($('input[id^="groupChildQty_"]:enabled'),function(i, n){
        thisSKU = $(n).attr('id').substr(14);
        tPrice = accAdd(tPrice, accMul($('#selectProductPrice_'+thisSKU).val(), $(n).val()));
        tCost = accAdd(tCost, accMul($('#selectProductCost_'+thisSKU).val(), $(n).val()));
    });
    $('#input-price').val(tPrice);
    $('#input-product-cost').val(tCost);
    resetTotalSpecialPrice();
}

function resetTotalSpecialPrice(){
    var tSpecialPrice = 0;
    var thisSKU = '';
    $.each($('input[id^="groupChildQty_"]:enabled'),function(i, n){
        thisSKU = $(n).attr('id').substr(14);
        tSpecialPrice = accAdd(tSpecialPrice, accMul($('#selectProductSpecialPrice_'+thisSKU).val(), $(n).val()));
    });
    $('#input-product-special-price').html(tSpecialPrice);
}
resetTotalSpecialPrice();

function gotoGroupChild(sku) {
    //alert(parseInt($('#selectProduct_'+sku).offset().top-$('#productGroupSelectedDiv').offset().top));
    //alert(parseInt($('#selectProduct_'+sku).position().top-$('#productGroupSelectedDiv').position().top));
    $('#productGroupSelectedDiv').scrollTop(parseInt($('#selectProduct_'+sku).offset().top-$('#productGroupSelectedDiv').offset().top));
    var bgColor = $('#selectProduct_'+sku).css('background-color');
    $('#selectProduct_'+sku).css('background-color', '#FFFF00');
    setTimeout('initBackGroundColor("'+sku+'", "'+bgColor+'")', 1000);
}

function enterToSearch(event){
    var kc = event.keyCode;
    if (13 == kc){
        GS(1);
        return false;
    }
    return true;
}

function numCheck(valueInt, inputId) {
    if ('' == valueInt) {
        valueInt = 1;
    }else{
        valueInt = parseInt(valueInt);
    }
    $('#'+inputId).val(Math.max(1, valueInt));
    if ('groupChildQty' == inputId.substr(0, 13)) {
        resetTotal();
    }
}

function numCheckPrice(valueInt, inputId) {
    if ('' == valueInt) {
        valueInt = 1;
    }else{
        valueInt = parseInt(valueInt);
    }
    $('#'+inputId).val(Math.max(1, valueInt));
    // if ('groupChildPriceQty' == inputId.substr(0, 13)) {
    //     resetTotal();
    // }
}

function mustInt(event, num) {
    var kc = event.keyCode;
    if (kc >47 && kc < 58){
        return true;
    }
    if (kc >95 && kc < 106){
        return true;
    }
    if (kc == 8 || kc == 9 || kc == 37 || kc == 39 || kc == 116){
        return true;
    }
    //alert(kc);
    return false;
}

function initBackGroundColor(sku, bgColor){
    $('#selectProduct_'+sku).css('background-color', bgColor);
}

$('#GS_code').keydown(function(event){
    return enterToSearch(event);
});
$('#GS_name').keydown(function(event){
    return enterToSearch(event);
});

$('#input-product-type').change(function(){
    var PT = $('#input-product-type').val();
    if (1 == PT){
        $('a[href$="tab-product-group"]').hide();
        $('#groupSpecialPrice').hide();
    }else if (2 == PT){
        $('a[href$="tab-product-group"]').show();
        $('#groupSpecialPrice').show();
    }
});

<?php
if (2 != $product_type){
    echo '$(\'a[href$="tab-product-group"]\').hide();';
    echo '$("#groupSpecialPrice").hide()';
}
?>

//$('#GS_code').autocomplete({
//    'source': function(request, response) {
//        $.ajax({
//            url: 'index.php?route=catalog/mvd_product/autocomplete&token=<?php echo $token; ?>&filter_product_code='+encodeURIComponent(request),
//            dataType: 'json',
//            success: function(json) {
//                response($.map(json, function(item) {
//                    return {
//                        label: item['product_code'],
//                        value: item['product_id']
//                    }
//                }));
//            }
//        });
//    },
//    'select': function(item) {
//        $('#GS_code').val(item['label']);
//    }
//});


//$('#GS_name').autocomplete({
//    'source': function(request, response) {
//        $.ajax({
//            url: 'index.php?route=catalog/mvd_product/autocomplete&token=<?php echo $token; ?>&filter_name='+encodeURIComponent(request),
//            dataType: 'json',
//            success: function(json) {
//                response($.map(json, function(item) {
//                    return {
//                        label: item['name'],
//                        value: item['product_id']
//                    }
//                }));
//            }
//        });
//    },
//    'select': function(item) {
//        $('#GS_name').val(item['label']);
//    }
//});

//$('#GS_sku').autocomplete({
//    'source': function(request, response) {
//        $.ajax({
//            url: 'index.php?route=catalog/mvd_product/autocomplete&token=<?php echo $token; ?>&filter_sku='+encodeURIComponent(request),
//            dataType: 'json',
//            success: function(json) {
//                response($.map(json, function(item) {
//                    return {
//                        label: item['sku'],
//                        value: item['product_id']
//                    }
//                }));
//            }
//        });
//    },
//    'select': function(item) {
//        $('#GS_sku').val(item['label']);
//    }
//});

function accAdd(arg1,arg2){
    var r1,r2,m;
    try{
        r1=arg1.toString().split(".")[1].length;
    }catch(e){
        r1=0
    }
    try{
        r2=arg2.toString().split(".")[1].length;
    }catch(e){
        r2=0
    }
    m = Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
}

function accMul(arg1,arg2){
    var m=0,s1=arg1.toString(),s2=arg2.toString();    
    try{
        m+=s1.split(".")[1].length}catch(e){}
    try{
        m+=s2.split(".")[1].length}catch(e){}
    return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m);
}
	
	<!--初始联动商品分类-->
	if('<?php echo $product_class1; ?>'){
		changeClass('<?php echo $product_class1; ?>','<?php echo $product_class2 ; ?>');
		changeClass2('<?php echo $product_class2; ?>','<?php echo $product_class3 ; ?>');
	}
	<!--初始联动商品分类-->
	
	//联动商品分类2
	function changeClass(class1_id,class2_id) {
		$.ajax({
			url: 'index.php?route=catalog/mvd_product/getClass2&token=<?php echo $token; ?>&parent_id=' + class1_id,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'product_class2\']').after('<i class="fa fa-circle-o-notch fa-spin"></i>'); //loading
			},
			complete: function() {
				$('.fa-spin').remove(); //移除loading
			},
			success: function(json) {
	
				html = '<option code="" value=""><?php echo $text_select; ?></option>';

				if (json != '') {
					for (i = 0; i < json.length; i++) {
						html += '<option code="'+json[i]['code']+'" value="' + json[i]['category_id'] + '"';
	
						if (json[i]['category_id'] == class2_id) {
							html += ' selected="selected"';
						}
	
						html += '>' + json[i]['name'] + '</option>';
					}
				} else {
					html += '<option code="" value=""><?php echo $text_none; ?></option>';
				}
	
				$('select[name=\'product_class2\']').html(html);
				
				//触发2级分类change
				$('select[name=\'product_class2\']').trigger('change');
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
	
	//联动商品分类3
	function changeClass2(class2_id,class3_id) {
		$.ajax({
			url: 'index.php?route=catalog/mvd_product/getClass2&token=<?php echo $token; ?>&parent_id=' + class2_id,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'product_class3\']').after('<i class="fa fa-circle-o-notch fa-spin"></i>'); //loading
			},
			complete: function() {
				$('.fa-spin').remove(); //移除loading
			},
			success: function(json) {
	
				html = '<option code="" value=""><?php echo $text_select; ?></option>';

				if (json != '') {
					for (i = 0; i < json.length; i++) {
						html += '<option code="'+json[i]['code']+'" value="' + json[i]['category_id'] + '"';
	
						if (json[i]['category_id'] == class3_id) {
							html += ' selected="selected"';
						}
	
						html += '>' + json[i]['name'] + '</option>';
					}
				} else {
					html += '<option code="" value=""><?php echo $text_none; ?></option>';
				}
	
				$('select[name=\'product_class3\']').html(html);
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
	
	//生成商品代码
	function createCode(){
		
		var frontcode = $("#select-class1").find("option:selected").attr("code");
		var backcode = $("#select-class3").find("option:selected").attr("code");
		
		if(backcode == "")
		{
			backcode = $("#select-class2").find("option:selected").attr("code"); 
		}
		
		if(frontcode == "" || backcode == "")
		{
			alert('编码缺失, 无法生成商品编码');
			return;
		}
		
		var class_code = frontcode+backcode;
		
		$.ajax({
			url: 'index.php?route=catalog/mvd_product/createCode&token=<?php echo $token; ?>&class_code=' + class_code,
			dataType: 'json',
			beforeSend: function() {
				$('#input-pro-code').after('<i class="fa fa-circle-o-notch fa-spin"></i>'); //loading
			},
			complete: function() {
				$('.fa-spin').remove(); //移除loading
			},
			success: function(data){
				$("#input-pro-code").val(data);
				$("#input-pro-code").trigger('change');
			},
			error: function() {
				alert('运行错误');
			}
		});
		
	}
	
	$("#input-pro-code").on('change',function(e){
		
		var no = 0;
		$(".option-table").find("tbody tr").each(function(){
			var option_code = $("#input-pro-code").val().toString()+no; 	
			$(this).find(".option-code").val(option_code);
			no++;
		});
		
	});
	
	<!--只分配城市autocomplete-->
	$('input[name=\'only_country\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=area/area/autoArea&type=country&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['country_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'only_country\']').val('');
			
			$('#only-country' + item['value']).remove();
			
			$('#only-country').append('<div id="only-country' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="only_countries[]" value="' + item['value'] + '" /></div>');	
		}
	});
	<!--只分配城市autocomplete-->
	
	<!--只分配市区autocomplete-->
	$('input[name=\'only_zone\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=area/area/autoArea&type=zone&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['zone_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'only_zone\']').val('');
			
			$('#only-zone' + item['value']).remove();
			
			$('#only-zone').append('<div id="only-zone' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="only_zones[]" value="' + item['value'] + '" /></div>');	
		}
	});
	<!--只分配市区autocomplete-->
		
	<!--不分配城市autocomplete-->
	$('input[name=\'no_country\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=area/area/autoArea&type=country&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['country_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'no_country\']').val('');
			
			$('#no-country' + item['value']).remove();
			
			$('#no-country').append('<div id="no-country' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="no_countries[]" value="' + item['value'] + '" /></div>');	
		}
	});
	<!--不分配城市autocomplete-->
	
	<!--不分配市区autocomplete-->
	$('input[name=\'no_zone\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=area/area/autoArea&type=zone&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['zone_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'no_zone\']').val('');
			
			$('#no-zone' + item['value']).remove();
			
			$('#no-zone').append('<div id="no-zone' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="no_zones[]" value="' + item['value'] + '" /></div>');	
		}
	});
	<!--不分配市区autocomplete-->
	
	<!--删除区域autocomplete-->
	$('.chose-area').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});
	<!--删除区域autocomplete-->

//--></script>
<!--mvde-->
<?php echo $footer; ?> 
