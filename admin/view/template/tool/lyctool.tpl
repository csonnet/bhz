<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">
	
    <!--顶部-->
	<div class="page-header">
		<div class="container-fluid">
      		<h1><?php echo $heading_title; ?></h1>
      		<ul class="breadcrumb">
        		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
        		<li><a href="<?php echo $breadcrumb['../stock/href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        		<?php } ?>
      		</ul>
		</div>
  	</div>
    <!--顶部-->
    
    <!--内容-->
	<div class="container-fluid">
    	
        <!--消息区-->
    	<?php if ($error_warning) { ?>
    	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?>
      		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
    	<?php if ($success) { ?>
    	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      		<button type="button" form="form-backup" class="close" data-dismiss="alert">&times;</button>
    	</div>
    	<?php } ?>
        <!--消息区-->
        
        <!--商城商品索引-->
    	<div class="panel panel-default">
        
			<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-exchange"></i> 重建商城商品索引</h3>
      		</div>
            
			<div class="panel-body">
            
            	<form action="<?php echo $rebuildProductAction; ?>" method="post" enctype="multipart/form-data" id="rebuildProductAction" class="form-horizontal">
				<div class="form-group">
                    <label class="col-sm-3 control-label">重建商城商品索引</label>
                    <div class="col-sm-9">
                      	<a class="btn btn-primary" onclick="rebuildProductIndex();">重建</a>
					</div>
				</div>       
				</form>  
                
			</div>
            
		</div>
        <!--商城商品索引-->
        
        <!--小程序商铺搜索索引-->
    	<div class="panel panel-default">
        
			<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-exchange"></i> 重建小程序搜索商铺商品索引</h3>
      		</div>
            
			<div class="panel-body">
            
            	<form action="<?php echo $rebuildWxappAction; ?>" method="post" enctype="multipart/form-data" id="rebuildWxappAction" class="form-horizontal">
				<div class="form-group">
                    <label class="col-sm-3 control-label">重建小程序搜索商铺商品索引</label>
                    <div class="col-sm-9">
                      	<a class="btn btn-primary" onclick="rebuildWxappShopProducts();">重建</a>
					</div>
				</div>       
				</form>  
                
			</div>
            
		</div>
        <!--小程序商铺搜索索引-->
        
        <!--用户相关-->
    	<div class="panel panel-default">
        
			<div class="panel-heading">
        		<h3 class="panel-title"><i class="fa fa-exchange"></i> 添加优惠券</h3>
      		</div>
            
			<div class="panel-body">
            
            	<form action="<?php echo $couponAction; ?>" method="post" enctype="multipart/form-data" id="couponAction" class="form-horizontal">
				<div class="form-group">
                    <label class="col-sm-3 control-label">手机号</label>
                    <div class="col-sm-9">
                    	<input type="text" name="c_tel" value="" placeholder="手机号" id="input-tel" class="form-control bhz-input" />
                        <input type="text" readonly name="c_id" value="" id="input-id" class="form-control bhz-input" />
                  	</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">优惠券金额</label>
                    <div class="col-sm-9">
                    	<input type="text" name="c_coupon" value="" placeholder="金额" id="input-coupon" class="form-control bhz-input" />
                  	</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">数量</label>
                    <div class="col-sm-9">
                  		<input type="text" name="c_number" value="" placeholder="数量" id="input-number" class="form-control bhz-input" />
                  	</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">有效天数</label>
                    <div class="col-sm-9">
                  		<input type="text" name="c_days" value="" placeholder="有效天数" id="input-days" class="form-control bhz-input" />
                        <a class="btn btn-primary" onclick="addCoupon();">添加</a>
                  	</div>
				</div>     
				</form>
                
			</div>
            
		</div>
        <!--用户相关-->

        <!--用户组特价-->
        <div class="panel panel-default">
        

           
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-exchange"></i> 导入用户组特价</h3>
            </div>
            
            <div class="panel-body">
            
                <form action="<?php echo $couponAction; ?>" method="post" enctype="multipart/form-data" id="couponAction" class="form-horizontal">
                <div class="form-group">
                     <a id="button-upload" data-toggle="tooltip" title="导入用户组特价" class="btn btn-primary"><i class="fa fa-calculator"></i> 导入用户组特价</a>
                </div>   
                </form>
                
            </div>
            
        </div>
        <!--用户组特价-->
        
	</div>
	<!--内容-->
  
</div>

<script type="text/javascript">
	
	//重建商城商品索引
	function rebuildProductIndex(){
		$('#rebuildProductAction').submit();
	}
	
	//重建小程序搜索商铺商品索引
	function rebuildWxappShopProducts(){
		$('#rebuildWxappAction').submit();
	}
	
	//添加优惠券
	function addCoupon(){
		$('#couponAction').submit();
	}
	
	<!-- 用户手机autocomplete -->
    $('input[name="c_tel"]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=tool/lyctool/autoTel&token=<?php echo $token; ?>&tel=' +  encodeURIComponent(request),
                dataType: "json",
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['telephone'],
                            value: item['customer_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name="c_tel"]').val(item['label']);
            $('input[name="c_id"]').val(item['value']);
        }
    });
    <!-- 用户手机autocomplete -->

$('#button-upload').on('click', function() {
  $('#form-upload').remove();
  
  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');
  
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  
  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);   
      
      $.ajax({
        url: 'index.php?route=tool/lyctool/importproSpeXial&token=<?php echo $token; ?>',
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            $('#form-order').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
                
          if (json['success']) {
            $('#form-order').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            // window.location.reload();
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          $('html').html(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

	
</script>

<?php echo $footer; ?>