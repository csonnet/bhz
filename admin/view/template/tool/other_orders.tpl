<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1>外场订单导入</h1>
            <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" form="form-backup" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-exchange"></i> 外场订单导入</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="upform" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-import">选择文件导入</label>
            <div class="col-sm-10">
              <input type="file" name="upload" id="uploadExcel" />
            </div>
          </div>
          
			<div class="form-group">
      			<label class="col-sm-2 control-label" for="input-import">采购单</label>
                <input type="hidden" name="uploadType" id="uploadType" value="" />
                <div class="col-sm-10">
         			<a class="btn btn-primary" onclick="upload('po');"><span>导入采购单</span></a>
      			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="input-import">订单</label>
				<div class="col-sm-10">
                	<span class="import-span">
                    	<strong>价格：</strong>
                		<input type="radio" name="importPrice" value="1" checked="checked" /><span>采用系统价格</span>
                    	<input type="radio" name="importPrice" value="0" /><span>采用excel价格</span>
             		</span>
                    <span class="import-span">
                    	<strong>库存：</strong>
                		<input type="radio" name="importStock" value="1" checked="checked" /><span>改动库存</span>
                    	<input type="radio" name="importStock" value="0" /><span>不改动库存</span>
             		</span>
   					<a class="btn btn-primary" onclick="upload('order');"><span>导入订单</span></a>
     			</div>
			</div>
        </form>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
<!--
function checkFileSize(id) {
    // See also http://stackoverflow.com/questions/3717793/javascript-file-upload-size-validation for details
    var input, file, file_size;

    if (!window.FileReader) {
        // The file API isn't yet supported on user's browser
        return true;
    }

    input = document.getElementById(id);
    if (!input) {
        // couldn't find the file input element
        return true;
    }else if (!input.files) {
        // browser doesn't seem to support the `files` property of file inputs
        return true;
    }else if (!input.files[0]) {
        // no file has been selected for the upload
        alert('点击【导入】前请先选择文件 ！');
        return false;
    }else {
        file = input.files[0];
        file_size = file.size;
        <?php if (!empty($post_max_size)) { ?>
        // check against PHP's post_max_size
        post_max_size = <?php echo $post_max_size; ?>;
        if (file_size > post_max_size) {
            alert('文件大小大于了PHP设定【post_max_size】 ！');
            return false;
        }
        <?php } ?>
        <?php if (!empty($upload_max_filesize)) { ?>
        // check against PHP's upload_max_filesize
        upload_max_filesize = <?php echo $upload_max_filesize; ?>;
        if (file_size > upload_max_filesize) {
            alert('文件大小大于了PHP设定【upload_max_file_size】 ！');
            return false;
        }
        <?php } ?>
        return true;
    }
}

function upload(uploadType) {
    $('#uploadType').val(uploadType);
    if (checkFileSize('uploadExcel')) {
        $('#upform').submit();
    }
}

//-->
</script>

<?php echo $footer; ?>