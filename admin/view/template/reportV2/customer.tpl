<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1>业务员业绩报表</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-body" id='panelBody'>
        <div class="well">
          <div class="row">
            <div class="col-sm-1">
              <div class="form-group">
                <label class="control-label">按时间查看</label>
                <select name="filter_date" id="filter_date" class="form-control">
                <?php
                foreach ($filterDateList as $filterDateValue=>$filterDateName) {
                    echo '<option value="'.$filterDateValue.'"';
                    if ($filter['date'] == $filterDateValue) {
                        echo 'selected';
                    }
                    echo '>'.$filterDateName.'</option>';
                }
                ?>
                </select>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="form-group">
                <label class="control-label">按业务员查看</label>
                <select name="filter_user" id="filter_user" class="form-control">
                  <option value="0">所有业务员</option>
                <?php
                foreach ($filterUserList as $filterUser) {
                    echo '<option value="'.$filterUser['user_id'].'"';
                    if ($filter['user'] == $filterUser['user_id']) {
                        echo 'selected';
                    }
                    echo '>'.$filterUser['fullname'].'（'.$filterUser['username'].'）</option>';
                }
                ?>
                </select>
              </div>
            </div>
            <div class="col-sm-1">
              <div class="form-group">
                <button type="button" id="button-filter" class="btn btn-primary pull-left" style="margin-top:22px;"><i class="fa fa-search"></i> 筛选</button>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive" style='width:100%;'>
            <?php
            if (0 == $filter['date'] && $filter['user']) {
                $strBarTitle = $strBarXData = "";
                $strBarSeriesData = array();
                foreach ($reportList as $key=>$reportInfo) {
                    $strBarTitle = $reportInfo['user'];
                    $strBarXData .= "'".$reportInfo['date']."',";
                    $strBarSeriesData['registerNow'][] = $reportInfo['registerNow'];
                    $strBarSeriesData['orderedNow'][] = $reportInfo['orderedNow'];
                    $strBarSeriesData['reOrderedNow'][] = $reportInfo['reOrderedNow'];
                }
            ?>
            <div id="bar_single" style="height:200px;margin:0;"></div>
            <script>
            var myBar = echarts.init(document.getElementById('bar_single'), 'shine');
            var myOption = {
                title: {
                    text: '<?php echo $strBarTitle; ?>',
                    left: 0,
                    top: 0
                },
                legend: {
                    right: 20,
                    top: 0,
                    data: ['当月新注册会员数','当月下单客户数','当月复购客户数']
                },
                barMaxWidth: '10%',
                grid: {
                    left: 30,
                },
                xAxis: {
                    data: [<?php echo substr($strBarXData, 0, -1); ?>]
                },
                yAxis: {},
                series: [
                    {
                        name: '当月新注册会员数',
                        type: 'bar',
                        data: [<?php echo implode(',', $strBarSeriesData['registerNow']); ?>]
                    },
                    {
                        name: '当月下单客户数',
                        type: 'bar',
                        data: [<?php echo implode(',', $strBarSeriesData['orderedNow']); ?>]
                    },
                    {
                        name: '当月复购客户数',
                        type: 'bar',
                        data: [<?php echo implode(',', $strBarSeriesData['reOrderedNow']); ?>]
                    }
                ]
            };
            myBar.setOption(myOption);
            window.onresize = function (){
                myBar.resize();
            }
            </script>
            <?php
            }
            ?>
            <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <?php
                    if ('date' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'date\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'date\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'date\', \'desc\')">';
                    }
                    ?>
                    时间</a>
                </td>
                <td>
                    <?php
                    if ('user' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'user\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'user\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'user\', \'desc\')">';
                    }
                    ?>
                    业务员</a>
                </td>
                <td>
                    <?php
                    if ('registerNow' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'registerNow\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'registerNow\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'registerNow\', \'desc\')">';
                    }
                    ?>
                    当月新注册会员数</a>
                </td>
                <td>
                    <?php
                    if ('orderedNow' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNow\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNow\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNow\', \'desc\')">';
                    }
                    ?>
                    当月下单客户数</a>
                </td>
                <td>
                    <?php
                    if ('reOrderedNow' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNow\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNow\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNow\', \'desc\')">';
                    }
                    ?>
                    当月复购客户数</a>
                </td>
                <td>
                    <?php
                    if ('orderedNowRate' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNowRate\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNowRate\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'orderedNowRate\', \'desc\')">';
                    }
                    ?>
                    转化率</a>
                </td>
                <td>
                    <?php
                    if ('reOrderedNowRate' == $sort['col']) {
                        if ('desc' == $sort['seq']) {
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNowRate\', \'asc\')" class="asc">';
                        }else{
                            echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNowRate\', \'desc\')" class="desc">';
                        }
                    }else{
                        echo '<a href="javascript:void(0);" onclick="displayBySort(\'reOrderedNowRate\', \'desc\')">';
                    }
                    ?>
                    复购率</a>
                </td>
                <td>流失率</td>
                <td>注册会员结构</td>
                <td>已合作客户结构</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($reportList as $key=>$reportInfo) {
                $strPieRegisterLegend = $strPieRegisterSeries = "";
                foreach ($reportInfo['registerShopTypes'] as $shopTypeName=>$nums) {
                    $strPieRegisterLegend .= "{name:'".$shopTypeName."'},";
                    $strPieRegisterSeries .= "{value:".$nums.", name:'".$shopTypeName."'},";
                }
                $strPieOrderedLegend = $strPieOrderedSeries = '';
                foreach ($reportInfo['orderedShopTypes'] as $shopTypeName=>$nums) {
                    $strPieOrderedLegend .= "{name:'".$shopTypeName."'},";
                    $strPieOrderedSeries .= "{value:".$nums.", name:'".$shopTypeName."'},";
                }
            ?>
            <tr>
                <td><?php echo $reportInfo['date']; ?></td>
                <td><?php echo $reportInfo['user']; ?></td>
                <td><?php echo $reportInfo['registerNow']; ?></td>
                <td><?php echo $reportInfo['orderedNow']; ?></td>
                <td><?php echo $reportInfo['reOrderedNow']; ?></td>
                <td><?php echo $reportInfo['orderedNowRate']; ?>%</td>
                <td><?php echo $reportInfo['reOrderedNowRate']; ?>%</td>
                <td><?php echo $reportInfo['lostRate']; ?>%</td>
                <td class='text-left' width='200'>
                    <div id='pie_register_<?php echo $key; ?>' style="width:200px;height:210px;margin:0;"></div>
                    <script>
                    var myPR_<?php echo $key; ?> = echarts.init(document.getElementById('pie_register_<?php echo $key; ?>'), 'shine');
                    var myOption_<?php echo $key; ?> = {
                        legend: {
                            x: 0,
                            y: 0,
                            data: [<?php echo substr($strPieRegisterLegend, 0, -1);?>]
                        },
                        series : [{
                            type: 'pie',
                            stillShowZeroSum: false,
                            itemStyle: {
                                normal: {
                                    label: {show: false},
                                    labelLine: {show: false}
                                }
                            },
                            radius: '50',
                            center: ['80', '150'],
                            data: [<?php echo substr($strPieRegisterSeries, 0, -1);?>]
                        }],
                    }
                    myPR_<?php echo $key; ?>.setOption(myOption_<?php echo $key; ?>);
                    </script>
                </td>
                <td class='text-left' width='200'>
                    <div id='pie_ordered_<?php echo $key; ?>' style="width:200px;height:210px;margin:0;"></div>
                    <script>
                    var myPR_<?php echo $key; ?> = echarts.init(document.getElementById('pie_ordered_<?php echo $key; ?>'), 'shine');
                    var myOption_<?php echo $key; ?> = {
                        legend: {
                            x: 0,
                            y: 0,
                            data: [<?php echo substr($strPieOrderedLegend, 0, -1);?>]
                        },
                        series: [{
                            type: 'pie',
                            stillShowZeroSum: false,
                            itemStyle: {
                                normal: {
                                    label: {show: false},
                                    labelLine: {show: false}
                                }
                            },
                            radius: '50',
                            center: ['80', '150'],
                            data: [<?php echo substr($strPieOrderedSeries, 0, -1);?>]
                        }]
                    }
                    myPR_<?php echo $key; ?>.setOption(myOption_<?php echo $key; ?>);
                    </script>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
var myFilter = {date:'',user:''};
var mySort = {col:'',seq:''};
$('#button-filter').on('click', function(){
    myFilter.date = $('#filter_date').val();
    myFilter.user = $('#filter_user').val();
    mySort.col = '';
    mySort.seq = '';
    pageReload();
})

function displayBySort(col, seq) {
    myFilter.date = $('#filter_date').val();
    myFilter.user = $('#filter_user').val();
    mySort.col = col;
    mySort.seq = seq;
    pageReload();
}

function pageReload() {
    var url = 'index.php?route=reportV2/customer&token=<?php echo $token; ?>';
    for (var tmp in myFilter) {
        url += '&filter_'+tmp+'='+myFilter[tmp];
    }
    for (var tmp in mySort) {
        url += '&sort_'+tmp+'='+mySort[tmp];
    }
    location = url;
}

</script>
<?php echo $footer; ?> 
