<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right">
          <a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">导出</a>

      </div>
      <h1>供应商分析统计</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-body" id='panelBody'>
                  <div class="table-responsive">
          <table class="table table-bordered table-hover">
          <thead>
                <tr>
                  <td>供应商编号</td>
                  <td>供应商名字</td>
                  <td>供应商状态</td>
                  <td>2017-01</td>
                  <td>2017-02</td>
                  <td>2017-03</td>
                  <td>2017-04</td>
                  <td>2017-05</td>
                  <td>2017-06</td>
                  <td>2017-07</td>
                  <td>2017-08</td>
                  <td>2017-09</td>
                  <td>2017-10</td>
                  <td>2017-11</td>
                  <td>2017-12</td>
                  <td>2018-01</td>
                  <td>年至上月末销售金额</td>
                  <td>利润额</td>
                  <td>毛利率</td>
                  <td>2017销售金额</td>
                  <td>2017利润额</td>
                  <td>2017毛利率</td>
<!--                   <td>费用</td> -->
                  <td>库存金额</td>
                  <td>库存天数</td>
                </tr>
              </thead><tbody>
                  <?php if ($invtotal) { ?>
                  <?php foreach ($invtotal as $key=> $invto) { ?>
                    <?php if($key){ ?>
                    <tr>
                      <td><?php echo $invto['vendor_id']; ?></td>
                      <td><?php echo $invto['vendor_name']; ?></td>
                      <td><?php echo $invto['status']; ?></td>
                      <td><?php echo $invto['2017-01']; ?></td>
                      <td><?php echo $invto['2017-02']; ?></td>
                      <td><?php echo $invto['2017-03']; ?></td>
                      <td><?php echo $invto['2017-04']; ?></td>
                      <td><?php echo $invto['2017-05']; ?></td>
                      <td><?php echo $invto['2017-06']; ?></td>
                      <td><?php echo $invto['2017-07']; ?></td>
                      <td><?php echo $invto['2017-08']; ?></td>
                      <td><?php echo $invto['2017-09']; ?></td>
                      <td><?php echo $invto['2017-10']; ?></td>
                      <td><?php echo $invto['2017-11']; ?></td>
                      <td><?php echo $invto['2017-12']; ?></td>
                      <td><?php echo $invto['2018-01']; ?></td>
              <!--         <td><?php echo $invto['product_class1'] ;?></td>
                      <td><?php echo $invto['name']; ?></td> -->
                      <td><?php echo $invto['outtotal']; ?></td>
                      <!-- <td><?php echo (round($invto['outtotal']/$invtotal[0][$invto['vendor_id']],4)*100).'%'; ?></td> -->
                      <td><?php echo ($invto['outtotal']-$invto['buytotal']); ?></td>
                      <td><?php echo  (round(($invto['outtotal']-$invto['buytotal'])/$invto['outtotal'], 4)*100).'%';?></td>

                      <td><?php echo $invto['outtotal17']; ?></td>
                      <!-- <td><?php echo (round($invto['outtotal']/$invtotal[0][$invto['vendor_id']],4)*100).'%'; ?></td> -->
                      <td><?php echo ($invto['outtotal17']-$invto['buytotal17']); ?></td>
                      <td><?php echo  (round(($invto['outtotal17']-$invto['buytotal17'])/$invto['outtotal17'], 4)*100).'%';?></td>
                      <!-- <td>--</td> -->
                      <td><?php echo $invto['invtotal']; ?></td>
                      <td><?php echo round($invto['invtotal']/$invto['wouttotal']*28, 2); ?></td>
                     
                      </tr>
                    <?php } ?>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 
