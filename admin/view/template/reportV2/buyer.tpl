<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
          <a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">导出</a>

      </div>
      <h1>采销员业绩统计</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-body" id='panelBody'>
                  <div class="table-responsive">
          <table class="table table-bordered table-hover">
          <thead>
                <tr >
                  <td colspan="13" >说明：毛利= （销售商品总额-销售商品进货总额）/销售商品总额<br>
销售商品进货总额=A销售商品数量*A平均进货价+B销售商品数量*B平均进货价+，，，，+N销售商品数量*N平均进货价。<br>
平均进货价=（第1次商品进货总数*第1次商品进货价+第2次商品进货总数*第2次商品进货价+，，，+第n次商品进货总数*第n次商品进货价）/(第1次商品进货总数+第2次商品进货总数+,,,+第n次商品进货总数)(注释：如果不存在进货，平均进货价，取当前定义的成本价)<br>
</td>
                </tr>
                <tr>
                  <td>采销员</td>
                  <td>大分类</td>
                  <td>今天销量额</td>
                  <td>昨天销量额</td>

                  <td>月至今销量额</td>
                  <td>月至今毛利</td>
                  <td>年至今销量额</td>
                  <td>年至今毛利</td>
                  <td>上月销量额</td>
                  <td>上月毛利</td>
                  <td>库存金额</td>
                  <td>订购金额</td>
                  <td>待发货金额</td>
                  <td>库存天数</td>
                </tr>

              </thead><tbody>
                  <?php if ($pclass['YL']) { ?>
                  <?php foreach ($pclass['YL'] as $cost) { ?>
                  <tr>
                    <td >应亮</td>
                    <td><?php echo $cost['name'] ?></td>
                    <td><?php echo $cost['TPXLND']; $totalTQNDy+= $cost['TPND'];?></td>
                    <td><?php echo $cost['TPXLLD']; $totalTQLDy+= $cost['TPLD'];?></td>
            
                    <td><?php echo $cost['TPXLLMN']; $totalTQLMNy+= $cost['TPLMN'];$totalTCLMNy+= $cost['TCLMN'];$totalTQLMNyn+= $cost['TPLMN-n'];$totalTCLMNyn+= $cost['TCLMN-n'];?></td>
                    <td><?php echo $cost['GPRLMN']?></td>
                    <td><?php echo $cost['TPXLTY']; $totalTPTYy+= $cost['TPTY'];$totalTCTYy+= $cost['TCTY'];$totalTPTYyn+= $cost['TPTY-n'];$totalTCTYyn+= $cost['TCTY-n'];?></td>
                    <td><?php echo $cost['GPRTY']?></td>
                    <td><?php echo $cost['TPXLSY']; $totalTQSYy+= $cost['TPSY'];$totalTCSYy+= $cost['TCSY'];$totalTQSYyn+= $cost['TPSY-n'];$totalTCSYyn+= $cost['TCSY-n'];?></td>
                    <td><?php echo $cost['GPRSY']?></td>
                    <td><?php echo $cost['totalpricexs']; $totaltotalpricey+= $cost['totalprice']; ?></td>
                    <td><?php echo $cost['pobuypricexs'];$totalpobuypricey+= $cost['pobuyprice']; ?></td>
                    <td><?php echo $cost['stockpricexs'];$totalstockpricey+= $cost['stockprice']; ?></td>
                    <td><?php echo $cost['SSC']; $totalSQNy+= $cost['availableQty'];$totalTQLMy+= $cost['TQLM']?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td >合计</td>
                    <td>--</td>
                    <td><?php $totalTQNDyxs = $totalTQNDy; echo number_format($totalTQNDyxs);?></td>
                    <td><?php $totalTQLDyxs = $totalTQLDy; echo number_format($totalTQLDyxs);?></td>
              
                    <td><?php $totalTQLMNyxs = $totalTQLMNy; echo number_format($totalTQLMNyxs);?></td>
                    <td><?php echo (round(($totalTQLMNyn-$totalTCLMNyn)/$totalTQLMNyn, 4)*100).'%';?></td>
                    <td><?php $totalTPTYyxs = $totalTPTYy;echo number_format($totalTPTYy);?></td>
                    <td><?php echo (round(($totalTPTYyn-$totalTCTYyn)/$totalTPTYyn, 4)*100).'%';?></td>
                    <td><?php $totalTQSYyxs = $totalTQSYy; echo number_format($totalTQSYyxs);?></td>
                    <td><?php echo (round(($totalTQSYyn-$totalTCSYyn)/$totalTQSYyn, 4)*100).'%';?></td>
                    <td><?php $totaltotalpriceyxs = $totaltotalpricey; echo number_format($totaltotalpriceyxs); ?></td>
                    <td><?php $totalpobuypriceyxs = $totalpobuypricey; echo number_format($totalpobuypriceyxs); ?></td>
                    <td><?php $totalstockpriceyxs = $totalstockpricey; echo number_format($totalstockpriceyxs); ?></td>
                    <td><?php echo round(($totalSQNy/$totalTQLMy)*28, 2);?></td>
                    </tr>
                <?php foreach ($pclass['DM'] as $cost) { ?>
                  <tr>
                    <td >燕燕</td>
                    <td><?php echo $cost['name'] ?></td>
                    <td><?php echo $cost['TPXLND']; $totalTQNDd+= $cost['TPND'];?></td>
                    <td><?php echo $cost['TPXLLD']; $totalTQLDd+= $cost['TPLD'];?></td>
                    
                    <td><?php echo $cost['TPXLLMN']; $totalTQLMNd+= $cost['TPLMN'];$totalTCLMNd+= $cost['TCLMN'];$totalTQLMNdn+= $cost['TPLMN-n'];$totalTCLMNdn+= $cost['TCLMN-n'];?></td>
                    <td><?php echo $cost['GPRLMN']?></td>
                    <td><?php echo $cost['TPXLTY']; $totalTPTYd+= $cost['TPTY'];$totalTCTYd+= $cost['TCTY']; $totalTPTYdn+= $cost['TPTY-n'];$totalTCTYdn+= $cost['TCTY-n'];?></td>
                    <td><?php echo $cost['GPRTY']?></td>
                    <td><?php echo $cost['TPXLSY']; $totalTQSYd+= $cost['TPSY'];$totalTCSYd+= $cost['TCSY'];$totalTQSYdn+= $cost['TPSY-n'];$totalTCSYdn+= $cost['TCSY-n'];?></td>
                    <td><?php echo $cost['GPRSY']?></td>
                    <td><?php echo $cost['totalpricexs']; $totaltotalpriced+= $cost['totalprice']; ?></td>
                    <td><?php echo $cost['pobuypricexs'];$totalpobuypriced+= $cost['pobuyprice']; ?></td>
                    <td><?php echo $cost['stockpricexs'];$totalstockpriced+= $cost['stockprice']; ?></td>
                    <td><?php echo $cost['SSC'] ;$totalSQNd+= $cost['availableQty'];$totalTQLMd+= $cost['TQLM']?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td >合计</td>
                    <td>--</td>
                    <td><?php $totalTQNDdxs = $totalTQNDd;echo  number_format($totalTQNDdxs);?></td>
                    <td><?php $totalTQLDdxs = $totalTQLDd;echo  number_format($totalTQLDdxs);?></td>
                    
                    <td><?php $totalTQLMNdxs = $totalTQLMNd;echo number_format($totalTQLMNd);?></td>
                    <td><?php echo (round(($totalTQLMNdn-$totalTCLMNdn)/$totalTQLMNdn, 4)*100).'%';?></td>
                    <td><?php $totalTPTYdxs = $totalTPTYd;echo number_format($totalTPTYdxs);?></td>
                    <td><?php echo (round(($totalTPTYdn-$totalTCTYdn)/$totalTPTYdn, 4)*100).'%';?></td>
                    <td><?php $totalTQSYdxs = $totalTQSYd;echo number_format($totalTQSYd);?></td>
                    <td><?php echo (round(($totalTQSYdn-$totalTCSYdn)/$totalTQSYdn, 4)*100).'%';?></td>
                    <td><?php $totaltotalpricedxs = $totaltotalpriced;echo number_format($totaltotalpricedxs); ?></td>
                    <td><?php $totalpobuypricedxs = $totalpobuypriced;echo number_format($totalpobuypricedxs); ?></td>
                    <td><?php $totalstockpricedxs = $totalstockpriced;echo number_format($totalstockpricedxs); ?></td>
                    <td><?php echo round(($totalSQNd/$totalTQLMd)*28, 2);?></td>
                    </tr>
                <?php foreach ($pclass['ZDM'] as $cost) { ?>
                  <tr>
                    <td >周东明</td>
                    <td><?php echo $cost['name'] ?></td>
                    <td><?php echo $cost['TPXLND']; $totalTQNDz+= $cost['TPND'];?></td>
                    <td><?php echo $cost['TPXLLD']; $totalTQLDz+= $cost['TPLD'];?></td>
                     
                    <td><?php echo $cost['TPXLLMN']; $totalTQLMNz+= $cost['TPLMN'];$totalTCLMNz+= $cost['TCLMN'];$totalTQLMNzn+= $cost['TPLMN-n'];$totalTCLMNzn+= $cost['TCLMN-n'];?></td>
                    <td><?php echo $cost['GPRLMN']?></td>
                    <td><?php echo $cost['TPXLTY']; $totalTPTYz+= $cost['TPTY'];$totalTCTYz+= $cost['TCTY'];$totalTPTYzn+= $cost['TPTY-n'];$totalTCTYzn+= $cost['TCTY-n'];?></td>
                    <td><?php echo $cost['GPRTY']?></td>
                    <td><?php echo $cost['TPXLSY']; $totalTQSYz+= $cost['TPSY'];$totalTCSYz+= $cost['TCSY'];$totalTQSYzn+= $cost['TPSY-n'];$totalTCSYzn+= $cost['TCSY-n'];?></td>
                    <td><?php echo $cost['GPRSY']?></td>
                    <td><?php echo $cost['totalpricexs']; $totaltotalpricez+= $cost['totalprice']; ?></td>
                    <td><?php echo $cost['pobuypricexs'];$totalpobuypricez+= $cost['pobuyprice']; ?></td>
                    <td><?php echo $cost['stockpricexs'];$totalstockpricez+= $cost['stockprice']; ?></td>
                    <td><?php echo $cost['SSC'];$totalSQNz+= $cost['availableQty'];$totalTQLMz+= $cost['TQLM'] ?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td >合计</td>
                    <td>--</td>
                    <td><?php $totalTQNDzxs = $totalTQNDz;echo number_format($totalTQNDzxs);?></td>
                    <td><?php $totalTQLDzxs = $totalTQLDz;echo number_format($totalTQLDzxs);?></td>
                    
                    <td><?php $totalTQLMNzxs = $totalTQLMNz;echo number_format($totalTQLMNzxs);?></td>
                    <td><?php echo (round(($totalTQLMNzn-$totalTCLMNzn)/$totalTQLMNzn, 4)*100).'%';?></td>
                    <td><?php $totalTPTYzxs = $totalTPTYz; echo  number_format($totalTPTYzxs);?></td>
                    <td><?php echo (round(($totalTPTYzn-$totalTCTYzn)/$totalTPTYzn, 4)*100).'%';?></td>
                    <td><?php $totalTQSYzxs = $totalTQSYz;echo number_format($totalTQSYzxs);?></td>
                    <td><?php echo (round(($totalTQSYzn-$totalTCSYzn)/$totalTQSYzn, 4)*100).'%';?></td>
                    <td><?php $totaltotalpricezxs = $totaltotalpricez; echo number_format($totaltotalpricezxs); ?></td>
                    <td><?php $totalpobuypricezxs = $totalpobuypricez;echo number_format($totalpobuypricezxs); ?></td>
                    <td><?php $totalstockpricezxs = $totalstockpricez;echo number_format($totalstockpricezxs); ?></td>

                    <td><?php echo round(($totalSQNz/$totalTQLMz)*28, 2);?></td>
                    </tr>
                    <?php foreach ($pclass['QT'] as $cost) { ?>
                  <tr>
                    <td >其他</td>
                    <td><?php echo $cost['name'] ?></td>
                    <td><?php echo $cost['TPXLND']; $totalTQNDq+= $cost['TPND'];?></td>
                    <td><?php echo $cost['TPXLLD']; $totalTQLDq+= $cost['TPLD'];?></td>
                    
                    <td><?php echo $cost['TPXLLMN']; $totalTQLMNq+= $cost['TPLMN'];$totalTCLMNq+= $cost['TCLMN'];$totalTQLMNqn+= $cost['TPLMN-n'];$totalTCLMNqn+= $cost['TCLMN-n'];?></td>
                    <td><?php echo $cost['GPRLMN']?></td>
                    <td><?php echo $cost['TPXLTY']; $totalTPTYq+= $cost['TPTY'];$totalTCTYq+= $cost['TCTY'];$totalTPTYqn+= $cost['TPTY-n'];$totalTCTYqn+= $cost['TCTY-n'];?></td>
                    <td><?php echo $cost['GPRTY']?></td>
                    <td><?php echo $cost['TPXLSY']; $totalTQSYq+= $cost['TPSY'];$totalTCSYq+= $cost['TCSY'];$totalTQSYqn+= $cost['TPSY-n'];$totalTCSYqn+= $cost['TCSY-n'];?></td>
                    <td><?php echo $cost['GPRSY']?></td>
                    <td><?php echo $cost['totalpricexs']; $totaltotalpriceq+= $cost['totalprice']; ?></td>
                    <td><?php echo $cost['pobuypricexs'];$totalpobuypriceq+= $cost['pobuyprice']; ?></td>
                    <td><?php echo $cost['stockpricexs'];$totalstockpriceq+= $cost['stockprice']; ?></td>
                    <td><?php echo $cost['SSC'] ?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td >合计</td>
                    <td>--</td>
                    <td><?php $totalTQNDqxs = $totalTQNDq;echo number_format($totalTQNDqxs);?></td>
                    <td><?php $totalTQLDqxs = $totalTQLDq;echo number_format($totalTQLDqxs);?></td>
                    
                    <td><?php $totalTQLMNqxs = $totalTQLMNq;echo number_format($totalTQLMNqxs);?></td>
                    <td><?php echo (round(($totalTQLMNqn-$totalTCLMNqn)/$totalTQLMNqn, 4)*100).'%';?></td>
                    <td><?php $totalTPTYqxs = $totalTPTYq;echo number_format($totalTPTYqxs);?></td>
                    <td><?php echo (round(($totalTPTYqn-$totalTCTYqn)/$totalTPTYqn, 4)*100).'%';?></td>
                    <td><?php $totalTQSYqxs = $totalTQSYq;echo number_format($totalTQSYqxs);?></td>
                    <td><?php echo (round(($totalTQSYqn-$totalTCSYqn)/$totalTQSYqn, 4)*100).'%';?></td>
                    <td><?php $totaltotalpriceqxs = $totaltotalpriceq;echo number_format($totaltotalpriceqxs); ?></td>
                    <td><?php $totalstockpriceqxs = $totalstockpriceq;echo number_format($totalstockpriceqxs); ?></td>

                    <td>--</td>
                </tr>
                <tr>
                    <td >百货栈总计</td>
                    <td>--</td>
                    <td><?php $zrxsn = $totalTQNDy+$totalTQNDd+$totalTQNDz+$totalTQNDq; echo number_format($zrxsn);?></td>
                    <td><?php $zrxs = $totalTQLDy+$totalTQLDd+$totalTQLDz+$totalTQLDq; echo number_format($zrxs);?></td>
                    
                     <td><?php $totalTQLMNt = $totalTQLMNy+$totalTQLMNd+$totalTQLMNz;$totalTCLMNt = $totalTCLMNy+$totalTCLMNd+$totalTCLMNz;
                      $totalTQLMNtn = $totalTQLMNyn+$totalTQLMNdn+$totalTQLMNzn;$totalTCLMNtn = $totalTCLMNyn+$totalTCLMNdn+$totalTCLMNzn;

                      $totalTQLMNtxs = $totalTQLMNt+$totalTQLMNq; echo number_format($totalTQLMNtxs);?></td>
                    <td><?php echo (round(($totalTQLMNtn-$totalTCLMNtn)/$totalTQLMNtn, 4)*100).'%';?></td>
                    <td><?php $totalTPTYt =  $totalTPTYy+$totalTPTYd+$totalTPTYz;$totalTCTYt =  $totalTCTYy+$totalTCTYd+$totalTCTYz ;
                      $totalTPTYtn =  $totalTPTYyn+$totalTPTYdn+$totalTPTYzn;$totalTCTYtn =  $totalTCTYyn+$totalTCTYdn+$totalTCTYzn ;
                      $totalTPTYtxs = $totalTPTYt+$totalTPTYq ;echo number_format($totalTPTYtxs);?></td>
                    <td><?php echo (round(($totalTPTYtn-$totalTCTYtn)/$totalTPTYtn, 4)*100).'%';?></td>
                    <td><?php $totalTQSYt = $totalTQSYy+$totalTQSYd+$totalTQSYz;$totalTCSYt = $totalTCSYy+$totalTCSYd+$totalTCSYz;
                      $totalTQSYtn = $totalTQSYyn+$totalTQSYdn+$totalTQSYzn;$totalTCSYtn = $totalTCSYyn+$totalTCSYdn+$totalTCSYzn;
                      $totalTQSYtxs = $totalTQSYt+$totalTQSYq; echo number_format($totalTQSYtxs);?></td>
                    <td><?php echo (round(($totalTQSYtn-$totalTCSYtn)/$totalTQSYtn, 4)*100).'%';?></td>
                    <td><?php $kcxs = $totaltotalpricey+$totaltotalpriced+$totaltotalpricez+$totaltotalpriceq; echo number_format($kcxs);?></td>
                    <td><?php $dgxs = $totalpobuypricey+$totalpobuypriced+$totalpobuypricez; echo number_format($dgxs);?></td>
                    <td><?php $dgxs = $totalstockpricey+$totalstockpriced+$totalstockpricez; echo number_format($dgxs);?></td>
                    <td><?php $alltotalSQN = $totalSQNy+$totalSQNd+$totalSQNz;$alltotalTQLM = $totalTQLMy+$totalTQLMd+$totalTQLMz; echo round(($alltotalSQN/$alltotalTQLM)*28, 2);?></td>
                    </tr>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="6">没有数据</td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          

          </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 
