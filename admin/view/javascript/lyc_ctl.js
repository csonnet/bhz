'use strict';

angular.module('bhzAdmin')
.controller('lycCtrl', function($scope, $http, $filter,$window){

	/*保存损益单*/
	$scope.savetrim = function(token){
		var invalid = false;

		if($("select[name='warehouse']") && $("select[name='warehouse']") .val() == 0)
		{
			alert('请选择仓库');
			invalid = true;
		}

		if(!$scope.products||$scope.products.length == 0){
			alert('请至少添加一件商品');
			invalid = true;
		}

		angular.forEach($scope.products, function(product){
			if(!product.product_id){
				product.productError='请选择商品';
				invalid = true;
			}

			if(!product.qty){
				product.qtyError='请输入数量';
				invalid = true;
			}
			if(!product.trim_status){
				product.statusError='请选择备注选项';
				invalid = true;
			}
		});

		if(invalid){
			return;
		}

		$http.post('index.php?route=sale/trim_main_stock/save&token='+token, {
			products:$scope.products,
			warehouse_id: $("select[name='warehouse']") .val(),
			status:$scope.status,
			included_order_ids:$scope.included_order_ids
		}).success(function(data){
			$window.location.href = 'index.php?route=sale/trim_main_stock/view&trim_id='+data.info+'&token='+token;
		}).error(function(thrownError) {
			alert(thrownError);
		})
	}
	/*保存损益单*/

	/*修改损益单数量*/
	$scope.change_trim_quantity = function(step, trim_id, token){

		var trim_stock_products = [];
		angular.forEach($scope.trims, function(product,key){
			if(product){
				trim_stock_products.push({trim_product_id:key, stock:product.stock ,
				comment:product.comment});
			}
		});

		var tofunction = '';
		if(step == 1){
			tofunction = "saveLgStock";
		}
		else if(step == 2){
			tofunction = "saveStock";
		}

		$http.post('index.php?route=sale/trim_main_stock/'+tofunction+'&token='+token, {
			products:trim_stock_products,
			trim_id:trim_id,
		 }).success(function(data){
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				if(json['data']){
					angular.forEach($scope.products, function(product, key){
						if(product && json['data'][key] && json['data'][key].length > 0) {
							product.stockError = json['data'][key];
						}
					});
				}
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})

	}
	/*修改损益单数量*/

	/*损益提交审核*/
	$scope.confirm_trim = function(step,trim_id, token){

		var tofunction = "";
		if(step == 1){
			tofunction = "confirmLgStock";
		}
		else if(step == 2){
			tofunction = "confirmStock";
		}

		$http.post('index.php?route=sale/trim_main_stock/'+tofunction+'&token='+token, {
			trim_id:trim_id
		}).success(function(data){
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})
	}
	/*损益提交审核*/

	/*保存调拨单*/
	$scope.saverequisition = function(token){
		console.log(11);
		var invalid = false;

		if($("select[name='out-warehouse']") && $("select[name='out-warehouse']") .val() == 0)
		{
			alert('请选择出库仓库');
			invalid = true;
		}

		if($("select[name='in-warehouse']") && $("select[name='in-warehouse']") .val() == 0)
		{
			alert('请选择入库仓库');
			invalid = true;
		}

		if(!$scope.products||$scope.products.length == 0){
			alert('请至少添加一件商品');
			invalid = true;
		}

		angular.forEach($scope.products, function(product){
			if(!product.product_id){
				product.productError='请选择商品';
				invalid = true;
			}
			// if(product.option_list && !product.option){
			// 	product.optionError='请选择选项';
			// 	invalid = true;
			// }
			if(!product.qty){
				product.qtyError='请输入数量';
				invalid = true;
			}
		});

		if(invalid){
			return;
		}
		// console.log($scope.products);

		$http.post('index.php?route=sale/requisition_main/save&token='+token, {
			products:$scope.products,
			out_warehouse_id: $("select[name='out-warehouse']") .val(),
			in_warehouse_id: $("select[name='in-warehouse']") .val(),
			status:$scope.status,
			included_order_ids:$scope.included_order_ids
		}).success(function(data){
			$window.location.href = 'index.php?route=sale/requisition_main/view&requisition_id='+data.info+'&token='+token;
		}).error(function(thrownError) {
			console.log($scope.products);
		})
	}
	/*保存调拨单*/

	/*修改调拨单数量*/
	$scope.change_requisition_quantity = function(step, requisition_id, token){
		var requisition_product = [];
		angular.forEach($scope.requisitions, function(product,key){
		  if(product) {
			requisition_product.push({requisition_product_id:key, stock:product.stock ,
			  comment:product.comment});
		  }
		});

		var tofunction = '';
		if(step == 1){
			tofunction = "saveLgStock";
		}
		else if(step == 2){
			tofunction = "saveStock";
		}

		$http.post('index.php?route=sale/requisition_main/'+tofunction+'&token='+token, {
		  products:requisition_product,
		  requisition_id:requisition_id,
		}).success(function(data){
		  var json = data;
		  if (json['error']) {
			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			if(json['data']){
			  angular.forEach($scope.products, function(product, key){
				if(product && json['data'][key] && json['data'][key].length > 0) {
				  product.stockError = json['data'][key];
				}
			  });
			}
		  }
		  if (json['success']) {
			$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			$window.location.reload();
		  }
		})
	  }
	/*修改调拨单数量*/

	/*调拨单生成出入库单*/
	$scope.create_requisition_stock = function(requisition_id, token){
		$http.post('index.php?route=sale/requisition_main/createStock&token='+token, {
			requisition_id:requisition_id
		}).success(function(data){
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})
	}
	/*调拨单生成出入库单*/

	/*保存退货单*/
	$scope.savero = function(token){
		var invalid = false;

		if(!$scope.products||$scope.products.length == 0){
			alert('请至少添加一件商品');
			invalid = true;
		}

		angular.forEach($scope.products, function(product){

			$scope.warehouse = $('#return-warehouse').val();
			if($scope.warehouse==0){
			  $scope.warehouseError="请选择入库仓库";
			  invalid = true;
			}
			$scope.market = $('#market').val();
			if($scope.market==""){
			  $scope.marketError="请选择退货超市";
			  invalid = true;
			}
			if(!product.product_id){
				product.productError='请选择商品';
				invalid = true;
			}
			// if(product.option_list && !product.option){
			// 	product.optionError='请选择选项';
			// 	invalid = true;
			// }
			if(!product.qty){
				product.qtyError='请输入数量';
				invalid = true;
			}
		});

		if(invalid){
			return;
		}

		$http.post('index.php?route=sale/return_main_order/save&token='+token, {
			products:$scope.products,
			warehouse_id: $("#return-warehouse") .val(),
			status:$scope.status,
			market:$scope.market,
			included_order_ids:$scope.included_order_ids
		}).success(function(data){
			$window.location.href = 'index.php?route=sale/return_main_order/view&ro_id='+data.info+'&token='+token;
		}).error(function(thrownError) {
			alert(thrownError);
		})
	}
	/*保存退货单*/

	/*修改退货单数量*/
	$scope.change_return_quantity = function(ro_id, token){
		var return_stock_products = [];
		angular.forEach($scope.returns, function(product,key){
			if(product) {
				return_stock_products.push({ro_product_id:key, stock:product.stock});
			}
		});

		$http.post('index.php?route=sale/return_main_order/saveStock&token='+token, {
			products:return_stock_products,
			ro_id:ro_id
		}).success(function(data){
			console.log(return_stock_products);
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				if(json['data']){
					angular.forEach($scope.products, function(product, key){
						if(product && json['data'][key] && json['data'][key].length > 0) {
							product.stockError = json['data'][key];
						}
					});
				}
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})

	  }
	/*修改退货单数量*/

	/*退货提交审核*/
	$scope.return_confirm = function(ro_id, token){
		$http.post('index.php?route=sale/return_main_order/confirmStock&token='+token, {
			ro_id:ro_id
		}).success(function(data){
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})
	}
	/*退货提交审核*/

	/*退货单生成入库单*/
	$scope.create_return_stock = function(ro_id, token){
		$http.post('index.php?route=sale/return_main_order/createStock&token='+token, {
			ro_id:ro_id
		}).success(function(data){
			var json = data;
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$window.location.reload();
			}
		})
	}
	/*退货单生成入库单*/

});
