'use strict';

angular.module('luckydrawAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.directive('datetimez', function() {
  return {
      restrict: 'A',
      require : 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
         format: 'yyyy-mm-dd hh:ii:ss',
         pickDate: true,
         pickTime: true,
         autoclose: true
        }).on('changeDate', function(e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
  };
})
.controller('luckydrawAdminCtrl', function($scope, $http, $filter,$window) {
  if (money_out.money_out_id) {

      $scope.money_typeout = money_out.name;
      $scope.refer_id = money_out.refer_id;
      $scope.payee_id = money_out.payee_id;
      $scope.payee = money_out.payee;
      $scope.payables = money_out.payables;
      $scope.paid = money_out.paid;
      $scope.status = money_out.status;
      $scope.comment = money_out.comment;
  }

  if (money_in.money_in_id) {
      $scope.comment = money_in.comment;
      $scope.payer_id = money_in.payer_id;
      $scope.status = money_in.status;
      $scope.payer_name = money_in.payer_name;
      $scope.receivable = money_in.receivable;
      $scope.received = money_in.received;
      $scope.refer_id = money_in.refer_id;
      $scope.refer_total = money_in.refer_total;
      $scope.money_type = money_in.type_name;
  }else{
      $scope.money_type=1;
  }  
  if(stock_list.in_id) {
      $scope.products = stock_list.product;
  }else{
    $scope.products = [];
  }
  if(stock_list.in_id) {
    $scope.stocks = [];
  }else{
    $scope.stocks = [];
  }

  $scope.outstatus=1;
  $scope.status=1;
  $scope.err_msg = false;
  $scope.remove = function(index){
    $scope.products.splice(index, 1);
  }
  $scope.removeNumber = function(index){
    $scope.numbers.splice(index, 1);
  }
  $scope.removestock = function(index){
    $scope.stocks.splice(index, 1);
  }

  $scope.removestockout = function(index){
    $scope.stock_out.splice(index, 1);
  }
  
  $scope.save_money = function(token){
    if(!$scope.money_type){
      alert('请选择相关单据类型');
      return false;
    }
    $scope.refer_id = $('input[name=\'refer_id\']').val();
    $scope.payer_name = $('input[name=\'payer_name\']').val();
    $scope.customer_id = $('input[name=\'customer_id\']').val();
    $scope.receivable = $('input[name=\'receivable\']').val();
    $scope.ori_total = $('input[name=\'ori_total\']').val();
    if(!$scope.status){
      alert('请选择状态');
      return false;

    }
    $http.post('index.php?route=stock/money_in/save&token='+token, {
      money_type:$scope.money_type,
      refer_id:$scope.refer_id,
      customer_id:$scope.customer_id,
       payer_name:$scope.payer_name,
       receivable:$scope.receivable,
       received:$scope.received,
       ori_total:$scope.ori_total,
       status:$scope.status,
       comment:$scope.comment,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/money_in'+'&token='+token;  
      }
    })
  }

$scope.edit_in = function(token,money_in_id){

    if(!$scope.addreceived){
      alert('请输入增加实收金额');
      return false;

    }

    $http.post('index.php?route=stock/money_in/edit_in&token='+token+'&money_in_id='+money_in_id, {
      //  money_type:$scope.money_type,
      // refer_id:$scope.refer_id,
      // customer_id:$scope.customer_id,
      //  payer_name:$scope.payer_name,
       receivable:$scope.receivable,
       received:$scope.received,
       addreceived:$scope.addreceived,
       // status:$scope.status,
       comment:$scope.comment,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        window.location.href = 'index.php?route=stock/money_in'+'&token='+token;  
      }
    })
  }

  $scope.finish_stock = function(token,money_in_id){
    $http.post('index.php?route=stock/money_in/finish_stock&token='+token+'&money_in_id='+money_in_id, {
      
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/money_in'+'&token='+token;  
      }
    })
  }


  $scope.save_out = function(token){
       if(!$scope.money_type){
        alert('请选择相关单据类型');
        return false;

      }
      $scope.refer_id = $('input[name=\'refer_id\']').val();
      $scope.payee = $('input[name=\'payee\']').val();
      $scope.payee_id = $('input[name=\'payee_id\']').val();
      $scope.payables = $('input[name=\'payables\']').val();
      $scope.refer_total = $('input[name=\'refer_total\']').val();
      if(!$scope.refer_id){
        alert('请选择相关单据号');
        return false;

      }
      if(!$scope.status){
        alert('请选择状态');
        return false;

      }
      $http.post('index.php?route=stock/money_out/save&token='+token, {
         refer_type_id:$scope.money_type,
         refer_id:$scope.refer_id,
          payee_id:$scope.payee_id,
          refer_total:$scope.refer_total,
         payee:$scope.payee,
         payables:$scope.payables,
         paid:$scope.paid,
         status:$scope.status,
         comment:$scope.comment,
      }).success(function(data){
        if(!data.success) {
          $scope.err_msg = data.error;
        } else {
           $window.location.href = 'index.php?route=stock/money_out'+'&token='+token;  
        }
      })
    }


    $scope.save_refund = function(token){
      $scope.refer_id = $('input[name=\'refer_id\']').val();
      $scope.payee = $('input[name=\'payee\']').val();
      $scope.payee_id = $('input[name=\'payee_id\']').val();
      if(!$scope.refer_id){
        alert('请选择相关订单号');
        return false;

      }
      if(!$scope.payables){
        alert('请输入应付总额');
        return false;

      }
      $http.post('index.php?route=stock/refund/save&token='+token, {
          refer_id:$scope.refer_id,
          payee_id:$scope.payee_id,
          payee:$scope.payee,
          payables:$scope.payables,
           comment:$scope.comment,
      }).success(function(data){
        if(!data.success) {
          $scope.err_msg = data.error;
        } else {
           $window.location.href = 'index.php?route=stock/refund'+'&token='+token;  
        }
      })
    }



    $scope.edit_out = function(token,money_out_id){
       if(!$scope.paidadd){
        alert('请输入增加实收总额');
        return false;

      }
      $http.post('index.php?route=stock/money_out/edit_out&token='+token+'&money_out_id='+money_out_id, {
         paidadd:$scope.paidadd,
         comment:$scope.comment,
      }).success(function(data){
        if(!data.success) {
          $scope.err_msg = data.error;
        } else {
           $window.location.href = 'index.php?route=stock/money_out'+'&token='+token;  
        }
      })
    }
  $scope.save_luckydraw = function(token){

    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_quantity<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/stock_in/save&token='+token, {
      refer_id:$scope.refer_id,
      refer_type:$scope.refer_type,
      warehouse:$scope.warehouse,
      comment:$scope.comment,
      status:$scope.status,
      products:$scope.products
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }

  $scope.save_edit = function(token){

    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_price<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }

    $http.post('index.php?route=stock/stock_in/savedit&token='+token, {
      in_id:stock_list.in_id,
      products:$scope.products
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }

  $scope.getProduct = function(value, token){
    return $http.get('index.php?route=stock/stock_in/products&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchProducts = data;
    });
  }


  $scope.selectProduct = function(p,product){
    product.product_code =  p.product_code;
    product.name = p.name ;
    product.sku = p.sku ;
    product.oname = p.oname ;
    product.product_price = p.product_price ;
  }
  $scope.save_stock = function(token,warehouse_id,refer_type){
        $('.complete').attr('disabled',"true");
     var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.product_code){
        product.priceNameError='请输入商品编码';
        invalid = true;
      }
      if(!product.product_quantity|| isNaN(product.product_quantity)||(product.product_quantity<0)){
        product.productNameError='输入入库数量不对';
        invalid = true;
      }
      if(!product.product_price || isNaN(product.product_price)||(product.product_price<0)){
        product.sortOrderError='输入销售单价不对';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/stock_in/savestock&token='+token+'&warehouse_id='+warehouse_id+'&refer_type_id='+refer_type, {
      in_id:stock_list.in_id,
      products:$scope.products,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/stock_in'+'&token='+token;  
      }
    })
  }
});