'use strict';

angular.module('luckydrawAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.directive('datetimez', function() {
  return {
      restrict: 'A',
      require : 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
         format: 'yyyy-mm-dd hh:ii:ss',
         pickDate: true,
         pickTime: true,
         autoclose: true
        }).on('changeDate', function(e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
  };
})
.controller('luckydrawAdminCtrl', function($scope, $http, $filter,$window) {
  $scope.add_acount = function(token){

    $scope.customer_id = $('input[name=\'customer_id\']').val();
    // alert($scope.customer_id);
    if(!$scope.customer_id){
      alert('手机号或超市名称');
      return false;

    }
    $http.post('index.php?route=stock/customer_account/save&token='+token, {
      customer_id:$scope.customer_id,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=stock/customer_account'+'&token='+token;  
      }
    })
  }

  $scope.confirm_action = function(token){
    // alert(11);
    $('#confirm_action').attr('disabled',"true");
    // alert(11);
    $http.post('index.php?route=stock/customer_account/sendConfirmSms&token='+token).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        alert(data.success);
        $window.location.href = 'index.php?route=stock/customer_account/view'+'&token='+token+'&templateId=91553507';  
      }
    })
  }


});