'use strict';

angular.module('bhzAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.constant('cfg', {
  baseUrl:'/~kirk/bhz/admin/index.php?route='
})
.controller('bhzAdminCtrl', function($scope, $http, cfg, $filter,$window) {
  $scope.status = 0;
  if(po_products){
      $scope.products = po_products;
  }else{
    $scope.products = [];
  }
  // console.log($scope.products);
  $scope.getProduct = function(value, token){
    if(!$scope.vendor || !$scope.vendor.vendor_id){
      var vendor_id = '';
    }else{
      var vendor_id = $scope.vendor.vendor_id;
    }

    return $http.get('index.php?route=sale/purchase_order/product&filter_name='+value+'&filter_vendor='+vendor_id+'&token='+token)
    .success(function(data){
       $scope.searchProducts = data;
    });
  }

  $scope.remove = function(index){
    $scope.products.splice(index, 1);
  }

  $scope.removeproduct = function(index){
    $scope.products.splice(index, 1);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.total);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }

  $scope.removeproductano = function(index){
    $scope.products.splice(index, 1);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.products_money);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }
  
  $scope.getVendor = function(value, token){
    return $http.get('index.php?route=sale/purchase_order/vendor&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchVendors = data;
    });
  }

  $scope.getLogcenter = function(value, token){
    return $http.get('index.php?route=sale/lgi_order/logcenter&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchLogcenters = data;
    });
  }
  $scope.selectProduct = function(p,product){
    product.product_id =  p.product_id;
    product.name =  p.name;
    product.sku =  p.sku;
    product.model =  p.model;
    product.packing_no = p.packing_no;
    product.option_name = p.option_name;
    product.option_id = p.option_id;
    product.product_code = p.product_code;
   
    product.unit_price =  parseFloat(p.cost);
  }
  $scope.change_price = function(product, option){
    if(option.price_prefix == '+'){
      product.unit_price = product.cost + parseFloat(option.price);
    }
    else{
      product.unit_price = product.cost - parseFloat(option.price);
    }
  }
  $scope.selectVendor = function(vendor){
    $scope.vendor = vendor;
  }

  $scope.selectLogcenter = function(logcenter){

    $scope.logcenter = logcenter;
  }
  $scope.confirm_po = function(po_id, token,warehouse_id,status){
    var invalid = false;

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }
      if(!product.qty || product.qty<=0){
        product.qtyError='请输入数量';
        invalid = true;
      }
      if(!product.unit_price || product.unit_price<=0){
        product.priceError='请输入价格';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/purchase_order/confirm_po&token='+token, {
      po_id:po_id,
      products:$scope.products,
      warehouse_id:warehouse_id,
      status:status,
    }).success(function(data){
         $window.location.href = 'index.php?route=sale/purchase_order&po_id='+data.info+'&token='+token;
    })
  }

  

});