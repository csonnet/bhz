'use strict';

angular.module('luckydrawAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.directive('datetimez', function() {
  return {
      restrict: 'A',
      require : 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
         format: 'yyyy-mm-dd hh:ii:ss',
         pickDate: true,
         pickTime: true,
         autoclose: true
        }).on('changeDate', function(e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
  };
})
.controller('luckydrawAdminCtrl', function($scope, $http, $filter,$window) {
  if(luckydraw_info.luckydraw_id) {
    $scope.products = luckydraw_info.products;
    $scope.numbers = luckydraw_info.numbers;
    $scope.luckydraw_name = luckydraw_info.luckydraw_name;
    $scope.chance = luckydraw_info.chance;
    $scope.start_time = luckydraw_info.start_time;
    $scope.end_time = luckydraw_info.end_time;
    $scope.status = luckydraw_info.status;  
    $scope.luckydraw_id = luckydraw_info.luckydraw_id;  
  } else {
    $scope.products = [];
    $scope.numbers = [];
    $scope.status = 0;  
    $scope.luckydraw_id = 0;
  }
  $scope.is_edit = is_edit;
  $scope.err_msg = false;

  $scope.remove = function(index){
    $scope.products.splice(index, 1);
  }
  $scope.removeNumber = function(index){
    $scope.numbers.splice(index, 1);
  }

  $scope.save_luckydraw = function(token){
    var invalid = false;
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件奖品');
      invalid = true;
      return;
    }
    if(!$scope.numbers||$scope.numbers.length == 0){
      alert('请至少添加一个抽奖次数');
      invalid = true;
      return;
    }
    angular.forEach($scope.products, function(product){
      if(!product.price_name){
        product.priceNameError='请输入奖项名';
        invalid = true;
      }
      if(!product.product_name){
        product.productNameError='请输入奖品名';
        invalid = true;
      }
      if(!product.sort_order || isNaN(product.sort_order)){
        product.sortOrderError='请输入整数排序';
        invalid = true;
      }
      if(!product.quantity || isNaN(product.quantity)){
        product.quantityError='请输入奖品总数';
        invalid = true;
      }
    });
    angular.forEach($scope.numbers, function(number){
      if(!number.amount || isNaN(number.amount)){
        number.amountError='请输入数字订单金额';
        invalid = true;
      }
      if(!number.number || isNaN(number.number)){
        number.numberError='请输入数字抽奖次数';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/luckydraw/save&token='+token, {
      luckydraw_name:$scope.luckydraw_name,
      chance:$scope.chance,
      status:$scope.status,
      start_time:$scope.start_time,
      end_time:$scope.end_time,
      sort_order:$scope.sort_order,
      products:$scope.products,
      numbers:$scope.numbers,
      is_edit:$scope.is_edit,
      luckydraw_id:$scope.luckydraw_id,
    }).success(function(data){
      if(!data.success) {
        $scope.err_msg = data.error;
      } else {
        $window.location.href = 'index.php?route=sale/luckydraw'+'&token='+token;  
      }
    })
  }

});