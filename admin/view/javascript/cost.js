'use strict';

angular.module('luckydrawAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.directive('datetimez', function() {
  return {
      restrict: 'A',
      require : 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
         format: 'yyyy-mm-dd hh:ii:ss',
         pickDate: true,
         pickTime: true,
         autoclose: true
        }).on('changeDate', function(e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
  };
})
.controller('luckydrawAdminCtrl', function($scope, $http, $filter,$window) {
  if (cost_list.cost_id) {
      $scope.cost_id = cost_list.cost_id;
      $scope.user_id = cost_list.user_id;
      $scope.date_added = cost_list.date_added;
      $scope.is_return = cost_list.is_return;
      $scope.cost_name = cost_list.cost_name;
      $scope.cost_status_id = cost_list.cost_status_id;
      $scope.cost_use_type_id = cost_list.cost_use_type_id;
      $scope.comment = cost_list.comment;
      $scope.days = cost_list.days;
      $scope.end_date = cost_list.end_date;
      $scope.start_date = cost_list.start_date;
      $scope.return_date = cost_list.return_date;
      $scope.cost_type_id = cost_list.cost_type_id;
      $scope.cost = cost_list.cost;
      $scope.auditors= cost_list.auditor;
  }else{
    $scope.auditors = [];
    $scope.cost_use_type_id = 1;
    $scope.is_return = 0;
  }
 
  $scope.getauditor = function(value, token){
    return $http.get('index.php?route=stock/cost/cost_use&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchauditors = data;
    });
  }

   $scope.selectauditor = function(p,auditor){
    auditor.user_id =  p.user_id;
    auditor.fullname = p.fullname ;
    auditor.contact_tel = p.contact_tel ;
  }
  $scope.remove = function(index){
    $scope.auditors.splice(index, 1);
  }


  $scope.save_cost = function(token){

    var invalid = false;
    if(!$scope.auditors||$scope.auditors.length == 0){
      alert('请至少添加一个审批人');
      invalid = true;
      return;
    }
    if(!$scope.cost_use_type_id){
        alert('请选择报销人类型');
        invalid = true;
        return;

    }
    var days = $('#days').val();
    if($scope.cost_use_type_id==1){
      if(!days){
          alert('请选择报销月份');
          invalid = true;
          return;

      }
    }

    if(!$scope.cost_type_id){
      alert('请选择报销类型');
      invalid = true;
      return;
    }
    if(!$scope.cost){
      alert('请填写费用金额');
      invalid = true;
      return;
    }

    var return_date = $('#return_date').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();

    if($scope.is_return==1){
      if(!return_date){
          alert('请选择归还日期');
          invalid = true;
          return;

      }
    }
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/cost/save_cost&token='+token, {
      cost_use_type_id:$scope.cost_use_type_id,
      cost_type_id:$scope.cost_type_id,
      return_date:return_date,
      cost:$scope.cost,
      comment:$scope.comment,
      is_return:$scope.is_return,
      days:days,
      start_date:start_date,
      end_date:end_date,
      auditors:$scope.auditors
    }).success(function(data){

        $window.location.href = 'index.php?route=stock/cost'+'&token='+token;  
    })
  }
  $scope.edit_cost = function(token,cost_id){

    var invalid = false;
    if(!$scope.auditors||$scope.auditors.length == 0){
      alert('请至少添加一个审批人');
      invalid = true;
      return;
    }
    if(!$scope.cost_use_type_id){
        alert('请选择报销人类型');
        invalid = true;
        return;

    }
    var days = $('#days').val();
    if($scope.cost_use_type_id==1){
      if(!days){
          alert('请选择报销月份');
          invalid = true;
          return;

      }
    }

    if(!$scope.cost_type_id){
      alert('请选择报销类型');
      invalid = true;
      return;
    }
    if(!$scope.cost){
      alert('请填写费用金额');
      invalid = true;
      return;
    }

    var return_date = $('#return_date').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();

    if($scope.is_return==1){
      if(!return_date){
          alert('请选择归还日期');
          invalid = true;
          return;

      }
    }
    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/cost/edit_cost&token='+token+'&cost_id='+cost_id, {
      cost_use_type_id:$scope.cost_use_type_id,
      cost_type_id:$scope.cost_type_id,
      return_date:return_date,
      cost:$scope.cost,
      comment:$scope.comment,
      is_return:$scope.is_return,
      days:days,
      start_date:start_date,
      end_date:end_date,
      auditors:$scope.auditors
    }).success(function(data){

        $window.location.href = 'index.php?route=stock/cost'+'&token='+token;   
        
    })
  }
});
