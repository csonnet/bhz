'use strict';

angular.module('bhzAdmin', [])
.directive('hrefVoid', [ function() {
  return {
    restrict : 'A',
    link : function(scope, iElement, iAttrs) {
      iElement.attr('href', 'javascript:void(0);');
      iElement.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
    }
  };
} ])
.constant('cfg', {
  baseUrl:'/~kirk/bhz/admin/index.php?route='
})
.controller('bhzAdminCtrl', function($scope, $http, cfg, $filter,$window) {
  $scope.products = [];
  $scope.signmans = [];
  $scope.status = 0;
  $scope.warehouse_id=4;

  $scope.getProduct = function(value, token){
    if(!$scope.vendor || !$scope.vendor.vendor_id){
      var vendor_id = '';
    }else{
      var vendor_id = $scope.vendor.vendor_id;
    }

    return $http.get('index.php?route=sale/purchase_order/product&filter_name='+value+'&filter_vendor='+vendor_id+'&token='+token)
    .success(function(data){
       $scope.searchProducts = data;
    });
  }

  $scope.remove = function(index){
    $scope.products.splice(index, 1);
  }

  $scope.removeproduct = function(index){

    $scope.products.splice(index, 1);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.total);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }

  $scope.removeproductano = function(index){
    $scope.products.splice(index, 1);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.products_money);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }

  $scope.getVendor = function(value, token){
    return $http.get('index.php?route=sale/purchase_order/vendor&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchVendors = data;
    });
  }

  $scope.getLogcenter = function(value, token){
    return $http.get('index.php?route=sale/lgi_order/logcenter&filter_name='+value+'&token='+token)
    .success(function(data){
       $scope.searchLogcenters = data;
    });
  }
  $scope.selectProduct = function(p,product){
    var po_add = $('#po_add').val();
    if (p.clearance==1&&po_add==1) {
      alert('清仓商品不允许采购');
    }else{
    product.product_id =  p.product_id;
    product.name =  p.name;
    product.sku =  p.sku;
    product.model =  p.model;
    product.packing_no = p.packing_no;
    product.product_code = p.product_code;
    if(p.option && p.option.length>0){
       product.option_list = p.option[0]['product_option_value'][0]['name'];
       product.product_option_value_id = p.option[0]['product_option_value'][0]['product_option_value_id'];
    }

    product.unit_price =  parseFloat(p.cost);
    product.cost =  parseFloat(p.cost);
    }
  }
  $scope.change_price = function(product, option){
    if(option.price_prefix == '+'){
      product.unit_price = product.cost + parseFloat(option.price);
    }
    else{
      product.unit_price = product.cost - parseFloat(option.price);
    }
  }
  $scope.selectVendor = function(vendor){
    $scope.vendor = vendor;
  }

  $scope.selectLogcenter = function(logcenter){

    $scope.logcenter = logcenter;
  }
  $scope.getTotalPrice = function(){
    var res=0;
    angular.forEach($scope.products, function(p){
      var price = p.unit_price* p.qty;
      if(!isNaN(price)){
        res += p.unit_price* p.qty;
      }
    });
    return res;
  }

  $scope.save_po = function(token){
    var invalid = false;
    if(!$scope.vendor||!$scope.vendor.vendor_id){
      invalid = true;
      $scope.vendorError="请选择供应商";
    }
    $scope.warehouse_id = $('#warehouse_id').val();
    if(!$scope.warehouse_id){
      invalid = true;
      $scope.warehouseError="请选择入库仓库";
    }
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件商品');
      invalid = true;
    }

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }

      if(!product.qty || product.qty<=0){
        product.qtyError='请输入数量';
        invalid = true;
      }
      if(!product.unit_price || product.unit_price<=0){
        product.priceError='请输入价格';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/purchase_order/save&token='+token, {
      vendor:$scope.vendor,
      products:$scope.products,
      deliver_time:$scope.deliver_time,
      warehouse_id:$scope.warehouse_id,
      status:$scope.status,
      included_order_ids:$scope.included_order_ids
    }).success(function(data){
        $window.location.href = 'index.php?route=sale/purchase_order/view&po_id='+data.info+'&token='+token;
    })
  }

  $scope.save_requisition = function(token){
    var invalid = false;

    $scope.deliver_time = $('.date input').val();

    if(!$scope.logcenter){
      invalid = true;
      $scope.logcenterNameError="请选择目的物流中心";
    }
    if(!$scope.deliver_time){
      invalid = true;
      $scope.deliverTimeError="请选择发货时间";
    }
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件商品');
      invalid = true;
    }

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }
      if(product.option_list && !product.option){
        product.optionError='请选择选项';
        invalid = true;
      }
      if(!product.qty || product.qty<=0){
        product.qtyError='请输入数量';
        invalid = true;
      }
      if(!product.unit_price || product.unit_price<=0){
        product.priceError='请输入价格';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/requisition_logcenter/save&token='+token, {
      vendor:$scope.vendor,
      products:$scope.products,
      deliver_time:$scope.deliver_time,
      status:$scope.status,
      logcenter:$scope.logcenter,
      included_order_ids:$scope.included_order_ids
    }).success(function(data){
       $window.location.href = 'index.php?route=sale/requisition_logcenter/view&requisition_id='+data.info+'&token='+token;
    })
  }

  $scope.save_ro = function(token){
    var invalid = false;
    // if(!$scope.vendor||!$scope.vendor.vendor_id){
    //   invalid = true;
    //   $scope.vendorError="请选择供应商";
    // }
    $scope.deliver_time = $('.date input').val();
    if(!$scope.deliver_time){
      invalid = true;
      $scope.deliverTimeError="请选择发货时间";
    }
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件商品');
      invalid = true;
    }

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }
      if(product.option_list && !product.option){
        product.optionError='请选择选项';
        invalid = true;
      }
      if(!product.qty || product.qty<=0){
        product.qtyError='请输入数量';
        invalid = true;
      }
      if(!product.unit_price || product.unit_price<=0){
        product.priceError='请输入价格';
        invalid = true;
      }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/return_logcenter_order/save&token='+token, {
      products:$scope.products,
      deliver_time:$scope.deliver_time,
      status:$scope.status,
      included_order_ids:$scope.included_order_ids
    }).success(function(data){
       $window.location.href = 'index.php?route=sale/return_logcenter_order/view&ro_id='+data.info+'&token='+token;
    })
  }

  $scope.save_stock = function(po_id, token){
    var stock_products = [];
    angular.forEach($scope.products, function(product,key){
      if(product) {
        stock_products.push({po_product_id:key, stock:product.stock});
      }
    });

    $http.post('index.php?route=sale/purchase_order/saveStock&token='+token, {
      products:stock_products,
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }


$scope.save_stocka = function(po_id, token){
    var stock_products = [];
    angular.forEach($scope.products, function(product,key){
      if(product) {
        stock_products.push({po_product_id:key, stock:product.stock});
      }
    });

    $http.post('index.php?route=sale/main_purchase_order/saveStock&token='+token, {
      products:stock_products,
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        alert('入库成功');
        $window.location.reload();
      }
    })
  }
  $scope.save_trim = function(token){
    var invalid = false;
    // if(!$scope.vendor||!$scope.vendor.vendor_id){
    //   invalid = true;
    //   $scope.vendorError="请选择供应商";
    // }
    $scope.deliver_time = $('.date input').val();
    if(!$scope.deliver_time){
      invalid = true;
      $scope.deliverTimeError="请选择发货时间";
    }
    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件商品');
      invalid = true;
    }

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }
      if(product.option_list && !product.option){
        product.optionError='请选择选项';
        invalid = true;
      }
      if(!product.qty){
        product.qtyError='请输入数量';
        invalid = true;
      }
      if(!product.unit_price || product.unit_price<=0){
        product.priceError='请输入价格';
        invalid = true;
      }
      if(!product.trim_status){
        product.statusError='请选择调整原因';
        invalid = true;
      }
      // if(!product.comment){
      //   product.commentError='请输入备注';
      //   invalid = true;
      // }
    });
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/trim_logcenter_stock/save&token='+token, {
      products:$scope.products,
      deliver_time:$scope.deliver_time,
      status:$scope.status,
      included_order_ids:$scope.included_order_ids
    }).success(function(data){
       $window.location.href = 'index.php?route=sale/trim_logcenter_stock/view&trim_id='+data.info+'&token='+token;
    })
  }

    $scope.return_save_stock = function(ro_id, token){
    var return_stock_products = [];
    angular.forEach($scope.returns, function(product,key){
      if(product) {
        return_stock_products.push({ro_product_id:key, stock:product.stock});
      }
    });

    $http.post('index.php?route=sale/return_main_order/saveStock&token='+token, {
      products:return_stock_products,
      ro_id:ro_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.trim_logcenter_stock = function(trim_id, token){
    var trim_stock_products = [];
    angular.forEach($scope.trims, function(product,key){
      if(product) {
        trim_stock_products.push({trim_product_id:key, stock:product.stock ,
          comment:product.comment});
      }
    });

    $http.post('index.php?route=sale/trim_logcenter_stock/saveStock&token='+token, {
      products:trim_stock_products,
      trim_id:trim_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.requisition_main_stock = function(requisition_id, token){
    var requisition_product = [];
    angular.forEach($scope.requisitions, function(product,key){
      if(product) {
        requisition_product.push({requisition_product_id:key, stock:product.stock ,
          comment:product.comment});
      }
    });

    $http.post('index.php?route=sale/requisition_main/saveStock&token='+token, {
      products:requisition_product,
      requisition_id:requisition_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.trim_main_stock = function(trim_id, token){
    var trim_stock_products = [];
    angular.forEach($scope.trims, function(product,key){
      if(product) {
        trim_stock_products.push({trim_product_id:key, stock:product.stock ,
          comment:product.comment});
      }
    });

    $http.post('index.php?route=sale/trim_main_stock/saveStock&token='+token, {
      products:trim_stock_products,
      trim_id:trim_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        if(json['data']){
          angular.forEach($scope.products, function(product, key){
            if(product && json['data'][key] && json['data'][key].length > 0) {
              product.stockError = json['data'][key];
            }
          });
        }
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.return_confirm_stock = function(ro_id, token){
    $http.post('index.php?route=sale/return_logcenter_order/confirmStock&token='+token, {
      ro_id:ro_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.requisition_logcenter_confirm = function(requisition_id, token){
    $http.post('index.php?route=sale/requisition_logcenter/confirmStock&token='+token, {
      requisition_id:requisition_id
    }).success(function(data){

      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.requisition_logcenter_accept = function(requisition_id, token){
    $http.post('index.php?route=sale/requisition_logcenter/acceptStock&token='+token, {
      requisition_id:requisition_id
    }).success(function(data){

      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.trim_logcenter_confirm_stock = function(trim_id, token){
    $http.post('index.php?route=sale/trim_logcenter_stock/confirmStock&token='+token, {
      trim_id:trim_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.trim_main_confirm_stock = function(trim_id, token){
    $http.post('index.php?route=sale/trim_main_stock/confirmStock&token='+token, {
      trim_id:trim_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.requisition_main_confirm = function(requisition_id, token){
    $http.post('index.php?route=sale/requisition_main/confirmStock&token='+token, {
      requisition_id:requisition_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.return_finsh_stock = function(ro_id, token){
    $http.post('index.php?route=sale/return_main_order/finshStock&token='+token, {
      ro_id:ro_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.allow_stock = function(po_id, token){
    $http.post('index.php?route=sale/main_purchase_order/allowStock&token='+token, {
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.wuliu_allow_stock = function(po_id, token){
    $http.post('index.php?route=sale/purchase_order/allowStock&token='+token, {
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }


  $scope.vendorChangePoStatus = function(po_id, token){
    var r=confirm("确定要修改采购单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/vdi_purchase_order/vendorChangePoStatus&token='+token, {
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.setLgBillDiff = function(lg_bill_id, token){
    $http.post('index.php?route=sale/logcenter_bill/setLogcenterBillDiff&token='+token, {
      logcenter_bill_id:lg_bill_id,
      difference:$scope.difference
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.chgLgBillStatus = function(lg_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/logcenter_bill/changeLogcenterBillStatus&token='+token, {
      logcenter_bill_id:lg_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.chgLgiLgBillStatus = function(lg_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/lgi_logcenter_bill/changeLogcenterBillStatus&token='+token, {
      logcenter_bill_id:lg_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.setVdBillDiff = function(vd_bill_id, token){
    $http.post('index.php?route=sale/vendor_bill/setVendorBillDiff&token='+token, {
      vendor_bill_id:vd_bill_id,
      difference:$scope.difference
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

$scope.setVdBillDiffNew = function(vd_bill_id, token){
    $http.post('index.php?route=stock/factory/setVendorBillDiff&token='+token, {
      vendor_bill_id:vd_bill_id,
      difference:$scope.difference
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.setVOBillDiffNew = function(distributor_bill_id, token){
    $http.post('index.php?route=stock/distributor/setVendorBillDiff&token='+token, {
      distributor_bill_id:distributor_bill_id,
      difference:$scope.difference
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }
  
  $scope.chgVdBillStatus = function(vd_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/vendor_bill/changeVendorBillStatus&token='+token, {
      vendor_bill_id:vd_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }
  $scope.chgVoBillStatusnew = function(distributor_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=stock/distributor/changeVendorBillStatus&token='+token, {
      distributor_bill_id:distributor_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }
  $scope.chgVdBillStatusnew = function(vd_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=stock/factory/changeVendorBillStatus&token='+token, {
      vendor_bill_id:vd_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.chgVdiVdBillStatus = function(vd_bill_id, token){
    var r=confirm("确定要修改对账单状态？")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/vdi_vendor_bill/changeVendorBillStatus&token='+token, {
      vendor_bill_id:vd_bill_id,
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }

  $scope.forceCompletePo = function(po_id, token){
    var r=confirm("确认要审核完成采购单吗？请仔细确认收货数量！")
    if (r!=true) {return;}

    $http.post('index.php?route=sale/main_purchase_order/forceCompletePo&token='+token, {
      po_id:po_id
    }).success(function(data){
      var json = data;
      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $window.location.reload();
      }
    })
  }
/*
  $scope.getmoney = function(token){
    var r=confirm("确定要打款？")
    if (r!=true) {return;}
    $http.post('index.php?route=sale/vendor_bill/remit&token='+token, {
      sum:$scope.difference,
    }).success(function(data){
      var json = data;
      var a = document.getElementById('gotoyjf');
      a.href = json;
      a.click();
    })
  }
*/
  $scope.getmoney = function(vbId, token){
    var r=confirm("确定要打款？")
    if (r!=true) {return;}
    $http.post('index.php?route=stock/factory/remit&token='+token, {
      'vbId':vbId,'sum':$scope.difference
    }).success(function(ret){
        if (ret.status){
          var a = document.getElementById('gotoyjf');
          a.href = ret.message;
          a.click();
        }else{
          alert(ret.message);
        }
    })
  }
  $scope.getmoneydis = function(vbId, token){
    var r=confirm("确定要打款？")
    if (r!=true) {return;}
    $http.post('index.php?route=stock/distributor/remit&token='+token, {
      'vbId':vbId,'sum':$scope.difference
    }).success(function(ret){
        if (ret.status){
          var a = document.getElementById('gotoyjf');
          a.href = ret.message;
          a.click();
        }else{
          alert(ret.message);
        }
    })
  }

  $scope.getWarehouse = function(value,token){
    return $http.post('index.php?route=sale/stock_out/warehouse&token='+token, {
      filter_name:value,
    }).success(function(data){
       $scope.searchWarehouse = data;
    });
  }

  $scope.selectWarehouse = function(warehouse){
    $scope.warehouse = warehouse;
  }

  $scope.getProductsByCode = function(value, token){

    console.log(11);
    return $http.post('index.php?route=sale/stock_out/getproducts&token='+token, {
      filter_code:value
    }).success(function(data){
       $scope.searchProducts = data;
    });
  }

  $scope.selectProductSec = function(p,product){
    product.name =  p.pdname;
    product.code =  p.code;
    product.price = p.price
    product.sku = p.sku;
  }

  $scope.selectProductSecano = function(p,product){
    product.name =  p.pdname;
    product.product_code =  p.code;
    product.product_price = p.price;
    product.sku = p.sku;
  }

  $scope.caulatetotal = function(product){
    product.total = (product.num * product.price).toFixed(2);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.total);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }

  $scope.caulatetotalano = function(product){
    product.products_money = (product.product_quantity * product.product_price).toFixed(2);

    var sale_money = 0;

    angular.forEach($scope.products, function(product){
      sale_money += parseFloat(product.products_money);
    })

    $('#sale_money').val(sale_money.toFixed(2));
  }

  $scope.save_stock_out = function(token){
    var invalid = false;
    $scope.refer_id = $('#refer_id').val();
    if(!$scope.refer_id){
      invalid = true;
      $scope.referidError="请填入销售单ID";
    }
    $scope.refer_type = $('#refer_type').val();
    if($('#refer_type').val() == 'empty'){
      invalid = true;
      $scope.ReferTypeError="请选择单据类型";
    }
    $scope.warehouse_id = $('#warehouse').val();
    if($('#warehouse').val() == 'empty'){
      invalid = true;
      $scope.WarehouseError="请选择出货仓库";
    }
    $scope.sale_money = $('#sale_money').val();
    $scope.status = $('#status').val();
    if($('#status').val() == 'empty'){
      invalid = true;
      $scope.StatusError="请选择状态";
    }
    $scope.comment = $('#comment').val();

    angular.forEach($scope.products, function(product){
      if(!product.name){
        product.nameError='请填入商品名称';
        invalid = true;
      }
      if(!product.code){
        product.codeError='请选择商品编码';
        invalid = true;
      }
      if(!product.num || product.num<=0){
        product.numError='请填入商品数量';
        invalid = true;
      }
      if(!product.price){
        product.priceError='请填入商品价格';
        invalid = true;
      }
    });
    if($scope.products.length == 0){
      alert('请添加商品');
      return;
    }
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/stock_out/save&token='+token, {
      refer_id:$scope.refer_id,
      refer_type:$scope.refer_type,
      warehouse_id:$scope.warehouse_id,
      status:$scope.status,
      sale_money:$scope.sale_money,
      comment:$scope.comment,
      products:$scope.products,
    }).success(function(data){
        if(data.error){
          alert(data.error);
        }else{
          $window.location.href = 'index.php?route=sale/stock_out&token='+token;
        }
    })
  }

  $scope.update_stock_out = function(out_id,token){
    var invalid = false;
    $scope.refer_id = $('#refer_id').val();
    if(!$scope.refer_id){
      invalid = true;
      $scope.referidError="请填入销售单ID";
    }
    $scope.refer_type = $('#refer_type').val();
    if($('#refer_type').val() == 'empty'){
      invalid = true;
      $scope.ReferTypeError="请选择单据类型";
    }
    $scope.warehouse_id = $('#warehouse').val();
    if($('#warehouse').val() == 'empty'){
      invalid = true;
      $scope.WarehouseError="请选择出货仓库";
    }
    $scope.sale_money = $('#sale_money').val();
    if(!$scope.sale_money){
      invalid = true;
      $scope.SaleMoneyError="请填入总价";
    }
    $scope.comment = $('#comment').val();

    angular.forEach($scope.products, function(product){
      if(!product.name){
        product.nameError='请填入商品名称';
        invalid = true;
      }
      if(!product.product_code){
        product.codeError='请选择商品编码';
        invalid = true;
      }
      if(!product.product_quantity || product.product_quantity<=0){
        product.numError='请填入商品数量';
        invalid = true;
      }
      if(!product.product_price){
        product.priceError='请填入商品价格';
        invalid = true;
      }
    });
    if($scope.products.length == 0){
      alert('请添加商品');
      return;
    }
    if(invalid){
      return;
    }
    $http.post('index.php?route=sale/stock_out/update&token='+token, {
      out_id:out_id,
      refer_id:$scope.refer_id,
      refer_type:$scope.refer_type,
      warehouse_id:$scope.warehouse_id,
      sale_money:$scope.sale_money,
      comment:$scope.comment,
      products:$scope.products,
    }).success(function(data){
        if(data['success'])
          $window.location.href = 'index.php?route=sale/stock_out&token='+token;
        else
          alert(data['error']);
    })
  }

  $scope.setWarehouse = function(value,token){
    return $http.post('index.php?route=sale/stock_out/warehouse&token='+token, {
      filter_name:value,
    }).success(function(data){
       $scope.warehouse = data[0];
    });
  }

  $scope.getdetailproducts = function(out_id,token){
    $http.get('index.php?route=sale/stock_out/getdetailproducts&out_id='+out_id+'&token='+token)
    .success(function(data){
      $scope.products = data;
    })
  }

  $scope.getlogcenters = function(token,loan_order_id){

    $http.get('index.php?route=stock/lend/getlogcenters&token='+token)
    .success(function(data){
      $scope.logcenter = data;
    })

    if(loan_order_id){
      $http.get('index.php?route=stock/lend/getlogcenterid&token='+token+'&loan_order_id='+loan_order_id)
      .success(function(data){
        $("#logcenter").val(data.logcenter_id);
        $("#lend_logcenter_address").val(data.address_1);
        $("#lend_logcenter_name").val(data.firstname);
        $("#lend_logcenter_tel").val(data.telephone);
      })
    }

  }

  $scope.getlendproducts = function(token,loan_order_id){

    if(loan_order_id){

      $http.get('index.php?route=stock/lend/getlendproducts&token='+token+'&loan_order_id='+loan_order_id)
      .success(function(data){

        $scope.products = data;

      })

    }
  }

  $scope.changelogcenter = function(l){

    $("#lend_logcenter_address").val(l.address_1);
    $("#lend_logcenter_name").val(l.firstname);
    $("#lend_logcenter_tel").val(l.telephone);

  }

  $scope.getallusers = function(token,loan_order_id){

    $http.get('index.php?route=stock/lend/getallusers&token='+token)
    .success(function(data){
      $scope.users = data;
    })

    if(loan_order_id){

      $http.get('index.php?route=stock/lend/getusersbyloanid&token='+token+'&loan_order_id='+loan_order_id)
      .success(function(data){
        var key;

        for(key in data){

          data[key].users = $scope.users;

        }

        $scope.signmans = data;

      })

    }

  }

  $scope.addsignman = function(){

    $scope.signmans.push({qty:1});

    $scope.signmans[$scope.signmans.length-1].users = $scope.users;

    $('select[name=\'signman\']').each(function(key){

      alert($(this).val());

    })

  }
//2018.3.12
  $scope.changeuser = function(signman,token){
      $http.post('index.php?route=stock/lend/truename&token='+token, {
          user_id:signman.user_id
      }).success(function(data){
          if(data['success'])
            $scope.truename=data['success']['success']['fullname'];
          else
            alert(data['error']);
      })
  }

  $scope.removesignman = function(index){

    $scope.signmans.splice(index, 1);

  }

  $scope.save_lend = function(token){

    // console.log($scope.signmans);
    // return;
    var invalid = false;

    $scope.fwarehouse = $('#fwarehouse').val();

    if(!$scope.fwarehouse){

      invalid = true;
      $scope.FwarehouseError = "请选择借货仓";

    }else{
      $scope.FwarehouseError = '';
    }

    $scope.tlogcenter = $('#logcenter').val();

    if(!$scope.tlogcenter){

      invalid = true;
      $scope.TlogcenterError = "请选择营销部";

    }else{
      $scope.TlogcenterError = '';
    }

    $scope.lend_logcenter_address = $('#lend_logcenter_address').val();

    if(!$scope.lend_logcenter_address){

      invalid = true;
      $scope.AddressError = "请输入地址";

    }else{
      $scope.AddressError = '';
    }

    $scope.lend_logcenter_name = $('#lend_logcenter_name').val();

    if(!$scope.lend_logcenter_name){

      invalid = true;
      $scope.NameError = "请输入人名";

    }else{
      $scope.NameError = '';
    }

    $scope.lend_logcenter_tel = $('#lend_logcenter_tel').val();

    if(!$scope.lend_logcenter_tel){

      invalid = true;
      $scope.TelError = "请输入电话";

    }else{
      $scope.TelError = '';
    }

    $scope.comment = $('#comment').text();

    angular.forEach($scope.products, function(product){
      if(!product.name){
        product.nameError='请填入商品名称';
        invalid = true;
      }else{
        product.nameError='';
      }
      if(!product.code){
        product.codeError='请选择商品编码';
        invalid = true;
      }else{
        product.codeError='';
      }
      if(!product.num || product.num<=0){
        product.numError='请填入商品数量';
        invalid = true;
      }else{
        product.numError='';
      }
    });

    if($scope.products.length == 0){
      invalid = true;
      alert('请添加商品');
    }

    angular.forEach($scope.signmans, function(signman){
      if(!signman.user_id){
        signman.IdError='请选择审批人';
        invalid = true;
      }else{
        signman.IdError='';
      }
    });

    if($scope.signmans.length == 0){
      invalid = true;
      alert('请添加审批人');
    }

    if(invalid){
      return;
    }
    $http.post('index.php?route=stock/lend/save&token='+token, {
      fwarehouse:$scope.fwarehouse,
      tlogcenter:$scope.tlogcenter,
      lend_logcenter_address:$scope.lend_logcenter_address,
      lend_logcenter_name:$scope.lend_logcenter_name,
      lend_logcenter_tel:$scope.lend_logcenter_tel,
      comment:$scope.comment,
      products:$scope.products,
      signmans:$scope.signmans
    }).success(function(data){
        if(data['success'])
          $window.location.href = 'index.php?route=stock/lend&token='+token;
        else
          alert(data['error']);
    })

  }

   $scope.save_back = function(token){
    var invalid = false;

    if(!$scope.products||$scope.products.length == 0){
      alert('请至少添加一件商品');
      invalid = true;
    }

    angular.forEach($scope.products, function(product){
      if(!product.product_id){
        product.productError='请选择商品';
        invalid = true;
      }

      if(!product.qty || product.qty<=0){
        product.qtyError='请输入数量';
        invalid = true;
      }

    });
    if(invalid){
      return;
    }
    //$scope.loan_order =angular.element('#loan_order_id').val();
    //console.log($scope.loan_order_id);
    $http.post('index.php?route=stock/lend/repay&token='+token, {
      products:$scope.products,
      warehouse_id:$scope.warehouse_id,
      loan_order_id:angular.element('#loan_order_id').val()
    }).success(function(data){
        if(data['success'])
          console.log(data['success']);
          //$window.location.href = 'index.php?route=stock/lend&token='+token;
        else
          alert(data['error']);
    })
  }

});
