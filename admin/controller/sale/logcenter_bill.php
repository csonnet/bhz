<?php
class ControllerSaleLogcenterBill extends Controller {
  private $error = array();

  public function export(){
    $logcenter_bill_id = I('get.logcenter_bill_id');
    if(!$logcenter_bill_id){
      $logcenter_bill_id = 0;
    }
    $this->load->model('sale/logcenter_bill');
    $logcenter_bill = $this->model_sale_logcenter_bill->getLogcenterBill($logcenter_bill_id);
    $data['logcenter_bill'] = $logcenter_bill;
    if(!$logcenter_bill){
      $this->load->language('error/not_found');
 
      $this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');
 
      $data['text_not_found'] = $this->language->get('text_not_found');
 
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
      );
 

      $data['footer'] = $this->load->controller('common/footer');
 
      $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
    }
    $logcenter_bill_orders_info = $this->model_sale_logcenter_bill->getLogcenterBillOrdersInfo($logcenter_bill_id);
    $logcenter_bill_orders = $this->model_sale_logcenter_bill->getLogcenterBillOrders($logcenter_bill_id);
    $order_num = count($logcenter_bill_orders);
    $order_total = $logcenter_bill['order_total'];

    $this->load->library('PHPExcel/PHPExcel');
    $objReader = PHPExcel_IOFactory::createReader('Excel5');
    $objPHPExcel = $objReader->load(DIR_SYSTEM . "template/logcenter_bill_tpl.xls"); 
    $objPHPExcel->getProperties()->setCreator("Think-tec")
               ->setLastModifiedBy("Think-tec")
               ->setTitle("BHZ物流营销中心对账单")
               ->setSubject("BHZ物流营销中心对账单")
               ->setDescription("BHZ物流营销中心对账单")
               ->setKeywords("BHZ, Think-tec")
               ->setCategory("Think-tec");
    // $objPHPExcel->setActiveSheetIndex(0);
    $objActSheet = $objPHPExcel->getActiveSheet();
    $objActSheet->setCellValue('L3', $order_num);
    $objActSheet->setCellValue('N3', '￥'.$order_total);

    $row_num = 4;
    $styleBorderOutline = array(
      'borders' => array(
        'outline' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
      ),
    );
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(5, count($logcenter_bill_orders_info));
    $count = 0;
    foreach ($logcenter_bill_orders_info as $key => $order_info) {
      $row_num++;
      $objActSheet->setCellValue('A'.$row_num, $row_num-4);
      $objActSheet->setCellValue('B'.$row_num, date('m-d', strtotime($order_info['date_added'])));
      $objActSheet->setCellValue('C'.$row_num, $order_info['order_id']);
      $objActSheet->setCellValue('D'.$row_num, $order_info['shipping_company']);
      $objActSheet->setCellValue('E'.$row_num, date('m-d', strtotime($order_info['date_modified'])));
      $objActSheet->setCellValue('F'.$row_num, '');
      $objActSheet->setCellValue('G'.$row_num, date('m-d', strtotime($order_info['date_modified'])));
      $objActSheet->setCellValue('H'.$row_num, '');
      $objActSheet->setCellValue('I'.$row_num, $order_info['vendor_id']);
      $objActSheet->setCellValue('J'.$row_num, $order_info['vendor_name']);
      $objActSheet->setCellValue('K'.$row_num, $order_info['product_name']);
      $objActSheet->setCellValue('L'.$row_num, $order_info['sku']);
      $objActSheet->setCellValue('M'.$row_num, $order_info['quantity']);
      // $objActSheet->setCellValue('M'.$row_num, $order_info['price']);
      // $objActSheet->setCellValue('N'.$row_num, $order_info['total']);
      // $objActSheet->setCellValue('O'.$row_num, $order_info['comment']);
      $objActSheet->setCellValue('N'.$row_num, $order_info['comment']);
      $objActSheet->mergeCells('N'.$row_num.':'.'N'.$row_num);
    }
    // $objPHPExcel->getActiveSheet()->getStyle('A5'.':'.'O'.$row_num)->applyFromArray($styleBorderOutline);
    
    $objActSheet->setTitle('物流营销中心对账单');


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="LOG_BILL_BHZ_'.date('Y-m-d-H-i',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
  }

  public function index() {

    $this->document->setTitle('物流营销中心对账单');

    $this->load->model('sale/logcenter_bill');

    $this->getList();
  }

  public function view(){

    $this->document->setTitle('查看物流营销中心对账单');

    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');

    $this->load->model('sale/logcenter_bill');
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '物流营销中心对账单',
      'href' => $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.logcenter_bill_id');
    if(!$id){
      $id=0;
    }
    

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $logcenter_bill = $this->model_sale_logcenter_bill->getLogcenterBill($id);
    $data['logcenter_bill'] = $logcenter_bill;
    $logcenter_bill_orders = $this->model_sale_logcenter_bill->getLogcenterBillOrders($id);
    $data['logcenter_bill_orders'] = $logcenter_bill_orders;
    $logcenter_bill_histories = $this->model_sale_logcenter_bill->getLogcenterBillHistories($id);
    $data['logcenter_bill_histories'] = $logcenter_bill_histories;


    
    switch ($logcenter_bill['status']) {
      case '0':
        $data['change_status_text'] = '平台确认对账单';
        break;
      case '2':
        $data['change_status_text'] = '已打款';
        break;
      case '4':
        $data['change_status_text'] = '已完成';
        break;
      default:
        $data['change_status_text'] = '';
        break;
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/logcenter_bill_view.tpl', $data));
  }

  public function save(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/logcenter_bill');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      if(!$data['deliver_time'] || !$data['products']||!$data['vendor']){
        $this->response->setOutput(json_encode(array('success'=>false, 'info'=>'数据不完整')));
      }
      else{
        $po_id = $this->model_sale_logcenter_bill->addPo($data, $this->user->getId());
      }
      $this->response->setOutput(json_encode(array('success'=>true, 'info'=>$po_id)));
      return;
    }
  }

  public function delete() {
    $json = array();

    $this->load->model('sale/logcenter_bill');

    $id = I('get.logcenter_bill_id');
    if(!$id){
      $id=0;
    }
    
    $logcenter_bill = $this->model_sale_logcenter_bill->getLogcenterBill($id);
    if(!$logcenter_bill||$logcenter_bill['status']!=0) {
      $json['error'] = '非新增状态物流营销中心对账单不能删除';
    }
    else {
      $this->model_sale_logcenter_bill->deleteBill($id);
      $json['success'] = '删除成功';
    }

    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  protected function form(){
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '物流营销中心对账单',
      'href' => $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.po_id');
    if(!$id){
      $id=0;
    }
    

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $po = $this->model_sale_logcenter_bill->getPo($id);
    $data['po'] = $po;
    $po_products = $this->model_sale_logcenter_bill->getPoProducts($id);
    $data['po_products'] = $po_products;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/logcenter_bill.tpl', $data));
  }

  

  protected function getList() {
    $page = I('get.page');
    if(!$page){
      $page = 1;
    }
    $filter_logcenter_bill_id = I('get.filter_logcenter_bill_id');
    $filter_logcenter_name = I('get.filter_logcenter_name');
    $filter_year_month = I('get.filter_year_month');
    $filter_total = I('get.filter_total');
    $filter_date_added = I('get.filter_date_added');
    $filter_logcenter_bill_status = I('get.filter_logcenter_bill_status');
    
    if($filter_logcenter_bill_id) {$filter['logcenter_bill_id'] = $filter_logcenter_bill_id;}
    if($filter_logcenter_name) {$filter['logcenter_name'] = array('like', '%'.$filter_logcenter_name.'%');}
    if($filter_year_month) {$filter['DATE_FORMAT(lb.year_month, "%Y-%m")'] = $filter_year_month;}
    if($filter_total) {$filter['total'] = $filter_total;}
    if($filter_date_added) {$filter['date_added'] = $filter_date_added;}
    if($filter_logcenter_bill_status || $filter_logcenter_bill_status==0) {
      $filter['lb.status'] = $filter_logcenter_bill_status;
    }

    $data['filter_logcenter_bill_id'] = $filter_logcenter_bill_id;
    $data['filter_logcenter_name'] = $filter_logcenter_name;
    $data['filter_year_month'] = $filter_year_month;
    $data['filter_total'] = $filter_total;
    $data['filter_date_added'] = $filter_date_added;
    $data['filter_logcenter_bill_status'] = $filter_logcenter_bill_status;

    if($filter_logcenter_bill_status=='') {
      unset($data['filter_logcenter_bill_status']);  
      unset($filter['lb.status']);
    }

    $sort = I('get.sort');
    $order = I('get.order');
    if($order) {
      $order = $order=='DESC'?'ASC':'DESC';
    }

    $sort_data = array(
      'sort' => $sort?$sort:'logcenter_bill_id',
      'order' => $order?$order:'DESC',
    );

    $data['order'] = $sort_data['order'];
    $data['sort'] = $sort_data['sort'];

    $url .= '&order=' . $sort_data['order'];

    $data['sort_logcenter_bill'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.logcenter_bill_id' . $url, 'SSL');
    $data['sort_logcenter_name'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.logcenter_name' . $url, 'SSL');
    $data['sort_status'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.status' . $url, 'SSL');
    $data['sort_year_month'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.year_month' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.date_added' . $url, 'SSL');
    $data['sort_date_modified'] = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&sort=lb.date_modified' . $url, 'SSL');

    
    if($this->user->getLp()){
      $filter['lg.user_id'] = $this->user->getId();
    }
    else if($this->user->getVp()){
      $filter['lg.vendor_id'] = $this->user->getId();
    }
    else{
      
    }

    $logcenter_bill_list =  $this->model_sale_logcenter_bill->getList($page, $filter, $sort_data);
    $bill_total = $this->model_sale_logcenter_bill->getListCount($filter);
    foreach ($logcenter_bill_list as $key => $value) {
      $logcenter_bill_list[$key]['status_name'] = getLogcenterBillStatus()[$value['status']];
    }

    $pagination = new Pagination();
    $pagination->total = $bill_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '物流营销中心对账单',
      'href' => $this->url->link('sale/logcenter_bill', 'token=' . $this->session->data['token'] , 'SSL')
    );

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    $data['logcenter_bill_statuses'] = getLogcenterBillStatus();

    $data['token'] = $this->session->data['token'];
    
    $data['logcenter_bills'] = $logcenter_bill_list;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/logcenter_bill_list.tpl', $data));
  }

  public function setLogcenterBillDiff() {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/logcenter_bill');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      // $id = I('post.po_id');
      $id = $data['logcenter_bill_id'];
      if(!$id){
        $id=0;
      }
      $logcenter_bill = $this->model_sale_logcenter_bill->getLogcenterBill($id);
      //判断logcenter_bill状态，如果是物流中心确认之后的状态则不允许在添加差额
      if($logcenter_bill['status'] > 1) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改物流中心确认之后的差价')));
        return;
      }

      $difference = $data['difference'];
      if($difference) {
        $difference = (float)$difference;
        $this->model_sale_logcenter_bill->changeDifference($id, $difference);
        $this->response->setOutput(json_encode(array('success'=>'修改对账单差价状态成功')));

        //添加品牌商对账单操作记录
        $this->load->model('user/user');
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $comment = '修改对账单差价为：'.$difference;
        $this->model_sale_logcenter_bill->addLogcenterBillHistory($id, $this->user->getId(), $user_info['fullname'], $comment);
      } else {
        $this->response->setOutput(json_encode(array('error'=>'差价不能为空')));
      }
      return;

      $this->response->setOutput(json_encode(array('success'=>'修改采购单状态成功')));
      return;
    } 
  }

  public function changeLogcenterBillStatus() {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/logcenter_bill');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      // $id = I('post.po_id');
      $id = $data['logcenter_bill_id'];
      if(!$id){
        $id=0;
      }
      $logcenter_bill = $this->model_sale_logcenter_bill->getLogcenterBill($id);

      switch ($logcenter_bill['status']) {
        case '0':
          $target_status = 1;
          break;
        case '2':
          $target_status = 3;
          break;
        case '4':
          $target_status = 5;
          break;
        default:
          $target_status = -1;
          break;
      }
      if($target_status == -1) {
        $this->response->setOutput(json_encode(array('error'=>'修改对账单状态失败')));
      } else {
        $this->model_sale_logcenter_bill->changeLogcenterBillStatus($id, $target_status);
        $this->response->setOutput(json_encode(array('success'=>'修改对账单状态成功')));

        //添加生成物流营销对账单操作记录
        $this->load->model('user/user');
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $comment = '修改对账单状态为：'.getLogcenterBillStatus()[$target_status];
        $this->model_sale_logcenter_bill->addLogcenterBillHistory($id, $this->user->getId(), $user_info['fullname'], $comment);      

      }
      return;
    } 
  }
  public function exportLists() {

    $filter_logcenter_bill_id = I('get.filter_logcenter_bill_id');
    $filter_logcenter_name = I('get.filter_logcenter_name');
    $filter_year_month = I('get.filter_year_month');
    $filter_total = I('get.filter_total');
    $filter_date_added = I('get.filter_date_added');
    $filter_logcenter_bill_status = I('get.filter_logcenter_bill_status');
    
    if($filter_logcenter_bill_id) {$filter['logcenter_bill_id'] = $filter_logcenter_bill_id;}
    if($filter_logcenter_name) {$filter['logcenter_name'] = array('like', '%'.$filter_logcenter_name.'%');}
    if($filter_year_month) {$filter['DATE_FORMAT(lb.year_month, "%Y-%m")'] = $filter_year_month;}
    if($filter_total) {$filter['total'] = $filter_total;}
    if($filter_date_added) {$filter['date_added'] = $filter_date_added;}
    if($filter_logcenter_bill_status || $filter_logcenter_bill_status==0) {
      $filter['lb.status'] = $filter_logcenter_bill_status;
    }
    $sort = I('get.sort');
    $order = I('get.order');
    if($order) {
      $order = $order=='DESC'?'ASC':'DESC';
    }

    $sort_data = array(
      'sort' => $sort?$sort:'logcenter_bill_id',
      'order' => $order?$order:'DESC',
    );
    $this->load->model('sale/logcenter_bill');
    $logcenter_bill_list =  $this->model_sale_logcenter_bill->getAllList($filter, $sort_data);
    $status_array = getLogcenterBillStatus();
    foreach ($logcenter_bill_list as $key => $value) {
      $save_data[$key] = array(
        "logcenter_bill_id"     =>  $value['logcenter_bill_id'],
        "logcenter_name"        =>  $value['logcenter_name'],
        "status"                =>  $status_array[$value['status']],
        "order_total"           =>  $value['order_total'],
        "commission"            =>  $value['commission'].'% ',
        "total"                 =>  $value['total'],
        "difference"            =>  $value['difference'],
        "real_total"            =>  $value['total']+$value['difference'],
        "ym"                    =>  date('Y-m',strtotime($value['year_month'])),
        "date_added"            =>  $value['date_added'],
        );
      # code...
    }

    $this->load->library('PHPExcel/PHPExcel');
    $objPHPExcel = new PHPExcel();    
    $objProps = $objPHPExcel->getProperties();    
    $objProps->setCreator("Think-tec");
    $objProps->setLastModifiedBy("Think-tec");    
    $objProps->setTitle("Think-tec Contact");    
    $objProps->setSubject("Think-tec Contact Data");    
    $objProps->setDescription("Think-tec Contact Data");    
    $objProps->setKeywords("Think-tec Contact");    
    $objProps->setCategory("Think-tec");
    $objPHPExcel->setActiveSheetIndex(0);     
    $objActSheet = $objPHPExcel->getActiveSheet(); 
       
    $objActSheet->setTitle('Sheet1');
    $col_idx = 'A';
    $headers = array( '对账单编号',        '物流中心名称',   '状态',  '总送货金额', '提成%',     '物流费', '差额',       '应付物流费', '年月份', '添加日期');
    $row_keys = array('logcenter_bill_id', 'logcenter_name', 'status','order_total','commission','total',  'difference', 'real_total', 'ym',     'date_added');
    foreach ($headers as $header) {
      $objActSheet->setCellValue($col_idx++.'1', $header);  
    }
    //添加物流信息
    $i = 2;
    foreach ($save_data as $rlst) {
      $col_idx = 'A';
      foreach ($row_keys as $rk) {
        // $objActSheet->setCellValue($col_idx++.$i, $rlst[$rk]); 
        $objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
      }
      $i++;
    } 

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="logcenter_bill_'.date('Y-m-d',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
  }

}