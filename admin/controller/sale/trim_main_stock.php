<?php
class ControllerSaleTrimMainStock extends Controller {
  private $error = array();

  public function vendor(){
    $this->load->model('sale/trim_stock');
    $json = array();
    $filter_name = I('get.filter_name');
    if(!$filter_name){
      $filter_name='';
    }
    $vendors = $this->model_sale_trim_stock->getVendors($filter_name);
    if($vendors){
      $json = $vendors;
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function product(){
    $json = array();

    if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
      $this->load->model('catalog/product');
      $this->load->model('catalog/option');
      $this->load->model('sale/trim_stock');
      //

      

      if (isset($this->request->get['filter_name'])) {
        $filter['pd.name'] = array('like', '%'.$this->request->get['filter_name'].'%');
      }
      if(isset($this->request->get['filter_vendor'])){
        $filter['vendor.vendor'] = $this->request->get['filter_vendor'];
      }
      $results = $this->model_sale_trim_stock->getProducts($filter);

      foreach ($results as $result) {
        $option_data = array();

        $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

        foreach ($product_options as $product_option) {
          $option_info = $this->model_catalog_option->getOption($product_option['option_id']);

          if ($option_info) {
            $product_option_value_data = array();

            foreach ($product_option['product_option_value'] as $product_option_value) {
              $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

              if ($option_value_info) {
                $product_option_value_data[] = array(
                  'product_option_value_id' => $product_option_value['product_option_value_id'],
                  'option_value_id'         => $product_option_value['option_value_id'],
                  'name'                    => $option_value_info['name'],
                  'price'                   => (float)$product_option_value['price'], 
                  'price_prefix'            => $product_option_value['price_prefix']
                );
              }
            }

            $option_data[] = array(
              'product_option_id'    => $product_option['product_option_id'],
              'product_option_value' => $product_option_value_data,
              'option_id'            => $product_option['option_id'],
              'name'                 => $option_info['name'],
              'type'                 => $option_info['type'],
              'value'                => $product_option['value'],
              'required'             => $product_option['required']
            );
          }
        }

        $json[] = array(
          'product_id' => $result['product_id'],
          'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
          'model'      => $result['model'],
          'cost'          => $result['product_cost'],
          'sku'         =>$result['sku'],
          'option'     => $option_data,
          'price'      => $result['price']
        );
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
  // public function exportShipping(){
  //   $po_id = I('get.po_id');
  //   if(!$po_id){
  //     $po_id = 0;
  //   }
  //   $this->load->model('sale/trim_stock');
  //   $po = $this->model_sale_trim_stock->getPo($po_id);

  //   $data['po'] = $po;

  //   if(!$po){
  //     $this->load->language('error/not_found');
 
  //     $this->document->setTitle($this->language->get('heading_title'));

  //     $data['heading_title'] = $this->language->get('heading_title');
 
  //     $data['text_not_found'] = $this->language->get('text_not_found');
 
  //     $data['breadcrumbs'] = array();

  //     $data['breadcrumbs'][] = array(
  //       'text' => $this->language->get('heading_title'),
  //       'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
  //     );
 

  //     $data['footer'] = $this->load->controller('common/footer');
 
  //     $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
  //   }
  //   $po_products = $this->model_sale_trim_stock->getTrimProducts($po_id);
  //   $count = 0;
  //   foreach ($po_products as $key => $po_product) {
  //     $count += $po_product['qty'];
  //   }

  //   $this->load->library('PHPExcel/PHPExcel');
  //   $objReader = PHPExcel_IOFactory::createReader('Excel5');
  //   $objPHPExcel = $objReader->load(DIR_SYSTEM . "template/order_shipping.xls"); 
  //   $objPHPExcel->getProperties()->setCreator("Think-tec")
  //              ->setLastModifiedBy("Think-tec")
  //              ->setTitle("Bai Huo Zhan PO")
  //              ->setSubject("Bai Huo Zhan PO")
  //              ->setDescription("Bai Huo Zhan PO")
  //              ->setKeywords("Bai Huo Zhan, Think-tec")
  //              ->setCategory("Think-tec");
  //   // $objPHPExcel->setActiveSheetIndex(0);
  //   $objActSheet = $objPHPExcel->getActiveSheet();
  //   $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setSize(48);
  //   $objActSheet->setCellValue('C1', $po['logcenter_info']['shipping_zone_city']);
  //   //$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  //   $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(16);
  //   $objActSheet->setCellValue('B2', $po['included_order_ids']);
  //   $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(16);
  //   $objActSheet->setCellValue('B3', $po['id']);
  //   $objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setSize(16);
  //   $objActSheet->setCellValue('B5', $po['logcenter_info']['firstname']);
  //   $objActSheet->setCellValue('B6', $po['logcenter_info']['address_1']);
  //   $objActSheet->setCellValue('B7', $po['logcenter_info']['telephone']);

  //   //$objPHPExcel->getActiveSheet()->getStyle('A7:G'.$trimw_num)->getFont()->setSize(12);
    
  //   $objActSheet->setTitle('损益单');


  //   $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  //   ob_end_clean();
  //   // Redirect output to a client’s web browser (Excel2007)
  //   header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //   header('Content-Disposition: attachment;filename="PO_BHZ_'.date('Y-m-d-H-i',time()).'.xlsx"');
  //   header('Cache-Control: max-age=0');
  //   // If you're serving to IE 9, then the following may be needed
  //   header('Cache-Control: max-age=1');
  //   // If you're serving to IE over SSL, then the following may be needed
  //   header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
  //   header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  //   header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
  //   header ('Pragma: public'); // HTTP/1.0
  //   $objWriter->save('php://output'); 
  //   exit;
  // }
  // public function export(){
  //   $po_id = I('get.po_id');
  //   if(!$po_id){
  //     $po_id = 0;
  //   }
  //   $this->load->model('sale/trim_stock');
  //   $po = $this->model_sale_trim_stock->getPo($po_id);
  //   $data['po'] = $po;
  //   if(!$po){
  //     $this->load->language('error/not_found');
 
  //     $this->document->setTitle($this->language->get('heading_title'));

  //     $data['heading_title'] = $this->language->get('heading_title');
 
  //     $data['text_not_found'] = $this->language->get('text_not_found');
 
  //     $data['breadcrumbs'] = array();

  //     $data['breadcrumbs'][] = array(
  //       'text' => $this->language->get('heading_title'),
  //       'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
  //     );
 

  //     $data['footer'] = $this->load->controller('common/footer');
 
  //     $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
  //   }
  //   $po_products = $this->model_sale_trim_stock->getTrimProducts($po_id);
  //   $data['po_products'] = $po_products;

  //   $this->load->library('PHPExcel/PHPExcel');
  //   $objReader = PHPExcel_IOFactory::createReader('Excel5');
  //   $objPHPExcel = $objReader->load(DIR_SYSTEM . "template/po_tpl.xls"); 
  //   $objPHPExcel->getProperties()->setCreator("Think-tec")
  //              ->setLastModifiedBy("Think-tec")
  //              ->setTitle("Bai Huo Zhan PO")
  //              ->setSubject("Bai Huo Zhan PO")
  //              ->setDescription("Bai Huo Zhan PO")
  //              ->setKeywords("Bai Huo Zhan, Think-tec")
  //              ->setCategory("Think-tec");
  //   // $objPHPExcel->setActiveSheetIndex(0);
  //   $objActSheet = $objPHPExcel->getActiveSheet();
  //   $objActSheet->setCellValue('B2', $po['id']);
  //   $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  //   $objActSheet->setCellValue('F2', date('Y-m-d H:i',time()));
  //   $objActSheet->setCellValue('B3', $po['vendor_id']);
  //   $objActSheet->setCellValue('B4', $po['vendor']['vendor_name']);
  //   $objActSheet->setCellValue('F3', $po['vendor']['firstname']);
  //   $objActSheet->setCellValue('F4', $po['vendor']['telephone']);

  //   $trimw_num = 6;
  //   $styleBorderOutline = array(
  //     'borders' => array(
  //       'outline' => array(
  //         'style' => PHPExcel_Style_Border::BORDER_THIN,
  //       ),
  //     ),
  //   );
  //   $objPHPExcel->getActiveSheet()->insertNewRowBefore(7, count($po_products));
  //   $count = 0;
  //   foreach ($po_products as $key => $po_product) {
  //     $trimw_num++;
  //     $objActSheet->setCellValue('A'.$trimw_num, $trimw_num-6);
  //     $objActSheet->setCellValue('B'.$trimw_num, $po_product['name'].' '.$po_product['option_name']);
  //     $objActSheet->setCellValue('C'.$trimw_num, $po_product['sku']);
  //     $objActSheet->setCellValue('D'.$trimw_num, $po_product['qty']);
  //     $objActSheet->setCellValue('E'.$trimw_num, $po_product['unit_price']);
  //     $objActSheet->setCellValue('F'.$trimw_num, $po_product['price']);
  //     $objActSheet->setCellValue('G'.$trimw_num, $po_product['comment']);

  //     $count += $po_product['qty'];

  //     $objPHPExcel->getActiveSheet()->getStyle('A'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('B'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('C'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('D'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('E'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('F'.$trimw_num)->applyFromArray($styleBorderOutline);
  //     $objPHPExcel->getActiveSheet()->getStyle('G'.$trimw_num)->applyFromArray($styleBorderOutline);
  //   }
  //   $trimw_num++;

  //   $objActSheet->setCellValue('D'.$trimw_num, $count);
  //   $objActSheet->setCellValue('F'.$trimw_num, $po['total']);
  //   $trimw_num++;
  //   $objActSheet->setCellValue('F'.$trimw_num, date('Y-m-d', strtotime($po['deliver_time'])));

  //   $objPHPExcel->getActiveSheet()->getStyle('A7:G'.$trimw_num)->getFont()->setSize(16);
    
  //   $objActSheet->setTitle('损益单');


  //   $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  //   ob_end_clean();
  //   // Redirect output to a client’s web browser (Excel2007)
  //   header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //   header('Content-Disposition: attachment;filename="PO_BHZ_'.date('Y-m-d-H-i',time()).'.xlsx"');
  //   header('Cache-Control: max-age=0');
  //   // If you're serving to IE 9, then the following may be needed
  //   header('Cache-Control: max-age=1');
  //   // If you're serving to IE over SSL, then the following may be needed
  //   header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
  //   header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  //   header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
  //   header ('Pragma: public'); // HTTP/1.0
  //   $objWriter->save('php://output'); 
  //   exit;
  // }

  public function index() {

    $this->document->setTitle('损益单');

    $this->load->model('sale/trim_stock');

    $this->getList();
  }

  public function add() {
    $this->document->setTitle('新增损益单');
    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');
	$this->document->addScript('view/javascript/lyc_ctl.js');

    $this->load->model('sale/trim_stock');

    $this->form();
  }

  public function edit(){

    $this->document->setTitle('查看损益单');

    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');
	$this->document->addScript('view/javascript/lyc_ctl.js');

    $this->load->model('sale/trim_stock');
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '损益单',
      'href' => $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.po_id');
    if(!$id){
      $id=0;
    }
    
    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $po = $this->model_sale_trim_stock->getPo($id);
    $data['trim'] = $po;
    $po_products = $this->model_sale_trim_stock->getTrimProducts($id);
    $data['trim_products'] = $po_products;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/trim_main_stock_edit.tpl', $data));
  }


  public function view(){

    $this->document->setTitle('查看损益单');

    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');
	$this->document->addScript('view/javascript/lyc_ctl.js');

    $this->load->model('sale/trim_stock');
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '损益单',
      'href' => $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];
	$data['button_cancel'] = $this->language->get('button_cancel');
	$data['cancel'] = $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $id = I('get.trim_id');
    if(!$id){
      $id=0;
    }


    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
     
    $trim = $this->model_sale_trim_stock->getTrim($id);

    $data['trim'] = $trim;
    $trim_products = $this->model_sale_trim_stock->getTrimProducts($id);
    $data['trim_products'] = $trim_products;
    $trim_histories = $this->model_sale_trim_stock->getTrimHistory($id);
    $data['trim_histories'] = $trim_histories;
    $data['included_orders'] = explode(',', $trim['included_order_ids']);
	
	if($trim['status']==1) {
      $data['need_logcenter_confirm'] = true;
    }

    if($trim['status']==2) {
      $data['need_main_allow'] = true;
    }

    if($trim['status']==2) {
      $data['need_main_finsh'] = true;
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/trim_stock_view.tpl', $data));
  }

	public function save(){

		if ($this->request->server['REQUEST_METHOD'] == 'POST'){

			$this->load->model('sale/trim_stock');
			$data = json_decode(file_get_contents('php://input'), true);
			$filter = C('DEFAULT_FILTER');
			$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
			
			if(!$data['products']){
				$this->response->setOutput(json_encode(array('success'=>false, 'info'=>'数据不完整')));
			}
			else{
				$trim_id = $this->model_sale_trim_stock->addTrim($data, $this->user->getId());
			}

			$this->response->setOutput(json_encode(array('success'=>true, 'info'=>$trim_id)));

		}

	}

  //仓库保存损益数量
  public function saveLgStock(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/trim_stock');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      
      if(!$data['products']){
        $this->response->setOutput(json_encode(array('error'=>'数据不完整')));
        return;
      }

      $to_save_stock = array();
      foreach ($data['products'] as $trim_product) {
        $to_save_stock[$trim_product['trim_product_id']] = $trim_product['stock'];
        $to_save_comment[$trim_product['trim_product_id']] = $trim_product['comment'];
      }

      $to_save_stock2 = array();

      // $id = I('rost.ro_id');
      $id = $data['trim_id'];
      if(!$id){
        $id=0;
      }
      $trim = $this->model_sale_trim_stock->getTrim($id);
      //判断po状态，如果是已经结算或者已经关闭，则不允许修改po数量
      if($trim['status'] == 0 || $trim['status'] == 2) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改已结算和已关闭损益单')));
        return;
      }

      $trim_products = $this->model_sale_trim_stock->getTrimProducts($id);

      foreach ($trim_products as $trim_p) {
        if(isset($to_save_stock[$trim_p['id']])) {
          $to_save_stock2[$trim_p['id']] = $to_save_stock[$trim_p['id']];
          $to_save_comment2[$trim_p['id']] = $to_save_comment[$trim_p['id']];
        }
        if(isset($to_save_comment[$trim_p['id']])) {
          $to_save_comment2[$trim_p['id']] = $to_save_comment[$trim_p['id']];
        }
      }

      $this->model_sale_trim_stock->setStock($id, $to_save_stock2);
      $this->model_sale_trim_stock->setComment($id, $to_save_comment2);

      $this->response->setOutput(json_encode(array('success'=>'修改成功')));
      return;
    } 
  }
  
  //总后台保存损益数量
  public function saveStock(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/trim_stock');
      $data = json_decode(file_get_contents('php://input'), true);

      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      
      if(!$data['products']){
        $this->response->setOutput(json_encode(array('error'=>'数据不完整')));
        return;
      }

      $to_save_stock = array();
      foreach ($data['products'] as $trim_product) {
        $to_save_stock[$trim_product['trim_product_id']] = $trim_product['stock'];
        $to_save_comment[$trim_product['trim_product_id']] = $trim_product['comment'];
      }
      $to_save_stock2 = array();

      // $id = I('rost.trim_id');
      $id = $data['trim_id'];
      if(!$id){
        $id=0;
      }
      $trim = $this->model_sale_trim_stock->getTrim($id);
      //判断po状态，如果是已经结算或者已经关闭，则不允许修改po数量

      if($trim['status'] == 1 || $trim['status'] == 3) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改已结算和已关闭损益单')));
        return;
      }

      $trim_products = $this->model_sale_trim_stock->getTrimProducts($id);

      foreach ($trim_products as $trim_p) {
        if(isset($to_save_stock[$trim_p['id']])) {
          $to_save_stock2[$trim_p['id']] = $to_save_stock[$trim_p['id']];
        }
        if(isset($to_save_comment[$trim_p['id']])) {
          $to_save_comment2[$trim_p['id']] = $to_save_comment[$trim_p['id']];
        }
      }

      $this->model_sale_trim_stock->setStock($id, $to_save_stock2);

      $this->model_sale_trim_stock->setComment($id, $to_save_comment2);

      $this->response->setOutput(json_encode(array('success'=>'修改成功')));
      return;
    } 
  }

  public function finshStock(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/trim_stock');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      $id = $data['trim_id'];
      if(!$id){
        $id=0;
      }
      $trim = $this->model_sale_trim_stock->getTrim($id);
      //判断po状态，如果是已经结算或者已经关闭，则不允许修改po数量
      if($trim['status'] == 0 || $trim['status'] == 2) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改已结算和已关闭损益单')));
        return;
      }

      $this->model_sale_trim_stock->changeRoStatus($id, 2);
      $comment = "总后台确认损益单";
      $this->model_sale_trim_stock->addRoHistory($id, $this->user->getId(), $this->user->getUserName(), $comment);  
      $this->response->setOutput(json_encode(array('success'=>'总后台确认损益单成功')));
      return;
    } 
  }

  public function delete() {
    $json = array();

    $this->load->model('sale/trim_stock');

    $id = I('get.trim_id');
    if(!$id){
      $id=0;
    }

    $trim = $this->model_sale_trim_stock->getTrim($id);
    if(!$trim||$trim['status']!=1){
      $json['error'] = '非新增状态损益单不能删除';
    }
    else{
      $this->model_sale_trim_stock->deleteTrim($id);
      $json['success'] = '删除成功';
    }

    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  protected function form(){
        $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '损益单',
      'href' => $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.trim_id');
    if(!$id){
      $id=0;
    }
    

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

	/*选择仓库*/
	$this->load->model('stock/stocksearch');
	$warehouses = $this->model_stock_stocksearch->getWarehouses(); //获取全部仓库
		
	foreach ($warehouses as $value){

		$data["warehouses"][] = array(
			'warehouse_id' => $value['warehouse_id'],
			'name' => $value['name']
		);

	}
	/*选择仓库*/

	$data['button_cancel'] = $this->language->get('button_cancel');
	$data['cancel'] = $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] . $url, 'SSL');
    
    $trim = $this->model_sale_trim_stock->getTrim($id);
    $data['trim'] = $trim;
    $trim_products = $this->model_sale_trim_stock->getTrimProducts($id);
    $data['trim_products'] = $trim_products;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

	$data['trim_status_array'] = getTrimStatus();

    $this->response->setOutput($this->load->view('sale/trim_stock.tpl', $data));
  }

  protected function getList() {
    $page = I('get.page');
    if(!$page){
      $page = 1;
    }
    /*
    if($this->user->getLp()){
      $filter['trim.user_id'] = $this->user->getId();
    }
    else if($this->user->getVp()){
      $filter['trim.vendor_id'] = $this->user->getId();
    }
    else{
      
    }
    */
    $data = I('get.');
	
	$url = '';
/*
    if(isset($data['filter_date_start'])){
      $filter_date_start = $data['filter_date_start'];
	  $url .= '&filter_date_start=' . $filter_date_start;
    }
	else{
		$filter_date_start = '0000-00-00';
		$url .= '&filter_date_start=' . $filter_date_start;
	}

    if(isset($data['filter_date_end'])){
      $filter_date_end = $data['filter_date_end']." 23:59:59";
	  $url .= '&filter_date_end=' . $filter_date_end;
    }
	else{
		$filter_date_end = '9999-12-31 23:59:59';
		$url .= '&filter_date_end=' . $filter_date_end;
	}

    $filter['trim.date_added'] = array('between',array($filter_date_start,$filter_date_end));
*/
    /*
     * 修正分页后时间下拉框不能操作的异常
     * @author sonicsjh
     */
    if (!empty($data['filter_date_start']) && !empty($data['filter_date_end'])) {
        $url .= '&filter_date_start='.$data['filter_date_start'].'&filter_date_end='.$data['filter_date_end'];
        $filter['trim.date_added'] = array('between', array($data['filter_date_start'].' 00:00:00', $data['filter_date_end'].' 23:59:59'));
    }else if (!empty($data['filter_date_start'])) {
        $url .= '&filter_date_start='.$data['filter_date_start'];
        $filter['trim.date_added'] = array('gt', $data['filter_date_start'].' 00:00:00');
    }else if (!empty($data['filter_date_end'])) {
        $url .= '&filter_date_end='.$data['filter_date_end'];
        $filter['trim.date_added'] = array('lt', $data['filter_date_end'].' 23:59:59');
    }

    if(isset($data['filter_return_warehouse'])){
		$filter_return_warehouse = trim($data['filter_return_warehouse']);
		$filter['ro.warehouse_id'] = $filter_return_warehouse;
		$url .= '&filter_return_warehouse=' . $filter_return_warehouse;
    }



    if(isset($data['filter_warehouse'])){
		$filter_warehouse = trim($data['filter_warehouse']);
		$filter['trim.warehouse_id'] = $filter_warehouse;
		$url .= '&filter_warehouse=' . $filter_warehouse;
    }

    if(isset($data['filter_status'])){
      $filter_status = trim($data['filter_status']);
      if($filter_status!='*'){
        $filter['trim.status'] = $filter_status;
		$url .= '&filter_status=' . $filter_status;
      }  
    }else{
      $data['filter_status'] = '*';
	  $url .= '&filter_status=' . $data['filter_status'];
    }

	/*筛选相关*/
	$this->load->model('stock/stocksearch');
	$warehouses = $this->model_stock_stocksearch->getWarehouses(); //获取全部仓库
		
	foreach ($warehouses as $value){

		$data["warehouses"][] = array(
			'warehouse_id' => $value['warehouse_id'],
			'name' => $value['name']
		);

	}
    /*
     * 强制添加仓库筛选功能
     * @author sonicsjh
     */
    $data['warehouseLocked'] = false;
    if ($this->user->getLP() > 0) {
        $data['warehouseLocked'] = true;
        $filter['trim.warehouse_id'] = $this->user->getLP();
    }
    
    /*筛选相关*/

    $trim_list =  $this->model_sale_trim_stock->getList($page, $filter);
    $order_total = $this->model_sale_trim_stock->getListCount($filter);

	foreach($trim_list as &$value){

		if($value["status"]==1){
			$value["color"] = "blue";
		}
		else if($value["status"]==2){
			$value["color"] = "orange";
		}

	}

    $data['status_array'] = getTrimOrderStatus();
    $data['status_array']['*'] = '全部';

	/*分页*/
	$pagination = new Pagination();
	$pagination->total = $order_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_limit_admin');
	$pagination->url = $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

	$data['pagination'] = $pagination->render();
	$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
	/*分页*/

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '损益单',
      'href' => $this->url->link('sale/trim_main_stock', 'token=' . $this->session->data['token'] , 'SSL')
    );

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    $data['token'] = $this->session->data['token'];
    
    $data['trim_list'] = $trim_list;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/trim_main_stock_list.tpl', $data));
  }

  function validateDate($date, $format = 'Y-m-d H:i:s')
  {
      $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date;
  }

  public function genVendorBill(){
    $vendor_bill_month = I('get.year_month');
    $month_valid = $this->validateDate($vendor_bill_month, 'Y-m');
    if(!$month_valid) {
      $this->session->data['error'] = '请选择有效的年月日期';
      $this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));
    }
    //获取选择月份之前半年内所有还未对账的完成状态的订单
    $d = DateTime::createFromFormat('Y-m', $vendor_bill_month);
    $cur_year_month = $d->format('Y-m-01');
    $cur_month = $d->format('Y-m-21');
    $d->modify('-6 month');
    $end_month = $d->format('Y-m-d');
    $this->load->model('sale/vendor_bill');
    $this->load->model('sale/trim_stock');
    $billed_po = $this->model_sale_vendor_bill->getBilledPo($cur_month, $end_month);
    $billed_po_ids = array();
    foreach ($billed_po as $bo) {
      $billed_po_ids[] = $bo['po_id'];
    }

    $vendor_ids = $this->model_sale_trim_stock->getVendorIdsFromPo($cur_month, $end_month, $billed_po_ids);

    foreach ($vendor_ids as $vendor_id) {
      $bill_data = $this->model_sale_trim_stock->getToBillPo($cur_month, $end_month, $billed_po_ids, $vendor_id['vendor_id']);  
      $bill_data_full[$vendor_id['vendor_id']] = $bill_data;
    }

    $commission = 1;
    foreach ($bill_data_full as $vendor_id=>$bill_data) {
      $po_total = 0;
      $po_ids = array();
      foreach ($bill_data as $po) {
        $po_total += (float)$po['total'];
        $po_ids[] = $po['id'];
      }
      $commission_total = $po_total*$commission;
      $data = array();

      //此处的$vendor_id是po表中的vendor_id 对应的是 vendors表中的user_id
      $this->load->model('catalog/vendor');
      $vendor_info = $this->model_catalog_vendor->getVendorByUserId($vendor_id);
      $data['vendor_id'] = $vendor_info['vendor_id'];
      $data['year_month'] = $cur_year_month;
      $data['total'] = $commission_total;
      $data['po_total'] = $po_total;
      $data['date_added'] = date('Y-m-d H:i:s', time());
      $vendor_bill_id = $this->model_sale_vendor_bill->saveVendorBill($data, $po_ids);

      //添加生成po操作记录
      $this->load->model('user/user');
      $user_info = $this->model_user_user->getUser($this->user->getId());
      $comment = '新增品牌商对账单';
      $this->model_sale_vendor_bill->addVendorBillHistory($vendor_bill_id, $this->user->getId(), $user_info['fullname'], $comment);      
    }
    


    $this->session->data['success'] = '生成品牌厂商对账单成功';
    $this->response->redirect($this->url->link('sale/vendor_bill', 'token=' . $this->session->data['token'], 'SSL'));
  }

  public function forceCompletePo() {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/trim_stock');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      // $id = I('post.po_id');
      $id = $data['po_id'];
      if(!$id){
        $id=0;
      }
      $po = $this->model_sale_trim_stock->getPo($id);
      //判断po状态
      if($po['status'] != 3) {
        $this->response->setOutput(json_encode(array('error'=>'只能审核完成部分收货状态损益单')));
        return;
      }

      if($po['status'] == 3) {
        //修改po单的total
        $po_products = $this->model_sale_trim_stock->getTrimProducts($id);
        $new_total = 0;
        foreach ($po_products as $po_product) {
          $new_total += (float)$po_product['unit_price']*(float)$po_product['delivered_qty'];
        }
        $this->model_sale_trim_stock->setPoTotal($id, $new_total);

        //修改po的status到审核完成
        $po_status = 6;
        $this->model_sale_trim_stock->changePoStatus($id, $po_status);

        //添加po状态修改操作记录
        $this->load->model('user/user');
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $comment = '审核完成损益单。损益单金额改为：' . $new_total . '￥';
        $this->model_sale_trim_stock->addPoHistory($id, $this->user->getId(), $user_info['fullname'], $comment);  
      }

      $this->response->setOutput(json_encode(array('success'=>'修改损益单状态成功')));
      return;
    } 


  }
  
  //物流仓库确认
  public function confirmLgStock(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/trim_stock');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);

      // $id = I('post.po_id');
      $id = $data['trim_id'];
      if(!$id){
        $id=0;
      }
      $trim = $this->model_sale_trim_stock->getTrim($id);
      //判断po状态，如果是已经结算或者已经关闭，
      if($trim['status'] != 1) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改损益单')));
		return;
      }  


        $this->model_sale_trim_stock->changeTrimStatus($id, 2);
        
		//确认损益单
        $this->load->model('catalog/logcenter');
        $comment = '物流中心确认损益单';

        $this->model_sale_trim_stock->addTrimHistory($id, $this->user->getId(), $this->user->getUserName(), $comment);  
  

      $this->response->setOutput(json_encode(array('success'=>'物流中心成功确认损益单')));
      return;
    } 
  }
  
	//总后台确认
	public function confirmStock(){
		
		if($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->load->model('sale/trim_stock');
			$data = json_decode(file_get_contents('php://input'), true);
			$filter = C('DEFAULT_FILTER');
			$data = is_array($data) ? array_map_recursive($filter,$data) : $filter($data);

			$id = $data['trim_id'];
			if(!$id){
				$id=0;
			}
      
			$trim = $this->model_sale_trim_stock->getTrim($id);
			$trimproducts = $this->model_sale_trim_stock->getTrimProducts($id);
      
			if($trim['status'] != 2){
				$this->response->setOutput(json_encode(array('error'=>'不能修改库存损益单')));
				return;
			}  

			$this->model_sale_trim_stock->changeTrimStatus($id, 3);

			$this->load->model('stock/autodocument');
			foreach($trimproducts as $value)
			{
				$this->model_stock_autodocument->changeInventory($trim['warehouse_id'],$value['product_code'],$value['qty'],'in', $id);
			}
        
			//确认库存损益单
			$this->load->model('catalog/logcenter');
			$comment = '后台确认损益单';

			$this->model_sale_trim_stock->addTrimHistory($id, $this->user->getId(), $this->user->getUserName(), $comment);  

			$this->response->setOutput(json_encode(array('success'=>'后台成功确认损益单')));
			return;
		} 
	}

  public function exportLists(){

    $this->load->model('sale/trim_stock');

    $data = I('get.');

    if(isset($data['filter_date_start'])){
      $filter_date_start = $data['filter_date_start'];
    }else{
      $filter_date_start = '0000-00-00';
    }

    if(isset($data['filter_date_end'])){
      $filter_date_end = $data['filter_date_end']." 23:59:59";
    }else{
      $filter_date_end = '9999-12-31 23:59:59';
    }

    $filter['trim.date_added'] = array('between',array($filter_date_start,$filter_date_end));
    // if(isset($data['filter_vendor_name'])){
    //   $filter_vendor_name = trim($data['filter_vendor_name']);
    //   $filter['v.vendor_name'] = array('like',"%".$filter_vendor_name."%");
    // }

    if(isset($data['filter_logcenter'])){
      $filter_logcenter = trim($data['filter_logcenter']);
      $filter['lg.logcenter_name'] = array('like',"%".$filter_logcenter."%");
    }

    if(isset($data['filter_status'])){
      $filter_status = trim($data['filter_status']);
      if($filter_status!='*'){
        $filter['ro.status'] = $filter_status;
      }  
    }else{
      $data['filter_status'] = '*';
    }

    $trim_list =  $this->model_sale_trim_stock->getAllProductList($filter);


    $status_array = getTrimStatus();
    foreach ($trim_list as $key => $value) {
      $save_data[] = array(
        'id'            =>  $key+1,
        'name'          =>  $value['name'],
        'option_name'   =>  $value['option_name'],
        'qty'           =>  $value['qty'],
        'price'         =>  $value['price'],
        'unit_price'    =>  $value['unit_price'],
        'deliver_time'  =>  $value['deliver_time'],
        'packing_no'    =>  $value['packing_no'],
        'date_added'    =>  $value['date_added'],
        'total'         =>  $value['qty']*$value['unit_price'],
        'vendor_name'   =>  $value['vendor_name'],
        'model'         =>  $value['model'],
        'logcenter_name'=>  $value['logcenter_name'],
        'status'        =>  $status_array[$value['status']],
        );
    }

    $this->load->library('PHPExcel/PHPExcel');
    $objPHPExcel = new PHPExcel();    
    $objProps = $objPHPExcel->getProperties();    
    $objProps->setCreator("Think-tec");
    $objProps->setLastModifiedBy("Think-tec");    
    $objProps->setTitle("Think-tec Contact");    
    $objProps->setSubject("Think-tec Contact Data");    
    $objProps->setDescription("Think-tec Contact Data");    
    $objProps->setKeywords("Think-tec Contact");    
    $objProps->setCategory("Think-tec");
    $objPHPExcel->setActiveSheetIndex(0);     
    $objActSheet = $objPHPExcel->getActiveSheet(); 
       
    $objActSheet->setTitle('Sheet1');
    $col_idx = 'A';
    $headers = array( '编号','商品名称','选项', '单价','数量','总价','箱入数', '创建时间', '损益时间', '供应商', '物流中心',     '型号', '损益单状态');
    $row_keys = array('id','name','option_name','unit_price','qty','total','packing_no','date_added', 'deliver_time', 'vendor_name','logcenter_name',  'model', 'status');
    foreach ($headers as $header) {
      $objActSheet->setCellValue($col_idx++.'1', $header);  
    }
    //添加物流信息
    $i = 2;
    foreach ($save_data as $rlst) {
      $col_idx = 'A';
      foreach ($row_keys as $rk) {
        // $objActSheet->setCellValue($col_idx++.$i, $rlst[$rk]); 
        $objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
      }
      $i++;
    } 

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="trim_main_stock_'.date('Y-m-d',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
  }

  public function addProductInitiai($ro_id){
      $this->load->model('sale/trim_stock');
      $this->model_sale_trim_stock->addProductInitiai($ro_id);
  }
	
	//导出模板
	public function exportTpl(){

		$this->load->library('PHPExcel/PHPExcel');
		$objPHPExcel = new PHPExcel();    
		$objProps = $objPHPExcel->getProperties();    
		$objProps->setCreator("Think-tec");
		$objProps->setLastModifiedBy("Think-tec");    
		$objProps->setTitle("Think-tec Contact");    
		$objProps->setSubject("Think-tec Contact Data");    
		$objProps->setDescription("Think-tec Contact Data");    
		$objProps->setKeywords("Think-tec Contact");    
		$objProps->setCategory("Think-tec");
		$objPHPExcel->setActiveSheetIndex(0);     
		$objActSheet = $objPHPExcel->getActiveSheet(); 
		   
		$objActSheet->setTitle('Sheet1');
		$col_idx = 'A';
		$headers = array( '条码','选项名','数量','备注选项','备注');
		foreach ($headers as $header) {
		  $objActSheet->setCellValue($col_idx++.'1', $header);  
		}

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		ob_end_clean();
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="trim_logcenter_stock_exp_tpl'.date('Y-m-d',time()).'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter->save('php://output'); 
		exit;
	}
	
	//导入
	public function import() {
		$json = array();
		if (1) {
		  if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
			// Sanitize the filename
			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
			// Validate the filename length
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
			  $json['error'] = '文件名过短';
			}
			// Allowed file extension types
			$allowed = array('xls','xlsx');
			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
			  $json['error'] = '请上传xls或者xlsx文件';
			}
			// Return any upload error
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
			  $json['error'] = '上传出现错误';
			}
		  } 
		}
		else {
		  $json['error'] = '上传失败';
		}

		if (!$json) {
		  $this->load->library('PHPExcel/PHPExcel');
		  $savePath = $this->request->files['file']['tmp_name'];
		  $PHPExcel = new PHPExcel();
		  $PHPReader = new PHPExcel_Reader_Excel2007();
		  if(!$PHPReader->canRead($savePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
		  }
		  $PHPExcel = $PHPReader->load($savePath);
		  $currentSheet = $PHPExcel->getSheet(0);
		  $allRow = $currentSheet->getHighestRow();
		  //获取order_product_id, express_name, express_number在excel中的index
		  $sku_idx = $option_name_idx = $qty_idx = $trim_status_idx = $comment_idx = 0;
		  $tmp_idx = 'A'; //假设导入表格不会长于Z
		  $currentRow = 1;
		  while(($tmp_idx === 0 || $option_name_idx === 0 || $qty_idx === 0 || $trim_status_idx === 0 || $comment_idx === 0)&&$tmp_idx<'Z') {
			switch (trim((String)$currentSheet->getCell($tmp_idx.$currentRow)->getValue())) {
			  case '条码':
				$sku_idx = $tmp_idx;
				break;
			  case '选项名':
				$option_name_idx = $tmp_idx;
				break;
			  case '数量':
				$qty_idx = $tmp_idx;
				break;
			  case '备注选项':
				$trim_status_idx = $tmp_idx;
				break;
			  case '备注':
				$comment_idx = $tmp_idx;
				break;
			}
			$tmp_idx++;
		  }
		  
		  if(0 === $sku_idx || 0 === $option_name_idx||0 === $qty_idx||0 === $trim_status_idx||0 === $comment_idx) {
			$json['error'] = '请检查文件第一行是否有：条码、选项名、数量、备注选项、备注';
		  }
		  
		  if (!$json) {
			$this->load->model('catalog/product');
			$products_info = array();
			for ($currentRow = 2;$currentRow <= $allRow;$currentRow++) {
			  $sku = (String)$currentSheet->getCell($sku_idx.$currentRow)->getValue();
			  $option_name = (String)$currentSheet->getCell($option_name_idx.$currentRow)->getValue();
			  $qty = (String)$currentSheet->getCell($qty_idx.$currentRow)->getValue();
			  $trim_status = (String)$currentSheet->getCell($trim_status_idx.$currentRow)->getValue();
			  $comment = (String)$currentSheet->getCell($comment_idx.$currentRow)->getValue();

			  //检查sku是否存在
			  $product = $this->model_catalog_product->getProductBySku($sku);
			  // var_dump($product);
			  if(empty($product)) {
				$json['error'] = '第' . $currentRow . '行条码对应商品未找到';
			  }
			  //检查此商品是否有选项
			  if(!isset($json['error'])) {
				if(!empty($product['option'])) {
				  //如果有选项则判断当前的选项是否和sku匹配
				  if(!array_key_exists($option_name, $product['option'])) {
					$json['error'] = '第' . $currentRow . '行选项名不属于此条码商品';
				  }
				}
			  }
			  if(!isset($json['error'])) {
				$trim_status_map = getTrimStatus();
				$trim_status_map = array_flip($trim_status_map);
				if(!array_key_exists($trim_status, $trim_status_map)) {
				  $json['error'] = '第' . $currentRow . '行备注选项不合法';
				}
			  }
			  if(isset($json['error'])) {
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				return;
			  }
			  
			  $product_cost = M('vendor')->where(array('vproduct_id'=>$product['product_id']))->getField('product_cost');
			  $product_data['product_id'] = $product['product_id'];
			  $product_data['qty'] = $qty;
			  $product_data['delivered_qty'] = 0;
			  $product_data['unit_price'] = $product_cost;
			  $product_data['status'] = $product['status'];
			  $product_data['name'] = $product['name'];
			  $product_data['sku'] = $product['sku'];
			  $product_data['comment'] = '';
			  $product_data['option'] = array();
			  $product_data['option']['product_option_value_id'] = $product['option'][$option_name];
			  $product_data['option']['name'] = array_key_exists($option_name, $product['option'])?$option_name:'';

			  $product_data['trim_status'] = $trim_status_map[$trim_status];
			  $product_data['comment'] = $comment;

			  $products_info[] = $product_data;
			}        

			//拼装addRo数据
			$ro_data['deliver_time'] = date('Y-m-d H:i:s', time());
			$ro_data['included_order_ids'] = '';
			$ro_data['products'] = $products_info;
			//调用addRo
			$this->load->model('sale/trim_stock');
			$trim_id = $this->model_sale_trim_stock->addTrim($ro_data, $this->user->getId());

			$this->model_sale_trim_stock->addTrimHistory($trim_id, $this->user->getId(), $this->user->getUserName(), '导入调整单');
		  }

		  if (!isset($json['error'])) {
			$json['success'] = sprintf('更新新建调整单 %s条商品记录', count($ro_data['products']));
		  }
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

    /* 导入损益商品 */
    public function add_import (){
        $json = array();
        if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
            $filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
            if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
                $json['error'] = '文件名过短';
            }
            $allowed = array('xls','xlsx');
            if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                $json['error'] = '请上传xls或者xlsx文件';
            }
            if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
                $json['error'] = '上传出现错误';
            }
        }
        else {
          $json['error'] = '上传失败';
        }

        if ('' == $json['error']) {
            $this->load->library('PHPExcel/PHPExcel');
            $savePath = $this->request->files['file']['tmp_name'];
            $PHPExcel = new PHPExcel();
            $PHPReader = new PHPExcel_Reader_Excel2007();
            if(!$PHPReader->canRead($savePath)){
                $PHPReader = new PHPExcel_Reader_Excel5();
            }
            $PHPExcel = $PHPReader->load($savePath);

            $objWorksheet = $PHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); 

            $code_idx = $num_idx = NULL;
            $tmp_idx = 0;
            $currentRow = 1;
            while(($code_idx === NULL || $num_idx === NULL)&&$tmp_idx<26) {
                switch (trim((String)$objWorksheet->getCellByColumnAndRow($tmp_idx,$currentRow)->getValue())) {
                    case '商品编码':
                        $code_idx = $tmp_idx;
                        break;
                    case '数量差额':
                        $num_idx = $tmp_idx;
                        break;
                }
                $tmp_idx++;
            }
            if(NULL === $code_idx || NULL===$num_idx) {
                $json['error'] = '请检查文件第一行是否有：商品编码、数量差额';
            }
        }

        if ('' == $json['error']) {
            $this->load->model('sale/trim_stock');
            for ($currentRow = 2;$currentRow <= $highestRow;$currentRow++) {
                $product_code = (String)$objWorksheet->getCellByColumnAndRow($code_idx,$currentRow)->getValue();
                $product_num = (String)$objWorksheet->getCellByColumnAndRow($num_idx,$currentRow)->getValue();
                $product_arr = $this->model_sale_trim_stock->searchProduct($product_code);
                $product_arr['qty'] = (int)$product_num;
                $product_arr['trim_status'] = ($product_num>0)?2:3;
                $product_arrs[] = $product_arr;
            }
            $json = array(
                'success' => '导入成功',
                'data' => $product_arrs
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));    
    }

}