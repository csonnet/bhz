<?php
class ControllerSaleVdiPurchaseOrder extends Controller {
  private $error = array();

  public function vendor(){
    $this->load->model('sale/purchase_order');
    $json = array();
    $filter_name = I('get.filter_name');
    if(!$filter_name){
      $filter_name='';
    }
    $vendors = $this->model_sale_purchase_order->getVendors($filter_name);
    if($vendors){
      $json = $vendors;
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function product(){
    $json = array();

    if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
      $this->load->model('catalog/product');
      $this->load->model('catalog/option');
      $this->load->model('sale/purchase_order');

      

      if (isset($this->request->get['filter_name'])) {
        $filter['pd.name'] = array('like', '%'.$this->request->get['filter_name'].'%');
      }
      if(isset($this->request->get['filter_vendor'])){
        $filter['vendor.vendor'] = $this->request->get['filter_vendor'];
      }
      $results = $this->model_sale_purchase_order->getProducts($filter);

      foreach ($results as $result) {
        $option_data = array();

        $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

        foreach ($product_options as $product_option) {
          $option_info = $this->model_catalog_option->getOption($product_option['option_id']);

          if ($option_info) {
            $product_option_value_data = array();

            foreach ($product_option['product_option_value'] as $product_option_value) {
              $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

              if ($option_value_info) {
                $product_option_value_data[] = array(
                  'product_option_value_id' => $product_option_value['product_option_value_id'],
                  'option_value_id'         => $product_option_value['option_value_id'],
                  'name'                    => $option_value_info['name'],
                  'price'                   => (float)$product_option_value['price'], 
                  'price_prefix'            => $product_option_value['price_prefix']
                );
              }
            }

            $option_data[] = array(
              'product_option_id'    => $product_option['product_option_id'],
              'product_option_value' => $product_option_value_data,
              'option_id'            => $product_option['option_id'],
              'name'                 => $option_info['name'],
              'type'                 => $option_info['type'],
              'value'                => $product_option['value'],
              'required'             => $product_option['required']
            );
          }
        }

        $json[] = array(
          'product_id' => $result['product_id'],
          'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
          'model'      => $result['model'],
          'cost'          => $result['product_cost'],
          'sku'         =>$result['sku'],
          'option'     => $option_data,
          'price'      => $result['price']
        );
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function export(){
    $po_id = I('get.po_id');
    if(!$po_id){
      $po_id = 0;
    }
    $this->load->model('sale/purchase_order');
    $po = $this->model_sale_purchase_order->getPo($po_id);

    $data['po'] = $po;
    if(!$po){
      $this->load->language('error/not_found');
 
      $this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');
 
      $data['text_not_found'] = $this->language->get('text_not_found');
 
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
      );
 

      $data['footer'] = $this->load->controller('common/footer');
 
      $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
    }
    $po_products = $this->model_sale_purchase_order->getPoProducts($po_id);
    $data['po_products'] = $po_products;

    $this->load->library('PHPExcel/PHPExcel');
    $objReader = PHPExcel_IOFactory::createReader('Excel5');
    $objPHPExcel = $objReader->load(DIR_SYSTEM . "template/po_tpl.xls"); 
    $objPHPExcel->getProperties()->setCreator("Think-tec")
               ->setLastModifiedBy("Think-tec")
               ->setTitle("Bai Huo Zhan PO")
               ->setSubject("Bai Huo Zhan PO")
               ->setDescription("Bai Huo Zhan PO")
               ->setKeywords("Bai Huo Zhan, Think-tec")
               ->setCategory("Think-tec");
    // $objPHPExcel->setActiveSheetIndex(0);
    $objActSheet = $objPHPExcel->getActiveSheet();
    $objActSheet->setCellValue('B2', $po['id']);
    $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objActSheet->setCellValue('F2', date('Y-m-d H:i',time()));
    $objActSheet->setCellValue('B3', $po['vendor_id']);
    $objActSheet->setCellValue('B4', $po['vendor']['vendor_name']);
    $objActSheet->setCellValue('F3', $po['vendor']['firstname']);
    $objActSheet->setCellValue('F4', $po['vendor']['telephone']);
    $objActSheet->setCellValue('B5', $po['logcenter_info']['logcenter_name']);
    $objActSheet->setCellValue('D5', $po['logcenter_info']['firstname']);
    $objActSheet->setCellValue('F5', $po['logcenter_info']['telephone']);
    $objActSheet->setCellValue('B6', $po['included_order_ids']);
    $row_num = 8;
    $styleBorderOutline = array(
      'borders' => array(
        'outline' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
      ),
    );
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(9, count($po_products));
    $count = 0;
    foreach ($po_products as $key => $po_product) {
      $row_num++;
      $objActSheet->setCellValue('A'.$row_num, $row_num-8);
      $objActSheet->setCellValue('B'.$row_num, $po_product['name'].' '.$po_product['option_name']);
      $objActSheet->setCellValueExplicit('C'.$row_num, $po_product['sku'],PHPExcel_Cell_DataType::TYPE_STRING);
      $objActSheet->setCellValue('D'.$row_num, $po_product['qty']);
      $objActSheet->setCellValue('E'.$row_num, $po_product['unit_price']);
      $objActSheet->setCellValue('F'.$row_num, $po_product['price']);
      $objActSheet->setCellValue('G'.$row_num, $po_product['comment']);

      $count += $po_product['qty'];

      $objPHPExcel->getActiveSheet()->getStyle('A'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('B'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('C'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('D'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('E'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('F'.$row_num)->applyFromArray($styleBorderOutline);
      $objPHPExcel->getActiveSheet()->getStyle('G'.$row_num)->applyFromArray($styleBorderOutline);
    }
    $row_num++;

    $objActSheet->setCellValue('D'.$row_num, $count);
    $objActSheet->setCellValue('F'.$row_num, $po['total']);
    $row_num++;
    $objActSheet->setCellValue('F'.$row_num, date('Y-m-d', strtotime($po['deliver_time'])));

    $objPHPExcel->getActiveSheet()->getStyle('A7:G'.$row_num)->getFont()->setSize(12);
    
    $objActSheet->setTitle('采购单');


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="PO_BHZ_'.date('Y-m-d-H-i',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
  }
public function exportShipping(){
    $po_id = I('get.po_id');
    if(!$po_id){
      $po_id = 0;
    }
    $this->load->model('sale/purchase_order');
    $po = $this->model_sale_purchase_order->getPo($po_id);

    $data['po'] = $po;
    //var_dump($po);die();
    if(!$po){
      $this->load->language('error/not_found');
 
      $this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');
 
      $data['text_not_found'] = $this->language->get('text_not_found');
 
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
      );
 

      $data['footer'] = $this->load->controller('common/footer');
 
      $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
    }
    $po_products = $this->model_sale_purchase_order->getPoProducts($po_id);
    $count = 0;
    foreach ($po_products as $key => $po_product) {
      $count += $po_product['qty'];
    }

    $this->load->library('PHPExcel/PHPExcel');
    $objReader = PHPExcel_IOFactory::createReader('Excel5');
    $objPHPExcel = $objReader->load(DIR_SYSTEM . "template/order_shipping.xls"); 
    $objPHPExcel->getProperties()->setCreator("Think-tec")
               ->setLastModifiedBy("Think-tec")
               ->setTitle("Bai Huo Zhan PO")
               ->setSubject("Bai Huo Zhan PO")
               ->setDescription("Bai Huo Zhan PO")
               ->setKeywords("Bai Huo Zhan, Think-tec")
               ->setCategory("Think-tec");
    // $objPHPExcel->setActiveSheetIndex(0);
    $objActSheet = $objPHPExcel->getActiveSheet();
    $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setSize(48);
    $objActSheet->setCellValue('C1', $po['logcenter_info']['shipping_zone_city']);
    //$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(16);
    $objActSheet->setCellValue('B2', $po['included_order_ids']);
    $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(16);
    $objActSheet->setCellValue('B3', $po['id']);
    $objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setSize(16);
    $objActSheet->setCellValue('B5', $po['logcenter_info']['firstname']);
    $objActSheet->setCellValue('B6', $po['logcenter_info']['address_1']);
    $objActSheet->setCellValue('B7', $po['logcenter_info']['telephone']);

    //$objPHPExcel->getActiveSheet()->getStyle('A7:G'.$row_num)->getFont()->setSize(12);
    
    $objActSheet->setTitle('采购单');


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="PO_BHZ_'.date('Y-m-d-H-i',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
  }



  public function index() {

    $this->document->setTitle('采购单');

    $this->load->model('sale/purchase_order');

    $this->getList();
  }

  public function add() {
    $this->document->setTitle('新增采购单');
    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');

    $this->load->model('sale/purchase_order');

    $this->form();
  }

  public function edit(){

    $this->document->setTitle('查看采购单');

    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');

    $this->load->model('sale/purchase_order');
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '采购单',
      'href' => $this->url->link('sale/purchase_order', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.po_id');
    if(!$id){
      $id=0;
    }
    
    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $po = $this->model_sale_purchase_order->getPo($id);
    $data['po'] = $po;
    $po_products = $this->model_sale_purchase_order->getPoProducts($id);
    $data['po_products'] = $po_products;
    $po_histories = $this->model_sale_purchase_order->getPoHistory($id);
    $data['po_histories'] = $po_histories;

    if($po['status']==0) {
      $data['chgStatusBtn'] = '确认采购单';
    } else if($po['status']==1) {
      $data['chgStatusBtn'] = '确认发货';
    } else {
      $data['chgStatusBtn'] = '';
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/vdi_purchase_order_edit.tpl', $data));
  }


  public function view(){

    $this->document->setTitle('查看采购单');

    $this->document->addScript('view/javascript/angular.js');
    $this->document->addScript('view/javascript/bhz_ctl.js');

    $this->load->model('sale/purchase_order');
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '采购单',
      'href' => $this->url->link('sale/purchase_order', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.po_id');
    if(!$id){
      $id=0;
    }
    

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $po = $this->model_sale_purchase_order->getPo($id);
    $data['po'] = $po;
    $po_products = $this->model_sale_purchase_order->getPoProducts($id);
    $data['po_products'] = $po_products;
    $po_histories = $this->model_sale_purchase_order->getPoHistory($id);
    $data['po_histories'] = $po_histories;
    $data['included_orders'] = explode(',', $po['included_order_ids']);

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/vdi_purchase_order_view.tpl', $data));
  }

  public function save(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/purchase_order');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      if(!$data['deliver_time'] || !$data['products']||!$data['vendor']){
        $this->response->setOutput(json_encode(array('success'=>false, 'info'=>'数据不完整')));
      }
      else{
        $po_id = $this->model_sale_purchase_order->addPo($data, $this->user->getId());
      }
      $this->response->setOutput(json_encode(array('success'=>true, 'info'=>$po_id)));
      return;
    }
  }

  public function saveStock(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/purchase_order');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      
      if(!$data['products']){
        $this->response->setOutput(json_encode(array('error'=>'数据不完整')));
        return;
      }

      $to_save_stock = array();
      foreach ($data['products'] as $po_product) {
        $to_save_stock[$po_product['po_product_id']] = $po_product['stock'];
      }
      $to_save_stock2 = array();

      // $id = I('post.po_id');
      $id = $data['po_id'];
      if(!$id){
        $id=0;
      }
      $po = $this->model_sale_purchase_order->getPo($id);
      //判断po状态，如果是已经结算或者已经关闭，则不允许修改po数量
      if($po['status'] == 5 || $po['status'] == 9) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改已结算和已关闭采购单')));
        return;
      }

      $po_products = $this->model_sale_purchase_order->getPoProducts($id);
      //检查stock数量是否在-delivered_qty之间~qty-delivered_qty之间
      $stock_error = array();
      foreach ($po_products as $po_p) {
        if(isset($to_save_stock[$po_p['id']])) {
          if($to_save_stock[$po_p['id']] > $po_p['qty']-$po_p['delivered_qty'] || $to_save_stock[$po_p['id']] < -$po_p['delivered_qty']) {
            $stock_error[$po_p['id']] = '数值需要为0' . '~' . ($po_p['qty']-$po_p['delivered_qty']) . '之间';
          }
          $to_save_stock2[$po_p['id']] = $to_save_stock[$po_p['id']];
        }
      }
      if(!empty($stock_error)) {
        $this->response->setOutput(json_encode(array('error'=>'入库数量不对', 'data'=>$stock_error)));
        return;
      }

      $this->model_sale_purchase_order->setStock($id, $to_save_stock2);

      //判断库存是否全部匹配，如果是的话，就置为全部收货，否则置为部分收货
      $po_products = $this->model_sale_purchase_order->getPoProducts($id);
      $delivered_qty_total = 0;
      $fully_delivered = true;
      $po_status = 0;
      foreach ($po_products as $po_p) {
        $delivered_qty_total += $po_p['delivered_qty'];
        if($po_p['qty'] != $po_p['delivered_qty']) {
          $fully_delivered = false;
        }
      }
      if($fully_delivered) {
        $po_status = 4;
      } elseif($delivered_qty_total!=0) {
        $po_status = 3;
      }
      if($po_status != 0) {
        $this->model_sale_purchase_order->changePoStatus($id, $po_status);
      }

      $this->response->setOutput(json_encode(array('success'=>'修改库存成功')));
      return;
    } 
  }

  protected function form(){
        $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '采购单',
      'href' => $this->url->link('sale/purchase_order', 'token=' . $this->session->data['token'] , 'SSL')
    );

    $data['token'] = $this->session->data['token'];

    $id = I('get.po_id');
    if(!$id){
      $id=0;
    }
    

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $po = $this->model_sale_purchase_order->getPo($id);
    $data['po'] = $po;
    $po_products = $this->model_sale_purchase_order->getPoProducts($id);
    $data['po_products'] = $po_products;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('sale/purchase_order.tpl', $data));
  }

  

  protected function getList() {
    $page = I('get.page');
    if(!$page){
      $page = 1;
    }
    if($this->user->getLp()){
      $filter['po.user_id'] = $this->user->getId();
    }
    else if($this->user->getVp()){
      $filter['po.vendor_id'] = $this->user->getId();
    }
    else{
      
    }
    $data = I('get.');

    if(isset($data['filter_date_start'])){
      $filter_date_start = $data['filter_date_start'];
    }else{
      $filter_date_start = '0000-00-00';
    }

    if(isset($data['filter_date_end'])){
      $filter_date_end = $data['filter_date_end'];
    }else{
      $filter_date_end = '9999-12-31';
    }

    $filter['po.date_added'] = array('between',array($filter_date_start,$filter_date_end));
    if(isset($data['filter_vendor_name'])){
      $filter_vendor_name = trim($data['filter_vendor_name']);
      $filter['v.vendor_name'] = array('like',"%".$filter_vendor_name."%");
    }

    if(isset($data['filter_logcenter'])){
      $filter_logcenter = trim($data['filter_logcenter']);
      $filter['lg.logcenter_name'] = array('like',"%".$filter_logcenter."%");
    }

    if(isset($data['filter_status'])){
      $filter_status = trim($data['filter_status']);
      if($filter_status!='*'){
        $filter['po.status'] = $filter_status;
      }  
    }else{
      $data['filter_status'] = '*';
    }
    
    $po_list =  $this->model_sale_purchase_order->getList($page, $filter);
    $order_total = $this->model_sale_purchase_order->getListCount($filter);

    $pagination = new Pagination();
    $pagination->total = $order_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('sale/vdi_purchase_order', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();
    $data['status_array'] = getPoStatus();
    $data['status_array']['*'] = '全部';
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => '采购单',
      'href' => $this->url->link('sale/purchase_order', 'token=' . $this->session->data['token'] , 'SSL')
    );

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];

      unset($this->session->data['error']);
    } elseif (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    $data['token'] = $this->session->data['token'];
    
    $data['po_list'] = $po_list;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');


    $this->response->setOutput($this->load->view('sale/vdi_purchase_order_list.tpl', $data));
  }

  public function vendorChangePoStatus() {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('sale/purchase_order');
      $data = json_decode(file_get_contents('php://input'), true);
      $filter = C('DEFAULT_FILTER');
      $data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
      // $id = I('post.po_id');
      $id = $data['po_id'];
      if(!$id){
        $id=0;
      }
      $po = $this->model_sale_purchase_order->getPo($id);
      //判断po状态，如果是已经结算或者已经关闭，则不允许修改po数量
      if($po['status'] != 0 && $po['status'] != 1) {
        $this->response->setOutput(json_encode(array('error'=>'不能修改已发货采购单')));
        return;
      }

      if($po['status'] == 0 || $po['status'] == 1) {
        //修改订购
        if ($po['status'] == 0 ) {
          $podata = $this->model_sale_purchase_order->getPoStatusById($id);
          $this->model_sale_purchase_order->addInQty($podata);
        }
        $po_status = $po['status']+1;
        $this->model_sale_purchase_order->changePoStatus($id, $po_status);

        //添加po状态修改操作记录
        $this->load->model('catalog/vendor');
        $vendor_info = $this->model_catalog_vendor->getVendorByUserId($this->user->getId());
        $comment = '修改采购单状态为：' . getPoStatus()[$po_status];
        $this->model_sale_purchase_order->addPoHistory($id, $this->user->getId(), $vendor_info['vendor_name'], $comment);  
      }

      $this->response->setOutput(json_encode(array('success'=>'修改采购单状态成功')));
      return;
    } 
  }

}