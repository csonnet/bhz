<?php
class ControllerReportV2Buyer extends Controller {

    public function index() {
        $this->load->model('reportV2/buyer');
        $data = $this->_initData();
        $avbuyer = $this->model_reportV2_buyer->getAgvBuy();
        $avbuyerp = $this->model_reportV2_buyer->getAgvBuyp();
        $pobuyer = $this->model_reportV2_buyer->getPobuyer();
        $pobuyer = $this->model_reportV2_buyer->getstockTotal($pobuyer);
        $inventqty = $this->model_reportV2_buyer->getInvenTotal($pobuyer );
        $pclass = $this->model_reportV2_buyer->getbuyer($inventqty,$avbuyer,$avbuyerp);
        $data['pclass'] = $pclass;
        // die();
        // var_dump($data['pclass']);
        $this->response->setOutput($this->load->view('reportV2/buyer.tpl', $data));
    }

    protected function _initData($data=array()) {
        $data['token'] = $this->session->data['token'];
        $data['breadcrumbs'] = array(
            array(
                'text' => '首页',
                'href' => $this->url->link('common/dashboard', 'token='.$data['token'], 'SSL'),
            ),
            array(
                'text' => '采销员业绩统计',
                'href' => $this->url->link('reportV2/buyer', 'token='.$data['token'], 'SSL'),
            ),
        );
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['export'] = $this->url->link('reportV2/buyer/exportLists', 'token=' . $this->session->data['token'] . $url, 'SSL');
        return $data;
    }

    public function exportLists(){
        $this->load->model('reportV2/buyer');
        $data = $this->_initData();
        $avbuyer = $this->model_reportV2_buyer->getAgvBuy();
        $avbuyerp = $this->model_reportV2_buyer->getAgvBuyp();
        $pobuyer = $this->model_reportV2_buyer->getPobuyer();
        $pobuyer = $this->model_reportV2_buyer->getstockTotal($pobuyer);
        $inventqty = $this->model_reportV2_buyer->getInvenTotal($pobuyer );
        $pclass = $this->model_reportV2_buyer->getbuyer($inventqty,$avbuyer,$avbuyerp);

            $i=0;
           
        foreach ($pclass as $key => $value) {
            $totalTPXLLDy=0;
            $totalTCSYy=0;
            $totalTPLDy=0;
            $totalTPNDy=0;
            $totalTPLMNy=0;
            $totalTCLMNy=0;
            $totalTPTYy=0;
            $totalTCTYy=0;
            $totaltotalpricey=0;
            $pobuypricey = 0;
            $stockpricey = 0;
            $availableQty=0;
            $TQLMy=0;

             $totalTPXLLDy_n=0;
            $totalTCSYy_n= 0;
            $totalTPLMNy_n= 0;
            $totalTCLMNy_n= 0;
            $totalTPTYy_n= 0;
            $totalTCTYy_n= 0;
            foreach ($value as $yl => $ylv) {
                $i++;
                $save_data[$i] = array(
                'GLNAME'          =>  '',
                'name'   =>  $ylv['name'],
                'TPND'           =>  $ylv['TPND'],
                'TPLD'           =>  $ylv['TPLD'],
                'TPXLLD'   =>  $ylv['TPSY'],
                'TPXLSY'   =>  $ylv['GPRSY'],
                'TPLMN'         =>  $ylv['TPLMN'],
                'GPRLMN'    =>  $ylv['GPRLMN'],
                'TPTY' =>  $ylv['TPTY'],
                'GPRTY'    =>  $ylv['GPRTY'],
                'totalprice'            =>  $ylv['totalprice'],
                'pobuyprice'            =>  $ylv['pobuyprice'],
                'stockprice'            =>  $ylv['stockprice'],
                'SSC'    =>  $ylv['SSC'],
                );
                if ($key=='YL') {
                    $save_data[$i]['GLNAME'] = '应亮';
                }elseif($key=='DM'){
                    $save_data[$i]['GLNAME'] = '燕燕';
                }elseif($key=='ZDM'){
                    $save_data[$i]['GLNAME'] = '周东明';
                }elseif($key=='QT'){
                    $save_data[$i]['GLNAME'] = '其他';
                }
                        $availableQty+= $ylv['availableQty'];
                        $TQLMy+= (float)$ylv['TQLM'];

                        $totalTPXLLDy+= (float)$ylv['TPSY'];
                        $totalTCSYy+= (float)$ylv['TCSY'];
                        $totalTPLDy+= (float)$ylv['TPLD'];
                        $totalTPNDy+= (float)$ylv['TPND'];
                        $totalTPLMNy+= $ylv['TPLMN'];
                        $totalTCLMNy+= $ylv['TCLMN'];
                        $totalTPTYy+= $ylv['TPTY'];
                        $totalTCTYy+= $ylv['TCTY'];

                        $totalTPXLLDy_n+= (float)$ylv['TPSY-n'];
                        $totalTCSYy_n+= (float)$ylv['TCSY-n'];
                        $totalTPLMNy_n+= $ylv['TPLMN-n'];
                        $totalTCLMNy_n+= $ylv['TCLMN-n'];
                        $totalTPTYy_n+= $ylv['TPTY-n'];
                        $totalTCTYy_n+= $ylv['TCTY-n'];

                        $totaltotalpricey+= $ylv['totalprice'];
                        $pobuypricey+= $ylv['pobuyprice'];
                        $stockpricey+= $ylv['stockprice'];
                
            }
            $i++;
            $save_data[$i] = array(
                'GLNAME'          => '合计',
                'name'   =>  '--',
                'TPND'           =>  $totalTPNDy,
                'TPLD'           =>  $totalTPLDy,
                'TPXLLD'         =>  $totalTPXLLDy,
                'TPXLSY'    =>  (round(($totalTPXLLDy_n-$totalTCSYy_n)/$totalTPXLLDy_n, 4)*100).'%',
                'TPLMN'         =>  $totalTPLMNy,
                'GPRLMN'    =>  (round(($totalTPLMNy_n-$totalTCLMNy_n)/$totalTPLMNy_n, 4)*100).'%',
                'TPTY' =>  $totalTPTYy,
                'GPRTY'    =>  (round(($totalTPTYy_n-$totalTCTYy_n)/$totalTPTYy_n, 4)*100).'%',
                'totalprice'            =>  $totaltotalpricey,
                'pobuyprice'            =>  $pobuypricey,
                'stockprice'            =>  $stockpricey,
                'SSC'    =>  round(($availableQty/$TQLMy)*28, 2),
            );
                
                $totalTPXLNDn+=$totalTPXLNDy ;
                $totalTPXLLDn+=$totalTPXLLDy ;
                $totalTPLMNn+=$totalTPLMNy ;
                $totalTPNDn+=$totalTPNDy ;
                $totalTPLDn+=$totalTPLDy ;
                $totalTPTYn+=$totalTPTYy ;
                $totaltotalprice+= $totaltotalpricey;
                $pobuyprice+= $pobuypricey;
                $stockprice+= $stockpricey;
            if ($key!='QT') {
                $totalTPND+=$totalTPNDy ;
                $totalTPLD+=$totalTPLDy ;
                $totalTPXLLD+=$totalTPXLLDy ;
                $totalTCSY+=$totalTCSYy ;
               
                $totalTPLMN+= $totalTPLMNy;
                $totalTCLMN+= $totalTCLMNy;
                $totalTPTY+= $totalTPTYy;
                $totalTCTY+= $totalTCTYy;

                $totalTPXLLD_n+=$totalTPXLLDy_n ;
                $totalTCSY_n+=$totalTCSYy_n;
               
                $totalTPLMN_n+= $totalTPLMNy_n;
                $totalTCLMN_n+= $totalTCLMNy_n;
                $totalTPTY_n+= $totalTPTYy_n;
                $totalTCTY_n+= $totalTCTYy_n;

                $TQLM+=$TQLMy ;
                $availableQt+=$availableQty ;

            }
            
        }
        $save_data[$i+1] = array(
                'GLNAME'          => '百货栈合计',
                'name'   =>  '--',
                'TPND'           =>  $totalTPNDn,
                'TPLD'           =>  $totalTPLDn,
                'TPXLLD'           =>  $totalTPXLLDn,
                'TPXLSY'           =>  (round(($totalTPXLLD_n-$totalTCSY_n)/$totalTPXLLD_n, 4)*100).'%',
                'TPLMN'         =>  $totalTPLMNn,
                'GPRLMN'    =>  (round(($totalTPLMN_n-$totalTCLMN_n)/$totalTPLMN_n, 4)*100).'%',
                'TPTY' =>  $totalTPTYn,
                'GPRTY'    =>  (round(($totalTPTY_n-$totalTCTY_n)/$totalTPTY_n, 4)*100).'%',
                'totalprice'            =>  $totaltotalprice,
                'pobuyprice'            =>  $pobuyprice,
                'stockprice'            =>  $stockprice,
                'SSC'    =>  round(($availableQt/$TQLM)*28, 2),
            );

    $this->load->library('PHPExcel/PHPExcel');
    $objPHPExcel = new PHPExcel();    
    $objProps = $objPHPExcel->getProperties();    
    $objProps->setCreator("Think-tec");
    $objProps->setLastModifiedBy("Think-tec");    
    $objProps->setTitle("Think-tec Contact");    
    $objProps->setSubject("Think-tec Contact Data");    
    $objProps->setDescription("Think-tec Contact Data");    
    $objProps->setKeywords("Think-tec Contact");    
    $objProps->setCategory("Think-tec");
    $objPHPExcel->setActiveSheetIndex(0);     
    $objActSheet = $objPHPExcel->getActiveSheet(); 
       
    $objActSheet->setTitle('Sheet1');
    $col_idx = 'A';
    $headers = array( '采销员','大分类','今天销量额 ','昨天销量额 ', '月至今销量额 ','月至今毛利','年至今销量额','年至今毛利','上个月销售额 ','上月毛利 ','库存金额', '订购金额','   待发货金额','库存天数');
    $row_keys = array('GLNAME','name','TPND','TPLD','TPLMN','GPRLMN','TPTY','GPRTY','TPXLLD','TPXLSY','totalprice','pobuyprice', 'stockprice',  'SSC');
    foreach ($headers as $header) {
      $objActSheet->setCellValue($col_idx++.'1', $header);  
    }
    //添加物流信息
    $i = 2;
    foreach ($save_data as $rlst) {
      $col_idx = 'A';
      foreach ($row_keys as $rk) {
        // $objActSheet->setCellValue($col_idx++.$i, $rlst[$rk]); 
        $objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
      }
      $i++;
    } 

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    
    ob_end_clean();
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="main_purchase_order_'.date('Y-m-d',time()).'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
    }

//导入buyer_report数据
    public function inreport() {
        $this->load->model('reportV2/buyer');
        $avbuyer = $this->model_reportV2_buyer->getAgvBuy();
        for ($i=1; $i <=11 ; $i++) { 
            if ($i<10) {
                $d = date('Y').'-0'.$i;
            }else{
                $d = date('Y').'-'.$i;
            }
            // var_dump($d);
            $invtotal = $this->model_reportV2_buyer->getbuyeReport($avbuyer,$d);
        }
    }

}
