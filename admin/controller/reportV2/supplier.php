<?php
class ControllerreportV2supplier extends Controller {

    public function index() {
        $t = microtime(true);
        $this->load->model('reportV2/supplier');
        $data = $this->_initData();
       
        $rp = $this->model_reportV2_supplier->getrp();

         $outtotal = $this->model_reportV2_supplier->getOutTotal($rp);

         $invtotal = $this->model_reportV2_supplier->getInvTotal($outtotal);
        $status = $this->model_reportV2_supplier->getstatus($invtotal);
         //$index = $this->model_reportV2_supplier->getIndex();
         //$intotal = $this->model_reportV2_supplier->getInTotalByIndex($index);
        foreach ($status as $key => $value) {
            if ($value['vendor_id']) {
                if (!isset($value['outtotal'])) {
                    $value['outtotal']=0;
                }
                $newarr[] = $value;
            }
        }
        $date = array_column($newarr, 'outtotal');

        array_multisort($date,SORT_DESC,$newarr);
        $data['invtotal'] = $newarr;
        // var_dump($invtotal);die();
        $this->response->setOutput($this->load->view('reportV2/supplier.tpl', $data));
    }

    protected function _initData($data=array()) {
        $data['token'] = $this->session->data['token'];
        $data['breadcrumbs'] = array(
            array(
                'text' => '首页',
                'href' => $this->url->link('common/dashboard', 'token='.$data['token'], 'SSL'),
            ),
            array(
                'text' => '供应商分析统计',
                'href' => $this->url->link('reportV2/supplier', 'token='.$data['token'], 'SSL'),
            ),
        );
        $data['export'] = $this->url->link('reportV2/supplier/exportLists', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        return $data;
    }
    public function exportLists(){
        $this->load->model('reportV2/supplier');
        // $intotal = $this->model_reportV2_supplier->getInTotal();
        $rp = $this->model_reportV2_supplier->getrp( );
        $outtotal = $this->model_reportV2_supplier->getOutTotal($rp);

        $invtotal = $this->model_reportV2_supplier->getInvTotal($outtotal);
        $status = $this->model_reportV2_supplier->getstatus($invtotal);
         //$index = $this->model_reportV2_supplier->getIndex();
         //$intotal = $this->model_reportV2_supplier->getInTotalByIndex($index);
         foreach ($status as $key => $value) {
            if ($value['vendor_id']) {
                if (!isset($value['outtotal'])) {
                    $value['outtotal']=0;
                }
                $newarr[] = $value;
            }
        }
        // $date = array_column($newarr, 'outtotal');

        // array_multisort($date,SORT_DESC,$newarr);
           
        foreach ($newarr as $key => $value) {
            if ($key) {
                $save_data[] = array(
                    'vendor_id'          =>$value['vendor_id'],
                    'vendor_name'          =>$value['vendor_name'],
                    'status'          =>$value['status'],
                    '2017-01'          =>$value['2017-01'],
                    '2017-02'          =>$value['2017-02'],
                    '2017-03'          =>$value['2017-03'],
                    '2017-04'          =>$value['2017-04'],
                    '2017-05'          =>$value['2017-05'],
                    '2017-06'          =>$value['2017-06'],
                    '2017-07'          =>$value['2017-07'],
                    '2017-08'          =>$value['2017-08'],
                    '2017-09'          =>$value['2017-09'],
                    '2017-10'          =>$value['2017-10'],
                    '2017-11'          =>$value['2017-11'],
                    '2017-12'          =>$value['2017-12'],
                    '2018-01'          =>$value['2018-01'],
                    '2018-02'          =>$value['2018-02'],
                    '2018-03'          =>$value['2018-03'],
                    '2018-04'          =>$value['2018-04'],
                    'outtotal'          =>$value['outtotal'],
                    'lre'          =>$value['outtotal']-$value['buytotal'],
                    'ml'          =>(round(($value['outtotal']-$value['buytotal'])/$value['outtotal'], 4)*100).'%',
                    'outtotal17'          =>$value['outtotal17'],
                    'lre17'          =>$value['outtotal17']-$value['buytotal17'],
                    'ml17'          =>(round(($value['outtotal17']-$value['buytotal17'])/$value['outtotal17'], 4)*100).'%',
                    'fy'          =>'--',
                    'invtotal'          =>$value['invtotal'],
                );
            }
           
        }
       
        $this->load->library('PHPExcel/PHPExcel');
        $objPHPExcel = new PHPExcel();    
        $objProps = $objPHPExcel->getProperties();    
        $objProps->setCreator("Think-tec");
        $objProps->setLastModifiedBy("Think-tec");    
        $objProps->setTitle("Think-tec Contact");    
        $objProps->setSubject("Think-tec Contact Data");    
        $objProps->setDescription("Think-tec Contact Data");    
        $objProps->setKeywords("Think-tec Contact");    
        $objProps->setCategory("Think-tec");
        $objPHPExcel->setActiveSheetIndex(0);     
        $objActSheet = $objPHPExcel->getActiveSheet(); 
        $objActSheet->setTitle('Sheet1');
        $col_idx = 'A';
        $headers = array( '供应商编号','供应商名字','供应商状态','2017-01','2017-02','2017-03','2017-04','2017-05','2017-06','2017-07','2017-08','2017-09','2017-10','2017-11','2017-12','2018-01','2018-02','2018-03','2018-04','年至上月末销售金额','利润额','毛利率','2017销售金额','2017利润额','2017毛利率', '费用','库存金额');
        $row_keys = array('vendor_id','vendor_name','status','2017-01','2017-02','2017-03','2017-04','2017-05','2017-06','2017-07','2017-08','2017-09','2017-10','2017-11','2017-12','2018-01','2018-02','2018-03','2018-04','outtotal','lre','ml','outtotal17','lre17','ml17','fy','invtotal');
        foreach ($headers as $header) {
          $objActSheet->setCellValue($col_idx++.'1', $header);  
        }
        //添加物流信息
        $i = 2;
        foreach ($save_data as $rlst) {
          $col_idx = 'A';
          foreach ($row_keys as $rk) {
            // $objActSheet->setCellValue($col_idx++.$i, $rlst[$rk]); 
            $objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
          }
          $i++;
        } 

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        
        ob_end_clean();
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="main_purchase_order_'.date('Y-m-d',time()).'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter->save('php://output'); 
        exit;
    }

    // public function getOut(){
    //     $this->load->model('reportV2/supplier');
    //     for ($i=1; $i <=12 ; $i++) { 
    //         if ($i<10) {
    //             $d = '2017-0'.$i;

    //         }else{
    //             $d = '2017-'.$i;
    //         }
    //         // var_dump($d);die();

    //         $invtotal = $this->model_reportV2_supplier->getOut($d);
    //     }
        


    // }
    

    public function getOut(){
        $this->load->model('reportV2/supplier');
        // for ($i=1; $i <=4 ; $i++) { 
        //     if ($i<10) {
        //         $d = '2018-0'.$i;

        //     }else{
        //         $d = '2018-'.$i;
        //     }
        //     // var_dump($d);die();

        //     $invtotal = $this->model_reportV2_supplier->getOut($d);
        // }
        
        $invtotal = $this->model_reportV2_supplier->getOut('2018-04');


    }

    public function getrp(){
        $this->load->model('reportV2/supplier');
        $invtotal = $this->model_reportV2_supplier->getrp();


      
    }
}
