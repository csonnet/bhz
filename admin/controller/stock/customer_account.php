<?php
class Controllerstockcustomeraccount extends Controller {
	private $error = array();
	
	//仓库初始化
	public function index() {
		$this->load->language('stock/customer_account');
		

		$this->document->setTitle($this->language->get('heading_title'));
		// var_dump($this->language->get('heading_title'));die();
		$this->load->model('stock/customer_account');

		$this->getList();

    }

	private function getList(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
	    // var_dump($data);
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/customer_ac.js');
	   
	   if(!empty($data['customer_id'])){
	      $customer_id = trim($data['customer_id']);
	      $filter['c.customer_id'] = array('like',"%".$customer_id."%");
	    }
	    if(!empty($data['fullname'])){
	      $fullname = trim($data['fullname']);
	      $filter['u.fullname'] = array('like',"%".$fullname."%");
	    }

	   	if(!empty($data['telephone'])){
	      $telephone = trim($data['telephone']);
	      $filter['c.telephone'] = array('like',"%".$telephone."%");
	    }
	    if(!empty($data['company_name'])){
	      $company_name = trim($data['company_name']);
	      $filter['c.company_name'] = array('like',"%".$company_name."%");
	    }else{
	     	$filter['c.company_name'] = array('neq',"null");
	    }
	    $filter['c.is_magfin'] = array('eq',"0");

		//获取数据


	    $account_list =  $this->model_stock_customer_account->getList($page, $filter);

	    $total = $this->model_stock_customer_account->getListCount($filter);
	    $pagination = new Pagination();
	    $pagination->total = $total;
	    $pagination->page = $page;
	    $pagination->limit = $this->config->get('config_limit_admin');
	    $pagination->url = $this->url->link('stock/customer_account', 'token=' . $this->session->data['token'] . '&page={page}'.'&customer_id='.$customer_id.'&fullname='.$fullname.'&telephone='.$data['telephone'].'&company_name='.$data['company_name'], 'SSL');
		$data['pagination'] = $pagination->render();
	    $data['account_list'] = $account_list;
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

			/*面包屑导航栏*/
			$data['heading_title'] = $this->language->get('heading_title');
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('stock/customer_account', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
			$data['enable_action'] = $this->url->link('stock/customer_account/payall', 'token=' . $this->session->data['token'] . $url, 'SSL');
			/*面包屑导航栏*/
			
			/*右上操作按钮*/
		
		$data['confirm_action'] = $this->url->link('stock/customer_account/sendConfirmSms', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['view'] = $this->url->link('stock/customer_account/view', 'token=' . $this->session->data['token'] .'&templateId=91553507' , 'SSL');
		/*右上操作按钮*/
		

		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		    // var_dump($data['refer_type_id']);
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/customer_account_list.tpl', $data));

	}


    public function searchCustomer() {
		
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = trim($this->request->get['filter_name']);
		}
		else{
			$filter_name = '';
		}
	
		$this->load->model('stock/customer_account');
		$results = $this->model_stock_customer_account->getAutoCompleteCus($filter_name);
		foreach ($results as $result){
			$json[] = array(
				'telephone' => $result['telephone'],
				'company_name' => $result['company_name'],
				'customer_id' => $result['customer_id'],

			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function save()
	{
		$this->load->model('stock/customer_account');
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		// var_dump($data);die();
		$this->load->model('stock/customer_account');
	 	$this->model_stock_customer_account->addCustomerAcc($data);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success'=>true)));

    }

  	public function delete(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
		$this->load->model('stock/customer_account');
	   
	   if(!empty($data['customer_id'])){
	      $customer_id = trim($data['customer_id']);
	      $res  =  $this->model_stock_customer_account->delete($customer_id);
	      if ($res) {
	      	$json['success'] = "删除成功";
	      }else{
	      	$json['error'] = "删除失败";
	      }
	    }
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	    /*
     * 发送通知短信
     */
    public  function sendConfirmSms() {
		$this->load->model('stock/customer_account');
        $this->load->model('tool/sms_log');
	    $res  =  $this->model_stock_customer_account->getCustomerAccount();
	    foreach ($res as $key => $value) {
	    	$phoneList[] = $value['telephone'];
	    	$phoneList[] = $value['contact_tel'];
	    }
	    $phoneList[] = '18217706627';//测试
	    $phoneList[] = '18616897011';//陈启令
	    $phoneList[] = '13382214258';
	    $phoneList[] = '13915009173';
	    $phoneList[] = '13916648759';
	    $phoneList[] = '13262339655';
	    $phoneList[] = '13862951387';
	    $phoneList[] = '18806766220';
	    // var_dump( $phoneList);
	   	$phoneList = array_unique($phoneList);
	    // var_dump( $phoneList);die();
        

        $this->load->helper('sms');
        $sms = new smsHelper();
        $appId = $this->config->get('tianyi_appid');
        $appSecret = $this->config->get('tianyi_appsecret');
        $templateId = '91553507';//开通帐期确认，详见 http://open.189.cn/ 
        $endDate = date("Y/m/d",strtotime("2 day"));
        $BeginDate=date('Y/m/01', strtotime(date("Y-m-d")));
        $rangeDate = $BeginDate.'-'.date('Y/m/d', strtotime("$BeginDate +1 month -1 day"));
        $rangeDate1 = $BeginDate.'-'.date('Y/m/d', strtotime("$BeginDate +1 month -1 day"));
        $firstday = date("Y/m/25");
        $payDate  = date('Y/m/d',strtotime("$firstday +1 month"));
        // var_dump($payDate);
        $tempParam = array(
            'endDate' => $endDate,
            'rangeDate' => $rangeDate,
            'payDate' => $payDate,
            'rangeDate1' => $rangeDate1,
        );
        // die();
        // var_dump($tempParam);
        $sms = new smsHelper();
        foreach ($phoneList as $phone) {
           	$retU = $sms->sendSms($appId, $appSecret, $templateId, $phone, $tempParam);
    		$this->model_tool_sms_log->saveData($retU['smsLog']);
        }
 		$json['success'] = '短信已发送';

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
     
    }


    	    /*
     * 账期催款
     */
    public  function sendrepayment() {
		$this->load->model('stock/customer_account');
        $this->load->model('tool/sms_log');
	    // $res  =  $this->model_stock_customer_account->getCustomerAccount();
	    // foreach ($res as $key => $value) {
	    // 	$phoneList[] = $value['telephone'];
	    // 	$phoneList[] = $value['contact_tel'];
	    // }
	    // $phoneList[] = '18217706627';//测试
	   	// $phoneList = array_unique($phoneList);
	    // var_dump( $phoneList);die();
        

        $this->load->helper('sms');
        $sms = new smsHelper();
        $appId = $this->config->get('tianyi_appid');
        $appSecret = $this->config->get('tianyi_appsecret');
        $templateId = '91553946';//开通帐期确认，详见 http://open.189.cn/
        $sql = "SELECT o.order_id, c.fullname, c.company_name, o.recommend_usr_id, u.username, u.contact_tel FROM `order` AS o LEFT JOIN customer AS c ON o.customer_id = c.customer_id LEFT JOIN `user` AS u ON o.recommend_usr_id = u.user_id WHERE o.order_id IN(7706,8189,7634,8190,7823,7693,7884,7623,7926,7624,8174,7877,7523,7775,8261,8085,8216,8283,7959,7729,7651,7846,8180,7828,8156) ORDER BY o.order_id ";
        $query=$this->db->query($sql);
        $sms = new smsHelper();
	 	foreach ($query->rows as $key => $value) {
	 		// var_dump()
	 		if ($value['order_id'] ==8156) {
	 			$tempParam = array(
		            'customer' => $value['company_name'],
		            'user' => '王希刚',
	        	);
	 		}else{
	 			$tempParam = array(
		            'customer' => $value['company_name'],
		            'user' => $value['username'],
	        	);
	 		}

 		    $phone = $value['fullname'];
 		    // print_r($tempParam);
 		    // var_dump($phone);
	       	$retU = $sms->sendSms($appId, $appSecret, $templateId, $phone, $tempParam);

    		$this->model_tool_sms_log->saveData($retU['smsLog']);
    		$phone = $value['contact_tel'];
 		    // print_r($tempParam);
 		    // var_dump($phone);
	       	$retU = $sms->sendSms($appId, $appSecret, $templateId, $phone, $tempParam);

    		// $this->model_tool_sms_log->saveData($retU['smsLog']);
    		// die();
	 	}
   

        // var_dump($payDate);
 		$json['success'] = '短信已发送';

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
     
    }



    public  function view() {
		$this->load->language('stock/customer_account');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('stock/customer_account');
		$data = I('get.');
	    $log_list =  $this->model_stock_customer_account->getSmsLog($data['templateId']);
	    // var_dump($log_list);
	    $data['log_list'] = $log_list;
		/*面包屑导航栏*/
		$data['heading_title'] = $this->language->get('heading_title');
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('stock/customer_account', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		/*右上操作按钮*/
		
		$data['cancel'] = $this->url->link('stock/customer_account/', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		

		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/

		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/customer_account_view.tpl', $data));

     
    }


}
