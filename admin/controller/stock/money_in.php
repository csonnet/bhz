<?php
class ControllerStockMoneyIn extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('stock/money_in');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('stock/money_in');

		$this->getList();

    }

	private function getList(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/bhz_ctl.js');
	    if(!empty($data['filter_date_start'])){
	      $filter_date_start = $data['filter_date_start'];
	    }else{
	      $filter_date_start = '0000-00-00';
	    }

	    if(!empty($data['filter_date_end'])){
	      $filter_date_end = $data['filter_date_end'];
	    }else{
	      $filter_date_end = '9999-12-31';
	    }

	    $filter['money_in.date_added'] = array('between',array($filter_date_start,$filter_date_end));

	    if(isset($data['filter_status'])){
	      $filter_status = trim($data['filter_status']);
	      if($filter_status!='*'){
	        $filter['money_in.status'] =array('eq',$filter_status);
	      }  
	    }else{
	      $data['filter_status'] = '*';
	      $filter['money_in.status'] = array('neq',4);
	    }

	    if(isset($data['filter_type'])){
	      $filter_type = trim($data['filter_type']);
	      if($filter_type!='*'){
	        $filter['money_in.refer_type_id'] = array('eq',$filter_type);
	      }  
	    }else{
	      $data['filter_type'] = '*';
	    }
	    if(!empty($data['refer_id'])){
	      $refer_id = trim($data['refer_id']);
	      $filter['money_in.refer_id'] = array('like',"%".$refer_id."%");
	    }

	    if(!empty($data['payer_name'])){
	      $payer_name = trim($data['payer_name']);
	      $filter['money_in.payer_name'] = array('like',"%".$payer_name."%");
	    }

	    
		//获取数据
		$data['status_array'] = $this->model_stock_money_in->getstatus();
		$data['status_array'][] = array('money_in_status_id'=>'*','name'=>'全部');
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(1);
		$data['money_type'][] = array('money_type_id'=>'*','type_name'=>'全部');

	    $money_list =  $this->model_stock_money_in->getList($page, $filter);
	    foreach ($money_list as $key => $value) {
	    	if ($value['status']!=2) {
	    		$money_list[$key]['all_color']='red';
	    	}
	    }
	    $total = $this->model_stock_money_in->getListCount($filter);
	    $pagination = new Pagination();
	    $pagination->total = $total;
	    $pagination->page = $page;
	    $pagination->limit = $this->config->get('config_limit_admin');
	    $pagination->url = $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] . '&page={page}'.'&filter_date_end='.$filter_date_end.'&filter_date_start='.$filter_date_start.'&filter_status='.$data['filter_status'].'&refer_id='.$data['refer_id'].'&payer_name='.$data['payer_name'].'&filter_type='.$data['filter_type'], 'SSL');
		$data['pagination'] = $pagination->render();
	    $data['money_list'] = $money_list;
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));


			/*面包屑导航栏*/
			$data['heading_title'] = $this->language->get('heading_title');
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
			/*面包屑导航栏*/
			
			/*右上操作按钮*/

		$data['button_add'] = $this->language->get('button_add');
		$data['buttons_enable'] = $this->language->get('buttons_enable');
		$data['buttons_disable'] = $this->language->get('buttons_disable');
		$data['text_confirm_enable'] = $this->language->get('text_confirm_enable');
		$data['text_confirm_disable'] = $this->language->get('text_confirm_disable');
		
		$data['add'] = $this->url->link('stock/money_in/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		

		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		    // var_dump($data['refer_type_id']);
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/money_in_list.tpl', $data));

	}


	public function add() {
		$this->load->model('stock/money_in');
		$this->load->model('stock/money_out');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
	    $this->getForm();
	}

	protected function getForm(){
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '应收款项',
		  'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '应收款项';
		$data['status'] = $this->model_stock_money_in->getstatus();
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(1);
		// var_dump($data['money_type']);

		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/money_in_add.tpl', $data));
	}

	public function save()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		$this->load->model('stock/money_in');
		$data['operator'] = $this->user->getUserName();
		$data['operator_id'] = $this->user->getId();
	    $money_in_id = $this->model_stock_money_in->addmoney($data,$this->user->getId(), $this->user->getUserName());
	    $comments = '新增应付';
	    $this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success'=>true)));
		

    }

    public function cancel()
	{
		$this->load->model('stock/money_in');
		$json = array();
		$money_in_id = I('get.money_in_id');
    	if (empty($money_in_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_money_in->cancel($money_in_id)) {
 				$comments = "作废应付" ; 
      			$this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);
 				$json['success'] = '成功';
 			}else{
 				$json['success'] = '失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    
	}

	public function recover()
	{
		$this->load->model('stock/money_in');
		$json = array();
		$money_in_id = I('get.money_in_id');
    	if (empty($money_in_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_money_in->recover($money_in_id)) {
 				$comments = "取消作废" ; 
      			$this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);
 				$json['success'] = '成功';
 			}else{
 				$json['success'] = '失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    
	}

	public function pay()
	{
		$this->load->model('stock/money_in');
		$money_in_id = I('get.money_in_id');
    	if (empty($money_in_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_money_in->pay($money_in_id)) {
 				$comments = "确认支付" ; 
      			$this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);
 				$json['success'] = '成功';
 			}else{
 				$json['success'] = '失败';
 			}
	    	
	    }

    	$this->index();
	}

	public function edit()
	{
		$this->load->model('stock/money_in');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
	    $this->form();
    
	}

	public function form()
	{
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '应收款项',
		  'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '应收款项';
		$money_in_id = I('get.money_in_id');
		$data['status'] = $this->model_stock_money_in->getstatus();
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(1);

		$data['money_in'] = $this->model_stock_money_in->getInById($money_in_id);

		  // var_dump($data['money_in']);
		// $data['status'] = $this->model_stock_money_in->getstatus();
		// $data['refer_type'] = $this->model_stock_money_in->getrefertype();


		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/money_in_edit.tpl', $data));
    
	}


	public function view(){
		$this->load->model('stock/money_in');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '应付款项',
		  'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '应付款项';
		$money_in_id = I('get.money_in_id');
		$data['money_in'] = $this->model_stock_money_in->getIn($money_in_id);
		$data['histories'] = $this->model_stock_money_in->getHistory($money_in_id);
		$data['token'] = $this->session->data['token'];
		$data['cancel'] = $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/money_in_view.tpl', $data));

    }


    public function edit_in()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		$this->load->model('stock/money_in');
		$money_in_id = I('get.money_in_id');
	 	$this->model_stock_money_in->editin($data,$money_in_id);
	 	$this->model_stock_money_in->confirm($money_in_id);
	 	$comments = "增加实收金额：".$data['addreceived']; 
	 	$this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success'=>true)));

    }

	public function searchOrder() {
		
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = trim($this->request->get['filter_name']);
		}
		else{
			$filter_name = '';
		}
		$this->load->model('stock/money_in');
		$money_type = trim($this->request->get['money_type']);
		
		if ($money_type==1) {
			$filter_data['order_id'] = array('like',"%".$filter_name."%");
			
			$results = $this->model_stock_money_in->getAutoCompleteOrder($filter_data);
			foreach ($results as $result){
				$json[] = array(
					'refer_id' => $result['order_id'],
					'payment_company' => $result['payment_company'],
					'customer_id' => $result['customer_id'],
					'ori_total' => $result['ori_total'],
					'receivable' => $result['receivable']
				);
			}

		}else{
			$json[] = array(
				'error'=>'请选择相关数据类型'
				);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function finish_stock(){
		$this->load->model('stock/money_in');
		$json = array();
		$money_in_id = I('get.money_in_id');
    	if (empty($money_in_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_money_in->finishstock($money_in_id)) {
 				$comments = "完成应付" ; 
      			$this->model_stock_money_in->addHistory($money_in_id, $this->user->getId(),$comments);
 				$json['success'] = '成功';
 			}else{
 				$json['success'] = '失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
	}


	public function test(){
		$this->load->model('stock/money_in');
		$res = $this->model_stock_money_in->dr();
		foreach ($res as $key => $value) {
			$data = array(
			'payer_id'=>$value['customer_id'], 
			'payer_name'=>$value['payment_company'].'('.$value['shipping_address'].')', 
			'refer_type_id'=>1, 
			'refer_id'=>$value['order_id'], 
			'receivable'=>$value['po'], 
			'refer_total'=>$value['ori_total'], 
			'status'=>$value['status'], 
			'comment'=>'系统批量导入', 
			'pay_time'=>$value['paid_time'],
			'date_added'=>$value['date_added'],
			'operator'=>'admin',
			'trader_type'=>2,
			'operator_id'=>1,
	  		);
	  		if ($value['is_pay']) {
	  			$data['received']=$value['total'];
	  			$data['status']=2;
	  		}else{
	  			$data['status']=1;
	  		}
	  		$res = $this->model_stock_money_in->in($data);

		}

    }

    public function aa(){
    	$this->load->model('stock/money_in');
		$res = $this->model_stock_money_in->getOrderById(5231);

		if ($res) {
			$this->model_stock_money_in->updatemoney($res,5231);
		}
    }
}