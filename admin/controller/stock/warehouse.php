<?php
class ControllerStockWarehouse extends Controller {
	
	private $error = array();
	
	//初始化
	public function index() {

		$this->load->language('stock/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('stock/warehouse');

		$this->getList();

    }
	
	//获取仓库列表
	private function getList(){

		$url = '';
		
		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
			$url .= '&page=' . $page;
		}
		else{
			$page = 1;
		}
		
		/*面包屑导航栏*/
		$data['heading_title'] = $this->language->get('heading_title');
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		/*右上操作按钮*/
		$data['button_add'] = $this->language->get('button_add');
		$data['buttons_enable'] = $this->language->get('buttons_enable');
		$data['buttons_disable'] = $this->language->get('buttons_disable');
		$data['text_confirm_enable'] = $this->language->get('text_confirm_enable');
		$data['text_confirm_disable'] = $this->language->get('text_confirm_disable');
		
		$data['add'] = $this->url->link('stock/warehouse/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['enable_action'] = $this->url->link('stock/warehouse/enableStatus', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['disable_action'] = $this->url->link('stock/warehouse/disableStatus', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		
		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		
		/*表格列*/
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_warehouse_name'] = $this->language->get('column_warehouse_name');
		$data['column_type'] = $this->language->get('column_type');
		$data['column_order'] = $this->language->get('column_order');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		$data['button_edit'] = $this->language->get('button_edit');
		/*表格列*/

		$data['can_edit'] = $this->user->hasPermission('modify', 'stock/warehouse');

		$filter_data = array(
			'start'  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'   => $this->config->get('config_limit_admin')
		);

		$warehouse_total = $this->model_stock_warehouse->getTotalWarehouses($filter_data);  //仓库总计数量
		$results = $this->model_stock_warehouse->getWarehouses($filter_data); //仓库数据

		foreach ($results as $result) {

			//类型显示文字
			if($result['type'] == 1){
				$type = $this->language->get('text_main_type');
			}
			elseif($result['type'] == 2) {
				$type = $this->language->get('text_normal_type');
			}
			elseif($result['type'] == 3) {
				$type = $this->language->get('text_defect_type');
			}
			
			//状态显示文字
			if($result['status'] == 1){
				$status = $this->language->get('text_enabled');
				$trcolor ="";
			}
			elseif($result['status'] == 0) {
				$status = $this->language->get('text_disabled');
				$trcolor ="red";
			}

			$data['warehouses'][] = array(
				'warehouse_id' => $result['warehouse_id'],
				'selected' => isset($this->request->post['selected']) && in_array($result['warehouse_id'],$this->request->post['selected']),
				'tr_color'  => $trcolor,
				'name'     => $result['name'],
				'type'    => $type,
				'order'    => $result['order'],
				'status'    => $status,
				'view'      => $this->url->link('', 'token=' . $this->session->data['token'] . '&filter_warehouse=' . $result['warehouse_id'] . $url, 'SSL'), //查看出入库单详细
				'edit'       => $this->url->link('stock/warehouse/edit', 'token=' . $this->session->data['token'] . '&warehouse_id=' . $result['warehouse_id'] . $url, 'SSL') //编辑仓库
			);

		}

		if(isset($this->error['warning'])){
			$data['error_warning'] = $this->error['warning'];
		}
		else{
			$data['error_warning'] = '';
		}

		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		else{
			$data['success'] = '';
		}
		
		/*分页*/
		$pagination = new Pagination();
		$pagination->total = $warehouse_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($warehouse_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($warehouse_total - $this->config->get('config_limit_admin'))) ? $warehouse_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $warehouse_total, ceil($warehouse_total / $this->config->get('config_limit_admin')));
		/*分页*/

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['sort_status'] = $this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . '&sort=status' . $sorturl, 'SSL');
		/*表格点击排序*/
		
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		//返回模板页
		$this->response->setOutput($this->load->view('stock/warehouse_list.tpl', $data));

	}
	
	//获取仓库详细页
	private function getForm(){

		/*面包屑导航栏*/
		$data['heading_title'] = $this->language->get('heading_title');
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		/*右上操作按钮*/
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		/*右上操作按钮*/
		
		/*标题*/
		$data['text_form'] = !isset($this->request->get['warehouse_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		/*标题*/
		
		/*表单项*/
		$data['entry_warehouse_name'] = $this->language->get('entry_warehouse_name');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_no_select'] = $this->language->get('text_no_select');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['text_none'] = $this->language->get('text_none');
		$data['entry_address'] = $this->language->get('entry_address');
		$data['entry_manager'] = $this->language->get('entry_manager');
		$data['entry_contact_tel'] = $this->language->get('entry_contact_tel');
		$data['entry_type'] = $this->language->get('entry_type');
		$data['entry_order'] = $this->language->get('entry_order');
		$data['entry_parent_warehouse'] = $this->language->get('entry_parent_warehouse');
		$data['text_main_type'] = $this->language->get('text_main_type');
		$data['text_normal_type'] = $this->language->get('text_normal_type');
		$data['text_defect_type'] = $this->language->get('text_defect_type');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		/*表单项*/
		
		/*获取省市*/
		$this->load->model('localisation/country');
		$data['countries'] = $this->model_localisation_country->getCountries();
		/*获取省市*/

		/*获取残次品仓所属仓库*/
		$data['parent_warehouses'] = $this->model_stock_warehouse->getParentWarehouses(); 
		/*获取残次品仓所属仓库*/
		
		/*错误信息*/
		if(isset($this->error['warning'])){
			$data['error_warning'] = $this->error['warning'];
		}
		else{
			$data['error_warning'] = '';
		}

		if(isset($this->error['name'])){
			$data['error_warehouse_name'] = $this->error['name'];
		}
		else{
			$data['error_warehouse_name'] = '';
		}

		if (isset($this->error['country'])) {
			$data['error_warehouse_country'] = $this->error['country'];
		}
		else{
			$data['error_warehouse_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$data['error_warehouse_zone'] = $this->error['zone'];
		}
		else{
			$data['error_warehouse_zone'] = '';
		}

		if (isset($this->error['city'])) {
			$data['error_warehouse_city'] = $this->error['city'];
		}
		else{
			$data['error_warehouse_city'] = '';
		}

		if(isset($this->error['address'])){
			$data['error_warehouse_address'] = $this->error['address'];
		}
		else{
			$data['error_warehouse_address'] = '';
		}

		if(isset($this->error['manager'])){
			$data['error_warehouse_manager'] = $this->error['manager'];
		}
		else{
			$data['error_warehouse_manager'] = '';
		}

		if(isset($this->error['contact_tel'])){
			$data['error_warehouse_contact_tel'] = $this->error['contact_tel'];
		}
		else{
			$data['error_warehouse_contact_tel'] = '';
		}

		if(isset($this->error['type'])){
			$data['error_warehouse_type'] = $this->error['type'];
		}
		else{
			$data['error_warehouse_type'] = '';
		}

		if(isset($this->error['order'])){
			$data['error_warehouse_order'] = $this->error['order'];
		}
		else{
			$data['error_warehouse_order'] = '';
		}
		/*错误信息*/
		
		if(!isset($this->request->get['warehouse_id'])){

			$data['action'] = $this->url->link('stock/warehouse/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');

		}
		else{

			$data['action'] = $this->url->link('stock/warehouse/edit', 'token=' . $this->session->data['token'] . '&warehouse_id=' . $this->request->get['warehouse_id'] . $url, 'SSL');

			//获取指定仓库信息
			$warehouse_info = $this->model_stock_warehouse->getWarehouse($this->request->get['warehouse_id']);

		}

		$data['cancel'] = $this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['token'] = $this->session->data['token'];

		if(isset($this->request->post['name'])){
			$data['name'] = $this->request->post['name'];
		}
		elseif(!empty($warehouse_info)){
			$data['name'] = $warehouse_info['name'];
		}
		else{  
			$data['name'] = '';
		}

		if(isset($this->request->post['country_id'])){
			$data['country_id'] = $this->request->post['country_id'];
		}
		elseif(!empty($warehouse_info)){
			$data['country_id'] = $warehouse_info['country_id'];
		}
		else{  
			$data['country_id'] = '';
		}

		if(isset($this->request->post['zone_id'])){
			$data['zone_id'] = $this->request->post['zone_id'];
		}
		elseif(!empty($warehouse_info)){
			$data['zone_id'] = $warehouse_info['zone_id'];
		}
		else{  
			$data['zone_id'] = '';
		}

		if(isset($this->request->post['city_id'])){
			$data['city_id'] = $this->request->post['city_id'];
		}
		elseif(!empty($warehouse_info)){
			$data['city_id'] = $warehouse_info['city_id'];
		}
		else{  
			$data['city_id'] = '';
		}

		if(isset($this->request->post['address'])){
			$data['address'] = $this->request->post['address'];
		}
		elseif(!empty($warehouse_info)){
			$data['address'] = $warehouse_info['address'];
		}
		else{  
			$data['address'] = '';
		}

		if(isset($this->request->post['manager'])){
			$data['manager'] = $this->request->post['manager'];
		}
		elseif(!empty($warehouse_info)){
			$data['manager'] = $warehouse_info['manager'];
		}
		else{  
			$data['manager'] = '';
		}

		if(isset($this->request->post['contact_tel'])){
			$data['contact_tel'] = $this->request->post['contact_tel'];
		}
		elseif(!empty($warehouse_info)){
			$data['contact_tel'] = $warehouse_info['contact_tel'];
		}
		else{  
			$data['contact_tel'] = '';
		}

		if(isset($this->request->post['type'])){
			$data['type'] = $this->request->post['type'];
		}
		elseif(!empty($warehouse_info)){
			$data['type'] = $warehouse_info['type'];
		}
		else{  
			$data['type'] = 2;
		}

		if(isset($this->request->post['parent_warehouse'])){
			$data['parent_warehouse'] = $this->request->post['parent_warehouse'];
		}
		elseif(!empty($warehouse_info)){
			$data['parent_warehouse'] = $warehouse_info['parent_warehouse'];
		}
		else{  
			$data['parent_warehouse'] = 0;
		}

		if(isset($this->request->post['order'])){
			$data['order'] = $this->request->post['order'];
		}
		elseif(!empty($warehouse_info)){
			$data['order'] = $warehouse_info['order'];
		}
		else{  
			$data['order'] = 0;
		}

		if(isset($this->request->post['status'])){
			$data['status'] = $this->request->post['status'];
		}
		elseif(!empty($warehouse_info)){
			$data['status'] = $warehouse_info['status'];
		}
		else{
			$data['status'] = 1;
		}

		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		//返回模板页
		$this->response->setOutput($this->load->view('stock/warehouse_form.tpl', $data));

	}
	
	//添加仓库
	public function insert(){
		
		$this->load->language('stock/warehouse');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('stock/warehouse');

		if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()){

			 $this->model_stock_warehouse->addwarehouse($this->request->post);

			 $this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if(isset($this->request->get['page'])){
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['sort'])){
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if(isset($this->request->get['order'])){
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->response->redirect($this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}

		$this->getForm();

	}

	//编辑仓库
	public function edit(){
		
		$this->load->language('stock/warehouse');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('stock/warehouse');

		if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()){

			 $this->model_stock_warehouse->editWarehouse($this->request->get['warehouse_id'], $this->request->post);

			 $this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if(isset($this->request->get['page'])){
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['sort'])){
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if(isset($this->request->get['order'])){
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->response->redirect($this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}

		$this->getForm();

	}
	
	//验证表单
	private function validateForm(){

		if(!$this->user->hasPermission('modify', 'stock/warehouse')){
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$data = $this->request->post;
		
		if($data['name'] == null){
			$this->error['name'] = $this->language->get('error_name');
		}

		if($data['country_id'] == ""){
			$this->error['country'] = $this->language->get('error_country');
		}

		if($data['zone_id'] == ""){
			$this->error['zone'] = $this->language->get('error_zone');
		}

		if($data['city_id'] == ""){
			$this->error['city'] = $this->language->get('error_city');
		}

		if($data['address'] == ""){
			$this->error['address'] = $this->language->get('error_address');
		}

		if($data['order'] == ""){
			$this->error['order'] = $this->language->get('error_order');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;

	}
	
	//启用仓库
	public function enableStatus(){
		
		$this->load->language('stock/warehouse');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('stock/warehouse');

		if(isset($this->request->post['selected'])){
			foreach($this->request->post['selected'] as $warehouse_id){
				$this->model_stock_warehouse->Enablewarehouse($warehouse_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if(isset($this->request->get['page'])){
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['sort'])){
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if(isset($this->request->get['order'])){
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->response->redirect($this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		
		}

		$this->getList();

	}

	//停用仓库
	public function disableStatus(){
		
		$this->load->language('stock/warehouse');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('stock/warehouse');

		if(isset($this->request->post['selected'])){
			foreach($this->request->post['selected'] as $warehouse_id){
				$this->model_stock_warehouse->Disablewarehouse($warehouse_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if(isset($this->request->get['page'])){
				$url .= '&page=' . $this->request->get['page'];
			}

			if(isset($this->request->get['sort'])){
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if(isset($this->request->get['order'])){
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->response->redirect($this->url->link('stock/warehouse', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		
		}

		$this->getList();

	}
	
}
?>