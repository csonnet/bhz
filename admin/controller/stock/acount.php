<?php
class Controllerstockacount extends Controller {

	//仓库初始化
	public function index() {

		$this->load->model('stock/acount');

		$this->document->setTitle('超市对账单');
		

		$this->getList();

    }

	private function getList(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
	    if(!empty($data['market'])){
	      $market = trim($data['market']);
	      $filter['money_in.payer_id'] = array('eq',$market);
	    }

	    if (!empty($data['filter_date_start']) && !empty($data['filter_date_end'])) {
	        $filter_date_start = $data['filter_date_start'];
	        $filter_date_end = $data['filter_date_end'];
	        $filter['money_in.date_added'] = array('between', array($filter_date_start.' 00:00:00', $filter_date_end.' 23:59:59'));
	    }else if (!empty($data['filter_date_start'])) {
	        $filter_date_start = $data['filter_date_start'];
	        $filter['money_in.date_added'] = array('gt', $filter_date_start.' 00:00:00');
	    }else if (!empty($data['filter_date_end'])) {
	        $filter_date_end = $data['filter_date_end'];
	        $filter['money_in.date_added'] = array('lt', $filter_date_end.' 23:59:59');
	    }
	    if(!empty($data['salesman'])){
	      	$salesman = trim($data['salesman']);
	    	$filter['`user`.user_id'] = array('eq',$salesman);

	    }

	    if(!empty($data['is_zero'])){
	      $is_zero = trim($data['is_zero']);
	    }else{
	      $data['is_zero'] = '*';
	    }
	     $acount =  $this->model_stock_acount->getList($page,$filter, $filter_date_start,$filter_date_start);
	     foreach ($acount as $key => $value) {
	     	$acount[$key]['payables'] =$value['money_out']['payables']; 
	     	$acount[$key]['paid'] =$value['money_out']['paid']; 
	     	$acount[$key]['differ'] =($value['receivable']-$value['received']-$value['money_out']['payables']+$value['money_out']['paid']); 
	     	if ($is_zero==1&&$acount[$key]['differ']!=0) {
	     		unset($acount[$key]);
	     	}
	     	if ($is_zero==2&&$acount[$key]['differ']==0) {
	     		unset($acount[$key]);
	     	}
	     }
	     $data['acount'] = $acount;
	  	$order_total = count($this->model_stock_acount->getListCount($filter));
	    $pagination = new Pagination();
	    $pagination->total = $order_total;
	    $pagination->page = $page;
	    $pagination->limit = $this->config->get('config_limit_admin');
	    $pagination->url = $this->url->link('stock/acount', 'token=' . $this->session->data['token'] . '&page={page}'.'&filter_date_end='.$filter_date_end.'&filter_date_start='.$filter_date_start.'&market='.$market.'&salesman='.$salesman.'&is_zero='.$is_zero, 'SSL');

	    $data['pagination'] = $pagination->render();
	    $data['stocl_list'] = $stocl_list;
		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));
		/*面包屑导航栏*/
		$data['heading_title'] = '超市对账单';
		$data['breadcrumbs'][] = array(
			'text' => '首页',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' =>'超市对账单',
			'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		$data['text_list'] = '超市对账单';


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/count.tpl', $data));

	}


	public function detail(){
		$this->load->model('stock/acount');

		$this->document->setTitle('超市对账单');
		$data = I('get.');	
		if(!empty($data['payer_id'])){
	      $payer_id = trim($data['payer_id']);
	      $filter['money_in.payer_id'] = array('eq',$payer_id);
	      $filter1['money_out.payee_id'] = array('eq',$payer_id);
	    }     
		if (!empty($data['filter_date_start']) && !empty($data['filter_date_end'])) {
	        $filter_date_start = $data['filter_date_start'];
	        $filter_date_end = $data['filter_date_end'];
	        $filter['money_in.date_added'] = array('between', array($filter_date_start.' 00:00:00', $filter_date_end.' 23:59:59'));
	        $filter1['money_out.date_added'] = array('between', array($filter_date_start.' 00:00:00', $filter_date_end.' 23:59:59'));
	    }else if (!empty($data['filter_date_start'])) {
	        $filter_date_start = $data['filter_date_start'];
	        $filter['money_in.date_added'] = array('gt', $filter_date_start.' 00:00:00');
	        $filter1['money_out.date_added'] = array('gt', $filter_date_start.' 00:00:00');
	    }else if (!empty($data['filter_date_end'])) {
	        $filter_date_end = $data['filter_date_end'];
	        $filter['money_in.date_added'] = array('lt', $filter_date_end.' 23:59:59');
	        $filter1['money_out.date_added'] = array('lt', $filter_date_end.' 23:59:59');
	    }
	    $filter1['money_out.trader_type'] = array('eq', 2);
	    $filter['money_in.trader_type'] = array('eq', 2);
		$money_out_id = $data['money_out_id'];
	    $data['money_in'] =  $this->model_stock_acount->getMoneyIn($filter);
	    $data['money_out'] =  $this->model_stock_acount->getMoneyOut($filter1);
	    $data['filter_date_start'] = $filter_date_start;
	    $data['filter_date_end'] = $filter_date_end;
		/*面包屑导航栏*/
		$data['heading_title'] = '超市对账单';
		$data['breadcrumbs'][] = array(
			'text' => '首页',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' =>'超市对账单',
			'href' => $this->url->link('stock/money_in', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		$data['text_list'] = '超市对账单';


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/count_view.tpl', $data));
	}
	

	public function salesman(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = trim($this->request->get['filter_name']);
		}
		else{
			$filter_name = '';
		}

		$this->load->model('stock/acount');
		

		$results = $this->model_stock_acount->getAutoCompleteUser($filter_name);

		foreach ($results as $result){
			$json[] = array(
				'fullname' => $result['fullname'],
				'recommended_code' => $result['recommended_code'],
				'user_id' => $result['user_id']

			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}