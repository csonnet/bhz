<?php
class ControllerStockLend extends Controller {

	public function index() {

		$this->load->model('stock/lend');

		$this->document->setTitle('借货单');

		$this->getList();

	}

	public function getList(){
		//filter_lend_id 借货单id
		if(isset($this->request->get['filter_lend_id'])){
			$filter_lend_id = $this->request->get['filter_lend_id'];
		}else{
			$filter_lend_id = null;
		}
		//filter_status 单据状态
		if(isset($this->request->get['filter_status'])){
			$filter_status = $this->request->get['filter_status'];
		}else{
			$filter_status = null;
		}
		//filter_add_data_start 订单创建开始时间
		if(isset($this->request->get['filter_add_date_start'])){
			$filter_add_date_start = $this->request->get['filter_add_date_start'];
		}else{
			$filter_add_date_start = null;
		}
		//filter_add_data_end 订单创建结束时间
		if(isset($this->request->get['filter_add_date_end'])){
			$filter_add_date_end = $this->request->get['filter_add_date_end'];
		}else{
			$filter_add_date_end = null;
		}
		//filter_waregouse 仓库名
		if(isset($this->request->get['filter_warehouse'])){
			$filter_warehouse = $this->request->get['filter_warehouse'];
		}else{
			$filter_warehouse = null;
		}
		//filter_name_code 商品编码
		if(isset($this->request->get['filter_name_code'])){
			$filter_name_code = $this->request->get['filter_name_code'];
		}else{
			$filter_name_code = null;
		}
		//sort 排序
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'lo.loan_order_id';
		}
		//order 正序排降序排
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		//page 分页
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		//$data['loan_del']=$this->url->link('stock/lend/delloan', 'token=' . $this->session->data['token'] , 'SSL');
		$url = '';

		if(isset($this->request->get['filter_lend_id'])){
			$url .= '&filter_lend_id=' . $this->request->get['filter_lend_id'];
		}

		if(isset($this->request->get['filter_name_code'])){
			$url .= '&filter_name_code=' . $this->request->get['filter_name_code'];
		}

		if(isset($this->request->get['filter_status'])){
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if(isset($this->request->get['filter_warehouse'])){
			$url .= '&filter_warehouse=' . $this->request->get['filter_warehouse'];
		}

		if(isset($this->request->get['filter_add_date_start'])){
			$url .= '&filter_add_date_start=' . $this->request->get['filter_add_date_start'];
		}

		if(isset($this->request->get['filter_add_date_end'])){
			$url .= '&filter_add_date_end=' . $this->request->get['filter_add_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => '借货单',
			'href' => $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		//echo $url;
		$filter_data = array(

			'filter_lend_id'        => $filter_lend_id,
			'filter_name_code'      => $filter_name_code,
			'filter_status'         => $filter_status,
			'filter_warehouse'      => $filter_warehouse,
			'filter_add_date_start' => $filter_add_date_start,
			'filter_add_date_end'   => $filter_add_date_end,
			'sort'                 => $sort,
			'order'                => $order,
			'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                => $this->config->get('config_limit_admin')

		);

		$lend_total = $this->model_stock_lend->getotalend($filter_data);
		// echo $lend_total;
		$results = $this->model_stock_lend->getlends($filter_data);
		 // print_r($results);
		foreach($results as $key=>$val){

			$results[$key]['view'] = $this->url->link('stock/lend/view', 'token=' . $this->session->data['token'] . '&loan_order_id=' . $val['loan_order_id'], 'SSL');

			$results[$key]['copy'] = $this->url->link('stock/lend/add', 'token=' . $this->session->data['token'] . '&loan_order_id=' . $val['loan_order_id'], 'SSL');

		}

		$data['heading_title'] = '借货单';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['add'] = $this->url->link('stock/lend/add','token=' . $this->session->data['token'] . $url, 'SSL');
		$data['status_arr'] = $this->model_stock_lend->get_status();
		$data['warehouse_arr'] = $this->model_stock_lend->get_warehouse();
		$data['loan_orders'] = $results;

		$data['user_id'] = $this->session->data['user_id'];
		$data['token'] = $this->session->data['token'];
		$data['store'] = HTTPS_CATALOG;

		$data['filter_lend_id'] = $filter_lend_id;
		$data['filter_name_code'] = $filter_name_code;
		$data['filter_warehouse'] = $filter_warehouse;
		$data['filter_add_date_start'] = $filter_add_date_start;
		$data['filter_add_date_end'] = $filter_add_date_end;
		$data['filter_status_array'] = array($filter_status => 'selected');
		$data['filter_warehouse_array'] = array($filter_warehouse => 'selected');
		$data['sort'] = $sort;
		$data['order'] = $order;

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if(isset($this->request->get['filter_lend_id'])){
			$url .= '&filter_lend_id=' . $this->request->get['filter_lend_id'];
		}

		if(isset($this->request->get['filter_name_code'])){
			$url .= '&filter_name_code=' . $this->request->get['filter_name_code'];
		}

		if(isset($this->request->get['filter_status'])){
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if(isset($this->request->get['filter_warehouse'])){
			$url .= '&filter_warehouse=' . $this->request->get['filter_warehouse'];
		}

		if(isset($this->request->get['filter_add_date_start'])){
			$url .= '&filter_add_date_start=' . $this->request->get['filter_add_date_start'];
		}

		if(isset($this->request->get['filter_add_date_end'])){
			$url .= '&filter_add_date_end=' . $this->request->get['filter_add_date_end'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_loan_order_id'] = $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . '&sort=lo.loan_order_id' . $url, 'SSL');

		$data['sort_status'] = $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . '&sort=lo.status' . $url, 'SSL');
		//$data['loan_del'] = $this->url->link('stock/lend/delloan', 'token=' . $this->session->data['token'], 'SSL');

		$url = '';

		if(isset($this->request->get['filter_lend_id'])){
			$url .= '&filter_lend_id=' . $this->request->get['filter_lend_id'];
		}

		if(isset($this->request->get['filter_name_code'])){
			$url .= '&filter_name_code=' . $this->request->get['filter_name_code'];
		}

		if(isset($this->request->get['filter_status'])){
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if(isset($this->request->get['filter_warehouse'])){
			$url .= '&filter_warehouse=' . $this->request->get['filter_warehouse'];
		}

		if(isset($this->request->get['filter_add_date_start'])){
			$url .= '&filter_add_date_start=' . $this->request->get['filter_add_date_start'];
		}

		if(isset($this->request->get['filter_add_date_end'])){
			$url .= '&filter_add_date_end=' . $this->request->get['filter_add_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $lend_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($lend_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($lend_total - $this->config->get('config_limit_admin'))) ? $lend_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $lend_total, ceil($lend_total / $this->config->get('config_limit_admin')));

		// API login
		$this->load->model('user/api');

		$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

		if ($api_info) {
			$data['api_id'] = $api_info['api_id'];
			$data['api_key'] = $api_info['key'];
			$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
		} else {
			$data['api_id'] = '';
			$data['api_key'] = '';
			$data['api_ip'] = '';
		}
		$this->response->setOutput($this->load->view('stock/lend_goods_list.tpl',$data));
	}

	public function add(){

		  $this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/bhz_ctl.js');

    	$this->load->model('stock/lend');

    	$loan_order_id = $this->request->get['loan_order_id'];

    	if((int)$loan_order_id){

    		$result = $this->model_stock_lend->getlend($loan_order_id);
    		$data['warehouse_id_selected'][$result['from_warehouse_id']] = 'selected';

    	}

    	if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		  $data['loan_order_id'] = (int)$loan_order_id;

    	$data['all_warehouses'] = $this->model_stock_lend->getWarehouse();

    	$data['return_url'] = $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . $url, 'SSL');

    	$data['token'] = $this->session->data['token'];

    	$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			// print_r($data);
		$this->response->setOutput($this->load->view('stock/lend_goods_add.tpl', $data));

	}

	public function save(){

		$this->load->model('stock/lend');
		$json = array();
		$data = json_decode(file_get_contents('php://input'),true);
		 //var_dump($data);

		foreach($data['signmans'] as $val){
			 $approvel_ids[] = $val['user_id'];
		}

		$approvel_ids = implode(',',$approvel_ids);
		$insert_data = array(
			'approvel' => $approvel_ids,
			'logcenter_id' => $data['tlogcenter'],
			'shipping_name' => $data['lend_logcenter_name'],
			'shipping_tel' => $data['lend_logcenter_tel'],
			'shipping_address' => $data['lend_logcenter_address'],
			'from_warehouse_id' => $data['fwarehouse'],
			'comment' => $data['comment']
		);

		$loan_order_id = $this->model_stock_lend->addlend($insert_data);


		$this->model_stock_lend->addlendetail($data['products'],$loan_order_id,$data['fwarehouse']);

		if($loan_order_id){

		 $json = array('success' => '借货单添加成功');

			   $this->response->addHeader('Content-Type: application/json');
		     $this->response->setOutput(json_encode($json));

		}else{

			$json = array('error' => '数据错误');

			$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));

		}

	}

	public function getlogcenters(){

		$this->load->model('stock/lend');

		$result = $this->model_stock_lend->getlogcenters();

		$this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($result));

	}

	public function getlogcenterid(){

		$this->load->model('stock/lend');

		$loan_order_id = $this->request->get['loan_order_id'];

		$logcenter_id = $this->model_stock_lend->getlogcenterid($loan_order_id);

		$result = $this->model_stock_lend->getLogcenter($logcenter_id);

		$this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($result));

	}

	public function getallusers(){

		$this->load->model('stock/lend');

		$result = $this->model_stock_lend->getUsers();

		$this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($result));

	}

	public function getusersbyloanid(){

		$this->load->model('stock/lend');

		$loan_order_id = $this->request->get['loan_order_id'];

		$result = $this->model_stock_lend->getuserbyloanid($loan_order_id);

		$this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($result));

	}

	public function getlendproducts(){

		$this->load->model('stock/lend');

		$loan_order_id = $this->request->get['loan_order_id'];

		$result = $this->model_stock_lend->getloandetail($loan_order_id);

		$this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($result));

	}
//借货显示
	public function view(){

		$loan_order_id = $this->request->get['loan_order_id'];
		$this->document->addScript('view/javascript/jquery/jquery-2.1.1.min.js');
		$this->document->addScript('view/javascript/jq.js');
		$this->load->model('stock/lend');
		// $this->load->model('sale/purchase_order');
		if((int)$loan_order_id){

			$data = $this->model_stock_lend->getlend($loan_order_id);
			$data['id']=$this->user->getId();
			//print_r($data);
			$data['lend_details'] = $this->model_stock_lend->getlendetail($loan_order_id);

			$data['histories'] = $this->model_stock_lend->gethistory($loan_order_id);
			$warehouse = $this->model_stock_lend->get_allwarehouse();
    	$data['warehouse'] = $warehouse;
			$allowverifymark = false;

			if($data['status_id'] == 0 || $data['status_id'] == 1){

				$approvel_ids_arr = explode(',',trim($data['approvel_ids'],','));

				if(!empty($data['approveled_ids'])){
					$approveled_ids_arr = explode(',',trim($data['approveled_ids'],','));
				}else{
					$approveled_ids_arr = array();
				}

				foreach($approvel_ids_arr as $key=>$val){

					if($val == $this->session->data['user_id']){

						if($key != 0 || count($approveled_ids_arr) != 0){

							if($key != 0 && count($approveled_ids_arr) != 0){

								if($approveled_ids_arr[count($approveled_ids_arr)-1] == $approvel_ids_arr[$key-1]){

									$allowverifymark = true;

								}

							}

						}else{

							$allowverifymark = true;

						}

						break;

					}

				}

			}

			$data['allowverifymark'] = $allowverifymark;

		}



		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => '借货单',
			'href' => $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['heading_title'] = '借货单';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['return_url'] = $this->url->link('stock/lend', 'token=' . $this->session->data['token'] . $url, 'SSL');

		// API login
		$this->load->model('user/api');
		$data['ajaxurl']=$this->url->link('stock/lend/repay', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));
		$data['stock_out_data'] = $this->model_stock_lend->getstockoutbyreferid($loan_order_id);
		foreach($data['stock_out_data'] as $key=>$val){
			$data['stock_out_data'][$key]['url'] = $this->url->link('sale/stock_out/view', 'token=' . $this->session->data['token'] . '&out_id=' . $val['out_id'], 'SSL');
		}
		if ($api_info) {
			$data['api_id'] = $api_info['api_id'];
			$data['api_key'] = $api_info['key'];
			$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
		} else {
			$data['api_id'] = '';
			$data['api_key'] = '';
			$data['api_ip'] = '';
		}

		$data['user_id'] = $this->session->data['user_id'];
		$data['token'] = $this->session->data['token'];
		$data['store'] = HTTPS_CATALOG;
		$data['url'] = $this->url->link('stock/lend/back', 'token=' . $this->session->data['token'] . '&loan_order_id=' . $loan_order_id, 'SSL');
		$this->response->setOutput($this->load->view('stock/lend_goods_view.tpl', $data));

	}
//2018.3.12
	public function truename(){
			$json = array();
			$data = json_decode(file_get_contents('php://input'),true);
			$this->load->model('user/user');
			$truename=$this->model_user_user->getfullname($data['user_id']);
		 if($truename){

		 		 $json = array('success' => $truename);
			   $this->response->addHeader('Content-Type: application/json');
		     $this->response->setOutput(json_encode($json));

		 }else{

			   $json = array('error' => '数据错误');
			   $this->response->addHeader('Content-Type: application/json');
		     $this->response->setOutput(json_encode($json));
		}

  }

//还货
  public function repay(){
  		$data['id']=trim($this->request->post['id']);
  		$data['product_code']=trim($this->request->post['product_code']);
  		$data['warehouse_id']=trim($this->request->post['warehouse_id']);
  		$data['badnub']=trim($this->request->post['badnub']);
  		$data['nub']=trim($this->request->post['nub']);
  		$this->load->model('stock/lend');
  		$plusnub=$this->model_stock_lend->plusnub($data);
  		 if($plusnub){
	  	 	  	$json = array('success' => "success");
				   $this->response->addHeader('Content-Type: application/json');
			     $this->response->setOutput(json_encode($json));
			  }else{
			 	   $json = array('error' => '数据错误');
			 	   $this->response->addHeader('Content-Type: application/json');
			      $this->response->setOutput(json_encode($json));
			 }
  }

  //删除借货单据
  public function delloan(){
  		$loan_order_id=$this->request->post['loanorderid'];
  		$this->load->model('stock/lend');
  		$result=$this->model_stock_lend->del_loan_order($loan_order_id);
  		if($result){
  				$json = array('success' => "success");
			 	   $this->response->addHeader('Content-Type: application/json');
			      $this->response->setOutput(json_encode($json));
  		}else{
  				$json = array('error' => "删除失败");
			 	   $this->response->addHeader('Content-Type: application/json');
			      $this->response->setOutput(json_encode($json));
  		}

  }

}

