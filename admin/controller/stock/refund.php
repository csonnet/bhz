<?php
class ControllerStockRefund extends Controller {
	private $error = array();
	
	//仓库初始化
	public function index() {
		$this->load->language('stock/refund');
		

		$this->document->setTitle($this->language->get('heading_title'));
		// var_dump($this->language->get('heading_title'));die();
		$this->load->model('stock/refund');
		$this->load->model('stock/money_in');

		$this->getList();

    }

	private function getList(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
	    // var_dump($data);
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/bhz_ctl.js');
	    if(!empty($data['filter_date_start'])){
	      $filter_date_start = $data['filter_date_start'];
	    }else{
	      $filter_date_start = '0000-00-00';
	    }

	    if(!empty($data['filter_date_end'])){
	      $filter_date_end = $data['filter_date_end'];
	    }else{
	      $filter_date_end = '9999-12-31';
	    }

	    $filter['money_out.date_added'] = array('between',array($filter_date_start,$filter_date_end));

	    if(isset($data['filter_status'])){
	      $filter_status = trim($data['filter_status']);
	      if($filter_status!='*'){
	        $filter['money_out.status'] = $filter_status;
	      }  
	    }else{
	      $data['filter_status'] = '*';
	    }
	    if(isset($data['filter_type'])){
	      $filter_type = trim($data['filter_type']);
	      if($filter_type!='*'){
	        $filter['money_out.refer_type_id'] = $filter_type;
	      }  
	    }else{
	     	 $data['filter_type'] = '*';
	    }

	    if(!empty($data['payee'])){
	      $payee = trim($data['payee']);
	      $filter['money_out.payee'] = array('like',"%".$payee."%");
	    }

	    $filter['money_out.operator_id'] = array('like',"%".$this->user->getId()."%");
		//获取数据
		$data['status_array'] = $this->model_stock_refund->getstatus();
		$data['status_array'][] = array('money_out_status_id'=>'*','name'=>'全部');
		// var_dump($data['status_array']);
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(2);
		$data['money_type'][] = array('money_type_id'=>'*','type_name'=>'全部');


	    $money_list =  $this->model_stock_refund->getList($page, $filter);
	    foreach ($stocl_list as $key => $value) {
	    	if ($value['status']!=2) {
	    		$stocl_list[$key]['all_color']='red';
	    	}
	    }
	    $total = $this->model_stock_refund->getListCount($filter);
	    $pagination = new Pagination();
	    $pagination->total = $total;
	    $pagination->page = $page;
	    $pagination->limit = $this->config->get('config_limit_admin');
	    $pagination->url = $this->url->link('stock/refund', 'token=' . $this->session->data['token'] . '&page={page}'.'&filter_date_end='.$filter_date_end.'&filter_date_start='.$filter_date_start.'&filter_status='.$data['filter_status'].'&payee='.$data['payee'].'&filter_type='.$data['filter_type'], 'SSL');
		$data['pagination'] = $pagination->render();
	    $data['money_list'] = $money_list;
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));


			/*面包屑导航栏*/
			$data['heading_title'] = $this->language->get('heading_title');
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('stock/refund', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
			/*面包屑导航栏*/
			
			/*右上操作按钮*/

		$data['button_add'] = $this->language->get('button_add');
		$data['buttons_enable'] = $this->language->get('buttons_enable');
		$data['buttons_disable'] = $this->language->get('buttons_disable');
		$data['text_confirm_enable'] = $this->language->get('text_confirm_enable');
		$data['text_confirm_disable'] = $this->language->get('text_confirm_disable');
		
		$data['add'] = $this->url->link('stock/refund/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		

		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		    // var_dump($data['refer_type_id']);
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/refund_list.tpl', $data));

	}

 	public function add() {
		$this->load->model('stock/money_in');
		$this->load->model('stock/refund');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
	    $this->getForm();
	}

	protected function getForm(){
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '退款',
		  'href' => $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '退款';
		$data['status'] = $this->model_stock_money_in->getstatus();
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(2);


		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/refund_add.tpl', $data));
	}

	public function save()
	{
		$this->load->model('stock/stock_in');
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		$this->load->model('stock/refund');
	 	$this->model_stock_refund->addout($data,$this->user->getId(), $this->user->getUserName());

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success'=>true)));

    }

    public function cancel()
	{
		$this->load->model('stock/refund');
		$json = array();
		$money_out_id = I('get.money_out_id');
    	if (empty($money_out_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_refund->cancel($money_out_id)) {
 				$comments = "作废应付" ; 
      			$this->model_stock_refund->addMoHistory($money_out_id, $this->user->getId(),$comments);
 				$json['success'] = '作废成功';
 			}else{
 				$json['success'] = '作废失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    
	}

	public function recover()
	{
		$this->load->model('stock/refund');
		$json = array();
		$money_out_id = I('get.money_out_id');
    	if (empty($money_out_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_refund->recover($money_out_id)) {
 				$comments = "取消作废" ; 
      			$this->model_stock_refund->addMoHistory($money_out_id, $this->user->getId(),$comments);
 				$json['success'] = '作废成功';
 			}else{
 				$json['success'] = '作废失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    
	}

	public function pay()
	{
		$this->load->model('stock/refund');
		$money_out_id = I('get.money_out_id');
    	if (empty($money_out_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_refund->pay($money_out_id)) {
 				$comments = "确认支付" ; 
      			$this->model_stock_refund->addMoHistory($money_out_id, $this->user->getId(),$comments);
 				$json['success'] = '作废成功';
 			}else{
 				$json['success'] = '作废失败';
 			}
	    	
	    }

    	$this->index();
	}

	public function edit()
	{
		$this->load->model('stock/refund');
		$this->load->model('stock/money_in');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
	    $this->form();
    
	}

	public function form()
	{
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '退款',
		  'href' => $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '退款';
		$money_out_id = I('get.money_out_id');
		$data['money_out'] = $this->model_stock_refund->getOutById($money_out_id);
		$data['status'] = $this->model_stock_money_in->getstatus();
		$data['money_type'] = $this->model_stock_money_in->getMoneyType(2);


		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/refund_edit.tpl', $data));
    
	}

	public function edit_out()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		$this->load->model('stock/refund');
		$money_out_id = I('get.money_out_id');
		
	 	$this->model_stock_refund->editout($data,$money_out_id);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success'=>true)));

    }

    public function view()
	{
		$this->load->model('stock/refund');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/stock.js');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '退款',
		  'href' => $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$this->document->addScript('view/javascript/angular.js');
    	$this->document->addScript('view/javascript/stock.js');
		$data['heading_title'] = '退款';
		$money_out_id = I('get.money_out_id');
		$data['money_out'] = $this->model_stock_refund->getOutById($money_out_id);
		if (!empty($data['money_out']['in_id'])&&!empty($data['money_out']['refer_type_id'])&&!empty($data['money_out']['refer_id'])) {
			$data['products'] = $this->model_stock_refund->getProduct($data['money_out']['in_id'],$data['money_out']['refer_type_id'],$data['money_out']['refer_id']);
		}

    	$data['histories'] =  $this->model_stock_refund->getHistory($money_out_id);

		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/refund', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/refund_list_view.tpl', $data));

    }
    public function searchOrder() {
		
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = trim($this->request->get['filter_name']);
		}
		else{
			$filter_name = '';
		}
		if (isset($this->request->get['money_type'])) {
			$money_type = trim($this->request->get['money_type']);
		}

		$this->load->model('stock/refund');
		$filter_data['order_id'] = array('like',"%".$filter_name."%");
		$results = $this->model_stock_refund->getAutoCompleteOr($filter_data);
		foreach ($results as $result){
			$json[] = array(
				'refer_id' => $result['order_id'],
				'payee' => $result['payment_company'],
				'payee_id' => $result['customer_id'],

			);
		}

		

		// var_dump($results);
		
		// var_dump($json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
