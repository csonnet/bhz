<?php
class ControllerStockCost extends Controller {
	private $error = array();
	
	//仓库初始化
	public function index() {
		$this->load->language('stock/cost');
		

		$this->document->setTitle($this->language->get('heading_title'));
		// var_dump($this->language->get('heading_title'));die();
		$this->load->model('stock/cost');
		$this->load->model('stock/money_in');

		$this->getList();

    }

	private function getList(){

		$page = I('get.page');
	    if(!$page){
	      $page = 1;
	    }
	    $data = I('get.');
	    // var_dump($data);
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/cost.js');
	    if(!empty($data['filter_date_start'])){
	      $filter_date_start = $data['filter_date_start'];
	    }else{
	      $filter_date_start = '0000-00-00';
	    }

	    if(!empty($data['filter_date_end'])){
	      $filter_date_end = $data['filter_date_end'];
	    }else{
	      $filter_date_end = '9999-12-31';
	    }

	    $filter['cost.date_added'] = array('between',array($filter_date_start,$filter_date_end));

	    if(isset($data['filter_status'])){
	      $filter_status = trim($data['filter_status']);
	      if($filter_status!='*'){
	        $filter['cost.cost_status_id'] = $filter_status;
	      }  
	    }else{
	      $data['filter_status'] = '*';
	    }
	    if(isset($data['filter_type'])){
	      $filter_type = trim($data['filter_type']);
	      if($filter_type!='*'){
	        $filter['cost.cost_type_id'] = $filter_type;
	      }  
	    }else{
	     	 $data['filter_type'] = '*';
	    }

	    // if(!empty($data['payee'])){
	    //   $payee = trim($data['payee']);
	    //   $filter['money_out.payee'] = array('like',"%".$payee."%");
	    // }
		//获取数据
		$data['status_array'] = $this->model_stock_cost->getstatus();
		$data['status_array'][] = array('cost_status_id'=>'*','name'=>'全部');
		// var_dump($data['status_array']);
		$data['cost_type'] = $this->model_stock_cost->getCostType();
		$data['cost_type'][] = array('cost_type_id'=>'*','cost_name'=>'全部');


	    $cost_list =  $this->model_stock_cost->getList($page, $filter);
	    $total = $this->model_stock_cost->getListCount($filter);
	    $pagination = new Pagination();
	    $pagination->total = $total;
	    $pagination->page = $page;
	    $pagination->limit = $this->config->get('config_limit_admin');
	    $pagination->url = $this->url->link('stock/cost', 'token=' . $this->session->data['token'] . '&page={page}'.'&filter_date_end='.$filter_date_end.'&filter_date_start='.$filter_date_start.'&filter_status='.$data['filter_status'].'&filter_type='.$data['filter_type'], 'SSL');
		$data['pagination'] = $pagination->render();
	    $data['cost_list'] = $cost_list;
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));


			/*面包屑导航栏*/
			$data['heading_title'] = $this->language->get('heading_title');
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('stock/cost', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
			/*面包屑导航栏*/
			
			/*右上操作按钮*/

		$data['button_add'] = $this->language->get('button_add');
		$data['buttons_enable'] = $this->language->get('buttons_enable');
		$data['buttons_disable'] = $this->language->get('buttons_disable');
		$data['text_confirm_enable'] = $this->language->get('text_confirm_enable');
		$data['text_confirm_disable'] = $this->language->get('text_confirm_disable');
		
		$data['add'] = $this->url->link('stock/cost/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		

		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		    // var_dump($data['refer_type_id']);
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('stock/cost_list.tpl', $data));

	}

 	public function add() {
		$this->load->model('stock/money_in');
		$this->load->model('stock/cost');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/cost.js');
	    $this->getForm();
	}

	protected function getForm(){
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '费用',
		  'href' => $this->url->link('stock/cost', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$data['heading_title'] = '费用';
		$data['cost_users'] = $this->model_stock_cost->getCostUses();
		$data['cost_type'] = $this->model_stock_cost->getCostType();

		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/cost', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/cost_add.tpl', $data));
	}
	public function edit()
	{
		$this->load->model('stock/cost');
	    $this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/cost.js');
	    $this->form();
    
	}

	public function form()
	{
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
		  'text' => $this->language->get('text_home'),
		  'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		  'text' => '费用',
		  'href' => $this->url->link('stock/cost', 'token=' . $this->session->data['token'] , 'SSL')
		);
		$data['heading_title'] = '费用';

		$data = I('get.');
    	$cost_list =  $this->model_stock_cost->getcost($data['cost_id']);
    	$cost_list['days'] = date('Y-m',$cost_list['days']);    
      	foreach ($cost_list['auditor'] as $key => $value) {
      		$cost_list['auditor'][$key]['fullname'] = $value['auditor_use_name'];
      		$cost_list['auditor'][$key]['contact_tel'] = $value['auditor_use_tel'];
      		$cost_list['auditor'][$key]['user_id'] = $value['auditor_use_id'];
     	 }
		$data['cost_list'] = $cost_list;
		$data['cost_users'] = $this->model_stock_cost->getCostUses();
		$data['cost_type'] = $this->model_stock_cost->getCostType();
		$data['token'] = $this->session->data['token'];

		$data['cancel'] = $this->url->link('stock/cost', 'token=' . $this->session->data['token'] , 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    	$this->response->setOutput($this->load->view('stock/cost_edit.tpl', $data));
    
	}

	public function cost_use()
	{
		$this->load->model('stock/cost');
		$filter_name = I('get.filter_name');
		$filter['user.fullname'] = array('like',"%".$filter_name."%");
		
	 	$json = $this->model_stock_cost->getUser($filter);

	 	 

	 	$this->response->addHeader('Content-Type: application/json');
    	$this->response->setOutput(json_encode($json));

    }


	public function save_cost(){
		$this->load->model('stock/cost');
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
		$json = array();
	 	$id = $this->model_stock_cost->addcost($data,$this->user->getId());
	 	$common='新增费用';
	 	$this->model_stock_cost->addHistory($id,$this->user->getId(),$common);
		$json['success'] = true;
    	

		 $this->response->addHeader('Content-Type: application/json');
		 $this->response->setOutput(json_encode($json));
		return;
    
	}

	public function edit_cost(){
		$this->load->model('stock/cost');
		$data = json_decode(file_get_contents('php://input'), true);
		$filter = C('DEFAULT_FILTER');
		$data   =   is_array($data) ? array_map_recursive($filter,$data) : $filter($data);
    	$cost_id = I('get.cost_id');
		$json = array();
	 	$id = $this->model_stock_cost->editcost($data,$this->user->getId(),$cost_id);
	 	$common='修改费用';
	 	$this->model_stock_cost->addHistory($id,$this->user->getId(),$common);
		$json['success'] = true;
    	

		 $this->response->addHeader('Content-Type: application/json');
		 $this->response->setOutput(json_encode($json));
		return;
    
	}


	public function view(){
		$this->load->language('stock/cost');
		// var_dump($this->language->get('heading_title'));die();
		$this->document->addScript('view/javascript/angular.js');
	    $this->document->addScript('view/javascript/cost.js');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('stock/cost');
		$url = '';
    	$data = I('get.');
    	$cost_list =  $this->model_stock_cost->getcost($data['cost_id']);
    	$stockHistory =  $this->model_stock_cost->getstockHistory($data['cost_id']);
    	$data['histories']=$stockHistory;
		$data['cost_list'] = $cost_list;
		/*面包屑导航栏*/
		$data['heading_title'] = '费用';
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('stock/cost', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		/*面包屑导航栏*/
		
		/*右上操作按钮*/
		$data['button_add'] = $this->language->get('button_add');
		$data['buttons_enable'] = $this->language->get('buttons_enable');
		$data['buttons_disable'] = $this->language->get('buttons_disable');
		$data['text_confirm_enable'] = $this->language->get('text_confirm_enable');
		$data['text_confirm_disable'] = $this->language->get('text_confirm_disable');
		$data['cancel'] = $this->url->link('stock/cost', 'token=' . $this->session->data['token'] , 'SSL');
		$data['add'] = $this->url->link('stock/cost/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['enable_action'] = $this->url->link('stock/cost/enableStatus', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['disable_action'] = $this->url->link('stock/cost/disableStatus', 'token=' . $this->session->data['token'] . $url, 'SSL');
		/*右上操作按钮*/
		
		/*标题*/
		$data['text_list'] = $this->language->get('text_list');
		/*标题*/
		
		//加载头部侧边栏底部
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// var_dump($data);
    	$this->response->setOutput($this->load->view('stock/cost_view.tpl', $data));
	}

	public function delete(){
		$this->load->model('stock/cost');
		$json = array();
		$cost_id = I('get.cost_id');
    	if (empty($cost_id)) {
	    	$json['error'] = '参数缺失';
    	}else{
 			if ($this->model_stock_cost->delete($cost_id)) {
 				$json['success'] = '成功';
 			}else{
 				$json['success'] = '失败';
 			}
	    	
	    }

	    $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
	}



	

}
