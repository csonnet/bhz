<?php
class ControllerAreaArea extends Controller {
	
	//������autocomplete
	public function autoArea(){

		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = trim($this->request->get['filter_name']);
		}
		else{
			$filter_name = '';
		}

		$filter_data = array(
			'filter_name' => $filter_name,
			'type' => $this->request->get['type'],
			'start' => 0,
			'limit' => 10
		);

		$this->load->model('area/area');

		$results = $this->model_area_area->getArea($filter_data);

		$json = $results;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

}