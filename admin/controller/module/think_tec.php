<?php
class ControllerModuleThinkTec extends Controller {
  public function install() {
    $this->load->model('thinktec/db');
    $this->model_thinktec_db->install();
  }
  
  public function uninstall() {
    $this->load->model('thinktec/db');
    $this->model_thinktec_db->uninstall();
  }
}