<?php
class ControllerToolShelfImport extends Controller {
    public function index() {
        if ($this->session->data['ret']) {
            if (true === $this->session->data['ret']['status']) {
                $data['success'] = $this->session->data['ret']['message'];
            }else{
                $data['error_warning'] = $this->session->data['ret']['message'];
            }
            unset($this->session->data['ret']);
        }

        $data['post_max_size'] = $this->_returnBytes( ini_get('post_max_size') );
        $data['upload_max_filesize'] = $this->_returnBytes( ini_get('upload_max_filesize') );
        $data['action'] = $this->url->link('tool/shelf_import/importExcel', 'token=' . $this->session->data['token'], 'SSL');

        $data['breadcrumbs'] = array(
            array(
                'text'      => '首页',
                'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => false,
            ),
            array(
                'text'      => '智能货架导入',
                'href'      => $this->url->link('tool/shelf_import', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            ),
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('tool/shelf_import.tpl', $data));
    }

    /*
     * excel导入主函数，保存到本地，记录导入日志，返回最终结果
     */
    public function importExcel() {
        if(($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->_validate())) {
            $uploadType = 'shelf';
            $dateAdded = date('Ymd');
            $fileUnique = md5(file_get_contents($this->request->files['upload']['tmp_name']));
            $fileExt = pathinfo($this->request->files['upload']['name'], PATHINFO_EXTENSION);
            $this->load->model('tool/other_orders');
            $excelUploadLogId = $this->model_tool_other_orders->addNew($fileUnique, $uploadType);
            @mkdir(DIR_APPLICATION.'excelUpload/SI_'.$dateAdded, 0777, true);
            $localFilePath = DIR_APPLICATION.'excelUpload/SI_'.$dateAdded.'/'.$fileUnique.'_'.$excelUploadLogId.'.'.$fileExt;
            if ((isset( $this->request->files['upload'] )) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {
                if (false === move_uploaded_file($this->request->files['upload']['tmp_name'], $localFilePath)) {
                    $ret = array(
                        'status' => false,
                        'message' => '上传文件失败，请检查文件大小 ！',
                    );
                }else{
                    $ret = $this->_importExcel($localFilePath, trim($this->request->files['upload']['name']));
                }
            }else{
                $ret = array(
                    'status' => false,
                    'message' => '上传文件不能为空 ！',
                );
            }
            if (!$ret['status']) {
                $this->model_tool_other_orders->importFailed($excelUploadLogId);
            }
        }else{
            $ret = array(
                'status' => false,
                'message' => '没有操作权限 ！',
            );
        }
        $this->session->data['ret'] = $ret;
        $this->response->redirect($this->url->link('tool/shelf_import', 'token=' . $this->session->data['token'], 'SSL'));
        //$this->response->addHeader('Content-Type: application/json');
        //$this->response->setOutput(json_encode($ret));
    }

    protected function _importExcel($excelPath, $file) {
        $ret = array(
            'status' => false,
            'message' => $file.' | 导入失败 ！',
        );

        $this->load->library('PHPExcel/PHPExcel');
        $PHPExcel = new PHPExcel();
        $PHPReader = new PHPExcel_Reader_Excel2007();
        if (!$PHPReader->canRead($excelPath)){
            $PHPReader = new PHPExcel_Reader_Excel5();
        }
        $PHPExcel = $PHPReader->load($excelPath);
        $currentSheet = $PHPExcel->getSheet(0);
        $allRow = $currentSheet->getHighestRow();
        $colList = array(//必填字段，固定顺序
            65 => array('方案名称', 'solutionName'),// A
            66 => array('货架编号', 'shelfName'),// B
            67 => array('货架规格', 'shelfType'),// C
            68 => array('货架品类', 'solutionCategory'),// D
            69 => array('货架定位', 'gradeSearch'),// E
            70 => array('层数', 'shelfLayer'),// F
            71 => array('顺序', 'layerPosition'),// G
            72 => array('组合', 'groupName'),// H
            73 => array('商品编码', 'productCode'),// I
            74 => array('品名', 'productName'),// J //商品名称，便于商品部门创建excel报表
            75 => array('货架陈列量', 'showQty'),// K
        );
        $errorList = array();
        for ($col=65; true; $col++) {//验证第一列表头信息是否完整
            $value = trim($currentSheet->getCell(chr($col).'1')->getValue());
            if ('' == $value){
                $endCol = $col;
                break;
            }else{
                if (array_key_exists($col, $colList)){
                    if ($value != $colList[$col][0]) {
                        $errorList[] = '第 '.chr($col).' 列 必须是 “'.$colList[$col][0].'”';
                    }
                }else{
                    $attrList[$col] = $value;
                }
            }
        }
        $solutionList = array();
        if (count($errorList)) {//表头列信息错误，直接返回
            $ret = array(
                'status' => false,
                'message' => $file.' | 导入失败，错误信息：<br />'.implode('<br />', $errorList),
            );
        }else{
            $batchNumber = time();//批次号，确保每次操作仅正对当前excel信息
            $this->load->model('tool/shelf_import');
            for ($row=2; $row<=$allRow; $row++) {
                $data = array();
                $thisWarn = '';
                for ($col=65; $col<$endCol; $col++) {//读取当前行信息，并验证必填字段合法性
                    //$value = trim($currentSheet->getCell(chr($col).$row)->getValue());//获取值或公式本身
                    $value = trim($currentSheet->getCell(chr($col).$row)->getCalculatedValue());//获取值，公式也被值代替
                    $checkMethodName = '_check'.ucfirst($colList[$col][1]);
                    if (method_exists(get_class($this), $checkMethodName)) {
                        $value = $this->{$checkMethodName}($value);
                    }
                    if ('' === $value) {
                        $thisWarn = '第 '.$row.' 行 商品导入失败 ！'.chr($col).'列（'.$colList[$col][0].'）数据异常。';
                        break;
                    }else{
                        if ($colList[$col][1]) {
                            $data[$colList[$col][1]] = $value;//必填字段
                        }else{
                            $data['attr'][$attrList[$col]] = $value;//必填以外的字段作为attr数组保存为json格式，便于未来使用
                        }
                    }
                }
                if ('' != $thisWarn) {//验证当前行的值是否合法
                    $warnList[] = $thisWarn;
                }else{
                    $temp = $this->model_tool_shelf_import->importData($data, $batchNumber);//导入数据
                    $solutionList[$temp['solutionId']] = 1;//保存当前行的方案编号
                    if (-1 == $temp['shelfProductId']) {
                        $warnList[] = '第 '.$row.' 行 商品导入失败 ！当前位置已经存在商品。';
                    }
                }
            }
        }
        if (count($solutionList)) {
            $this->model_tool_shelf_import->updateData($solutionList);
            $ret['status'] = true;
            $ret['message'] = $file.' | 导入成功 ！';
        }
        if (count($warnList)) {
            $ret['message'] .= '<br />'.implode('<br />', $warnList);
        }
        return $ret;
    }

    protected function _returnBytes($val) {
        $val = trim($val);
        switch (strtolower(substr($val, -1))) {
            case 'm': $val = (int)substr($val, 0, -1) * 1048576; break;
            case 'k': $val = (int)substr($val, 0, -1) * 1024; break;
            case 'g': $val = (int)substr($val, 0, -1) * 1073741824; break;
            case 'b':
                switch (strtolower(substr($val, -2, 1))) {
                    case 'm': $val = (int)substr($val, 0, -2) * 1048576; break;
                    case 'k': $val = (int)substr($val, 0, -2) * 1024; break;
                    case 'g': $val = (int)substr($val, 0, -2) * 1073741824; break;
                    default : break;
                } break;
            default: break;
        }
        return $val;
    }

    protected function _checkShelfType($value) {//验证货架类型是否存在
        if (false !== ($ret = array_search($value, getShelfType()))) {
            return $ret;
        }
        return '';
    }

    protected function _checkSolutionCategory($value) {//验证货架方案分类是否存在
        $list = getSolutionCategory();
        foreach ($list as $info) {
            if ($value == $info['cid']) {
                return $info['cid'];
            }
        }
        return '';
    }

    protected function _checkGradeSearch($value) {//转换货架档次到数字范围
        $list = array(
            '低' => array(1, 2),
            '中' => array(3, 6),
            '高' => array(7, 9),
            '标配' => array(1, 9),
        );
        if (array_key_exists($value, $list)) {
            $start = intval($list[$value][0]);
            $end = intval($list[$value][1]);
            return compact('start', 'end');
        }
        return '';
    }

    protected function _checkShelfLayer($value) {//格式化层号，大写字母到数字
        $ret = ord(substr(strtoupper($value), 0, 1))-64;
        if ($ret > 0 && $ret < 27) {
            return $ret;
        }
        return '';
    }

    protected function _checkLayerPosition($value) {//数字化序号，避免可能出现的精度问题
        $ret = intval($value);
        if ($ret > 0){
            return $ret;
        }
        return '';
    }

    protected function _checkProductCode($value) {//验证商品唯一码是否存在
        $code = str_pad($value, 11, 0);
        $this->load->model('catalog/mvd_product');
        if (0 !== ($id = $this->model_catalog_mvd_product->checkProductCodeExists($code))) {
            return compact('id', 'code');
        }
        return '';
    }

    protected function _checkShowQty($value) {//数字化商品陈列量，避免可能出现的精度问题
        $ret = intval($value);
        if ($ret > 0){
            return $ret;
        }
        return '';
    }

    private function _validate() {
        if (!$this->user->hasPermission('modify', 'tool/shelf_import')) {
            $this->error['warning'] = $this->language->get('警告: 无权限修改本插件！');
        }
        if (!$this->error) {
            return true;
        }else{
            return false;
        }
    }

}
