<?php
class ControllerToolSexcel extends Controller{
	public function import() {
		$json = array();

		if (!$json) {
			$this->load->library('PHPExcel/PHPExcel');
			$savePath = DIR_SYSTEM . "template\s.xlsx";

			$PHPExcel = new PHPExcel();
			$PHPReader = new PHPExcel_Reader_Excel2007();
			if(!$PHPReader->canRead($savePath)){
				$PHPReader = new PHPExcel_Reader_Excel5();
			}
			$PHPExcel = $PHPReader->load($savePath);
			$currentSheet = $PHPExcel->getSheet(0);
			$allRow = $currentSheet->getHighestRow();
			//获取order_product_id, express_name, express_number在excel中的index
			$product_name_idx=$option_name_idx = $special_idx = $price_idx = $sku_idx = $packing_no_idx = $item_no_idx = 0;
			$tmp_idx = 'A'; //假设导入表格不会长于Z
			$currentRow = 1;
			while(($product_name_idx === 0 || $option_name_idx === 0 || $special_idx === 0 || $price_idx === 0||$sku_idx === 0||$packing_no_idx ===0 ||$item_no_idx === 0)&&$tmp_idx<'Z') {
				switch (trim((String)$currentSheet->getCell($tmp_idx.$currentRow)->getValue())) {
					case '商品名称':
						$product_name_idx = $tmp_idx;
						break;
					case '选项':
						$option_name_idx = $tmp_idx;
						break;
					case '售价':
						$special_idx = $tmp_idx;
						break;
					case '零售参考价':
						$price_idx = $tmp_idx;
						break;
					case '条形码':
						$sku_idx = $tmp_idx;
						break;
					case '箱入数':
						$packing_no_idx = $tmp_idx;
						break;
					case '最小起订量':
						$item_no_idx = $tmp_idx;
						break;
				}
				$tmp_idx++;
			}
			
			if (!$json) {
				$express_infos = array();
				for ($currentRow = 2;$currentRow <= $allRow;$currentRow++) {
					$express_infos[] = array(
						'product_name' => (String)$currentSheet->getCell($product_name_idx.$currentRow)->getValue(),
						'option' => (String)$currentSheet->getCell($option_name_idx.$currentRow)->getValue(),
						'special' => (String)$currentSheet->getCell($special_idx.$currentRow)->getValue(),
						'price' => (String)$currentSheet->getCell($price_idx.$currentRow)->getValue(),
						'sku' =>(String)$currentSheet->getCell($sku_idx.$currentRow)->getValue(),
						'packing_no' =>(String)$currentSheet->getCell($packing_no_idx.$currentRow)->getValue(),
						'minimum' =>(String)$currentSheet->getCell($item_no_idx.$currentRow)->getValue(),
						);
				}

				
			}
		}
		$product_model = M('product');
		foreach ($express_infos as $key => $value) {
			$where['sku'] = $value['sku'];
			$data['price'] = $value['price'];
			$data['minimum'] = $value['minimum'];
			$product_model
			->where($where)
			->data($data)
			->save();
		}


	}
}


?>