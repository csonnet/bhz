<?php
class ControllerReportProductPurchased extends Controller {
	public function index() {
		$this->load->language('report/product_purchased');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/product');

		$data['products'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		$product_total = $this->model_report_product->getTotalPurchased($filter_data);

		$results = $this->model_report_product->getPurchased($filter_data);

		foreach ($results as $result) {
			$data['products'][] = array(
				'name'       => $result['name'],
				'model'      => $result['model'],
				'quantity'   => $result['quantity'],
				'total'      => $this->currency->format($result['total'], $this->config->get('config_currency')),
				'opt_name'   => $result['opt_name'],
				'opt_value'   => $result['opt_value'],
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_export'] = $this->language->get('button_export');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/product_purchased.tpl', $data));
	}

	public function exportLists(){

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status = null;
		}

	
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = null;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'o.order_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}


		$filter_data = array(
			'filter_order_status'  => $filter_order_status,
			'filter_date_start'	   => $filter_date_start,
			'filter_date_end'	   => $filter_date_end,
			'sort'                 => $sort,
			'order'                => $order,
		);
		$this->load->model('sale/order');

		$results = $this->model_sale_order->getExportOrders($filter_data);
		$bill_array = array(
			'1'=>'未开发票',
			'2'=>'已开发票',
			'0'=>'不需要开发票',
			);
		foreach ($results as $result) {
			
			$data[] = array(
				'order_id'      	=> $result['order_id'],
				// 'customer'      	=> $result['customer'],
				'status'        	=> $result['status'],
				// 'total'         	=> $result['total'],
				'date_added'    	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				// 'date_modified' 	=> date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
				// 'bill_status'		=> $bill_array[$result['bill_status']],
				// 'recommended_code'	=> $result['recommended_code'],
				'vendor_name'		=> $result['vendor_name'],
				'product_name'		=> $result['product_name'],
				'sku'				=> $result['sku'],
				'shipping_country'	=> $result['shipping_country'],
				'shipping_zone' 	=> $result['shipping_zone'],
				'model'				=> $result['model'],
				'quantity'			=> $result['quantity'],
				'price'				=> $result['price'],
				'cd_name'			=> $result['cd_name'],
				'total'				=> $result['total'],
				// 'telephone'			=> $result['telephone'],
				// 'shipping_address'	=> $result['shipping_address'],
			);
		}
	    $this->load->library('PHPExcel/PHPExcel');
	    $objPHPExcel = new PHPExcel();    
	    $objProps = $objPHPExcel->getProperties();    
	    $objProps->setCreator("Think-tec");
	    $objProps->setLastModifiedBy("Think-tec");    
	    $objProps->setTitle("Think-tec Contact");    
	    $objProps->setSubject("Think-tec Contact Data");    
	    $objProps->setDescription("Think-tec Contact Data");    
	    $objProps->setKeywords("Think-tec Contact");    
	    $objProps->setCategory("Think-tec");
	    $objPHPExcel->setActiveSheetIndex(0);     
	    $objActSheet = $objPHPExcel->getActiveSheet(); 
	       
	    $objActSheet->setTitle('Sheet1');
	    $col_idx = 'A';
	    $headers = array( '订单日期',  '订单ID', '商品名称',	 '品牌产商',	'条形码','型号',	'数量',	   '单品价格','单品小计','状态',  		   '省市',			  '市县','分类');
	    $row_keys = array('date_added','order_id','product_name','vendor_name', 'sku',	 'model','quantity','price',   'total',   'status','shipping_country','shipping_zone','cd_name');
	    foreach ($headers as $header) {
	      $objActSheet->setCellValue($col_idx++.'1', $header);  
	    }
	    //添加物流信息
	    $i = 2;
	    foreach ($data as $rlst) {
	      $col_idx = 'A';
	      foreach ($row_keys as $rk) {
	        $objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
	      }
	      $i++;
	    } 

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	    
	    ob_end_clean();
	    // Redirect output to a client’s web browser (Excel2007)
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment;filename="order_'.date('Y-m-d',time()).'.xlsx"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0
	    $objWriter->save('php://output'); 
	    exit;
  }
	public function export() {
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
		);

		$this->load->model('report/product');
		$results = $this->model_report_product->getPurchased($filter_data);
		$this->load->library('PHPExcel/PHPExcel');
		$objPHPExcel = new PHPExcel();    
		$objProps = $objPHPExcel->getProperties();    
		$objProps->setCreator("Think-tec");
		$objProps->setLastModifiedBy("Think-tec");    
		$objProps->setTitle("Think-tec Contact");    
		$objProps->setSubject("Think-tec Contact Data");    
		$objProps->setDescription("Think-tec Contact Data");    
		$objProps->setKeywords("Think-tec Contact");    
		$objProps->setCategory("Think-tec");
		$objPHPExcel->setActiveSheetIndex(0);     
		$objActSheet = $objPHPExcel->getActiveSheet(); 
		   
		$objActSheet->setTitle('Sheet1');
		$col_idx = 'A';
		$headers = array('商品名称', '选项名称',	'选项值', 	'型号',	'数量',		'总计');
		$row_keys = array('name',		'opt_name',	'opt_value','model','quantity', 'total');
		foreach ($headers as $header) {
			$objActSheet->setCellValue($col_idx++.'1', $header);	
		}
		//添加物流信息
		$i = 2;
		foreach ($results as $rlst) {
			$col_idx = 'A';
			foreach ($row_keys as $rk) {
				// $objActSheet->setCellValue($col_idx++.$i, $rlst[$rk]);	
				$objActSheet->setCellValueExplicit($col_idx++.$i, $rlst[$rk], PHPExcel_Cell_DataType::TYPE_STRING);
			}
			$i++;
		}	

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		ob_end_clean();
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Customers_'.date('Y-m-d',time()).'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
    $objWriter->save('php://output'); 
    exit;
	}
}