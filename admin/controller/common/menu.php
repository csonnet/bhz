<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');
		// $data['category_mgr_lite'] = $this->url->link('catalog/category_mgr_lite', 'token=' . $this->session->data['token'], 'SSL');
		$this->load->language('catalog/category_mgr_lite');
		$data['category_mgr_lite_heading_title'] = $this->language->get('category_mgr_heading_title');

		$data['text_analytics'] = $this->language->get('text_analytics');

		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_ground'] = $this->language->get('text_ground');
		$data['text_api'] = $this->language->get('text_api');
		$data['text_attribute'] = $this->language->get('text_attribute');
		$data['text_attribute_group'] = $this->language->get('text_attribute_group');
		$data['text_backup'] = $this->language->get('text_backup');
		$data['text_export'] = $this->language->get('text_export');
		$data['text_banner'] = $this->language->get('text_banner');
		$data['text_captcha'] = $this->language->get('text_captcha');
		$data['text_catalog'] = $this->language->get('text_catalog');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_country'] = $this->language->get('text_country');
		$data['text_coupon'] = $this->language->get('text_coupon');
		$data['text_currency'] = $this->language->get('text_currency');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_customer_group'] = $this->language->get('text_customer_group');
		$data['text_customer_field'] = $this->language->get('text_customer_field');
		$data['text_custom_field'] = $this->language->get('text_custom_field');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_paypal'] = $this->language->get('text_paypal');
		$data['text_paypal_search'] = $this->language->get('text_paypal_search');
		$data['text_design'] = $this->language->get('text_design');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_error_log'] = $this->language->get('text_error_log');
		$data['text_extension'] = $this->language->get('text_extension');
		$data['text_feed'] = $this->language->get('text_feed');
		$data['text_fraud'] = $this->language->get('text_fraud');
		$data['text_filter'] = $this->language->get('text_filter');
		$data['text_geo_zone'] = $this->language->get('text_geo_zone');
		$data['text_dashboard'] = $this->language->get('text_dashboard');
		$data['text_help'] = $this->language->get('text_help');
		$data['text_information'] = $this->language->get('text_information');
		$data['text_installer'] = $this->language->get('text_installer');
		$data['text_language'] = $this->language->get('text_language');
		$data['text_layout'] = $this->language->get('text_layout');
		$data['text_localisation'] = $this->language->get('text_localisation');
		$data['text_location'] = $this->language->get('text_location');
		$data['text_marketing'] = $this->language->get('text_marketing');
		$data['text_modification'] = $this->language->get('text_modification');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_module'] = $this->language->get('text_module');
		$data['text_option'] = $this->language->get('text_option');

		$data['text_order'] = $this->language->get('text_order');
		$data['text_order_status'] = $this->language->get('text_order_status');
		$data['text_mycncart'] = $this->language->get('text_mycncart');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_reports'] = $this->language->get('text_reports');
		$data['text_report_sale_order'] = $this->language->get('text_report_sale_order');
		$data['text_report_sale_tax'] = $this->language->get('text_report_sale_tax');
		$data['text_report_sale_shipping'] = $this->language->get('text_report_sale_shipping');
		$data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$data['text_report_sale_coupon'] = $this->language->get('text_report_sale_coupon');
		$data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
		$data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
		$data['text_report_customer_activity'] = $this->language->get('text_report_customer_activity');
		$data['text_report_customer_online'] = $this->language->get('text_report_customer_online');
		$data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
		$data['text_report_customer_reward'] = $this->language->get('text_report_customer_reward');
		$data['text_report_customer_credit'] = $this->language->get('text_report_customer_credit');
		$data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
		$data['text_report_affiliate'] = $this->language->get('text_report_affiliate');
		$data['text_report_affiliate_activity'] = $this->language->get('text_report_affiliate_activity');
		$data['text_purchase_order'] = $this->language->get('text_purchase_order');
		$data['text_review'] = $this->language->get('text_review');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_return_action'] = $this->language->get('text_return_action');
		$data['text_return_reason'] = $this->language->get('text_return_reason');
		$data['text_return_status'] = $this->language->get('text_return_status');
		$data['text_shipping'] = $this->language->get('text_shipping');
		$data['text_setting'] = $this->language->get('text_setting');
		$data['text_sms'] = $this->language->get('text_sms');
		$data['text_stock_status'] = $this->language->get('text_stock_status');
		$data['text_system'] = $this->language->get('text_system');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_tax_class'] = $this->language->get('text_tax_class');
		$data['text_tax_rate'] = $this->language->get('text_tax_rate');
		$data['text_tools'] = $this->language->get('text_tools');
		$data['text_total'] = $this->language->get('text_total');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_tracking'] = $this->language->get('text_tracking');
		$data['text_user'] = $this->language->get('text_user');
		$data['text_user_group'] = $this->language->get('text_user_group');
		$data['text_users'] = $this->language->get('text_users');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_voucher_theme'] = $this->language->get('text_voucher_theme');
		$data['text_weight_class'] = $this->language->get('text_weight_class');
		$data['text_length_class'] = $this->language->get('text_length_class');
		$data['text_zone'] = $this->language->get('text_zone');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_order_recurring'] = $this->language->get('text_order_recurring');
		$data['text_others'] = $this->language->get('text_others');
		$data['text_url_alias'] = $this->language->get('text_url_alias');
		$data['text_weidian'] = $this->language->get('text_weidian');
		$data['text_wdcategory'] = $this->language->get('text_wdcategory');
		$data['text_wdproduct'] = $this->language->get('text_wdproduct');
		$data['text_config'] = $this->language->get('text_config');

		/*进销存相关*/
		$data['text_inventory_manage'] = $this->language->get('text_inventory_manage');
		$data['text_stock_out'] = $this->language->get('text_stock_out');
		$data['text_stock_in'] = $this->language->get('text_stock_in');
		$data['text_warehouse'] = $this->language->get('text_warehouse');
		$data['text_stock_search'] = $this->language->get('text_stock_search');
		$data['text_stock_change'] = $this->language->get('text_stock_change');

		$data['text_lyctool'] = $this->language->get('text_lyctool');
		/*进销存相关*/

		/*应收应付相关*/
		$data['text_money'] = $this->language->get('text_money');
		$data['text_money_in'] = $this->language->get('text_money_in');
		$data['text_acount'] = $this->language->get('text_acount');
		$data['text_factory'] = $this->language->get('text_factory');
		$data['text_distributor'] = $this->language->get('text_distributor');
		$data['text_vdi_distributor'] = $this->language->get('text_vdi_distributor');
		$data['text_vdi_factory'] = $this->language->get('text_vdi_factory');
		$data['text_money_out'] = $this->language->get('text_money_out');
		$data['text_customer_account'] = $this->language->get('text_customer_account');
		$data['text_refund'] = $this->language->get('text_refund');
		$data['text_cost'] = $this->language->get('text_cost');
		$data['text_auditor'] = $this->language->get('text_auditor');
		/*应收应付相关*/

		$data['text_vendor_tool'] = $this->language->get('text_vendor_tool');
		$data['text_add_vendors'] = $this->language->get('text_add_vendors');
		$data['text_add_courier'] = $this->language->get('text_add_courier');
		$data['text_pro2ven'] = $this->language->get('text_pro2ven');
		$data['text_vendor_sales'] = $this->language->get('text_vendor_sales');
		$data['text_vendor_transaction'] = $this->language->get('text_vendor_transaction');
		$data['text_payment_history'] = $this->language->get('text_payment_history');
		$data['text_contract_history'] = $this->language->get('text_contract_history');
		$data['text_vendor_sales_orders'] = $this->language->get('text_vendor_sales_orders');
		$data['text_vendor_report'] = $this->language->get('text_vendor_report');
		$data['text_setup'] = $this->language->get('text_setup');
		$data['text_vendor_setting'] = $this->language->get('text_vendor_setting');
		$data['text_vendor_commission'] = $this->language->get('text_vendor_commission');
		$data['text_package_limit'] = $this->language->get('text_package_limit');
		$data['text_vendor_personal'] = $this->language->get('text_vendor_personal');
		$data['text_vendor_update_profile'] = $this->language->get('text_vendor_update_profile');
		$data['text_vendor_update_password'] = $this->language->get('text_vendor_update_password');
		$data['text_vendor_tools'] = $this->language->get('text_vendor_tools');
		$data['text_quicker_status_modifier'] = $this->language->get('text_quicker_status_modifier');
		$data['text_contract_status_modifier'] = $this->language->get('text_contract_status_modifier');
		$data['text_marketing_tools'] = $this->language->get('text_marketing_tools');
		$data['text_luckydraw'] = $this->language->get('text_luckydraw');
		$data['text_remit_money'] = $this->language->get('text_remit_money');
		$data['text_logcenter_tool'] = $this->language->get('text_logcenter_tool');
		$data['text_add_logcenters'] = $this->language->get('text_add_logcenters');
		$data['token'] =  $this->session->data['token'];

		$data['text_pim'] = $this->language->get('text_pim');
		$data['text_return_main_order'] = $this->language->get('text_return_main_order');
		$data['text_trim_stock'] = $this->language->get('text_trim_stock');
		$data['text_requisition_main'] = $this->language->get('text_requisition_main');
		$data['text_check_stock'] = $this->language->get('text_check_stock');
        $data['text_lend_order'] = $this->language->get('text_lend_order');
		if (!$this->user->getVP() && !$this->user->getLP()) {
      $menu_routes = array(
      								'analytics'=>'extension/analytics',
											'home'=>'common/dashboard',
											'affiliate'=>'marketing/affiliate',
											'ground'=>'marketing/ground',
											'api'=>'user/api',
											'attribute'=>'catalog/attribute',
											'attribute_group'=>'catalog/attribute_group',
											'backup'=>'tool/backup',
											'export'=>'tool/mccexcelexportimport',
											'banner'=>'design/banner',
											'captcha'=>'extension/captcha',
											'category'=>'catalog/category',
											'country'=>'localisation/country',
											'contact'=>'marketing/contact',
											'message'=>'marketing/message',
											'coupon'=>'marketing/coupon',
											'currency'=>'localisation/currency',
											'customer'=>'customer/customer',
											'customer_distributor'=>'customer/customer_distributor',
											'customer_fields'=>'customer/customer_field',
											'customer_group'=>'customer/customer_group',
											'custom_field'=>'customer/custom_field',
											'download'=>'catalog/download',
											'error_log'=>'tool/error_log',
											'feed'=>'extension/feed',
											'filter'=>'catalog/filter',
											'fraud'=>'extension/fraud',
											'geo_zone'=>'localisation/geo_zone',
											'information'=>'catalog/information',
											'installer'=>'extension/installer',
											'language'=>'localisation/language',
											'layout'=>'design/layout',
											'location'=>'localisation/location',
											'modification'=>'extension/modification',
											'manufacturer'=>'catalog/manufacturer',
											'marketing'=>'marketing/marketing',
											'module'=>'extension/module',
											'option'=>'catalog/option',
											'goodsshelf' => 'shelf/goodsshelf', //货架
											'order'=>'sale/order',
											'order_distributor'=>'sale/order_distributor',
											'order_status'=>'localisation/order_status',
											'payment'=>'extension/payment',
											'paypal_search'=>'payment/pp_express/search',
											'product'=>'catalog/product',
											'report_sale_order'=>'report/sale_order',
											'report_sale_tax'=>'report/sale_tax',
											'report_sale_shipping'=>'report/sale_shipping',
											'report_sale_return'=>'report/sale_return',
											'report_sale_coupon'=>'report/sale_coupon',
											'report_product_viewed'=>'report/product_viewed',
											'report_product_purchased'=>'report/product_purchased',
											'report_customer_activity'=>'report/customer_activity',
											'report_customer_online'=>'report/customer_online',
											'report_customer_order'=>'report/customer_order',
											'report_customer_reward'=>'report/customer_reward',
											'report_customer_credit'=>'report/customer_credit',
											'report_marketing'=>'report/marketing',
											'report_affiliate'=>'report/affiliate',
											'report_affiliate_activity'=>'report/affiliate_activity',
											'review'=>'catalog/review',
											'return'=>'sale/return',
											'return_action'=>'localisation/return_action',
											'return_reason'=>'localisation/return_reason',
											'return_status'=>'localisation/return_status',
											'shipping'=>'extension/shipping',
											'setting'=>'setting/store',
											'sms'=>'extension/sms',
											'stock_status'=>'localisation/stock_status',
											'tax_class'=>'localisation/tax_class',
											'tax_rate'=>'localisation/tax_rate',
											'total'=>'extension/total',
											'upload'=>'tool/upload',
											'user'=>'user/user',
											'user_group'=>'user/user_permission',
											'voucher'=>'sale/voucher',
											'voucher_theme'=>'sale/voucher_theme',
											'weight_class'=>'localisation/weight_class',
											'length_class'=>'localisation/length_class',
											'url_alias'=>'catalog/url_alias',
											'zone'=>'localisation/zone',
											'recurring'=>'catalog/recurring',
											'order_recurring'=>'sale/recurring',
											'wdcategory'=>'weidian/category',
											'wdproduct'=>'weidian/product',
											'config'=>'weidian/config',
											'openbay_link_extension'=>'extension/openbay',
											'openbay_link_orders'=>'extension/openbay/orderlist',
											'openbay_link_items'=>'extension/openbay/items',
											'openbay_link_ebay'=>'openbay/ebay',
											'openbay_link_ebay_settings'=>'openbay/ebay/settings',
											'openbay_link_ebay_links'=>'openbay/ebay/viewitemlinks',
											'openbay_link_etsy'=>'openbay/etsy',
											'openbay_link_etsy_settings'=>'openbay/etsy/settings',
											'openbay_link_etsy_links'=>'openbay/etsy_product/links',
											'openbay_link_ebay_orderimport'=>'openbay/ebay/vieworderimport',
											'openbay_link_amazon'=>'openbay/amazon',
											'openbay_link_amazon_settings'=>'openbay/amazon/settings',
											'openbay_link_amazon_links'=>'openbay/amazon/itemlinks',
											'openbay_link_amazonus'=>'openbay/amazonus',
											'openbay_link_amazonus_settings'=>'openbay/amazonus/settings',
											'openbay_link_amazonus_links'=>'openbay/amazonus/itemlinks',
											'mvd_add_vendors'=>'catalog/vendor',
											'mvd_add_courier'=>'catalog/courier',
											'mvd_product'=>'catalog/mvd_product',
											'mvd_download'=>'catalog/mvd_download',
											'mvd_option'=>'catalog/mvd_option',
											'mvd_category'=>'catalog/mvd_category',
											'mvd_information'=>'catalog/mvd_information',
											'mvd_attribute'=>'catalog/mvd_attribute',
											'mvd_attribute_group'=>'catalog/mvd_attribute_group',
											'mvd_prostatctrl'=>'catalog/prostatctrl',
											'mvd_setting'=>'catalog/vendor_setting',
											'mvd_commission'=>'catalog/commission',
											'mvd_limit'=>'catalog/prolimit',
											'mvd_contstatctrl'=>'sale/contstatctrl',
											'mvd_ven_transaction'=>'report/mvd_vendor_transaction',
											'mvd_payment_history'=>'sale/mvd_payment_history',
											'mvd_contract_history'=>'sale/mvd_contract_history',
											'mvd_ven_report'=>'report/vendor_sale_order',
											'mvd_coupon'=>'marketing/mvd_coupon',
											'mvd_add_logcenters'=>'catalog/logcenter',
											'pim'=>'common/filemanager/pim',
											'logcenter_bill'=>'sale/logcenter_bill',
											'vendor_bill'=>'sale/vendor_bill',
											'main_purchase_order'=>'sale/main_purchase_order',
											'return_main_order'=>'sale/return_main_order',
											'trim_logcenter_stock'=>'sale/trim_logcenter_stock',
											'trim_main_stock'=>'sale/trim_main_stock',
											'requisition_main'=>'sale/requisition_main',
											'category_mgr_lite'=>'catalog/category_mgr_lite',
											'check_main_stock'=>'sale/check_main_stock',
											'luckydraw'=>'sale/luckydraw',
											'remit_money'=>'sale/remit_money',

											/*进销存相关*/
											'warehouse'=>'stock/warehouse',
											'stock_search'=>'stock/stocksearch',
											'stock_in'=>'stock/stock_in',
											'money_in'=>'stock/money_in',
											'acount'=>'stock/acount',
											'distributor'=>'stock/distributor',
											'vdi_distributor'=>'stock/vdi_distributor',
											'factory'=>'stock/factory',
											'vdi_factory'=>'stock/vdi_factory',
											'money_out'=>'stock/money_out',
											'customer_account'=>'stock/customer_account',
											'refund'=>'stock/refund',
											'cost'=>'stock/cost',
											'auditor'=>'stock/auditor',
											'stock_out'=>'sale/stock_out',
											/*进销存相关*/

											/*工具相关*/
											'lyctool'=>'tool/lyctool',
											'other_orders'=>'tool/other_orders',
											'shelf_import'=>'tool/shelf_import',
											/*工具相关*/

                                            /* 新版报表 */
                                            'reportV2_customer' => 'reportV2/customer',
                                            'supplier' => 'reportV2/supplier',
                                            'buyer' => 'reportV2/buyer',
                                            /* 新版报表 */

                                            /* 借货模块 */
                                            'lend_order'=>'stock/lend',
                                            /* 借货模块 */
                                            /* 超市商品目录 */
                                            'shop_category'=>'shop/shop_category',
											'shop_filter'=>'shop/shop_filter',
											'shop_attribute'=>'shop/shop_attribute',

                                            /* 超市商品目录 */



											);
				//print_r($menu_routes);die;

			foreach ($menu_routes as $key => $value) {
					$data[$key] = $this->url->link($value, 'token=' . $this->session->data['token'], 'SSL');
					$data['permissions'][$key] = $this->user->hasPermission('access', $value);
			}

			// print_r($data['permissions']);die;
			//禁用不常用功能
			$need_disable_functions = array('fraud', 'feed', 'marketing', 'affiliate', 'coupon', 'contact', 'location', 'upload', 'backup', 'error_log', 'report_marketing', 'report_affiliate', 'report_affiliate_activity', 'modification', 'installer', 'api', 'order_recurring', 'recurring', 'report_sale_order', 'report_sale_tax', 'report_sale_shipping', 'report_sale_return', 'report_sale_coupon', 'report_product_viewed', 'report_customer_activity', 'report_customer_online', 'report_customer_order', 'report_customer_reward', 'report_customer_credit', 'voucher', 'voucher_theme', 'url_alias', 'review');
			foreach ($need_disable_functions as $ndf) {
				$data['permissions'][$ndf] = false;
			}

			$data['openbay_show_menu'] = $this->config->get('openbaypro_menu');
			$data['openbay_markets'] = array(
        'ebay' => $this->config->get('ebay_status'),
        'amazon' => $this->config->get('openbay_amazon_status'),
        'amazonus' => $this->config->get('openbay_amazonus_status'),
        'etsy' => $this->config->get('etsy_status'),
      );

			return $this->load->view('common/menu.tpl', $data);
		} elseif($this->user->getVP()) {
			$data['home'] = $this->url->link('common/vdi_dashboard', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_product'] = $this->url->link('catalog/vdi_product', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_category'] = $this->url->link('catalog/vdi_category', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_option'] = $this->url->link('catalog/vdi_option', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_attribute'] = $this->url->link('catalog/vdi_attribute', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_attribute_group'] = $this->url->link('catalog/vdi_attribute_group', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_information'] = $this->url->link('catalog/vdi_information', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_download'] = $this->url->link('catalog/vdi_download', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_report_product_viewed'] = $this->url->link('report/vdi_product_viewed', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_report_product_purchased'] = $this->url->link('report/vdi_product_purchased', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_contract_history'] = $this->url->link('sale/vdi_contract_history', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_update_vendor_profile'] = $this->url->link('catalog/vdi_vendor_profile', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_user_password'] = $this->url->link('user/vdi_user_password', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_sale_order'] = $this->url->link('sale/vdi_order', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_coupon'] = $this->url->link('marketing/vdi_coupon', 'token=' . $this->session->data['token'], 'SSL');
			$data['vdi_transaction'] = $this->url->link('report/vdi_transaction', 'token=' . $this->session->data['token'], 'SSL');

			if ($this->user->getExpireDate()!= '0000-00-00') {
				$data['expiration_date'] = '<span style="color:white;background-color:#4AA02C;padding:1px 5px 1px 5px;border-radius: 3px 3px 3px 3px">' . $this->user->getExpireDate() . '</span>';
			} else {
				$data['expiration_date'] = false;
			}

			return $this->load->view('common/vdi_menu.tpl', $data);
		} elseif($this->user->getLP()) {
			$data['lgi_product'] = $this->url->link('catalog/lgi_product', 'token=' . $this->session->data['token'], 'SSL');
			$data['lgi_update_vendor_profile'] = $this->url->link('catalog/lgi_logcenter_profile', 'token=' . $this->session->data['token'], 'SSL');
			$data['lgi_user_password'] = $this->url->link('user/lgi_user_password', 'token=' . $this->session->data['token'], 'SSL');
			$data['lgi_sale_order'] = $this->url->link('sale/lgi_order', 'token=' . $this->session->data['token'], 'SSL');
			$data['trim_logcenter_stock'] = $this->url->link('sale/trim_logcenter_stock', 'token=' . $this->session->data['token'], 'SSL');
			$data['requisition_logcenter'] = $this->url->link('sale/requisition_logcenter', 'token=' . $this->session->data['token'], 'SSL');
			$data['check_logcenter_stock'] = $this->url->link('sale/check_logcenter_stock', 'token=' . $this->session->data['token'], 'SSL');
			if ($this->user->getExpireDate()!= '0000-00-00') {
				$data['expiration_date'] = '<span style="color:white;background-color:#4AA02C;padding:1px 5px 1px 5px;border-radius: 3px 3px 3px 3px">' . $this->user->getExpireDate() . '</span>';
			} else {
				$data['expiration_date'] = false;
			}

			$data['view_only'] = $this->user->getLpLevel() == 2;

			return $this->load->view('common/lgi_menu.tpl', $data);
		}
	}
}
