<?php
class ControllerCommonDashboard extends Controller {
	public function index() {
		//Liqn 营销物流中心和品牌厂商调整
		if ($this->user->getLP()) {
			$this->response->redirect($this->url->link('sale/lgi_order', 'token=' . $this->session->data['token'], 'SSL'));
		}
		if ($this->user->getVP()) {
			$this->response->redirect('catalog/vdi_product', 'token=' . $this->session->data['token'], 'SSL');
		}
        /*
         * 没有仪表盘查看权限的用户默认跳转到第一个有查看权限的页面
         * @author sonicsjh
         */
        if (false === $this->user->hasPermission('access', 'common/dashboard')){
            $this->response->redirect($this->url->link($this->user->getFirstAccessPermission(), 'token=' . $this->session->data['token'], 'SSL'));
        }
		
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_map'] = $this->language->get('text_map');
		$data['text_activity'] = $this->language->get('text_activity');
		$data['text_recent'] = $this->language->get('text_recent');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		// Check install directory exists
		if (0&&is_dir(dirname(DIR_APPLICATION) . '/install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}
		

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['order'] = $this->load->controller('dashboard/order');
		$data['sale'] = $this->load->controller('dashboard/sale');
		$data['customer'] = $this->load->controller('dashboard/customer');
		$data['online'] = $this->load->controller('dashboard/online');
		$data['map'] = $this->load->controller('dashboard/map');
		$data['chart'] = $this->load->controller('dashboard/chart');
		$data['activity'] = $this->load->controller('dashboard/activity');
		$data['recent'] = $this->load->controller('dashboard/recent');
		$data['footer'] = $this->load->controller('common/footer');

		$data['text_total_product'] = $this->language->get('text_total_product');
		$data['text_total_shipping'] = $this->language->get('text_total_shipping');
		$data['text_total_product_approval'] = $this->language->get('text_total_product_approval');
		$data['text_total_product_pendding'] = $this->language->get('text_total_product_pendding');
		$data['text_total_vendor_approval'] = $this->language->get('text_total_vendor_approval');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}
			
		$this->response->setOutput($this->load->view('common/dashboard.tpl', $data));
	}
	
	
}
