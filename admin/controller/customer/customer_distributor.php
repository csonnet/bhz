<?php
class ControllerCustomerCustomerDistributor extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
        } else {
            $filter_email = null;
        }

        if (isset($this->request->get['filter_company_name'])) {
            $filter_company_name = $this->request->get['filter_company_name'];
        } else {
            $filter_company_name = null;
        }

        if (isset($this->request->get['filter_customer_group_id'])) {
            $filter_customer_group_id = $this->request->get['filter_customer_group_id'];
        } else {
            $filter_customer_group_id = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['filter_logcenter_id'])) {
            $filter_logcenter_id = $this->request->get['filter_logcenter_id'];
        } else {
            $filter_logcenter_id = null;
        }

        if (isset($this->request->get['filter_approved'])) {
            $filter_approved = $this->request->get['filter_approved'];
        } else {
            $filter_approved = null;
        }

        if (isset($this->request->get['filter_need_review'])) {
            $filter_need_review = $this->request->get['filter_need_review'];
        } else {
            $filter_need_review = null;
        }

        if (isset($this->request->get['filter_is_magfin'])) {
            $filter_is_magfin = $this->request->get['filter_is_magfin'];
        } else {
            $filter_is_magfin = null;
        }

        if (isset($this->request->get['filter_telephone'])) {
            $filter_telephone = $this->request->get['filter_telephone'];
        } else {
            $filter_telephone = null;
        }

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = null;
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = null;
        }

        if (isset($this->request->get['filter_order_sum'])) {
            $filter_order_sum = $this->request->get['filter_order_sum'];
        } else {
            $filter_order_sum = null;
        }

        if (isset($this->request->get['filter_last_date_start'])) {
            $filter_last_date_start = $this->request->get['filter_last_date_start'];
        } else {
            $filter_last_date_start = null;
        }

        if (isset($this->request->get['filter_last_date_end'])) {
            $filter_last_date_end = $this->request->get['filter_last_date_end'];
        } else {
            $filter_last_date_end = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_company_name'])) {
            $url .= '&filter_company_name=' . urlencode(html_entity_decode($this->request->get['filter_company_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_group_id'])) {
            $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['filter_logcenter_id'])) {
            $url .= '&filter_logcenter_id=' . $this->request->get['filter_logcenter_id'];
        }

        if (isset($this->request->get['filter_approved'])) {
            $url .= '&filter_approved=' . $this->request->get['filter_approved'];
        }

        if (isset($this->request->get['filter_need_review'])) {
            $url .= '&filter_need_review=' . $this->request->get['filter_need_review'];
        }

        if (isset($this->request->get['filter_is_magfin'])) {
            $url .= '&filter_is_magfin=' . $this->request->get['filter_is_magfin'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_order_sum'])) {
            $url .= '&filter_order_sum=' . $this->request->get['filter_order_sum'];
        }

        if (isset($this->request->get['filter_last_date_start'])) {
            $url .= '&filter_last_date_start=' . $this->request->get['filter_last_date_start'];
        }

        if (isset($this->request->get['filter_last_date_end'])) {
            $url .= '&filter_last_date_end=' . $this->request->get['filter_last_date_end'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['customers'] = array();

        $filter_data = array(
            'filter_company_name'      => $filter_company_name,
            'filter_name'              => $filter_name,
            'filter_email'             => $filter_email,
            'filter_customer_group_id' => $filter_customer_group_id,
            'filter_status'            => $filter_status,
            'filter_logcenter_id'      => $filter_logcenter_id,
            'filter_approved'          => $filter_approved,
            'filter_need_review'       => $filter_need_review,
            'filter_is_magfin'         => $filter_is_magfin,
            'filter_date_start'        => $filter_date_start,
            'filter_date_end'          => $filter_date_end,
            'filter_order_sum'         => $filter_order_sum,
            'filter_last_date_start'   => $filter_last_date_start,
            'filter_last_date_end'     => $filter_last_date_end,
            'filter_telephone'         => $filter_telephone,
            'filter_user_id'           => $this->user->getId(),
            'sort'                     => $sort,
            'order'                    => $order,
            'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                    => $this->config->get('config_limit_admin')
        );

        //Liqn 获取物流营销中心列表
        $this->load->model('catalog/logcenter');
        $data['logcenters'] = $this->model_catalog_logcenter->getLogcenters();
        $logcenters = array();
        foreach ($data['logcenters'] as $logcenter) {
            $logcenters[$logcenter['logcenter_id']] = $logcenter['logcenter_name'];
        }

        $customer_total = $this->model_customer_customer->getTotalCustomers($filter_data);

        $results = $this->model_customer_customer->getCustomers($filter_data);

        foreach ($results as $result) {
            $login_info = $this->model_customer_customer->getTotalLoginAttempts($result['email']);

            $data['customers'][] = array(
                'customer_id'    => $result['customer_id'],
                'company_name'   => $result['company_name'],
                'name'           => $result['fullname'],
                'email'          => $result['email'],
                'customer_group' => $result['customer_group'],
                'status'         => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'need_review'    => ($result['need_review'] ? $this->language->get('text_yes') : $this->language->get('text_no')),
                'is_magfin'    => ($result['is_magfin'] ? $this->language->get('text_yes') : $this->language->get('text_no')),
                'has_license_image'    => ($result['license_image_code'] ? '已上传' : '未上传'),
                'telephone'      => $result['telephone'],
                'logcenter'      => isset($logcenters[$result['logcenter_id']])?$logcenters[$result['logcenter_id']]:'未分配',
                'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'order_sum'      => intval($result['order_sum']),
                'last_order_date'=> $result['last_order_date'],
                'approve'        => $approve,
                'unlock'         => $unlock,
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_company_name'] = $this->language->get('column_company_name');
        $data['column_email'] = $this->language->get('column_email');
        $data['column_customer_group'] = $this->language->get('column_customer_group');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_approved'] = $this->language->get('column_approved');
        $data['column_need_review'] = $this->language->get('column_need_review');
        $data['column_is_magfin'] = $this->language->get('column_is_magfin');
        $data['column_telephone'] = $this->language->get('column_telephone');
        $data['column_logcenter_id'] = $this->language->get('column_logcenter_id');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_company_name'] = $this->language->get('entry_company_name');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_approved'] = $this->language->get('entry_approved');
        $data['entry_need_review'] = $this->language->get('entry_need_review');
        $data['entry_is_magfin'] = $this->language->get('entry_is_magfin');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_date_start'] = $this->language->get('entry_date_start');
        $data['entry_date_end'] = $this->language->get('entry_date_end');
        $data['entry_logcenter_id'] = $this->language->get('entry_logcenter_id');

        $data['button_approve'] = $this->language->get('button_approve');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_login'] = $this->language->get('button_login');
        $data['button_unlock'] = $this->language->get('button_unlock');

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_company_name'])) {
            $url .= '&filter_company_name=' . urlencode(html_entity_decode($this->request->get['filter_company_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_group_id'])) {
            $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['filter_logcenter_id'])) {
            $url .= '&filter_logcenter_id=' . $this->request->get['filter_logcenter_id'];
        }

        if (isset($this->request->get['filter_approved'])) {
            $url .= '&filter_approved=' . $this->request->get['filter_approved'];
        }

        if (isset($this->request->get['filter_need_review'])) {
            $url .= '&filter_need_review=' . $this->request->get['filter_need_review'];
        }

        if (isset($this->request->get['filter_is_magfin'])) {
            $url .= '&filter_is_magfin=' . $this->request->get['filter_is_magfin'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_order_sum'])) {
            $url .= '&filter_order_sum=' . $this->request->get['filter_order_sum'];
        }

        if (isset($this->request->get['filter_last_date_start'])) {
            $url .= '&filter_last_date_start=' . $this->request->get['filter_last_date_start'];
        }

        if (isset($this->request->get['filter_last_date_end'])) {
            $url .= '&filter_last_date_end=' . $this->request->get['filter_last_date_end'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_company_name'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=company_name' . $url, 'SSL');
        $data['sort_name'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_email'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=c.email' . $url, 'SSL');
        $data['sort_customer_group'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=customer_group' . $url, 'SSL');
        $data['sort_status'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '
            &sort=c.status' . $url, 'SSL');
        $data['sort_logcenter_id'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '
            &sort=c.logcenter_id' . $url, 'SSL');
        $data['sort_order_sum'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=order_sum' . $url, 'SSL');
        $data['sort_last_order_date'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=last_order_date' . $url, 'SSL');
        $data['sort_need_review'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=c.need_review' . $url, 'SSL');
        $data['sort_telephone'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=c.telephone' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . '&sort=c.date_added' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_company_name'])) {
            $url .= '&filter_company_name=' . urlencode(html_entity_decode($this->request->get['filter_company_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_group_id'])) {
            $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['filter_logcenter_id'])) {
            $url .= '&filter_logcenter_id=' . $this->request->get['filter_logcenter_id'];
        }

        if (isset($this->request->get['filter_approved'])) {
            $url .= '&filter_approved=' . $this->request->get['filter_approved'];
        }

        if (isset($this->request->get['filter_need_review'])) {
            $url .= '&filter_need_review=' . $this->request->get['filter_need_review'];
        }

        if (isset($this->request->get['filter_is_magfin'])) {
            $url .= '&filter_is_magfin=' . $this->request->get['filter_is_magfin'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_order_sum'])) {
            $url .= '&filter_order_sum=' . $this->request->get['filter_order_sum'];
        }

        if (isset($this->request->get['filter_last_date_start'])) {
            $url .= '&filter_last_date_start=' . $this->request->get['filter_last_date_start'];
        }

        if (isset($this->request->get['filter_last_date_end'])) {
            $url .= '&filter_last_date_end=' . $this->request->get['filter_last_date_end'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $customer_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customer/customer_distributor', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

        $data['filter_company_name'] = $filter_company_name;
        $data['filter_name'] = $filter_name;
        $data['filter_email'] = $filter_email;
        $data['filter_customer_group_id'] = $filter_customer_group_id;
        $data['filter_status'] = $filter_status;
        $data['filter_logcenter_id'] = $filter_logcenter_id;
        $data['filter_approved'] = $filter_approved;
        $data['filter_need_review'] = $filter_need_review;
        $data['filter_is_magfin'] = $filter_is_magfin;
        $data['filter_telephone'] = $filter_telephone;
        $data['filter_date_start'] = $filter_date_start;
        $data['filter_date_end'] = $filter_date_end;
        $data['filter_order_sum'] = $filter_order_sum;
        $data['filter_last_date_start'] = $filter_last_date_start;
        $data['filter_last_date_end'] = $filter_last_date_end;

        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        $this->load->model('setting/store');

        $data['stores'] = $this->model_setting_store->getStores();

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/customer_list_distributor.tpl', $data));
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_recommended_code']) ||isset($this->request->get['filter_company_name']) ||isset($this->request->get['filter_name']) || isset($this->request->get['filter_email']) || isset($this->request->get['filter_telephone'])) {
            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_email'])) {
                $filter_email = $this->request->get['filter_email'];
            } else {
                $filter_email = '';
            }

            if (isset($this->request->get['filter_recommended_code'])) {
                $filter_recommended_code = $this->request->get['filter_recommended_code'];
            } else {
                $filter_recommended_code = '';
            }

            if (isset($this->request->get['filter_company_name'])) {
                $filter_company_name = $this->request->get['filter_company_name'];
            } else {
                $filter_company_name = '';
            }

            if (isset($this->request->get['filter_telephone'])) {
                $filter_telephone = $this->request->get['filter_telephone'];
            } else {
                $filter_telephone = '';
            }
            if ($this->user->getLP()) {
                $filter_logcenter_id = $this->user->getLP();
            }

            $this->load->model('customer/customer');

            $filter_data = array(
                'filter_name'  => $filter_name,
                'filter_recommended_code'  => $filter_recommended_code,
                'filter_email' => $filter_email,
                'filter_company_name' => $filter_company_name,
                'filter_telephone' => $filter_telephone,
                'filter_user_id' => $this->user->getId(),
                'start'        => 0,
                'limit'        => 5
            );

            if (isset($filter_logcenter_id)) {
                $filter_data['filter_logcenter_id'] = $filter_logcenter_id;
            }

            $results = $this->model_customer_customer->getCustomers($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'recommended_code'    => $result['recommended_code'],
                    'customer_id'       => $result['customer_id'],
                    'customer_group_id' => $result['customer_group_id'],
                    'name'              => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
                    'customer_group'    => $result['customer_group'],
                    'fullname'          => $result['fullname'],
                    'email'             => $result['email'],
                    'telephone'         => $result['telephone'],
                    'company_name'         => $result['company_name'],
                    'fax'               => $result['fax'],
                    'custom_field'      => json_decode($result['custom_field'], true),
                    'address'           => $this->model_customer_customer->getAddresses($result['customer_id'])
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
