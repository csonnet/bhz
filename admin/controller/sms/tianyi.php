<?php
class ControllerSmsTianYi extends Controller {
  private $error = array ();
  private function dataInit() {
    foreach ( $this->language->data as $key => $value ) {
      $data [$key] = $value;
    }
    return $data;
  }
  public function index() {
    $this->inputData = array (
        'tianyi_appid' => true,
        'tianyi_appsecret' => true,
        'tianyi_templateid' => true,
        'tianyi_templateid_co' => true,
        'tianyi_templateid_sp' => true,
        'tianyi_templateid_sv' => true,
        'tianyi_templateid_tc' => true,
        'tianyi_templateid_dc' => true,
		'tianyi_templateid_reg' => true,
		'tianyi_templateid_ex' => true,
        'tianyi_status' => false
    );
    $this->load->language ( 'sms/tianyi' );
    
    $this->document->setTitle ( $this->language->get ( 'heading_title' ) );
    
    $this->load->model ( 'setting/setting' );
    if (($this->request->server ['REQUEST_METHOD'] == 'POST') && $this->validate ()) {
      $this->model_setting_setting->editSetting ( 'tianyi', $this->request->post );
      
      $this->session->data ['success'] = $this->language->get ( 'text_success' );
      
      $this->response->redirect ( $this->url->link ( 'extension/sms', 'token=' . $this->session->data ['token'], 'SSL' ) );
    }
    
    $data = $this->dataInit ();
    
    foreach ($this->error as $key => $value){
      if (isset ( $this->error ['warning'] )) {
        $data['error_'. $key] =  $this->error[$key];
      }
      else{
        $data['error_'. $key] = '';
      }
      
    }
    
    if (isset ( $this->error ['warning'] )) {
      $data ['error_warning'] = $this->error ['warning'];
    } else {
      $data ['error_warning'] = '';
    }
    
    if (isset ( $this->error ['error_appid'] )) {
      $data ['error_appid'] = $this->error ['error_appid'];
    } else {
      $data ['error_appid'] = '';
    }
    
    if (isset ( $this->error ['error_appsecret'] )) {
      $data ['error_appsecret'] = $this->error ['error_appsecret'];
    } else {
      $data ['error_appsecret'] = '';
    }
    
    if (isset ( $this->error ['error_templateid'] )) {
      $data ['error_templateid'] = $this->error ['error_templateid'];
    } else {
      $data ['error_templateid'] = '';
    }

    if (isset ( $this->error ['error_templateid_co'] )) {
      $data ['error_templateid_co'] = $this->error ['error_templateid_co'];
    } else {
      $data ['error_templateid_co'] = '';
    }

    if (isset ( $this->error ['error_templateid_sp'] )) {
      $data ['error_templateid_sp'] = $this->error ['error_templateid_sp'];
    } else {
      $data ['error_templateid_sp'] = '';
    }

    if (isset ( $this->error ['error_templateid_sv'] )) {
      $data ['error_templateid_sv'] = $this->error ['error_templateid_sv'];
    } else {
      $data ['error_templateid_sv'] = '';
    }

    if (isset ( $this->error ['error_templateid_tc'] )) {
      $data ['error_templateid_tc'] = $this->error ['error_templateid_tc'];
    } else {
      $data ['error_templateid_tc'] = '';
    }

    if (isset ( $this->error ['error_templateid_dc'] )) {
      $data ['error_templateid_dc'] = $this->error ['error_templateid_dc'];
    } else {
      $data ['error_templateid_dc'] = '';
    }

	if (isset ( $this->error ['error_templateid_reg'] )) {
      $data ['error_templateid_reg'] = $this->error ['error_templateid_reg'];
    } else {
      $data ['error_templateid_reg'] = '';
    }

	if (isset ( $this->error ['error_templateid_ex'] )) {
      $data ['error_templateid_ex'] = $this->error ['error_templateid_ex'];
    } else {
      $data ['error_templateid_ex'] = '';
    }
    
    $data ['breadcrumbs'] = array ();
    
    $data ['breadcrumbs'] [] = array (
        'text' => $this->language->get ( 'text_home' ),
        'href' => $this->url->link ( 'common/dashboard', 'token=' . $this->session->data ['token'], 'SSL' ) 
    );
    
    $data ['breadcrumbs'] [] = array (
        'text' => $this->language->get ( 'text_sms' ),
        'href' => $this->url->link ( 'extension/sms', 'token=' . $this->session->data ['token'], 'SSL' ) 
    );
    
    $data ['breadcrumbs'] [] = array (
        'text' => $this->language->get ( 'heading_title' ),
        'href' => $this->url->link ( 'sms/tianyi', 'token=' . $this->session->data ['token'], 'SSL' ) 
    );
    
    $data ['action'] = $this->url->link ( 'sms/tianyi', 'token=' . $this->session->data ['token'], 'SSL' );
    
    $data ['cancel'] = $this->url->link ( 'extension/sms', 'token=' . $this->session->data ['token'], 'SSL' );
    if (isset ( $this->request->post ['tianyi_appid'] )) {
      $data ['tianyi_appid'] = $this->request->post ['tianyi_appid'];
    } else {
      $data ['tianyi_appid'] = $this->config->get ( 'tianyi_appid' );
    }
    
    if (isset ( $this->request->post ['tianyi_appsecret'] )) {
      $data ['tianyi_appsecret'] = $this->request->post ['tianyi_appsecret'];
    } else {
      $data ['tianyi_appsecret'] = $this->config->get ( 'tianyi_appsecret' );
    }
    
    if (isset ( $this->request->post ['tianyi_templateid'] )) {
      $data ['tianyi_templateid'] = $this->request->post ['tianyi_templateid'];
    } else {
      $data ['tianyi_templateid'] = $this->config->get ( 'tianyi_templateid' );
    }
    
    if (isset ( $this->request->post ['tianyi_templateid_co'] )) {
      $data ['tianyi_templateid_co'] = $this->request->post ['tianyi_templateid_co'];
    } else {
      $data ['tianyi_templateid_co'] = $this->config->get ( 'tianyi_templateid_co' );
    }

    if (isset ( $this->request->post ['tianyi_templateid_sp'] )) {
      $data ['tianyi_templateid_sp'] = $this->request->post ['tianyi_templateid_sp'];
    } else {
      $data ['tianyi_templateid_sp'] = $this->config->get ( 'tianyi_templateid_sp' );
    }

    if (isset ( $this->request->post ['tianyi_templateid_sv'] )) {
      $data ['tianyi_templateid_sv'] = $this->request->post ['tianyi_templateid_sv'];
    } else {
      $data ['tianyi_templateid_sv'] = $this->config->get ( 'tianyi_templateid_sv' );
    }

    if (isset ( $this->request->post ['tianyi_templateid_tc'] )) {
      $data ['tianyi_templateid_tc'] = $this->request->post ['tianyi_templateid_tc'];
    } else {
      $data ['tianyi_templateid_tc'] = $this->config->get ( 'tianyi_templateid_tc' );
    }

    if (isset ( $this->request->post ['tianyi_templateid_dc'] )) {
      $data ['tianyi_templateid_dc'] = $this->request->post ['tianyi_templateid_dc'];
    } else {
      $data ['tianyi_templateid_dc'] = $this->config->get ( 'tianyi_templateid_dc' );
    }

	 if (isset ( $this->request->post ['tianyi_templateid_reg'] )) {
      $data ['tianyi_templateid_reg'] = $this->request->post ['tianyi_templateid_reg'];
    } else {
      $data ['tianyi_templateid_reg'] = $this->config->get ( 'tianyi_templateid_reg' );
    }

	 if (isset ( $this->request->post ['tianyi_templateid_ex'] )) {
      $data ['tianyi_templateid_ex'] = $this->request->post ['tianyi_templateid_ex'];
    } else {
      $data ['tianyi_templateid_ex'] = $this->config->get ( 'tianyi_templateid_ex' );
    }

    if (isset ( $this->request->post ['tianyi_status'] )) {
      $data ['tianyi_status'] = $this->request->post ['tianyi_status'];
    } else {
      $data ['tianyi_status'] = $this->config->get ( 'tianyi_status' );
    }
    
    $data ['header'] = $this->load->controller ( 'common/header' );
    $data ['column_left'] = $this->load->controller ( 'common/column_left' );
    $data ['footer'] = $this->load->controller ( 'common/footer' );
    
    $this->response->setOutput ( $this->load->view ( 'sms/tianyi.tpl', $data ) );
  }
  protected function validate() {
    if (! $this->user->hasPermission ( 'modify', 'sms/tianyi' )) {
      $this->error ['warning'] = $this->language->get ( 'error_permission' );
    }
    foreach($this->inputData as $key=>$value){
      if($value){
        if (! $this->request->post [$key]) {
          $this->error ['appid'] = $this->language->get ( 'error_'.$key );
        }
      }
    }
    return ! $this->error;
  }
}