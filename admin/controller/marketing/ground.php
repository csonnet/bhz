<?php
class ControllerMarketingGround extends Controller {
  private $error = array();

  public function index() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    $this->getList();
  }

  public function add() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      $this->model_marketing_ground->addGround($this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getForm();
  }

  public function edit() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      $this->model_marketing_ground->editGround($this->request->get['ground_id'], $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getForm();
  }

  public function delete() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    if (isset($this->request->post['selected']) && $this->validateDelete()) {
      foreach ($this->request->post['selected'] as $ground_id) {
        $this->model_marketing_ground->deleteGround($ground_id);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getList();
  }

  public function approve() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    if (isset($this->request->get['ground_id']) && $this->validateApprove()) {
      $this->model_marketing_ground->approve($this->request->get['ground_id']);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getList();
  }
  
  public function unlock() {
    $this->load->language('marketing/ground');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('marketing/ground');

    if (isset($this->request->get['email']) && $this->validateUnlock()) {
      $this->model_marketing_ground->deleteLoginAttempts($this->request->get['email']);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getList();
  }
  
  protected function getList() {
    if (isset($this->request->get['filter_name'])) {
      $filter_name = $this->request->get['filter_name'];
    } else {
      $filter_name = null;
    }

    if (isset($this->request->get['filter_email'])) {
      $filter_email = $this->request->get['filter_email'];
    } else {
      $filter_email = null;
    }

    if (isset($this->request->get['filter_status'])) {
      $filter_status = $this->request->get['filter_status'];
    } else {
      $filter_status = null;
    }

    if (isset($this->request->get['filter_approved'])) {
      $filter_approved = $this->request->get['filter_approved'];
    } else {
      $filter_approved = null;
    }

    if (isset($this->request->get['filter_date_added'])) {
      $filter_date_added = $this->request->get['filter_date_added'];
    } else {
      $filter_date_added = null;
    }

    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'name';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_email'])) {
      $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['filter_approved'])) {
      $url .= '&filter_approved=' . $this->request->get['filter_approved'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );

    $data['approve'] = $this->url->link('marketing/ground/approve', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['add'] = $this->url->link('marketing/ground/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['delete'] = $this->url->link('marketing/ground/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $data['grounds'] = array();

    $filter_data = array(
      'filter_name'       => $filter_name,
      'filter_email'      => $filter_email,
      'filter_status'     => $filter_status,
      'filter_approved'   => $filter_approved,
      'filter_date_added' => $filter_date_added,
      'sort'              => $sort,
      'order'             => $order,
      'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
      'limit'             => $this->config->get('config_limit_admin')
    );

    $ground_total = $this->model_marketing_ground->getTotalGrounds($filter_data);

    $results = $this->model_marketing_ground->getGrounds($filter_data);

    foreach ($results as $result) {
      if (!$result['approved']) {
        $approve = $this->url->link('marketing/ground/approve', 'token=' . $this->session->data['token'] . '&ground_id=' . $result['ground_id'] . $url, 'SSL');
      } else {
        $approve = '';
      }     
      
      // $login_info = $this->model_marketing_ground->getTotalLoginAttempts($result['email']);
      
      // if ($login_info && $login_info['total'] >= $this->config->get('config_login_attempts')) {
      //   $unlock = $this->url->link('marketing/ground/unlock', 'token=' . $this->session->data['token'] . '&email=' . $result['email'] . $url, 'SSL');
      // } else {
      //   $unlock = '';
      // }
            
      $data['grounds'][] = array(
        'ground_id' => $result['ground_id'],
        'name'         => $result['name'],
        'email'        => $result['email'],
        // 'balance'      => $this->currency->format($result['balance'], $this->config->get('config_currency')),
        'status'       => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
        'date_added'   => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
        'approve'      => $approve,
        // 'unlock'       => $unlock,
        'edit'         => $this->url->link('marketing/ground/edit', 'token=' . $this->session->data['token'] . '&ground_id=' . $result['ground_id'] . $url, 'SSL')
      );
    }

    $data['heading_title'] = $this->language->get('heading_title');
    
    $data['text_list'] = $this->language->get('text_list');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['text_yes'] = $this->language->get('text_yes');
    $data['text_no'] = $this->language->get('text_no');
    $data['text_no_results'] = $this->language->get('text_no_results');
    $data['text_confirm'] = $this->language->get('text_confirm');
    $data['text_none'] = $this->language->get('text_none');

    $data['column_name'] = $this->language->get('column_name');
    $data['column_email'] = $this->language->get('column_email');
    $data['column_balance'] = $this->language->get('column_balance');
    $data['column_status'] = $this->language->get('column_status');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_action'] = $this->language->get('column_action');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_approved'] = $this->language->get('entry_approved');
    $data['entry_date_added'] = $this->language->get('entry_date_added');

    $data['button_approve'] = $this->language->get('button_approve');
    $data['button_add'] = $this->language->get('button_add');
    $data['button_edit'] = $this->language->get('button_edit');
    $data['button_delete'] = $this->language->get('button_delete');
    $data['button_filter'] = $this->language->get('button_filter');

    $data['token'] = $this->session->data['token'];

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    if (isset($this->request->post['selected'])) {
      $data['selected'] = (array)$this->request->post['selected'];
    } else {
      $data['selected'] = array();
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_email'])) {
      $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['filter_approved'])) {
      $url .= '&filter_approved=' . $this->request->get['filter_approved'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_name'] = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
    $data['sort_email'] = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . '&sort=a.email' . $url, 'SSL');
    $data['sort_status'] = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . '&sort=a.status' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . '&sort=a.date_added' . $url, 'SSL');

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_email'])) {
      $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['filter_approved'])) {
      $url .= '&filter_approved=' . $this->request->get['filter_approved'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $ground_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($ground_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ground_total - $this->config->get('config_limit_admin'))) ? $ground_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ground_total, ceil($ground_total / $this->config->get('config_limit_admin')));

    $data['filter_name'] = $filter_name;
    $data['filter_email'] = $filter_email;
    $data['filter_status'] = $filter_status;
    $data['filter_approved'] = $filter_approved;
    $data['filter_date_added'] = $filter_date_added;

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('marketing/ground_list.tpl', $data));
  }

  protected function getForm() {
    $data['heading_title'] = $this->language->get('heading_title');
    
    $data['text_form'] = !isset($this->request->get['ground_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_ground_detail'] = $this->language->get('text_ground_detail');
    $data['text_ground_address'] = $this->language->get('text_ground_address');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['text_select'] = $this->language->get('text_select');
    $data['text_none'] = $this->language->get('text_none');
    $data['text_loading'] = $this->language->get('text_loading');
    $data['text_cheque'] = $this->language->get('text_cheque');
    $data['text_paypal'] = $this->language->get('text_paypal');
    $data['text_bank'] = $this->language->get('text_bank');
    $data['text_alipay'] = $this->language->get('text_alipay');

    $data['entry_fullname'] = $this->language->get('entry_fullname');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_telephone'] = $this->language->get('entry_telephone');
    $data['entry_fax'] = $this->language->get('entry_fax');
    $data['entry_company'] = $this->language->get('entry_company');
    $data['entry_website'] = $this->language->get('entry_website');
    $data['entry_address'] = $this->language->get('entry_address');
    $data['entry_city'] = $this->language->get('entry_city');
    $data['entry_postcode'] = $this->language->get('entry_postcode');
    $data['entry_country'] = $this->language->get('entry_country');
    $data['entry_zone'] = $this->language->get('entry_zone');
    $data['entry_code'] = $this->language->get('entry_code');
    $data['entry_commission'] = $this->language->get('entry_commission');
    $data['entry_tax'] = $this->language->get('entry_tax');
    $data['entry_payment'] = $this->language->get('entry_payment');
    $data['entry_cheque'] = $this->language->get('entry_cheque');
    $data['entry_paypal'] = $this->language->get('entry_paypal');
    $data['entry_bank_name'] = $this->language->get('entry_bank_name');
    $data['entry_bank_account_name'] = $this->language->get('entry_bank_account_name');
    $data['entry_bank_account_number'] = $this->language->get('entry_bank_account_number');
    $data['entry_alipay_account_name'] = $this->language->get('entry_alipay_account_name');
    $data['entry_alipay'] = $this->language->get('entry_alipay');
    $data['entry_password'] = $this->language->get('entry_password');
    $data['entry_confirm'] = $this->language->get('entry_confirm');
    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_amount'] = $this->language->get('entry_amount');
    $data['entry_description'] = $this->language->get('entry_description');

    $data['help_code'] = $this->language->get('help_code');
    $data['help_commission'] = $this->language->get('help_commission');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_transaction_add'] = $this->language->get('button_transaction_add');
    $data['button_remove'] = $this->language->get('button_remove');

    $data['tab_general'] = $this->language->get('tab_general');
    $data['tab_payment'] = $this->language->get('tab_payment');
    $data['tab_transaction'] = $this->language->get('tab_transaction');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['fullname'])) {
      $data['error_fullname'] = $this->error['fullname'];
    } else {
      $data['error_fullname'] = '';
    }

    if (isset($this->error['email'])) {
      $data['error_email'] = $this->error['email'];
    } else {
      $data['error_email'] = '';
    }

    if (isset($this->error['cheque'])) {
      $data['error_cheque'] = $this->error['cheque'];
    } else {
      $data['error_cheque'] = '';
    }

    if (isset($this->error['paypal'])) {
      $data['error_paypal'] = $this->error['paypal'];
    } else {
      $data['error_paypal'] = '';
    }

    if (isset($this->error['bank_account_name'])) {
      $data['error_bank_account_name'] = $this->error['bank_account_name'];
    } else {
      $data['error_bank_account_name'] = '';
    }

    if (isset($this->error['bank_account_number'])) {
      $data['error_bank_account_number'] = $this->error['bank_account_number'];
    } else {
      $data['error_bank_account_number'] = '';
    }
    
    if (isset($this->error['alipay_account_name'])) {
      $data['error_alipay_account_name'] = $this->error['alipay_account_name'];
    } else {
      $data['error_alipay_account_name'] = '';
    }

    if (isset($this->error['alipay'])) {
      $data['error_alipay'] = $this->error['alipay'];
    } else {
      $data['error_alipay'] = '';
    }

    if (isset($this->error['telephone'])) {
      $data['error_telephone'] = $this->error['telephone'];
    } else {
      $data['error_telephone'] = '';
    }

    if (isset($this->error['password'])) {
      $data['error_password'] = $this->error['password'];
    } else {
      $data['error_password'] = '';
    }

    if (isset($this->error['confirm'])) {
      $data['error_confirm'] = $this->error['confirm'];
    } else {
      $data['error_confirm'] = '';
    }

    if (isset($this->error['address'])) {
      $data['error_address'] = $this->error['address'];
    } else {
      $data['error_address'] = '';
    }

    if (isset($this->error['city'])) {
      $data['error_city'] = $this->error['city'];
    } else {
      $data['error_city'] = '';
    }

    if (isset($this->error['postcode'])) {
      $data['error_postcode'] = $this->error['postcode'];
    } else {
      $data['error_postcode'] = '';
    }

    if (isset($this->error['country'])) {
      $data['error_country'] = $this->error['country'];
    } else {
      $data['error_country'] = '';
    }

    if (isset($this->error['zone'])) {
      $data['error_zone'] = $this->error['zone'];
    } else {
      $data['error_zone'] = '';
    }

    if (isset($this->error['code'])) {
      $data['error_code'] = $this->error['code'];
    } else {
      $data['error_code'] = '';
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_email'])) {
      $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['filter_approved'])) {
      $url .= '&filter_approved=' . $this->request->get['filter_approved'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL')
    );

    if (!isset($this->request->get['ground_id'])) {
      $data['action'] = $this->url->link('marketing/ground/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
    } else {
      $data['action'] = $this->url->link('marketing/ground/edit', 'token=' . $this->session->data['token'] . '&ground_id=' . $this->request->get['ground_id'] . $url, 'SSL');
    }

    $data['cancel'] = $this->url->link('marketing/ground', 'token=' . $this->session->data['token'] . $url, 'SSL');

    if (isset($this->request->get['ground_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $ground_info = $this->model_marketing_ground->getGround($this->request->get['ground_id']);
    }

    $data['token'] = $this->session->data['token'];

    if (isset($this->request->get['ground_id'])) {
      $data['ground_id'] = $this->request->get['ground_id'];
    } else {
      $data['ground_id'] = 0;
    }

    if (isset($this->request->post['fullname'])) {
      $data['fullname'] = $this->request->post['fullname'];
    } elseif (!empty($ground_info)) {
      $data['fullname'] = $ground_info['fullname'];
    } else {
      $data['fullname'] = '';
    }


    if (isset($this->request->post['email'])) {
      $data['email'] = $this->request->post['email'];
    } elseif (!empty($ground_info)) {
      $data['email'] = $ground_info['email'];
    } else {
      $data['email'] = '';
    }

    if (isset($this->request->post['telephone'])) {
      $data['telephone'] = $this->request->post['telephone'];
    } elseif (!empty($ground_info)) {
      $data['telephone'] = $ground_info['telephone'];
    } else {
      $data['telephone'] = '';
    }

    if (isset($this->request->post['fax'])) {
      $data['fax'] = $this->request->post['fax'];
    } elseif (!empty($ground_info)) {
      $data['fax'] = $ground_info['fax'];
    } else {
      $data['fax'] = '';
    }

    if (isset($this->request->post['company'])) {
      $data['company'] = $this->request->post['company'];
    } elseif (!empty($ground_info)) {
      $data['company'] = $ground_info['company'];
    } else {
      $data['company'] = '';
    }

    if (isset($this->request->post['website'])) {
      $data['website'] = $this->request->post['website'];
    } elseif (!empty($ground_info)) {
      $data['website'] = $ground_info['website'];
    } else {
      $data['website'] = '';
    }

    if (isset($this->request->post['address'])) {
      $data['address'] = $this->request->post['address'];
    } elseif (!empty($ground_info)) {
      $data['address'] = $ground_info['address'];
    } else {
      $data['address'] = '';
    }

    if (isset($this->request->post['city'])) {
      $data['city'] = $this->request->post['city'];
    } elseif (!empty($ground_info)) {
      $data['city'] = $ground_info['city'];
    } else {
      $data['city'] = '';
    }

    if (isset($this->request->post['postcode'])) {
      $data['postcode'] = $this->request->post['postcode'];
    } elseif (!empty($ground_info)) {
      $data['postcode'] = $ground_info['postcode'];
    } else {
      $data['postcode'] = '';
    }

    if (isset($this->request->post['country_id'])) {
      $data['country_id'] = $this->request->post['country_id'];
    } elseif (!empty($ground_info)) {
      $data['country_id'] = $ground_info['country_id'];
    } else {
      $data['country_id'] = '';
    }

    $this->load->model('localisation/country');

    $data['countries'] = $this->model_localisation_country->getCountries();

    if (isset($this->request->post['zone_id'])) {
      $data['zone_id'] = $this->request->post['zone_id'];
    } elseif (!empty($ground_info)) {
      $data['zone_id'] = $ground_info['zone_id'];
    } else {
      $data['zone_id'] = '';
    }

    if (isset($this->request->post['code'])) {
      $data['code'] = $this->request->post['code'];
    } elseif (!empty($ground_info)) {
      $data['code'] = $ground_info['code'];
    } else {
      $data['code'] = uniqid();
    }

    if (isset($this->request->post['commission'])) {
      $data['commission'] = $this->request->post['commission'];
    } elseif (!empty($ground_info)) {
      $data['commission'] = $ground_info['commission'];
    } else {
      $data['commission'] = $this->config->get('config_ground_commission');
    }

    if (isset($this->request->post['tax'])) {
      $data['tax'] = $this->request->post['tax'];
    } elseif (!empty($ground_info)) {
      $data['tax'] = $ground_info['tax'];
    } else {
      $data['tax'] = '';
    }

    if (isset($this->request->post['payment'])) {
      $data['payment'] = $this->request->post['payment'];
    } elseif (!empty($ground_info)) {
      $data['payment'] = $ground_info['payment'];
    } else {
      $data['payment'] = 'alipay';
    }

    if (isset($this->request->post['cheque'])) {
      $data['cheque'] = $this->request->post['cheque'];
    } elseif (!empty($ground_info)) {
      $data['cheque'] = $ground_info['cheque'];
    } else {
      $data['cheque'] = '';
    }

    if (isset($this->request->post['bank_name'])) {
      $data['bank_name'] = $this->request->post['bank_name'];
    } elseif (!empty($ground_info)) {
      $data['bank_name'] = $ground_info['bank_name'];
    } else {
      $data['bank_name'] = '';
    }

    if (isset($this->request->post['bank_account_name'])) {
      $data['bank_account_name'] = $this->request->post['bank_account_name'];
    } elseif (!empty($ground_info)) {
      $data['bank_account_name'] = $ground_info['bank_account_name'];
    } else {
      $data['bank_account_name'] = '';
    }

    if (isset($this->request->post['bank_account_number'])) {
      $data['bank_account_number'] = $this->request->post['bank_account_number'];
    } elseif (!empty($ground_info)) {
      $data['bank_account_number'] = $ground_info['bank_account_number'];
    } else {
      $data['bank_account_number'] = '';
    }
    
    if (isset($this->request->post['alipay_account_name'])) {
      $data['alipay_account_name'] = $this->request->post['alipay_account_name'];
    } elseif (!empty($ground_info)) {
      $data['alipay_account_name'] = $ground_info['alipay_account_name'];
    } else {
      $data['alipay_account_name'] = '';
    }

    if (isset($this->request->post['alipay'])) {
      $data['alipay'] = $this->request->post['alipay'];
    } elseif (!empty($ground_info)) {
      $data['alipay'] = $ground_info['alipay'];
    } else {
      $data['alipay'] = '';
    }

    if (isset($this->request->post['status'])) {
      $data['status'] = $this->request->post['status'];
    } elseif (!empty($ground_info)) {
      $data['status'] = $ground_info['status'];
    } else {
      $data['status'] = true;
    }

    if (isset($this->request->post['password'])) {
      $data['password'] = $this->request->post['password'];
    } else {
      $data['password'] = '';
    }

    if (isset($this->request->post['confirm'])) {
      $data['confirm'] = $this->request->post['confirm'];
    } else {
      $data['confirm'] = '';
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('marketing/ground_form.tpl', $data));
  }

  protected function validateForm() {
    if (!$this->user->hasPermission('modify', 'marketing/ground')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if ((utf8_strlen(trim($this->request->post['fullname'])) < 1) || (utf8_strlen(trim($this->request->post['fullname'])) > 32)) {
      $this->error['fullname'] = $this->language->get('error_fullname');
    }

    if ((utf8_strlen($this->request->post['email']) > 96) || (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email']))) {
      $this->error['email'] = $this->language->get('error_email');
    }

    $ground_info = $this->model_marketing_ground->getGroundByEmail($this->request->post['email']);

    if (!isset($this->request->get['ground_id'])) {
      if ($ground_info) {
        $this->error['warning'] = $this->language->get('error_exists');
      }
    } else {
      if ($ground_info && ($this->request->get['ground_id'] != $ground_info['ground_id'])) {
        $this->error['warning'] = $this->language->get('error_exists');
      }
    }

    if ((utf8_strlen($this->request->post['telephone']) < 1) || (utf8_strlen($this->request->post['telephone']) > 32)) {
      $this->error['telephone'] = $this->language->get('error_telephone');
    }

    // if ($this->request->post['password'] || (!isset($this->request->get['ground_id']))) {
    //   if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
    //     $this->error['password'] = $this->language->get('error_password');
    //   }

    //   if ($this->request->post['password'] != $this->request->post['confirm']) {
    //     $this->error['confirm'] = $this->language->get('error_confirm');
    //   }
    // }
    
    if ($this->error && !isset($this->error['warning'])) {
      $this->error['warning'] = $this->language->get('error_warning');
    }

    return !$this->error;
  }

  protected function validateDelete() {
    if (!$this->user->hasPermission('modify', 'marketing/ground')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }

  protected function validateApprove() {
    if (!$this->user->hasPermission('modify', 'marketing/ground')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }
  
  protected function validateUnlock() {
    if (!$this->user->hasPermission('modify', 'marketing/ground')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }
  
  public function transaction() {
    $this->load->language('marketing/ground');

    $this->load->model('marketing/ground');

    $data['text_no_results'] = $this->language->get('text_no_results');
    $data['text_balance'] = $this->language->get('text_balance');

    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_description'] = $this->language->get('column_description');
    $data['column_amount'] = $this->language->get('column_amount');

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $data['transactions'] = array();

    $results = $this->model_marketing_ground->getTransactions($this->request->get['ground_id'], ($page - 1) * 10, 10);

    foreach ($results as $result) {
      $data['transactions'][] = array(
        'amount'      => $this->currency->format($result['amount'], $this->config->get('config_currency')),
        'description' => $result['description'],
        'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added']))
      );
    }

    $data['balance'] = $this->currency->format($this->model_marketing_ground->getTransactionTotal($this->request->get['ground_id']), $this->config->get('config_currency'));

    $transaction_total = $this->model_marketing_ground->getTotalTransactions($this->request->get['ground_id']);

    $pagination = new Pagination();
    $pagination->total = $transaction_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('marketing/ground/transaction', 'token=' . $this->session->data['token'] . '&ground_id=' . $this->request->get['ground_id'] . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($transaction_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($transaction_total - 10)) ? $transaction_total : ((($page - 1) * 10) + 10), $transaction_total, ceil($transaction_total / 10));

    $this->response->setOutput($this->load->view('marketing/ground_transaction.tpl', $data));
  }

  public function addTransaction() {
    $this->load->language('marketing/ground');

    $json = array();

    if (!$this->user->hasPermission('modify', 'marketing/ground')) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      $this->load->model('marketing/ground');

      $this->model_marketing_ground->addTransaction($this->request->get['ground_id'], $this->request->post['description'], $this->request->post['amount']);

      $json['success'] = $this->language->get('text_success');
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function autocomplete() {
    $ground_data = array();

    if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_email'])) {
      if (isset($this->request->get['filter_name'])) {
        $filter_name = $this->request->get['filter_name'];
      } else {
        $filter_name = '';
      }

      if (isset($this->request->get['filter_email'])) {
        $filter_email = $this->request->get['filter_email'];
      } else {
        $filter_email = '';
      }

      $this->load->model('marketing/ground');

      $filter_data = array(
        'filter_name'  => $filter_name,
        'filter_email' => $filter_email,
        'start'        => 0,
        'limit'        => 5
      );

      $results = $this->model_marketing_ground->getGrounds($filter_data);

      foreach ($results as $result) {
        $ground_data[] = array(
          'ground_id' => $result['ground_id'],
          'name'         => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
          'email'        => $result['email']
        );
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($ground_data));
  }
}