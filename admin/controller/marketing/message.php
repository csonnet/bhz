<?php

header("Content-Type: text/html;charset=utf-8"); 

class ControllerMarketingMessage extends Controller {
	
	private $error = array();

	public function index() {

		$this->load->language('marketing/message');
		$this->load->model('marketing/message');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/message', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['token'] = $this->session->data['token'];
		
		$data['saleAreas'] = $this->model_marketing_message->getSaleAreas();
		$data['warehouses'] = $this->model_marketing_message->getWarehouses();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/message.tpl', $data));

	}
	
	//用户autocomplete
	public function customerAuto(){

		$json = array();

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = trim($this->request->get['filter_customer']);
		}
		else{
			$filter_customer = '';
		}

		$filter_data = array(
			'filter_customer' => $filter_customer,
			'start' => 0,
			'limit' => 5
		);

		$this->load->model('marketing/message');

		$results = $this->model_marketing_message->getCustomers($filter_data);

		foreach ($results as $result){
			$json[] = array(
				'customer_id' => $result['customer_id'],
				'fullname' => $result['fullname']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	
	//发送通知
	public function sendMessage(){
		
		$json = array();

		$data['title'] = $this->request->post['title'];
		$data['content'] = $this->request->post['content'];
		$data['method'] = $this->request->post['method'];
		$data['sa'] = json_decode($this->request->post['sa']);
		$data['wh'] = json_decode($this->request->post['wh']);
		$data['persons'] = json_decode($this->request->post['persons']);

		if($data['title'] == ""){
			$json['error'] = '标题不能为空';
		}
		else if($data['content'] == ""){
			$json['error'] = '内容不能为空';
		}
		else if($data['method'] == "normal" && empty($data['persons'])){
			$json['error'] = '至少选择一个用户发送';
		}
		else{

			$this->load->model('marketing/message');
			
			//发送业务员或仓管
			if($data['method'] == "special"){
				
				$sagroup = $this->model_marketing_message->saCustomers($data['sa']);
				$whgroup = $this->model_marketing_message->whCustomers($data['wh']);
				
				if($sagroup){
					$data['persons'] = array_merge($data['persons'],$sagroup);
				}

				if($whgroup){
					$data['persons'] = array_merge($data['persons'],$whgroup);
				}
				
				$data['persons'] = array_flip(array_flip($data['persons']));

			}
			else if($data['method'] == "admin"){
				$admingroup = $this->model_marketing_message->adminAll();
				$data['persons'] = $admingroup;
			}
			
			if(empty($data['persons'])){
				$json['error'] = '没有合适的人发送，消息未发送';
			}
			else{

				$this->model_marketing_message->sendMessage($data);
				$json['success'] = '发送消息成功';

			}

		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

}
