<?php 
class ControllerPaymentWxyijiPay extends Controller {
	private $error = array(); 

	public function index() {
		$this->load->language('payment/wx_yijipay');

		$this->document->settitle($this->language->get('heading_title'));
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/wx_yijipay', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('payment/wx_yijipay', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->load->model('setting/setting');
			
			$this->model_setting_setting->editSetting('wx_yijipay', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_wx_yijipay_mchid'] = $this->language->get('entry_wx_yijipay_mchid');
		$data['entry_wx_yijipay_appid'] = $this->language->get('entry_wx_yijipay_appid');
		$data['entry_wx_yijipay_key'] = $this->language->get('entry_wx_yijipay_key');
		$data['entry_wx_yijipay_appsecret'] = $this->language->get('entry_wx_yijipay_appsecret');
			
		$data['entry_wx_yijipay_status'] = $this->language->get('entry_wx_yijipay_status');
		$data['entry_wx_yijipay_sort_order'] = $this->language->get('entry_wx_yijipay_sort_order');
		$data['entry_total'] = $this->language->get('entry_total');
		
		$data['entry_trade_success_status'] = $this->language->get('entry_trade_success_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_log'] = $this->language->get('entry_log');
		
		
		$data['help_mchid'] = $this->language->get('help_mchid');
		$data['help_appid'] = $this->language->get('help_appid');
		$data['help_key'] = $this->language->get('help_key');
		$data['help_appsecret'] = $this->language->get('help_appsecret');
		$data['help_total'] = $this->language->get('help_total');
		$data['help_trade_success'] = $this->language->get('help_trade_success');
		$data['help_log'] = $this->language->get('help_log');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_order_status'] = $this->language->get('tab_order_status');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
 		if (isset($this->error['mchid'])) {
			$data['error_wx_yijipay_mchid'] = $this->error['mchid'];
		} else {
			$data['error_wx_yijipay_mchid'] = '';
		}
		
		if (isset($this->error['appid'])) {
			$data['error_wx_yijipay_appid'] = $this->error['appid'];
		} else {
			$data['error_wx_yijipay_appid'] = '';
		}
		
		if (isset($this->error['key'])) {
			$data['error_wx_yijipay_key'] = $this->error['key'];
		} else {
			$data['error_wx_yijipay_key'] = '';
		}
		
		if (isset($this->error['appsecret'])) {
			$data['error_wx_yijipay_appsecret'] = $this->error['appsecret'];
		} else {
			$data['error_wx_yijipay_appsecret'] = '';
		}


		
		if (isset($this->request->post['wx_yijipay_mchid'])) {
			$data['wx_yijipay_mchid'] = $this->request->post['wx_yijipay_mchid'];
		} else {
			$data['wx_yijipay_mchid'] = $this->config->get('wx_yijipay_mchid');
		}

		if (isset($this->request->post['wx_yijipay_appid'])) {
			$data['wx_yijipay_appid'] = $this->request->post['wx_yijipay_appid'];
		} else {
			$data['wx_yijipay_appid'] = $this->config->get('wx_yijipay_appid');
		}

		if (isset($this->request->post['wx_yijipay_key'])) {
			$data['wx_yijipay_key'] = $this->request->post['wx_yijipay_key'];
		} else {
			$data['wx_yijipay_key'] = $this->config->get('wx_yijipay_key');
		}
		
		if (isset($this->request->post['wx_yijipay_appsecret'])) {
			$data['wx_yijipay_appsecret'] = $this->request->post['wx_yijipay_appsecret'];
		} else {
			$data['wx_yijipay_appsecret'] = $this->config->get('wx_yijipay_appsecret');
		}		
		
		if (isset($this->request->post['wx_yijipay_total'])) {
			$data['wx_yijipay_total'] = $this->request->post['wx_yijipay_total'];
		} else {
			$data['wx_yijipay_total'] = $this->config->get('wx_yijipay_total');
		}
		
		if (isset($this->request->post['wx_yijipay_log'])) {
			$data['wx_yijipay_log'] = $this->request->post['wx_yijipay_log'];
		} else {
			$data['wx_yijipay_log'] = $this->config->get('wx_yijipay_log');
		}

		if (isset($this->request->post['wx_yijipay_trade_success_status_id'])) {
			$data['wx_yijipay_trade_success_status_id'] = $this->request->post['wx_yijipay_trade_success_status_id'];
		} elseif($this->config->get('wx_yijipay_trade_success_status_id')) {
			$data['wx_yijipay_trade_success_status_id'] = $this->config->get('wx_yijipay_trade_success_status_id'); 
		} else {
			$data['wx_yijipay_trade_success_status_id'] = 5;//complete
		}
		
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
			
		if (isset($this->request->post['wx_yijipay_geo_zone_id'])) {
			$data['wx_yijipay_geo_zone_id'] = $this->request->post['wx_yijipay_geo_zone_id'];
		} else {
			$data['wx_yijipay_geo_zone_id'] = $this->config->get('wx_yijipay_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['wx_yijipay_status'])) {
			$data['wx_yijipay_status'] = $this->request->post['wx_yijipay_status'];
		} else {
			$data['wx_yijipay_status'] = $this->config->get('wx_yijipay_status');
		}
		
		if (isset($this->request->post['wx_yijipay_sort_order'])) {
			$data['wx_yijipay_sort_order'] = $this->request->post['wx_yijipay_sort_order'];
		} else {
			$data['wx_yijipay_sort_order'] = $this->config->get('wx_yijipay_sort_order');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/wx_yijipay.tpl', $data));
		
	}


	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/wx_yijipay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['wx_yijipay_mchid']) {
			$this->error['mchid'] = $this->language->get('error_wx_yijipay_mchid');
		}

		if (!$this->request->post['wx_yijipay_appid']) {
			$this->error['appid'] = $this->language->get('error_wx_yijipay_appid');
		}

		if (!$this->request->post['wx_yijipay_key']) {
			$this->error['key'] = $this->language->get('error_wx_yijipay_key');
		}
		
		if (!$this->request->post['wx_yijipay_appsecret']) {
			$this->error['wx_yijipay_appsecret'] = $this->language->get('error_wx_yijipay_appsecret');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}