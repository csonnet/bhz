<?php
class Cart {
	private $data = array();
    /*
     * 缓存购物车产品数据，避免单个url请求中getProducts方法重复被调用。
     * @author sonicsjh
     */
    private $_cacheProductsInCart = false;

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		// Remove all the expired carts with no customer ID
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

		if ($this->customer->getId()) {
			// We want to change the session ID on all the old items in the customers cart
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

			// Once the customer is logged in we want to update the customer ID on all items he has
			$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

			foreach ($cart_query->rows as $cart) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");

				// The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
				$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id']);
			}
		}
	}

	public function getProducts() {
        /* @author sonicsjh */
        if (false !== $this->_cacheProductsInCart){
            return $this->_cacheProductsInCart;
        }
        /* @author sonicsjh */
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
		$sql = "SELECT vendor.vproduct_id FROM `vendor` WHERE vendor = 108";
		$query = $this->db->query($sql);
		$idarr = $query->rows;
		$idarr = array_column($idarr, 'vproduct_id');

        /* optimize by sonicsjh */
        foreach ($cart_query->rows as $productInfo)
            $qtyByProductId[$productInfo['product_id']] += $productInfo['quantity'];
        /* optimize by sonicsjh */

		foreach ($cart_query->rows as $cart) {
			$stock = true;

			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

			if ($product_query->num_rows && ($cart['quantity'] > 0)) {
				$option_price = 0;
				$option_points = 0;
				$option_weight = 0;

				$option_data = array();

				foreach (json_decode($cart['option']) as $product_option_id => $value) {
					$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($option_query->num_rows) {
						if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
							$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix, pov.product_code FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_value_query->num_rows) {
								if ($option_value_query->row['price_prefix'] == '+') {
									$option_price += $option_value_query->row['price'];
								} elseif ($option_value_query->row['price_prefix'] == '-') {
									$option_price -= $option_value_query->row['price'];
								}

								if ($option_value_query->row['points_prefix'] == '+') {
									$option_points += $option_value_query->row['points'];
								} elseif ($option_value_query->row['points_prefix'] == '-') {
									$option_points -= $option_value_query->row['points'];
								}

								if ($option_value_query->row['weight_prefix'] == '+') {
									$option_weight += $option_value_query->row['weight'];
								} elseif ($option_value_query->row['weight_prefix'] == '-') {
									$option_weight -= $option_value_query->row['weight'];
								}

								if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
									$stock = false;
								}

								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => $value,
									'product_code'            => $option_value_query->row['product_code'],
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => $option_value_query->row['option_value_id'],
									'name'                    => $option_query->row['name'],
									'value'                   => $option_value_query->row['name'],
									'type'                    => $option_query->row['type'],
									'quantity'                => $option_value_query->row['quantity'],
									'subtract'                => $option_value_query->row['subtract'],
									'price'                   => $option_value_query->row['price'],
									'price_prefix'            => $option_value_query->row['price_prefix'],
									'points'                  => $option_value_query->row['points'],
									'points_prefix'           => $option_value_query->row['points_prefix'],
									'weight'                  => $option_value_query->row['weight'],
									'weight_prefix'           => $option_value_query->row['weight_prefix']
								);
							}
						} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
							foreach ($value as $product_option_value_id) {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix, pov.product_code FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}

									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}

									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
										$stock = false;
									}

									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $product_option_value_id,
										'product_code'            => $option_value_query->row['product_code'],
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);
								}
							}
						} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'product_code'            => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => '',
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
								'weight_prefix'           => ''
							);
						}
					}
				}

				$price = $product_query->row['sale_price'];

				// Product Discounts
                /* optimize by sonicsjh */
                $discount_quantity = $qtyByProductId[$cart['product_id']];
                /* optimize by sonicsjh */
				//$discount_quantity = 0;

				//$cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

				//foreach ($cart_2_query->rows as $cart_2) {
				//	if ($cart_2['product_id'] == $cart['product_id']) {
				//		$discount_quantity += $cart_2['quantity'];
				//	}
				//}

				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end >= CURDATE())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				// Downloads
				$download_data = array();

				$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$cart['product_id'] . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

				foreach ($download_query->rows as $download) {
					$download_data[] = array(
						'download_id' => $download['download_id'],
						'name'        => $download['name'],
						'filename'    => $download['filename'],
						'mask'        => $download['mask']
					);
				}

				// Stock
				// if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
				// 	$stock = false;
				// }

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($recurring_query->num_rows) {
					$recurring = array(
						'recurring_id'    => $cart['recurring_id'],
						'name'            => $recurring_query->row['name'],
						'frequency'       => $recurring_query->row['frequency'],
						'price'           => $recurring_query->row['price'],
						'cycle'           => $recurring_query->row['cycle'],
						'duration'        => $recurring_query->row['duration'],
						'trial'           => $recurring_query->row['trial_status'],
						'trial_frequency' => $recurring_query->row['trial_frequency'],
						'trial_price'     => $recurring_query->row['trial_price'],
						'trial_cycle'     => $recurring_query->row['trial_cycle'],
						'trial_duration'  => $recurring_query->row['trial_duration']
					);
				} else {
					$recurring = false;
				}

				//清仓商品获取后台库存lack_status
                // if ($product_query->row['clearance']||$product_query->row['lack_status']==3) {
                    $sql = "SELECT SUM(i.available_quantity) as 'clearqty',pv.product_code FROM inventory AS i LEFT JOIN product_option_value AS pv ON pv.product_code = i.product_code LEFT JOIN warehouse AS w ON i.warehouse_id = w.warehouse_id WHERE i.`status` = 1 AND w.type!=3 AND pv.product_id=  ".$product_query->row['product_id'];
			                $query = $this->db->query($sql);
			                $clearqty = $query->row['clearqty'];

			                $procode = $pCode = substr($query->row['product_code'], 0, 10);
			                $tmp = $this->getsaleqty($procode,1);
			                // var_dump($tmp);
			                foreach ($tmp as $wh=>$qty) {
			                    $saleqty += $qty;
			                }

			                    // var_dump($saleqty);

			                    // var_dump($clearqty);
			                    // var_dump($saleqty);
			                 //    die();
			        $clearqty = $clearqty-$saleqty;
			        $clearqty = max(0,$clearqty); 
                // }
				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'name'            => $product_query->row['name'],
					'model'           => $product_query->row['model'],
					'shipping'        => $product_query->row['shipping'],
					'image'           => $product_query->row['image'],
					'option'          => $option_data,
					'download'        => $download_data,
					'quantity'        => $cart['quantity'],
					'minimum'         => $product_query->row['minimum'],
					'subtract'        => $product_query->row['subtract'],
					'stock'           => $stock,
					'price'           => ($price + $option_price),
					'total'           => (int)$product_query->row['gift_status']?0:($price + $option_price) * $cart['quantity'],
					'reward'          => $reward * $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
					'tax_class_id'    => $product_query->row['tax_class_id'],
					'weight'          => ($product_query->row['weight'] + $option_weight) * $cart['quantity'],
					'weight_class_id' => $product_query->row['weight_class_id'],
					'length'          => $product_query->row['length'],
					'width'           => $product_query->row['width'],
					'vendor_id'           => $product_query->row['vendor_id'],
					'height'          => $product_query->row['height'],
					'length_class_id' => $product_query->row['length_class_id'],
					'recurring'       => $recurring,
					'gift_status'     => $product_query->row['gift_status'],
					'clearance'     => $product_query->row['clearance'],
					'product_type'     => $product_query->row['product_type'],
					'clearqty'     => $clearqty
				);
				// var_dump($product_query->row['product_id']);
				// var_dump($idarr);

				if (in_array($product_query->row['product_id'],$idarr)) {
					$num +=$cart['quantity'];
				}
				$mwarr = array(2763,2762,2761 , 5098 ,2764,2519,5099);

				if (in_array($product_query->row['product_id'],$mwarr)) {
					$mwnum +=$cart['quantity'];
				}


				// var_dump($product_data);
			} else {
				$this->remove($cart['cart_id']);
			}
		}
		// if ($num>=10) {
		// 	$qytnum = (int)($num/10);
		// 	$product_data[] = array(
		// 			'cart_id'         => $cart['cart_id'],
		// 			'product_id'      => 3793,
		// 			'name'            => '赠送棉堡素色彩线缎边面巾 0244',
		// 			'model'           => 0244,
		// 			'shipping'        => 1,
		// 			'image'           => 'catalog/未命名文件夹 3/6955955170244.jpg',
		// 			'option'          => '',
		// 			'download'        => '',
		// 			'quantity'        => $qytnum,
		// 			'subtract'        => 1,
		// 			'stock'           => $stock,
		// 			'price'           => 0,
		// 			'total'           => 0,
		// 			'reward'          => 0,
		// 			'points'          => 0,
		// 			'tax_class_id'    =>0 ,
		// 			'weight'          =>0,
		// 			'weight_class_id' => 0,
		// 			'length'          => 0,
					
		// 		);
		// 	# code...
		// }

		// if ($mwnum>=10) {
		// 	$qytmw = (int)($mwnum/10);
		// 	$product_data[] = array(
		// 			'cart_id'         => $cart['cart_id'],
		// 			'product_id'      => 4493,
		// 			'name'            => '赠送迪美达 钢带子弹头500ml NCDZ-D500-2（蓝色）',
		// 			'model'           => 'NCDZ-D500-2',
		// 			'shipping'        => 1,
		// 			'image'           => 'catalog/永康/迪美达/NCDZ-D500-2/1.jpg',
		// 			'option'          => '',
		// 			'download'        => '',
		// 			'quantity'        => $qytmw,
		// 			'subtract'        => 1,
		// 			'stock'           => $stock,
		// 			'price'           => 0,
		// 			'total'           => 0,
		// 			'reward'          => 0,
		// 			'points'          => 0,
		// 			'tax_class_id'    =>0 ,
		// 			'weight'          =>0,
		// 			'weight_class_id' => 0,
		// 			'length'          => 0,
					
		// 		);
		// 	# code...
		// }
		// var_dump($num);
        /* @author sonicsjh */
        $this->_cacheProductsInCart = $product_data;
        /* @author sonicsjh */

		return $product_data;
	}

	public function getsaleqty($productCode,$flog) {
        $ret = '';
        $date = date('Ymd', strtotime('-2 month'));
        //获取单品销售的未发货数据
        $sql = "
        SELECT
            SUM(OP.`lack_quantity`) AS qty,OP.`product_code`,
            L.`default_warehouse` AS wh_id
        FROM
            `order_product` AS OP
            LEFT JOIN `order` AS O ON (O.`order_id`=OP.`order_id`)
            LEFT JOIN `logcenters` AS L ON (O.`logcenter_id`=L.`logcenter_id`)
        WHERE 
            (O.order_status_id = '1' OR O.order_status_id = '2' OR O.order_status_id = '3' OR O.order_status_id = '20' OR O.order_status_id = '17') AND O.verify_status != '2' AND O.date_added > '".$date."000000'
            AND OP.product_code ='".$productCode."0'
        GROUP BY
            L.`default_warehouse`
        ";
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $qtyInWarehouse[$row['wh_id']] += max(0, $row['qty']);
        }
        //获取商品组内部商品的未发货数据
        $sql = "
        SELECT
            SUM(OPG.`lack_quantity`) AS qty,OPG.`product_code`,
            L.`default_warehouse` AS wh_id
        FROM
            `order_product_group` AS OPG
            LEFT JOIN `order_product` AS OP ON (OPG.`order_product_id`=OP.`order_product_id`)
            LEFT JOIN `order` AS O ON (O.`order_id`=OP.`order_id`)
            LEFT JOIN `logcenters` AS L ON (O.`logcenter_id`=L.`logcenter_id`)
        WHERE 
            (O.order_status_id = '1' OR O.order_status_id = '2' OR O.order_status_id = '3' OR O.order_status_id = '20' OR O.order_status_id = '17') AND O.verify_status != '2' AND O.date_added > '".$date."000000'
            AND OPG.product_code ='".$productCode."0'
        GROUP BY
            L.`default_warehouse`
        ";
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->rows as $row) {
            $qtyInWarehouse[$row['wh_id']] += max(0, $row['qty']);
        }
        foreach ($qtyInWarehouse as $wh_id=>$qty) {
            if ($qty > 0 && $wh_id > 0) {
                if (4 == $wh_id) {
                    $tmp['无锡'] += $qty;
                }else{
                    $tmp['永康'] += $qty;
                }
            }
        }
        krsort($tmp);
        foreach ($tmp as $wh=>$qty) {
            $ret .= $qty.'（'.$wh.'）<br />';
        }
        if ($flog==1) {
            return $tmp;
            
        }else{
            return substr($ret,0, -6);
        }
        
    }

	public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0) {
        $this->addOptimize($product_id, $quantity, $option, $recurring_id);
        return true;
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

		if (!$query->row['total']) {
			$this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
		}
	}

    /* @author sonicsjh */
    public function addOptimize($product_id, $quantity = 1, $option = array(), $recurring_id = 0){
        $cartResult = $this->db->query("SELECT `cart_id` FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

        if (intval($cartResult->row['cart_id'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE `cart_id` = '".intval($cartResult->row['cart_id'])."'");
        } else {
            $this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
        }
    }

	public function update($cart_id, $quantity) {
		$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function remove($cart_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function clear() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function getRecurringProducts() {
		$product_data = array();

		foreach ($this->getProducts() as $value) {
			if ($value['recurring']) {
				$product_data[] = $value;
			}
		}

		return $product_data;
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getCouponC() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			// var_dump($product['product_id']);
			if ($product['product_id']==5288||$product['product_id']==5290||$product['product_id']==5291||$product['product_id']==5292||$product['product_id']==5293) {
				// echo 1;
				continue;
			}
			// echo 2;
			$total += $product['total'];
		}
	// var_dump($total);
		// $res['total'] = $total;

		return $total;
	}

	public function getCouponDis() {
		$num = 0;

		foreach ($this->getProducts() as $product) {
			$num ++;
		}
		return $num;
	}

	public function getCouponTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			// var_dump($product['product_id']);
			if ($product['vendor_id']==146) {
				// echo 1;
				continue;
			}
			// var_dump($product['product_id']);
			// var_dump($product['total']);
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->getProducts());
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}
}
