<?php
//*
header('Location: index.php');
exit();
// */
/*
 * 获取当前销售区域信息
 * @author sonicsjh
 */
//* 139 服务器
$GBCONFIG = array(
    'HOST'  => 'localhost',
    'USER'  => 'root',
    'PASS'  => 'Baihuozhan123',
    'PORT'  => '3306',
    'DB'    => 'bhz',
);
// */

function getUserNameByList($ids, $uList) {
    $ret = array();
    $list = explode(',', $ids);
    foreach ($list as $info) {
        if ($info) {
            $ret[] = $uList[$info]['name'];
        }
    }
    return implode($ret, ',');
}

function getUserListBySalesAreaId($saId, $uList) {
    $ret = array();
    foreach ($uList as $info) {
        if ($saId == $info['saId']) {
            $ret[] = $info['name'];
        }
    }
    return implode($ret, ',');
}

$mysql = mysqli_connect($GBCONFIG['HOST'], $GBCONFIG['USER'], $GBCONFIG['PASS'], $GBCONFIG['DB']);
mysqli_query($mysql, 'SET NAMES utf8');

$sql = "SELECT * FROM `sales_area`";
$res = mysqli_query($mysql, $sql);
while ($row = mysqli_fetch_assoc($res)) {
    $saList[] = $row;
}

$sql = "SELECT `user_id`,`username`,`fullname`,`contact_tel`,`sales_area_id` FROM `user`";
$res = mysqli_query($mysql, $sql);
while ($row = mysqli_fetch_assoc($res)) {
    $uList[$row['user_id']] = array(
        'name' => $row['fullname'].'（'.$row['contact_tel'].'）',
        'saId' => $row['sales_area_id'],
    );
}

$ret = array();
foreach ($saList as $k=>$saInfo) {
    $ret[$saInfo['name']] = array(
        '区域经理' => $uList[$saInfo['manager']]['name'],
        '经理助理' => getUserNameByList($saInfo['manager_assistant'], $uList),
        '业绩查看' => getUserNameByList($saInfo['view_report'], $uList),
        '下属业务员' => getUserListBySalesAreaId($saInfo['sales_area_id'], $uList),
    );

}

print_r($ret);
